@handheld
<!-- popup list all menu categories  -->
@include('template-part.header.modal-list-categories-menu')
<!-- modal header-topbar-right -->
@include('template-part.header.modal-header-topbar-right')
@endhandheld
<!-- {{-- Header --}} -->
<header id="site-header">
<!-- {{-- header-upper --}} -->
    <div class="header-topbar ">
        <div class="container">
            <!-- site-logo -->
            <div id="site-logo">
                <a href="{{ route('home')}}" title="Logo">
                    <span class="hidden-sm-down">
                        <img src="{{asset('assets/img/logo/Logo-01.svg')}}" alt="logo">
                    </span>
                    <span class="hidden-md-up">
                        <img src="{{asset('assets/img/logo/Logo-mobile.svg')}}" alt="logo">
                    </span>
                </a>
            </div>
            <!-- header-topbar-right -->
            <ul class="header-topbar-right list-inline text-right">
                @if(!Auth::check())
                    <li class="signin-url list-inline-item transition03">
                        <a href="{{route('auth.login')}}" title="Đăng nhập">
                            Đăng nhập
                        </a>
                        <span style="font-weight: 400">|</span>
                        <a href="{{route('auth.register')}}" title="Đăng ký">
                            Đăng ký
                        </a>
                    </li>
            @endif
            @auth
                <!-- header-notification -->
                @include('template-part.header.header-notification')
                <!-- end  header-notification -->
            @endauth
            <!-- header-notification -->
            @include('template-part.header.group-profile')
            <!-- end  header-notification -->

                <li class="list-inline-item">
                    <a class="btn-post-ad btn text-capitalize" href="{{ route('ads.new.select_cate')}}" title="">
                        Tôi muốn đăng tin
                    </a>
                </li>
            </ul>
            <!-- icon-toggle -->
            <div class="icon-toggle" data-toggle="modal" data-target="#modal-header-topbar-right">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
    </div>
<!-- {{-- end header-upper --}} -->
<!-- {{-- wrap-main-navigation --}} -->
@include('template-part.header.wrap-main-navigation')
<!-- {{-- end wrap-main-navigation --}} -->
</header>
<!-- /header -->