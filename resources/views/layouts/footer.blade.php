<!-- footer -->
<footer id="site-footer" role="footer">
	<div class="container">
		<ul class="social list-unstyled">
			<li>
				<a href="" title="Facebook" target="_blank">
					<i class="ion-social-facebook"></i>
				</a>
			</li>
			<li>
				<a href="" title="Google Plus" target="_blank">
					<i class="ion-google-plus"></i>
				</a>
			</li>
			<li>
				<a href="" title="Youtobe" target="_blank">
					<i class="ion-social-youtube"></i>
				</a>
			</li>
			<li>
				<img src="{{asset('assets/img/default-image/img-stamp.png')}}" alt="Đăng ký bộ công thương">
			</li>
		</ul>
		<!-- end social -->
		<div class="menu-footer">
			<ul class="list-unstyled">
				<li>
					<a href="{{route('landing-page.car')}}" title="Ô tô và Xe máy">Ô tô và Xe máy</a>
				</li>
				<li>
					<a href="{{route('search.list',['cate'=>'thiet-bi-gia-dinh'])}}" title="THIẾT BỊ GIA ĐÌNH">Thiết bị gia đình</a>
				</li>
				<li>
					<a href="{{route('landing-page.job')}}" title="VIỆC LÀM">Việc làm</a>
				</li>
				<li>
					<a href="{{route('landing-page.bds')}}" title="Bất động sản">Bất động sản</a>
				</li>
				<li>
					<a href="{{route('search.list',['cate'=>'dich-vu-cho-thue'])}}" title="DỊCH VỤ CHO THUÊ">DỊCH VỤ CHO THUÊ</a>
				</li>
				<li>
					<a href="{{route('search.list',['cate'=>'dien-tu-may-tinh'])}}" title="ĐIỆN TỬ MÁY TÍNH">Điện Tử</a>
				</li>
			</ul>
		</div>
		<!-- end menu footer -->
		<div class="footer-menu-suppor">
			<ul class="list-unstyled">
				<li>
					<a href="{{route('support-center')}}" title="Trung tâm hố trợ" target="_blank">Trung tâm hố trợ</a>
				</li>
				<li>
					<a href="{{route('an-toan-mua-ban')}}" title="An toàn mua bán" target="_blank">An toàn mua bán</a>
				</li>
				<li>
					<a href="{{route('support')}}" title="Quy định cần biết" target="_blank">Quy định cần biết</a>
				</li>
				<li>
					<a href="{{route('contact')}}" title="Liên hệ hố trợ" target="_blank">Liên hệ hố trợ</a>
				</li>
			</ul>
		</div>
		<!-- end footer menu suppor -->
		<hr>
		<div class="copyright">
			@ 2017 - Choviahe . All rights reserved  -  Designed by <a href="https://redsand.vn/" title="RedSand JSC" target="_blank">RedSand JSC</a>
		</div>
		<!-- end copyright -->
	</div>
</footer>
<!-- end footer -->
