<div class="card-horizontal-lg card-horizontal {{$ads->ads_class()}} mb-2" id="post-{{$ads->id}}">
    <div class="card-img-top">
        <span class="text-mockup"></span>
        @if ($ads->hasFeatures('has-gallery'))
            @if(count($ads->getThumbnail(3))==1)
                <a class="img-gallery single-gallery" href="{{$ads->getUrl()}}" title="{{$ads->title}}">
                    <img class="img-fluid" src="{{$ads->getThumbnail()[0]['small']}}" alt="{{$ads->title}}"
                         alt="{{$ads->title}}">
                </a>
            @elseif(count($ads->getThumbnail(3))==2)
                <a class="img-gallery vertical-gallery" href="{{$ads->getUrl()}}" title="{{$ads->title}}">
                    @foreach ($ads->getThumbnail(3) as $key => $image)
                        <img class="img-fluid" src="{{$image['vertical']}}" alt="{{$ads->title}}" alt="{{$ads->title}}">
                    @endforeach
                </a>
            @else
                <a class="img-gallery horizontal-gallery" href="{{$ads->getUrl()}}" title="{{$ads->title}}">
                    @foreach ($ads->getThumbnail(count($ads->images) <= 3 ? count($ads->images) : 3) as $key => $image)
                        <img class="img-fluid" src="{{$image['small']}}" alt="{{$ads->title}}" alt="{{$ads->title}}">
                    @endforeach
                </a>
            @endif
        @else
            <a href="{{$ads->getUrl()}}" title="{{$ads->title}}">
                <img class="img-fluid" src="{{$ads->getThumbnail()[0]['small']}}" alt="{{$ads->title}}"
                     alt="{{$ads->title}}">
            </a>
        @endif
    </div>
    <div class="card-block pb-0">
        <div class="clear-fix mb-sm-1">
            <div class="card-avatar d-xs-inline-block align-middle">
                <img src="{{$ads->author->getAvatar()}}" alt="{{$ads->author->name}}">
            </div>
            <div class="card-header">
                <h6 class="card-title text-capitalize mb-0">
                    <a href="{{$ads->getUrl()}}" title="{{$ads->title}}">{{$ads->title}}</a>
                </h6>
                <span class="card-price text-capitalize font-weight-bold pb-md-1 hidden-xs-down">{{$ads->priceFormat}}</span>
            </div>
            <div class="date-address">
                <p class="card-address m-0 text-truncate">{{$ads->address}}</p>
                <time class="hidden-xs-down"><span>{{$ads->ago}}</span></time>
            </div>
        </div>
        <span class="card-price text-uppercase font-weight-bold pb-md-1 hidden-sm-up">{{$ads->priceFormat}}</span>
        <div class="card-description">
            @if(Request::filled('distance'))
                <div class="detail-address text-truncate">
                    <i class="ion-ios-location"></i> khoảng cách chừng {{$ads->dis}} km
                </div>
            @endif
            <p class="hidden-sm-down">
                {{$ads->excerpt}}
            </p>
            <div class="date-address">
                <p class="card-address m-0 text-truncate">{{$ads->address}}</p>
                <time><span>{{$ads->ago}}</span></time>
            </div>
            <a class="add-favorite {{$ads->favoriteClass()}}" data-id="{{$ads->id}}"
               title="Thêm vào mục quan tâm" href="#">
                <i class="ion-ios-star"></i>
            </a>
        </div>
    </div>
</div>