<div class="card-post card {{$ads->ads_class()}}" id="post-{{$ads->id}}">
    <div class="card-img-top">
        <span class="text-mockup"></span>
        <a href="{{$ads->getUrl()}}" title="{{$ads->title}}">
            <img class="img-fluid" src="{{$ads->getThumbnail()[0]['small']}}" alt="{{$ads->title}}">
            <span class="card-price text-capitalize font-weight-bold text-center">{{$ads->priceFormat}}</span>
        </a>
    </div>
    <div class="card-block">
        <div class="card-avatar align-middle">
            <a href="{{route('account-seller',['id'=>$ads->user_id])}}"
               title="{{$ads->author->name}}">
                <img src="{{$ads->author->getAvatar()}}" alt="{{$ads->author->name}}">
            </a>
        </div>
        <div class="card-description align-middle">
            <h6 class="card-title text-truncate text-capitalize"><a href="{{$ads->getUrl()}}" title="{{$ads->title}}">{{$ads->title}}</a></h6>
            <a href="{{$ads->getUrl()}}" title="{{$ads->address}}" class="card-address d-block">{{$ads->address}}</a>
        </div>
        <a class="add-favorite {{$ads->favoriteClass()}}" href="#" title="Thêm vào mục ưa thích" data-id="{{$ads->id}}">
            <i class="ion-ios-star"></i>
        </a>
    </div>
</div>