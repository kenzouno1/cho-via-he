<div class="card-bds  card-horizontal card-img-zoom mb-3 {{$ads->ads_class()}}">
    <div class="d-flex flex-row">
        <div class="card-img-top card-img radius-none">
            <span class="text-mockup"></span>
            <a href="{{$ads->getUrl()}}" title="{{$ads->title}}">
                <img class="img-fluid" src="{{$ads->getThumbnail()[0]['small']}}" alt="{{$ads->title}}"
                     alt="{{$ads->title}}">
            </a>
        </div>
        <div class="card-block d-flex flex-row">
        	<div class="avata">
                <a href="{{route('account-seller',['id'=>$ads->user_id])}}"
                   title="{{$ads->author->name}}">
                    <img src="{{$ads->author->getAvatar()}}" alt="{{$ads->author->name}}">
                </a>
        	</div>
            <div class="card-description">
                <h6 class="card-title text-capitalize mb-0"><a href="{{$ads->getUrl()}}" title="{{$ads->title}}">{{$ads->title}} </a></h6>
                <p class="card-address mb-1 text-truncate">{{$ads->address}}</p>
                <span class="card-price text-capitalize font-weight-bold pb-1 text-truncate">{{$ads->priceFormat}}</span>
                <p class="mb-0 hidden-xs-down">
                    {{$ads->excerpt}}
                </p>
            </div>
            <a class="add-favorite {{$ads->favoriteClass()}}" data-id="{{$ads->id}}"
               title="Thêm vào mục quan tâm" href="#">
                <i class="ion-ios-star"></i>
            </a>
        </div>
    </div>
</div>
<!-- end card post -->