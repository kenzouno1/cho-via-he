<div class="card-user-activity card-horizontal">
    <div class="card-img-top">
        <a href="{{$ads->getUrl()}}" title="{{$ads->title}}">
            <img class="img-fluid" src="{{$ads->getThumbnail()[0]['small']}}" alt="{{$ads->title}}" >
        </a>
    </div>
    <div class="card-block">
        <div class="card-description">
            <h6 class="card-title text-truncate text-capitalize mb-0"><a href="{{$ads->getUrl()}}" title="{{$ads->title}}">{{$ads->title}} </a></h6>
             <span class="card-price text-capitalize font-weight-bold pb-1 text-truncate">{{$ads->priceFormat}}</span>
            <p class="card-address m-0 text-truncate">{{$ads->adress}}</p>
        </div>
        <a class="add-favorite {{$ads->favoriteClass()}}" href="#" title="Thêm vào mục quan tâm" data-id="{{$ads->id}}">
            <i class="ion-ios-star"></i>
        </a>
    </div>
</div>