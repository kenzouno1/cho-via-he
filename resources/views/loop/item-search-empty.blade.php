<div class="card-user-activity card-horizontal">
    <div class="card-img-top">
        <a href="{{$ads->getUrl()}}" title="{{$ads->title}}">
            <img class="img-fluid" src="{{asset('assets/img/default-image/no-image.png')}}" alt="{{$ads->title}}" >
        </a>
    </div>

    <div class="card-block">
        <div class="card-description">
            <h6 class="card-title mb-0"><a href="{{$ads->getUrl()}}" title="{{$ads->title}}">{{$ads->title}} </a></h6>
        </div>
        <a class="add-favorite {{$ads->favoriteClass()}}" data-id="{{$ads->id}}" title="Thêm vào mục quan tâm" >
            <i class="ion-ios-star"></i>
        </a>
    </div>
</div>