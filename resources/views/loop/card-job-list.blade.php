<div class="card-job-list card-horizontal-lg card-horizontal mb-2 {{$ads->ads_class()}}">
    <div class="card-block">
        <div class="card-block-top clear-fix  mb-sm-1">
            <div class="card-avatar d-inline-block align-middle">
                <a href="{{route('account-seller',['id'=>$ads->user_id])}}"
                   title="{{$ads->author->name}}">
                    <img src="{{$ads->author->getAvatar()}}" alt="{{$ads->author->name}}">
                </a>
            </div>
            <div class="card-header">
                <h6 class="card-title text-capitalize mb-1"><a href="{{$ads->getUrl()}}"
                                                             title="{{$ads->title}}">{{$ads->title}}</a></h6>
                <span class="card-price font-weight-bold pb-md-1 hidden-xs-down text-capitalize">{{$ads->priceFormat}}</span>
            </div>
            <div class="date-address">
                <p class="card-address m-0">{{$ads->address}}</p>
                <time class="hidden-xs-down">{{$ads->ago}}</time>
            </div>
        </div>
        <span class="card-price font-weight-bold pb-md-1 hidden-sm-up text-capitalize">{{$ads->get_meta('mucluong')}}</span>
        <div class="card-description">
            <p class="name-company font-weight-bold">
                {{$ads->get_meta('congty')}}
            </p>
            <div class="detail-address">
                <i class="ion-ios-location"></i> {{$ads->get_meta('diachi')}}
            </div>
            <p class="hidden-sm-down">
                {{$ads->excerpt}}
            </p>
            <a class="add-favorite {{$ads->favoriteClass()}}" data-id="{{$ads->id}}" title="Thêm vào mục quan tâm">
                <i class="ion-ios-star"></i>
            </a>
        </div>
    </div>
</div>