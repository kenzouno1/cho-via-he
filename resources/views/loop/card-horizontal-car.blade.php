<div class="card-horizontal-car card-horizontal card-img-zoom d-flex flex-row {{$ads->ads_class()}}"
     id="post-{{$ads->id}}">
    <div class="card-img-top card-img radius-none">
        <span class="text-mockup"></span>
        <a href="{{$ads->getUrl()}}" title="{{$ads->title}}">
            <a href="{{$ads->getUrl()}}" title="{{$ads->title}}">
                <img class="img-fluid" src="{{$ads->getThumbnail()[0]['small']}}" alt="{{$ads->title}}"
                     alt="{{$ads->title}}">
            </a>
    </div>
    <div class="card-block d-flex flex-row">
        <div class="avata">
            <a href="{{route('account-seller',['id'=>$ads->user_id])}}"
               title="{{$ads->author->name}}">
                <img src="{{$ads->author->getAvatar()}}" alt="{{$ads->author->name}}">
            </a>
        </div>
        <div class="card-description">
            <h6 class="card-title text-capitalize mb-0">
                <a href="{{$ads->getUrl()}}" title="{{$ads->title}}">{{$ads->title}}</a>
            </h6>
            <p class="card-address mb-1 text-truncate">{{$ads->address}}</p>
            <span class="card-price text-uppercase font-weight-bold pb-1 text-truncate">{{$ads->priceFormat}}</span>
        </div>
        <a class="add-favorite {{$ads->favoriteClass()}}" data-id="{{$ads->id}}" href="#">
            <i class="ion-ios-star"></i>
        </a>
    </div>
</div>