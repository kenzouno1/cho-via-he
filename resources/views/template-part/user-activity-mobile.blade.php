<ul class="mb-4 box-url-link list-unstyled bg-white my-4 p-2 hidden-sm-up">
    <li>
        <a href="{{route('member.saved-searches')}}" title="Đã tìm kiếm">Đã tìm kiếm</a>
    </li>
    <li>
        <a href="{{route('member.viewed')}}" title="Đã Xem"> Đã xem</a>
    </li>
    <li>
        <a href="{{route('member.favorited')}}" title="Tin bạn quan tâm">Quan tâm</a>
    </li>
</ul>
<!-- end box url -->