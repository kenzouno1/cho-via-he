<li class="group-profile list-inline-item transition03">
    <a href="#" title="Tên người sử dụng">
        @if($member = AuthHelper::current_user())
            <img class="rounded-circle" src="{{ get_avatar()}}" alt="Ảnh đại diện">
            {{$member->name}}
        @else
            <img class="rounded-circle" src="{{ asset('avatar/avatar-default.jpg')}}" alt="Ảnh đại diện">
            Tôi
        @endif
        <i class="ion-ios-arrow-down"></i>
    </a>
    <ul class="group-profile-sub group-nav list-unstyled text-left">
        <li class="wellcome text-center">Chào mừng
            @if($member = AuthHelper::current_user())
                {{$member->name}}
            @else
                bạn !
            @endif
        </li>
        <li>
            <a href="{{route('member.favorited')}}" title="Quan tâm"><i class="ion-android-star"></i> Quan tâm</a>
        </li>
        <li>
            <a href="{{route('member.viewed')}}" title="Đã xem"><i class="ion-eye"></i> Đã xem</a>
        </li>
        <li>
            <a href="{{route('member.manage-post')}}" title="Quản lý bài đăng"><i class="ion-image"></i> Quản lí bài
                đăng</a>
        </li>
        <li>
            <a href="{{route('member.saved-searches')}}" title="Danh sách đã tìm kiếm"><i class="ion-bookmark"></i>
                Danh sách đã tìm kiếm</a>
        </li>
        <li>
            <a href="{{route('member.message')}}" title="Tin nhắn"><i class="ion-android-chat"></i> Tin nhắn</a>
        </li>
        <li>
            <a href="{{route('payment.step1')}}" title="Nạp tiền vào tài khoản"><i class="ion-dollar"></i>Nạp tiền
                vào tài khoản</a>
        </li>
        <li>
            <a href="{{route('member.profile')}}" title="Trang cá nhân"><i class="ion-person"></i> Trang cá nhân</a>
        </li>
        @if(Auth::check())
            <li>
                <a href="{{route('auth.logout')}}" title="Đăng xuất"><i class="ion-arrow-right-a"></i>Đăng xuất</a>
            </li>
        @endif
    </ul>
</li>