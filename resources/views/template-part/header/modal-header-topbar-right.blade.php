<div class="modal " id="modal-header-topbar-right" tabindex="-1" role="dialog"
     aria-labelledby="box-list-categories-menu">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!-- modal-header -->
            <div class="modal-header">
                <h5 class="modal-title">Chào mừng @if($member = AuthHelper::current_user())
                        {{$member->name}}
                    @else
                        bạn !
                    @endif</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ion-close-round"></i>
                </button>
            </div>
            <!-- modal-body -->
            <ul class="group-profile-sub group-nav modal-body list-unstyled">
                <li>
                    <a href="{{route('member.favorited')}}" title="Quan tâm"><i class="ion-android-star"></i> Quan tâm</a>
                </li>
                <li>
                    <a href="{{route('member.viewed')}}" title="Đã xem"><i class="ion-eye"></i> Đã xem</a>
                </li>
                <li>
                    <a href="{{route('member.manage-post')}}" title="Quản lí bài đăng"><i class="ion-image"></i> Quản lí bài đăng</a>
                </li>
                <li>
                    <a href="{{route('member.saved-searches')}}" title="Danh sách đã tìm kiếm"><i class="ion-bookmark"></i> Danh sách đã tìm kiếm</a>
                </li>
                <li>
                    <a href="{{route('member.message')}}" title="Tin nhắn"><i class="ion-android-chat"></i> Tin nhắn</a>
                </li>
                <li>
                    <a href="{{route('payment.step1')}}" title="Trang cá nhân"><i class="ion-person"></i> Nạp tiền vào tài khoản</a>
                </li>
                <li>
                    <a href="{{route('member.profile')}}" title="Trang cá nhân"><i class="ion-person"></i> Trang cá nhân</a>
                </li>
                @if(Auth::check())
                <li>
                    <a href="{{route('auth.logout')}}" title="Thoát"><i class="ion-arrow-right-a"></i> Đăng xuất</a>
                </li>
                @endif
            </ul>
            <div class="btn-post-ad-mobi">
                <a class=" btn btn-link text-capitalize" href="{{ route('ads.new.select_cate')}}" title="Đăng tin mới">Tôi muốn đăng tin</a>
            </div>
            @if(!Auth::check())
                <div class="signin-url-mobi">
                    <div class="login-url">
                        <a href="{{route('auth.login')}}" title="Đăng nhập">Đăng nhập</a>
                    </div>
                    <div class="register-url">
                        <a href="{{route('auth.register')}}" title="Đăng ký">Đăng ký</a>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>