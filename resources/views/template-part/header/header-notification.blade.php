
<li class="header-notification list-inline-item">
    <a class="header-notification-icon" href="">
        <i class="ion-android-notifications"></i>
        <span>{{$notification->sum()}}</span>
    </a>
    <ul class="header-notification-sub list-unstyled text-left">
        <li>
            Thông báo
        </li>
        <li>
            <a class="d-flex align-items-center" href="{{route('member.message')}}" title="Quản lý tin nhắn">
                <i class="ion-android-notifications"></i>
                Bạn có {{$notification->get('messager','0')}} tin nhắn mới
            </a>
        </li>
    </ul>
</li>