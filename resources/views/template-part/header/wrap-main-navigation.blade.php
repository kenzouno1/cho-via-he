<div class="wrap-main-navigation clearfix" role="wrap-main-navigation">
    <div class="container">
        <div class="toggle-menu">
            <span></span>
            <span></span>
            <span></span>
        </div>
        <nav class="main-navigation">
            <ul class="main-menu list-unstyled">
                <li class="has-child-dropdownren root-item">
                    <a href="{{route('landing-page.car')}}" title="Ô TÔ VÀ XE MÁY">Ô TÔ VÀ XE MÁY </a>
                    <ul class="child-vertical sub-menu list-unstyled">
                        <li><a href="{{ route('search.list',['cate'=>'o-to'])}}" title="Ô tô"><strong>Ô tô</strong></a></li>
                        <li class="sub-menu-open has-child-dropdownren "><a href="{{route('search.list',['cate'=>'Xe-hoi'])}}" title="Xe hơi">Xe Hơi</a>
                          @include('template-part.nav-sub',['cateId'=>34])
                        </li>
                        <li class="has-child-dropdownren"><a href="{{route('search.list',['cate'=>'xe-tai'])}}" title="Xe Tải">Xe tải</a>
                          @include('template-part.nav-sub',['cateId'=>35])
                        </li>
                        <li><a href="{{route('search.list',['cate'=>'xe-khach'])}}" title="Xe khách">Xe khách</a>
                          @include('template-part.nav-sub',['cateId'=>36])
                        </li>
                        <li><a href="{{route('search.list',['cate'=>'xe-o-to-khac'])}}" title="Ô tô Khác">Khác</a>
                          @include('template-part.nav-sub',['cateId'=>91])
                        </li>
                        <li><a href="{{route('search.list',['cate'=>'xe-may'])}}" title="Xe máy"><strong>Xe máy</strong></a>
                        </li>
                        <li><a href="{{route('search.list',['cate'=>'xe-so'])}}" title="Xe số">Xe số</a>
                          @include('template-part.nav-sub',['cateId'=>38])
                        </li>
                        <li><a href="{{route('search.list',['cate'=>'xe-ga'])}}">Xe ga</a>
                          @include('template-part.nav-sub',['cateId'=>39])
                        </li>
                        <li><a href="{{route('search.list',['cate'=>'xe-the-thao'])}}" title="Xe thể thao">Xe thể thao</a>
                          @include('template-part.nav-sub',['cateId'=>97])
                        </li>
                        <li><a href="{{route('search.list',['cate'=>'xe-may-khac'])}}" title="Xe máy khác">Khác</a>
                          @include('template-part.nav-sub',['cateId'=>34])
                        </li>
                        <li><a href="{{route('search.list',['cate'=>'phu-tung-xe'])}}" title="Phụ tùng xe"><strong>Phụ tùng xe</strong></a>
                          @include('template-part.nav-sub',['cateId'=>41])
                        </li>
                    </ul>
                </li>
                <li class="has-child-dropdownren root-item"><a href="{{route('landing-page.bds')}}" title="Bất động sản">Bất động sản</a>
                    @include('template-part.nav-sub-bds')
                </li>
                <li class="has-child-dropdownren root-item"><a href="{{route('landing-page.job')}}" title="VIỆC LÀM"> VIỆC LÀM </a>
                    @include('template-part.nav-sub-job')
                </li>
                <li class="root-item"><a href="{{route('search.list',['cate'=>'thiet-bi-gia-dinh'])}}" title="BÁCH HÓA GIA ĐÌNH">BÁCH HÓA GIA ĐÌNH</a></li>
                <li class="root-item"><a href="{{route('search.list',['cate'=>'me-va-be'])}}" title="MẸ VÀ BÉ">MẸ VÀ BÉ</a></li>
                <li class="root-item"><a href="{{route('search.list',['cate'=>'dien-tu-may-tinh'])}}" title="ĐIỆN TỬ MÁY TÍNH">ĐIỆN TỬ MÁY TÍNH</a></li>
                <li class="has-child-dropdownren root-item">
                    <a href="#"> XEM THÊM DANH MỤC</a>
                    <ul class="root-item-more sub-menu list-unstyled">
                        <li><a href="{{route('search.list',['cate'=>'thoi-trang'])}}" title="Thời trang"> Thời trang </a></li>
                        <li><a href="{{route('search.list',['cate'=>'suc-khoe-sac-dep'])}}" title="Sức khỏe làm đẹp">Sức khỏe sắc đẹp</a></li>
                        <li><a href="{{route('search.list',['cate'=>'vat-nuoi-thu-cung'])}}" title="Vật nuôi thú cưng">Vật nuôi thú cưng</a></li>
                        <li><a href="{{route('search.list',['cate'=>'do-dung-van-phong'])}}" title="Đồ dùng văn phòng">Đồ dùng văn phòng</a></li>
                        <li><a href="{{route('search.list',['cate'=>'sach-dich-vu'])}}" title="Sách và dịch vụ">Sách và dịch vụ</a></li>
                        <li><a href="{{route('search.list',['cate'=>'dong-ho-phu-kien'])}}" title="Đồng hồ và phụ kiện">Đồng hồ và phụ kiện</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        @handheld
        <a class="view-categories-url" data-toggle="modal" data-target="#modal-list-categories-menu">
            Tất cả
        </a>
        @endhandheld
    </div>
</div>