<div class="modal " id="modal-list-categories-menu" tabindex="-1" role="dialog"
     aria-labelledby="box-list-categories-menu">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!-- modal-header -->
            <div class="modal-header">
                <h5 class="modal-title">Tất cả danh mục</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ion-close-round"></i>
                </button>
                <a href=""></a>
            </div>
            <!-- modal-body -->
            <ul class="list-dropdown modal-body list-unstyled">
                {!! \App\Helper\NavHelper::renderListCate() !!}
            </ul>
        </div>
    </div>
</div>