<ul class="child-horizontal-heigt-auto child-horizontal sub-menu list-unstyled">
    <li>
        <a href="{{UrlHelper::getUrl(['post-type'=>'rent'],route('search.list', ['cate' => 'bat-dong-san']))}}" title="Bất động sản cho thuê">
            <img src="{{asset('assets/img/default-image/bds-cho-thue.jpg')}}">
            <h4>563 Bài Đăng</h4>
        </a>
    </li>
    <li>
        <a href="{{UrlHelper::getUrl(['post-type'=>'buysell'], route('search.list', ['cate' => 'bat-dong-san']))}}" title="Bất động sản mua bán">
            <img src="{{asset('assets/img/default-image/bds-muaban.jpg')}}">
            <h4>594 Bài Đăng</h4>
        </a>
    </li>
    <li>
        <a href="{{UrlHelper::getUrl(['post-type'=>'new'], route('search.list', ['cate' => 'bat-dong-san']))}}" title="Bất động sản mới">
            <img src="{{asset('assets/img/default-image/bds-moi.jpg')}}">
            <h4>102 Bài Đăng</h4>
        </a>
    </li>
</ul>