@if(count($lstSavedSearch)>0)
    <div class="row-card ">
        @foreach($lstSavedSearch as $key=> $search)
            @include('loop.user-saved-search')
        @endforeach
    </div>
    <a class="view-more" href="{{route('member.saved-searches')}}" title="Xem tất cả">Xem tất cả</a>
@else
    @include('parts.search-empty')
@endif