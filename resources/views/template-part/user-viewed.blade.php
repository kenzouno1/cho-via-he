@if(count($lstViewed = AdsHelper::getAdsViewed())>0)
    <div class="row-card">
        @foreach($lstViewed as $ads)
            <div class="col-card-4 mb-3">
                @include('loop.card-user-activity')
            </div>
        @endforeach
    </div>
    <a class="view-more" href="{{route('member.viewed')}}" title="Xem tất cả">Xem tất cả</a>
@else
    @include('parts.viewed-empty')
@endif