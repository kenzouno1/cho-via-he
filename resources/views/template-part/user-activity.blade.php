<div class="user-activity tab-custom hidden-xs-down mb-4">
    <!-- Nav tabs -->
    <ul class="nav d-flex justify-content-center py-1">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#da-xem" role="tab"> Đã xem</a>
        </li>
        <li class="nav-item">
            <a class="nav-link " data-toggle="tab" href="#da-tim-kiem" role="tab">Đã tìm kiếm</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#quan-tam" role="tab">Quan tâm</a>
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane p-3 active" id="da-xem" role="tabpanel">
        @include('template-part.user-viewed')
        <!-- end not news -->
        </div>
        <div class="tab-pane  p-3" id="da-tim-kiem" role="tabpanel">
            @include('template-part.user-saved-search')
        </div>
        <div class="tab-pane p-3" id="quan-tam" role="tabpanel">
            @include('template-part.user-favorited')
        </div>
    </div>
</div>
<!-- end tab -->
@include('template-part.user-activity-mobile')
@push('script-custom')
    <script>
        $(window).bind('favorites.update',function(){
            $.ajax({
                url: cvh_data.favorites_url,
                type: 'GET',
            }).done(function (response) {
                if (response.status == 200) {
                    $('#quan-tam').html(response.html);
                }
            })
        });
    </script>
@endpush