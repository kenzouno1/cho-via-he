<section class="section-favorite ">
    <div class="container">
        <div class="header-title">
            <h4>Danh MỤC YÊU THÍCH</h4>
        </div>
        <!-- end header title -->
        <div class="tabs-favorite wrapper-tabs">
            <!-- Nav tabs -->
            <div class=" row">
                <ul class=" col-lg-3 list-unstyled text-uppercase" role="tablist">
                    <li class="nav-item">
                        <a class=" active" data-toggle="tab" href="#o-to-xe-may" role="tab"> <i class="ion-android-car"></i> Ô tô và xe máy</a>
                    </li>
                    <li class="nav-item">
                        <a data-toggle="tab" href="#bat-dong-san" role="tab"><i class="ion-podium"></i> BẤT ĐỘNG SẢN</a>
                    </li>
                    <li class="nav-item">
                        <a data-toggle="tab" href="#vieclam" role="tab"><i class="ion-briefcase"></i> VIỆC LÀM</a>
                    </li>
                    <li class="nav-item hidden-md-down">
                        <a data-toggle="tab" href="#tin-moi-nhat" role="tab"><i class="ion-ios-pricetag"></i> TIN MỚI NHẤT</a>
                    </li>
                    <li class="nav-item hidden-md-down">
                        <a data-toggle="tab" href="#quan-tam" role="tab"><i class="ion-android-star"></i> Quan tâm</a>
                    </li>
                    <li class="nav-item hidden-md-down">
                        <a data-toggle="tab" href="#danh-sach-da-tim-kiem" role="tab"><i class="ion-bookmark"></i> danh sách  đã tìm kiếm</a>
                    </li>
                    <li class="nav-item hidden-md-down">
                        <a data-toggle="tab" href="#da-xem" role="tab"><i class="ion-eye"></i> đã xem</a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content col-lg-9">
					@include('pages/home/parts/list-oto-xemay')
					@include('pages/home/parts/list-bds')
					@include('pages/home/parts/list-job')
					@include('pages/home/parts/list-new')
					@include('pages/home/parts/list-favorites')
					@include('pages/home/parts/list-saved-search')
					@include('pages/home/parts/list-viewed')
                </div>
                 <a class="btn-view-all btn-green text-uppercase font-weight-bold mt-2 btn btn-secondary hidden-sm-up w-100 mx-3" href="{{route('search.list')}}" title="">Xem Nhiều hơn</a>
            </div>
        </div>
    </div>
</section>