<!-- wrapper-banner -->
<div class="banner-page-content wrapper-banner">
    <div class="container">
        <div class="main-banner">
            @handheld
            @include('template-part.form-search-mobile')
            @endhandheld
            @include('template-part.form-search')
        <!-- wrapper-form-search -->
        </div>
    </div>
</div>
<!-- end wrapper-banner -->