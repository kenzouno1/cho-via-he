@if(count($lstFavorites)>0)
    <div class="list-post-card">
        <div class="row">
            @foreach($lstFavorites as $ads)
                <div class="col-md-4 col-sm-6 col-2">
                    @include('loop.card-post')
                </div>
            @endforeach
        </div>
    </div>
@else
    <div class="row">
        @include('parts.favorites-tempty')
    </div>
@endif