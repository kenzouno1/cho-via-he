<div class="search-controls">
    <form action="{{route('search.submit')}}" class="wrapper-form-search clearfix" method="POST">
        {{csrf_field()}}
        <div class="search-control-header">
            <h5>Tìm kiếm</h5>
            <button type="button" class="close">
                <i class="ion-close-round"></i>
            </button>
        </div>
        <div class="wrap-dropdown-categories wrap-dropdown">
            <a class="btn-dropdown" href="#" title="advance">
                <i class="{{isset($currentCate) && !is_null($currentCate) ? $currentCate->image : 'ion-android-menu'}}"></i>
            </a>
            <ul class="list-dropdown">
                {!! \App\Helper\NavHelper::renderListCate() !!}
            </ul>
            <input type="hidden" name="cate"
                   value="{{isset($currentCate) && !is_null($currentCate) ? $currentCate->slug : ''}}">
        </div>
        <div class="form-input-search form-input">
            <input type="search" name="title" class="form-control input-search"
                   placeholder="Nhập từ khoá bạn cần tìm ..."
                   value="{{Request::filled('tu-khoa') ? Request::get('tu-khoa') : ''}}">
            <a  href="#" id="saved-search" title="Lưu tìm kiếm">Lưu tìm kiếm <i class="ion-bookmark"></i></a>
        </div>
        <div class="form-input-address form-input">
            <input id="search-address" type="text" name="address" class="form-control location"
                   value="{{Request::filled('address') ? Request::get('address') : config('conf.default_search.address')}}"
                   placeholder="Hồ Chí Minh">
            <input id="search-location" type="hidden" name="location"
                   value="{{Request::filled('location') ? Request::get('location') : config('conf.default_search.location')}}">
        </div>
        <div class="wrap-dropdown-km wrap-dropdown custom-select-option">
            <div class="btn-select"><a href="#" title="advance">{{Request::filled('distance') ? Request::get('distance') : 20}}
                    km</a></div>
            <ul class="list-dropdown option-select">
                @foreach(config('conf.km') as $item)
                    <li data-value="{{$item}}" class="has-child-dropdown ">
                        <span class="text"> {{$item}}km </span>
                    </li>
                @endforeach
            </ul>
            <input type="hidden" name="distance" value="{{Request::filled('distance') ? Request::get('distance') : 20}}">
        </div>
        <button class="btn-primary button-search">
            <i class="ion-ios-search-strong"></i>
        </button>

    </form>
</div>
@push('css-custom')
    <link rel="stylesheet" href="{!! asset('assets/vendor/jquery-ui/jquery-ui.min.css') !!}">
@endpush
@section('js')
    <script src="{!! asset('assets/vendor/alertify/all.js') !!}"></script>
    <script src="{!! asset('assets/vendor/jquery-ui/jquery-ui.min.js') !!}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_API','AIzaSyCcCGIu_Jz3R1wUtpLCtxjvNDNlueSCuTE')}}&libraries=places"
    ></script>
    <script type="text/javascript">
        /* Load search heder
        -------------------------------------------------------------------------- */
        $('.input-search').autocomplete({
            source: '{{route('search.autocomplete')}}',
            minLength: 2,
            select: function (event, ui) {
                $('.input-search').val(ui.item.value);
            }
        });
        /*
            Map Location
          */
        var input = document.getElementsByClassName('location');
        for (var i = 0; i < input.length; i++) {
            searchmap = new google.maps.places.Autocomplete(
                input[i],
                {
                    types: ['geocode'],
                    componentRestrictions: {country: "vn"}
                }
            );
            google.maps.event.addListener(searchmap, 'place_changed', function () {
                var place = searchmap.getPlace();
                var lat = place.geometry.location.lat();
                var lng = place.geometry.location.lng();
                var latlng = '' + lat + '-' + lng + '';
                $('input[name="location"]').val(latlng);
            });

        }

        $('.location').on('keyup keypress', function (e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });
//        saved search
        $('#saved-search').click(function (event) {
            event.preventDefault();
            var data = $(this).closest('form').serialize();
            $.ajax({
                url: cvh_data.saved_search_url,
                type: 'POST',
                data: data,
            }).done(function (response) {
                if (response.status == 200) {
                    $('#da-tim-kiem').html(response.html)
                    alertify.success("Lưu thành công");
                }

            });
        });
//        end  saved search

        $('.input-search-mobi').click(function(event) {
            event.preventDefault();
            $(this).closest('.wrapper-banner').addClass('show-search-mobile');
        });
        $('.search-control-header .close').click(function(event) {
            $(this).closest('.wrapper-banner').removeClass('show-search-mobile')
        });
    </script>
@endsection