<div class="box-img-ads text-center" style="background-image: url({{$cateDetails['image']}})">
    <a href="{{ route('search.list',['cate'=>$cate->slug])}}" title="Tất cả bài đăng {{$cate['name']}}">
        <div class="description">
            <span>Xem tất cả {{$cate['count']}} bài đăng </span>
            <h5 class="text-uppercase m-0">Dành cho {{$cate['name']}}</h5>
        </div>
    </a>
</div>
<!-- end box-img-ads -->
