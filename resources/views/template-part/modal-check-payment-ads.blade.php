<div id="modal-check-price" class="modal fade modal-report" id="modal-report" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!-- modal-header -->
            <div class="modal-header">
                <h5 class="modal-title">Tài khoản của bạn không đủ !</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ion-close-round"></i>
                </button>
            </div>
            <!-- modal-body -->
            <div class="modal-body">
                <ul class="list-report list-unstyled text-center">
                    <li class="current-price"><a href="#"></a></li>
                    <li class="order-price"><a href="#"></a></li>
                    <li><a href="{{route('payment.step1')}}">Vui lòng nạp thêm tiền vào tài khoản hoặc giảm bớt tính
                            năng</a></li>
                </ul>
            </div>
            <!-- end modal body -->
        </div>
    </div>
</div>
{{--@push('script-custom')--}}
{{--<script>--}}

{{--</script>--}}
{{--@endpush--}}