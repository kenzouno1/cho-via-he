<ul class="child-horizontal sub-menu list-unstyled">
    @foreach(\App\Helper\NavHelper::loadCategoryNav($cateId) as $nav)
        @include('loop.nav-cate',['navItem'=>$nav])
    @endforeach
</ul>