<section class="section-featured-categories">
	<div class="container">
		<div class="header-title">
            <h4>DANH MỤC ĐẶC TRƯNG</h4>
        </div>
        <!-- end header title -->
        <div class="row">
        	<div class="col-md-4 col-sm-6">
				<div class="card-full-image ">
				    <a href="{{ route('landing-page.car')}}" title="Ô tô và xe máy">
				        <img class="card-img-top transition05" src="{{asset('assets/img/upload/cate1.png')}}" alt="">
				        <div class="card-block text-center">
				            <h5 class="card-title text-uppercase">Ô tô và xe máy</h5>
				            <p class="card-text">{{\App\Helper\CategoryHelper::getCountAdsById(17)}} thông tin</p>
				        </div>
				    </a>
				</div>
        	</div>
			<div class="col-md-4 col-sm-6">
				<div class="card-full-image ">
					<a href="{{ route('landing-page.job')}}" title="Việc làm">
						<img class="card-img-top transition05" src="{{asset('assets/img/upload/cate3.png')}}" alt="">
						<div class="card-block text-center">
							<h5 class="card-title text-uppercase">Việc làm</h5>
							<p class="card-text">{{\App\Helper\CategoryHelper::getCountAdsById(19)}} thông tin</p>
						</div>
					</a>
				</div>
				<!-- end card full image -->
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="card-full-image ">
					<a href="{{ route('landing-page.bds')}}" title="Bất động sản">
						<img class="card-img-top transition05" src="{{asset('assets/img/upload/cate2.png')}}" alt="">
						<div class="card-block text-center">
							<h5 class="card-title text-uppercase">Bất động sản</h5>
							<p class="card-text">{{\App\Helper\CategoryHelper::getCountAdsById(18)}} thông tin</p>
						</div>
					</a>
				</div>
				<!-- end card full image -->
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="card-full-image ">
					<a href="{{route('search.list',['cate'=>'thiet-bi-gia-dinh'])}}" title="Thiết bị gia đình">
						<img class="card-img-top transition05" src="{{asset('assets/img/upload/cate4.png')}}" alt="">
						<div class="card-block text-center">
							<h5 class="card-title text-uppercase">Thiết bị gia đình</h5>
							<p class="card-text">{{\App\Helper\CategoryHelper::getCountAdsById(20)}} thông tin</p>
						</div>
					</a>
				</div>
				<!-- end card full image -->
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="card-full-image ">
					<a href="{{route('search.list',['cate'=>'dich-vu-cho-thue'])}}" title="Dịch vụ cho thuê">
						<img class="card-img-top transition05" src="{{asset('assets/img/upload/cate5.png')}}" alt="">
						<div class="card-block text-center">
							<h5 class="card-title text-uppercase">Dịch vụ Cho Thuê</h5>
							<p class="card-text">{{\App\Helper\CategoryHelper::getCountAdsById(21)}} thông tin</p>
						</div>
					</a>
				</div>
				<!-- end card full image -->
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="card-full-image ">
					<a href="{{route('search.list',['cate'=>'dien-tu-may-tinh'])}}" title="Điện tử máy tính">
						<img class="card-img-top transition05" src="{{asset('assets/img/upload/cate6.png')}}" alt="">
						<div class="card-block text-center">
							<h5 class="card-title text-uppercase">Điện tử máy tính</h5>
							<p class="card-text">{{\App\Helper\CategoryHelper::getCountAdsById(22)}} thông tin</p>
						</div>
					</a>
				</div>
				<!-- end card full image -->
			</div>
        </div>
	</div>
</section>