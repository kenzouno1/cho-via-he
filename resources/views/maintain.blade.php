<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Demo chợ  vỉa hè</title>
    @include('layouts.favicon')
    <link rel="stylesheet" href="{{ asset('backend/login/main.css ') }}">

</head>

<body>
<div class="main-bg"></div>
<div class="login-form">
    <a href="#" class="admin-logo">
        <img src="{{asset('assets/img/logo/Logo-01.svg')}}" alt="">
    </a>
    <form action="{{route('demo.login')}}" method="post">
        {{ csrf_field() }}
        <h3>Đăng Nhập</h3>
        <div class="form-wrap">
            <input type="text" name="user" value="{{ old('user') }}" placeholder="Tên Tài Khoản" required>

        </div>
        <div class="form-wrap">
            <input type="password" name="pwd" placeholder="Mật khẩu" required>
        </div>
        <div class="form-wrap">
            <button type="submit">Đăng Nhập</button>
        </div>
    </form>
</div>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all" rel="stylesheet" type="text/css">
</body>
</html>
