@extends('master-signin')
@section('content-page')
    <main class="wrapper-main" role="main">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="login-register col-lg-8 ">
                    <div class="row d-flex align-items-stretch">
                        <div class="left-login-register col-md-4 hidden-sm-down d-flex align-items-center">
                            <img src="{{asset('assets/img/default-image/singin-img.png')}}" alt="Signin Image">
                        </div>
                        <div class="col-md-8">
                            <div class="right-login-register">
                                <div class="sign-head">
                                    <h3>Đăng nhập</h3>
                                    <p>
                                        Đăng nhập ngay để trải nghiệm nhiều tính năng của chợ vỉa hè<br>
                                        * Là thông tin bắt buộc
                                    </p>
                                </div>
                                <div class="row">
                                    <div class="col-sm-8">
                                        <form class="login-register-form" method="POST" route="{{route('auth.login')}}"  data-parsley-validate="">
                                            <input type="hidden" name="_callback" value="{{$callbackUrl}}">
                                            {{csrf_field()}}
                                            <div class="form-group {{validate_form_class('phone',$errors)}}">
                                                <label class="hidden-xs-down" for="phone">Số điện
                                                    thoại*</label>
                                                <input  tabindex="1" type="number" name="phone" class="form-control" id="phone" placeholder="Số điện thoại*" required="" data-parsley-required-message="Nhập số điện thoại" data-parsley-length="[10, 11]" data-parsley-length-message="Vui lòng kiểm tra lại số điện thoại" data-parsley-type="number">
                                                {{ render_validate_msg('phone',$errors) }}
                                            </div>
                                            <div  class="form-link ">
                                                <a href="{{route('auth.forgotpwd')}}" title="">Quên mật khẩu?</a>
                                            </div>
                                          <div class="form-group {{validate_form_class('phone',$errors)}}">
                                                <label class="hidden-xs-down" for="pwd">Mật
                                                    khẩu*</label>
                                                <input  tabindex="2" type="password" name="pwd" class="form-control" id="pwd" placeholder="Mật khẩu*" required="" data-parsley-required-message="Nhập mật khẩu" data-parsley-length="[6, 40]" data-parsley-length-message="Vui lòng kiểm tra lại mật khẩu">
                                                {{ render_validate_msg('pwd',$errors) }}
                                            </div>
                                            {{render_error_msg($errors)}}
                                            <div class="form-group form-check">
                                                <div class=" form-check-inline">
                                                    <label class="form-check-label">
                                                        <input tabindex="4" type="checkbox" name="remember" class="form-check-input">
                                                        Ghi nhớ mật khẩu
                                                    </label>
                                                </div>
                                                <div class="form-check-inline form-link">
                                                    <a href="{{route('auth.register')}}" title="Tới trang đăng ký">Đăng ký</a>
                                                </div>
                                            </div>
                                            <button  tabindex="3" type="submit" class="btn btn-primary">Đăng nhập</button>
                                        </form>
                                    </div>
                                    <div class="col-sm-4">
                                        @include('auth.sociallogin')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

@push('body-class')
{{'singin-page'}}
@endpush
@push('script-custom')
    @include('auth.assets.js')
@endpush
