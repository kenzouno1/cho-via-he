@extends('master-signin')
@section('content-page')
    <main class="wrapper-main" role="main">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="login-register col-lg-8 ">
                    <div class="row d-flex align-items-stretch">
                        <div class="left-login-register col-md-4 hidden-sm-down d-flex align-items-center">
                            <img src="{{asset('assets/img/default-image/activate-img.png')}}" alt="">
                        </div>
                        <div class="col-md-8">
                            <div class="right-login-register">
                                <div class="sign-head">
                                    <h3>Kích hoạt tài khoản</h3>

                                    <p>@if(Session::has('errtext'))
                                            {!! Session::get('errtext') !!}
                                        @else
                                            Để đảm bảo an toàn cho tài khoản của bạn, chúng tôi đã gửi<br> mã khích hoạt
                                            tới điện thoại của bạn.<br> Hãy nhập nó vào đây
                                        @endif
                                    </p>

                                </div>
                                <form class="login-register-form" method="POST" action="{{route('auth.active',compact('phone'))}}"
                                      data-parsley-validate="">
                                    {{ csrf_field()	}}
                                    <input type="hidden" name="_callback" value="{{$callbackUrl}}">
                                    <input type="hidden" name="_action" value="{{$callbackUrl}}">
                                    <div class="form-group {{validate_form_class('code',$errors)}}">
                                        <label class="hidden-xs-down" for="active-code">Nhập mã kích hoạt*</label>
                                        <input tabindex="1" type="text" class="form-control" name="code"
                                               id="active-code" placeholder="Nhập mã kích hoạt*" required=""
                                               data-parsley-required-message="Nhập mật mã kích hoạt"
                                               data-parsley-length="[4, 6]"
                                               data-parsley-length-message="Vui lòng kiểm tra mã kích hoạt">
                                        {{ render_validate_msg('code',$errors) }}
                                    </div>

                                    <div class="form-group {{validate_form_class('name',$errors)}}">
                                        <label class="hidden-xs-down" for="name">Họ tên của bạn*</label>
                                        @auth
                                            <input tabindex="2" type="text" class="form-control"
                                                   value="{{old('name',AuthHelper::current_user()->name)}}" name="name"
                                                   id="name" placeholder="Họ tên của bạn*" required=""
                                                   data-parsley-required-message="Nhập họ tên của bạn" minlength="2"
                                                   data-parsley-minlength-message="Vui lòng kiểm tra lại họ tên của bạn">
                                        @endauth
                                        @guest
                                            <input tabindex="2" type="text" class="form-control" value="{{old('name')}}"
                                                   name="name" id="name" placeholder="Họ tên của bạn*" required=""
                                                   data-parsley-required-message="Nhập họ tên của bạn" minlength="2"
                                                   data-parsley-minlength-message="Vui lòng kiểm tra lại họ tên của bạn">
                                        @endguest

                                        {{ render_validate_msg('name',$errors) }}
                                    </div>
                                    {{render_error_msg($errors)}}
                                    <button tabindex="3" type="submit" class="btn btn-primary">Kích hoạt tài khoản
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
@push('script-custom')
    @include('auth.assets.js')
@endpush

