<div class="login-register-social social">
    <a href="{{route('auth.social',['provider'=>'facebook'])}}" title="Đăng nhập bằng Facebook">
        <i class="ion-social-facebook"></i>
    </a>
    <a href="{{route('auth.social',['provider'=>'google'])}}" title="Đăng nhập bằng google plus">
        <i class="ion-google-plus"></i>
    </a>
</div>