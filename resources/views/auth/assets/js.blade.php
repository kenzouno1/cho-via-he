<script src="{!! asset('assets/vendor/parsley/parsley.min.js') !!}"></script>
<script type="text/javascript">
    $(".login-register-form").parsley({
        errorClass: 'has-danger',
        successClass: 'has-success',
        classHandler: function (ParsleyField) {
            return ParsleyField.$element.parents('.form-group');
        },
        errorsContainer: function (ParsleyField) {
            return ParsleyField.$element.parents('.form-group');
        },
        errorsWrapper: '<span class="form-control-feedback">',
        errorTemplate: '<div></div>'
    });
</script>