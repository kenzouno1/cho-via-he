@extends('master-signin')
@section('content-page')
    <main class="wrapper-main" role="main">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="login-register col-lg-8 ">
                    <div class="row d-flex align-items-stretch">
                        <div class="left-login-register col-md-4 hidden-sm-down d-flex align-items-center">
                            <img src="{{asset('assets/img/default-image/activate-img.png')}}" alt="">
                        </div>
                        <div class="col-md-8">
                            <div class="right-login-register">
                                <div class="sign-head">
                                    <h3>Kích hoạt tài khoản</h3>
                                    <p>
                                        Để đảm bảo an toàn cho tài khoản của bạn,<br> chúng tôi đã gửi mã khích hoạt tới điện thoại của bạn.<br> Hãy nhập nó vào đây
                                    </p>
                                </div>
                                <form class="login-register-form" method="POST" action="{{route('auth.submit.forgot.pwd',compact('phone'))}}" data-parsley-validate="">
                                    {{ csrf_field()}}
                                    <div class="form-group {{validate_form_class('code',$errors)}}">
                                        <label class="hidden-xs-down" for="active-code">Nhập mã xác nhận*</label>
                                        <input  tabindex="1" type="text" class="form-control" name="code" id="active-code" placeholder="Nhập mã kích hoạt*" required="" data-parsley-required-message="Nhập mật mã kích hoạt" data-parsley-length="[4, 6]" data-parsley-length-message="Vui lòng kiểm tra mã kích hoạt">
                                        {{ render_validate_msg('code',$errors) }}
                                    </div>
                                    {{render_error_msg($errors)}}
                                    <button  tabindex="2" type="submit" class="btn btn-primary">Xác nhận</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
@push('script-custom')
    @include('auth.assets.js')
@endpush