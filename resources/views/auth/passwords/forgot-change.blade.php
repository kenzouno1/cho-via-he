@extends('master-signin')
@section('content-page')
    <main class="wrapper-main" role="main">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="login-register col-lg-8 ">
                    <div class="row d-flex align-items-stretch">
                        <div class="left-login-register col-md-4 hidden-sm-down d-flex align-items-center">
                            <img src="{{asset('assets/img/default-image/singup-img.png')}}" alt="">
                        </div>
                        <div class="col-md-8">
                            <div class="right-login-register">
                                <div class="sign-head">
                                    <h3>Đổi mật khẩu</h3>
                                </div>
                                <form class="login-register-form" method="POST" action="{{route('auth.forgot.changepwd')}}" data-parsley-validate="">
                                    {{ csrf_field()}}
                                    <div class="form-group {{validate_form_class('newpwd',$errors)}}">
                                        <label class="hidden-xs-down" for="active-code">Nhập mật khẩu mới*</label>
                                        <input  tabindex="1" type="password" class="form-control" name="newpwd" required="" data-parsley-required-message="Nhập mật khẩu" data-parsley-length="[6, 40]" data-parsley-length-message="Vui lòng kiểm tra lại mật khẩu">
                                        {{ render_validate_msg('newpwd',$errors) }}
                                    </div>
                                    {{render_error_msg($errors)}}
                                    <button  tabindex="2" type="submit" class="btn btn-primary">Đổi mật khẩu</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
@push('script-custom')
    @include('auth.assets.js')
@endpush