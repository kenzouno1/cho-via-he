@extends('master-signin')
@section('content-page')
    <main class="wrapper-main" role="main">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="login-register col-lg-8 ">
                    <div class="row d-flex align-items-stretch">
                        <div class="left-login-register col-md-4 hidden-sm-down d-flex align-items-center">
                            <img src="{{asset('assets/img/default-image/singin-img.png')}}" alt="Sign Up Image">
                        </div>
                        <div class="col-md-8">
                            <div class="right-login-register">
                                <div class="sign-head">
                                    <h3>Đăng ký</h3>
                                    <p>
                                        Đăng ký tài khoản ngay với Chợ Vỉa Hè để được trải nghiệm những <br>tính năng tuyệt vời nhất!
                                    </p>
                                </div>
                                <div class="row">
                                    <div class="col-sm-8">
                                        <form class="login-register-form" method="POST" action="{{ route('auth.register') }}"  data-parsley-validate="">
                                            {{ csrf_field() }}
                                            <div class="form-group {{validate_form_class('phone',$errors)}}">
                                                <label class="hidden-xs-down" for="phone">Số điện thoại*</label>
                                                <input tabindex="1" type="number" value="{{old('phone')}}" name="phone" class="form-control" id="phone" placeholder="Số điện thoại*" required="" data-parsley-required-message="Nhập số điện thoại" data-parsley-length="[10, 11]" data-parsley-length-message="Vui lòng kiểm tra lại số điện thoại" data-parsley-type="number">
                                                {{ render_validate_msg('phone',$errors) }}
                                            </div>
                                            <div class="form-group {{validate_form_class('pwd',$errors)}}">
                                                <label class="hidden-xs-down" for="pwd">Mật khẩu*</label>
                                                <input  tabindex="2" type="password" name="pwd" class="form-control" id="pwd" placeholder="Mật khẩu*"  required="" data-parsley-required-message="Nhập mật khẩu" data-parsley-length="[6, 40]" data-parsley-length-message="Vui lòng kiểm tra lại mật khẩu">
                                                {{ render_validate_msg('pwd',$errors) }}
                                            </div>
                                            <div class="form-check-inline form-link">
                                                <a href="{{route('auth.login')}}" title="Tới trang đăng nhập">Bạn đã có tài khoản?</a>
                                            </div>
                                            <button  tabindex="3" type="submit" class="btn btn-primary">Đăng ký</button>
                                        </form>
                                    </div>
                                    <div class="col-sm-4">
                                        @include('auth.sociallogin')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
@push('script-custom')
    @include('auth.assets.js')
@endpush
