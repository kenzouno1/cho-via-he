@extends('master-signin')
@section('content-page')
    <main class="wrapper-main" role="main">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="login-register col-lg-8 ">
                    <div class="row d-flex align-items-stretch">
                        <div class="left-login-register col-md-4 hidden-sm-down d-flex align-items-center">
                            <img src="{{asset('assets/img/default-image/activate-img.png')}}" alt="">
                        </div>
                        <div class="col-md-8">
                            <div class="right-login-register">
                                <div class="sign-head">
                                    <h3>Nhập số điện thoại</h3>
                                </div>
                                <form class="login-register-form" method="POST" action="{{route('auth.submit-phone')}}" data-parsley-validate="">
                                    {{ csrf_field()	 }}
                                    <div class="form-group {{validate_form_class('phone',$errors)}}">
                                        <label class="hidden-xs-down" for="phone">Số điện thoại*</label>
                                        <input tabindex="1" type="tel" class="form-control" name="phone" id="phone" placeholder="Số điện thoại*"  required="" data-parsley-required-message="Nhập số điện thoại" data-parsley-length="[10, 11]" data-parsley-length-message="Vui lòng kiểm tra lại số điện thoại" data-parsley-type="number">
                                        {{ render_validate_msg('phone',$errors) }}
                                    </div>
                                    {{render_error_msg($errors)}}
                                    <button  tabindex="2" type="submit" class="btn btn-primary">Lưu</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
@push('script-custom')
    @include('auth.assets.js')
@endpush