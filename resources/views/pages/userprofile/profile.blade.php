@extends('master')
@section('content-page')
    @include('pages.userprofile.parts.modal-suscess')
    @include('pages.userprofile.parts.modal-repost')
    {{--end  modal repost step 2--}}
    <main class="manage-page wrapper-main" role="main">
        <!-- wrapper-banner -->
        <div class="banner-page-content wrapper-banner"
             style="background-image:url('{{asset('assets/img/default-image/bg-banner.png')}}'); ">
        </div>
        <!-- end wrapper-banner -->
        <section class="accout-seller section-content bg-graylight">
            <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="col-lg-10">
                        @include('pages.userprofile.parts.information')
                        {{--end accout seller header--}}
                        <div class="accout-seller-title mb-3 bg-white px-3 py-4">
                            <div class="header-title">
                                <h4 class="bg-white">Đăng tin cá nhân</h4>
                            </div>
                            <ul class="nav font-weight-bold" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link p-0 @if(!isset($active)) active @endif"
                                       href="{{route('account-seller',['id'=>$member->id])}}">Tin
                                        Đang bán ({{$totalAdsActive}})</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link @if(isset($active)) active @endif p-0"
                                       href="{{route('account-seller.lst.sold',['id'=>$member->id])}}">Tin đã bán
                                        ({{$totalAdsSold}})</a>
                                </li>
                            </ul>
                        </div>
                        {{--end account seller title--}}
                        <div class="account-seller-content px-4  py-3 bg-white mb-3">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tin-dang-ban" role="tabpanel">
                                    @if($lstAds->isNotEmpty())
                                        <div class="row">
                                            @foreach($lstAds as $ads)
                                                <div class="col-md-4 col-6">
                                                    @include('loop.card-post')
                                                </div>
                                            @endforeach
                                        </div>
                                        {{$lstAds->links()}}
                                    @else
                                        @include('pages.myprofile.parts.list-post-empty')
                                    @endif
                                </div>
                            </div>
                        </div>
                        {{--end content--}}
                    </div>
                </div>
            </div>
        </section>
        <!-- end section contnet search -->
    </main>
@endsection
@push('script-custom')
    @include('pages.userprofile.assets.js')
@endpush
