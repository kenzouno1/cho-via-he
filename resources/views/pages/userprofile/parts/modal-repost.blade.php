<div class="modal fade modal-report" id="modal-report" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!-- modal-header -->
            <div class="modal-header">
                <h5 class="modal-title">Người bán này có vấn đề gì?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ion-close-round"></i>
                </button>
            </div>
            <!-- modal-body -->
            <div class="modal-body">
                <ul class="list-report list-unstyled text-center">
                    <li><a data-toggle="modal" data-target="#modal-report-step2" data-reason="thumb-error" href="#">Hình đại diện sai phạm</a>
                    </li>
                    <li><a data-toggle="modal" data-target="#modal-report-step2" data-reason="info-error" href="#">Thông tin cá nhân sai
                            phạm</a></li>
                    <li><a data-toggle="modal" data-target="#modal-report-step2" data-reason="scam" href="#">Người bán có dấu hiệu lừa
                            đảo</a></li>
                    <li><a data-toggle="modal" data-target="#modal-report-step2" data-reason="other" href="#">Lý do khác</a></li>
                </ul>
            </div>
            <!-- end modal body -->
        </div>
    </div>
</div>
{{--end  modal-repost--}}
<div class="modal fade modal-report" id="modal-report-step2" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!-- modal-header -->
            <div class="modal-header">
                <h5 class="modal-title">Vui lòng cho biết thêm thông tin</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ion-close-round"></i>
                </button>
            </div>
            <!-- modal-body -->
            <div class="modal-body">
                <form id="form-report-spam">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Mô tả dấu hiệu sai phạm</label>
                        <textarea name="message" class="form-control" id="exampleTextarea" rows="9"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Thông tin của bạn để chúng tôi liên lạc khi cần
                            thiết</label>
                        <input name="phone" value="{{$member->phone}}" type="text" class="form-control" placeholder="Số điện thoại">
                        <input name="email" value="{{$member->email}}" type="email" class="form-control" placeholder="Email">
                    </div>
                    <input type="hidden" id="report-reason" name="reason">
                    <input type="hidden" name="id" value="{{$member->id}}">
                    <div class="btn-submit">
                        <button id="send-report" type="button" class="btn btn-primary text-uppercase">Gửi</button>
                    </div>
                </form>
                <!-- end form report span -->
            </div>
            <!-- end modal body -->
        </div>
    </div>
</div>
@push('script-custom')
    <script>
        $('.list-report a').click(function (event) {
            event.preventDefault();
            var type = $(this).data('reason');
            $('#report-reason').val(type);
        })
    </script>
@endpush