<!-- send message  -->
<div  class="modal fade modal-style-sm" id="modal-send-message-suscessful" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!-- modal-header -->
            <div class="modal-header">
                <h5 class="modal-title">Gửi tin nhắn đến <span>{{'@'.$member->name}}}</span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ion-close-round"></i>
                </button>
            </div>
            <!-- modal-body -->
            <div class="modal-body">
                <div class="d-flex align-items-end justify-content-center">
                    <span>Bạn đã gửi tin nhắn thành công </span>
                    <img src="{{asset('assets/img/icon/success.png')}}" alt=""></div>
            </div>
            <!-- end modal body -->
        </div>
    </div>
</div>

<div  class="modal fade modal-style-sm" id="modal-send-report-suscessful" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!-- modal-header -->
            <div class="modal-header">
                <h5 class="modal-title"></span>Báo cáo thành công</span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ion-close-round"></i>
                </button>
            </div>
            <!-- modal-body -->
            <div class="modal-body">
                <div class="d-flex align-items-end justify-content-center">
                    <span>Cảm ơn bạn đã gửi báo cáo tới chợ vỉa he.Chúng tôi sẽ xem xét xử lý.</span>
                    <img src="{{asset('assets/img/icon/success.png')}}" alt=""></div>
            </div>
            <!-- end modal body -->
        </div>
    </div>
</div>