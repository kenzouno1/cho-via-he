<div class="accout-seller-header bg-white text-center mb-3">
    <div class="info p-3">
        <div class="avata">
            <img src="{{$member->getAvatar()}}" alt="{{$member->name}}">
        </div>
        <h5>{{$member->name}}</h5>
        <p>Ngày tham gia {{date_format($member->created_at,"d-m-Y")}}</p>
        <div class="social-seller d-sm-flex align-self-sm-center justify-content-sm-center">
            <span>Thông tin đã cung cấp</span>
            <ul class="list-unstyled d-flex justify-content-center">
                <li class="{{$member->isFacebookAccount() ? 'active' : ''}}"><i
                            class="ion-social-facebook"></i></li>
                <li class="active"><i
                            class="ion-android-call"></i></li>
                <li class="{{$member->hasAddress() ? 'active' : ''}}"><i
                            class="ion-android-mail"></i></li>
                <li class="{{$member->hasAddress() ? 'active' : ''}}"><i
                            class="ion-android-pin"></i></li>
            </ul>
        </div>
        <div class="action-more">
            <div class="dropdown">
                <a href="#" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    <i class="ion-ion-more"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right"
                     aria-labelledby="dropdownMenuButton">
                    <a data-toggle="modal" data-target="#modal-report" class="dropdown-item"
                       href="#"><i class="ion-flag"></i> Báo cáo vi phạm</a>
                </div>
            </div>
        </div>

    </div>
</div>