<script src="{!! asset('assets/vendor/parsley/parsley.min.js') !!}"></script>
<script>
	//validate form new post
    $("#form-report-spam").parsley({
        errorClass: 'has-danger',
        successClass: 'has-success',
        classHandler: function (ParsleyField) {
            return ParsleyField.$element.parents('.form-group');
        },
        errorsContainer: function (ParsleyField) {
            return ParsleyField.$element.parents('.form-group');
        },
        errorsWrapper: '<span class="form-control-feedback">',
        errorTemplate: '<div></div>'
    });
    //end validate form
    //custom modal
    $('#modal-report a[data-target="#modal-report-step2"]').click(function(event) {
        $('#modal-report').modal('hide');
    });
</script>
@include('pages.userprofile.assets.js-report')