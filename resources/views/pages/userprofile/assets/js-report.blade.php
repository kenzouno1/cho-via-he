<script>
    $('#send-report').click(function (event) {
        event.preventDefault();
        var formData = $(this).closest('form').serialize();
        $.ajax({
            url: "{{route('account-seller.report')}}",
            type: 'POST',
            data: formData,
        }).done(function (response) {
            $('.modal').modal('hide');
            $('#modal-send-report-suscessful').modal('show');
        });
    })
</script>