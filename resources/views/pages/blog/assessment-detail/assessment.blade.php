@extends('master')
@section('content-page')

<!-- popup search -->


<main class="car-detail-page wrapper-main" role="main">
	<!-- wrapper-banner -->
   	@include('template-part.banner-page-content')
    <!-- end wrapper-banner -->
    <section class="section-assessment-detail py-4 section-content bg-graylight">
    	<div class="container bg-graylight">
    		<a class="select-categories-mobile hidden-md-up" href="" title="">Lựa chọn loại xe</a>
    		<div class="row">
    			<div class="col-md-3">
					<div class="content-page-left">
						<div class="refine-header title-style1 py-2">
    						<a href="" title="">Back</a>
    					</div>
						<div class="list-categories-car module-refine">
							<p class="title mb-0 pl-0">
								Hãng xe
							</p>
							<ul class="list-unstyled pl-0">
								<li><a href="" title="">Toyota</a></li>
								<li><a href="" title="">Ford</a></li>
								<li><a href="" title="">Audi</a></li>
								<li><a href="" title="">Honda</a></li>
								<li><a href="" title="">Toyota</a></li>
								<li><a href="" title="">Toyota</a></li>
								<li><a href="" title="">Ford</a></li>
								<li><a href="" title="">Audi</a></li>
								<li><a href="" title="">Honda</a></li>
								<li><a href="" title="">Toyota</a></li>
								<li><a href="" title="">Toyota</a></li>
								<li><a href="" title="">Ford</a></li>
								<li><a href="" title="">Audi</a></li>
								<li><a href="" title="">Honda</a></li>
								<li><a href="" title="">Toyota</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-9">
					<div class="assessment-detail-content bg-white p-4 mb-4">
						<header>
							<span>Porsche giành vị trí số 1 với 446 kW 911 Turbo</span>
							<hr>
							<h1 class="mb-2">Driven: Isuzu pumps up D-Max Dành vị trí số 1  full name tiêu đề bài viết 2 dòng nhảy xuống </h1>
							<div class="meta">
								By: DANIEL DeGASPERI<br>
								15/6/2017
							</div>
						</header>
						<div class="gallery">
							<div class="item-image">
								<a href="{{asset('assets/img/upload/img-large.png')}}" title=""  data-fancybox="gallery">
									<img data-lazy="{{asset('assets/img/upload/img-large.png')}}" alt="">
								</a>
							</div>
							<!-- end item image -->
							<div class="item-image">
								<a href="{{asset('assets/img/upload/img-large.png')}}" title=""  data-fancybox="gallery">
									<img data-lazy="{{asset('assets/img/upload/img-thumb2.png')}}" alt="">
								</a>	
							</div>
							<!-- end item image -->
							<div class="item-image">
								<a href="{{asset('assets/img/upload/img-large.png')}}" title=""  data-fancybox="gallery">
									<img data-lazy="{{asset('assets/img/upload/img-thumb3.png')}}" alt="">
								</a>
							</div>
							<!-- end item image -->
							<div class="item-image">
								<a href="{{asset('assets/img/upload/img-large.png')}}" title=""  data-fancybox="gallery">
									<img data-lazy="{{asset('assets/img/upload/img-thumb4.png')}}" alt="">
								</a>
							</div>
							<!-- end item image -->
							<div class="item-image">
								<a href="{{asset('assets/img/upload/img-large.png')}}" title=""  data-fancybox="gallery">
									<img data-lazy="{{asset('assets/img/upload/img-thumb5.png')}}" alt="">
								</a>
							</div>
							<!-- end item image -->
							<div class="item-image">
								<a href="{{asset('assets/img/upload/img-large.png')}}" title=""  data-fancybox="gallery">
									<img data-lazy="{{asset('assets/img/upload/img-thumb5.png')}}" alt="">
								</a>
							</div>
							<!-- end item image -->
							<div class="item-image">
								<a href="{{asset('assets/img/upload/img-large.png')}}" title=""  data-fancybox="gallery">
									<img data-lazy="{{asset('assets/img/upload/img-thumb4.png')}}" alt="">
								</a>
							</div>
							<!-- end item image -->
							<div class="item-image">
								<a href="{{asset('assets/img/upload/img-large.png')}}" title=""  data-fancybox="gallery">
									<img data-lazy="{{asset('assets/img/upload/img-thumb5.png')}}" alt="">
								</a>
							</div>
							<!-- end item image -->
							<div class="item-image">
								<a href="{{asset('assets/img/upload/img-large.png')}}" title=""  data-fancybox="gallery">
									<img data-lazy="{{asset('assets/img/upload/img-thumb5.png')}}" alt="">
								</a>
							</div>
							<!-- end item image -->
						</div>
						<!-- end gallery -->
						<div class="thumnail-gallery hidden-xs-down">
							<div class="item-image">
								<img data-lazy="{{asset('assets/img/upload/img-thumb1.png')}}" alt="">
							</div>
							<!-- end item image -->
							<div class="item-image">
								<img data-lazy="{{asset('assets/img/upload/img-thumb2.png')}}" alt="">
							</div>
							<!-- end item image -->
							<div class="item-image">
								<img data-lazy="{{asset('assets/img/upload/img-thumb3.png')}}" alt="">
							</div>
							<!-- end item image -->
							<div class="item-image">
								<img data-lazy="{{asset('assets/img/upload/img-thumb4.png')}}" alt="">
							</div>
							<!-- end item image -->
							<div class="item-image">
								<img data-lazy="{{asset('assets/img/upload/img-thumb5.png')}}" alt="">
							</div>
							<!-- end item image -->
							<div class="item-image">
								<img data-lazy="{{asset('assets/img/upload/img-thumb5.png')}}" alt="">
							</div>
							<!-- end item image -->
							<div class="item-image">
								<img data-lazy="{{asset('assets/img/upload/img-thumb4.png')}}" alt="">
							</div>
							<!-- end item image -->
							<div class="item-image">
								<img data-lazy="{{asset('assets/img/upload/img-thumb5.png')}}" alt="">
							</div>
							<!-- end item image -->
							<div class="item-image">
								<img data-lazy="{{asset('assets/img/upload/img-thumb5.png')}}" alt="">
							</div>
							<!-- end item image -->
						</div>
						<!-- end gallery -->
						<div class="descrition-post-detail">
							<p>
								Động thái mạnh tay giảm giá của Honda đã giúp doanh số xe CR-V có bước chuyển mình đáng ghi nhận khi góp mặt trở lại trong top 10 mẫu ô tô bán chạy nhất tháng 5. Tuy vẫn còn kém xa so với các đối thủ cùng phân khúc như Toyota Fortuner hay Mazda CX-5 nhưng việc doanh số bán xe có bước nhảy vọt là điều đáng ghi nhận.
							</p>
							<p>
								Mẫu xe thứ 2 nằm trong danh sách giảm giá tháng 5 là Mitsubishi Pajero Sport phiên bản động cơ xăng 4x2 AT, mức giảm tháng 6 lên đến 106 triệu đồng, hiện tại giá bán của các đại lý giảm từ 1,355 tỷ đồng xuống còn 1,249 tỷ đồng. Ngoài ra, xe còn được áp dụng thêm quà tặng đi kèm như: Phiếu nhiên liệu, bộ phụ kiện, bảo hiểm… Mitsubishi là “em út” trong cuộc đua giảm giá, khi đến tháng 6, hãng ô tô Nhật Bản này mới có động thái mới. Ngoài Pajero Sport, nhiều sản phẩm khác của Mitsubishi cũng giảm từ 20 - 80 triệu đồng.
							</p>
							<p>
								Đứng thứ 3 là mẫu Ford Everest Trend 2.2L với mức giảm lên đến 105 triệu đồng, giá thực tế tại các đại lý hiện chỉ còn 1,080 tỷ đồng. Ford cũng đồng loạt giảm nhiều sản phẩm ô tô khác của hãng, mức giảm dao động từ 25 – 100 triệu đồng, tùy loại. Mẫu ô tô Ford Ranger đang được nhiều người tiêu dùng kỳ vọng giảm mạnh trong tháng tới.
							</p>
							<p>
								Đứng thứ 5 là thương hiệu xe hơi Pháp – Peugeot, mức giảm giá công bố của mẫu CUV Peugeot 3008 đến 75 triệu đồng cùng ưu đãi quà tặng đi kèm. Đây là lần giảm giá hiếm hoi của Peugeot tại Việt Nam.
							</p>
							<p>
								Chevrolet Cruze, Chevrolet Colorado và Toyota Vios đồng hạng 6 với mức giảm giá cao nhất là 70 triệu đồng. Ngoài ra, khi mua Toyota Vios trong tháng 6, khách hàng còn nhận được ưu đãi gói bão dưỡng 3 năm chính hãng.
							</p>
							<p>
								Một số mẫu ô tô khác như: Mazda 6 2.5 FL Premium 2016, Kia Optima cũng nhận được mức giảm giá lần lượt là 50 và 45 triệu đồng. Bên cạnh đó, giá xe Hyundai cũng giảm nhẹ từ 7,5 – 30 triệu đồng, mẫu Hyundai i10 giảm nhẹ nhất mới số tiền là 7,5 triệu đồng. Đứng thứ 5 là thương hiệu xe hơi Pháp – Peugeot, mức giảm giá công bố của mẫu CUV Peugeot 3008 đến 75 triệu đồng cùng ưu đãi quà tặng đi kèm. Đây là lần giảm giá hiếm hoi của Peugeot tại Việt Nam.
							</p>
							<p>
								Chevrolet Cruze, Chevrolet Colorado và Toyota Vios đồng hạng 6 với mức giảm giá cao nhất là 70 triệu đồng. Ngoài ra, khi mua Toyota Vios trong tháng 6, khách hàng còn nhận được ưu đãi gói bão dưỡng 3 năm chính hãng.
							</p>
							<p>
								Một số mẫu ô tô khác như: Mazda 6 2.5 FL Premium 2016, Kia Optima cũng nhận được mức giảm giá lần lượt là 50 và 45 triệu đồng. Bên cạnh đó, giá xe Hyundai cũng giảm nhẹ từ 7,5 – 30 triệu đồng, mẫu Hyundai i10 giảm nhẹ nhất mới số tiền là 7,5 triệu đồng.Đứng thứ 5 là thương hiệu xe hơi Pháp – Peugeot, mức giảm giá công bố của mẫu CUV Peugeot 3008 đến 75 triệu đồng cùng ưu đãi quà tặng đi kèm. Đây là lần giảm giá hiếm hoi của Peugeot tại Việt Nam.
								Chevrolet Cruze, Chevrolet Colorado và Toyota Vios đồng hạng 6 với mức giảm giá cao nhất là 70 triệu đồng. Ngoài ra, khi mua Toyota Vios trong tháng 6, khách hàng còn nhận được ưu đãi gói bão dưỡng 3 năm chính hãng.
								Một số mẫu ô tô khác như: Mazda 6 2.5 FL Premium 2016, Kia Optima cũng nhận được mức giảm giá lần lượt là 50 và 45 triệu đồng. Bên cạnh đó, giá xe Hyundai cũng giảm nhẹ từ 7,5 – 30 triệu đồng, mẫu Hyundai i10 giảm nhẹ nhất mới số tiền là 7,5 triệu đồng.
							</p>
						</div>
						<!-- end  -->
					</div>
					<!-- end post detail -->

					<div class="assessment-note bg-white p-3 mb-3">
						<p>
							<span style="font-weight:500"> Lưu ý: </span>- Các bài viết được tổng hợp từ cái thành viên cũng như sưu tầm của một số trang thông tin đáng tin cậy khác. Chúng tôi cập nhât các bài viết đành giá về sản phẩm giúp các bạn có thể hiểu rõ hơn về 1 sản phẩm mà mình đang muốn mua hoặc nắm thông tin để hiểu biế của bạn được bổ sung. Bạn có thể  ...
						</p>
					</div>
					<!-- end assessment-note -->
				</div>
    		</div>
    	</div>
    </section>
    <!-- end section contnet search -->
</main>
@endsection
 @push('css-custom')
 	@include('pages.template-car.assessment-detail.css')
@endpush
@push('script-custom')
	@include('pages.template-car.assessment-detail.js')
@endpush
