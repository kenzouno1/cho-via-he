<script src="{{asset('assets/vendor/slick/slick.min.js')}}"></script>
<script src="{{asset('assets/vendor/sticky-kit/jquery.sticky-kit.min.js')}}" type="text/javascript" ></script>
<script src="{{asset('assets/vendor/fancybox/jquery.fancybox.min.js')}}"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
	// slider slick
	 $('.gallery').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  fade: true, lazyLoad: 'ondemand',
	  asNavFor: '.thumnail-gallery',
	  prevArrow:'<a class="slick-prev" href="#" title="prev"><i class="ion-angle-double-left"></i></a>',
	  nextArrow:'<a class="slick-next" href="#" title="prev"><i class="ion-angle-double-right"></i></a>'
	});
	$('.thumnail-gallery').slick({
	  slidesToShow: 5,
	  slidesToScroll: 1,
	  asNavFor: '.gallery',
        lazyLoad: 'ondemand',
	  dots: false,
	  centerMode: false,
	  focusOnSelect: true,
	  prevArrow:'<a class="slick-prev" href="#" title="prev"><i class="ion-ios-arrow-left"></i></a>',
	  nextArrow:'<a class="slick-next" href="#" title="prev"><i class="ion-ios-arrow-right"></i></a>'
	});
	// stick sidebar left

	$(".content-page-left").stick_in_parent();
	// select-categories-mobile
	$(".select-categories-mobile").click(function(event) {
		event.preventDefault();
		$('.content-page-left').slideToggle('fast');
	});
	//fancyboxs
    $("[data-fancybox]").fancybox({
        thumbs: {
            autoStart: true
        },
        baseClass : 'fancy-thumbs-bottom',
    });
});

</script>