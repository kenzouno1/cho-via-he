<script src="{{asset('assets/vendor/sticky-kit/jquery.sticky-kit.min.js')}}" type="text/javascript" ></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		// stick sidebar left
		$(".content-page-left").stick_in_parent();
		// select-categories-mobile
		$(".select-categories-mobile").click(function(event) {
			event.preventDefault();
			$('.content-page-left').slideToggle('fast');
		});
	});
</script>
