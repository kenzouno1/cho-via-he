@extends('master')
@section('content-page')

<!-- popup search -->


<main class="car-detail-page wrapper-main" role="main">
	<!-- wrapper-banner -->
    @include('template-part.banner-page-content')
    <!-- end wrapper-banner -->
    <section class="section-assessment-list py-4 section-content bg-graylight">
    	<div class="container bg-graylight">
    		<a class="select-categories-mobile hidden-md-up" href="" title="">Lựa chọn loại xe</a>
    		<div class="row">
    			<div class="col-md-3">
					<div class="content-page-left">
						<div class="refine-header title-style1 py-2">
    						<a href="" title="">Back</a>
    					</div>
						<div class="list-categories-car module-refine">
							<p class="title mb-0 pl-0">
								Hãng xe
							</p>
							<ul class="list-unstyled pl-0">
								<li><a href="" title="">Toyota</a></li>
								<li><a href="" title="">Ford</a></li>
								<li><a href="" title="">Audi</a></li>
								<li><a href="" title="">Honda</a></li>
								<li><a href="" title="">Toyota</a></li>
								<li><a href="" title="">Toyota</a></li>
								<li><a href="" title="">Ford</a></li>
								<li><a href="" title="">Audi</a></li>
								<li><a href="" title="">Honda</a></li>
								<li><a href="" title="">Toyota</a></li>
								<li><a href="" title="">Toyota</a></li>
								<li><a href="" title="">Ford</a></li>
								<li><a href="" title="">Audi</a></li>
								<li><a href="" title="">Honda</a></li>
								<li><a href="" title="">Toyota</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-9">
					<div class="content-page-right mb-5">
						<!-- end select-categories-mobile -->
						<h5 class="mb-4">Tin tức về các loại xe</h5>
						<ul class="list-assessment list-unstyled">
							@foreach(range(1,8) as $item)
								<li class="item-assessment ">
									<a class="d-flex align-items-center" href="" title="">
										<img class="d-flex mr-3" src="{{asset('assets/img/upload/assessment-list.png')}}" alt=" placeholder image">
										<div class="media-body">
										  <h5 class="mt-0 mb-0 mb-sm-2">Driven: Isuzu pumps up D-Max with torque and tech</h5>
										  Thêm mô-men xoắn, hộp số 6 cấp, nâng cấp NVH, thêm công nghệ trong MY17 suzu D-Max
										</div>
									</a>
								</li>
								<!-- end media -->
							@endforeach
						</ul>
					</div>
					<!-- end content page right -->
					@include('layouts.pagination')
					<!-- end pagination -->
				</div>
    		</div>
    	</div>
    </section>
    <!-- end section contnet search -->
</main>
@endsection

@push('script-custom')
	@include('pages.template-car.assessment.js')
@endpush
