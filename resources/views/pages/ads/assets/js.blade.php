<script src="{!! asset('assets/vendor/jquery-ui/jquery-ui.min.js') !!}"></script>
<script src="{!! asset('assets/vendor/matchHeight/jquery.matchHeight.js') !!}"></script>
<script src="{!! asset('assets/vendor/file-upload/jquery.iframe-transport.js') !!}"></script>
<script src="{!! asset('assets/vendor/file-upload/jquery.fileupload.js') !!}"></script>
<script src="{!! asset('assets/vendor/read-money/read-money.js') !!}"></script>
<script src="{!! asset('assets/vendor/select2/select2.min.js') !!}"></script>
<script src="{!! asset('assets/vendor/parsley/parsley.min.js') !!}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_API','AIzaSyCcCGIu_Jz3R1wUtpLCtxjvNDNlueSCuTE')}}&libraries=places&callback=initMap"
        async defer></script>
<script src="{{asset('assets/vendor/slick/slick.min.js')}}"></script>
<script type="text/javascript">
    //close on click body
    $(document).click(function (event) {
        if (!$(event.target).is('.custom-select-option, .custom-select-option *')) {
            $(this).find(".option-select").hide();
        }
    });
//    slider modal package
    $('.modal-package-preview-slider').slick({
        prevArrow: '<a href="#" title="Slick Preview" class="slick-prev"> <i class="ion-chevron-left"></i> </a> ',
        nextArrow: '<a href="#" title="Slick Next" class="slick-next"> <i class="ion-chevron-right"></i> </a> '
    });
    $('.modal-package-preview-button').on('click', '.btn-select', function(event){
        event.preventDefault();
        dataPackage = $(this).attr("data-value");
        $('.feature-package-button .btn-select[data-value="' + dataPackage + '"]').click();
    })

    $('.feature-package-button a[data-slide]').click(function(e) {
        e.preventDefault();
        var slideno = $(this).data('slide');
        $('.modal-package-preview-slider').slick('slickGoTo', slideno - 1);
    });

    //validate form new post
    $("#form-new-post").parsley({
        errorClass: 'has-danger',
        successClass: 'has-success',
        classHandler: function (ParsleyField) {
            return ParsleyField.$element.parents('.form-group');
        },
        errorsContainer: function (ParsleyField) {
            return ParsleyField.$element.parents('.form-group');
        },
        errorsWrapper: '<span class="form-control-feedback">',
        errorTemplate: '<div></div>'
    });
    //end validate form

//    $('.add-checkbox').click(function (event) {
//        $(this).closest('.add').find('input[type="checkbox"]').attr('checked', 'checked');
//    });
    //group info option
    $('.button-info-option').click(function (event) {
        event.preventDefault();
        $(this).remove();
        $('.form-group-info-option').slideDown('fast');
    });

    //move image
    $(".upload-img-post ul").sortable({
        items: "li:not(.upload-image-disabled)",
        stop: function (event, ui) {
            reloadListImage();
        }
    });
    $(".upload-image ul li").disableSelection();
    //custom select boxes
    $(".custom-select2").select2();
    // max height package content
    // $('.feature-packages .feature-package-content').matchHeight();
    // custom button select
    $('.feature-package-button .btn-select').click(function (event) {
        var n_image = $(this).data('number-image')
        var n_image_current = $('.upload-img-post ul li.upload-image-disabled').not($('.upload-begin')).length;
        event.preventDefault();
        $('.price-table-note').slideDown('fast');
        $('.form-post-add').slideDown('fast');
        $('.feature-package-button .btn-select').removeClass('active');
        $('.feature-item').show();
        $(this).closest('.feature-package-content').find('.feature-package-features li.feature').each(function (index) {
            var slugFeature = $(this).data('slug');
            $('.feature-' + slugFeature + '').hide().find('input[type="checkbox"]').val("");
        });
        $(this).toggleClass('active');
        $('.upload-img-post ul li').not($('.upload-begin')).remove();
        if ($('.upload-img-post ul .upload-begin').length <= 0){
            $('.upload-img-post ul').append('<li class="upload-begin upload-image-disabled"><i class="ion-camera"></i></li>');
        }
        var i;
        for (i = 1; i < n_image; ++i) {
            $('.upload-img-post ul').append('<li class="upload-image-disabled"><i class="ion-camera"></i></li>');
        }
    });
    //check minium amount
    $('.check-minimum-amount input[type="checkbox"]').click(function () {
        if ($(this).is(":checked")) {
            $('.set-minimum-amount').slideDown('fast');
        }
        else if ($(this).is(":not(:checked)")) {
            $('.set-minimum-amount').slideUp('fast');
        }
    });
    //popup scroll price table mobile
    $('.price-table-mobi .feature-package-info').on('click', function (event) {
        event.preventDefault();
        var target = $(this).attr('href');
        var offset=50;
        $('body').addClass('stop-scrolling');
        $('.bg-overlay').fadeIn();
        $('.price-table-desktop').addClass('show');
        $('.price-table-desktop').mCustomScrollbar('scrollTo','.col-3 '+target, {scrollInertia: 500});
//        $('.price-table-desktop').mCustomScrollbar("scrollTo",  target.offset().left - 20, {scrollInertia: 500});

    });
    $('.feature-packages .btn-secondary').on('click', function (event) {
        if ($(window).width() < 577) {
            event.preventDefault();
            $('body').removeClass('stop-scrolling');
            $('.bg-overlay').fadeOut();
            $('.price-table-desktop').removeClass('show');
        }
    });

    $('.feature-packages .btn-secondary.btn-select').on('click', function (event) {
        event.preventDefault();
        var activefeature = $(this).attr("data-target");
        $('.price-table-mobi .feature-packages').removeClass('active');
        $(activefeature).addClass('active');
    });

    //    select packages on mobile
    $('.price-table-mobi .feature-package-description, .price-table-mobi .feature-packages-header').on('click', function (event) {
        var activefeature = $(this).parent().attr("data-target");
        $(activefeature).find('.btn-select').click();
    });
    //    end package on mobile

    //scroll popup feature package
    @mobile
    // $(window).on("load resize", function () {
    //     if ($(window).width() > 980) {
    $(".price-table-desktop").mCustomScrollbar({
        axis: "x",
        theme: "light-3",
        advanced: {autoExpandHorizontalScroll: true}
    });
    //     }
    // });
    @endmobile
    $(document).on('input', 'input[name="price"],input[name="price-min"]', function (event) {
        var wrapper = $(this).closest('.wapper-price');
        var that = $(this);
        setTimeout(function () {
            var price = that.val();
            var docgia = docso(price);
            wrapper.find('.docgia').text(docgia);
        }, 1000);
    })

    $('.feature-package-button .btn-select').click(function (event) {
        event.preventDefault();
        var id = $(this).data('value');
        $('#package-value').val(id);
    })
    $('.select-price-type .custom-radio').click(function (event) {
        var value = $(this).find('input[name="price-type"]').val();
        if (value == 'free' || value == 'trade') {
            $('input[name="price"]').val(0).attr({
                'disabled': 'disabled',
                'data-parsley-required': false
            }).css('curson', 'not-allowed');
            $('.select-min-amount').hide();
            $('.wapper-price').removeClass('has-danger').find('.form-control-feedback').remove();
        } else {
            $('input[name="price"]').val(0).attr({
                'disabled': false,
                'data-parsley-required': true
            });
            $('.select-min-amount').show();
        }
    });
    //check muc luong
    $('#check-wage').on('change',function(event) {
        var checked = $(this).prop('checked');
        $('select.mucluong').prop('disabled',checked);
        if (checked) {
            $('.select-wage').attr({
                'data-parsley-required': false
            });
        } else {
            $('.select-wage').attr({
                'data-parsley-required': true
            });
        }
    });
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        return !(charCode > 31 && (charCode < 48 || charCode > 57));
    }

</script>
@include('pages.ads.assets.upload')
@include('pages.ads.assets.map')