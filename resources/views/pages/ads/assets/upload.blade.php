<?php
/**
 * Created by PhpStorm.
 * User: Dat Nguyen
 * Date: 7/8/2017
 * Time: 10:00 AM
 */
?>

<script type="text/javascript">
    //upload file
    $('.upload-img-post ').on('click', '.btn-upload, .upload-begin', function (event) {
        event.preventDefault();
        $('#file-upload').click();
    });
    var uploadDing = 0;
    var ul = $('.upload-img-post > ul');
    var list_image = [];
    $('#form-new-post').fileupload({
        url: '{{route('upload.upload')}}',
        autoUpload: true,
        sequentialUploads: true,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        maxFileSize: 999000,
        imageMaxWidth: 200,

        add: function (e, data) {
            var
                goUpload = true;
            var
                uploadFile = data.files[0];
            if (uploadDing >= 1) {
                $('.upload-img-post').removeClass('upload-placeholder');
            }
            if (!(/(\.|\/)(gif|jpe?g|png)$/i).test(uploadFile.name)) {
                goUpload = false;
                alert('Không hỗ trợ upload file này');
            }
            if ($('.upload-image-disabled').length <= 0) {
                goUpload = false;
                console.log('chỉ được phép upload 10 file');
            }
            if (goUpload) {
                uploadDing++;

                // data.context = tpl.appendTo(ul);
                // draw preview image
                var
                    reader = new FileReader();
                reader.onloadend = function (e) {
//                    console.log(data);
                    var
                        tempImg = new Image();
                    tempImg.src = reader.result;
                    tempImg.onload = function () {
                        var
                            max_size = 109;
                        var
                            width = this.width;
                        var
                            height = this.height;
                        if (width < 150 || height < 150) {
                            alert('vui lòng nhập ảnh lớn hơn 150x150');
                            return;
                        } else {
                            var tpl = $('<a class="remove-img" href="" title=""><i class="ion-close-round"></i></a><img /><div class="progress"><div class="progress-bar"></div></div>');
                            var currentLi = ul.find('.upload-begin').first();
                            currentLi.html(tpl).removeClass('upload-begin upload-image-disabled').addClass('upload-processing');
                            currentLi.next('li').addClass('upload-begin');
                            data.context = currentLi;
                            if (width > height) {
                                if (width > max_size) {
                                    height *= max_size / width;
                                    width = max_size;
                                }
                            } else {
                                if (height > max_size) {
                                    width *= max_size / height;
                                    height = max_size;
                                }
                            }
                            var
                                canvas = document.createElement('canvas');
                            canvas.width = width;
                            canvas.height = height;
                            var
                                ctx = canvas.getContext("2d");
                            ctx.drawImage(this, 0, 0, width, height);
                            var
                                dataURL = canvas.toDataURL("image/jpeg");
                            data.context.find('img').attr('src', dataURL);
                            var abc = data.submit();
                        }
                    };
                };
                reader.readAsDataURL(uploadFile);

            }
        },
        progress: function (e, data) {
            // Calculate the completion percentage of the upload
            var
                progress = parseInt(data.loaded / data.total * 100, 10);
            data.context.find('.progress-bar').css('width', progress + '%');
            if (progress == 100) {
                data.context.removeClass('upload-processing');
            }
        },
        done: function (e, data) {
            data.context.addClass('success');
            data.context.attr('data-src', data.result);
            reloadListImage();
        },
        fail: function (e, data) {
            data.context.addClass('error');
        },
    });
    $('.upload-img-post ').on('click', '.remove-img', function (event) {
        event.preventDefault();
        $(this).parent('li').fadeOut('slow', function () {
            $(this).remove();
            $('<li class="upload-image-disabled"><i class="ion-camera"></i></li>').appendTo(ul);
            reloadListImage();
        });
    });

    function reloadListImage() {
        var list_image = [];
//        $('.upload-img-post li.success').each(function( index ) {
//            list_image.push($('li.success').eq(index).data('src'));
//        });
        var lenght = $('.upload-img-post li.success').length;
        for (var i = 0; i < lenght; i++) {
            list_image.push($('li.success').eq(i).data('src'));
        }
        var myJsonString = JSON.stringify(list_image);
        $('input[name="list_image"]').val(myJsonString);
    }
</script>