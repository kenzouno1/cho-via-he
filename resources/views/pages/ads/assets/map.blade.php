<?php
/**
 * Created by PhpStorm.
 * User: Dat Nguyen
 * Date: 7/8/2017
 * Time: 10:04 AM
 */
?>
<script>
    $('#location').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });
    /*
     Map Location
     */
    var autocomplete;
    function initMap() {
        var input = document.getElementById('location');
        var loc = [];
        gmaps = new google.maps.places.Autocomplete(
            input,
            {
                types: ['geocode'],
                componentRestrictions: {country: "vn"}
            }
        );
        google.maps.event.addListener(gmaps, 'place_changed', function () {
            $('button[type="submit"]').prop( "disabled", true );
            var place = gmaps.getPlace();
            var lat = place.geometry.location.lat();
            var lng = place.geometry.location.lng();
            $('input[name="lat"]').val(lat);
            $('input[name="lng"]').val(lng);
            $('button[type="submit"]').prop( "disabled", false );
        });
    }

    $('#form-new-post button[type="submit"]').click(function (event) {
        event.preventDefault();
        var form = $(this).closest('form');
        var input = document.getElementById('location');
        // Get geocoder instance
        var geocoder = new google.maps.Geocoder();

        // Geocode the address
        geocoder.geocode({'address': input.value}, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK && results.length > 0) {
                $('input[name="lat"]').val(results[0].geometry.location.lat());
                $('input[name="lng"]').val(results[0].geometry.location.lng());
                ajaxCheckPrice(form);
            } else {
                var top = $(input).offset().top - $(window).height() / 2;
                $('html,body').stop().animate({scrollTop: top}, 500, function () {
                    $('#location').focus();
                });
            }
            ;
        });
    });
    function ajaxCheckPrice(form) {
        $.ajax({
            type: 'POST',
            url: '{{ route('ads.ajaxCheckPrice')}}',
            data: form.serialize(),
        }).done(function (response) {
            var data = $.parseJSON(response);
            if (data.checkPrice == 'false') {
                $('.current-price a').text('Số Dư tài khoản : ' + data.currentPrice + ' VNĐ');
                $('.order-price a').text('Tổng số tiền cần để đăng bài : ' + data.orderPrice + ' VNĐ');
                $('#modal-check-price').modal('show');
            } else {
                form.submit();
            }
        });
    }
    $('.current-location').click(function (event) {
        event.preventDefault();
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var geocoder = new google.maps.Geocoder;
                geocoder.geocode({'location': pos}, function (results, status) {
                    if (status === 'OK') {
                        if (results[0]) {
                            get_adress(results);
                            $('#location').val(results[0].formatted_address);
                        } else {
                            window.alert('No results found');
                        }
                    } else {
                        window.alert('Geocoder failed due to: ' + status);
                    }
                });
            });
        } else {
            alert("Geolocation is not supported by this browser.");
        }
    });

    function get_adress(results) {
        $('button[type="submit"]').prop( "disabled", true );
        var place_id = [];
        var lat = results[0].geometry.location.lat();
        var lng = results[0].geometry.location.lng();
        $('input[name="lat"]').val(lat);
        $('input[name="lng"]').val(lng);
        $('button[type="submit"]').prop( "disabled", false );
    }

</script>
