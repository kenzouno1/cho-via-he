<div class="modal fade modal-package-preview" id="modal-package-preview" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!-- modal-body -->
            <div class="modal-body">
                <div class="modal-package-preview-slider">
                    <div id="feature-packages-free" class="item">
                        <div class="modal-package-preview-content">
                            <img src="{{asset('assets/img/default-image/mienphi.jpg')}}" alt="">
                        </div>
                        <div class="modal-package-preview-button text-uppercase p-3 d-flex justify-content-around">
                            <a class="btn btn-secondary font-weight-bold px-5" href="" title="Không cảm ơn" data-dismiss="modal">Không, Cảm ơn</a>
                            <a data-value="1" class="btn-select btn btn-primary font-weight-bold px-5" href="#" title="Không cảm ơn" data-dismiss="modal">Chọn gói
                                Miễn phí</a>
                        </div>
                    </div>
                    {{--end item --}}
                    <div id="feature-packages-featured" class="item">
                        <div class="modal-package-preview-content">
                            <img src="{{asset('assets/img/default-image/tieubieu.jpg')}}" alt="">
                        </div>
                        <div class="modal-package-preview-button text-uppercase p-3 d-flex justify-content-around">
                            <a class="btn btn-secondary font-weight-bold px-5" href="" title="Không cảm ơn"  data-dismiss="modal">Không, Cảm ơn</a>
                            <a data-value="2"  class=" btn-select btn btn-primary font-weight-bold px-5" href="" title="Không cảm ơn" data-dismiss="modal">Chọn gói
                                Tiêu biểu</a>
                        </div>
                    </div>

                    {{--end item --}}
                    <div id="feature-packages-plus" class="item">
                        <div class="modal-package-preview-content">
                            <img src="{{asset('assets/img/default-image/noibat.jpg')}}" alt="">
                        </div>
                        <div class="modal-package-preview-button text-uppercase p-3 d-flex justify-content-around">
                            <a class="btn btn-secondary font-weight-bold px-5" href="" title="Không cảm ơn" data-dismiss="modal">Không, Cảm ơn</a>
                            <a data-value="3" class="btn-select btn btn-primary font-weight-bold px-5" href="" title="Không cảm ơn" data-dismiss="modal">Chọn gói
                                Nổi bật</a>
                        </div>
                    </div>
                    {{--end item --}}

                    <div id="feature-packages-premium" class="item">
                        <div class="modal-package-preview-content">
                            <img src="{{asset('assets/img/default-image/uutien.jpg')}}" alt="">
                        </div>
                        <div class="modal-package-preview-button text-uppercase p-3 d-flex justify-content-around">
                            <a class="btn btn-secondary font-weight-bold px-5" href="" title="Không cảm ơn" data-dismiss="modal">Không, Cảm ơn</a>
                            <a data-value="4" class="btn-select btn btn-primary font-weight-bold px-5" href="" title="Không cảm ơn" data-dismiss="modal">Chọn gói
                                Ưu tiên</a>
                        </div>
                    </div>
                    {{--end item --}}
                </div>
            </div>
            <!-- end modal body -->
        </div>
    </div>
</div>