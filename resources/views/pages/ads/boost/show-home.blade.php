@if($lstShowHome->isNotEmpty())
    @php
        $firstShowHome = $lstShowHome->first();
    @endphp
    <div class="col-xl-7 col-lg-8 mb-2 feature-item feature-show-home">
        <div class="compose-extra compose-extra-large clearfix mb-sm-3 d-flex align-items-stretch">
            <div class="img float-left">
                <img src="{{asset(config('package.boost.show-home.image'))}}" alt="">
            </div>
            <div class="description float-left">
                <header>
                    <h6>{{config('package.boost.show-home.label')}}</h6>
                    <div class="custom-select-option custom-select-style">
                        <div class="btn-select">
                            <a href="" title="">@money($firstShowHome->price)/{{$firstShowHome->days}}ngày</a> <i
                                    class="ion-arrow-down-b hidden-xs-down"></i>
                        </div>
                        <ul class="option-select">
                            @foreach($lstShowHome as $showHome)
                                <li data-value="{{$showHome->id}}">@money($showHome->price)/{{$showHome->days}}ngày
                                </li>
                            @endforeach
                        </ul>
                        <input type="hidden" name="show_home" value="{{$firstShowHome->id}}">
                    </div>
                </header><!-- /header -->
                <p class="hidden-sm-down">
                    {{config('package.boost.show-home.description')}}
                </p>
            </div>
            <div class="add float-right">
                <button type="button" class="btn text-center font-weight-bold text-uppercase add-checkbox "><i
                            class="ion-round"></i></button>
                <input type="checkbox" id="check-show-home" name="check_show_home" style="display: none;">
            </div>
        </div>
        <!-- end compose-extra -->
    </div>
@endif