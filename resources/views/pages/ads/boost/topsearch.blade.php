@if($lstTopSearch->isNotEmpty())
    @php
        $firstTopSearch = $lstTopSearch->first();
    @endphp
    <div class="col-xl-7 col-lg-8 mb-2 feature-item feature-top-search">
        <div class="compose-extra compose-extra-large clearfix mb-sm-3 d-flex align-items-stretch">
            <div class="img float-left">
                <img src="{{asset(config('package.boost.top-search.image'))}}" alt="">
            </div>
            <div class="description float-left">
                <header>
                    <h6>{{config('package.boost.top-search.label')}}</h6>
                    <div class="custom-select-option custom-select-style">
                        <div class="btn-select">
                            <a href="" title="">@money($firstTopSearch->price)/{{$firstTopSearch->days}} ngày</a> <i
                                    class="ion-arrow-down-b hidden-xs-down"></i>
                        </div>
                        <ul class="option-select">
                            @foreach($lstTopSearch as $topSearch)
                                <li data-value="{{$topSearch->id}}">@money($topSearch->price)/{{$topSearch->days}}
                                    ngày
                                </li>
                            @endforeach
                        </ul>
                        <input type="hidden" name="top-search" value="{{$firstTopSearch->id}}">
                    </div>
                </header><!-- /header -->
                <p class="hidden-sm-down">
                    {{config('package.boost.top-search.description')}}
                </p>
            </div>
            <div class="add float-right">
                <button type="button"
                        class="btn text-center font-weight-bold text-uppercase add-checkbox ">
                    <i
                            class="ion-round"></i></button>
                <input type="checkbox"
                       id="check-top-search" name="check_top_search" style="display: none;">
            </div>
        </div>
        <!-- end compose-extra -->
    </div>
@endif