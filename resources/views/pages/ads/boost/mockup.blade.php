@if($lstMockup->isNotEmpty())
    @php
        $firstMockup = $lstMockup->first();
    @endphp
    <div class="row">
        <div class="col-lg-7">
            <div class="compose-extra clearfix mb-3 d-flex align-items-stretch">
                <div class="img float-left">
                    <img src="{{asset(config('package.boost.has-mockup.image'))}}" alt="">
                </div>
                <div class="description float-left">
                    <header>
                        <h6>{{config('package.boost.has-mockup.label')}}</h6>
                        <div class="custom-select-option custom-select-style">
                            <div class="btn-select">
                                <a href="" title="">@money($firstMockup->price)/{{$firstMockup->days}} ngày</a> <i
                                        class="ion-arrow-down-b hidden-xs-down"></i>
                            </div>
                            <ul class="option-select">
                                @foreach($lstMockup as $mockup)
                                    <li data-value="{{$mockup->id}}">@money($mockup->price)/{{$mockup->days}} ngày</li>
                                @endforeach
                            </ul>
                            <input type="hidden" name="mockup" value="{{$firstMockup->id}}">
                        </div>
                    </header><!-- /header -->
                    <p class="hidden-sm-down">
                        {{config('package.boost.has-mockup.description')}}
                    </p>
                </div>
                <div class="add float-right">
                    <button  type="button" class="btn text-center font-weight-bold text-uppercase add-checkbox "><i
                                class="ion-round"></i></button>
                        <input  type="checkbox" id="check-mockup" name="check_mockup" style="display: none;">

                </div>
            </div>
            <!-- end compose-extra -->
        </div>
    </div>
@endif