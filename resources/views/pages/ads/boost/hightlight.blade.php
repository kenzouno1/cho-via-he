@if($lstHighLight->isNotEmpty())
@php
    $firstHighLight = $lstHighLight->first();
@endphp
<div class="col-xl-7 col-lg-8 mb-2 feature-item feature-highlight">
    <div class="compose-extra compose-extra-large clearfix mb-sm-3 d-flex align-items-stretch">
        <div class="img float-left">
            <img src="{{asset(config('package.boost.highlight.image'))}}" alt="">
        </div>
        <div class="description float-left">
            <header>
                <h6>{{config('package.boost.highlight.label')}}</h6>
                <div class="custom-select-option custom-select-style">
                    <div class="btn-select">
                        <a href="" title="">@money($firstHighLight->price)/{{$firstHighLight->days}} ngày</a> <i
                                class="ion-arrow-down-b hidden-xs-down"></i>
                    </div>
                    <ul class="option-select">
                        @foreach($lstHighLight as $highlight)
                            <li data-value="{{$highlight->id}}">@money($highlight->price)/{{$highlight->days}}
                                ngày
                            </li>
                        @endforeach
                    </ul>
                    <input type="hidden" name="highlight" value="{{$firstHighLight->id}}">
                </div>
            </header><!-- /header -->
            <p class="hidden-sm-down">
                {{config('package.boost.highlight.description')}}
            </p>
        </div>
        <div class="add float-right">
            <button type="button" class="btn text-center font-weight-bold text-uppercase add-checkbox "><i
                        class="ion-round"></i></button>
            <input type="checkbox" id="check-highlight" name="check_highlight" style="display: none;">
        </div>
    </div>
    <!-- end compose-extra -->
</div>
@endif