<div class="header-title">
    <h4 class="text-uppercase">Tôi muốn đăng tin</h4>
</div>
<div class="breadcrumbs ">
    <ul class="list-unstyled">
        <li>
            Lựa chọn các gói đăng
        </li>
    </ul>
</div>
<div class="price-table">
    <div class="price-table-mobi price-table hidden-sm-up">
        <!-- end feature-packages -->
        @foreach($packages as $package)
            <div class="{{$package->get_package_html_id()}} feature-packages d-flex align-items-stretch clearfix mb-md-4" data-value="{{$package->id}}"
                 data-target=".{{$package->get_package_html_id()}}">
                <div class="feature-packages-header">
                </div>
                <div class="feature-package-description">
                    <h4>{{$package->name}}</h4>
                    <h3 class="price font-weight-bold mb-0">
                        {{UtilHelper::money($package->price)}}
                    </h3>
                    <p>
                        {{$package->description}}
                    </p>
                </div>
                <a class="feature-package-info" href="#{{$package->get_package_html_id()}}"
                   title="Xem {{$package->name}}">
                    <i class="ion-information-circled"></i>
                </a>
            </div>
        @endforeach
    </div>
{{ render_validate_msg('package',$errors) }}

<!-- end price table mobi -->

    <div class="price-table-desktop ">
        <div class="list-price-table">
            <div class="row">
                @foreach($packages as $key =>  $package)
                    @php
                    $features =$package->get_features();
                    @endphp
                    <div class="packages-box col-lg-3 col-sm-6 col-3">
                        <div id="{{$package->get_package_html_id()}}"
                             class="{{$package->get_package_html_id()}} feature-packages text-center transition05 mb-sm-4">
                            <div class="feature-packages-header font-weight-bold">
                                <h4>{{$package->name}}</h4>
                                <p class="mb-0">
                                    {{$package->description}}
                                </p>
                                <div class="feature-packages-star">
                                    <i class="ion-ios-star"></i>
                                    <i class="ion-ios-star"></i>
                                </div>
                            </div>
                            <!-- end feature-packages -->
                            <div class="feature-package-content">
                                <div class="feature-package-description">
                                    <h3 class="price mb-0">
                                        {{UtilHelper::money($package->price)}}
                                    </h3>
                                    @if(($view_number = $package->get_option('view',true)) !='')
                                        <p class="view">
                                            {!! $view_number !!} lượt xem
                                        </p>
                                    @endif
                                </div>
                                <ul class="feature-package-features list-unstyled px-sm-3 py-sm-0 p-3">
                                    <li>Chỉnh sửa không giới hạn</li>
                                    <li>Tải tối đa 10 ảnh</li>
                                    @forelse ($features as $feature)
                                        <li class="feature"
                                            data-slug="{{$feature->value}}">{{$lstFeatures[$feature->value]['label']}}</li>
                                    @endforelse
                                </ul>
                                <div class="feature-package-button ">
                                    <a data-slide="{{$key+1}}" class="feature-package-preview hidden-sm-down" href="#" title="Xem mẫu" data-toggle="modal" data-target="#modal-package-preview">Xem mẫu</a>
                                    <button data-value="{{$package->id}}"
                                            data-number-image = "{{config("package.package.numberImage.$package->id")}}"
                                            data-target=".{{$package->get_package_html_id()}}"
                                            class="btn-select btn btn-secondary text-uppercase {{old('package')== $package->id ? 'active' : ''}}">
                                        lựa chọn <i
                                                class="ion-round"></i></button>
                                    <button class="hidden-sm-up btn btn-secondary text-uppercase">Bỏ qua</button>
                                </div>
                            </div>
                        </div>
                        <!-- end feature-packages -->
                    </div>
                @endforeach
            </div>
        </div>

    </div>
    <!-- end price table -->
    {{ render_validate_msg('package',$errors) }}
</div>
<div class="price-table-note mb-3 {{count($errors) > 0 ? 'show' : ''}}">
    <div class=" d-flex justify-content-end">
        <i class="ion-android-create"></i>
        <p class="mb-0">
            Quảng cáo của bạn sẽ <strong>hoạt động trong 30 ngày</strong><br>
            (*) Sau 30 ngày tin của bạn sẽ trở về gói miễn phí. Bạn có thể chọn tự động gia hạn
        </p>
    </div>
</div>