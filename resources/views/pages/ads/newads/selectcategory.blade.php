@extends('master')
@section('content-page')
    <main id="compose-post-page" class="compose-post-page-step1  compose-post-page wrapper-main" role="main">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-lg-6 col-md-8">
                    <div class="header-title">
                        <h4 class="text-uppercase">Tôi muốn đăng tin</h4>
                    </div>
                    <form id="category-selector-form">
                        <div class="category-selector-group">
                            <div class="breadcrumbs">
                                <ul class="category-selector-heading list-unstyled">
                                    <li>
                                        Lựa chọn 1 danh mục
                                    </li>
                                </ul>
                            </div>
                            <ul class="group-nav list-unstyled">
                                @foreach($lstCates as $cate)
                                    @include('pages.ads.newads.parts.list-category')
                                @endforeach
                            </ul>
                        </div>
                        <input id="selected-cate" type="hidden" name="danhmuc">
                    </form>
                </div>
            </div>
        </div>
    </main>
@endsection

@push('script-custom')
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            // select categories
            $('.compose-post-page .group-nav  a').click(function (event) {
                event.preventDefault();
                if ($(this).closest('li').hasClass('has-child')) {
                    var id = $(this).parent('li').attr("data-id");
                    var txt = $(this).text();
                    $('.compose-post-page .group-nav li[data-root="' + id + '"]').show();
                    $('.compose-post-page .group-nav li:not([data-root="' + id + '"])').hide();
                    $('.category-selector-heading').append('<li data-link="' + id + '">' + txt + '</li>');
                } else {
                    window.location.href = $(this).data('href');
                }


            });
//        $('.compose-post-page .group-nav a').
            //breadcrumbs
            $('.category-selector-heading').on('click', 'li', function (event) {
                event.preventDefault();
                var id = $(this).attr("data-link");
                $(this).next().remove();
                $('.compose-post-page-step1 .group-nav li:not(li[data-root="' + id + '"])').hide();
                $('.compose-post-page-step1 .group-nav li[data-root="' + id + '"]').show();
            });
            $('.category-selector-heading').on('click', 'li:first-child', function (event) {
                $('.compose-post-page-step1 .group-nav li').hide();
                $('.compose-post-page-step1 .group-nav li[data-root="0"]').show();
            });
        });
    </script>
@endpush

@push('css-custom')
    <style>
        .compose-post-page {
            min-height: 700px;
        }
    </style>
@endpush

{{--data-link > data-id > data-root--}}