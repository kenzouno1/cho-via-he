<div class="form-value mb-3">
    <div class="group">
        <label class="font-weight-bold">Giá</label>
        <div class="row d-flex align-items-md-top">
            <div class="col-lg-3 wapper-price form-group">
                <div class="form-inline">
                    <input value="{{old('price')}}" name="price" type="number" class="r-xl-2 mr-md-1 mr-2 price form-control" min="0"
                           onkeypress="return isNumberKey(event)" data-parsley-required="" data-parsley-required-message="Nhập giá sản phẩm">
                    <span class="currency">VNĐ</span>
                </div>
                <small class="form-text text-muted d-block docgia">Giá sản phẩm bằng chữ</small>
            </div>
            <div class="col-lg-9 pt-2 select-price-type">
                <label class="custom-radio d-lg-inline-block d-block mr-lg-3 mb-2 ">
                    <input  {{checked(old('price-type','fixed'),'fixed')}} name="price-type" value="fixed" name="radio-stacked" type="radio"
                           class="custom-control-input">
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Đúng giá</span>
                </label>
                <label class="custom-radio d-lg-inline-block d-block mr-lg-3 mb-2 ">
                    <input {{checked(old('price-type'),'treat')}} name="price-type" value="treat" name="radio-stacked" type="radio"
                           class="custom-control-input">
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Thương lượng</span>
                </label>
                <label class="custom-radio d-lg-inline-block d-block mr-lg-3 mb-2 ">
                    <input  {{checked(old('price-type'),'trade')}} name="price-type" value="trade" type="radio" class="custom-control-input">
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Trao đổi / Thương mại</span>
                </label>
            </div>
        </div>
    </div>
    <!-- end form gruop -->
</div>
<div class="select-min-amount">
    <div class="form-group mb-2">
        <label class="check-minimum-amount custom-checkbox custom-radio-inline ">
            <input type="checkbox" name="has-min" value="1" class="custom-control-input">
            <span class="custom-control-indicator"></span>
            <span class="custom-control-description font-weight-bold">Bạn có muốn đặt số tiền cho phép tối thiểu không?</span>
        </label>
    </div>
    <!-- end form group -->
    <div class="set-minimum-amount form-group  mb-0 ">
        <div class="row">
            <div class="col-lg-8 wapper-price  mb-3">
                <div class="form-inline">
                    <input name="price-min" type="number"
                           class="input-xs mr-xl-2 mr-md-1 price form-control" min="0"
                           onkeypress="return isNumberKey(event)">
                    <span class="text-muted">VNĐ</span>
                </div>
                <small class="form-text text-muted d-block docgia">Giá bằng chữ</small>
            </div>
            <div class="col-lg-4 hidden-md-down">
                <div class="note note-secondary  p-sm-3">
                    <div class="icon-note"><i class="ion-information-circled"></i></div>
                    <p>
                        Bạn sẽ không nhận được thông báo dưới số tiền cho phép tối thiểu.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end form-group -->