<div class="module mb-4">
    <div class="module-header">
        <div class="p-sm-3 px-2 py-3 font-weight-bold">
            <h6>Thông tin liên lạc</h6>
        </div>
    </div>
    <fieldset class="m-sm-3 mx-2 my-3">
        @if(AuthHelper::current_user()->isMod())
            <div class="row">
                <div class="col-lg-7">
                    <div class="form-group {{ validate_form_class('mod-fullname',$errors) }}">
                        <label class="font-weight-bold">Tên người đăng</label>
                        <input type="text" class="form-control" name="mod-fullname" value="{{old('mod-fullname')}}" required="" data-parsley-required-message="Bạn phải nhập tên người đăng">
                        {{ render_validate_msg('mod-fullname',$errors) }}
                    </div>
                </div>
            </div>
        @endif
        {{--<div class="row d-flex align-items-md-center justify-content-between">--}}
        {{--<div class="col-lg-7">--}}
        {{--<div class="form-group {{ validate_form_class('email',$errors) }}">--}}
        {{--<label class="font-weight-bold">Email--}}
        {{--<small>(không bắt buộc)</small>--}}
        {{--</label>--}}
        {{--<input type="email" class="form-control" name="email" value="{{$member->email}}"--}}
        {{--data-parsley-error-message="Email nhập vào không hợp lệ">--}}
        {{--{{ render_validate_msg('email',$errors) }}--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-lg-4 hidden-md-down">--}}
        {{--<div class="note note-secondary p-sm-3">--}}
        {{--<div class="icon-note"><i class="ion-android-mail"></i></div>--}}
        {{--<p>--}}
        {{--Địa chỉ email của bạn sẽ không được hiển thị--}}
        {{--</p>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        <div class="row">
            <div class="col-lg-7">
                <div class="form-group">
                    <label class="font-weight-bold">Số điện thoại</label>
                    @if(AuthHelper::current_user()->isMod())
                        <input name="mod-phone" type="text" class="form-control" value="{{old('mod-phone')}}" required="" data-parsley-required-message="Bạn phải nhập số điện thoại người đăng">
                    @else
                        <input disabled type="text" class="form-control" value="{{$member->phone}}" >
                    @endif
                </div>
            </div>
        </div>
        <div class="row mb-md-3 d-flex align-items-md-center justify-content-between">
            <div class="col-lg-7">
                <div class="form-group">
                    <label class="font-weight-bold">Địa chỉ </label>
                    <a class="float-right primary-color current-location" href="" title=""><i
                                class="ion-android-pin"></i> Sự dụng vị trí hiện tại của tôi?</a>
                    <input value="{{old('address')}}" id="location" name="address" type="text" class="form-control">
                    <input value="{{old('lat')}}" name="lat" type="hidden" value="">
                    <input value="{{old('lng')}}" name="lng" type="hidden" value="">
                    <div class="form-control-feedback err-location hide">Bạn chưa nhập địa chỉ hoặc bạn đã nhập sai
                    </div>
                </div>
            </div>
            <div class="col-lg-4 hidden-md-down">
                <div class="note note-secondary p-sm-3">
                    <div class="icon-note"><i class="ion-android-pin"></i></div>
                    <p>
                        Địa chỉ của bạn sẽ giúp người mua có thể xác định vị trí của bạn để mua hàng. Vì vậy bạn nên
                        điền đầy đủ thông tin chính xác nhất.
                    </p>
                </div>
            </div>
        </div>
    </fieldset>
</div>
