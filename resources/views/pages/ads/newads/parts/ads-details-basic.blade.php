 <div class="module mb-4">
    <div class="module-header">
        <div class="p-sm-3 font-weight-bold px-2 py-3">
            <h6>Thông tin mô tả</h6>
        </div>
    </div>
    <fieldset>
        <div class="has-danger form-group m-sm-3 mx-2 my-3">
            <label class="mr-sm-5 mb-sm-0 mb-3 font-weight-bold d-block d-sm-inline-block">Loại tin</label>
            <label class="custom-radio-inline mr-sm-3 mr-5 mb-0 custom-radio">
                <input  {{checked(old('ads-type','sell'),'sell')}} id="can-ban" name="ads-type" value="sell" type="radio" class="custom-control-input">
                <span class="custom-control-indicator"></span>
                <span class="custom-control-description">Cần bán</span>
            </label>
            <label class="custom-radio-inline mr-sm-3 mb-0 custom-radio">
                <input  {{checked(old('ads-type'),'buy')}} id="can-mua" name="ads-type" value="buy" type="radio" class="custom-control-input">
                <span class="custom-control-indicator"></span>
                <span class="custom-control-description">Cần mua</span>
            </label>
        </div>
    </fieldset>
    <fieldset>
        <div class="m-sm-3 mx-2 my-3">
            @include('pages.ads.newads.parts.ads-select-price')
            @include('pages.ads.newads.parts.ads-general')
        </div>
    </fieldset>
</div>