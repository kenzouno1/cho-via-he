<div class="module mb-4">
    <div class="module-header">
        <div class="p-sm-3 font-weight-bold px-2 py-3">
            <h6>Thông tin mô tả</h6>
        </div>
    </div>
    <fieldset>
        <div class="has-danger form-group m-sm-3 mx-2 my-3">
            <label class="mr-sm-5 mb-sm-0 mb-3 font-weight-bold d-block d-sm-inline-block">Loại tin</label>
            <label class="custom-radio-inline mr-sm-3 mr-5 mb-0 custom-radio">
                <input {{checked(old('ads-type','buy'),'buy')}} id="can-ban" name="ads-type" value="buy" type="radio"
                       class="custom-control-input">
                <span class="custom-control-indicator"></span>
                <span class="custom-control-description">Việc cần người</span>
            </label>
            <label class="custom-radio-inline mr-sm-3 mb-0 custom-radio">
                <input id="can-mua" name="ads-type" type="radio" value="sell" class="custom-control-input">
                <span class="custom-control-indicator"></span>
                <span class="custom-control-description">Người tìm việc</span>
            </label>
        </div>
    </fieldset>
    <fieldset>
        <div class="m-sm-3 mx-2 my-3">
            <div class="row d-flex align-items-md-center">
                <div class="col-lg-5 col-md-8">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="font-weight-bold">Mức lương tối thiểu</label>
                            <select name="muc-luong-min" class="select-wage select-style-full  custom-select mucluong">
                                @foreach(range(1,20) as $item )
                                    <option value="{{$item*1000000}}">{{$item}} triệu</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group  col-md-6">
                            <label class="font-weight-bold">Mức lương Tối đa</label>
                            <select name="muc-luong-max" class="select-wage select-style-full  custom-select mucluong">
                                @foreach(range(3,50) as $item )
                                    <option value="{{$item*1000000}}">{{$item}} triệu</option>
                                @endforeach
                                    <option value="9999">lớn hơn 100 triệu</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 mt-md-3">
                    <label class="custom-radio-tl custom-radio-inline mr-sm-3 mr-5 mb-lg-0 mb-3 custom-radio">
                        <input id="check-wage" name="price-type" value="treat" type="checkbox"
                               class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Thương lượng</span>
                    </label>
                </div>
            </div>
            <!-- end -->
            <div class="has-danger form-group">
                <label class="mr-sm-5 mb-sm-0 mb-3 font-weight-bold d-block d-sm-inline-block">Hình thức</label>
                <label class="custom-radio-inline mr-sm-3 mr-5 mb-0 custom-radio">
                    <input checked="" id="can-ban" name="hinhthuc" value="toàn thời gian" type="radio"
                           class="custom-control-input">
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Toàn thời gian</span>
                </label>
                <label class="custom-radio-inline mr-sm-3 mb-0 custom-radio">
                    <input id="can-mua" name="hinhthuc" type="radio" value="bán thời gian" class="custom-control-input">
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Bán thời gian</span>
                </label>
            </div>
            <!-- end form-group -->
            <div class="row">
                <div class="col-lg-7">
                    <div class="form-group ">
                        <label class="font-weight-bold">Tên công ty hoặc Cơ sở</label>
                        <input name="congty" type="text" class="form-control" >
                        <!-- required=""
                               data-parsley-required-message="Nhập tên công ty hoặc cơ sở"
                               data-parsley-length="[6, 100]"
                               data-parsley-length-message="Tên công ty phải ít nhất 6 ký tự không được quá 100 ký tự" -->
                    </div>
                </div>
            </div>
            <!-- end form-group -->
            <div class="row">
                <div class="col-lg-7">
                    <div class="form-group ">
                        <label class="font-weight-bold">Nơi làm việc</label>
                        <input name="diachi" type="text" class="form-control" required=""
                               data-parsley-required-message="Nhập nơi làm việc">
                    </div>
                </div>
            </div>
            <!-- end form-group -->
            <div class="row">
                <div class="col-lg-7">
                    <div class="form-group ">
                        <label class="font-weight-bold">Hạn nạp hồ sơ / tuyển dụng</label>
                        <input name="hannop" class="form-control" type="date" required=""
                               data-parsley-required-message="Nhập hạn nạp hồ sơ">
                    </div>
                </div>
            </div>
            <!-- end form-group -->
            <a class="button-info-option font-weight-bold" href="" title=""><i class="ion-ios-compose"></i> Thêm thông
                tin khác</a>
            <div class="form-group-info-option">

            </div>
            <!-- end form group -->
            @include('pages.ads.newads.parts.ads-general-job')
        </div>
    </fieldset>
</div>
@push('script-custom')
    <script>

        $('select[name="muc-luong-min"]').change(function () {
            var min = parseInt($(this).val());
            $('select[name="muc-luong-max"] option').each(function () {
                var max = parseInt($(this).val());
                if (max < min) {
                    $(this).prop("disabled", true);
                    $(this).attr("selected", false);
                }
                else if (max == min) {
                    $(this).prop("disabled", false);
                    $(this).attr("selected", true);
                }
                else {
                    $(this).prop("disabled", false);
                    $(this).attr("selected", false);
                }
            });
        });
//        $('.mucluong').change(function () {
//            var min = $('select[name="muc-luong-min"]').val();
//            var max = $('select[name="muc-luong-max"]').val();
//            $('input[name="mucluong"]').val(min + ' triệu' + ' - ' + max + ' triệu');
//        });
    </script>
@endpush
