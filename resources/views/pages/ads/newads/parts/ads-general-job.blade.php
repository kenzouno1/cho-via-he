<div class="row d-flex align-items-md-center">
    <div class="col-lg-7 col-md-8">
        <div class="form-group {{ validate_form_class('title',$errors) }}">
            <label class="font-weight-bold">Tên Công việc</label>
            <input type="text" name="title" value="{{old('title')}}" class="form-control" required=""
                   data-parsley-required-message="Nhập têm công việc">
            {{ render_validate_msg('title',$errors) }}
        </div>
    </div>
</div>
@include('pages.ads.boost.mockup')
<div class="row d-flex align-items-md-center justify-content-between">
    <div class="col-lg-7">
        <div class="form-group mb-0">
            <label class="font-weight-bold">Mô tả chi tiết công việc</label>
            <textarea value="{{old('description')}}" class="form-control" name="description" rows="3" required="" data-parsley-required-message="Công việc của bạn sẽ được nhiều người chú ý hơn khi được mô tả chi tiết" data-parsley-length="[20, 5000]"  data-parsley-minlength-message="Bạn phải nhật ít nhất 20 ký tự và tối đa là 5000"></textarea>
        </div>
    </div>
    <div class="col-md-4 hidden-md-down">
        <div class="note note-secondary p-sm-3">
            <div class="icon-note"><i class="ion-android-create"></i></div>
            <p>
                Bạn có biết chúng tôi MIỄN PHÍ khi chỉnh sửa bài đăng của bạn trong suốt thời gian bài đăng
                còn tồn tại trên hệ thống.
            </p>
        </div>
    </div>
</div>