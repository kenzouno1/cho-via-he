<li class="{{$cate->cate_class()}}" data-id="{{$cate->id}}"
    data-root="{{$cate->parent}}">
    <a href="#" data-href="{{route('ads.new',['slug'=>$cate->slug])}}"
       title="{{$cate->name}}">
        <i class="{{$cate->image}}"></i> {{$cate->name}}
    </a>
</li>
@if($cate->hasChild())
    @foreach($cate->getChildrent() as $item)
        @include('pages.ads.newads.parts.list-category',['cate'=>$item])
    @endforeach
@endif
