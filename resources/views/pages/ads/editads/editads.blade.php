@extends('master')
@section('content-page')
    @include('template-part.modal-check-payment-ads')
    {{--@endif--}}
    {{--modal package-preview--}}
    @include('pages.ads.boost.modal-package-preview')
    {{--end modal package-preview--}}
    <main class="compose-post-page wrapper-main" role="main">
        <div class="container">
            @include('pages.ads.editads.package')
            <form class="form-post-add show" id="form-new-post"
                  action="{{route('member.manage-post.edit',['id' => $ads->id])}}" method="post"
                  enctype="multipart/form-data" data-parsley-validate="">
                {{csrf_field()}}
                <input type="hidden" name="rand" value="{{AuthHelper::current_user()->id.time()}}">
                <input type="hidden" name="package" value="{{$ads->getPackageIdAttribute()}}" id="package-value">
                <input type="hidden" name="ads_id" value="{{$ads->id}}">
                @include("pages.ads.editads.parts.ads-details-$template")
                @include('pages.ads.editads.parts.upload')
                @include('pages.ads.editads.parts.contact')
                <div class="module mb-4">
                    <div class="module-header">
                        <div class="p-sm-3 px-2 py-3 font-weight-bold">
                            <h6>Hoàn thành và đăng tin</h6>
                        </div>
                    </div>
                    <fieldset class="m-sm-3 mx-2 my-3">
                        <p>Chọn một trong những tính năng quảng cáo bên dưới và tăng cường quảng cáo của bạn</p>
                        <div class="row">
                            @include('pages.ads.boost.hightlight')
                            @include('pages.ads.boost.show-home')
                            @include('pages.ads.boost.topsearch')
                            <div class="col-xl-7 col-lg-8 mb-2">
                                <div class="form-check">
                                    <label class="checkbox-style1 custom-checkbox custom-radio-inline d-flex justify-content-start ">
                                        <input {{checked($ads->getPackageRenewAttribute(),1)}} type="checkbox"
                                               name="auto-renew" class="custom-control-input">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">
									  	<strong class="d-block">Bạn có muốn tự động gia hạn?</strong>
									  	 Quảng cáo cộng với của bạn sẽ được tự động gia hạn sau (30 ngày) và thanh toán sẽ tự động được khấu trừ khỏi thẻ tín dụng của bạn
									  </span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <button type="submit"
                                class="btn-post-add btn btn-primary text-uppercase font-weight-bold mb-3 mt-1">Đăng
                            tin
                        </button>
                        <a class="preview-post-url font-weight-bold" href="" title="">Xem trước</a>
                    </fieldset>
                    {{ render_success_msg($errors) }}
                </div>
            </form>
        </div>
    </main>
@endsection
@push('css-custom')
    @include('pages.ads.assets.css')
@endpush
@push('script-custom')
    @include('pages.ads.assets.js')
@endpush
