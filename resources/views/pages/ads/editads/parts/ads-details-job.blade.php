@php
    if ($ads->get_meta('mucluong') != 'Thương Lượng'){
    $max_min = explode("-",$ads->get_meta('mucluong'));
    }else{
     $max_min = [0,0];
    }
@endphp
<div class="module mb-4">
    <div class="module-header">
        <div class="p-sm-3 font-weight-bold px-2 py-3">
            <h6>Thông tin mô tả</h6>
        </div>
    </div>
    <fieldset>
        <div class="has-danger form-group m-sm-3 mx-2 my-3">
            <label class="mr-sm-5 mb-sm-0 mb-3 font-weight-bold d-block d-sm-inline-block">Loại tin</label>
            <label class="custom-radio-inline mr-sm-3 mr-5 mb-0 custom-radio">
                <input {{ checked($ads->ads_type,'buy')}}  checked="" id="can-ban" name="ads-type" value="buy"
                       type="radio" class="custom-control-input">
                <span class="custom-control-indicator"></span>
                <span class="custom-control-description">Việc cần người</span>
            </label>
            <label class="custom-radio-inline mr-sm-3 mb-0 custom-radio">
                <input {{ checked($ads->ads_type,'sell')}} id="can-mua" name="ads-type" type="radio" value="sell"
                       class="custom-control-input">
                <span class="custom-control-indicator"></span>
                <span class="custom-control-description">Người tìm việc</span>
            </label>
        </div>
    </fieldset>
    <fieldset>
        <div class="m-sm-3 mx-2 my-3">
            <div class="row d-flex align-items-md-center">
                <div class="col-lg-5 col-md-8">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="font-weight-bold">Mức lương tối thiểu</label>
                            <select name="muc-luong-min"
                                    class="select-wage select-style-full  custom-select mucluong" {{$ads->price_type == 'treat' ? 'disabled' : ''}}>
                                @foreach(range(1,20) as $item )
                                    <option {{selected($ads->get_meta('muc-luong-min'),$item*1000000)}} value="{{$item*1000000}}">{{$item}}
                                        triệu
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group  col-md-6">
                            <label class="font-weight-bold">Mức lương Tối đa</label>
                            <select name="muc-luong-max"
                                    class="select-wage select-style-full  custom-select mucluong" {{$ads->price_type == 'treat' ? 'disabled' : ''}}>
                                @foreach(range(3,50) as $item )
                                    <option {{selected($ads->get_meta('muc-luong-max'),$item*1000000)}} value="{{$item*1000000}}">{{$item}}
                                        triệu
                                    </option>
                                @endforeach
                                <option value="9999">lớn hơn 100 triệu</option>
                            </select>
                        </div>
                        <input type="hidden" name="mucluong" value="{{$ads->get_meta('mucluong')}}">
                    </div>
                </div>
                <div class="col-lg-3 col-md-4">
                    <label class="custom-radio-inline mr-sm-3 mr-5 mb-lg-0 mb-3 custom-radio">
                        <input {{ checked($ads->price_type,'treat')}} id="check-wage"
                               name="price-type"
                               value="treat" type="checkbox" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Thương lượng</span>
                    </label>
                </div>
            </div>
            <!-- end -->
            <div class="has-danger form-group">
                <label class="mr-sm-5 mb-sm-0 mb-3 font-weight-bold d-block d-sm-inline-block">Hình thức</label>
                <label class="custom-radio-inline mr-sm-3 mr-5 mb-0 custom-radio">
                    <input {{ checked($ads->get_meta('hinhthuc'),'fulltime')}} id="can-ban" name="hinhthuc"
                           value="toàn thời gian" type="radio" class="custom-control-input">
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Toàn thời gian</span>
                </label>
                <label class="custom-radio-inline mr-sm-3 mb-0 custom-radio">
                    <input {{ checked($ads->get_meta('hinhthuc'),'part-time')}} id="can-mua" name="hinhthuc" type="radio"
                           value="bán thời gian" class="custom-control-input">
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Bán thời gian</span>
                </label>
            </div>
            <!-- end form-group -->
            <div class="row">
                <div class="col-lg-7">
                    <div class="form-group ">
                        <label class="font-weight-bold">Tên công ty hoặc Cơ sở</label>
                        <input value="{{$ads->get_meta('congty')}}" name="congty" type="text" class="form-control"
                               required="" data-parsley-required-message="Nhập tên công ty hoặc cơ sở">
                    </div>
                </div>
            </div>
            <!-- end form-group -->
            <div class="row">
                <div class="col-lg-7">
                    <div class="form-group ">
                        <label class="font-weight-bold">Nơi làm việc</label>
                        <input value="{{$ads->get_meta('diachi')}}" name="diachi" type="text" class="form-control">
                    </div>
                </div>
            </div>
            <!-- end form-group -->
            <div class="row">
                <div class="col-lg-7">
                    <div class="form-group ">
                        <label class="font-weight-bold">Hạn nạp hồ sơ / tuyển dụng</label>
                        <input value="{{$ads->get_meta('hannop')}}" name="hannop" type="text" class="form-control">
                    </div>
                </div>
            </div>
            <!-- end form-group -->
            <a class="button-info-option font-weight-bold" href="" title=""><i class="ion-ios-compose"></i> Thêm thông
                tin khác</a>
            <div class="form-group-info-option">
            </div>
            <!-- end form group -->
            @include('pages.ads.editads.parts.ads-general-job')
        </div>
    </fieldset>
</div>
@push('script-custom')
    <script>

        $('select[name="muc-luong-min"]').change(function () {
            var min = parseInt($(this).val());
            $('select[name="muc-luong-max"] option').each(function () {
                var max = parseInt($(this).val());
                if (max < min) {
                    $(this).prop("disabled", true);
                    $(this).attr("selected", false);
                }
                else if (max == min) {
                    $(this).prop("disabled", false);
                    $(this).attr("selected", true);
                }
                else {
                    $(this).prop("disabled", false);
                    $(this).attr("selected", false);
                }
            });
        });
        $('.mucluong').change(function () {
            var min = $('select[name="muc-luong-min"]').val();
            var max = $('select[name="muc-luong-max"]').val();
            $('input[name="mucluong"]').val(min + ' triệu' + ' - ' + max + ' triệu');
        });
    </script>
@endpush