<div class="module mb-4">
    <div class="module-header">
        <div class="p-sm-3 font-weight-bold px-2 py-3">
            <h6>Thông tin mô tả</h6>
        </div>
    </div>
    <fieldset>
        <div class="has-danger form-group m-sm-3 mx-2 my-3">
            <label class="mr-sm-5 mb-sm-0 mb-3 font-weight-bold d-block d-sm-inline-block">Loại tin</label>
            <label class="custom-radio-inline mr-sm-3 mr-5 mb-0 custom-radio">
                <input {{checked($ads->ads_type,'sellbuy')}} id="can-ban" name="ads-type" value="sellbuy" type="radio" class="custom-control-input">
                <span class="custom-control-indicator"></span>
                <span class="custom-control-description">Bất động sản mua bán</span>
            </label>
            <label class="custom-radio-inline mr-sm-3 mb-0 custom-radio">
                <input {{checked($ads->ads_type,'rent')}} id="can-mua" name="ads-type" value="rent" type="radio" class="custom-control-input">
                <span class="custom-control-indicator"></span>
                <span class="custom-control-description">Bất động sản cho thuê</span>
            </label>
            <label class="custom-radio-inline mr-sm-3 mb-0 custom-radio">
                <input {{checked($ads->ads_type,'new')}} id="can-mua" name="ads-type" value="new" type="radio" class="custom-control-input">
                <span class="custom-control-indicator"></span>
                <span class="custom-control-description">Dự án mới</span>
            </label>
        </div>
    </fieldset>
    <fieldset>
        <div class="m-sm-3 mx-2 my-3">
            @include('pages.ads.newads.parts.ads-select-price')
            <div class="row">
                <div class="col-lg-7">
                    <div class="form-group ">
                        <label class="font-weight-bold">Diện tích</label>
                        <input type="text" name="dt" class="form-control" value="{{$ads->get_meta('dt')}}">
                    </div>
                </div>
            </div>
            <!-- end form-group -->
            <div class="row">
                <div class="col-lg-7">
                    <div class="form-group ">
                        <label class="font-weight-bold">Hướng</label>
                        <input type="text" name="huong" class="form-control" value="{{$ads->get_meta('huong')}}">
                    </div>
                </div>
            </div>
            <!-- end form-group -->

            <div class="row d-flex align-items-md-center">
                <div class="col-lg-5 col-md-8">
                    <div class="form-group ">
                        <label class="font-weight-bold">Số tầng</label>
                        <select name="sotang" class="select-style-full  custom-select">
                            @foreach(range(1,10) as $item)
                                <option {{ selected($item,$ads->get_meta('sotang')) }} value="{{$item}}">{{$item}} Tầng</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-7">
                    <div class="form-group ">
                        <label class="font-weight-bold">Số phòng ngủ</label>
                        <select name="phongngu" class="select-style-full  custom-select">
                            <option value="" selected>Số phòng</option>
                            @foreach($lstSoPhong as $item )
                                <option {{ selected($item,$ads->get_meta('phongngu')) }} value="{{$item}}">{{$item}} phòng</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <!-- end form-group -->
            <div class="row">
                <div class="col-lg-7">
                    <div class="form-group ">
                        <label class="font-weight-bold">Số phòng vệ sinh</label>
                        <select name="phongvs" class="select-style-full  custom-select">
                            <option value="" selected>Số phòng</option>
                            @foreach($lstSoPhong as $item )
                                <option {{ selected($item,$ads->get_meta('phongvs')) }} value="{{$item}}">{{$item}} phòng</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <!-- end form-group -->
            <a class="button-info-option font-weight-bold" href="" title=""><i class="ion-ios-compose"></i> Thêm thông
                tin khác</a>
            <div class="form-group-info-option">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="form-group ">
                            <label class="font-weight-bold">Bạn là</label>
                            <select name="nguoiban" class="select-style-full  custom-select">
                                <option {{ selected($ads->get_meta('phongvs'),'owner') }} value="owner">Chủ sở hứu</option>
                                <option {{ selected($ads->get_meta('phongvs'),'owner') }} value="broker">Môi giới</option>
                            </select>
                        </div>
                    </div>
                </div>
                <!-- end form-group -->
            </div>
            <!-- end form group -->
            @include('pages.ads.editads.parts.ads-general')
        </div>
    </fieldset>
</div>