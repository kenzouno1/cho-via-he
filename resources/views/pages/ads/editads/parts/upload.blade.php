<div class="module mb-4">
    <div class="module-header">
        <div class="p-sm-3 px-2 py-3 font-weight-bold">
            <h6>Thêm ảnh</h6>
        </div>
    </div>
    <fieldset class="m-sm-3 mx-2 my-3">
        <div class="row justify-content-between align-items-md-center row-upload-img">
            <div class="col-lg-7">
                <div class="form-group">
                    <p>Để có kết quả tốt nhất chúng tôi khuyên bạn nên chọn hình ảnh thực tế để tạo sự tin tưởng</p>
                    <div class="upload-img-post upload-placeholder">
                        <ul class="list-unstyled">
                            @foreach($lstImage as $img)
                                <li class="success" data-src="{{$ads->getImageName($img)}}">
                                    <a class="remove-img" href="" title="">
                                        <i class="ion-close-round"></i>
                                    </a>
                                    <img src="{{$img}}"
                                         style=" width: 100%; height: 100%;">
                                    <div class="progress">
                                        <div class="progress-bar" style="width: 100%;"></div>
                                    </div>
                                </li>
                            @endforeach
                            @foreach(range(count($lstImage),9) as $index)
                                @if($loop->first)
                                    <li class="upload-begin upload-image-disabled"><i class="ion-camera"></i></li>
                                @else
                                    <li class="upload-image-disabled"><i class="ion-camera"></i></li>
                                @endif
                            @endforeach
                        </ul>
                        <button type="button"
                                class="mt-md-2 btn btn-primary btn-upload text-uppercase font-weight-bold">
                            Thêm ảnh
                        </button>
                        <input id="file-upload" type="file" name="images" multiple>
                        <input type="hidden" name="list_image" value="{{json_encode($ads->images)}}">
                    </div>
                </div>
            </div>
            <div class="col-md-4 hidden-md-down">
                <div class="note note-secondary p-sm-3 mb-md-4">
                    <div class="icon-note"><i class="ion-camera"></i></div>
                    <p>
                        Quảng cáo với hình ảnh thành công hơn. Tải lên tối đa đến 10 hình. Bạn có thể tải lên hình ảnh
                        có kích thước đến 4MB. Kéo thả tệp vào cửa sổ này để tải ảnh.
                    </p>
                </div>
                <div class="note note-secondary p-sm-3">
                    <div class="icon-note"><i class="ion-arrow-move"></i></div>
                    <p>
                        Tải hình lên xong bạn có thể chọn kéo di chuyển vị trí với các hình xung quanh theo thứ tự mình
                        muốn.
                    </p>
                </div>
            </div>
        </div>
    </fieldset>
</div>