<div class="module mb-4">
    <div class="module-header">
        <div class="p-sm-3 font-weight-bold px-2 py-3">
            <h6>Thông tin mô tả</h6>
        </div>
    </div>
    <fieldset>
        <div class="has-danger form-group m-sm-3 mx-2 my-3">
            <label class="mr-sm-5 mb-sm-0 mb-3 font-weight-bold d-block d-sm-inline-block">Loại tin</label>
            <label class="custom-radio-inline mr-sm-3 mr-5 mb-0 custom-radio">
                <input {{checked($ads->ads_type,'sell')}} id="can-ban" name="ads-type" value="sell" type="radio"
                       class="custom-control-input">
                <span class="custom-control-indicator"></span>
                <span class="custom-control-description">Cần bán</span>
            </label>
            <label class="custom-radio-inline mr-sm-3 mb-0 custom-radio">
                <input {{checked($ads->ads_type,'buy')}} id="can-mua" name="ads-type" value="buy" type="radio"
                       class="custom-control-input">
                <span class="custom-control-indicator"></span>
                <span class="custom-control-description">Cần mua</span>
            </label>
        </div>
    </fieldset>
    <fieldset>
        <div class="m-sm-3 mx-2 my-3">
        @include('pages.ads.editads.parts.ads-select-price')
        <!-- end form-group -->
            <div class="row">
                <div class="col-lg-7">
                    <div class="form-group ">
                        <label class="font-weight-bold">Dòng xe</label>
                        <select name="dongxe" class="select-style-full  custom-select">
                            @foreach($lstDongXe as $item)
                                <option value="{{$cate->name.' '.$item}}">{{$cate->name.' '.$item}}</option>
                            @endforeach
                        </select>
                        {{--<input type="text" name="dongxe" class="form-control">--}}
                    </div>
                </div>
            </div>
            <div class="row d-flex align-items-md-center">
                <div class="col-lg-7 col-md-8">
                    <div class="form-group">
                        <label class="font-weight-bold">Năm sản xuất</label>
                        <select name="namsx" class="select-style-full  custom-select">
                            @foreach(range(date('Y'),1930) as $year)
                                <option {{ selected($year,$ads->get_meta('namsx')) }} value="{{$year}}">{{$year}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <!-- end -->
            <div class="row">
                <div class="col-lg-7">
                    <div class="form-group ">
                        <label class="font-weight-bold">Kylomet đã chạy</label>
                        <input value="{{$ads->get_meta('kmdachay')}}" name="kmdachay" type="number"
                               class="form-control">
                    </div>
                </div>
            </div>
            <!-- end form-group -->
            <div class="row">
                <div class="col-lg-7">
                    <div class="form-group ">
                        <label class="font-weight-bold">Hộp số</label>
                        <select name="hopso" class="select-style-full  custom-select">
                            @foreach(config('car.general.hopso') as $item)
                                <option {{ selected($item,$ads->get_meta('hopso')) }} value="{{$item}}">{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <!-- end form-group -->
            <a class="button-info-option font-weight-bold" href="" title=""><i class="ion-ios-compose"></i> Thêm thông
                tin khác</a>
            <div class="form-group-info-option">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="form-group ">
                            <label class="font-weight-bold">Nhiên liệu</label>
                            <select name="nhienlieu" class="select-style-full  custom-select">
                                <option value=""></option>
                                @foreach(config('car.general.nhienlieu') as $key => $item)
                                    <option {{ selected($item,$ads->get_meta('nhienlieu')) }} value="{{$key}}">{{$item}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-7">
                        <div class="form-group ">
                            <label class="font-weight-bold">Xuất xứ</label>
                            <select name="noisx" class="select-style-full  custom-select">
                                <option value=""></option>
                                @foreach(config('car.general.noisx') as $key => $item)
                                    <option {{ selected($key,$ads->get_meta('noisx')) }} value="{{$key}}">{{$item}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                @if(config("car.$cate->parent.socho"))
                    <div class="row">
                        <div class="col-lg-7">
                            <div class="form-group ">
                                <label class="font-weight-bold">Số chỗ ngồi</label>
                                <select name="socho" class="select-style-full  custom-select">
                                    <option value=""></option>
                                    @foreach(config("car.$cate->parent.socho") as $item)
                                        <option {{ selected($item,$ads->get_meta('socho')) }} value="{{$item}}">{{$item}}</option>
                                    @endforeach
                                    <option value="other">Khác</option>
                                </select>
                            </div>
                        </div>
                    </div>
                @endif
                @if(config("car.$cate->parent.kieudang"))
                    <div class="row">
                        <div class="col-lg-7">
                            <div class="form-group ">
                                <label class="font-weight-bold">Kiểu dáng</label>
                                <select name="kieudang" class="select-style-full  custom-select">
                                    <option value=""></option>
                                    @foreach(config("car.$cate->parent.kieudang") as $key => $item)
                                        <option {{ selected($key,$ads->get_meta('kieudang')) }} value="{{$key}}">{{$item}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
            @endif

            <!-- end form-group -->
            {{--<div class="row">--}}
            {{--<div class="col-lg-7">--}}
            {{--<div class="form-group ">--}}
            {{--<label class="font-weight-bold">Địa điểm</label>--}}
            {{--<select class="select-style-full  custom-select">--}}
            {{--<option selected="">Chủ sở hứu</option>--}}
            {{--<option value="1">Mua giới</option>--}}
            {{--</select>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            <!-- end form-group -->
            </div>
            <!-- end form group -->
            @include('pages.ads.editads.parts.ads-general')
        </div>
    </fieldset>
</div>