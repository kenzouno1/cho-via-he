<div class="row d-flex align-items-md-center">
    <div class="col-lg-5 col-md-8">
        <div class="form-group {{ validate_form_class('title',$errors) }}">
            <label class="font-weight-bold">Tên Công việc</label>
            <input type="text" name="title" value="{{old('title',$ads->title)}}" class="form-control" required=""
                   data-parsley-required-message="Nhập thông tin sản phẩm">
            <!-- <div class="form-control-feedback">Shucks, check the formatting of that and try again.</div> -->
            {{ render_validate_msg('title',$errors) }}
        </div>
    </div>
</div>
@include('pages.ads.boost.mockup')
<div class="row d-flex align-items-md-center justify-content-between">
    <div class="col-lg-7">
        <div class="form-group mb-0">
            <label class="font-weight-bold">Mô tả chi tiết công việc</label>
            <textarea value="{{old('description',$ads->des)}}" class="form-control" name="description" rows="3"
                      required=""
                      data-parsley-required-message="Sản phẩm của bạn sẽ có nhiều người chú ý hơn nếu có mô tả về nó">
                {{old('description',$ads->des)}}
            </textarea>
        </div>
    </div>
    <div class="col-md-4 hidden-md-down">
        <div class="note note-secondary p-sm-3">
            <div class="icon-note"><i class="ion-android-create"></i></div>
            <p>
                Bạn có biết chúng tôi MIỄN PHÍ khi chỉnh sửa bài đăng của bạn trong suốt thời gian bài đăng
                còn tồn tại trên hệ thống.
            </p>
        </div>
    </div>
</div>