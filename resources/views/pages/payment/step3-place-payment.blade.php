	<!-- place payment -->
	<div class="place-payment">
		<div class="row">
			<div class="col-md-6 pr-lg-5 mb-3 mb-md-0">
				<h6 class="place-payment-title mb-3">
					Thanh toán tại điểm giao dịch
                    </h6>
                    <p>
                        Cám ơn bạn đã sự dụng dịch vụ của Chợ Vỉa Hè Để hoàn tất nạp tiền vào tài khoản, vui lòng thanh toán tại các điểm giao dịch gần nhất với mã mà chúng tôi cung cấp cho bạn sau:
                    </p>
                    <div class="number-money w-100 mb-3">
                        10.000.000 VNĐ
				</div>
				<p class="medium">
					* Để lưu giữ mã thanh toán 1 cách thuận lợi và chính xác, chúng tôi đã gửi mã thanh toán về email: <a href="mailto:abc@gmail.com" title="">abc@gmail.com</a>. Mã có giá trị trong vòng 1 tuần. Khi bạn thanh toán thành công chúng tôi sẽ cập nhật trong <a class="history-payment" href="" title="">lịch sử thanh toán</a> cho bạn.
				</p>
			</div>
			<div class="col-md-6 pl-lg-5">
				<h6 class="place-payment-title mb-3">
					Chọn nơi giao dịch
				</h6>
				<form class="form-select-location-bank">
                    <div class="form-group ">
                        <select class="select-city custom-select w-100">
                          <option selected>Tỉnh/ Thành phố</option>
                          <option value="1">One</option>
                          <option value="2">Two</option>
                          <option value="3">Three</option>
                        </select>
                    </div>
                    <!-- end form-group -->
                    <div class="form-group ">
                        <select class="select-trousers custom-select w-100">
                          <option selected>Quận/ huyện</option>
                          <option value="1">One</option>
                          <option value="2">Two</option>
                          <option value="3">Three</option>
                        </select>
                    </div>
                     <!-- end form-group -->
                    <div class="form-group mb-0">
                      <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Điểm giao dịch gần nhất</span>
                      </label>
                    </div>
                     <!-- end form-group -->
                     <div class="box-list-location-bank mt-3">
                        <h6 class="title mb-2">Tìm thấy 20 địa điểm</h6>
                        <div class="content-list-location-bank">
                            <ul class="list-unstyled">
                                @foreach(range(1,7) as $item)
                                    <li>
                                        <a href="" title="">
                                            <p>FamilyMart </p>
                                            134 Pasteur P.Bến Nghé Q.1 TP.HCM
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- end list locatio -->
                </form>
                <!-- end form select location bank-->
			</div>
		</div>
		<div class="btn-footer-payment d-sm-flex flex-sm-row my-4">
			<a class="btn-green btn btn-secondary mr-sm-4 mb-3 mb-sm-0" href="" title="">Quay về trang chủ</a>
			<a class="btn-yellow btn btn-secondary hidden-xs-down" href="" title="">ĐẾN TRANG QUẢN LÝ BÀI ĐĂNG</a>
		</div>
	</div>