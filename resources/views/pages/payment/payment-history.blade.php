<div class="payment-history bg-white p-3">
    <h5 class="mb-4 text-center">Lịch sử nạp tiền</h5>
    <div class="table-responsive payment-history-table">
        <table class="table table-striped">
        <thead>
        <tr>
            <th class="text-truncate">Mã giao dịch</th>
            <th class="text-truncate">Ngày giờ</th>
            <th class="text-truncate">Loại nạp</th>
            <th class="text-truncate">Nhà mạng/ Ngân hàng</th>
            <th class="text-truncate">Trạng thái</th>
            <th class="text-truncate">Số tiền</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>001</td>
            <td>12h:00 23/12/2015</td>
            <td>ATM</td>
            <td>Vietcombank</td>
            <td>Thành công</td>
            <td>+ 500.000 vnđ</td>
        </tr>
        <tr>
            <td>002</td>
            <td>12h:00 23/12/2015</td>
            <td>Card</td>
            <td>Vina</td>
            <td>Thất bại</td>
            <td>0 vnđ</td>
        </tr>
        <tr>
            <td>003</td>
            <td>12h:00 23/12/2015</td>
            <td>Card</td>
            <td>Mobile</td>
            <td>Thành công</td>
            <td>+ 500.000 vnđ</td>
        </tr>
        </tbody>
    </table>
    </div>
</div>