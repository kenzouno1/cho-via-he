<li data-id="{{$bank['id']}}">
    <a href="#" title="{{ $bank['name'] }}">
        <img class="img-bank" src="{{$bank['logo_url']}}" title="{{ $bank['name'] }}"/>
    </a>
</li>