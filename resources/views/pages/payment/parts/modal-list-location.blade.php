<!-- popup list location exchange -->
<div  class="modal fade" id="modal-list-location" tabindex="-1" role="dialog" aria-labelledby="modal-list-location">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!-- modal-header -->
            <div class="modal-header">
                <h5 class="modal-title">Danh sách các điểm giao dịch</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <i class="ion-close-round"></i>
                </button>
            </div>
            <!-- modal-body -->
           <div class="modal-body py-4 px-3">
                <form class="form-select-location-bank">
                    <div class="form-group ">
                        <select class="select-city custom-select w-100">
                          <option selected>Tỉnh/ Thành phố</option>
                          <option value="1">One</option>
                          <option value="2">Two</option>
                          <option value="3">Three</option>
                        </select>
                    </div>
                    <!-- end form-group -->
                    <div class="form-group ">
                        <select class="select-trousers custom-select w-100">
                          <option selected>Quận/ huyện</option>
                          <option value="1">One</option>
                          <option value="2">Two</option>
                          <option value="3">Three</option>
                        </select>
                    </div>
                     <!-- end form-group -->
                    <div class="form-group mb-0">
                      <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Điểm giao dịch gần nhất</span>
                      </label>
                    </div>
                     <!-- end form-group -->
                     <div class="box-list-location-bank mt-3">
                        <h6 class="title mb-2">Tìm thấy 20 địa điểm</h6>
                        <div class="content-list-location-bank">
                            <ul class="list-unstyled">
                                @foreach(range(1,7) as $item)
                                    <li>
                                        <a href="" title="">
                                            <p>FamilyMart </p>
                                            134 Pasteur P.Bến Nghé Q.1 TP.HCM
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- end list locatio -->
                </form>
                <!-- end form select location bank-->
                
           </div>
        </div>
    </div>
</div>