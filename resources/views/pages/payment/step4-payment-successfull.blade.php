<div class="payment-successful payment-status">
	<p class=" mb-3">Trạng thái thanh toán</p>
	<div class="row justify-content-lg-end mb-3">
		<div class="col-lg-8">
			<h6 class="text-uppercase text-payment-status">BẠN ĐÃ THANH TOÁN THÀNG CÔNG <img src="{{asset('assets/img/icon/success.png')}}" alt=""></h6>
		</div>
	</div>
	<div class="row mb-2 mb-lg-3">
		<div class="col-lg-4">
			<p>Thông tin tài khoản</p>
		</div>
		<div class="col-lg-8">
			<span>Số dư ban đầu: {{\App\Helper\UtilHelper::moneyNumber($user->old_money)}}</span>
		</div>
	</div>
	<div class="row mb-3 mb-sm-4">
		<div class="col-lg-4">
			<p>Thông tin thanh toán:</p>
		</div>
		<div class="col-lg-8">
			<div class="row">
				<div class="col-sm-4 mb-2 mb-sm-0">
					<p>Tên dịch vụ</p>
					<span>Nạp thẻ</span>
				</div>
				<div class="col-sm-4 mb-2 mb-sm-0">
					<p>Trạng thái</p>
					<span>Thành công</span>
				</div>
				<div class="col-sm-4 mb-2 mb-sm-0">
					<p>Thành tiền</p>
					<span>{{\App\Helper\UtilHelper::moneyNumber($package->money)}}</span>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			<h5>Tài khoản của bạn hiện tại:</h5>
		</div>
		<div class="col-lg-8">
			<div class="number-money bg-green w-100 mb-3">
				{{$user->moneyFormat}} VNĐ
			</div>
			<span>Vui lòng liên hệ với Chợ Vỉa Hè với mã giao dịch  <span style="color: #ffb300;">CVH000-{{$paymentLogs->id}}</span>  nếu bạn có thắc mắc về giao dịch này</span>
		</div>
	</div>
	<hr>
	<div class="btn-footer-payment d-sm-flex flex-sm-row my-4">
		<a class="btn-green btn btn-secondary mr-sm-4 mb-3 mb-sm-0" href="{{route('home')}}" title="Về trang chủ">Quay về trang chủ</a>
		<a class="btn-yellow btn btn-secondary hidden-xs-down" href="" title="">ĐẾN TRANG QUẢN LÝ BÀI ĐĂNG</a>
	</div>
</div>