@extends('master')
@section('content-page')

<main class="manage-page wrapper-main" role="main">
    <section class="section-payment-page section-content bg-graylight py-4 py-sm-5">
    	<div class="container">
    		<div class="row justify-content-md-center">
    			<div class="col-md-10">
    				<div class="title-payment title mb-4">
		    			<h6 class="m-0 bg-graylight">NẠP TIỀN VÀO TÀI KHOẢN CHỢ VỈA HÈ</h6>
		    		</div>
		    		<!-- end title -->
		    		@include('pages.payment.step'.$step)
    			</div>
    		</div>
    	</div>
    </section>
    <!-- end section contnet search -->
</main>
@endsection
@push('script-custom')
	@include('pages.payment.js')
@endpush