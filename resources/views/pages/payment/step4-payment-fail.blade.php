<div class="payment-fail payment-status">
	<p class=" mb-3">Trạng thái thanh toán</p>
	<div class="row justify-content-lg-end mb-3">
		<div class="col-lg-8">
			<h6 class="text-uppercase text-payment-status">THANH TOÁN CỦA BẠN ĐÃ BỊ HỦY <img src="{{asset('assets/img/icon/fail.png')}}" alt=""></h6>
		</div>
	</div>
	<div class="row mb-3 mb-sm-4">
		<div class="col-lg-4">
			<p>Thông tin thanh toán:</p>
		</div>
		<div class="col-lg-8">
			<div class="row">
				<div class="col-sm-4 mb-2 mb-sm-0">
					<p>Tên dịch vụ</p>
					<span>Nạp thẻ</span>
				</div>
				<div class="col-sm-4 mb-2 mb-sm-0">
					<p>Trạng thái</p>
					<span>Hủy</span>
				</div>
				<div class="col-sm-4 mb-2 mb-sm-0">
					<p>Thành tiền</p>
					<span>{{$package->priceFormat}}</span>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			<h5>Tài khoản của bạn hiện tại:</h5>
		</div>
		<div class="col-lg-8">
			<div class="number-money bg-green w-100 mb-3">
				{{$user->money}}
			</div>
		</div>
	</div>
	<hr>
	<div class="btn-footer-payment d-sm-flex flex-sm-row my-4">
		<a class="btn-green btn btn-secondary mr-sm-4 mb-3 mb-sm-0" href="{{route('home')}}" title="Về trang chủ">Quay về trang chủ</a>
		<a class="btn-yellow btn btn-secondary hidden-xs-down" href="{{route('member.manage-post')}}" title="Quản lý bài đăng">ĐẾN TRANG QUẢN LÝ BÀI ĐĂNG</a>
	</div>
</div>