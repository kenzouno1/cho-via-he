<div class="submit-payment-error">
	<div class="row justify-content-md-center">
		<div class="col-lg-6">
			<p class="text1 mb-4 mb-sm-5">
				Xin lỗi, hiện tại chúng tôi chỉ hỗ trợ dịch vụ Internet Banking Ngân hàng bạn chọn chưa hỗ trợ ...
			</p>
			<p class="text2">
				Click <a href="" title="">VÀO ĐÂY</a> để tiếp tục thanh toán.
			</p>
		</div>
	</div>
</div>
<!-- end submit error -->