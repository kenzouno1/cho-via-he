<div class="cards-payment bg-white p-3">
	<h6 class="medium mb-4 d-flex justify-content-center">Nạp thẻ cào</h6>
	<form method="POST" action="{{route('payment.mobile',compact('mang','package'))}}">
		{{csrf_field()}}
		<div class="row justify-content-center">
			<div class="col-md-4">
				<div class="form-group">
					<input type="text" class="form-control" name="pin"  placeholder="Nhập mã thẻ cào" required>
				</div>
				 <!-- end form-group -->
				 <div class="form-group">
					<input type="text" class="form-control" name="seri" placeholder="Nhập số seri" required>
				</div>
				 <!-- end form-group -->
				<p  class="mb-4">Vui lòng sự dụng thẻ cào đúng với giá trị của Chợ vỉa hè để nhận ưu đãi của chúng tôi.</p>
				<button type="submit" class="btn btn-primary text-uppercase font-weight-bold mb-3">Hoàn tất</button>
				<div class="alert alert-danger" role="alert">
					<strong>Nạp thẻ thất bại!</strong> Mã thẻ không tồn tại, hoặc đã được sử dụng
				</div>
				<div class="alert alert-success" role="alert">
					<strong>Nạp thẻ thành công!</strong> Nhấp <a href="#" class="alert-link">Vào đây</a> Để kiểm tra tài khoản.
				</div>
			</div>

		</div>

	</form>
</div>