<script type="text/javascript">
    jQuery(document).ready(function ($) {
        //active money
        $('.pillow-money li').click(function (event) {
            if (!$(this).hasClass('active')) {
                $('.pillow-money li').removeClass('active');
                $(this).addClass('active');
                var route = $(this).data('route');
                $('.btn-next').prop('disable',false).attr('href',route);
            }
        });
        //click select method payment
        $('.method-payment').on('click', ' .select-method-payment > li', function (event) {
            if (!$(this).hasClass('active')) {
                $('.select-method-payment li ul').slideUp('fast');
                $('.select-method-payment > li').removeClass('active');
                $(this).addClass('active');
                $(this).find('ul').slideDown('fast');
            }
        });
        //click apply code
//        $('.btn-apply-code').click(function (event) {
//            event.preventDefault();
//            $(this).parent('p').hide('fast');
//            $('.form-promotion-code').slideDown('fast');
//        });

        //show more list bank
        $('.view-more-bank').click(function (event) {
            event.preventDefault();
            $('.list-banks-payment li:nth-of-type(n+10)').show();
            $(this).hide();
        });
        //select card payment
        $('.list-option-payment li').click(function (event) {
            event.preventDefault();
            if (!$(this).hasClass('active')) {
                $('.list-option-payment li').removeClass('active');
                $(this).addClass('active');
                var id = $(this).data('id');
                $('#bank_payment_method_id').val(id);
            }
        });

    });
</script>