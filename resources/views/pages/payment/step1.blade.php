<!-- end header -->
<div class="content-payment bg-white p-3">
	{{--<div class="note p-sm-3">--}}
		{{--<p>--}}
			{{--Đây là phương thức thanh toán dành cho bạn để dùng có mục đích cho việc  “Viết và đăng tin” trên hệ thống hoặc một số dịch vụ nâng cao khác.--}}
		{{--</p>--}}
		{{--<div class="d-sm-flex justify-content-sm-between">--}}
			{{--<a href="" title="">Xem thêm</a>--}}
			{{--<a href="" title="">Lịch sử giao dịch</a>--}}
		{{--</div>--}}
	{{--</div>--}}
	<!-- end note -->
	<div class="total-money-payment row my-4">
		<div class="col-md-5">
			<h6>Tài khoản của bạn hiện tại:</h6>
		</div>
		<div class="col-md-7">
			<div class="number-money bg-green w-100">
				{{$user->moneyFormat}}
			</div>
		</div>
	</div>
	<!-- end  total-money-payment -->
	<div class="pillow-money">
		<p>Chọn 1 gói nạp</p>
		<ul class="list-unstyled clearfix mb-3">
			@foreach($lstPackage as $package)
				<li data-route="{{route('payment.step2',['package'=>$package->id])}}">Nạp {{$package->name}} </li>
			@endforeach
		</ul>
			{{--<p>--}}
				{{--Bạn có mã khuyến mãi ? <a class="btn-apply-code" href="" title="">Áp dụng</a>--}}
			{{--</p>--}}
			{{--<form class="form-promotion-code  has-danger mb-3">--}}
				{{--<div class="form-group d-flex align-items-stretch mb-0">--}}
					{{--<input type="text" class="form-control" name="" value="" placeholder="Nhập mã khuyến mãi">--}}
					 {{--<button type="submit"><i class="ion-android-checkbox-outline"></i> Áp dụng</button>--}}
				{{--</div>--}}
				{{--<div class="form-control-feedback">Mã giảm giá không tồn tại. Vui lòng nhập lại mã giảm giá. </div>--}}
			{{--</form>--}}
			<!-- end promotion code -->
	</div>
	<!-- end pillow-money -->
	<a class="btn-next btn btn-primary mb-3 text-uppercase font-weight-bold" href="" title="">Tiếp</a>
</div>
<!-- end content -->