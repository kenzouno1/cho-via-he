<!-- end header -->
<div class="content-payment bg-white p-3">
    <div class="info-total-payment mb-lg-4">
        <p class="title">Tổng số tiền cần thanh toán:</p>
        <div class="row text-lg-center">
            <div class="col-lg-4 mb-2 mb-lg-0">
                <span>Tiền nạp thêm: {{$package->moneyFormat}}  </span>
            </div>
            <div class="col-lg-4 mb-2 mb-lg-0">
                <span>Khuyến mãi: 0 </span>
            </div>
            <div class="col-lg-4 mb-2 mb-lg-0">
                <span>Số tiền phải trả: {{$package->priceFormat}}</span>
            </div>
        </div>
    </div>
    <!-- end  info total payment -->
    <div class="total-money-payment row mb-lg-4">
        <div class="col-lg-4 mb-3 mb-lg-0">
            <h6>Tổng số tiền nhận được:</h6>
        </div>
        <div class="col-lg-8 mb-3 mb-lg-0">
            <div class="number-money number-money-style-1 w-100">
                {{$package->moneyFormat}}
            </div>
        </div>
    </div>
    <!-- end  total-money-payment -->
    <div class="method-payment row">
        <div class="col-lg-4 mb-3 mb-lg-0">
            <h6>Chọn hình thức thanh toán</h6>
        </div>
        <div class="col-lg-8">
            <ul class="select-method-payment list-unstyled">
                <li class="active">
                    <span><i class="ion-round"></i>Thẻ ngân hàng nội địa</span>
                    <ul class="list-banks-payment list-option-payment list-unstyled clearfix my-3"
                        style="display: block">
                        {!! $localCardImg !!}
                    </ul>
                    <!-- end list-banks -->
                </li>
                <li>
                    <span><i class="ion-round"></i>Chuyển khoản InternetBanking</span>
                    <ul class="list-banks-payment list-option-payment list-unstyled clearfix my-3">
                        {!! $internetBankingImg !!}
                    </ul>
                    <!-- end list-banks -->
                </li>
                <li>
                    <span><i class="ion-round"></i>Thanh toán trực tuyến bằng thẻ quốc tế</span>
                    <ul class="list-banks-payment list-option-payment list-unstyled clearfix my-3">
                        {!! $creditCardImg !!}
                    </ul>
                    <!-- end list-banks -->
                </li>
                @if($package->showMobile())
                    <li>
                        <span><i class="ion-round"></i> Thẻ cào  (dành cho dưới 500.000 VNĐ)</span>
                        @include('pages.payment.parts.mobile-card')
                    </li>
                @endif
                <p class="mt-2">
                    Bằng việc sử dụng dịch vụ tin đăng, bạn đồng ý với <a style="color: #8bc34a;" href="" title="">Điều
                        khoản sử dụng</a> của Chợ Vỉa hè.
                </p>
            </ul>
            <!-- end select method-payment -->
        </div>
    </div>
    <!-- end method-payment -->
    <form action="{{route('payment.step2.process',['package'=>$package])}}" method="POST">
        {{csrf_field()}}
        <input type="hidden" name="bank_payment_method_id" id="bank_payment_method_id">
        <button class="btn-next btn btn-primary mb-3 text-uppercase font-weight-bold">Tiếp</button>
    </form>
</div>
<!-- end content -->