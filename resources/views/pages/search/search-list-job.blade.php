<div class="search-content ">
    <div class="list-search bg-white">
        <div class="header py-3 px-2">
            Tốp tìm kiếm
        </div>
        <div class="px-2 px-sm-3 py-3">
        @foreach(range(1,8) as $item)
            @include('loop.card-job-list')
            <!-- end card-card-card-horizontal -->
            @endforeach
        </div>
    </div>
    <!-- end  list search -->
    <div class="list-search bg-white">
        <div class="header py-3 px-2">
            Tin mới đăng
        </div>
        <div class="px-2 px-sm-3 py-3">
            <div class="card-job-list card-horizontal-lg card-horizontal has-highlight mb-2">
                <div class="card-block">
                    <div class="card-block-top clear-fix  mb-sm-1">
                        <div class="card-avatar d-inline-block align-middle">
                            <img src="{{asset('assets/img/upload/avata3.png')}}" alt="">
                        </div>
                        <div class="card-header">
                            <h6 class="card-title text-truncate mb-1"><a href="" title="">Tuyển nhân viên kế toán có kinh nghiệm 2 năm</a></h6>
                            <span class="card-price font-weight-bold pb-md-1 hidden-xs-down">Mức lương 4.000.000 - 6.000.000 vnđ</span>
                        </div>
                        <div class="date-address">
                            <p class="card-address m-0">Vinh, Nghệ An</p>
                            <time class="hidden-xs-down"><span>16:00</span> 20/10/2017</time>
                        </div>
                    </div>
                    <span class="card-price text-uppercase font-weight-bold pb-md-1 hidden-sm-up">200.000.000 vnđ</span>
                    <div class="card-description">
                        <p class="name-company font-weight-bold">
                            Công ty cổ phần thiết bị y tế Hoàng Thông
                        </p>
                        <div class="detail-address">
                            <i class="ion-ios-location"></i> Số 290 Bạch Đằng , P8, Bình Thạnh, Hồ Chí Minh, Việt Nam
                        </div>
                        <p class="hidden-sm-down">
                            Cheap & Reliable Dealer in Perth A good example of this popular colt featuring constant variable transmission for a smooth ride, 1.5 litre engine for economy, nice condition throughout with a handy hatch, goodfeaturing constant variable transmission for a smooth ride, 1.5 litre  tyres on alloy rims, cold air conditio quite a
                        </p>
                        <a class="add-favorite" href="#">
                            <i class="ion-ios-star"></i>
                        </a>
                    </div>
                </div>
            </div>
            <!-- end card-card-card-horizontal -->
            <div class="card-job-list card-horizontal-lg card-horizontal mb-2 has-mockup">
                <div class="card-block">
                    <div class="card-block-top clear-fix  mb-sm-1">
                        <div class="card-avatar d-inline-block align-middle">
                            <img src="{{asset('assets/img/upload/avata3.png')}}" alt="">
                        </div>
                        <div class="card-header">
                            <h6 class="card-title text-truncate mb-1"><a href="" title="">Tuyển nhân viên kế toán có kinh nghiệm 2 năm</a></h6>
                            <span class="card-price font-weight-bold pb-md-1 hidden-xs-down">Mức lương 4.000.000 - 6.000.000 vnđ</span>
                        </div>
                        <div class="date-address">
                            <p class="card-address m-0">Vinh, Nghệ An</p>
                            <time class="hidden-xs-down"><span>16:00</span> 20/10/2017</time>
                        </div>
                    </div>
                    <span class="card-price text-uppercase font-weight-bold pb-md-1 hidden-sm-up">200.000.000 vnđ</span>
                    <div class="card-description">
                        <p class="name-company font-weight-bold">
                            Công ty cổ phần thiết bị y tế Hoàng Thông
                        </p>
                        <div class="detail-address">
                            <i class="ion-ios-location"></i> Số 290 Bạch Đằng , P8, Bình Thạnh, Hồ Chí Minh, Việt Nam
                        </div>
                        <p class="hidden-sm-down">
                            Cheap & Reliable Dealer in Perth A good example of this popular colt featuring constant variable transmission for a smooth ride, 1.5 litre engine for economy, nice condition throughout with a handy hatch, goodfeaturing constant variable transmission for a smooth ride, 1.5 litre  tyres on alloy rims, cold air conditio quite a
                        </p>
                        <a class="add-favorite" href="#">
                            <i class="ion-ios-star"></i>
                        </a>
                    </div>
                </div>
            </div>
            <!-- end card-card-card-horizontal -->
        @foreach(range(1,8) as $item)
            @include('loop.card-job-list')
            <!-- end card-card-card-horizontal -->
            @endforeach
        </div>
    </div>
    <!-- end  list search -->
        @include('template-part.pagination')
    <!-- end pagination -->
    <!-- user-activity -->
        @include('template-part.user-activity')
    <!-- end user-activity -->
</div>