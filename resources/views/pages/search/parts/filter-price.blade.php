@php
    $config_price = config('conf.price_search')[$template];
@endphp
<input type="hidden" value="">
<div class="price-refine module-refine">
    <a class="title" data-toggle="collapse" href="#price-filter">
        <i class="ion-arrow-up-b"></i>
        Giá
    </a>
    <div class="collapse in" id="price-filter">
        <div class="form-group d-flex flex-row ml-2">
            <div class="w-50 pr-2">
                <select class="form-control" name="{{$template == 'job' ? 'muc-luong-min' : 'price-min'}}">
                    <option value="">Lớn hơn</option>
                    @foreach($config_price as $number => $val )
                        <option {{selected(Request::get($template == 'job' ? 'muc-luong-min' : 'price-min'),$number)}} value="{{$number}}">{{$val}}</option>
                    @endforeach
                </select>
            </div>
            <div class="w-50 pl-2">
                <select class="form-control" name="{{$template == 'job' ? 'muc-luong-max' : 'price-max'}}">
                    <option value="">Bé hơn</option>
                    @foreach($config_price as $number => $val)
                        <option {{selected(Request::get($template == 'job' ? 'muc-luong-max' : 'price-max'),$number)}} value="{{$number}}">{{$val}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>

