{{--{{$cateId}}--}}

<div class="refine-search refine-filter bg-graylight mb-4">
    <div class="refine-search-header">
        <h5>Lọc và tùy chỉnh</h5>
        <button type="button" class="close">
            <i class="ion-close-round"></i>
        </button>
    </div>
    {{--end refine search header--}}
    <div class="refine-content">
    @include('pages.search.parts.filter-cate')
    @include('pages.search.parts.filter-type-post')
    @include('pages.search.parts.filter-feature')
    <!-- digital-refine -->
    @include('pages.search.parts.filter-price')
    <!-- digital-refine -->
    @include('pages.search.parts.filter-price-type')

    <!-- digital-refine -->
    </div>
</div>