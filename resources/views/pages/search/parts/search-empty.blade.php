<div class="search-no-result list-search bg-white">
    <div class="header py-3 px-2 px-sm-3">
        Kết quả tìm kiếm
    </div>
    <div class="px-2 px-sm-3 py-5 clearfix text-center">
        <img class="d-block m-auto pb-4" src="{{asset('assets/img/icon/NoResultSearch.png')}}" alt="Không có kết quả">
        <p class="mb-1">Xin lỗi chúng tôi không tìm thấy kết quả nào cho {{Request::get('tu-khoa')}} trong danh sách</p>
        <small>Bạn có thể viết lại từ khoá hoặc vào danh mục tìm kiếm mặc định theo chủ đề trên thanh “tìm kiếm”</small>
    </div>
</div>
{{--<!-- end no-result -->--}}
