{{--{{$cateId}}--}}
    <div class="refine-search refine-filter bg-graylight mb-4">
        <div class="refine-content">
        @include('pages.search.parts.filter-cate')
        <!-- end refine-cate -->
        @include('pages.search.parts.filter-type-post')
        @include('pages.search.parts.filter-feature')
        <!-- digital-refine -->
        @include('pages.search.parts.filter-price')
        <!-- digital-refine -->
        @include('pages.search.parts.filter-price-type')

        <!-- digital-refine -->
            @if(count($meta)>0)
                <div class="digital-refine module-refine">
                    <a class="title" data-toggle="collapse" href="#digital-filter">
                        <i class="ion-arrow-down-b"></i>
                        Thông tin chi tiết
                    </a>
                    <div class="collapse" id="digital-filter">
                        <ul>
                            @foreach($meta as $key => $item)
                                <div class="option-parameter">
                                    <div class="option-parameter-content">
                                        <select class="form-control" name="{{$key}}">
                                            <option value="">{{config('job.title.'.$key)}}</option>
                                            @foreach ($item->unique('meta_value')->sortBy('meta_value') as $value)
                                                <option value="{{$value->meta_value}}">{{$value->meta_value}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endforeach
                        </ul>
                    </div>
                </div>
        @endif
            <!-- digital-refine -->
        </div>
    </div>