<div class="condition-refine module-refine">
    <a class="title" data-toggle="collapse" href="#condition-filter">
        <i class="ion-arrow-up-b"></i>
        {{$template == 'bds' ? 'Loại Bất Động sản' : 'Loại tin'}}
    </a>
    <div class="collapse in" id="condition-filter">
        <ul>
            @if($template == 'bds')
                <li class="{{UrlHelper::activeUrl(['post-type'=>'buysell'])}}"><a
                            href="{{UrlHelper::getUrl(['post-type'=>'buysell'])}}"
                            title="">Bất động sản mua bán</a></li>
                <li class="{{UrlHelper::activeUrl(['post-type'=>'rent'])}}"><a
                            href="{{UrlHelper::getUrl(['post-type'=>'rent'])}}"
                            title="">Bất động sản cho thuê</a></li>
                <li class="{{UrlHelper::activeUrl(['post-type'=>'new'])}}"><a
                            href="{{UrlHelper::getUrl(['post-type'=>'new'])}}"
                            title="">Dự án mới</a></li>
            @else
                <li class="{{UrlHelper::activeUrl(['post-type'=>'sell'])}}"><a
                            href="{{UrlHelper::getUrl(['post-type'=>'sell'])}}"
                            title="">{{$template == 'job' ? 'Người tìm việc' : 'Cần bán'}}</a></li>
                <li class="{{UrlHelper::activeUrl(['post-type'=>'buy'])}}"><a
                            href="{{UrlHelper::getUrl(['post-type'=>'buy'])}}"
                            title="">{{$template == 'job' ? 'Việc tìm người' : 'Cần mua'}}</a></li>
            @endif
        </ul>
    </div>
</div>
<!-- digital-refine -->