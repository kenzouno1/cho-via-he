<div class="price-type-refine module-refine">
    <a class="title" data-toggle="collapse" href="#price-type-filter">
        <i class="ion-arrow-up-b"></i>
        Loại Giá
    </a>

    <div class="collapse in" id="price-type-filter">
        <ul>
            <li class="{{UrlHelper::activeUrl(['price-type'=>'fixed'])}}"><a href="{{UrlHelper::getUrl(['price-type'=>'fixed'])}}" title="">Đúng giá</a> </li>
            <li class="{{UrlHelper::activeUrl(['price-type'=>'treat'])}}"><a href="{{UrlHelper::getUrl(['price-type'=>'treat'])}}" title="">Thương lượng</a></li>
            <li class="{{UrlHelper::activeUrl(['price-type'=>'trade'])}}"><a href="{{UrlHelper::getUrl(['price-type'=>'trade'])}}" title="">Trao đổi/Thương mại</a></li>
        </ul>
    </div>
</div>