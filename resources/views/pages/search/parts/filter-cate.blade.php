<div class="cate-refine module-refine">
    <a class="title" data-toggle="collapse" href="#cate-filter">
        <i class="ion-arrow-up-b"></i>
        Danh mục
    </a>
    <div class="collapse in" id="cate-filter">
        <ul>
            @foreach($listCate as $key=> $cate)
                <li class="{{$cate['active'] ? 'active' :'' }}"><a href="{{$cate['url']}}"
                                                                   title="{{$cate['title']}}">{{$cate['title']}}</a>
                    @if(isset($cate['child']))
                        <ul>
                            @foreach($cate['child'] as $child)
                                <li class="{{$child['active'] ? 'active' :'' }}"><a href="{{$child['url']}}"
                                       title="{{$child['title']}}">{{$child['title']}}</a>
                                    @if(isset($child['child']))
                                        <ul>
                                            @foreach($child['child'] as $child)
                                                <li class="{{$child['active'] ? 'active' :'' }}"><a
                                                            href="{{$child['url']}}"
                                                            title="{{$child['title']}}">{{$child['title']}}</a>
                                            @endforeach
                                        </ul>
                                @endif
                            @endforeach
                        </ul>
                    @endif
                </li>
            @endforeach
        </ul>
    </div>
</div>
<!-- end refine-cate -->