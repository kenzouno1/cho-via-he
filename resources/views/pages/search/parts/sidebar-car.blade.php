 <div class="refine-search refine-filter bg-graylight mb-4">
        <div class="refine-content">
        @include('pages.search.parts.filter-cate')
        <!-- end refine-cate -->
        @include('pages.search.parts.filter-type-post')
        @include('pages.search.parts.filter-feature')
        <!-- digital-refine -->
        @include('pages.search.parts.filter-price')
        <!-- digital-refine -->
        @include('pages.search.parts.filter-price-type')

        <!-- digital-refine -->

            @if(count($meta)>0)
                <div class="digital-refine module-refine">
                    <a class="title" data-toggle="collapse" href="#digital-filter">
                        <i class="ion-arrow-down-b"></i>
                        Thông số kỹ thuật
                    </a>
                    <div class="collapse" id="digital-filter">
                        <ul>
                            @foreach($meta as $key => $item)
                                <div class="option-parameter">
                                    <div class="option-parameter-content">
                                        @if($key != 'hopso')
                                            <select class="form-control" name="{{$key}}">
                                                <option value="">{{config('car.title.'.$key)}}</option>
                                                @foreach ($item->unique('meta_value')->sortBy('meta_value') as $value)
                                                    <option {{selected(Request::get("$key"),$value->meta_value)}} value="{{$value->meta_value}}">{{$value->meta_value}}</option>
                                                @endforeach
                                            </select>
                                        @else
                                            <span class="option-parameter-title">
                                                {{config('car.title.'.$key)}}
                                            </span>
                                            <div>
                                                <a class="{{UrlHelper::activeUrl([$key=>'all'])}}"
                                                   href="{{UrlHelper::getUrl([$key=>'all'])}}">Cả hai</a>
                                                <a class="{{UrlHelper::activeUrl([$key=>'automatic'])}}"
                                                   href="{{UrlHelper::getUrl([$key=>'automatic'])}}">Số Tự động</a>
                                                <a class="{{UrlHelper::activeUrl([$key=>'manual'])}}"
                                                   href="{{UrlHelper::getUrl([$key=>'manual'])}}">Số sàn</a>
                                            </div>
                                        @endif

                                    </div>
                                </div>
                            @endforeach
                            <div class="option-parameter">
                                <a class="option-parameter-title">
                                    Tình Trạng
                                </a>
                                <div class="option-parameter-content">
                                    <a class="{{UrlHelper::activeUrl(['trangthai'=>'new'])}}"
                                       href="{{UrlHelper::getUrl(['trangthai'=>'new'])}}">Mới</a>
                                    <a class="{{UrlHelper::activeUrl(['trangthai'=>'old'])}}"
                                       href="{{UrlHelper::getUrl(['trangthai'=>'old'])}}">Cũ</a>
                                </div>
                            </div>
                        </ul>
                    </div>
                </div>
        @endif
        <!-- digital-refine -->
        </div>
    </div>
