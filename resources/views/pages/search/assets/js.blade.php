@javascript('lstAdd',$listMap)
@javascript('maplocation',$location )
<script type="text/javascript">
    var map_data = {
//        "address": "London, V\u01b0\u01a1ng Qu\u1ed1c Anh",
        "center": maplocation['center'],
        "zoom": maplocation['zoom'],
        "scrollwheel": "false",
        "ui": "true",
        "css_class": "gmap"
    };
</script>
<script src="{{ asset('assets/vendor/CustomGoogleMapMarker/CustomGoogleMapMarker.js') }}" async></script>
<script src="{{ asset('assets/vendor/sticky-kit/jquery.sticky-kit.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">
    $('.module-refine .title ').click(function (event) {
        if ($(this).children('i').hasClass('ion-arrow-up-b')) {
            $(this).children('i').removeClass('ion-arrow-up-b').addClass('ion-arrow-down-b');
        } else {
            $(this).children('i').removeClass('ion-arrow-down-b').addClass('ion-arrow-up-b');
        }
    });
    //        stick kit map on desktop
    if ($(window).width() > 991) {
        $(".custom-gmap").stick_in_parent();
        //transform content search

    }
        $('.show-map').on('click', function (event) {
            event.preventDefault();
            if ($(window).width() > 991) {
                $(this).toggleClass('show');
                $('.card-horizontal-lg').toggleClass('card-compact');
            }
            var latlgn = map_data.center.split(',');
            $('.section-content-search').toggleClass('active');
            google.maps.event.trigger(map, 'resize');
            map.setCenter(new google.maps.LatLng(latlgn[0], latlgn[1]));
        });

    $('.close-map').on('click', function (event) {
        event.preventDefault();
        $('.section-content-search').removeClass('active');
    });

    // refine-search
    if ($(window).width() < 992) {
        $('.refine-header').on('click', function (event) {
            event.preventDefault();
            $('.refine-search').addClass('refine-search-show');
            $('body').addClass('stop-scrolling');
        });
        $('.refine-search-header .close').on('click', function (event) {
            event.preventDefault();
            $('.refine-search').removeClass('refine-search-show');
            $('body').removeClass('stop-scrolling');
        });
    }
    $('.sidebar-search').on('change', 'select', function () {
        $(this).closest('form').submit();
    });
</script>
