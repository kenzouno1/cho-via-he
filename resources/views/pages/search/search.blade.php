@extends('master')
@section('content-page')
    <!-- popup search  -->
    <main class="search-page wrapper-main" role="main">
        <!-- wrapper-banner -->
    @include('template-part.banner-page-content')
    <!-- end wrapper-banner -->
        <section class="section-content-search section-content bg-graylight">
            <div class="box-content-search container bg-graylight">
                <div class="banner-search-page">
                    <div class="row">
                        <div class="col-md-3 col-6">
                            <div class="refine-header title-style1">
                                Lọc và tuỳ chỉnh
                            </div>
                        </div>
                        <div class="col-md-9 col-6">
                            <div class="title-search-page title-style1 py-3  ">
                                {{--@if(Request::filled('search'))--}}
                                <p class="hidden-sm-down mb-0">Kết quả tìm kiếm cho
                                    <span>{{Request::get('search')}}</span> được {{$listAdsNew->total()}} bài đăng
                                    {{--@endif--}}
                                </p>
                                <ul class="select-display">
                                    <li class="{{$view=='grid'?'active':''}}">
                                        <a href="{{UrlHelper::getUrl(['view' => 'grid'])}}" title="">
                                            <i class="ion-android-apps"></i>
                                        </a>
                                    </li>
                                    <li class="{{$view=='list'?'active':''}}">
                                        <a  href="{{UrlHelper::getUrl(['view' => 'list'])}}" title="">
                                            <i  class="ion-android-menu"></i>
                                        </a>
                                    </li>
                                    <li class="show-map">
                                        <a href="#" title="">
                                            <i class="ion-android-pin"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 sidebar-search">
                        <form id="filter-search" action="">
                            @include("pages.search.parts.sidebar-$template")
                        </form>
                    </div>
                    <div class="col-lg-9">
                        <div class="search-content ">
                            @if(count($listTopSearch) > 0)
                                <div class="list-search bg-white">
                                    <div class="header py-3 px-2 px-sm-3">
                                        Tốp tìm kiếm
                                    </div>
                                    <div class="px-2 px-sm-3 py-3 clearfix row-card">
                                        @foreach($listTopSearch as $ads)
                                            @include("loop.search-$view")
                                        @endforeach
                                    </div>
                                </div>
                        @endif
                        <!-- end  list search -->
                            <div class="list-search bg-white">
                                @if(count($listAdsNew) > 0)
                                    <div class="header py-3 px-2 px-sm-3 ">
                                        Tin mới đăng
                                    </div>
                                    <div class="px-2 px-sm-3 py-3 clearfix row-card">
                                    @foreach($listAdsNew as $ads)
                                        <!-- end card-card-card-horizontal -->
                                            @include("loop.search-$view")
                                        @endforeach
                                    </div>
                                @else
                                    @include('pages.search.parts.search-empty')
                                @endif
                            </div>
                            <!-- end  list search -->
                        @include('template-part.pagination')
                        <!-- end pagination -->
                            <!-- user-activity -->
                        @include('template-part.user-activity')
                        <!-- end user-activity -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="custom-gmap box-gmap">
                <div id="gmap"></div>
                <div class="close-map"><i class="ion-close-round"></i></div>
                {{--close maps--}}
            </div>
            <!-- end custom-gmap -->


        </section>
        <!-- end section contnet search -->
    </main>
@endsection

@push('script-custom')
    @include('pages.search.assets.js')
@endpush
