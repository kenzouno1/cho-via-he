<script src="{{asset('assets/vendor/slick/slick.min.js')}}"></script>
<script>
    loadLocation();
    function loadLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var geocoder = new google.maps.Geocoder;
                geocoder.geocode({'location': pos}, function (results, status) {
                    if (status === 'OK') {
                        if (results[0]) {
                            $('#search-address').val(results[0].formatted_address);
                            $('#search-location').val(pos.lat + '-' + pos.lng);
                        } else {
                            console.log('No results found');
                        }
                    } else {
                        console.log('Geocoder failed due to: ' + status);
                    }
                });
            });
        } else {
            console.log("Geolocation is not supported by this browser.");
        }
    }
    // slider categories favorite
    $('.carousel-favorite').each(function (index, el) {
        var number_items = $(this).data('items');
        $(this).slick({
            infinite: true,
            adaptiveHeight: true,
            speed: 300,
            slidesToShow: number_items,
            slidesToScroll: number_items,
            prevArrow: "<a class='slick-prev' href='' title=''><i class='ion-ios-arrow-left'></i></a>",
            nextArrow: "<a class='slick-next' href='' title=''><i class='ion-ios-arrow-right'></i></a>",
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: number_items - 2,
                        slidesToScroll: number_items - 2,
                        infinite: true,
                    }
                },
            ]
        });
    });
    $('.tabs-favorite .nav-item a').on('shown.bs.tab', function (e) {
        $('.carousel-favorite').slick('setPosition', $('.carousel-favorite').slick('slickCurrentSlide'));
    });
    //custom background below footer
    $(window).on('scroll', function (event) {
        event.preventDefault();
        if (isScrolledIntoView('.background-building .person') && !$('.background-building .person').hasClass('start') && !$('.background-building .person').hasClass('repeat')) {
            $('.background-building .person').addClass('start');
            $('.background-building .person').trigger('start_animation');
        }
    });
    $('.background-building .person').bind('start_animation', function (event) {
        $('.background-building .person.start').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
            $('.background-building .person').removeClass('start').addClass('repeat');
        });
    });

    function isScrolledIntoView(elem) {
        var docViewTop = $(window).scrollTop();
        var elemTop = $(elem).offset().top;
        return ((elemTop >= docViewTop));
    }
    $(window).bind('favorites.update',function(){
        $.ajax({
            url: cvh_data.favorites_url,
            type: 'GET',
            data:{
                style:'card'
            }
        }).done(function (response) {
            if (response.status == 200) {
              $('#quan-tam').html(response.html);
            }
        })
    });
    if ($(window).width() < 980) {
        // scroll tab favorite mobile
       $(".tabs-favorite .tab-pane").mCustomScrollbar({
           axis: "x",
           theme: "minimal",
           advanced: {autoExpandHorizontalScroll: true}
       });
    };
</script>