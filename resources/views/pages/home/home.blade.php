@extends('master')
@section('content-page')
	<!-- popup search -->
	<main class="wrapper-main" role="main"  >
		<!-- Wrapper-banner -->
		@include('pages.home.banner-home')
		<!-- End Wrapper-banner -->
		
	   @include('template-part.section-post-ad-mobi')
	    <!-- end section-post-ad-mobi" -->

	    @include('template-part.section-favorite')
	    <!-- end section favorite -->

	    @include('template-part.featured-categories')
	    <!-- end section fravorite -->

	    @include('pages.home.below-footer')
	</main>
@endsection
@push('css-custom')
	@include('pages.home.assets.css')
@endpush
@push('script-custom')
	@include('pages.home.assets.js')
@endpush