<div class="below-footer" style="background-image: url('{{asset('assets/img/default-image/below-footer.jpg')}}');">
	<div class="container">
		<div class="below-footer-header">
			<h1 class="text-uppercase">Chợ vỉa hè</h1>
			<p>Miễn phí, dễ dàng tìm kiếm nhiều thứ tại địa phương của bạn đang sống, và nhiều nơi khác.</p>
		</div>
	</div>
	<div class="background-building">
		<div class="person">
            <div class="person-play"></div>
        </div>
	</div>
</div>