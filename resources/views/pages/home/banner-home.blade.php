<!-- wrapper-banner -->
<div class="banner-home wrapper-banner" style="background-image: url('{{asset('assets/img/default-image/banner-home.jpg')}}');">
	<div class="container">
		<div class="main-banner">
			<div class="header-banner">
				<h2>Tìm kiếm mọi thứ bạn cần</h2>
				<p>Trăm người bán - Vạn người mua</p>
			</div>
			@include('template-part.form-search-mobile')
			<!-- input-search-mobi -->
			@include('template-part.form-search')
			<!-- wrapper-form-search -->
		</div>
	</div>
</div>
<!-- end wrapper-banner -->