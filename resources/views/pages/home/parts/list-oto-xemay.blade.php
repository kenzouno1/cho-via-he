<div class="tab-pane active" id="o-to-xe-may" role="tabpanel">
    
    @if($lstCars->isNotEmpty())
        <div class="list-post-card">
            <div class="row">
                @foreach($lstCars as $ads)
                    <div class="col-md-4 col-sm-6 col-2">
                        @include('loop.card-post')
                    </div>
                @endforeach
            </div>
        </div>
    @else
        @include('pages.home.parts.empty-post')
    @endif
        
    @if($lstCateChildOfCar)
        <div class="slider-categries carousel-favorite hidden-xs-down" data-items="7">
            @foreach($lstCateChildOfCar as $cate)
                @include('pages.home.parts.category-slide-item')
            @endforeach
        </div>
    @endif
</div>