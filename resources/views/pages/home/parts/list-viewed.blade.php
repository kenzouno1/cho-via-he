<div class="tab-pane" id="da-xem" role="tabpanel">
    @if($lstViewed)
        <div class="list-post-card">
            <div class="row">
                @foreach($lstViewed as $ads)
                    <div class="col-md-4 col-sm-6 col-2">
                        @include('loop.card-post')
                    </div>
                @endforeach
            </div>
        </div>
        @if($lstViewedCate)
            <div class="slider-categries carousel-favorite hidden-xs-down" data-items="7">
                @foreach($lstViewedCate as $cate)
                    @include('pages.home.parts.category-slide-item')
                @endforeach
            </div>
        @endif
    <!-- end slider-categries -->
    @else
        @include('parts.viewed-empty')
    @endif
</div>