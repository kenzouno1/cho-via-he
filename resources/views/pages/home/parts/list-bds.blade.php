<div class="tab-pane" id="bat-dong-san" role="tabpanel">
    @if($lstLands->isNotEmpty())
        <div class="list-post-card">
            <div class="row">
            @foreach($lstLands as $ads)
                <div class="col-md-4 col-sm-6 col-2">
                    @include('loop.card-post')
                </div>
            @endforeach
            </div>
        </div>
    @else
        @include('pages.home.parts.empty-post')
    @endif
    <div class="slider-categries carousel-favorite hidden-xs-down" data-items="7">
        @foreach($lstCateChildOfLand as $cate)
            @include('pages.home.parts.category-slide-item')
        @endforeach
    </div>
    <!-- end slider-categroies -->
</div>