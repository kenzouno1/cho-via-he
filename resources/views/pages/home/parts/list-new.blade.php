<div class="tab-pane" id="tin-moi-nhat" role="tabpanel">
    <div class="list-post-card">
        <div class="row">
            @foreach($lstNews as $ads)
                <div class="col-md-4 col-sm-6 col-2">
                    @include('loop.card-post')
                </div>
            @endforeach
        </div>
    </div>
    @if($lstNewestCate)
    <div class="slider-categries carousel-favorite hidden-xs-down" data-items="7">
        @foreach($lstNewestCate as $cate)
            @include('pages.home.parts.category-slide-item')
        @endforeach
    </div>
    @endif
<!-- end slider-categries -->
</div>