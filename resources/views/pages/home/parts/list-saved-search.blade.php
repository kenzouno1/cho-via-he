<div class="tab-pane" id="danh-sach-da-tim-kiem" role="tabpanel">
    @include('template-part.user-saved-search')
    <div class="slider-categries carousel-favorite hidden-xs-down" data-items="7">
        @foreach($lstCateChildOfLand as $cate)
            @include('pages.home.parts.category-slide-item')
        @endforeach
    </div>
    <!-- end slider-categroies -->
</div>