<script>
    (function () {
        'use strict';
        //chart
        var slideDatra = document.getElementById('slide-datra');
        var slideThoiHan = document.getElementById('slide-thoihan');
        var slideLaiSuat = document.getElementById('slide-laisuat');
        var slideSetra = document.getElementById('slide-setra');
        var setra = 0;
        ads_common.price = parseInt(ads_common.price);
        noUiSlider.create(slideSetra, {
            start: ads_common.price * 0.9,
            connect: [true, false],
            step: 1000000,
            range: {
                'min': ads_common.min_price || 1000000,
                'max': ads_common.price * 1.2
            }
        });
        noUiSlider.create(slideDatra, {
            start: ads_common.price * 0.5,
            connect: [true, false],
            step: 1000000,
            range: {
                'min': 1000000,
                'max': ads_common.price * 0.7
            }
        });
        noUiSlider.create(slideThoiHan, {
            start: 3,
            connect: [true, false],
            range: {
                'min': 1,
                'max': 30
            }
        });
        noUiSlider.create(slideLaiSuat, {
            start: 7,
            connect: [true, false],
            range: {
                'min': 0.1,
                'max': 15
            }
        });

        var ctx = document.getElementById('myChart').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: ["Đặt Cọc", "Tiền Lãi", "Còn Lại"],
                datasets: [{
                    backgroundColor: [
                        "#ffb300",
                        "#8bc34a",
                        "#95a5a6",
                    ],
                    data: [0, 0, 0]
                }]
            },
            options: {
                legend: {
                    boxWidth: 10,
                    labels: {
                        boxWidth: 15, //Width legend colorbox
                        padding: 15,
                    }
                },
                tooltips: {
                    callbacks: {
                        label: function (tooltipElement, data) {
                            var label = data.labels[tooltipElement.index] || "Other",
                                value = data.datasets[tooltipElement.datasetIndex].data[tooltipElement.index];
                            return label + ": " + moneyFormat(value) + ' đ';
                        }
                    }
                },
            }
        });


        slideLaiSuat.noUiSlider.on('update', function (values) {
            loadChart();
            $(slideLaiSuat).prev('p').find('.number-money-chart').text(values);
        });
        slideThoiHan.noUiSlider.on('update', function (values, handle, unencoded) {
            loadChart();
            $(slideThoiHan).prev('p').find('.number-money-chart').text(parseInt(unencoded));
        });
        slideDatra.noUiSlider.on('update', function (values, handle, unencoded) {
            loadChart();
            $(slideDatra).prev('p').find('.number-money-chart').text(moneyFormat(unencoded));
        });
        slideSetra.noUiSlider.on('update', function (values, handle, unencoded) {
            var min = Math.round(parseInt(unencoded)*0.1);
            min = min>1000000 ? 1000000 : min;
            slideDatra.noUiSlider.updateOptions({
                range: {
                    'min':min,
                    'max': parseInt(unencoded) * 0.9 //90%
                }
            });
            loadChart();
            $(slideSetra).prev('p').find('.number-money-chart').text(moneyFormat(unencoded));
        });
        loadChart();

        function loadChart() {
            setra = parseInt(slideSetra.noUiSlider.get());
            var datra = parseInt(slideDatra.noUiSlider.get());
            var laisuat = parseInt(slideLaiSuat.noUiSlider.get());
            var thoihan = parseInt(slideThoiHan.noUiSlider.get()) * 12;
            var vay = setra - datra;
            var trahangthang = Math.round(vay / thoihan);
            var tonglai = 0;
            var conlai = vay;
            for (var i = 0; i < thoihan; i++) {
                tonglai += Math.round(conlai * laisuat / (12 * 100));
                conlai -= trahangthang;
            }
            myChart.data.datasets[0].data = [datra, tonglai, vay];
            $('#total-payment').text(moneyFormat(Math.round(datra + tonglai + vay)));
            $('#interest-payment').text(moneyFormat(Math.round(trahangthang)));
            myChart.update();
        }

        function moneyFormat(nStr) {
            nStr += '';
            var decSeperate = '.';
            var groupSeperate = ',';
            var x = nStr.split(decSeperate);
            var x1 = x[0];
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + groupSeperate + '$2');
            }
            return x1;
        }
    })();
</script>