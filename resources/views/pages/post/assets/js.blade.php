<script src="{!! asset('assets/vendor/CustomGoogleMapMarker/CustomGoogleMapMarker.js') !!}" async></script>
<script src="{!! asset('assets/vendor/sticky-kit/jquery.sticky-kit.min.js') !!}" type="text/javascript"></script>
<script src="{!! asset('assets/vendor/fancybox/jquery.fancybox.min.js') !!}"></script>
<script src="{!! asset('assets/vendor/slick/slick.min.js') !!}"></script>
<script src="{!! asset('assets/vendor/matchHeight/jquery.matchHeight.js') !!}"></script>
<script src="{!! asset('assets/vendor/parsley/parsley.min.js') !!}"></script>
<script src="{!! asset('assets/vendor/Chart/Chart.min.js') !!}"></script>
<script src="{!! asset('assets/vendor/nouislider/nouislider.min.js') !!}"></script>
<script type="text/javascript">
    $('.user-activity .item-save-search').matchHeight();
    $('.gallery').slick({
        reInit: 'slick',
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true,
        centerMode: true,
        infinite: true,
        fade: true,
        lazyLoad: 'progressive',
        asNavFor: '.thumnail-gallery',
        prevArrow: '<a class="slick-prev" href="#" title="prev"><i class="ion-angle-double-left"></i></a>',
        nextArrow: '<a class="slick-next" href="#" title="prev"><i class="ion-angle-double-right"></i></a>'
    });
    $('.thumnail-gallery').slick({
        lazyLoad: 'progressive',
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '.gallery',
        dots: false,
        infinite: false,
        centerMode: false,
        focusOnSelect: true,
        prevArrow: '<a class="slick-prev" href="#" title="prev"><i class="ion-ios-arrow-left"></i></a>',
        nextArrow: '<a class="slick-next" href="#" title="prev"><i class="ion-ios-arrow-right"></i></a>'
    });
    $("[data-fancybox]").fancybox({
        thumbs: {
            autoStart: true
        },
        baseClass: 'fancy-thumbs-bottom',
    });

    //show form price proposal
    $('.btn-price-proposal').click(function (event) {
        event.preventDefault();
        $(this).hide();
        $('.form-price-proposal').slideDown('fast');
    })
    //show number
    $('.show-phone-number a').click(function (event) {
        event.preventDefault();
        var _that = $(this);
        $.ajax({
            url: '{{route('ads.phone')}}',
            type: 'POST',
            data: {
                slug: ads_common.slug
            }
        }).done(function (response) {
            if (response.status == 200) {
                _that.hide();
                _that.next('h6').html(response.msg).slideDown('fast');
            }
        });
    });
    $('.send-price').click(function (event) {
        event.preventDefault();
        var price = $('.quick-price').val();
        $.ajax({
            url: '{{route('member.message.send')}}',
            type: 'POST',
            data: {
                post: ads_common.id,
                msg: price,
                type:'dicker'
            }
        }).done(function (response) {
            if (response.status == 204) {
                $('.form-price-proposal').slideUp(function () {
                    $('.price-proposal-seccessful .number-price').text(price + ' đ');
                    $('.price-proposal-seccessful').show();
                    $('.form-price-proposal').hide();
                });
            }
        })
    });
    $('#send-msg').submit(function (event) {
        event.preventDefault();
        var msg = $(this).find('#msg').val();
        if ($(this).find('#msg').val()) {
            $.ajax({
                url: '{{route('member.message.send')}}',
                type: 'POST',
                data: {
                    post: ads_common.id,
                    msg: msg,
                    type:'chat'
                }
            }).done(function (response) {
                if (response.status == 204) {
                    $('#modal-send-message').modal('hide');
                    $('#modal-send-message-suscessful').modal('show');
                }
            })
        }
    })
    //custom modal
    $('#modal-send-message button[data-target="#modal-send-message-suscessful"]').click(function (event) {
        $('#modal-send-message').modal('hide');
    });

</script>
@include('pages.post.assets.report-js')
@include('pages.post.assets.loans-js')