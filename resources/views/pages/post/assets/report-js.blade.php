<script>
    $('#send-report').click(function (event) {
        $("#form-report-step2").parsley({
            errorClass: 'has-danger',
            successClass: 'has-success',
            classHandler: function (ParsleyField) {
                return ParsleyField.$element.parents('.form-group');
            },
            errorsContainer: function (ParsleyField) {
                return ParsleyField.$element.parents('.form-group');
            },
            errorsWrapper: '<span class="form-control-feedback">',
            errorTemplate: '<div></div>'
        });
        if ($(this).closest('form').parsley().isValid()) {
            event.preventDefault();
            var formData = $(this).closest('form').serialize();
            $.ajax({
                url: "{{route('report.ads')}}",
                type: 'POST',
                data: formData,
            }).done(function (response) {
                $('.modal').modal('hide');
                $('#modal-send-report-suscessful').modal('show');

            });
        }
    })
</script>