@extends('master')
@section('content-page')

<!-- popup search  -->

<!-- send message  -->
	@include('template-part.modal-send-message')

<main class="search-page wrapper-main" role="main">
	<!-- wrapper-banner -->
    @include('template-part.banner-page-content')
    <!-- end wrapper-banner -->

    <section class="section-content-post-detail section-content bg-graylight">
    	<div class="container">
    		<div class="box-post-privew p-3 p-sm-4">
		    	<p>Bạn đang xem trước bài đăng, hãy kiểm duyệt lại các thông tin trước khi đăng bài, nếu còn sai sót bạn có thể chỉnh sửa lại.</p>
		    	<a class="btn btn-primary" href="" title="">Đăng tin</a>
		    	<a class="btn btn-secondary" href="" title="">Chỉnh sửa</a>
		    </div>
    		<div class="banner-post-detail-page banner-page">
    			<div class="title-search-page title-style1 py-3  ">
    				<ul class="breadcrumbs">
					  <li><a href="#">Home</a></li>
					  <li><a href="#">Ô  tô  </a></li>
					  <li><a href="#">Hãng xe</a></li>
					  <li class=" active">Loại xe</li>
					</ul>
    			</div>
    		</div>
    		<div class="row">
				<div class="col-lg-8">
					<div class="post-detail-content bg-white p-4 mb-4">
						<header>
							<div class="row">
								<div class="col-md-9">
									<h1>1998 Holden Combo Vabc 1998 Holden Combo Vabc </h1>
									<p class="price">Giá 200.000.000 vnđ</p>
								</div>
								<div class="col-md-3">
									<a class="save-view add-favorite" href="" title=""><i class="ion-android-star"></i></a>
								</div>
							</div>
							<div class="row">
								<div class="col-md-9">
									<p class="address"><i class="ion-ios-location"></i> Số 290 Bạch Đằng , P8, BT, Hồ Chí Minh, Việt Nam (416 km)   <i class="ion-eye"></i>  8 lượt xem</p>
								</div>
								<div class="col-md-3">
									<a class="btn-share d-flex align-items-start" href="" title=""><i class="ion-android-share-alt"></i> Chia sẻ bài đăng</a>
								</div>
							</div>
						</header>
						<div class="gallery">
							 @foreach(range(1,9) as $item)
							<div class="item-image">
								<a href="{{asset('assets/img/upload/img-large.png')}}" title=""  data-lightbox="img-gallery">
									<img src="{{asset('assets/img/upload/img-large.png')}}" alt="">
								</a>
							</div>
							<!-- end item image -->
							 @endforeach
						</div>
						<!-- end gallery -->
						<div class="thumnail-gallery hidden-xs-down">
							@foreach(range(1,9) as $item)
							<div class="item-image">
								<img src="{{asset('assets/img/upload/img-thumb1.png')}}" alt="">
							</div>
							<!-- end item image -->
							@endforeach
						</div>
						<!-- end thumnail gallery -->
						<div class="row">
							<div class="col-md-9">
								<div class="descrition-post-detail">
									<h4>Mô tả:</h4>
									<ol class="list-unstyled list-inline">
										<li class="list-inline-item"><i class="ion-round"></i> Hãng Huyndai</li>
										<li class="list-inline-item"><i class="ion-round"></i> Đã chạy 5000 km</li>
										<li class="list-inline-item"><i class="ion-round"></i> Loại sô tự động</li>
										<li class="list-inline-item"><i class="ion-round"></i> Dung tích 2.4 lít</li>
									</ol>
									<h6 class="text-uppercase">Ngoại thất</h6>
									<p>
										<span style="font-weight: 500">Nắp ca-pô:</span> Là phần khung kim loại ở phía đầu xe có công dụng bảo vệ cho khoang động cơ, có thể đóng mở để bảo trì và sửa chữa các bộ phận bên trong.
									</p>
									<p>
										<span style="font-weight: 500">Lưới tản nhiệt:</span> Hầu hết ô tô đều trang bị lưới tản nhiệt ở mặt trước để bảo vệ bộ tản nhiệt và động cơ, đồng thời cho phép không khí luồn vào bên trong. Ngoài ra, lưới tản nhiệt có thể được đặt ở một số vị trí như phía trước bánh xe (để làm mát hệ thống phanh) hoặc trên phía sau xe, đối với các xe có động cơ đặt sau.
									</p>
									<p>
										<span style="font-weight: 500">Đèn pha:</span> Là thiết bị chiếu sáng thường đặt ở hai góc trái phải nối liền giữa nắp capô và mặt trước của xe. Đèn pha tạo ra luồng sáng mạnh và tập trung,
										chiếu ngang mặt đường và có khả năng chiếu sáng khoảng 100 m. Đèn pha có thể được dùng kết hợp với đèn cốt (đèn chiếu gần) trong cùng một chóa đèn, hoặc lắp bổ sung cho độ chiếu sáng tối ưu.
									</p>
									<p>
										<span style="font-weight: 500">Cản:</span> Là cấu trúc gắn liền hoặc được tích hợp vào phía trước và phía sau của ô tô để hấp thụ lực tác động khi xảy ra va chạm, góp phần giảm thiểu chấn thương cho người ngồi trong xe và hư hại ở các bộ phận khác.
									</p>
									<p>
										<span style="font-weight: 500">Kính chắn gió:</span> Là một dạng cửa sổ kính nằm ở phía trước của ô tô, không chỉ có công dụng chắn gió, bụi, mưa… vào trong xe, mà còn tham gia vào việc gia tăng độ cứng vững cho kết cấu xe và bảo vệ an toàn cho hành khách trong một số tình huống va chạm.
										<span style="font-weight: 500">Gương chiếu hậu:</span> Là gương được gắn bên góc của hai cửa trước nhằm mục đích hỗ trợ người lái nhìn thấy khu vực phía sau và hai bên của chiếc xe.
									</p>
									<h6 class="text-uppercase">Nội thất</h6>
									<p>
										<span style="font-weight: 500">Vô lăng:</span> Là một phần trong hệ thống lái được điều khiển bởi tài xế. Phần còn lại của hệ thống sẽ phản ứng với những tác động từ người lái lên vô lăng thông qua sự phối hợp giữa hai cặp cơ cấu lái bánh răng - thanh răng và trục vít - bánh vít, đồng thời có thể được hỗ trợ từ bơm thủy lực.
									</p>
								</div>
							</div>
							<div class="col-md-3">
								<div class="date-time-post-detail">
									Ngày đăng:
									10/01/2017<br>
									Ngày hết hạn:
									10/06/2017
								</div>
							</div>
						</div>
						<!-- end  -->
						@include('parts.report')
						<!-- endreport-post -->
					</div>
					<!-- end post detail -->

					<div class="post-relative bg-white ">
						<div class="module-header">
							<div class="p-sm-3  px-2 py-3">
								<h6 class="text-nowrap">Có thể một số tin bạn quan tâm</h6>
							</div>
						</div>
						<div class="content-post-relative px-3 mb-4 pb-3">
							<div class="row-card">
								@foreach(range(1,4) as $item)
								<div class="col-card-3">
									@include('loop.post-relative')
	                                <!-- end post relative -->
								</div>
								 @endforeach
							</div>
						</div>
					</div>
					<!-- end post relative -->

					<!-- user-activity -->
					@include('template-part.user-activity')
					<!-- end user-activity -->

				</div>
				<div class="col-lg-4">
					<div class="content-page-right">
						<div class="contact-person bg-white p-4 mb-4 ">
							<div class="clearfix mb-3">
								<div class="avata">
									<img src="{{asset('assets/img/default-image/avata-no.png')}}" alt="">
								</div>
								<div class="content">
									<h6>Nguyên văn A</h6>
									<span>Ngày tham gia 20/11/2017</span>
								</div>
							</div>
							<a data-toggle="modal" data-target="#modal-send-message" class="btn btn-primary text-uppercase font-weight-bold" href="" title="">Gửi tin nhắn trực tiếp</a>
							<a class="btn btn-secondary text-uppercase font-weight-bold" href="" title="">Form đề nghị tăng giá</a>
							<hr>
							<strong>Giúp bạn mua hàng an toàn:</strong>
							<ol class="list-unstyled">
								<li><i class="ion-android-arrow-dropright"></i>Không nên trả tiền trước khi nhận được hàng.</li>
								<li><i class="ion-android-arrow-dropright"></i>Kiểm tra hàng cẩn thận, nhất là hàng đắt tiền.</li>
								<li><i class="ion-android-arrow-dropright"></i>Hẹn giao - nhận ở nơi công cộng.</li>
								<li><i class="ion-android-arrow-dropright"></i>Nếu bạn mua hàng hiệu, hãy gặp mặt tại cửa hàng để nhờ xác minh, tránh mua phải hàng giả.</li>
								<li><i class="ion-android-arrow-dropright"></i>Tìm hiểu thêm trong mục thông tin an toàn mua ... </li>
							</ol>
						</div>
						<!-- end contact-person -->
						<div class=" box-gmap hidden-sm-down">
							<div id="gmap"></div>
						</div>
						<!-- end custom-gmap -->
					</div>
				</div>
    		</div>
    	</div>
    </section>
    <!-- end section contnet search -->
</main>
@endsection
 @push('css-custom')
	 @include('pages.post.assets.css')
@endpush
@push('script-custom')
    @include('pages.post.assets.js')
</script>
@endpush
