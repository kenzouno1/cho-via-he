@extends('master')

@section('content-page')
    <!-- popup repost step 1  -->
    @include('pages.post.parts.modal-report')
    <!-- popup repost step 2  -->
    @include('pages.post.parts.modal-report-step2')
    <!-- send message  -->
    @include('pages.post.parts.modal-send-message')
    <!-- send message suscessful -->
    @include('pages.post.parts.modal-send-message-suscessful')
    <main class="search-page wrapper-main" role="main">
        <!-- wrapper-banner -->
    @include('template-part.banner-page-content')
    <!-- end wrapper-banner -->
        <section class="section-content-post-detail section-content bg-graylight">
            <div class="container bg-graylight">
                @if($ads->isStatus('deactive'))
                    @include('pages.post.parts.post-wait')
                @endif
                <div class="banner-post-detail-page banner-page">
                    <div class="title-style1 py-3  ">
                        @include('pages.post.parts.breadcumbs')
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8">
                        <div class="post-detail-content bg-white p-3 p-sm-4 mb-4" id="post-{{$ads->id}}">
                            <header>
                                <div class="row">
                                    <div class="col">
                                        <h1 class="text-capitalize">{{$ads->title}} </h1>
                                        <div class="price">
                                            <strong class="font-weight-bold">{{$ads->priceFormat}}</strong>
                                            {!! $ads->MinPrice !!}
                                            {{--<span class="text-muted">(Mức giá nhỏ nhất 195.000.000vnđ)</span>--}}
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <a class="save-view add-favorite {{$ads->isFavorites() ? 'active' :''}}"
                                           data-id="{{$ads->id}}" href="#" title="Thêm vào mục quan tâm"><i
                                                    class="ion-android-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-9 col-md-8">
                                        <p class="address"><i class="ion-ios-location"></i> {{$ads->address}} <i
                                                    class="ion-eye"></i> {{$ads->view_count}} lượt xem</p>
                                    </div>
                                    <div class="col-lg-3 col-md-4">
                                        @include('parts.share')
                                    </div>
                                </div>
                            </header>
                            @if(collect($ads->images)->isNotEmpty())
                            <div class="gallery">
                                @foreach($thumbs as $img)
                                    <div class="item-image">
                                        <a href="{{$img['medium']}}" title="{{$ads->title}}" data-fancybox="gallery">
                                            <img data-lazy="{{$img['medium']}}" alt="{{$ads->title}}">
                                        </a>
                                    </div>
                                    <!-- end item image -->
                                @endforeach
                            </div>
                            <!-- end gallery -->
                            <div class="thumnail-gallery hidden-xs-down">
                                @foreach($thumbs as $img)
                                    <div class="item-image">
                                        <img data-lazy="{{$img['small']}}" alt="{{$ads->title}}">
                                    </div>
                                    <!-- end item image -->
                                @endforeach
                            </div>
                            @endif
                            <div class="box-description">
                                <div class="descrition-post-detail">
                                    <h4>Mô tả:</h4>
                                    @php
                                    if($ads->author->isMod()){
                                        $countMeta = 3;
                                    }else{
                                        $countMeta = 0;
                                    }
                                    @endphp
                                    @if(count($ads->metas)>$countMeta)
                                        <ol class="list-unstyled list-inline px-3 pb-0 pt-3">
                                            @foreach($ads->metas as $meta)
                                                @if(config("$template.title.$meta->meta_key"))
                                                    <li class="list-inline-item"><i
                                                                class="ion-round"></i> {{config("$template.title.$meta->meta_key")}}
                                                        : {{$meta->meta_value}}</li>
                                                @endif
                                            @endforeach
                                        </ol>
                                    @endif
                                    {!! $ads->description !!}
                                </div>
                            </div>
                            <!-- end  -->
                            @include('parts.report')
                        </div>
                        @desktop
                        <!-- end post detail -->
                    @include('pages.post.parts.post-relative')
                    <!-- end post relative -->
                    @include('template-part.user-activity')
                    <!-- end user-activity -->
                        @enddesktop

                    </div>
                    <div class="col-lg-4">
                        @include('pages.post.parts.sidebar')
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8">
                        @handheld
                    @include('pages.post.parts.post-relative')
                    <!-- end post relative -->

                    @include('template-part.user-activity')
                    <!-- end user-activity -->
                        @endhandheld
                    </div>
                </div>
            </div>
        </section>
        <!-- end section contnet search -->
    </main>
@endsection
@push('css-custom')
    @include('pages.post.assets.css')
@endpush
@push('script-custom')
    @include('pages.post.assets.js')
@endpush
@push('common')
    @javascript('lstAdd',$lstAdd)
    <script>
        var map_data = {
            "center": '{{$ads->getCenter()}}',
            "zoom": 17,
            "scrollwheel": false,
            "ui": false,
        };
        var ads_common ={!!$ads->toCommon()!!};
    </script>
@endpush