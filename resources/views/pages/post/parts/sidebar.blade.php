<div class="content-page-right">
    <div class="contact-person bg-white p-3 p-sm-4 mb-4 ">
        <div class="clearfix mb-3">
            <div class="avata">
                <img src="{{$ads->author->getAvatar()}}" alt="{{$ads->author->name}}">
            </div>
            <div class="content">
                @if($ads->author->isMod())
                    <h6 class="text-truncate"><a
                                title="Xem thông tin {{$ads->get_meta('mod-fullname')}}">{{$ads->get_meta('mod-fullname')}}</a>
                    </h6>
                @else
                    <h6 class="text-truncate"><a
                                href="{{route('account-seller',['id'=>$ads->user_id])}}"
                                title="Xem thông tin {{$ads->author->name}}">{{$ads->author->name}}</a>
                    </h6>
                @endif
                <span>Ngày tham gia {{$ads->author->created_at->format('d/m/Y')}}</span>
            </div>
        </div>
        @if(!$ads->isAuthor())
            @include('pages.post.parts.send-message')
        @endif

        <hr class="hidden-sm-down">
        <strong class="hidden-sm-down">Giúp bạn mua hàng an toàn:</strong>
        <ol class="list-unstyled hidden-sm-down">
            <li><i class="ion-android-arrow-dropright"></i>Không nên trả tiền trước khi nhận
                được hàng.
            </li>
            <li><i class="ion-android-arrow-dropright"></i>Kiểm tra hàng cẩn thận, nhất là hàng
                đắt tiền.
            </li>
            <li><i class="ion-android-arrow-dropright"></i>Hẹn giao - nhận ở nơi công cộng.</li>
            <li><i class="ion-android-arrow-dropright"></i>Nếu bạn mua hàng hiệu, hãy gặp mặt
                tại cửa hàng để nhờ xác minh, tránh mua phải hàng giả.
            </li>
            <li><i class="ion-android-arrow-dropright"></i>Tìm hiểu thêm trong mục thông tin an
                toàn mua ...
            </li>
        </ol>
    </div>
    <!-- end contact-person -->
    <div class=" box-gmap hidden-sm-down">
        <div id="gmap"></div>
    </div>
    <!-- end custom-gmap -->
    @if($ads->hasInterestLoans())
        @include('pages.post.parts.money-interest')
    @endif
</div>