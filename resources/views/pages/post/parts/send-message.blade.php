@if(Auth::check())
    <a data-toggle="modal" data-target="#modal-send-message"
       class=" btn-send-message btn btn-primary text-uppercase font-weight-bold mb-3"
       href="#" title="Gửi tin">Gửi tin nhắn trực tiếp</a>
    @if($ads->canProposalPrice())
        <a class="btn-price-proposal btn btn-secondary text-uppercase font-weight-bold"
           href="#" title="Đề nghị giá">Đề nghị giá</a>
        <form class="form-price-proposal">
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">Đ</span>
                <input type="number" class="form-control quick-price" value="{{$ads->proposalPrice}}"
                       step="5000">
                <span class="input-group-btn">
                                            <button class="btn btn-secondary send-price" type="button">GỬI</button>
                                          </span>
            </div>
        </form>

        <div class="price-proposal-seccessful text-center mb-2" style="display: none">
            Bạn đã đề nghị giá <span class="number-price">1.000.000 đ</span> thành
            công<br>
            <a href="{{route('member.message')}}" title="Quản lý tin nhắn">Quản lý tin
                nhắn</a>
        </div>
    @endif
@else
    <a class=" btn-send-message btn btn-primary text-uppercase font-weight-bold"
       href="{{route('auth.login')}}?callback={{route('ads.details',['slug'=>$ads->slug])}}"
       title="Gửi tin">Gửi tin
        nhắn trực tiếp</a>
    <a class="btn btn-secondary text-uppercase font-weight-bold mb-3"
       href="{{route('auth.login')}}?callback={{route('ads.details',['slug'=>$ads->slug])}}"
       title="Đề nghị giá">Đề nghị giá</a>
@endif
<div class="show-phone-number text-center">
    <a href="#" title="Xem số điện thoại">Xem số điện thoại</a>
    <h6>0909 xxxx</h6>
</div>
{{--end show phone number--}}