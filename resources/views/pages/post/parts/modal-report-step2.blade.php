<!-- send message  -->
<div class="modal fade modal-report" id="modal-report-step2" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!-- modal-header -->
            <div class="modal-header">
                <h5 class="modal-title">Vui lòng cho biết thêm thông tin</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ion-close-round"></i>
                </button>
            </div>
            <!-- modal-body -->
            <div class="modal-body">
                <form  class="form-report-step2" id="form-report-step2">
                    <div class="form-group">
                        <label>Nội dung</label>
                        <textarea class="form-control" name="message" rows="9" required="" data-parsley-required-message="Bạn cần nhập nôi dung vi phạm"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Điền thông tin của bạn để chúng tôi liên lạc khi cần
                            thiết</label>
                        <input type="tel" name="phone" class="form-control"
                               value="{{AuthHelper::currentPhone()}}"
                               placeholder="Số điện thoại">
                        <input type="email" name="email" class="form-control"
                               value="{{AuthHelper::currentEmail()}}"
                               placeholder="Email">
                    </div>
                    <input type="hidden" id="report-reason" name="reason">
                    <input type="hidden" name="id" value="{{$ads->id}}">
                    <div class="btn-submit">
                        <button type="submit" type="button" id="send-report" class="btn btn-primary text-uppercase">Gửi</button>
                    </div>
                </form>
                <!-- end  module -->
            </div>
            <!-- end modal body -->
        </div>
    </div>
</div>
