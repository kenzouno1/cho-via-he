<div class="box-money-interest bg-white py-4 px-2 my-4">
    <div class="wrap-loans-calculator">
        <h5 class="text-uppercase mb-3 text-center">
            Tính lãi suất ngân hàng
        </h5>
        <div class="wrap-slide">
            <p><strong>Số tiền sẽ trả:</strong> <span class="number-money-chart">10</span> đ</p>
            <div id="slide-setra"></div>
        </div>
        <div class="wrap-slide">
            <p><strong>Đặt cọc:</strong> <span class="number-money-chart">10</span> đ</p>
            <div id="slide-datra"></div>
        </div>
        <div class="wrap-slide">
            <p><strong>Lãi suất:</strong> <span class="number-money-chart">10</span> %</p>
            <div id="slide-laisuat"></div>
        </div>
        <div class="wrap-slide">
            <p><strong>Thời hạn vay:</strong> <span class="number-money-chart">10</span> năm</p>
            <div id="slide-thoihan"></div>
        </div>
    </div>
    <div class="wrap-chart m-auto">
        <div class="chart-canvas">
            <canvas width="350" height="300" id="myChart"></canvas>
        </div>
    </div>
    {{--end chart--}}
    <h6 class="d-block my-3">Tiền gốc phải trả hàng tháng: <span id="interest-payment"></span> VNĐ</h6>
    <h6 class="d-block my-3 ">Tổng Số tiền phải trả: <span id="total-payment"></span> VNĐ</h6>
    <small class="d-block mt-3">(*) Chức năng này chỉ mang tính chất tham khảo</small>
</div>
{{--end  box money interest--}}