<!-- send message  -->
<div  class="modal fade modal-style-sm" id="modal-send-message" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!-- modal-header -->
            <div class="modal-header">
                <h5 class="modal-title">Gửi tin nhắn đến <span>{{'@'.$ads->author->name }}</span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <i class="ion-close-round"></i>
                </button>
            </div>
            <!-- modal-body -->
            <div class="modal-body">
	            <form id="send-msg">
	            	<div class="form-group">
					    <textarea class="form-control" id="msg" rows="9" placeholder="Nội dung" required></textarea>
					</div>
                    <div class="btn-submit">
                        <button type="submit" class="btn btn-primary text-uppercase">Gửi</button>
                    </div>
	            </form>
            </div>
            <!-- end modal body -->
        </div>
    </div>
</div>