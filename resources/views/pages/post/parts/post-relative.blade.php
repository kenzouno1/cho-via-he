@if($lstRelated->isNotEmpty())
    <div class="post-relative bg-white ">
        <div class="module-header">
            <div class="p-sm-3  px-2 py-3">
                <h6 class="text-nowrap">Có thể một số tin bạn quan tâm</h6>
            </div>
        </div>
        <div class="content-post-relative px-2 mb-4 pb-3">
            <div class="row no-gutters">
                @foreach($lstRelated as $item)
                    @if($template = 'job')
                        <div class="p-3 col-md-12">
                            <div class="card-job-list card-horizontal-lg card-horizontal mb-2 {{$ads->ads_class()}}">
                                <div class="card-block">
                                    <div class="card-block-top clear-fix  mb-sm-1">
                                        <div class="card-avatar d-inline-block align-middle">
                                            <a href="{{route('account-seller',['id'=>$ads->user_id])}}"
                                               title="{{$ads->author->name}}">
                                                <img src="{{$ads->author->getAvatar()}}" alt="{{$ads->author->name}}">
                                            </a>
                                        </div>
                                        <div class="card-header">
                                            <h6 class="card-title text-capitalize mb-1"><a href="{{$ads->getUrl()}}"
                                                                                           title="{{$ads->title}}">{{$ads->title}}</a></h6>
                                            <span class="card-price font-weight-bold pb-md-1 hidden-xs-down text-capitalize">{{$ads->priceFormat}}</span>
                                        </div>
                                        <div class="date-address">
                                            <p class="card-address m-0">{{$ads->address}}</p>
                                            <time class="hidden-xs-down">{{$ads->ago}}</time>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="col-md-4">
                            @include('loop.card-post',['ads'=>$item])
                        </div>
                    @endif
                <!-- end card -->

                @endforeach
            </div>
        </div>
    </div>
@endif