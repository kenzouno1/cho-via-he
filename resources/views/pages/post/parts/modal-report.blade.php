<div class="modal fade modal-report" id="modal-report" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!-- modal-header -->
            <div class="modal-header">
                <h5 class="modal-title">Báo cáo tin không hợp lệ</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ion-close-round"></i>
                </button>
            </div>
            <!-- modal-body -->
            <div class="modal-body">
                <ul class="list-report list-unstyled text-center">
                    @foreach(config('conf.report') as $key=>$item)
                        <li><a data-toggle="modal" data-target="#modal-report-step2" data-reason="{{$key}}" href="#">{{$item}}</a></li>
                    @endforeach
                </ul>
            </div>
            <!-- end modal body -->
        </div>
    </div>
</div>
@push('script-custom')
    <script>
        $('.list-report a').click(function (event) {
            event.preventDefault();
            var type = $(this).data('reason');
            $('#report-reason').val(type);
        })
    </script>
@endpush