@extends('master')
@section('content-page')

    <main class="search-page wrapper-main" role="main">
    <!-- end wrapper-banner -->
        <section class="section-content-post-detail section-content bg-graylight">
            <div class="container bg-graylight">
                <div class="box-post-privew p-3 p-sm-4">
                    <p>Tin của bạn đã được đăng thành công, và đang trong quá trình kiểm duyệt. </p>
                    <a class="btn btn-primary" href="" title="Quản lý bài đăng">Quản lý bài đăng</a>
                    <a class="btn btn-secondary" href="" title="Chỉnh sửa">Chỉnh sửa</a>
                </div>
                <div class="banner-post-detail-page banner-page">
                    <div class="title-style1 py-3  ">
                        @include('pages.post.parts.breadcumbs')
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8">
                        <div class="post-detail-content bg-white p-3 p-sm-4 mb-4">
                            <header>
                                <div class="row">
                                    <div class="col">
                                        <h1>{{$ads->title}} </h1>
                                        <div class="price">
                                            <strong class="font-weight-bold">Giá {{$ads->priceFormat}}</strong>
                                            {{--<span class="text-muted">(Mức giá nhỏ nhất 195.000.000vnđ)</span>--}}
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <a class="save-view add-favorite {{$ads->isFavorites() ? 'active' :''}}"
                                           data-id="{{$ads->id}}" href="#" title="Thêm vào mục quan tâm"><i class="ion-android-star"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-9 col-md-8">
                                        <p class="address"><i class="ion-ios-location"></i> {{$ads->address}} <i class="ion-eye"></i> {{$ads->view_count}} lượt xem</p>
                                    </div>
                                    <div class="col-lg-3 col-md-4">
                                        @include('parts.share')
                                    </div>
                                </div>
                            </header>
                            <div class="gallery">
                                @foreach($thumbs as $img)
                                    <div class="item-image">
                                        <a href="{{$img['medium']}}" title="{{$ads->title}}" data-fancybox="gallery">
                                            <img data-lazy="{{$img['medium']}}" alt="{{$ads->title}}">
                                        </a>
                                    </div>
                                    <!-- end item image -->
                                @endforeach
                            </div>
                            <!-- end gallery -->
                            <div class="thumnail-gallery hidden-xs-down">
                                @foreach($thumbs as $img)
                                    <div class="item-image">
                                        <img data-lazy="{{$img['small']}}" alt="{{$ads->title}}">
                                    </div>
                                    <!-- end item image -->
                                @endforeach
                            </div>
                            <div class="box-description">
                                <div class="descrition-post-detail">
                                    <h4>Mô tả:</h4>
                                    <ol class="list-unstyled list-inline px-3 pb-0 pt-3">
                                    <li class="list-inline-item"><i class="ion-round"></i>  Số lượng 2 người   </li>
                                    <li class="list-inline-item"><i class="ion-round"></i> Cấp bậc quản lý  </li>
                                    <li class="list-inline-item"><i class="ion-round"></i>2  năm kinh nghiệm   </li>
                                    <li class="list-inline-item"><i class="ion-round"></i> Trình độ đại học</li>
                                    <li class="list-inline-item"><i class="ion-round"></i> Làm việc toàn thời gian</li>
                                    </ol>
                                    {!! nl2br(e($ads->des)) !!}
                                </div>
                            </div>
                            <!-- end  -->
{{--                            @include('parts.report')--}}
                        </div>
                        <!-- end post detail -->
                    </div>
                    <div class="col-lg-4">
                        <div class="content-page-right">
                            <div class="contact-person bg-white p-3 p-sm-4 mb-4 ">
                                <div class="clearfix mb-3">
                                    <div class="avata">
                                        <img src="{{$ads->author->getAvatar()}}" alt="{{$ads->author->name}}">
                                    </div>
                                    <div class="content">
                                        <h6 class="text-truncate"><a href="#"
                                                                     title="{{$ads->author->name}}}">{{$ads->author->name}}</a>
                                        </h6>
                                        <span>Ngày tham gia {{$ads->author->created_at->format('d/m/Y')}}</span>
                                    </div>
                                </div>

                                <hr class="hidden-sm-down">
                                <strong class="hidden-sm-down">Giúp bạn mua hàng an toàn:</strong>
                                <ol class="list-unstyled hidden-sm-down">
                                    <li><i class="ion-android-arrow-dropright"></i>Không nên trả tiền trước khi nhận
                                        được hàng.
                                    </li>
                                    <lyhi><i class="ion-android-arrow-dropright"></i>Kiểm tra hàng cẩn thận, nhất là hàng
                                        đắt tiền.
                                    </lyhi>
                                    <li><i class="ion-android-arrow-dropright"></i>Hẹn giao - nhận ở nơi công cộng.</li>
                                    <li><i class="ion-android-arrow-dropright"></i>Nếu bạn mua hàng hiệu, hãy gặp mặt
                                        tại cửa hàng để nhờ xác minh, tránh mua phải hàng giả.
                                    </li>
                                    <li><i class="ion-android-arrow-dropright"></i>Tìm hiểu thêm trong mục thông tin an
                                        toàn mua ...
                                    </li>
                                </ol>
                            </div>
                            <!-- end contact-person -->
                            <div class=" box-gmap hidden-sm-down">
                                <div id="gmap"></div>
                            </div>
                            <!-- end custom-gmap -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end section contnet search -->
    </main>
@endsection
@push('css-custom')
    @include('pages.post.assets.css')
@endpush
@push('script-custom')
{{--    @include('pages.post.assets.js')--}}
@endpush
