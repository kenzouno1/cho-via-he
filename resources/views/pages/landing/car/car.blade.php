@extends('master')
@section('content-page')
    <main class="wrapper-main" role="main">
        <!-- wrapper-banner -->
        <div class="banner-page-landing wrapper-banner"
             style="background-image: url('{{asset('assets/img/default-image/banner.jpg')}}');">
            <div class="container">
                <div class="main-banner">
                    <div class=" header-banner ">
                        <p class="text1">Mua & bán</p>
                        <h2 class="text2">Ô TÔ</h2>
                    </div>
                    <!-- end header banner -->
                </div>
            </div>
        </div>
        <!-- end wrapper-banner -->

        <section class="section-content-car bg-graylight">
            <div class="container">
            @include('pages.landing.car.parts.form-search-car')
            <!-- end form search -->
                <div class="hidden-lg-up">
                @include('template-part.box-ads-blade')
                <!-- end box-img-ads -->
                </div>
                <div class="row mb-4">
                    <div class="col-lg-9">
                        <div class="group-card px-md-3 pb-1">
                            <div class="row-card">
                                @foreach($lstAds as $ads)
                                    <div class="col-card-6 mb-3">
                                        @include('loop.card-horizontal-car')
                                    </div>
                                @endforeach

                            </div>
                            <a class="click-more-post-car click-more text-center hidden-md-up" href="" title=""><i
                                        class="ion-arrow-down-b"></i> Hiện thị thêm </a>
                        </div>
                        <!-- end group card -->
                         {{--<div class="bg-gray p-3 hidden-lg-up mb-4 mb-lg-0">--}}
                            {{--<p class="title-style1 mb-2"> Tìm kiếm với các thông tin sẵn</p> --}}
                         {{--@include('template-part.form-search-car-2') --}}
                        <!-- end form-search-car -->
                        <!-- </div> -->
                        <!-- end search car mobile -->
                    </div>
                    <div class="col-lg-3">
                        <div class="hidden-md-down mb-3">
                        @include('template-part.box-ads-blade')
                        <!-- end box-img-ads -->
                        </div>
                        <!-- end box img -->
                    @include('pages.landing.parts.sider-post-ad',['sectionTitle'=>'Bán Xe Của Bạn'])
                    <!-- end sider-post-ad-->
                    @include('pages.landing.parts.sider-signin-url')
                    <!-- end sider-signin-url-->
                    @include('pages.landing.parts.sider-tag')
                    <!-- end sider-tag-->
                    </div>
                </div>
            {{--<div class="bg-gray p-3 hidden-md-down">--}}
            {{--<p class="title-style1 mb-2"> Tìm kiếm với các thông tin sẵn</p>--}}
            {{--@include('template-part.form-search-car-2')--}}
            {{--</div>--}}
            <!-- end form search car desktop -->
            </div>
        </section>
        <!-- end section -->

    @include('pages.landing.car.parts.partner')
    <!-- end partner -->

        {{--<section class="section-assessment bg-graylight ">--}}
        {{--<div class="container">--}}
        {{--<div class="title mb-3">--}}
        {{--Đánh giá các loại xe--}}
        {{--</div>--}}
        {{--<div class="content-assessment clearfix">--}}
        {{--@foreach(range(1,4) as $item)--}}
        {{--@include('loop.card-car')--}}
        {{--@endforeach--}}
        {{--</div>--}}
        {{--<!-- end content-assessment -->--}}
        {{--<div class="text-center">--}}
        {{--<a class="btn-orange btn btn-secondary m-auto font-weight-bold text-uppercase py-2 px-4" href="" title="">tất cả các bài đánh giá</a>--}}
        {{--</div>--}}
        {{--<!-- end btn -->--}}
        {{--</div>--}}
        {{--</section>--}}
    </main>
@endsection

@push('script-custom')
    @include('pages.landing.car.assets.js')
    @include('pages.landing.assets.js')
@endpush
