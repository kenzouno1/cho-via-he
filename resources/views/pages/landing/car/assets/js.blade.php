<script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_API','AIzaSyCcCGIu_Jz3R1wUtpLCtxjvNDNlueSCuTE')}}&libraries=places"
        async defer></script>
<script type="text/javascript">
    //redmore search car on mobile
    $('.click-more-option-car').click(function (event) {
        event.preventDefault();
        $(this).css('display', 'none');
        $('.form-search-style-2 .form-group:nth-child(n+5)').slideDown('fast');
    });
    //redmore car on mobile
    $('.click-more-post-car').click(function (event) {
        event.preventDefault();
        $(this).css('display', 'none');
        $('.group-card .col-card-6:nth-of-type(1n+7)').slideDown('fast');
    });
    //scroll partner and asessmet moile
    $(".section-partner .content-partner, .section-assessment .content-assessment").mCustomScrollbar({
        axis: "x",
        theme: "minimal",
        advanced: {autoExpandHorizontalScroll: true}
    });
    $('select[name="cate"]').change(function () {
        $.ajax({
            url: '{{ route('landing-page.car.model')}}',
            type: 'POST',
            data: {slug: $(this).val()},
        }).done(function (response) {
            var lstDongXe = response.data;
            $('select[name="dongxe"]').find('option').remove();
            $.each(lstDongXe, function (index, value) {
                $('select[name="dongxe"]').append('<option value="' + value + '">' + value + '</option>');
            });
        });
    });
</script>