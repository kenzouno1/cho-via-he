<form class="form-search-car form-search-style-2"  action="{{route('landing.car.search')}}" method="POST">
    {{csrf_field()}}
    <input type="hidden" name="cate" value="{{$cate->slug}}">
    <div class="row">
        <div class="form-group col-md-6">
            <input type="text" name="title" class="form-control" placeholder="Nhập 1 từ khoá ...">
        </div>
        <div class="form-group col-md-3">
            <select name="cate" class="custom-select w-100">
                <option value="{{$cate->slug}}" selected>Chọn hãng</option>
                @foreach($lstHangXe as $key => $item)
                    <option value="{{$item}}">{{$item}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-md-3">
            <select name="dongxe" class="custom-select w-100">
                <option value="" selected>Dòng xe</option>
            </select>
        </div>
        <div class="form-group col-md-3">
            <select name="price" class="custom-select w-100">
                <option value="" selected>Giá</option>
                @foreach($lstGiaxe as $key => $item)
                    <option value="{{$key}}">{{$item}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-md-3">
            <select name="year" class="custom-select w-100">
                <option value="" selected>Năm</option>
                @foreach(range(date('Y'),1930) as $year)
                    <option value="{{$year}}">{{$year}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-md-3">
            <select name="location" class="custom-select w-100">
                <option value="" selected>Tỉnh thành</option>
                @foreach($lstTinhThanh as $key=> $item)
                    <option data-latlng="{{$item['latlng']}}" data-id="{{$key}}" value="{{$item['label']}}">{{$item['label']}}</option>
                @endforeach
            </select>
            <input type="hidden" name="latlng">
        </div>
        <div class="form-group col-md-3">
            <select name="product-state" class="custom-select w-100">
                <option value="" selected>Tình trạng</option>
                <option value="new">Mới</option>
                <option value="old">Cũ</option>
            </select>
        </div>
        <a class="click-more-option-car click-more text-center hidden-md-up" href="#" title="Hiện thêm lựa chọn"><i
                    class="ion-arrow-down-b"></i> Hiện thị thêm lựa chọn tìm kiếm</a>
        <div class="form-group col-md-12">
            <button type="submit" class="btn btn-primary w-100 text-uppercase font-weight-bold">Tìm kiếm</button>
        </div>
    </div>
</form>
<!-- end form search -->
