<form class="form-search-car-2 form-search-style-2 clearfix">
	<div class="d-lg-flex flex-lg-row">
		<div class="form-group">
			<select class="custom-select w-100">
			  <option selected>Bất kỳ</option>
			  <option value="1">Bất kỳ Bất kỳBất kỳ</option>
			  <option value="2">Two</option>
			  <option value="3">Three</option>
			</select>
		</div>
		<div class="form-group">
			<select class="custom-select w-100">
			  <option selected>Model</option>
			  <option value="1">One</option>
			  <option value="2">Two</option>
			  <option value="3">Three</option>
			</select>
		</div>
		<div class="form-group">
			<select class="custom-select w-100">
			  <option selected>Năm</option>
			  <option value="1">One</option>
			  <option value="2">Two</option>
			  <option value="3">Three</option>
			</select>
		</div>
		<div class="form-group">
			<select class="custom-select w-100">
			  <option selected>Dung tích</option>
			  <option value="1">One</option>
			  <option value="2">Two</option>
			  <option value="3">Three</option>
			</select>
		</div>
		<div class="form-group">
			<select class="custom-select w-100">
			  <option selected>Xe số</option>
			  <option value="1">One</option>
			  <option value="2">Two</option>
			  <option value="3">Three</option>
			</select>
		</div>
	</div>
	<div class="form-group m-auto">
		<button type="submit" class="btn btn-primary w-100 text-uppercase font-weight-bold">Tìm kiếm</button>
	</div>
</form>