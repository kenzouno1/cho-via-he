<section class="section-partner bg-white py-4 py-md-5 text-center">
	<div class="container">
		<div class="title text-center font-weight-bold">
			Các hãng xe
		</div>
		<ul class="content-partner list-unstyled clearfix mb-3">
			<li>
				<a href="{{route('search.list',['cate'=>'audio'])}}" title="Audio">
    				<img src="{{asset('assets/img/upload/dt1.png')}}" alt="Audio">
    			</a>
			</li>
			<li>
				<a href="{{route('search.list',['cate'=>'bmw'])}}" title="BMW">
    				<img src="{{asset('assets/img/upload/dt2.png')}}" alt="BMW">
    			</a>
			</li>
			<li>
				<a href="{{route('search.list',['cate'=>'mazda'])}}" title="Mazda">
    				<img src="{{asset('assets/img/upload/dt3.png')}}" alt="Mazda">
    			</a>
			</li>
			<li>
				<a href="{{route('search.list',['cate'=>'honda'])}}" title="Honda">
    				<img src="{{asset('assets/img/upload/dt4.png')}}" alt="Honda">
    			</a>
			</li>
			<li>
				<a href="{{route('search.list',['cate'=>'hyundai'])}}" title="hyundai">
    				<img src="{{asset('assets/img/upload/dt5.png')}}" alt="hyundai">
    			</a>
			</li>
			<li>
				<a href="{{route('search.list',['cate'=>'mitsubishi'])}}" title="mitsubishi">
    				<img src="{{asset('assets/img/upload/dt6.png')}}" alt="mitsubishi">
    			</a>
			</li>
			<li>
				<a href="{{route('search.list',['cate'=>'nisan'])}}" title="nisan">
    				<img src="{{asset('assets/img/upload/dt7.png')}}" alt="nisan">
    			</a>
			</li>
			<li>
				<a href="{{route('search.list',['cate'=>'toyota'])}}" title="toyota">
    				<img src="{{asset('assets/img/upload/dt8.png')}}" alt="toyota">
    			</a>
			</li>
			<li>
				<a href="{{route('search.list',['cate'=>'suzuki'])}}" title="suzuki">
    				<img src="{{asset('assets/img/upload/dt9.png')}}" alt="suzuki">
    			</a>
			</li>
			<li>
				<a href="{{route('search.list',['cate'=>'ford'])}}" title="Ford">
    				<img src="{{asset('assets/img/upload/dt10.png')}}" alt="Ford">
    			</a>
			</li>
		</ul>
		{{--<a class="btn-orange btn btn-secondary m-auto font-weight-bold text-uppercase py-2 px-4" href="" title="">xem tất cả  các hãng</a>--}}
	</div>
</section>
<!-- end partner -->