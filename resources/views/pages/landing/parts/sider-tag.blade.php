<div class="sider-tag bg-white p-3 mb-3 box-tag">
    <p class="title-tag">Các danh mục khác</p>
    <div class="list-tag">
        @foreach($lstCategories as $key => $cate)
            <a class="text-capitalize" href="{{route('search.list',['cate' => $cate->slug])}}" title="{{$cate->name}}">{{$cate->name}}</a>
        @endforeach
    </div>
    @if($lstCategories->count()>12)
    <a class="click-more-tag click-more text-center" href="" title=""><i class="ion-arrow-down-b"></i> Hiện thị thêm</a>
        @endif
</div>
<!-- end tag-->
