@if(!Auth::check())
    <div class="slider-signin-url bg-white p-3 mb-3">
        <span class="d-block mb-3">Bạn đã có tài khoản chưa?</span>
        <a class="btn-orange btn btn-secondary  w-100 text-uppercase font-weight-bold" href="{{route('auth.register')}}" title="">Đăng ký</a>
    </div>
@endif
<!-- end slider-signin-url-->