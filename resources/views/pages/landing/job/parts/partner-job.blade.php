<section class="section-partner bg-white py-4 py-md-5 text-center">
	<div class="container">
		<div class="title text-center font-weight-bold">
			Chọn việc tốt
		</div>
		<ul class="content-partner list-unstyled clearfix mb-3">
			@foreach(range(1,5) as $item)
			<li>
				<a href="" title="">
    				<img src="{{asset('assets/img/upload/unilever.png')}}" alt="">
    			</a>
			</li>
			@endforeach
		</ul>
		<a class="btn-orange btn btn-secondary m-auto font-weight-bold text-uppercase py-2 px-4" href="" title="">xem tất cả Công việc</a>
	</div>
</section>
<!-- end partner -->