<form action="{{route('landing-page.job.timkiem')}}" method="POST" class="form-search-job form-search-style-2">
    {{csrf_field()}}
    <input type="hidden" name="cate" value="{{$cate->slug}}">
    <div class="row">
        <div class="form-group col-md-3">
            <input name="tu-khoa" type="text" class="form-control" placeholder="Nhập 1 từ khoá ...">
        </div>
        <div class="form-group col-md-3">
            <select name="muc-luong" class="custom-select w-100">
                <option value="0-99999" selected>Mức lương</option>
                @foreach($lstMucLuong as $key => $item)
                    <option value="{{$key}}">{{$item}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-md-3">
            <select name="cate" class="custom-select w-100">
                <option value="{{ $cate->slug }}" selected>Ngành nghề</option>
                @foreach($lstCategories as $nganhNghe)
                    <option value="{{$nganhNghe->slug}}">{{$nganhNghe->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-md-3">
            <select name="location" class="custom-select w-100">
                <option value="" selected>Tỉnh thành</option>
                @foreach($lstTinhThanh as $key=> $item)
                    <option data-latlng="{{$item['latlng']}}" data-id="{{$key}}" value="{{$item['label']}}">{{$item['label']}}</option>
                @endforeach
            </select>
            <input name="latlng" type="hidden" value="">
        </div>
        <div class="form-group col-md-12">
            <button type="submit" class="btn btn-primary w-100 text-uppercase font-weight-bold">Tìm kiếm</button>
        </div>
    </div>
</form>
<!-- end form search -->