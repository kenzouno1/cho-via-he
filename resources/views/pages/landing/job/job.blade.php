@extends('master')
@section('content-page')
    <main class="wrapper-main" role="main">
        <!-- wrapper-banner -->
        <div class="banner-page-landing wrapper-banner"
             style="background-image: url('{{asset('assets/img/default-image/banner-job.png')}}');">
            <div class="container">
                <div class="main-banner">
                    <div class=" header-banner text-uppercase">
                        <p class="text1">Người tìm việc</p>
                        <h2 class="text2">Việc cần người</h2>
                    </div>
                    <!-- end header banner -->
                </div>
            </div>
        </div>
        <!-- end wrapper-banner -->

        <section class="section-content-car bg-graylight">
            <div class="container">
            @include('pages.landing.job.parts.form-search-job')
            <!-- end form search -->
                <div class="hidden-lg-up">
                @include('template-part.box-ads-blade')
                <!-- end box-img-ads -->
                </div>
                <div class="row mb-4">
                    <div class="col-lg-9">
                        <div class="group-card px-md-3 pb-1">
                            @foreach($lstAds as $ads)
                                @include('loop.card-job-list')
                            @endforeach
                        </div>
                        <!-- end group card -->
                    </div>
                    <div class="col-lg-3">
                        <div class="hidden-md-down mb-3">
                        @include('template-part.box-ads-blade')
                        <!-- end box-img-ads -->
                        </div>
                        <!-- end box img -->
                    @include('pages.landing.parts.sider-post-ad',['sectionTitle'=>'Đăng Tin Tuyển Dụng'])
                    <!-- end sider-post-ad-->
                    @include('pages.landing.parts.sider-signin-url')
                    <!-- end sider-signin-url-->
                    @include('pages.landing.parts.sider-tag')
                    <!-- end sider-tag-->
                    </div>
                </div>
            </div>
        </section>
        <!-- end section -->

    {{--    @include('pages.landing.job.partner-job')--}}

    <!-- end partner -->

        {{--<section class="section-assessment bg-graylight ">--}}
        {{--<div class="container">--}}
        {{--<div class="title mb-3">--}}
        {{--Việc làm tìm kiếm nhiều nhất--}}
        {{--</div>--}}
        {{--<div class="content-assessment clearfix">--}}
        {{--@foreach(range(1,4) as $item)--}}
        {{--@include('loop.card-job')--}}
        {{--@endforeach--}}
        {{--</div>--}}
        {{--<!-- end content-assessment -->--}}
        {{--<div class="text-center">--}}
        {{--<a class="btn-orange btn btn-secondary m-auto font-weight-bold text-uppercase py-2 px-4" href=""--}}
        {{--title="">tất cả các bài đánh giá</a>--}}
        {{--</div>--}}
        {{--<!-- end btn -->--}}
        {{--</div>--}}
        {{--</section>--}}
    </main>
@endsection

@push('script-custom')
    @include('pages.landing.job.assets.js')
    @include('pages.landing.assets.js')
@endpush
