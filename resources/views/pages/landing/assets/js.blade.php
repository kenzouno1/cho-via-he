<script>
    loadLocation();

    function loadLocation() {
        var lstPlaceIds = ["ChIJjSriTooZCjERuojAUiywHNw", "ChIJQ44uKJxsdTERUu3qQZlT48w", "ChIJeSj5jmq2dDERcxSNHInanzM", "ChIJHyJ92y9oczEREUL4cg4bwJQ", "ChIJoVmP1vLDdjERjsI0345CNyc", "ChIJvxtxbu8tbzERN_3lOgRMv0U", "ChIJDb3qxxupmDERT8bH4BD4NO8", "ChIJ7c9Gs6kQNTERyFDrUFtMQjw", "ChIJTeuFSkjgyjYR0G5JK0iQA78", "ChIJUXqvJl0KNTERcI9VxsAgWeU", "ChIJn6e8DFyoCjERQ3UxZ-VUPKo", "ChIJ52dYYbZcyjYRWFR2N_RyN2U", "ChIJez-heqBJoTERs-aysVPn-a8", "ChIJo4EvAoIfbDERDbVDgfa1Uow", "ChIJb9jgnBgUNDERF_4UZiqEJag", "ChIJsXHT6sp5zDYRqIDLmrB4SIM", "ChIJ4WQZGBHFNTERUmgyOw25qXQ", "ChIJx8Pp7qEoODERaxB9uCZ7xxo", "ChIJXSYdMDWvNTERgtTWzxbqqac", "ChIJNVLgSPR6SjERU5AdBNFX6YI", "ChIJs2mi7ZHyoDER5dn-Lulyo3k", "ChIJldPcqIwbcDERzggjMicIwmk", "ChIJzXUakhzaCTERevNfMyZB54c", "ChIJ39GqWCPnazERVIrYzKoRm2E", "ChIJUbTROSwyLTERLjxIkWCRKeI", "ChIJeUgqlTy2CjERI0HFz5nt6ts", "ChIJBw5Mv4w7zTYRaJBG-7m2oiE", "ChIJfdvzWSYTcTERbPVUX9_SWrs", "ChIJbyVFtz5UtTYRWm6Chdo0Pfo", "ChIJ3xtSUSEMNjERYt8l_r-i68g", "ChIJrR1aC2TOOTERGaUZ13y_YVw", "ChIJwcgQb6twNjERLQwTPE5T5W0", "ChIJrwn6F26VcDERRZ6Odp6S9fg", "ChIJ-zyQ3XmDNDERbmTIyOCN7DA", "ChIJ_65MY79wbzERgBoXHBy-bv4", "ChIJLW8pOPawODERj1kjYi6HH5A", "ChIJezo14dQNQjEReXZ0ZrGrnOQ", "ChIJcwkWqew-aDERse05Mm6NoU0", "ChIJL4V4mtcBSzERaoal8PR5B-0", "ChIJKfEdqqvkQDERKE3VIOD6ceQ", "ChIJIY5foglSoDERTgqWSmqWc9Y", "ChIJ1R093JBZMjERezgPalpkLVg", "ChIJt-3jbZ9ioDERWbYg-9sJf1I", "ChIJoRyG2ZurNTERqRfKcnt_iOc", "ChIJ0xo_nvN6SjER6Ad6-F_I_6U", "ChIJ0T2NLikpdTERKxE8d61aX_E", "ChIJEyolkscZQjERh2RDRKDjFPw", "ChIJe_RbMQL4NjERleXIlccfjEc", "ChIJM3A59dPkNTERx1sdOPgF5Go", "ChIJd3qgchjENDERW0_vaRA3qVc", "ChIJ95BkcoecQTERtYTL98oPHKs", "ChIJgSaoteuvCjERdWM3wQen4zw", "ChIJj-0MGk4XoDERE4o57BhuS7c", "ChIJ4R5JpfutNDERf3iEknEBVH8", "ChIJMXJO1JFCCzERzWzchde8giQ", "ChIJE45EIbybCjERpXVcaT5qxlI", "ChIJuy3qgk3lNDERXxAb4T-O0MA", "ChIJGStwEvU5MzERteIKRp__AAU", "ChIJ8_vR4A4fLTER9A7fYoWMcUg", "ChIJV3Ew99r3cTERFTOI2ZR2-Y4", "ChIJJ6lE5uy1czERrYej9DDhMHU", "ChIJJepKuFLZdDERVRZuS_HmSuA", "ChIJHxIM1bNlCjERQG40rV7JoNw"];
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                    var pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    var geocoder = new google.maps.Geocoder;
                    geocoder.geocode({'location': pos}, function (results, status) {
                        if (status === 'OK' && results) {
                            for (var i = 0; i < results.length; i++) {
                                if (lstPlaceIds.indexOf(results[i].place_id) >= 0) {
                                    var val_select = $('select[name="location"]').find('option[data-id="' + results[i].place_id + '"]').val();
                                    $('select[name="location"]').val(val_select);
                                    var latlng = '' + pos.lat + '-' + pos.lng + '';
                                    $('input[name="latlng"]').val(latlng);
                                }
                            }
                        }
                        else {
                            console.log('Geocoder failed due to: ' + status);
                        }
                    });
                }
            );
        } else {
            console.log("Geolocation is not supported by this browser.");
        }
    }
    $('select[name="location"]').change(function (event) {
        event.preventDefault();
        var latlng = $(this).find(':selected').attr('data-latlng');
        $('input[name="latlng"]').val(latlng);
    });
    $('.click-more-tag').click(function (event) {
        event.preventDefault();
        $(this).hide();
        $('.list-tag a:nth-child(n+12)').slideDown();
    });
</script>