{{--<script src="{{asset('assets/vendor/slick/slick.min.js')}}"></script>--}}
<script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_API','AIzaSyCcCGIu_Jz3R1wUtpLCtxjvNDNlueSCuTE')}}&libraries=places"
        async defer></script>
<script type="text/javascript">
    //readmore bds on mobile
    $('.click-more-post-bds').click(function (event) {
        event.preventDefault();
        $(this).css('display', 'none');
        $('.card-bds:nth-of-type(1n+7)').slideDown('fast');
    });
    $('select[name="ads-type"]').change(function () {
        var type_value = $(this).val();
        console.log(type_value);
        $.ajax({
            url: '{{ route('landing-page.bds.lstPrice')}}',
            type: 'POST',
            data: {ads_type: type_value},
        }).done(function (data) {
            $('select[name="mucgia"]').find('option').remove();
            $.each(data, function (index, value) {
                $('select[name="mucgia"]').append('<option value="' + value + '">' + value + '</option>');
            });
        })
    });

    //slider
    //    $('.slider-real-estate').slick({
    //	  dots: true,
    //	  arrows:false,
    //	  slidesToShow: 4,
    //	  slidesToScroll: 4,
    //	  responsive: [
    //	    {
    //	      breakpoint: 1024,
    //	      settings: {
    //	        slidesToShow: 3,
    //	        slidesToScroll: 3,
    //	        infinite: true,
    //	        dots: true
    //	      }
    //	    },
    //	    {
    //	      breakpoint: 767,
    //	      settings: {
    //	        slidesToShow: 2,
    //	        slidesToScroll: 2
    //	      }
    //	    },
    //	    {
    //	      breakpoint: 480,
    //	      settings: {
    //	        slidesToShow: 1,
    //	        slidesToScroll: 1
    //	      }
    //	    }
    //	  ]
    //	});
</script>