@extends('master')
@section('content-page')
<main class="wrapper-main" role="main">
	<!-- wrapper-banner -->
	    <div class="banner-job banner-page-landing wrapper-banner" style="background-image: url('{{asset('assets/img/default-image/banner-bds.png')}}');">
	    	<div class="container">
	    		<div class="main-banner">
	    			<div class=" header-banner ">
	    				<p class="text1">Mua  &  bán</p>
	    				<h2 class="text2">BẤT ĐỘNG SẢN</h2>
	    			</div>
	    			<!-- end header banner -->
	    		</div>
	    	</div>
	    </div>
	    <!-- end wrapper-banner -->

	    <section class="section-content-bds bg-graylight">
	    	<div class="container">
				@include('pages.landing.bds.parts.form-search-bds')
				<!-- end form search -->
				<div class="hidden-lg-up">
				@include('template-part.box-ads-blade')
				<!-- end box-img-ads -->
				</div>
	    		<div class="row mb-4">
	    			<div class="col-lg-9">
					    <div class="group-card px-md-3 pb-1">
					    	 @foreach($lstAds as $ads)
					             @include('loop.card-bds')
					        @endforeach
						    <a class="click-more-post-bds click-more text-center hidden-md-up" href="#" title="Hiện thị thêm"><i class="ion-arrow-down-b"></i> Hiện thị thêm </a>
					    </div>
					    <!-- end group card -->
					</div>
					<div class="col-lg-3">
						<div class="hidden-md-down mb-3">
						@include('template-part.box-ads-blade')
						<!-- end box-img-ads -->
						</div>
						<!-- end box img -->
					@include('pages.landing.parts.sider-post-ad',['sectionTitle'=>'Đăng tin'])
					<!-- end sider-post-ad-->
					@include('pages.landing.parts.sider-signin-url')
					<!-- end sider-signin-url-->
					@include('pages.landing.parts.sider-tag')
						<!-- end sider-tag-->
					</div>
	    		</div>
	    		{{--<div class="box-suggest my-5">--}}
	    			{{--<h5 class="mb-4"> Căn hộ cho thuê</h5>--}}
	    			{{--<div class="slider-real-estate">--}}
	    				{{--@foreach(range(1,7) as $item)--}}
		                    {{--@include('loop.card-post')--}}
		                {{--@endforeach--}}
	    			{{--</div>--}}
	    			{{--<a class="btn-view-all btn btn-primary" href="" title="">Tất cả cho thuê căn hộ</a>--}}
	    		{{--</div>--}}
	    		<!-- end form search car desktop -->
	    		{{--<div class="real-estate-suggest">--}}
	    			{{--<h5 class="mb-4">Bất động sản Thành phố Hồ Chí Minh</h5>--}}
	    			{{--<div class="row">--}}
	    				{{--@foreach(range(1,32) as $item)--}}
		                    {{--<div class="col-md-2 col-sm-3 col-6">--}}
		    					{{--<a href="" title="">Quận 1</a>--}}
		    				{{--</div>--}}
		                {{--@endforeach--}}
	    			{{--</div>--}}
	    		{{--</div>--}}
	    		<!-- end real-estate-suggest -->
	    	</div>
	    </section>
	    <!-- end section -->
</main>
@endsection
@push('css-custom')
	@include('pages.landing.bds.assets.css')
@endpush
@push('script-custom')
	@include('pages.landing.bds.assets.js')
	@include('pages.landing.assets.js')
@endpush
