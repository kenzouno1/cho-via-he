<div class="box-form-search-car box-form-search-style-2 form-search-style-2 hidden-md-up">
	<div class="container">
		<form class="form-search-car">
			<div class="row">
				<div class="form-group col-md-6">
				    <input type="text" class="form-control" placeholder="Nhập 1 từ khoá ...">
				</div>
				<div class="form-group col-md-6">
					<select class="custom-select w-100">
					  <option selected>Loại bất động sản</option>
					  <option value="1">One</option>
					  <option value="2">Two</option>
					  <option value="3">Three</option>
					</select>
				</div>
				<div class="form-group col-md-4">
					<select class="custom-select w-100">
					  <option selected>Cần bán</option>
					  <option value="1">One</option>
					  <option value="2">Two</option>
					  <option value="3">Three</option>
					</select>
				</div>
				<div class="form-group col-md-4">
					<select class="custom-select w-100">
					  <option selected>Tỉnh thành</option>
					  <option value="1">One</option>
					  <option value="2">Two</option>
					  <option value="3">Three</option>
					</select>
				</div>
				<div class="form-group col-md-4">
					<select class="custom-select w-100">
					  <option selected>Mức giá</option>
					  <option value="1">One</option>
					  <option value="2">Two</option>
					  <option value="3">Three</option>
					</select>
				</div>
				<div class="form-group col-md-12">
					<button type="submit" class="btn btn-primary w-100 text-uppercase font-weight-bold">Tìm kiếm</button>
				</div>
			</div>
		</form>
		<!-- end form search -->
		<div class="box-img-ads text-center" style="background-image: url('{{asset('assets/img/upload/ads.jpg')}}')">
			<div class="description">
				<span>Xem tất cả 2500 bài đăng </span>
				<h5 class="text-uppercase m-0">dành cho Bất động sản</h5>
			</div>
		</div>
		<!-- end box-img-ads -->
	</div>
</div>