<form action="{{route('landing-page.bds.timkiem')}}" method="POST" class="form-search-bds form-search-style-2">
    {{csrf_field()}}
    <input type="hidden" name="cate" value="{{$cate->slug}}">
    <div class="row">
        <div class="form-group col-md-6">
            <input name="tu-khoa" type="text" class="form-control" placeholder="Nhập 1 từ khoá ...">
        </div>
        <div class="form-group col-md-6">
            <select name="cate" class="custom-select w-100">
                <option value="{{$cate->slug}}" selected>Loại bất động sản</option>
                @foreach($lstLoaiBds as $key => $item)
                    <option value="{{$key}}">{{$item}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-md-4">
            <select name="ads-type" class="custom-select w-100">
                <option value="sellbuy" selected>Bất động sản mua bán</option>
                <option value="rent">Bất động sản thuê</option>
                <option value="new">Dự án mới</option>
            </select>
        </div>
        <div class="form-group col-md-4">
            <select name="location" class="custom-select w-100">
                <option value="" selected>Tỉnh thành</option>
                @foreach($lstTinhThanh as $key=> $item)
                    <option data-latlng="{{$item['latlng']}}" data-id="{{$key}}" value="{{$item['label']}}">{{$item['label']}}</option>
                @endforeach
            </select>
            <input name="latlng" type="hidden" value="">
        </div>
        <div class="form-group col-md-4">
            <select name="mucgia" class="custom-select w-100">
                <option value="" selected>Mức giá</option>
                @foreach($lstMucGia as $key => $item)
                    <option value="{{$key}}">{{$item}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-md-12">
            <button type="submit" class="btn btn-primary w-100 text-uppercase font-weight-bold">Tìm kiếm</button>
        </div>
    </div>
</form>
<!-- end form search -->
