@if($lstMessages->isEmpty())
    <div class="p-md-3 p-2 bg-white mb-3">
        <div class="not-news text-center">
            <span class="icon-lagre"><i class="ion-android-chat"></i></span>
            <strong class="mb-2">Bạn chưa tin nhắn nào</strong>
            <p>
                Bạn có thể nhắn tin để trao đổi thông tin cho người đăng tin. Chúng tôi sẽ lưu tin nhắn ở đây!
            </p>
        </div>
    </div>
@else
    <header class="header-pane font-weight-bold mb-4 bg-white p-sm-4 p-3 ">
        Hiển thị {{$lstMessages->firstItem()}}-{{$lstMessages->lastItem()}} trong {{$lstMessages->total()}} cuộc hội thoại
    </header><!-- /header -->
    <div class="box-list-talk ">
        <ul class="list-unstyled">
            @foreach($lstMessages as $message)
                @include('pages.myprofile.parts.loop-chat')
            @endforeach
        </ul>
    </div>
    {{$lstMessages->links()}}
@endif
