<div class="box-list-talk ">
    <ul class="list-unstyled">
        <li id="1" class="item-talk bg-white p-3 d-md-flex justify-content-md-stat mb-3">
            <div class="talk-image mb-3 mb-md-0">
                <a class="d-md-block d-flex justify-content-start" href="{{$message->ads->getUrl()}}"
                   title="{{$message->ads->title}}">
                    <img class="img-fluid" src="{{$message->ads->getThumbnail()[0]['small']}}" alt="{{$message->ads->title}}">
                    <div class="py-2 pl-3 pl-md-0">
                        <p>{{$message->ads->title}}</p>
                        <p class="font-weight-bold price">{{$message->ads->priceFormat}} </p>
                    </div>
                </a>
            </div>
            <!-- end msg-image -->
            <div class="talk-content">
                <header class="p-3 d-flex justify-content-between">
                    <a href="{{route('account-seller',['id'=>$message->userParticipant()->id])}}" title="Xem thông tin {{$message->userParticipant()->name}}">
                        <img src="{{$message->userParticipant()->avatar}}" alt="Ảnh đại diện">
                        <span class="name font-weight-bold text-capitalize">{{$message->userParticipant()->name}}</span>
                    </a>
                    <div class="right hidden-sm-down">
                        <span class="font-weight-bold">Số Cuộc trò chuyện {{$message->messages->count()}}</span>
                        <a class="delete" href="{{route('member.message.delete',['slug'=>$slug])}}" title="Xoá tin này"><i class="ion-trash-a"></i></a>
                    </div>
                </header><!-- /header -->
                <!-- end talk-content -->
                <div class="box-message py-2">
                <div id="msg-details">
                    @foreach($message->messages as $msg)
                        <div class="message {{$msg->getMessageAlight()}}">
                            <div class="message-text">
                                <p>
                                    {{$msg->body}}
                                </p>
                                <small class="time-msg">{{TimeHelper::ago($msg->created_at)}}</small>
                            </div>
                        </div>
                    @endforeach
                </div>
                    <div class="send-message">
                        <form id="send">
                            <input type="text" id="msgInput" class="send-input" placeholder="Viết tin nhắn "/>
                        </form>
                    </div>
                </div>
                <!-- end phone container -->
            </div>
            <!-- end talk-content -->
        </li>
        <!-- end item-talk -->
    </ul>
</div>
@push('script-custom')
    <script type="text/javascript">
        $(function () {
            $('#send').bind('submit', function (event) {
                event.preventDefault();
                var msgText = $('#msgInput').val();
                if (msgText != '') {
                    $('#msg-details').append(
                        $('<div>').addClass('message right').append(
                            $('<div>').addClass('message-text').text(msgText)
                        )
                    );
                    sendMsg(msgText);
                    $('#msgInput').val('');
                }
            });
            function sendMsg(msg) {
                $.ajax({
                    url:'{{route('member.message.send')}}',
                    type:'POST',
                    data:{
                        post:'{{$message->ads->id}}',
                        msg:msg,
                    }
                });
            }
        });
    </script>
@endpush
