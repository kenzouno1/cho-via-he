<li id="1" class="item-talk bg-white p-3 d-md-flex justify-content-md-stat mb-3">
    <div class="talk-image mb-3 mb-md-0">
        <a class="d-md-block d-flex justify-content-start"  href="{{$message->ads->getUrl()}}" title="{{$message->ads->title}}">
            <img class="img-fluid" src="{{$message->ads->getThumbnail()[0]['small']}}" alt="{{$message->ads->title}}">
            <div class="py-2 pl-3 pl-md-0">
                <p>{{$message->ads->title}}</p>
                <p class="font-weight-bold price">{{$message->ads->priceFormat}} </p>
            </div>
        </a>
    </div>
    <!-- end msg-image -->
    <div class="talk-content">
        <header class="p-3 d-flex justify-content-between">
            <a href="{{route('account-seller',['id'=>$message->userParticipant()->id])}}" title="Xem thông tin {{$message->userParticipant()->name}}">
                <img src="{{$message->userParticipant()->avatar}}" alt="Ảnh đại diện">
                <span class="name font-weight-bold text-capitalize">{{$message->userParticipant()->name}}</span>
            </a>
            <div class="right hidden-sm-down">
                <span class="font-weight-bold">Cuộc trò chuyện {{$message->messages->count()}}</span>
                <a class="delete" href="{{ route('member.message.delete',['slug'=>$message->ads->slug.'-'.$message->id] ) }}" title="Xóa tin này"><i class="ion-trash-a"></i></a>
            </div>
        </header><!-- /header -->
        <div class="talk-description p-3" >
            <a href="{{ route('member.message.details',['slug'=>$message->ads->slug.'-'.$message->id] ) }}">
                @foreach($message->messages as $msg)
                    <div class="message {{$msg->getMessageAlight()}}">
                        <div class="message-text">
                            <p>
                                {{$msg->body}}
                            </p>
                            <small class="time-msg">{{TimeHelper::ago($msg->created_at)}}</small>
                        </div>
                    </div>
                @endforeach
            </a>
        </div>
    </div>
    <!-- end talk-content -->
</li>
<!-- end item-talk -->