<div class="p-md-4 p-3 bg-white d-md-flex justify-content-md-between d-md-inline text-md-left text-center">
    <p class="mb-md-0 py-md-2">
        @if(count($lstAds) <= 0)
             Hiện không có tin nào trong phần này
        @endif
    </p>
    <a class="btn-post btn btn-primary px-5 px-2 text-uppercase font-weight-bold" href="{{route('ads.new.select_cate')}}" title="">Tôi muốn đăng tin</a>
</div>