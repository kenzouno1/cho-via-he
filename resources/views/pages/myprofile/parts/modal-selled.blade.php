<!-- modal modal display none composer -->
<form action="">
    {{ csrf_field() }}
    <input type="hidden" name="post-id">
    <div class="modal-display-composer modal fade modal-style-sm" id="modal-display-composer" tabindex="-1"
         role="dialog" aria-labelledby="modal-display-composer">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <!-- modal-header -->
                <div class="modal-header d-flex  align-items-start">
                    Ẩn tin
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="ion-close-round"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <p>
                        Khi bạn đã bán được hàng hoặc ngưng muốn tin hiện thị trên hệ
                        thống Chợ Vỉa Hè, bạn hãy chọn 1 trong các lý do sau:
                    </p>
                    <div class="custom-controls-stacked">
                        <label class="custom-control custom-radio d-flex  align-items-center">
                            <input id="radioStacked1" name="radio-stacked" type="radio" value="hidden"
                                   class="custom-control-input">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Ngưng hiện thị bài đăng</span>
                        </label>
                        <label class="custom-control custom-radio d-flex  align-items-center">
                            <input id="radioStacked2" name="radio-stacked" type="radio" value="sold"
                                   class="custom-control-input">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Đã bán trên hệ thống Chợ Vỉa Hè</span>
                        </label>
                        <label class="custom-control custom-radio d-flex  align-items-center">
                            <input id="radioStacked2" name="radio-stacked" value="sold elsewhere" type="radio"
                                   class="custom-control-input">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Đã bán nơi khác</span>
                        </label>
                    </div>
                </div>
                <!-- modal-body -->
                <div class="modal-footer d-flex justify-content-start">
                    <a class="btn btn-primary ajax-hidden-post" href="#" title="Gửi">Gửi</a>
                    <a class="btn btn-secondary" data-dismiss="modal" href="" title="Hủy">Hủy</a>
                </div>
            </div>
        </div>
    </div>
</form>
<!-- End modal display none composer -->
@push('script-custom')
    <script>
        $('.hidden-post').click(function (event) {
            event.preventDefault();
            var post_id = $(this).data('id');
            $('input[name="post-id"]').val(post_id);
        });
        $('.ajax-hidden-post').click(function (event) {
            $.ajax({
                type: 'POST',
                url: '{{ route('member.hidden-post')}}',
                data: $(this).closest('form').serialize(),
            }).done(function (response) {
                var post_id = $('input[name="post-id"]').val();
                $('#modal-display-composer').modal('toggle');
                $('ul.list-unstyled li.post-'+post_id).remove();
//                $('.modal-upload-avata .close').click();
//                that.find('.form-upload-avata .avata img').attr('src', imageData);
            });
        });
    </script>
@endpush
