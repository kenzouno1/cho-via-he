<li class="p-md-3 p-2 bg-white mb-3 {{$ads->ads_class()}}">
    <div class="plus card-manage card-horizontal-lg card-horizontal ">
        <div class="card-img-top">
            <a href="{{$ads->getUrl()}}" title="{{$ads->title}}">
                @if ($ads->hasFeatures('has-gallery'))
                    @if(count($ads->getThumbnail(3))==1)
                        <a class="img-gallery single-gallery" href="{{$ads->getUrl()}}" title="{{$ads->title}}">
                            <img class="img-fluid" src="{{$ads->getThumbnail()[0]['small']}}" alt="{{$ads->title}}"
                                 alt="{{$ads->title}}">
                        </a>
                    @elseif(count($ads->getThumbnail(3))==2)
                        <a class="img-gallery vertical-gallery" href="{{$ads->getUrl()}}" title="{{$ads->title}}">
                            @foreach ($ads->getThumbnail(3) as $key => $image)
                                <img class="img-fluid" src="{{$image['vertical']}}" alt="{{$ads->title}}"
                                     alt="{{$ads->title}}">
                            @endforeach
                        </a>
                    @else
                        <a class="img-gallery horizontal-gallery" href="{{$ads->getUrl()}}" title="{{$ads->title}}">
                            @foreach ($ads->getThumbnail(count($ads->images) <= 3 ? count($ads->images) : 3) as $key => $image)
                                <img class="img-fluid" src="{{$image['small']}}" alt="{{$ads->title}}"
                                     alt="{{$ads->title}}">
                            @endforeach
                        </a>
                    @endif
                @else
                    <a href="{{$ads->getUrl()}}" title="{{$ads->title}}">
                        <img class="img-fluid" src="{{$ads->getThumbnail()[0]['small']}}" alt="{{$ads->title}}"
                             alt="{{$ads->title}}">
                    </a>
                @endif
            </a>
        </div>
        <div class="card-header-mb card-header hidden-md-up">
            <h6 class="card-title"><a href="{{$ads->getUrl()}}" title="{{$ads->title}}">{{$ads->title}}</a></h6>
            <div class="type-post">
                {{$ads->PackageName}}
            </div>
        </div>
        <div class="card-block">
            <div class="clear-fix  mb-sm-1">
                <div class="card-header">
                    <h6 class="card-title text-truncate mb-1 hidden-sm-down"><a href="{{$ads->getUrl()}}"
                                                                                title="{{$ads->title}}">{{$ads->title}}</a>
                    </h6>
                    <ul class="list-unstyled mb-1">
                        <li>{{$ads->categoryName}}</li>
                        <li class="card-price text-uppercase">{{$ads->priceFormat}}</li>
                        <li>{{$ads->address}}</li>
                        <li>{{$ads->ago}}</li>
                    </ul>
                </div>
                <div class="type-post hidden-sm-down">
                    {{$ads->packageName}}
                </div>
            </div>
            <div class="card-description">
                <p>
                    {{\Illuminate\Support\Str::words($ads->des,30,'...')}}
                </p>
                <div class="date-address">
                    <p class="card-address m-0">{{$ads->address}}</p>
                    <time>{{$ads->ago}}</time>
                </div>
            </div>
        </div>
        {{--end card bock--}}
        <div class="dropdown">
            <a href="#" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="ion-android-more-horizontal"></i>
            </a>

            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                <a onclick="window.open(this.href, 'mywin','left=50,top=50,width=600,height=350,toolbar=0'); return false;"
                   class="dropdown-item" href="https://www.facebook.com/sharer.php?u={{$ads->getUrl()}}"
                   title="Chia sẻ"><i class="ion-android-share-alt"></i> Chia sẻ</a>
                <a class="dropdown-item hidden-post" href="#" title="Đã bán / Ẩn tin" data-toggle="modal"
                   data-target="#modal-display-composer" data-id="{{$ads->id}}"><i class="ion-eye-disabled"></i>Đã bán /
                    Ẩn tin</a>
                <a class="dropdown-item" href="{{route('member.manage-post.edit',['id'=>$ads->id])}}"
                   title="Sửa bài đăng"><i class="ion-compose"></i>Sửa bài đăng</a>
                <a class="dropdown-item delete-post" href="#" title="Xoá bài đăng" data-id="{{$ads->id}}"><i
                            class="ion-trash-a"></i> Xoá bài đăng</a>
            </div>
        </div>
        {{--end dropdown--}}
    </div>
</li>
@push('script-custom')
    <script>
        $('.delete-post').click(function (event) {
            event.preventDefault();
            var post_id = $(this).data('id');
            $.ajax({
                url: '{{ route('admin.report.deleteAds')}}',
                type: 'POST',
                data: {ads_id: post_id},
            }).done(function (data) {
                $('ul.list-unstyled li.post-' + post_id).remove();
            })
        });
    </script>
@endpush