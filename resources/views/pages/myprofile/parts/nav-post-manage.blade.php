<header class="header-pane  mb-4 bg-white py-md-4 px-md-3 ">
    <ul class=" d-md-flex flex-md-row font-weight-bold list-unstyled">
        <li class="{{UrlHelper::classUserProfileActiveUrl('member.manage-post',true)}}">
            <a href="{{route('member.manage-post')}}" title="Tất cả bài đăng">Tất cả bài đăng ({{$countAll}})</a>
        </li>
        <li class="{{UrlHelper::classUserProfileActiveUrl('member.manage-post.active')}}">
            <a href="{{route('member.manage-post.active')}}" title="Bài đăng hiện tại">Bài đăng hiện tại ({{$countActive}})</a>
        </li>
        <li class="{{UrlHelper::classUserProfileActiveUrl('member.manage-post.deactive')}}">
            <a href="{{route('member.manage-post.deactive')}}" title="Bài đăng đang chờ xử lý">Bài đăng đang chờ xử lý ({{$countDeactive}})</a>
        </li>
    </ul>
</header><!-- /header -->