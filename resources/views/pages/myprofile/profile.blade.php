@extends('master')
@section('content-page')
    <!-- modal upload avata -->
    <div class="modal-upload-avata modal fade " id="modal-upload-avata" tabindex="-1" role="dialog"
         aria-labelledby="modal-upload-avata">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <!-- modal-header -->
                <div class="modal-header d-flex  align-items-start">
                    Chọn vị trí và khích thước ảnh
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="ion-close-round"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="image-editor">
                        <div class="cropit-preview"></div>
                        <div class="slider-wrapper d-flex align-items-center justify-content-center">
                            <i class="ion-image"></i>
                            <input type="range" class="cropit-image-zoom-input">
                            <i class="ion-image large-image"></i>
                        </div>
                        <input type="file" class="cropit-image-input" type="hidden">
                    </div>
                </div>
                <!-- modal-body -->
                <div class="modal-footer d-flex align-items-star ">
                    <a class="btn-upload-avata btn btn-secondary" href="" title="">Chọn ảnh</a>
                    <a class="btn btn-secondary export" href="" title="">Lưu</a>
                    <a class="btn-close btn btn-secondary " href="" title="">Hủy</a>
                </div>
            </div>
        </div>
    </div>
    <!-- End modal upload avata -->
    <main class="manage-page wrapper-main" role="main">
        <!-- wrapper-banner -->
        <div class="banner-page-content wrapper-banner"
             style="background-image:url('{{asset('assets/img/default-image/bg-banner.png')}}'); ">
        </div>
        <!-- end wrapper-banner -->

        <section class="content-manage-post section-content bg-graylight">
            <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="col-xl-10 ">
                        @include('pages.myprofile.nav-account')
                        <div class="tab-pane">
                            <form class="form-upload-avata mb-sm-4 mb-3 bg-white p-2 p-md-5">
                                <div class="avata">
                                    <img src="{{AuthHelper::getAvatar()}}" alt="">
                                </div>
                                <div class="content">
                                    <h3>{{AuthHelper::current_user()->name}}</h3>
                                    <span>{{AuthHelper::current_user()->email}}</span>
                                    <a class="upload-img-avata" href="#" title="" data-toggle="modal"
                                       data-target="#modal-upload-avata"><i class="ion-camera"></i></a>
                                </div>
                            </form>
                            <div class="form-change-profile">
                                <form id="form-change-account" action="{{route('member.change-pass-profile')}}"
                                      method="POST">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}"
                                           data-parsley-validate="">
                                    <div class="module bg-white mb-sm-4 mb-3 ">
                                        <div class="module-header">
                                            <div class="p-3 font-weight-bold d-flex justify-content-between">
                                                <p class="mb-0">Thay đổi thông tin tài khoản</p>
                                                <a class="click-hidde-form hidden-md-up" href="" title="">Thu gọn</a>
                                            </div>
                                        </div>
                                        <fieldset class="p-3">
                                            <div class="row d-flex align-items-md-center">
                                                <div class="col-lg-5 col-md-8">
                                                    <div class="form-group {{ validate_form_class('email',$errors) }}">
                                                        <label class="font-weight-bold">Email</label>
                                                        <input name="email"
                                                               value="{{old('email',AuthHelper::current_user()->email)}}"
                                                               type="email" class="form-control"  data-parsley-type-message="Email không hợp lệ">
                                                        {{ render_validate_msg('email',$errors) }}
                                                    </div>
                                                    <!-- end form-group -->
                                                    <div class="form-group {{ validate_form_class('password_old',$errors) }}">
                                                        <label class="font-weight-bold">Mật khẩu cũ</label>
                                                        <input name="password_old" type="password" class="form-control">
                                                        {{ render_validate_msg('password_old',$errors) }}
                                                    </div>
                                                    <!-- end form-group -->
                                                    <div class="form-group {{ validate_form_class('password_new',$errors) }}">
                                                        <label class="font-weight-bold">Mật khẩu mới</label>
                                                        <input id="password_new" name="password_new" type="password"
                                                               class="form-control">
                                                        {{ render_validate_msg('password_new',$errors) }}
                                                    </div>
                                                    <!-- end form-group -->
                                                    <div class="form-group {{ validate_form_class('password_confirm',$errors) }}">
                                                        <label class="font-weight-bold">Nhập lại mật khẩu</label>
                                                        <input name="password_confirm" type="password"
                                                               class="form-control">
                                                        {{ render_validate_msg('password_confirm',$errors) }}
                                                    </div>
                                                    <!-- end form-group -->
                                                    <button type="submit"
                                                            class=" btn btn-primary text-uppercase font-weight-bold mb-sm-3 mt-1 px-5 py-2">
                                                        Lưu
                                                    </button>
                                                    {{ render_success_msg($errors,'success_account') }}
                                                </div>
                                            </div>
                                            <!-- end row -->
                                        </fieldset>
                                    </div>
                                </form>
                                <!-- end  module -->
                            </div>
                            <!-- end form-change-profile -->
                            <div class="form-change-profile">
                                <form id="form-change-profile" form action="{{route('member.editprofile')}}"
                                      method="POST">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="module bg-white mb-sm-4 mb-3">
                                        <div class="module-header">
                                            <div class="p-3 font-weight-bold d-flex justify-content-between ">
                                                <p class="mb-0">Thay đổi thông tin cá nhân</p>
                                                <a class="click-hidde-form hidden-md-up" href="" title="">Thu gọn</a>
                                            </div>
                                        </div>
                                        <fieldset class="p-3">
                                            <div class="row d-flex align-items-md-center">
                                                <div class="col-lg-5 col-md-8">
                                                    <div class="form-group ">
                                                        <label class="font-weight-bold">Tên liên hệ</label>
                                                        <input tabindex="1" name="name"
                                                               value="{{old('name',AuthHelper::current_user()->name)}}"
                                                               type="text" class="form-control" required=""
                                                               data-parsley-required-message="Nhập họ tên của bạn"
                                                               minlength="2"
                                                               data-parsley-minlength-message="Vui lòng kiểm tra lại họ tên của bạn">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="font-weight-bold">
                                                            Ngày Sinh
                                                        </label>
                                                        <input value="{{old('birthday',AuthHelper::current_user()->birthday)}}" name="birthday" class="form-control" type="date" max="{{\Carbon\Carbon::now()->subYears(12)}}" data-parsley-max-message="bạn chưa đủ 12 tuổi">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="font-weight-bold">
                                                           Giới tính
                                                        </label>
                                                        <select class="form-control" name="gender">
                                                            <option {{selected(AuthHelper::current_user()->gender,'male')}} value="male">Nam</option>
                                                            <option {{selected(AuthHelper::current_user()->gender,'female')}} value="female">Nữ</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="font-weight-bold">
                                                            Nghề nghiệp
                                                        </label>
                                                        <input value="{{old('job',AuthHelper::current_user()->job)}}" name="job" class="form-control" type="text">
                                                    </div>
                                                    <!-- end form-group -->
                                                    <div class="form-group">
                                                        <label class="font-weight-bold">Địa chỉ </label>
                                                        <a class="float-right primary-color current-location" href=""
                                                           title=""><i class="ion-android-pin"></i> Sự dụng vị trí hiện
                                                            tại của tôi?</a>
                                                        <input tabindex="2" id="location" type="text"
                                                               class="form-control" name="address"
                                                               value="{{old('address',AuthHelper::current_user()->address)}}"
                                                               placeholder="Địa chỉ của bạn" required=""
                                                               data-parsley-required-message="Nhập địa chỉ của bạn">
                                                    </div>
                                                    <!-- end form-group -->
                                                    <button tabindex="3" type="submit"
                                                            class=" btn btn-primary text-uppercase font-weight-bold mb-sm-3 mt-1 px-5 py-2">
                                                        Lưu
                                                    </button>
                                                    {{ render_success_msg($errors,'success_profile') }}
                                                </div>
                                            </div>
                                            <!-- end row -->
                                        </fieldset>
                                    </div>
                                </form>
                                <!-- end  module -->
                            </div>
                            <!-- end form-change-profile -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end section contnet search -->
    </main>
@endsection
@push('script-custom')
    @include('pages.myprofile.assets.js')
    @include('pages.myprofile.assets.js-nav-mobile')
@endpush
