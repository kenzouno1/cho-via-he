<!-- Nav tabs -->
{{--<div class="menu-profile-mobile">--}}
    {{--<div class="icon-toggle">--}}
        {{--<span></span>--}}
        {{--<span></span>--}}
        {{--<span></span>--}}
    {{--</div>--}}
{{--</div>--}}
{{--end menu profile mobile--}}
<ul class="nav nav-tabs">
    <li class="nav-item">
        <a class="nav-link {{UrlHelper::classUserProfileActiveUrl('member.profile')}}" href="{{route('member.profile')}}" title="Trang cá nhân"><i class="ion-person"></i> Trang cá nhân</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{UrlHelper::classUserProfileActiveUrl('member.message')}}" href="{{route('member.message')}}" title="Tin nhắn"><i class="ion-android-chat"></i> Tin nhắn</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{UrlHelper::classUserProfileActiveUrl('member.manage-post')}}" href="{{route('member.manage-post')}}" title="Quản lí bài đăng"><i class="ion-image"></i> Quản lí bài đăng</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{UrlHelper::classUserProfileActiveUrl('member.saved-searches')}}"  href="{{route('member.saved-searches')}}" title="Đã tim kiếm"><i class="ion-bookmark"></i> Đã tim kiếm</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{UrlHelper::classUserProfileActiveUrl('member.viewed')}}"  href="{{route('member.viewed')}}" title="Đã xem"><i class="ion-eye"></i> Đã xem</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{UrlHelper::classUserProfileActiveUrl('member.favorited')}}"  href="{{route('member.favorited')}}" title="Quan tâm"><i class="ion-android-star"></i> Quan tâm</a>
    </li>
</ul>
