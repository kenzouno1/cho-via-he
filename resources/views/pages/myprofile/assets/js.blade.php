<script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_API','AIzaSyCcCGIu_Jz3R1wUtpLCtxjvNDNlueSCuTE')}}&libraries=places"
        async defer></script>
<script src="{!! asset('assets/vendor/crop-avata/jquery.cropit.js') !!}"></script>
<script src="{!! asset('assets/vendor/parsley/parsley.min.js') !!}"></script>
<script type="text/javascript">
    //hide form in profile on mobile
    $('.form-change-profile').on('click', '.click-hidde-form', function (event) {
        event.preventDefault();
        $(this).closest(".form-change-profile").find('fieldset').slideToggle('fast');
    });
    // upload avata
    $('.modal-upload-avata').on('click', '.btn-upload-avata', function (event) {
        event.preventDefault();
        $('.cropit-image-input').click();
    });
    //crop avata
    $('.image-editor').cropit({
        width: {{config('conf.avatar.size.w')}},
        height: {{config('conf.avatar.size.h')}}
    });

    $('.rotate-cw').click(function () {
        $('.image-editor').cropit('rotateCW');
    });
    $('.rotate-ccw').click(function () {
        $('.image-editor').cropit('rotateCCW');
    });
    //change image
    $('#wrapper').on('click', '.export', function (event) {
        event.preventDefault();
        var that = $(this).closest('#wrapper');
        var imageData = $('.image-editor').cropit('export');
        $.ajax({
            type: 'POST',
            url: '{{ route('member.change-avatar')}}',
            data: {imageData: imageData},
        }).done(function () {
            $('.modal-upload-avata .close').click();
            that.find('.form-upload-avata .avata img').attr('src', imageData);
        });
    });
    // close
    $('.btn-close').click(function (event) {
        event.preventDefault();
        $('.modal-upload-avata .close').click();
    });

    $('.form-upload-avata').on('click', '.upload-img-avata', function (event) {
        event.preventDefault();
        $('#file-upload').click();
    });
    //validate form new post
    $("#form-change-profile, #form-change-account").parsley({
        errorClass: 'has-danger',
        successClass: 'has-success',
        classHandler: function (ParsleyField) {
            return ParsleyField.$element.parents('.form-group');
        },
        errorsContainer: function (ParsleyField) {
            return ParsleyField.$element.parents('.form-group');
        },
        errorsWrapper: '<span class="form-control-feedback">',
        errorTemplate: '<div></div>'
    });
    //end validate form
    /*
     Location Name
     */
    function initMap() {
        var input = document.getElementById('location');
        new google.maps.places.Autocomplete(
            input,
            {
                types: ['geocode'],
                componentRestrictions: {country: "vn"}
            }
        );
    }

    $('.current-location').click(function (event) {
        event.preventDefault();
        if (navigator.geolocation) {

            navigator.geolocation.getCurrentPosition(function (position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var geocoder = new google.maps.Geocoder;
                geocoder.geocode({'location': pos}, function (results, status) {
                    if (status === 'OK') {
                        if (results[0]) {
                            get_adress(results);
                            $('#location').val(results[0].formatted_address);
                        } else {
                            window.alert('No results found');
                        }
                    } else {
                        window.alert('Geocoder failed due to: ' + status);
                    }
                });
            });
        } else {
            alert("Geolocation is not supported by this browser.");
        }
    });

    function get_adress(results) {
        var place_id = [];
        $.each(results, function (index) {
            $('#location').after('<input  name="location[' + index + '][place_id]" type="hidden" value="' + results[index].place_id + '">');
//            $('#location').after('<input  name="location['+index+'][order]" type="hidden" value="'+index+'">');
            $('#location').after('<input  name="location[' + index + '][name_adress]" type="hidden" value="' + results[index].formatted_address + '">');
        });

    }

</script>

