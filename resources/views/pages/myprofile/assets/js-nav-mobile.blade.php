<script src="{!! asset('assets/vendor/matchHeight/jquery.matchHeight.js') !!}"></script>
<script type="text/javascript">
    //menu mobile profile
    $('.menu-profile-mobile .icon-toggle').click(function (event) {
        $(this).closest('.menu-profile-mobile').next('.nav-tabs').slideToggle( "slow" );
    });
    //set max height for saved search
    $('.item-save-search').matchHeight();
    // set min height
    var height_post = $(window).height() - $('#site-header').height() - $('.banner-page-content').height() - $('#site-footer').height();
    $('.content-manage-post').css('min-height', height_post);
</script>