@extends('master')
@section('content-page')
    <main class="manage-page wrapper-main" role="main">
        <!-- wrapper-banner -->
        <div class="banner-page-content wrapper-banner"
             style="background-image:url('{{asset('assets/img/default-image/bg-banner.png')}}'); ">
        </div>
        <!-- end wrapper-banner -->

        <section class="content-manage-post section-content bg-graylight">
            <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="col-xl-10 ">
                        @include('pages.myprofile.nav-account')
                        <div class="p-md-4 p-3 bg-white">
                            @if(count($lstViewed)>0)
                                <div class="row-card">
                                    @foreach($lstViewed as $ads)
                                        <div class="col-card-4 mb-3">
                                            @include('loop.card-user-activity')
                                        </div>
                                    @endforeach
                                </div>
                            @else
                                @include('parts.viewed-empty')
                            @endif
                        </div>

                    </div>
                    {{--{{$lstAds->links()}}--}}
                </div>
            </div>

            </div>
        </section>
        <!-- end section contnet search -->
    </main>
@endsection
@push('script-custom')
    @include('pages.myprofile.assets.js-nav-mobile')
@endpush