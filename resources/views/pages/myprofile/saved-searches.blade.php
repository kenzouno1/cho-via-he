@extends('master')
@section('content-page')

    <main class="manage-page wrapper-main" role="main">
        <!-- wrapper-banner -->
        <div class="banner-page-content wrapper-banner"
             style="background-image:url('{{asset('assets/img/default-image/bg-banner.png')}}'); ">
        </div>
        <!-- end wrapper-banner -->

        <section class="content-manage-post section-content bg-graylight">
            <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="col-xl-10 ">
                        @include('pages.myprofile.nav-account')
                        <div class="tab-pane">
                            <div class="p-md-3 p-2 bg-white mb-3">
                                @if(count($lstSavedSearch)>0)
                                    <div class=" row">
                                        @foreach($lstSavedSearch as $key=> $search)
                                            <div class="col-md-4 col-sm-6">
                                                @include('parts.save-search')
                                            </div>
                                        @endforeach
                                    </div>
                                @else
                                    @include('parts.search-empty')
                                @endif
                            </div>
                            <!-- end -search -->
                            @if(count($lstSavedSearch)>0)
                                <div class="py-3 d-md-flex justify-content-md-between">
                                    {{--<p class="mb-md-0">** Bạn phải có từ khoá trong tìm kiếm đã lưu của bạn để chọn tuỳ chọn--}}
                                    {{--Ngay lập tức.</p>--}}
                                    <a class="btn btn-primary px-4 py-2 text-uppercase font-weight-bold"
                                       href="{{route('saved_search.remove.all')}}" title="Xóa tất cả">Xoá
                                        tất cả</a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end section contnet search -->
    </main>
@endsection
@push('script-custom')
    @include('pages.myprofile.assets.js-nav-mobile')
@endpush
