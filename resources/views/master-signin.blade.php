<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Template Chợ Vỉa Hè
    </title>
    <meta name="description" content="#">
    <meta name="keywords" content="#">
    <meta name="author" content="#">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
@include('layouts.favicon')
    <!-- normalize -->
    {{--<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700&amp;subset=vietnamese"
          rel="stylesheet">--}}
    {{--<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=vietnamese" rel="stylesheet">--}}
    <!-- <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> -->
    <link rel="stylesheet" href="{!! asset('assets/fonts/ionicons/css/ionicons.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/css/bootstrap.min.css') !!}">
    @stack('css-plugins')
    <link rel="stylesheet" href="{!! asset('assets/vendor/CustomScrollbar/jquery.mCustomScrollbar.min.css') !!}">
    @stack('css-custom')
    <!-- Custom CSS  -->
    <link rel="stylesheet" href="{!! asset('assets/css/main.min.css') !!}">
    <style type="text/css" media="screen">
        html,
          body {
              min-height: 100%;
          }
    </style>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
    <!-- Modernizr js -->
    <script src="{!! asset('assets/js/modernizr.min.js') !!}"></script>
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ])?>
    </script>
</head>

<body class="sign-page @stack('body-class')">
    <div id="wrapper">
        <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
      <![endif]-->
        @include('layouts.header')
        	@yield('content-page')
    </div>
    <!--Javascript Library -->
    <script src="{!! asset('assets/vendor/jquery2-2-4/jquery.min.js') !!}"></script>
    <!-- getbootstrap -->
    <script src="{!! asset('assets/js/tether.min.js') !!}"></script>
    <script src="{!! asset('assets/js/bootstrap.min.js') !!}"></script>
    @stack('script-plugins')
       <script src="{!! asset('assets/vendor/CustomScrollbar/jquery.mCustomScrollbar.js') !!}"></script>
    @stack('script-custom')
   <!-- Main Script -->
    <script src="{!! asset('assets/js/main.js') !!}"></script>
</body>

</html>
