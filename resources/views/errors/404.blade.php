@extends('master-404')
@section('content-page')
    <main class="wrapper-main" role="main">
        <div class="box-404 text-center">
            <img src="{{asset('assets/img/default-image/img-404.png')}}" alt="404">
            <h2>Rất tiếc bạn đang gặp phải</h2>
            <h1>lỗi 404</h1>
            <p>
                Trang bạn cần xem chúng tôi không thể tìm thấy !<br>
                Bạn có muốn quay <a href="{{ route('home')}}" title="Trang chủ">về trang chủ ?</a>
            </p>
        </div>
    </main>
@endsection