<div class="wapper-form-search">
	<div class="rst-search-form">
        <form class="clearfix" action="/" method="get" accept-charset="utf-8">
            <div class="rst-wrap-dropdown rst-wrap-dropdown-style1">
                <a class="btn-dropdown" href="#" title="advance"><i class="fa fa-cubes" aria-hidden="true"></i></a>
                <ul class="list-categories">
                    <li  class="has-child has-icon">
                     <i class="fa fa-tachometer" aria-hidden="true"></i>
                    	<span class="text">
                    		Tất cả danh mục
                    	</span>
                    </li>
                    <li  class="has-child has-icon">
                    <i class="fa fa-cutlery" aria-hidden="true"></i>
                    	<span class="text">
                    		Danh mục 1
                    	</span>
                    	<span class="btn-toggle">
			              <i class="fa fa-caret-down"></i>
			            </span>
						<ul>
							<li class="has-child">
								<span class="text"> Category 1</span>
								<span class="btn-toggle">
			                      <i class="fa fa-caret-down"></i>
			                    </span>
								<ul>
									<li class="has-child">
										<span class="text">
											Menu sub 1
										</span>
									</li>
									<li class="has-child">
										<span class="text">
											Menu sub 1
										</span>
									</li>
									<li class="has-child">
										<span class="text">
											Menu sub 1
										</span>
									</li>
									<li class="has-child">
										<span class="text">
											Menu sub 1
										</span>
									</li>
									<li class="has-child">
										<span class="text">
											Menu sub 1
										</span>
									</li>
								</ul>
							</li>
							<li class="has-child">
								<span class="text"> Category 2</span>
							</li>
							<li class="has-child">
								<span class="text"> Category 3</span>
							</li>
							<li class="has-child">
								<span class="text"> Category 4</span>
							</li>
							<li class="has-child">
								<span class="text"> Category 5</span>
							</li>
						</ul>
                    </li>
                    <li  class="has-child has-icon">
                    	<i class="fa fa-deaf" aria-hidden="true"></i>
                    	<span class="text">Danh mục 2</span>
                    </li>
                    <li  class="has-child has-icon">
                    	<i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                    	<span class="text">Danh mục 2</span>
                    </li>
                    <li  class="has-child has-icon">
                    	<i class="fa fa-id-card-o" aria-hidden="true"></i>
                    	<span class="text">Danh mục 2</span>
                    </li>
                    <li  class="has-child has-icon">
                    	<i class="fa fa-envelope-square" aria-hidden="true"></i>
                    	<span class="text">Danh mục 2</span>
                    </li>
                    <li  class="has-child has-icon">
                    	<i class="fa fa-eraser" aria-hidden="true"></i>
                    	<span class="text">Danh mục 2</span>
                    </li>
                    <li  class="has-child has-icon">
                    	<i class="fa fa-motorcycle" aria-hidden="true"></i>
                    	<span class="text">Danh mục 2</span>
                    </li>
                    <li  class="has-child has-icon">
                    	<i class="fa fa-money" aria-hidden="true"></i>
                    	<span class="text">Danh mục 2</span>
                    </li>
                    <li  class="has-child has-icon">
                    	<i class="fa fa-paw" aria-hidden="true"></i>
                    	<span class="text">Danh mục 2</span>
                    </li>
                </ul>
                <input type="hidden" name="advance">
            </div>
            <div class="input-search">
            	<input type="search" name="s" class="form-control " required="required" placeholder="I'm looking for...">
            </div>
            <div class="input-address">
            	<input type="text" name="s" class="form-control " required="required" placeholder="Vietname...">
            </div>
            <div class="rst-wrap-dropdown rst-wrap-dropdown-style2">
                <a href="#" title="advance">0km</a>
                <ul class="list-categories">
                    <li  class="has-child has-icon">
                    	<span class="text"> 0km </span>
                    </li>
                    <li  class="has-child has-icon">
                    	<span class="text">2km</span>
                    </li>
                    <li  class="has-child has-icon">
                    	<span class="span text">5km</span>
                    </li>
                    <li  class="has-child has-icon">
                    	<span class="text">10km</span>
                    </li>
                    <li  class="has-child has-icon">
                    	<span class="text">50km</span>
                    </li>
                    <li  class="has-child has-icon">
                    	<span class="text">100km</span>
                    </li>
                    <li  class="has-child has-icon">
                    	<span class="text">150km</span>
                    </li>
                    <li  class="has-child has-icon">
                    	<span class="text">200km</span>
                    </li>
                    <li  class="has-child has-icon">
                    	<span class="text">250km</span>
                    </li>
                </ul>
                <input type="hidden" name="advance">
            </div>
            <button class="button-search">
                <i class="fa fa-search"></i>
            </button>
        </form>
    </div>
</div>
{{-- end form search --}}