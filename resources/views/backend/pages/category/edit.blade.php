@extends('backend.admin') @section('page-head')
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ route('admin.dashboard') }}">Bảng Tin</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('admin.category.list') }}">Danh Mục</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Sửa Danh Mục</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- END PAGE HEADER-->
@endsection @section('page-content')

    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa-pencil-square-o fa"></i>Sửa danh mục
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form action="{{ route('admin.category.edit',['id'=>$id]) }}" method="post" class="form-horizontal" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-body">
                    <div class="form-group {{ validate_form_class('name',$errors) }}">
                        <label class="col-md-3 control-label">Tên Danh Mục</label>
                        <div class="col-md-4">
                            <input type="text" value="{{ old('name', $category->name )}}" name="name"
                                   class="form-control">
                            {{ render_validate_msg('name',$errors) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Danh Mục Cha</label>
                        <div class="col-md-4">
                            <select id="list-cate" name="parent" class="form-control select2">
                                <option value="0">Chọn danh mục</option>
                                @foreach ($categories as $item)
                                    <option value="{{ $item->id }}" {{ selected(old('parent',$category->parent),$item->id)  }}>{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group last {{ validate_form_class('icon',$errors) }}">
                        <label class="col-md-3 control-label">Icon Class</label>
                        <div class="col-md-4">
                            <input type="text" value="{{ old('icon', $category->image )}}" name="icon"
                                   class="form-control ">
                            {{ render_validate_msg('icon',$errors) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-md-push-3">
                            {{ render_success_msg($errors) }}
                        </div>
                    </div>
                </div>
                <div class="form-group last">
                    <div class="form-group {{ validate_form_class('avatar',$errors) }}">
                        <label class="col-md-3 control-label">Ảnh Đại Diện</label>
                        <div class="col-md-9">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                    <img src="{{!is_null($category->thumbnail) ? asset($category->thumbnail) : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image'}}"
                                         alt=""/>
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail"
                                     style="max-width: 200px; max-height: 150px;"></div>
                                <div>
                                    <span class="btn default btn-file">
                                        <span class="fileinput-new"> Chọn ảnh </span>
                                        <span class="fileinput-exists"> Đổi ảnh khác </span>
                                        <input type="file" name="thumb" accept="image/*"> </span>
                                    <a href="javascript:;" class="btn red fileinput-exists"
                                       data-dismiss="fileinput">
                                        Xóa </a>
                                </div>
                            </div>
                            {{ render_validate_msg('avatar',$errors) }}
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <button type="submit" class="btn btn-circle green">Lưu</button>
                        </div>
                    </div>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
@endsection
@push('script-plugins')
    <script src="{{ asset('backend/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('backend/assets/global/plugins/select2/js/select2.full.min.js')}}"
            type="text/javascript"></script>
@endpush
@push('script-custom')
    <script type="text/javascript">
        $.fn.select2.defaults.set("theme", "bootstrap");
        $(".select2").select2({
            width: null
        });
    </script>
@endpush
@push('css-plugins')
    <link rel="stylesheet"
          href="{{ asset('backend/assets/global/plugins/jquery-file-upload/css/jquery.fileupload.css')}}">
    <link rel="stylesheet"
          href="{{ asset('backend/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}">
    <link rel="stylesheet" href="{{ asset('backend/assets/global/plugins/select2/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{ asset('backend/assets/global/plugins/select2/css/select2-bootstrap.min.css')}}">
@endpush