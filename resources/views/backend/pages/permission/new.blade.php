@extends('backend.admin') @section('page-head')
<!-- BEGIN PAGE HEADER-->
<!-- BEGIN PAGE BAR -->
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}">Dashboard</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('admin.roles.list') }}">Nhóm Quyền</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Thêm Nhóm Quyền</span>
        </li>
    </ul>
</div>
<!-- END PAGE BAR -->
<!-- END PAGE HEADER-->
@endsection @section('page-content')

<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus"></i>Thêm Nhóm Quyền</div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="{{ route('admin.roles.new') }}" method="post" class="form-horizontal">
            {{ csrf_field() }}
            <div class="form-body">
                <div class="form-group {{ validate_form_class('name',$errors) }}">
                    <label class="col-md-3 control-label">Tên Nhóm Quyền</label>
                    <div class="col-md-4">
                        <input type="text" name="name" class="form-control">
                        {{ render_validate_msg('name',$errors) }}
                    </div>
                </div>
                <div class="form-group last">
                    <label class="col-md-3 control-label">Chọn Quyền Hạn</label>
                    <div class="col-md-7">
                        <div class="mt-checkbox-inline row">
                        @foreach ($capabilities as $key=> $cap)
                        <div class="col-md-4">
                                <label class="mt-checkbox">
                                    <input name="cap[]" value="{{ $key }}" type="checkbox" {{ checked(old('cap'),$key) }}> {{  $cap}}
                                    <span></span>
                                </label>
                            </div>
                        @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-push-3">
                    {{ render_success_msg($errors) }}
                </div>
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                        <button type="submit" class="btn btn-circle green">Lưu</button>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>
@endsection


@push('css-plugins')
    <link rel="stylesheet" href="{{ asset('backend/assets/global/css/components.min.css')}}">
@endpush