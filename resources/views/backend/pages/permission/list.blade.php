@extends('backend.admin') @section('page-head')
<!-- BEGIN PAGE HEADER-->
<!-- BEGIN PAGE BAR -->
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}">Dashboard</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('admin.roles.list') }}">Nhóm Quyền</a>
            <i class="fa fa-circle"></i>
        </li>
    </ul>
</div>
<!-- END PAGE BAR -->
<!-- END PAGE HEADER-->
@endsection @section('page-content')
<div class="portlet light portlet-fit portlet-datatable bordered">
    <div class="portlet-title">
        <div class="row">
            <div class="caption col-md-4">
                <i class="fa-list fa font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase">Nhóm Quyền</span>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                                <a href="{{ route('admin.roles.new') }}" class="btn sbold green"> Thêm Nhóm Quyền
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="actions">
                                <div class="dt-buttons table-btn pull-right">
                                    <a data-class=".buttons-print" class="dt-button buttons-print btn dark btn-outline" tabindex="0" aria-controls="list-roles" href="#"><span>Print</span></a>
                                    <a data-class=".buttons-pdf" class="dt-button buttons-pdf buttons-html5 btn green btn-outline" tabindex="0" aria-controls="list-roles" href="#"><span>PDF</span></a>
                                    <a data-class=".buttons-excel" class="dt-button buttons-excel buttons-html5 btn yellow btn-outline" tabindex="0" aria-controls="list-roles" href="#"><span>Excel</span></a>
                                    <a data-class=".buttons-csv" class="dt-button buttons-csv buttons-html5 btn purple btn-outline" tabindex="0" aria-controls="list-roles" href="#"><span>CSV</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-bordered table-hover " id="list-roles">
                    <thead>
                        <tr role="row" class="heading">
                            <th width="75%"> Nhóm Quyền </th>
                            <th> Tác Vụ </th>
                        </tr>
                    </thead>
                    <tbody> </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
{{--<div id="select-role" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false" aria-hidden="true">--}}
        {{--<div class="modal-body">--}}
        {{--<div class="form-group">--}}
            {{--<label for="">Chuyển toàn bộ nhân viên thuộc nhóm này sang</label>--}}
               {{--<select name="change_role"></select>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<div class="modal-footer">--}}
            {{--<button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>--}}
            {{--<button type="button" class="btn green continue">Continue Task</button>--}}
        {{--</div>--}}
    {{--</div>--}}
@endsection @push('script-custom')
<script type="text/javascript">
jQuery(document).ready(function($) {
var current_id = 0;
    var exportSetting = {
        columns: [1, 2]
    };
    var exportTitle = ' Nhom Quyen Cho Via He'
    var list = $('#list-roles').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            type: 'POST',
            url: "{{ route('admin.roles.list') }}",
        },
        "columns": [ {
            data: 'name',
            name: 'name'
        }, {
            data: 'action',
            name: 'action',
            orderable: false,
            searchable: false,
        }],
        "language": {
            "emptyTable": "Không tìm thấy dữ liệu",
            "info": "Đang hiển thị kết quả từ _START_ tới _END_ trên tổng số _TOTAL_ kết quả",
            "infoEmpty": "Không có kết quả",
            "infoFiltered": "(trên tổng số  _MAX_ kết quả được tìm thấy)",
            "lengthMenu": "Số dòng hiển thị _MENU_",
            "search": "Tìm kiếm:",
            'processing': 'Đang xử lý....',
            "zeroRecords": "Không có kết quả phù hợp",
            "paginate": {
                "previous": "Trở lại",
                "next": "Tiếp",
                "last": "Cuối",
                "first": "Đầu"
            }
        },
        "lengthMenu": [
            [5, 10, 15, 20, -1],
            [5, 10, 15, 20, "All"]
        ],
        "order": [
            [0, "asc"]
        ],
        // set the initial value
        "pageLength": 20,
        "pagingType": "bootstrap_full_number",
        buttons: [{
            extend: 'print',
            className: 'dt-button buttons-print',
            exportOptions: exportSetting,
            title: exportTitle,
        }, {
            extend: 'pdf',
            className: 'btn green btn-outline',
            exportOptions: exportSetting,
            title: exportTitle,
        }, {
            extend: 'excel',
            className: 'btn yellow btn-outline ',
            exportOptions: exportSetting,
            title: exportTitle,
        }, {
            extend: 'csv',
            className: 'btn purple btn-outline ',
            exportOptions: exportSetting,
            title: exportTitle,
        }, ],
    });
    $('.table-btn a').click(function(e) {
        e.preventDefault();
        $('#list-roles').DataTable().button($(this).data('class')).trigger()
    });
    {{--var rolesStr= '{{$roles}}';--}}
    {{--var roles = jQuery.parseJSON(rolesStr.replace(/&quot;/g,'"'));--}}
    {{--var data=[];--}}
        {{--$.each(roles, function(index, val) {--}}
            {{--data.push({id:val.id,text:val.name});--}}
        {{--});--}}
//     var select = $('select[name="change_role"]').select2({width:null,data:data});
    $('#list-roles').on('click', '.btn-delete', function(event) {
        event.preventDefault();
         current_id = $(this).data('id');
        var name = $(this).data('name');
//        var newData = data.filter(function(option){
//            return this != option.id;
//        },current_id);
//        newData.unshift({id:0,text:'Chọn nhóm quyền mới'});
//        $('select[name="change_role"]').select2('destroy').find('option').remove();
//        $('select[name="change_role"]').select2({data:newData});
//        $('#select-role').modal('show');

        alertify.confirm("Xác Nhận Xóa Nhóm Quyền " + name, function() {
            $.ajax({
                url: '{{ route('admin.roles.del')}}',
                type: 'POST',
                data: {
                    id: current_id,
//                newid:newId
                },
            })
                .done(function () {
                    setTimeout(function () {
                        $('#list-roles').DataTable().ajax.reload();
                    }, 200);
                });
        });
    });
    $('.continue').click(function() {
//        var newId = $('select[name="change_role"]').val();
//
//        if(newId==0){
//
//        }else{

            $.ajax({
            url: '{{ route('admin.roles.del')}}',
            type: 'POST',
            data: {
                id: current_id,
//                newid:newId
            },
        })
        .done(function() {
//         for(var i=0;i<data.length;i++){
//             var item = data[i];
//             if (item.id === parseInt(current_id)) {
//                 data.splice(i, 1);
//                 current_id=0;
//             }
//         }
            setTimeout(function() {
//                $('#select-role').modal('hide');
                $('#list-roles').DataTable().ajax.reload();
            }, 200);
        });
//        }
    });
    $.fn.select2.defaults.set("theme", "bootstrap");

});
</script>
@endpush
@push('css-plugins')
<link href="{{ asset('backend/assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ asset('backend/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.min.css')}}" rel="stylesheet" type="text/css" /> 
<link rel="stylesheet" href="{{ asset('backend/assets/global/plugins/alertify/alertify.min.css')}}">
<link rel="stylesheet" href="{{ asset('backend/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css')}}">
<link rel="stylesheet" href="{{ asset('backend/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css')}}">
    <link rel="stylesheet" href="{{ asset('backend/assets/global/plugins/select2/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{ asset('backend/assets/global/plugins/select2/css/select2-bootstrap.min.css')}}">
@endpush
 @push('script-plugins')
 <script src="{{ asset('backend/assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('backend/assets/global/scripts/datatable.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('backend/assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('backend/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('backend/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('backend/assets/global/plugins/alertify/alertify.min.js')}}" type="text/javascript"></script>
@endpush @push('css-custom')
<style type="text/css" media="screen">
#list-roles_filter .select2-container {
    display: inline-block;
    padding-left: 10px;
    min-width: 150px;
}

#list-roles_filter label + label {
    margin-left: 10px;
}
</style>
@endpush
