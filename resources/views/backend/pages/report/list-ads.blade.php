@extends('backend.admin') @section('page-head')
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('admin.report.ads') }}">Báo cáo tin
                </a>
                <i class="fa fa-circle"></i>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- END PAGE HEADER-->
@endsection @section('page-content')
    <div class="portlet light portlet-fit portlet-datatable bordered">
        <div class="portlet-title">
            <div class="row">
                <div class="caption col-md-4">
                    <i class="fa-list fa font-dark"></i>
                    <span class="caption-subject font-dark sbold uppercase">Danh sách báo cáo tin</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    {{--<a href="{{ route('admin.member.new') }}" class="btn sbold green"> Thêm Thành Viên--}}
                                    {{--<i class="fa fa-plus"></i>--}}
                                    {{--</a>--}}
                                    <a data-table="#list-report"
                                       data-route="#" href="#"
                                       class="btn sbold red disabled remove-selected"> Xóa Báo cáo Được Chọn
                                        <i class="fa fa-remove"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="actions">
                                    <div class="dt-buttons table-btn pull-right">
                                        <a data-class=".buttons-print"
                                           class="dt-button buttons-print btn dark btn-outline" tabindex="0"
                                           aria-controls="list-cate" href="#"><span>Print</span></a>
                                        <a data-class=".buttons-pdf"
                                           class="dt-button buttons-pdf buttons-html5 btn green btn-outline"
                                           tabindex="0" aria-controls="list-cate" href="#"><span>PDF</span></a>
                                        <a data-class=".buttons-excel"
                                           class="dt-button buttons-excel buttons-html5 btn yellow btn-outline"
                                           tabindex="0" aria-controls="list-cate" href="#"><span>Excel</span></a>
                                        <a data-class=".buttons-csv"
                                           class="dt-button buttons-csv buttons-html5 btn purple btn-outline"
                                           tabindex="0" aria-controls="list-cate" href="#"><span>CSV</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-checkable" id="list-report">
                        <thead>
                        <tr role="row" class="heading">
                            <th width="2%" class="sorting_disabled" rowspan="1" colspan="1">
                                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                    <input type="checkbox" class="group-checkable" data-set="#list-report">
                                    <span></span>
                                </label>
                            </th>
                            <th width="3%"> ID</th>
                            <th width="20%"> Tiêu đề bài viết</th>
                            <th width="10%"> Lí do</th>
                            <th width="25%"> Tin nhắn</th>
                            <th width="20%"> Email người báo</th>
                            <th width="7%"> SĐT người báo</th>
                            <th width="8%"> Thời gian báo</th>
                            <th width="7%"> Tác Vụ</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script-custom')
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            var exportSetting = {
                columns: [1, 2, 3, 4]
            };
            var exportTitle = 'Danh sách báo cáo tin'
            var list = $('#list-report').dataTable({
                "processing": true,
                "serverSide": true,
                "ajax": {
                    type: 'POST',
                    url: "{{ route('admin.report.ads') }}",
                    data: function (d) {
//                    d.name = $('#searchByName').val();
//                    d.roles = $('#searchByRole').val() !== 0 ? $('#searchByRole').val() : 2;
                    },
                },
                "columns": [{
                    data: 'checkbox',
                    name: 'id',
                    orderable: false,
                }, {
                    data: 'id',
                    name: 'id',
                    searchable: false,
                    visible: false,
                }, {
                    data: 'title',
                    name: 'title'
                }, {
                    data: 'reason',
                    name: 'reason'
                }, {
                    data: 'message',
                    name: 'message'
                }, {
                    data: 'email',
                    name: 'email',
                }, {
                    data: 'phone',
                    name: 'phone',
                    "orderable": "false",
                    searchable: false,
                }, {
                    data: 'created_at',
                    name: 'created_at',
                    "orderable": true,
                    searchable: false,
                }, {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false,
                }],
                "language": {
                    "emptyTable": "Không tìm thấy dữ liệu",
                    "info": "Đang hiển thị kết quả từ _START_ tới _END_ trên tổng số _TOTAL_ kết quả",
                    "infoEmpty": "Không có kết quả",
                    "infoFiltered": "(trên tổng số  _MAX_ kết quả được tìm thấy)",
                    "lengthMenu": "Số dòng hiển thị _MENU_",
                    "search": "Tìm kiếm:",
                    'processing': 'Đang xử lý....',
                    "zeroRecords": "Không có kết quả phù hợp",
                    "paginate": {
                        "previous": "Trở lại",
                        "next": "Tiếp",
                        "last": "Cuối",
                        "first": "Đầu"
                    }
                },
                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],
                "order": [
                    [1, "asc"]
                ],
                // set the initial value
                "pageLength": 20,
                "pagingType": "bootstrap_full_number",
                buttons: [{
                    extend: 'print',
                    className: 'dt-button buttons-print',
                    exportOptions: exportSetting,
                    title: exportTitle,
                }, {
                    extend: 'pdf',
                    className: 'btn green btn-outline',
                    exportOptions: exportSetting,
                    title: exportTitle,
                }, {
                    extend: 'excel',
                    className: 'btn yellow btn-outline ',
                    exportOptions: exportSetting,
                    title: exportTitle,
                }, {
                    extend: 'csv',
                    className: 'btn purple btn-outline ',
                    exportOptions: exportSetting,
                    title: exportTitle,
                },],

            });
            $('.table-btn a').click(function (e) {
                e.preventDefault();
                $('#list-report').DataTable().button($(this).data('class')).trigger();
            });
            /* Hidden Post - Repost */
            $('#list-report_wrapper').on("click", ".hidden-post", function () {
                var row = $(this).closest('tr');
                var ads_id = $(this).data('target');
                var report_id = $(this).data('report');
                alertify.confirm("Không cho tin này hiển thị trên hệ thống",
                    function () {
                        $.ajax({
                            url: '{{route('admin.report.hiddenAds')}}',
                            type: 'POST',
                            data: {
                                ads_id: ads_id,
                                report_id: report_id
                            }
                        }).done(function (response) {
                            $('#list-report').DataTable().ajax.reload();
                            alertify.success('Đã ẩn tin');
                        })
                    },
                    function () {
                        alertify.error('Cancel');
                    });

            });
            /* Delete Post - Repost */
            $('#list-report_wrapper').on("click", ".del-post", function () {
                var row = $(this).closest('tr');
                var ads_id = $(this).data('target');
                var report_id = $(this).data('report');
                alertify.confirm("Xóa Tin này!",
                    function () {
                        $.ajax({
                            url: '{{route('admin.report.deleteAds')}}',
                            type: 'POST',
                            data: {
                                ads_id: ads_id,
                                report_id: report_id
                            }
                        }).done(function (response) {
                            $('#list-report').DataTable().ajax.reload();
                            alertify.success('Đã xoá tin');
                        })
                    },
                    function () {
                        alertify.error('Cancel');
                    });

            });
            /* Delete Repost */
            $('#list-report_wrapper').on("click", ".del-report", function () {
                var row = $(this).closest('tr');
                var report_id = $(this).data('report');
                alertify.confirm("Xóa báo cáo này !",
                    function () {
                        $.ajax({
                            url: '{{route('admin.report.deleteReport')}}',
                            type: 'POST',
                            data: {
                                report_id: report_id,
                            }
                        }).done(function (response) {
                            $('#list-report').DataTable().ajax.reload();
                            alertify.success('Đã xóa báo cáo');
                        })
                    },
                    function () {
                        alertify.error('Cancel');
                    });

            });
            $('.group-checkable').change(function (event) {
                var checked = $(this).is(":checked");
                $('input[name="id[]"]').prop('checked', checked);
                if (checked) {
                    $('.remove-selected').removeClass('disabled');
                } else {
                    $('.remove-selected').addClass('disabled');
                }
            });
            $('table').on('change', 'input[name="id[]"]', function (event) {
                if (!$(this).is(":checked")) {
                    $('.group-checkable').prop('checked', false);
                }
                if ($('input[name="id[]"]:checked').length) {
                    $('.remove-selected').removeClass('disabled');
                } else {
                    $('.remove-selected').addClass('disabled');
                }
            });
            $('.remove-selected').click(function (event) {
                event.preventDefault();
                alertify.confirm("Xác Nhận Xóa Các Thành Viên Đang Chọn", function () {
                    var ids = [];
                    $('input[name="id[]"]:checked').each(function (index, el) {
                        ids[index] = $(this).val();
                    });
                    console.log(ids);
                    $.ajax({
                        type: 'POST',
                        url: '{{ route('admin.report.removeSelected')}}',
                        data: {id: ids},
                    }).done(function () {
                        $('#list-report').DataTable().ajax.reload();
                    });
                })
            });
        });
    </script>
    <style>
        #modal-blockmember {
            top: 50%;
            transform: translateY(-50%);
        }
    </style>
    @stack('css-custom')
@endpush @push('css-plugins')
    <link href="{{ asset('backend/assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('backend/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.min.css')}}"
          rel="stylesheet" type="text/css"/>
@endpush
<link rel="stylesheet" href="{{ asset('backend/assets/global/plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet"
      href="{{ asset('backend/assets/global/plugins/select2/css/select2-bootstrap.min.css')}}">
@push('script-plugins')
    <script src="{{ asset('backend/assets/global/scripts/datatable.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('backend/assets/global/plugins/datatables/datatables.min.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('backend/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('backend/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.min.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('backend/assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('backend/assets/global/plugins/select2/js/select2.full.min.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('backend/assets/global/plugins/alertify/alertify.min.js')}}" type="text/javascript"></script>
@endpush