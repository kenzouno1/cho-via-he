@extends('backend.admin') @section('page-head')
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('admin.package.list') }}">Gói Cước</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Thêm Gói Cước</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- END PAGE HEADER-->
@endsection @section('page-content')
    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-plus"></i>Sửa Gói Cước
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form action="{{ route('admin.package.edit',['id'=>$package->id]) }}" method="post" class="form-horizontal">
                {{ csrf_field() }}
                <div class="form-body">
                    <div class="form-group {{ validate_form_class('name',$errors) }}">
                        <label class="col-md-3 control-label">Tên Gói Cước</label>
                        <div class="col-md-4">
                            <input type="text" name="name" class="form-control" value="{{old('name',$package->name)}}">
                            {{ render_validate_msg('name',$errors) }}
                        </div>
                    </div>
                    <div class="form-group {{ validate_form_class('days',$errors) }}">
                        <label class="col-md-3 control-label">Số ngày</label>
                        <div class="col-md-4">
                            <input type="text" name="days" class="form-control" value="{{old('days',$package->days)}}">
                            {{ render_validate_msg('days',$errors) }}
                        </div>
                    </div>
                    <div class="form-group {{ validate_form_class('price',$errors) }}">
                        <label class="col-md-3 control-label">Giá tiền</label>
                        <div class="col-md-4">
                            <input type="text" name="price" class="form-control"
                                   value="{{old('price',$package->price)}}">
                            {{ render_validate_msg('price',$errors) }}
                        </div>
                    </div>
                    <div class="form-group  {{ validate_form_class('features',$errors) }}">
                        @php
                        $type = $package->type=='package' ? 'checkbox' : 'radio';
                        @endphp
                        <label class="col-md-3 control-label">Chọn tính năng</label>
                        <div class="col-md-7">
                            <div class="mt-{{$type}}-inline ">
                                <div class="row">@foreach ($lstFeatures as $key=> $feature)
                                        <div class="col-md-4">
                                            <label class="mt-{{$type}}">
                                                <input name="features[]" value="{{ $key }}"
                                                       type="{{$type}}" {{ checked(old('features',$package->get_option('features')),$key) }}> {{ $feature['label'] }}
                                                <span></span>
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                                {{ render_validate_msg('features',$errors) }}
                            </div>
                        </div>
                    </div>
                    @if($package->type=='package')
                    <div class="form-group {{ validate_form_class('description',$errors) }}">
                        <label class="col-md-3 control-label">Mô tả</label>
                        <div class="col-md-4">
                            <textarea name="description" class="form-control">{{old('description',$package->description)}}</textarea>
                            {{ render_validate_msg('description',$errors) }}
                        </div>
                    </div>
                    @endif
                </div>
                <div class="row">
                    <div class="col-md-4 col-md-push-3">
                        {{ render_success_msg($errors) }}
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <button type="submit" class="btn btn-circle green">Lưu</button>
                        </div>
                    </div>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
@endsection
@push('css-plugins')
<link rel="stylesheet" href="{{ asset('backend/assets/global/css/components.min.css')}}">
@endpush