@extends('backend.admin') @section('page-head')
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('admin.package.list') }}">Gói Cước</a>
                <i class="fa fa-circle"></i>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- END PAGE HEADER-->
@endsection @section('page-content')
    <div class="portlet light portlet-fit portlet-datatable bordered">
        <div class="portlet-title">
            <div class="row">
                <div class="caption col-md-4">
                    <i class="fa-list fa font-dark"></i>
                    <span class="caption-subject font-dark sbold uppercase">Gói Cước</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <a href="{{ route('admin.category.new') }}" class="btn sbold green"> Thêm Gói Cước
                                        <i class="fa fa-plus"></i>
                                    </a>
                                    <a data-table="#list-package" data-route="{{ route('admin.package.remove_selected') }}" href="#" class="btn sbold red disabled remove-selected"> Xóa Gói Cước Được Chọn
                                        <i class="fa fa-remove"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="actions">
                                    <div class="dt-buttons table-btn pull-right">
                                        <a data-class=".buttons-print" class="dt-button buttons-print btn dark btn-outline" tabindex="0" aria-controls="list-package" href="#"><span>Print</span></a>
                                        <a data-class=".buttons-pdf" class="dt-button buttons-pdf buttons-html5 btn green btn-outline" tabindex="0" aria-controls="list-package" href="#"><span>PDF</span></a>
                                        <a data-class=".buttons-excel" class="dt-button buttons-excel buttons-html5 btn yellow btn-outline" tabindex="0" aria-controls="list-package" href="#"><span>Excel</span></a>
                                        <a data-class=".buttons-csv" class="dt-button buttons-csv buttons-html5 btn purple btn-outline" tabindex="0" aria-controls="list-package" href="#"><span>CSV</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-checkable" id="list-package">
                        <thead>
                        <tr role="row" class="heading">

                            <th> ID</th>
                            <th width="25%"> Tên Gói Cước </th>
                            <th width="15%"> Giá tiền </th>
                            <th width="22%"> Thời hạn </th>
                            <th width="22%"> Loại Gói </th>
                            <th width="10%"> Tác Vụ </th>
                        </tr>
                        </thead>
                        <tbody> </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection @push('script-custom')
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        var exportSetting = {
            columns: [0,1, 2, 3, 4]
        };
        var exportTitle = 'Member Cho Via He'
        var list = $('#list-package').dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                type: 'POST',
                url: "{{ route('admin.package.list') }}",
                data: function (d) {
//                    d.name = $('#searchByName').val();
//                    d.roles = $('#searchByRole').val() !== 0 ? $('#searchByRole').val() : 2;
                },
            },
            "columns": [ {
                data: 'id',
                name: 'id',
                searchable: false,
            }, {
                data: 'name',
                name: 'name'
            }, {
                data: 'price',
                name: 'price',
            }, {
                data: 'days',
                name: 'days',
                "orderable": true,
                searchable: false,
            },{
                data: 'type',
                name: 'type',
                "orderable": true,
                searchable: false,
            },{
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false,
            }],
            "language": {
                "emptyTable": "Không tìm thấy dữ liệu",
                "info": "Đang hiển thị kết quả từ _START_ tới _END_ trên tổng số _TOTAL_ kết quả",
                "infoEmpty": "Không có kết quả",
                "infoFiltered": "(trên tổng số  _MAX_ kết quả được tìm thấy)",
                "lengthMenu": "Số dòng hiển thị _MENU_",
                "search": "Tìm kiếm:",
                'processing': 'Đang xử lý....',
                "zeroRecords": "Không có kết quả phù hợp",
                "paginate": {
                    "previous": "Trở lại",
                    "next": "Tiếp",
                    "last": "Cuối",
                    "first": "Đầu"
                }
            },
            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],
            "order": [
                [1, "asc"]
            ],
            // set the initial value
            "pageLength": 20,
            "pagingType": "bootstrap_full_number",
            buttons: [{
                extend: 'print',
                className: 'dt-button buttons-print',
                exportOptions: exportSetting,
                title: exportTitle,
            }, {
                extend: 'pdf',
                className: 'btn green btn-outline',
                exportOptions: exportSetting,
                title: exportTitle,
            }, {
                extend: 'excel',
                className: 'btn yellow btn-outline',
                exportOptions: exportSetting,
                title: exportTitle,
            }, {
                extend: 'csv',
                className: 'btn purple btn-outline ',
                exportOptions: exportSetting,
                title: exportTitle,
            },],

        });
        $.fn.select2.defaults.set("theme", "bootstrap");
        $(".select2").select2({
            width: null
        });
        $('.table-btn a').click(function(e) {
            e.preventDefault();
            $('#list-package').DataTable().button($(this).data('class')).trigger()
        });
        $('#list-package').on('click', '.btn-delete', function(event) {
            event.preventDefault();
            var id = $(this).data('id');
            var name = $(this).data('name');
            alertify.confirm("Xác Nhận Gói " + name, function() {
                $.ajax({
                    url: '{{ route('admin.package.del')}}',
                    type: 'POST',
                    data: {
                        id: id
                    },
                })
                .done(function() {
                    setTimeout(function() {
                        $('#list-package').DataTable().ajax.reload();
                    }, 200);
                });
            })

        });
    });
</script>
@endpush @push('css-plugins')
<link href="{{ asset('backend/assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ asset('backend/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.min.css')}}" rel="stylesheet" type="text/css" /> @endpush
<link rel="stylesheet" href="{{ asset('backend/assets/global/plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{ asset('backend/assets/global/plugins/select2/css/select2-bootstrap.min.css')}}">
<link rel="stylesheet" href="{{ asset('backend/assets/global/plugins/alertify/alertify.min.css')}}"> @push('script-plugins')
<script src="{{ asset('backend/assets/global/scripts/datatable.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('backend/assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('backend/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('backend/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('backend/assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('backend/assets/global/plugins/alertify/alertify.min.js')}}" type="text/javascript"></script>
@endpush @push('css-custom')
<style type="text/css" media="screen">
    #list-package_filter label + label {
        margin-left: 10px;
    }
    .remove-selected {
        margin-left: 10px!important;
    }
</style>
@endpush
