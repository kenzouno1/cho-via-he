@extends('backend.admin') @section('page-head')
<!-- BEGIN PAGE HEADER-->
<!-- BEGIN PAGE BAR -->
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}">Bảng Tin</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('admin.employee.list') }}">Nhân Viên</a>
            <i class="fa fa-circle"></i>
        </li>
    </ul>
</div>
<!-- END PAGE BAR -->
<!-- END PAGE HEADER-->
@endsection @section('page-content')
<div class="portlet light portlet-fit portlet-datatable bordered">
    <div class="portlet-title">
        <div class="row">
            <div class="caption col-md-4">
                <i class="fa-list fa font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase">Danh Sách Tài Khoản</span>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                                <a href="{{ route('admin.employee.new') }}" class="btn sbold green"> Thêm Tài Khoản
                                    <i class="fa fa-plus"></i>
                                </a>
                                <a data-table="#list-user" data-route="{{ route('admin.employee.remove_selected') }}" href="#" class="btn sbold red disabled remove-selected"> Xóa Tài Khoản Được Chọn
                                    <i class="fa fa-remove"></i>
                                </a>
                        </div>
                        <div class="col-md-6">
                            <div class="actions">
                                <div class="dt-buttons table-btn pull-right">
                                    <a data-class=".buttons-print" class="dt-button buttons-print btn dark btn-outline" tabindex="0" aria-controls="list-user" href="#"><span>Print</span></a>
                                    <a data-class=".buttons-pdf" class="dt-button buttons-pdf buttons-html5 btn green btn-outline" tabindex="0" aria-controls="list-user" href="#"><span>PDF</span></a>
                                    <a data-class=".buttons-excel" class="dt-button buttons-excel buttons-html5 btn yellow btn-outline" tabindex="0" aria-controls="list-user" href="#"><span>Excel</span></a>
                                    <a data-class=".buttons-csv" class="dt-button buttons-csv buttons-html5 btn purple btn-outline" tabindex="0" aria-controls="list-user" href="#"><span>CSV</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-bordered table-hover table-checkable" id="list-user">
                    <thead>
                        <tr role="row" class="heading">
                            <th width="2%" class="sorting_disabled" rowspan="1" colspan="1">
                                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                    <input type="checkbox" class="group-checkable" >
                                </label>                                    <span></span>

                            </th>
                               <th width="0%">ID</th>
                            <th width="25%">Tên</th>
                            <th width="28%">Email </th>
                            <th width="15%">Điện Thoại</th>
                            <th width="10%">Trạng Thái </th>
                            <th width="12%"> Quyền Hạn </th>
                            <th width="8%"> Tác Vụ </th>
                        </tr>
                    </thead>
                    <tbody> </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection @push('script-custom')`
<script type="text/javascript">
jQuery(document).ready(function($) {
    var exportSetting = {
        columns: [1, 2, 3, 4]
    };
    var exportTitle = 'Member Cho Via He'
    var list = $('#list-user').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            type: 'POST',
            url: "{{ route('admin.employee.list') }}",
             data: function(d) {
                d.name = $('#searchByName').val();
                d.roles = $('#searchByRole').val() !==0 ? $('#searchByRole').val() : 2;
            },
        },
        "columns": [{
            data: 'checkbox',
            name: 'id',
            orderable: false,
        }, {
            data: 'id',
            name: 'id',
            searchable: false,
            visible:false,
        }, {
            data: 'name',
            name: 'name'
        }, {
            data: 'email',
            name: 'email',
        },  {
            data: 'phone',
            name: 'phone',
            "orderable": "false",
            searchable: false,
        }, {
            data: 'status',
            name: 'status',
            "orderable": false,
            searchable: false,
        }, {
            data: 'roles',
            name: 'roles',
            "orderable": "false",
            searchable: false,
        }, {
            data: 'action',
            name: 'action',
            orderable: false,
            searchable: false,
        }],
        "language": {
            "emptyTable": "Không tìm thấy dữ liệu",
            "info": "Đang hiển thị kết quả từ _START_ tới _END_ trên tổng số _TOTAL_ kết quả",
            "infoEmpty": "Không có kết quả",
            "infoFiltered": "(trên tổng số  _MAX_ kết quả được tìm thấy)",
            "lengthMenu": "Số dòng hiển thị _MENU_",
            "search": "Tìm kiếm:",
            'processing': 'Đang xử lý....',
            "zeroRecords": "Không có kết quả phù hợp",
            "paginate": {
                "previous": "Trở lại",
                "next": "Tiếp",
                "last": "Cuối",
                "first": "Đầu"
            }
        },
        "lengthMenu": [
            [5, 10, 15, 20, -1],
            [5, 10, 15, 20, "All"]
        ],
        "order": [
            [1, "asc"]
        ],
        // set the initial value
        "pageLength": 20,
        "pagingType": "bootstrap_full_number",
        buttons: [{
            extend: 'print',
            className: 'dt-button buttons-print',
            exportOptions: exportSetting,
            title: exportTitle,
        }, {
            extend: 'pdf',
            className: 'btn green btn-outline',
            exportOptions: exportSetting,
            title: exportTitle,
        }, {
            extend: 'excel',
            className: 'btn yellow btn-outline ',
            exportOptions: exportSetting,
            title: exportTitle,
        }, {
            extend: 'csv',
            className: 'btn purple btn-outline ',
            exportOptions: exportSetting,
            title: exportTitle,
        }, ],

    });
    $('.table-btn a').click(function(e) {
        e.preventDefault();
        $('#list-user').DataTable().button($(this).data('class')).trigger();
    });
    var searchParentHtml = '<label><select id="searchByRole" name="roles" class="form-control select2"><option value="0">Quyền Hạn</option>'
    @foreach($roles as $role) + '<option value="{{ $role->id }}">{{ $role->name }}</option>'
    @endforeach + '</select>';
    var searchNameHtml = '<label>Tìm kiếm:<input id="searchByName" name="name" type="search" class="form-control input-sm input-small input-inline" placeholder="" aria-controls="list-user"></label>'
    $('#list-user_filter').html(searchNameHtml + searchParentHtml);
    $('#list-user').on('click', '.btn-delete', function(event) {
        event.preventDefault();
        var id = $(this).data('id');
        var name = $(this).data('name');
        alertify.confirm("Bạn có chắc rằng muốn xóa thành viên "+name, function() {
            $.ajax({
                url: '{{ route('admin.employee.del')}}',
                type: 'POST',
                data: {
                    id: id
                },
            })
            .done(function() {
                $('#list-user').DataTable().ajax.reload();
            })
        });
    });
    $.fn.select2.defaults.set("theme", "bootstrap");
    $(".select2").select2({
        width: null
    });
    $('#searchByRole').on('change', function(event) {
        event.preventDefault();
        list.DataTable().draw();
    });
    $('#searchByName').on('keyup', function(event) {
        event.preventDefault();
        list.DataTable().draw();
    });
    $('.group-checkable').change(function(event) {
        var checked =$(this).is(":checked");
        $('input[name="id[]"]').prop('checked',checked);
        if(checked){
            $('.remove-selected').removeClass('disabled');
        }else{
            $('.remove-selected').addClass('disabled');
        }
    });
    $('table').on('change', 'input[name="id[]"]', function(event) {
        if(!$(this).is(":checked")){
            $('.group-checkable').prop('checked', false);
        }
        if($('input[name="id[]"]:checked').length){
            $('.remove-selected').removeClass('disabled');
        }else{
            $('.remove-selected').addClass('disabled');
        }
    });
    $('.remove-selected').click(function(event) {
        event.preventDefault();
          alertify.confirm("Xác Nhận Xóa Các Thành Viên Đang Chọn", function() {
        var ids = [];
        $('input[name="id[]"]:checked').each(function(index, el) {
            ids[index] = $(this).val();
        });
        var url = $(this).data('route');
        var table = $(this).data('table');
        $.ajax({
            url: url,
            data: {id: ids},
        })
        .done(function() {
           $(table).DataTable().ajax.reload();
        });
    })
    });
});
</script>
@endpush @push('css-plugins')
<link href="{{ asset('backend/assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ asset('backend/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.min.css')}}" rel="stylesheet" type="text/css" /> @endpush
<link rel="stylesheet" href="{{ asset('backend/assets/global/plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{ asset('backend/assets/global/plugins/select2/css/select2-bootstrap.min.css')}}"> @push('script-plugins')
<script src="{{ asset('backend/assets/global/scripts/datatable.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('backend/assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('backend/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('backend/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('backend/assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('backend/assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('backend/assets/global/plugins/alertify/alertify.min.js')}}" type="text/javascript"></script>
@endpush @push('css-custom')
<style type="text/css" media="screen">
#list-user_filter .select2-container {
    display: inline-block;
    padding-left: 10px;
    min-width: 150px;

}
.select2-container span{
    text-transform: capitalize;
}

.pagination i {
    line-height: 30px!important
}

.pagination>li>a,
.pagination>li>span {
    line-height: 30px;
    padding-top: 0;
    padding-bottom: 0;
}

#list-user_filter label + label {
    margin-left: 10px;
}
</style>
@endpush
