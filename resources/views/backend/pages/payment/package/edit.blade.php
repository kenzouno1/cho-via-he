@extends('backend.admin') @section('page-head')
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('admin.payment.package.list') }}">Gói Nạp</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Thêm Gói Nạp</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- END PAGE HEADER-->
@endsection @section('page-content')
    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-plus"></i>Sửa Gói Nạp
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form action="{{ route('admin.payment.package.edit',['id'=>$package->id]) }}" method="post" class="form-horizontal">
                {{ csrf_field() }}
                <div class="form-body">
                    <div class="form-group {{ validate_form_class('name',$errors) }}">
                        <label class="col-md-3 control-label">Tên Gói Nạp</label>
                        <div class="col-md-4">
                            <input type="text" name="name" class="form-control" value="{{old('name',$package->name)}}">
                            {{ render_validate_msg('name',$errors) }}
                        </div>
                    </div>
                    <div class="form-group {{ validate_form_class('price',$errors) }}">
                        <label class="col-md-3 control-label">Số tiền (VNĐ)</label>
                        <div class="col-md-4">
                            <input type="text" name="price" class="form-control" value="{{old('price',$package->price)}}">
                            {{ render_validate_msg('price',$errors) }}
                        </div>
                    </div>
                    <div class="form-group {{ validate_form_class('money',$errors) }}">
                        <label class="col-md-3 control-label">Số tiền nhận được</label>
                        <div class="col-md-4">
                            <input type="text" name="money" class="form-control"
                                   value="{{old('money',$package->money)}}">
                            {{ render_validate_msg('money',$errors) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Trạng thái</label>
                        <div class="col-md-7">
                            <div class="mt-radio-inline ">
                                <label class="mt-checkbox">
                                    <input name="status" value="1"
                                           type="checkbox"
                                            {{ checked(old('status',$package->status),1) }}> Kích hoạt
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-md-push-3">
                        {{ render_success_msg($errors) }}
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <button type="submit" class="btn btn-circle green">Lưu</button>
                        </div>
                    </div>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
@endsection
@push('css-plugins')
    <link rel="stylesheet" href="{{ asset('backend/assets/global/css/components.min.css')}}">
@endpush