@extends('backend.admin') @section('page-head')
<!-- BEGIN PAGE HEADER-->
<!-- BEGIN PAGE BAR -->
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}">Dashboard</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('admin.employee.list') }}">Nhân Viên</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Thêm Nhân Viên</span>
        </li>
    </ul>
</div>
<!-- END PAGE BAR -->
<!-- END PAGE HEADER-->
@endsection @section('page-content')

<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus"></i>Thêm Nhân Viên
</div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="{{ route('admin.employee.new') }}" method="post" class="form-horizontal" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-body row">
                <div class="col-md-6">
                    <div class="form-group {{ validate_form_class('username',$errors) }}">
                        <label class="col-md-3 control-label">Tên Tài Khoản</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-user" aria-hidden="true"></i>
                                </span>
                                <input class="form-control" name="username" type="text" value={{old('username')}}>
                                 </div>
                              {{ render_validate_msg('username',$errors) }}
                        </div>
                    </div>
                     <div class="form-group {{ validate_form_class('pwd',$errors) }}"">
                        <label class="col-md-3 control-label">Mật Khẩu</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                </span>
                                <input class="form-control" name="pwd" type="password">

                            </div>
                              {{ render_validate_msg('pwd',$errors) }}
                        </div>
                    </div>
                   <div class="form-group {{ validate_form_class('email',$errors) }}"">
                        <label class="col-md-3 control-label">Địa Chỉ Email</label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-envelope"></i>
                                </span>
                                <input class="form-control" name="email" type="email" value="{{old('email')}}">

                            </div>
                              {{ render_validate_msg('email',$errors) }}
                        </div>
                    </div>
                      <div class="form-group {{ validate_form_class('name',$errors) }}"">
                        <label class="col-md-3 control-label">Tên Nhân Viên</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-user" aria-hidden="true"></i>
                                </span>
                                <input class="form-control" name="name" type="text" value="{{old('name')}}">

                            </div>
                                  {{ render_validate_msg('name',$errors) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Ngày Sinh</label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                </span>
                                <input class="form-control" name="birth" type="date" value="{{old('birth')}}">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Số Điện Thoại</label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                </span>
                                <input class="form-control" name="phone" type="tel" value="{{old('phone')}}">
                            </div>
                        </div>
                    </div>

                    <div class=" form-group">
                        <label class="col-md-3 control-label">Địa Chỉ</label>
                        <div class="col-md-9 ">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                </span>
                                <input class="form-control" name="add" type="text" value="{{old('add')}}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group {{ validate_form_class('role',$errors) }}">
                        <label class="col-md-3 control-label">Nhóm Quyền</label>
                        <div class="col-md-9">
                            <select class="bs-select form-control" name="role[]" multiple>
                                @foreach ($roles as $role)
                                     <option value="{{$role->id}}" {{selected( old('role',''),$role->id)}}>{{$role->name}}</option>
                                @endforeach
                            </select>
                            {{ render_validate_msg('role',$errors) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Trạng thái</label>
                        <div class="col-md-9">
                            <select class="bs-select form-control" name="status">
                              @foreach (config('conf.employee.status') as $key => $status)
                                  @if($key != 'default')
                                        <option value="{{$status}}">{{$status}}</option>
                                      @endif
                              @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ validate_form_class('avatar',$errors) }}"">
                        <label class="col-md-3 control-label">Ảnh Đại Diện</label>
                        <div class="col-md-9">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                <div>
                                    <span class="btn default btn-file">
                                        <span class="fileinput-new"> Chọn ảnh </span>
                                        <span class="fileinput-exists"> Đổi ảnh khác </span>
                                        <input type="file" name="avatar"  accept="image/*"> </span>
                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Xóa </a>
                                </div>
                            </div>
                                {{ render_validate_msg('avatar',$errors) }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-push-3">
                    {{ render_success_msg($errors) }}
                </div>
            </div>
            <div class="form-actions right">
                <button type="submit" class="btn btn-circle green">Lưu</button>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>
@endsection
@push('script-plugins')
  <script src="{{ asset('backend/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
   <script src="{{ asset('backend/assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
@endpush
@push('script-custom')
    <script type="text/javascript">
        $.fn.select2.defaults.set("theme", "bootstrap");
      $("select").select2({
            width: null,
            'placeholder':'Chọn nhóm quyền'
        });
    </script>
@endpush
@push('css-plugins')
    <link rel="stylesheet" href="{{ asset('backend/assets/global/css/components.min.css')}}">
    <link rel="stylesheet" href="{{ asset('backend/assets/global/plugins/jquery-file-upload/css/jquery.fileupload.css')}}">
    <link rel="stylesheet" href="{{ asset('backend/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}">
     <link rel="stylesheet" href="{{ asset('backend/assets/global/plugins/select2/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{ asset('backend/assets/global/plugins/select2/css/select2-bootstrap.min.css')}}">
@endpush