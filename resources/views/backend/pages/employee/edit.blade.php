
@extends('backend.admin') @section('page-head')
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ route('admin.dashboard') }}">Bảng tin</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('admin.employee.list') }}">Nhân Viên</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Thêm Nhân Viên</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- END PAGE HEADER-->
@endsection @section('page-content')
@php
function active_tab($key,$default=false){
    if(session('tab')==$key)
        echo 'active';
    if($default && !session()->filled('tab')){
         echo 'active';
    }
}

@endphp
    <h1 class="page-title"> Sửa nhân viên
    </h1>
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PROFILE SIDEBAR -->
            <div class="profile-sidebar">
                <!-- PORTLET MAIN -->
                <div class="portlet light profile-sidebar-portlet ">
                    <!-- SIDEBAR USERPIC -->
                    <div class="profile-userpic">
                        <img src="{{ get_avatar(true,$id) }}" class="img-responsive" alt=""></div>
                    <!-- END SIDEBAR USERPIC -->
                    <!-- SIDEBAR USER TITLE -->
                    <div class="profile-usertitle">
                        <div class="profile-usertitle-name"> {{ $employee->name }} </div>
                    </div>
                    <!-- END SIDEBAR USER TITLE -->
                </div>
                <!-- END PORTLET MAIN -->
            </div>
            <!-- END BEGIN PROFILE SIDEBAR -->
            <!-- BEGIN PROFILE CONTENT -->
            <div class="profile-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light ">
                            <div class="portlet-title tabbable-line">
                                <div class="caption caption-md">
                                    <i class="icon-globe theme-font hide"></i>
                                </div>
                                <ul class="nav nav-tabs">
                                    <li class="{{active_tab('profile',true)}}">
                                        <a href="#tab_1_1" data-toggle="tab">Thông tin cá nhân</a>
                                    </li>
                                    <li class="{{active_tab('avatar')}}">
                                        <a href="#tab_1_2" data-toggle="tab">Ảnh đại diện</a>
                                    </li>
                                    <li class="{{active_tab('pwd')}}">
                                        <a href="#tab_1_3" data-toggle="tab">Tài khoản</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="portlet-body">
                                <div class="tab-content">
                                    <!-- PERSONAL INFO TAB -->
                                    <div class="tab-pane {{ active_tab('profile',true) }}" id="tab_1_1">
                                        <form role="form" method="post" action="{{route('admin.employee.edit.profile',['id'=>$id])}}">
                                            {{ csrf_field() }}
                                            <div class="form-group {{ validate_form_class('name',$errors) }}">
                                                <label class="control-label">Họ &amp; tên</label>
                                                <input type="text" name="name" value="{{old('name',$employee->name)}}" class="form-control">
                                                {{ render_validate_msg('name',$errors) }}
                                            </div>
                                            <div class="form-group {{ validate_form_class('email',$errors) }}">
                                                <label class="control-label">Email</label>
                                                <input type="email" name="email" value="{{old('email',$employee->email)}}" class="form-control">
                                                {{ render_validate_msg('email',$errors) }}
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label">Số Điện Thoại</label>
                                                <input type="tel" name="phone" value="{{old('phone',$employee->phone)}}" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Địa Chỉ</label>
                                                <input type="text"  name="add" value="{{old('add',$employee->address)}}" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Ngày Sinh</label>
                                                <input type="date"  name="birth" value="{{old('birth',$employee->birthday)}}" class="form-control">
                                            </div>
                                            {{ render_success_msg($errors) }}
                                            <div class="margiv-top-10">
                                                <button type="submit" class="btn green">Lưu</button>
                                            </div>

                                        </form>
                                    </div>
                                    <!-- END PERSONAL INFO TAB -->
                                    <!-- CHANGE AVATAR TAB -->
                                    <div class="tab-pane {{ active_tab('avatar') }}" id="tab_1_2">
                                        <form  method="post" role="form" action="{{route('admin.employee.edit.avatar',['id'=>$id])}}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="form-group">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail">
                                                        <img src="{{ get_avatar(true,$id) }}" alt="Avatar">
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail"
                                                    ></div>
                                                    <div>
                                                    <span class="btn default btn-file">
                                                     <span class="fileinput-new"> Chọn ảnh</span>
                                                    <span class="fileinput-exists"> Đổi ảnh khác </span>
                                                        {{ render_validate_msg('avatar',$errors) }}
                                                    <input type="file" name="avatar"> </span>
                                                        <a href="javascript:;" class="btn default fileinput-exists"
                                                           data-dismiss="fileinput"> Xóa </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="margin-top-10">
                                                 <button type="submit" class="btn green">Lưu</button>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- END CHANGE AVATAR TAB -->
                                    <!-- CHANGE PASSWORD TAB -->
                                    <div class="tab-pane {{ active_tab('pwd') }}" id="tab_1_3">
                                        <form role="form" method="post" action="{{route('admin.employee.edit.role',['id'=>$id])}}">
                                            {{ csrf_field() }}
                                            <h4>Phân quyền</h4>
                                                <div class="form-group">
                                                    <label class="control-label">Chọn nhóm quyền</label>
                                                    <select class="bs-select form-control" name="role[]" multiple>
                                                        @foreach ($roles as $role)
                                                            <option value="{{$role->id}}" {{selected( old('role',$employee->getRolesId()),$role->id)}}>{{$role->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    {{ render_validate_msg('role',$errors) }}
                                                </div>
                                            <div class="margin-top-10">
                                                <button type="submit" class="btn green">Lưu</button>
                                            </div>
                                        </form>
                                        <hr>
                                        <form role="form" method="post" action="{{route('admin.employee.edit.password',['id'=>$id])}}">
                                            {{ csrf_field() }}
                                            <h4>Đổi mật khẩu</h4>
                                            <div class="form-group {{ validate_form_class('pwd',$errors) }}">
                                                <label class="control-label">Mật khẩu mới</label>
                                                <input type="password" name="pwd" class="form-control">
                                                  {{ render_validate_msg('pwd',$errors) }}
                                            </div>
                                            <div class="form-group {{ validate_form_class('re-pwd',$errors) }}">
                                                <label class="control-label">Nhập lại mật khẩu</label>
                                                <input type="password" name="re-pwd" class="form-control">
                                                  {{ render_validate_msg('re-pwd',$errors) }}
                                            </div>
                                            <div class="margin-top-10">
                                                 <button type="submit" class="btn green">Lưu</button>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- END CHANGE PASSWORD TAB -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PROFILE CONTENT -->
        </div>
    </div>
@endsection @push('script-plugins')
<script src="{{ asset( 'backend/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}} "
        type="text/javascript "></script>
<script src="{{ asset( 'backend/assets/global/plugins/select2/js/select2.full.min.js')}} "
        type="text/javascript "></script>
@endpush @push('script-custom')
<script type="text/javascript ">
    $.fn.select2.defaults.set("theme ", "bootstrap ");
    $("select").select2({
        width: '100%',
    });

</script>
@endpush @push('css-plugins') @push('css-custom')
<style>
    .profile-sidebar {
        float: left;
        width: 300px;
        margin-right: 20px
    }

    .profile-content {
        overflow: hidden
    }

    .profile-sidebar-portlet {
        padding: 30px 0  !important
    }

    .profile-userpic img {
        float: none;
        margin: 0 auto;
        width: 50%;
        height: 50%;
        -webkit-border-radius: 50% !important;
        -moz-border-radius: 50% !important;
        border-radius: 50% !important
    }

    .profile-usertitle {
        text-align: center;
        margin-top: 20px
    }

    .profile-usertitle-name {
        color: #5a7391;
        font-size: 20px;
        font-weight: 600;
        margin-bottom: 7px
    }



    .profile-userbuttons button {
        text-transform: uppercase;
        font-size: 11px;
        font-weight: 600;
        padding: 6px 15px;
    }

    .profile-usermenu ul li {
        border-bottom: 1px solid #f0f4f7
    }

    .profile-usermenu ul li:last-child {
        border-bottom: none
    }

    .profile-usermenu ul li a {
        color: #93a3b5;
        font-size: 16px;
        font-weight: 400
    }

    .profile-usermenu ul li a i {
        margin-right: 8px;
        font-size: 16px
    }

    .profile-usermenu ul li a:hover {
        background-color: #fafcfd;
        color: #5b9bd1
    }

    .profile-usermenu ul li.active a {
        color: #5b9bd1;
        background-color: #f6f9fb;
        border-left: 2px solid #5b9bd1;
        margin-left: -2px
    }

    .profile-desc-link i {
        width: 22px;
        font-size: 19px;
        color: #abb6c4;
        margin-right: 5px;
    }

    .profile-desc-link a {
        font-size: 14px;
        font-weight: 600;
        color: #5b9bd1;
    }
    .thumbnail img{
        min-width:300px;
    }
    @media (max-width: 991px) {
        .profile-sidebar {
            float: none;
            width: 100% !important;
            margin: 0
        }

        .profile-sidebar > .portlet {
            margin-bottom: 20px
        }

        .profile-content {
            overflow: visible
        }
    }
    .page-content {
        background: #eef1f5;
    }
</style>
@endpush
<link rel="stylesheet " href="{{ asset( 'backend/assets/global/css/components.min.css')}} ">
<link rel="stylesheet "
      href="{{ asset( 'backend/assets/global/plugins/jquery-file-upload/css/jquery.fileupload.css')}} ">
<link rel="stylesheet "
      href="{{ asset( 'backend/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}} ">
<link rel="stylesheet " href="{{ asset( 'backend/assets/global/plugins/select2/css/select2.min.css')}} ">
<link rel="stylesheet "
      href="{{ asset( 'backend/assets/global/plugins/select2/css/select2-bootstrap.min.css')}} "> @endpush
