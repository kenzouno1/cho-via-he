<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject bold uppercase font-dark">Thống kê doanh thu</span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                {{--<h4 class="widget-thumb-heading">Current Balance</h4>--}}
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-green icon-layers"></i>
                    <div class="widget-thumb-body">
                        <span class="widget-thumb-subtitle">Số tiền thu được</span>
                        <span class="widget-thumb-body-stat"><span id="total-money">{{$countMoney}}</span> VNĐ</span>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
    </div>
</div>