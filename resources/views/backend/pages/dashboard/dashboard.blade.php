@extends('backend.admin')
@section('page-head')
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <span>Dashboard</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- END PAGE HEADER-->
@endsection
@section('page-content')
    <div class="custom-dasboard row">
        <div class="col-md-12 col-xs-12 col-sm-12 number-member">
            <a class="dashboard-stat dashboard-stat-v2 blue " href="#">
                <div class="visual">
                    <i class="fa fa-comments"></i>
                </div>
                <div class="details">
                    <div class="desc">Hiện Có</div>
                    <div class="number">
                        <span id="total-user">{{$countUser}}</span>
                    </div>
                    <div class="desc">THÀNH VIÊN</div>
                </div>
            </a>
        </div>
        <div class="col-lg-6 col-xs-12 col-sm-12">
            @include('backend.pages.dashboard.statistics-ads')
        </div>
        <div class="col-lg-6 col-xs-12 col-sm-12">
            @include('backend.pages.dashboard.statistics-money')
        </div>
    </div>

@endsection
@push('script-plugins')
    <script>
        function loadtotalAds() {
            $.ajax({
                url: '{{ route('admin.dashboard.count')}}',
            }).done(function (data) {
                $('#total-post').text(data.count_ads);
                $('#total-car').text(data.count_car);
                $('#total-bds').text(data.count_bds);
                $('#total-job').text(data.count_job);
                $('#total-user').text(data.count_user);
                $('#total-money').text(data.count_money);
            });
        }
        loadtotalAds(); // This will run on page load
        setInterval(function () {
            loadtotalAds() // this will run after every 5 seconds
        }, 1000 * 60 * 5);
    </script>
@endpush
