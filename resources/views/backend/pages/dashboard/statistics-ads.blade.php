<div class="portlet light bordered ">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject bold uppercase font-dark">Thống kê bài đăng</span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                {{--<h4 class="widget-thumb-heading">Current Balance</h4>--}}
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-green icon-layers"></i>
                    <div class="widget-thumb-body">
                        <span class="widget-thumb-subtitle">Tổng Số Bài Đăng</span>
                        <span class="widget-thumb-body-stat" id="total-post">{{$countAds}}</span>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-6">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                {{--<h4 class="widget-thumb-heading">Weekly Sales</h4>--}}
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-red icon-layers"></i>
                    <div class="widget-thumb-body">
                        <span class="widget-thumb-subtitle">Ô Tô</span>
                        <span class="widget-thumb-body-stat" id="total-car">{{$countCar}}</span>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-6">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                {{--<h4 class="widget-thumb-heading">Biggest Purchase</h4>--}}
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-purple icon-screen-desktop"></i>
                    <div class="widget-thumb-body">
                        <span class="widget-thumb-subtitle">Bất Động Sản</span>
                        <span class="widget-thumb-body-stat" id="total-bds">{{$countBds}}</span>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-6">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                {{--<h4 class="widget-thumb-heading">Average Monthly</h4>--}}
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-blue icon-bar-chart"></i>
                    <div class="widget-thumb-body">
                        <span class="widget-thumb-subtitle">Việc Làm</span>
                        <span class="widget-thumb-body-stat" id="total-job">{{$countJob}}</span>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
    </div>
</div>
