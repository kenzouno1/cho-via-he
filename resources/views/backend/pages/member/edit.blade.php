
@extends('backend.admin') @section('page-head')
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('admin.roles.list') }}">Thành Viên
                </a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Thêm Thành Viên</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- END PAGE HEADER-->
@endsection @section('page-content')
    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-plus"></i>Thêm Thành Viên
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form action="{{route('admin.member.edit.profile',['id'=>$member->id])}}" method="post" class="form-horizontal" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-body row">
                    <div class="col-md-6">
                        <div class="form-group {{ validate_form_class('name',$errors) }}">
                            <label class="col-md-3 control-label">Tên Thành Viên</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-user" aria-hidden="true"></i>
                                </span>
                                    <input class="form-control" value="{{old('name',$member->name)}}" name="name" placeholder="Tên Thành Viên" type="text">
                                </div>
                                {{ render_validate_msg('name',$errors) }}
                            </div>
                        </div>
                        <div class="form-group {{ validate_form_class('email',$errors) }}">
                            <label class="col-md-3 control-label">Địa Chỉ Email</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-envelope"></i>
                                </span>
                                    <input class="form-control" value="{{old('email',$member->email)}}"  name="email" placeholder="Địa Chỉ Email" type="email">
                                </div>
                                {{ render_validate_msg('email',$errors) }}
                            </div>
                        </div>
                        <div class="form-group {{ validate_form_class('phone',$errors) }}">
                            <label class="col-md-3 control-label">Số Điện Thoại</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                </span>
                                    <input disabled class="form-control" name="phone" value="{{old('phone',$member->phone)}}" placeholder="Số Điện Thoại" type="tel">
                                </div>
                                {{ render_validate_msg('phone',$errors) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Ngày Sinh</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                </span>
                                    <input class="form-control" value="{{old('birthday',$member->birthday)}}" name="birthday" placeholder="Ngày Sinh" type="date">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Địa Chỉ</label>
                            <div class="col-md-9 ">
                                <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                </span>
                                    <input class="form-control" value="{{old('address',$member->address)}}" name="address" placeholder="Địa Chỉ" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Trạng thái</label>
                            <div class="col-md-9">
                                <select class="bs-select form-control" value="{{old('status',$member->status)}}" name="status" value="">
                                    <option value="{{old('status',$member->status)}}">{{config('conf.user.status')[old('status',$member->status)]}}</option>
                                    @foreach (config('conf.user.status') as $key => $status)
                                        @if($key != 'default' && $key != old('status',$member->status))
                                            <option value="{{$key}}">{{$status}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Ảnh Đại Diện</label>
                            <div class="col-md-9">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                        <img src="{{ get_avatar(false,$member->id) }}" alt="" /> </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                    <div>
                                    <span class="btn default btn-file">
                                        <span class="fileinput-new"> Chọn ảnh </span>
                                        <span class="fileinput-exists"> Đổi ảnh khác </span>
                                        <input type="file"  name="avatar"  accept="image/*"> </span>
                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Xóa </a>
                                    </div>
                                </div>
                                {{ render_validate_msg('avatar',$errors) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-md-push-3">
                        {{ render_success_msg($errors) }}
                    </div>
                </div>
                <div class="form-actions right">
                    <button type="submit" class="btn btn-circle green">Lưu</button>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
@endsection
@push('script-plugins')
<script src="{{ asset('backend/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
<script src="{{ asset('backend/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('backend/assets/pages/scripts/components-bootstrap-select.min.js')}}" type="text/javascript"></script>
@endpush
@push('script-custom')
<script type="text/javascript">
    $('.bs-select').selectpicker();
</script>
@endpush
@push('css-plugins')
<link rel="stylesheet" href="{{ asset('backend/assets/global/css/components.min.css')}}">
<link rel="stylesheet" href="{{ asset('backend/assets/global/plugins/jquery-file-upload/css/jquery.fileupload.css')}}">
<link rel="stylesheet" href="{{ asset('backend/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}">
<link rel="stylesheet" href="{{ asset('backend/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css')}}">
@endpush