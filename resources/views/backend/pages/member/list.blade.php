
@extends('backend.admin') @section('page-head')
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('admin.category.list') }}">Thành Viên
                </a>
                <i class="fa fa-circle"></i>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- END PAGE HEADER-->
    <!--   Popup block member -->
    <div class="modal fade" id="modal-blockmember" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Khóa tài khoản</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" id="form-block-member">
                        <input type="hidden" name="id-block">
                        <div class="form-group">
                            <label for="message-text" class="form-control-label">Lí do Khóa:</label>
                            <textarea name="comment" class="form-control" id="message-text"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="form-control-label">Thời gian Khóa:</label>
                            <select class="bs-select form-control" name="day">
                                <option value="1">1 ngày</option>
                                <option value="2">2 ngày</option>
                                <option value="3">3 ngày</option>
                                <option value="7">1 tuần</option>
                                <option value="30">1 tháng</option>
                                <option value="9999">vĩnh viễn</option>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                    <button type="button" class="btn btn-primary btn-save-block">Lưu lại</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Popup block member  -->
@endsection @section('page-content')
    <div class="portlet light portlet-fit portlet-datatable bordered">
        <div class="portlet-title">
            <div class="row">
                <div class="caption col-md-4">
                    <i class="fa-list fa font-dark"></i>
                    <span class="caption-subject font-dark sbold uppercase">Thành Viên</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <a href="{{ route('admin.member.new') }}" class="btn sbold green"> Thêm Thành Viên
                                        <i class="fa fa-plus"></i>
                                    </a>
                                    <a data-table="#list-user"
                                       data-route="{{ route('admin.member.remove_selected') }}" href="#"
                                       class="btn sbold red disabled remove-selected"> Xóa Thành Viên Được Chọn
                                        <i class="fa fa-remove"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="actions">
                                    <div class="dt-buttons table-btn pull-right">
                                        <a data-class=".buttons-print"
                                           class="dt-button buttons-print btn dark btn-outline" tabindex="0"
                                           aria-controls="list-cate" href="#"><span>Print</span></a>
                                        <a data-class=".buttons-pdf"
                                           class="dt-button buttons-pdf buttons-html5 btn green btn-outline"
                                           tabindex="0" aria-controls="list-cate" href="#"><span>PDF</span></a>
                                        <a data-class=".buttons-excel"
                                           class="dt-button buttons-excel buttons-html5 btn yellow btn-outline"
                                           tabindex="0" aria-controls="list-cate" href="#"><span>Excel</span></a>
                                        <a data-class=".buttons-csv"
                                           class="dt-button buttons-csv buttons-html5 btn purple btn-outline"
                                           tabindex="0" aria-controls="list-cate" href="#"><span>CSV</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-checkable" id="list-user">
                        <thead>
                        <tr role="row" class="heading">
                            <th width="2%" class="sorting_disabled" rowspan="1" colspan="1">
                                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                    <input type="checkbox" class="group-checkable" data-set="#list-cate">
                                    <span></span>
                                </label>
                            </th>
                            <th width="3%"> ID</th>
                            <th width="25%"> Tên Thành Viên
                            <th width="25%"> Email</th>
                            <th width="15%"> Số Điện Thoại</th>
                            <th width="15%"> Trạng Thái</th>
                            <th width="17%"> Tác Vụ</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script-custom')
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        var exportSetting = {
            columns: [1, 2, 3, 4]
        };
        var exportTitle = 'Member Cho Via He'
        var list = $('#list-user').dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                type: 'POST',
                url: "{{ route('admin.member.list') }}",
                data: function (d) {
//                    d.name = $('#searchByName').val();
//                    d.roles = $('#searchByRole').val() !== 0 ? $('#searchByRole').val() : 2;
                },
            },
            "columns": [{
                data: 'checkbox',
                name: 'id',
                orderable: false,
            }, {
                data: 'id',
                name: 'id',
                searchable: false,
                visible: false,
            }, {
                data: 'name',
                name: 'name'
            }, {
                data: 'email',
                name: 'email',
            }, {
                data: 'phone',
                name: 'phone',
                "orderable": "false",
                searchable: false,
            }, {
                data: 'status',
                name: 'status',
                "orderable": true,
                searchable: false,
            }, {
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false,
            }],
            "language": {
                "emptyTable": "Không tìm thấy dữ liệu",
                "info": "Đang hiển thị kết quả từ _START_ tới _END_ trên tổng số _TOTAL_ kết quả",
                "infoEmpty": "Không có kết quả",
                "infoFiltered": "(trên tổng số  _MAX_ kết quả được tìm thấy)",
                "lengthMenu": "Số dòng hiển thị _MENU_",
                "search": "Tìm kiếm:",
                'processing': 'Đang xử lý....',
                "zeroRecords": "Không có kết quả phù hợp",
                "paginate": {
                    "previous": "Trở lại",
                    "next": "Tiếp",
                    "last": "Cuối",
                    "first": "Đầu"
                }
            },
            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],
            "order": [
                [1, "asc"]
            ],
            // set the initial value
            "pageLength": 20,
            "pagingType": "bootstrap_full_number",
            buttons: [{
                extend: 'print',
                className: 'dt-button buttons-print',
                exportOptions: exportSetting,
                title: exportTitle,
            }, {
                extend: 'pdf',
                className: 'btn green btn-outline',
                exportOptions: exportSetting,
                title: exportTitle,
            }, {
                extend: 'excel',
                className: 'btn yellow btn-outline ',
                exportOptions: exportSetting,
                title: exportTitle,
            }, {
                extend: 'csv',
                className: 'btn purple btn-outline ',
                exportOptions: exportSetting,
                title: exportTitle,
            },],

        });
        $('.table-btn a').click(function (e) {
            e.preventDefault();
            $('#list-user').DataTable().button($(this).data('class')).trigger();
        });

        {{--$('#list-user').on('click', '.btn-delete', function (event) {--}}
            {{--event.preventDefault();--}}
            {{--var id = $(this).data('id');--}}
            {{--var name = $(this).data('name');--}}
            {{--alertify.confirm("Bạn có chắc rằng muốn xóa thành viên " + name, function () {--}}
                {{--$.ajax({--}}
                    {{--url: '{{ route('admin.employee.del')}}',--}}
                    {{--type: 'POST',--}}
                    {{--data: {--}}
                        {{--id: id--}}
                    {{--},--}}
                {{--})--}}
                    {{--.done(function () {--}}
                        {{--$('#list-user').DataTable().ajax.reload();--}}
                    {{--})--}}
            {{--});--}}
        {{--});--}}
        /*
         Block Member Ajax
         */
        var event_block;
        $('#list-user').on('click','.block-member',function (event) {
            event_block = $(this);
            $('#modal-blockmember #form-block-member input[name="id-block"]').val($(this).data('id'));
        });
        $('#modal-blockmember').on('click','.btn-save-block',function (event) {
            var name = event_block.data('name');
            var that = $(this);
            alertify.confirm("Khóa Thành Viên " + name,
                function(){
                    $.ajax({
                        url: '{{ route('admin.member.block')}}',
                        type: 'POST',
                        data: $('#form-block-member').serialize(),
                    }).done(function (data) {
                        $('#modal-blockmember button[data-dismiss="modal"]').trigger( "click" );
                        $('#list-user').DataTable().ajax.reload();
                        alertify.success('Đã khóa thành viên' + name);
                    })
                },
                function(){
                    alertify.error('Không thành công');
                });
        });
        /*
        Unbloc Member Ajax
         */
        $('#list-user').on('click','.unblock-member',function (event) {
            var id = $(this).data('id');
            var name = $(this).data('name');
            alertify.confirm("Mở khóa thành viên " + name,
                function(){
                    $.ajax({
                        url: '{{ route('admin.member.unblock')}}',
                        type: 'POST',
                        data: {id : id },
                    }).done(function (data) {
                        $('#list-user').DataTable().ajax.reload();
                        alertify.success('Mở khóa thành công ' + name);
                    })
                },
                function(){
                    alertify.error('Không thực hiện mở khóa');
                });
        });
        /*
        Remove Member Ajax
         */
        $('#list-user').on('click','.remove-member',function (event) {
            var id = $(this).data('id');
            var name = $(this).data('name');
            alertify.confirm("Xóa Thành Viên " + name,
                function(){
                    $.ajax({
                        url: '{{ route('admin.member.del')}}',
                        type: 'POST',
                        data: {id : id },
                    }).done(function (data) {
                        $('#list-user').DataTable().ajax.reload();
                        alertify.success('Xóa thành công thành viên ' + name);
                    })
                },
                function(){
                    alertify.error('không xóa thành công');
                });
        });
        /*
         Active Member Ajax
         */
        $('#list-user').on('click','.active-member',function (event) {
            var id = $(this).data('id');
            var name = $(this).data('name');
            alertify.confirm("Kích hoạt Thành Viên " + name,
                function(){
                    $.ajax({
                        url: '{{ route('admin.member.active')}}',
                        type: 'POST',
                        data: {id : id },
                    }).done(function (data) {
                        $('#list-user').DataTable().ajax.reload();
                        alertify.success('Kích hoạt thành công thành viên ' + name);
                    })
                },
                function(){
                    alertify.error('không kích hoạt thành công');
                });
        });
        $.fn.select2.defaults.set("theme", "bootstrap");
        $(".select2").select2({
            width: null
        });

        $('.group-checkable').change(function (event) {
            var checked = $(this).is(":checked");
            $('input[name="id[]"]').prop('checked', checked);
            if (checked) {
                $('.remove-selected').removeClass('disabled');
            } else {
                $('.remove-selected').addClass('disabled');
            }
        });
        $('table').on('change', 'input[name="id[]"]', function (event) {
            if (!$(this).is(":checked")) {
                $('.group-checkable').prop('checked', false);
            }
            if ($('input[name="id[]"]:checked').length) {
                $('.remove-selected').removeClass('disabled');
            } else {
                $('.remove-selected').addClass('disabled');
            }
        });
        $('.remove-selected').click(function (event) {
            event.preventDefault();
            alertify.confirm("Xác Nhận Xóa Các Thành Viên Đang Chọn", function () {
                var ids = [];
                $('input[name="id[]"]:checked').each(function (index, el) {
                    ids[index] = $(this).val();
                });
//                var url = $(this).data('route');
//                var table = $(this).data('table');
                $.ajax({
                    url: '{{ route('admin.member.remove_selected')}}',
                    data: {id: ids},
                })
                    .done(function () {
                        $('#list-user').DataTable().ajax.reload();
                    });
            })
        });
    });
</script>
<style>
    #modal-blockmember {top:50%; transform:translateY(-50%);}
</style>
@stack('css-custom')
@endpush @push('css-plugins')
<link href="{{ asset('backend/assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet"
      type="text/css"/>
<link href="{{ asset('backend/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.min.css')}}"
      rel="stylesheet" type="text/css"/>
@endpush
<link rel="stylesheet" href="{{ asset('backend/assets/global/plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet"
      href="{{ asset('backend/assets/global/plugins/select2/css/select2-bootstrap.min.css')}}">
@push('script-plugins')
<script src="{{ asset('backend/assets/global/scripts/datatable.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('backend/assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('backend/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"
        type="text/javascript"></script>
<script src="{{ asset('backend/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.min.js')}}"
        type="text/javascript"></script>
<script src="{{ asset('backend/assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js')}}"
        type="text/javascript"></script>
<script src="{{ asset('backend/assets/global/plugins/select2/js/select2.full.min.js')}}"
        type="text/javascript"></script>
<script src="{{ asset('backend/assets/global/plugins/alertify/alertify.min.js')}}" type="text/javascript"></script>
@endpush