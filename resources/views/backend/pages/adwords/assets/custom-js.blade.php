<script type="text/javascript">
    jQuery(document).ready(function ($) {
        var searchCateHtml = '<label><select id="searchCateHtml" name="cate" class="form-control select2"><option value="">Chọn Chuyên mục</option>'
                @foreach($categories as $category) + '<option value="{{ $category->id }}">{{ $category->name }}</option>'
                @endforeach + '</select></label>';
        var searchBoostHtml = '<label><select id="searchBoostHtml" name="boost" class="form-control select2"><option value="">Chọn Gói Cước</option>'
                @foreach($packages as $package) + '<option value="{{ $package->id }}">{{ $package->name }}</option>'
                @endforeach + '</select></label>';
//        var searchNameHtml = '<label>Tìm kiếm:<input id="searchByName" type="search" class="form-control input-sm input-small input-inline" placeholder="" aria-controls="list-cate"></label>'
        $('#list-post_filter').append(searchCateHtml + searchBoostHtml);
        $('#searchCateHtml, #searchBoostHtml').on('change', function (event) {
            event.preventDefault();
            list.DataTable().draw();
        });
        $('#list-post_wrapper').on('click', '.update-status', function (event) {
            var id = $(this).data('id');
            var status = $(this).data('status');
            alertify.confirm("Duyệt bài viết",
                function () {
                    $.ajax({
                        url: '{{ route('admin.ads.updateStatus')}}',
                        type: 'POST',
                        data: {id: id, status: status},
                    }).done(function (data) {
                        $('#list-post').DataTable().ajax.reload();
                        alertify.success('Đã Duyệt');
                    })
                },
                function () {
                    alertify.error('Hủy');
                });
        });
        $('#list-post_wrapper').on('click', '.remove-post', function (event) {
            var id = $(this).data('id');
            alertify.confirm("Xóa bài đăng",
                function () {
                    $.ajax({
                        url: '{{ route('admin.report.deleteAds')}}',
                        type: 'POST',
                        data: {ads_id: id},
                    }).done(function (data) {
                        $('#list-post').DataTable().ajax.reload();
                        alertify.success('Đã Xóa');
                    })
                },
                function () {
                    alertify.error('Hủy');
                });
        });
    });
</script>

