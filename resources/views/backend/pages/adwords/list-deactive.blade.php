@extends('backend.admin') @section('page-head')
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ route('admin.dashboard') }}">Bảng Tin</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Danh sách bài viết đã duyệt</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- END PAGE HEADER-->
@endsection @section('page-content')
    <div class="portlet light portlet-fit portlet-datatable bordered">
        <div class="portlet-title">
            <div class="row">
                <div class="caption col-md-4">
                    <i class="fa-list fa font-dark"></i>
                    <span class="caption-subject font-dark sbold uppercase">Danh sách bài viết</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                {{--<div class="btn-group">--}}
                                {{--<a href="{{ route('admin.category.new')}}" class="btn sbold green"> Hủy Duyệt Bài--}}
                                {{--viết được chọn--}}
                                {{--<i class="fa fa-plus"></i>--}}
                                {{--</a>--}}
                                {{--<a data-table="#list-post"--}}
                                {{--data-route="{{ route('admin.category.remove_selected') }}" href="#"--}}
                                {{--class="btn sbold red disabled remove-selected"> Xóa Bài viết Được Chọn--}}
                                {{--<i class="fa fa-remove"></i>--}}
                                {{--</a>--}}
                                {{--</div>--}}
                            </div>
                            <div class="col-md-6">
                                <div class="actions">
                                    <div class="dt-buttons table-btn pull-right">
                                        <a data-class=".buttons-print"
                                           class="dt-button buttons-print btn dark btn-outline" tabindex="0"
                                           aria-controls="list-post" href="#"><span>Print</span></a>
                                        <a data-class=".buttons-pdf"
                                           class="dt-button buttons-pdf buttons-html5 btn green btn-outline"
                                           tabindex="0" aria-controls="list-post" href="#"><span>PDF</span></a>
                                        <a data-class=".buttons-excel"
                                           class="dt-button buttons-excel buttons-html5 btn yellow btn-outline"
                                           tabindex="0" aria-controls="list-post" href="#"><span>Excel</span></a>
                                        <a data-class=".buttons-csv"
                                           class="dt-button buttons-csv buttons-html5 btn purple btn-outline"
                                           tabindex="0" aria-controls="list-post" href="#"><span>CSV</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-checkable" id="list-post">
                        <thead>
                        <tr role="row" class="heading">
                            <th width="2%" class="sorting_disabled" rowspan="1" colspan="1">
                                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                    <input type="checkbox" class="group-checkable" data-set="#list-post">
                                    <span></span>
                                </label>
                            </th>
                            <th width="3%"> ID</th>
                            <th width="40%"> Tên Bài viết</th>
                            <th width="30%"> Danh Mục</th>
                            <th width="30%"> Tác Vụ</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script-custom')
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            var exportSetting = {
                columns: [1, 2, 3, 4]
            };
            var exportTitle = 'Danh sách bài viết đã duyệt Cho Via He'
            var list = $('#list-post').dataTable({
                "processing": true,
                "serverSide": true,
                "ajax": {
                    type: 'POST',
                    url: "{{ route('admin.ads.listDeactive') }}",
                    data: function (d) {
                        d.cate = $('#searchCateHtml').val();
                        d.boost = $('#searchBoostHtml').val();
                    }
                },
                "columns": [{
                    data: 'checkbox',
                    name: 'id',
                    orderable: false,
                }, {
                    'data': 'id',
                    'name': 'id',
                    visible: false,
                }
                    , {
                        'data': 'title',
                        'name': 'title'
                    }, {
                        'data': 'cate_id',
                        'name': 'cate_id'
                    }, {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false,
                    }
                ],
                "language": {
                    "emptyTable": "Không tìm thấy dữ liệu",
                    "info": "Đang hiển thị kết quả từ _START_ tới _END_ trên tổng số _TOTAL_ kết quả",
                    "infoEmpty": "Không có kết quả",
                    "infoFiltered": "(trên tổng số  _MAX_ kết quả được tìm thấy)",
                    "lengthMenu": "Số dòng hiển thị _MENU_",
                    "search": "Tìm kiếm:",
                    'processing': 'Đang xử lý....',
                    "zeroRecords": "Không có kết quả phù hợp",
                    "paginate": {
                        "previous": "Trở lại",
                        "next": "Tiếp",
                        "last": "Cuối",
                        "first": "Đầu"
                    }
                },
                "lengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],
                "order": [
                    [1, "asc"]
                ],
                // set the initial value
                "pageLength": 20,
                "pagingType": "bootstrap_full_number",
                buttons: [{
                    extend: 'print',
                    className: 'dt-button buttons-print',
                    exportOptions: exportSetting,
                    title: exportTitle,
                }, {
                    extend: 'pdf',
                    className: 'btn green btn-outline',
                    exportOptions: exportSetting,
                    title: exportTitle,
                }, {
                    extend: 'excel',
                    className: 'btn yellow btn-outline ',
                    exportOptions: exportSetting,
                    title: exportTitle,
                }, {
                    extend: 'csv',
                    className: 'btn purple btn-outline ',
                    exportOptions: exportSetting,
                    title: exportTitle,
                },],
            });

        });

    </script>
    @include('backend.pages.adwords.assets.custom-js')
@endpush

@push('css-plugins')
    @include('backend.pages.adwords.assets.plugin-css')
@endpush
@push('script-plugins')
    @include('backend.pages.adwords.assets.plugin-js')
@endpush