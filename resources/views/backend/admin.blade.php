<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8"/>
    <title>Metronic Admin Theme #1 | Blank Page Layout</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="Preview page of Metronic Admin Theme #1 for blank page layout" name="description"/>
    <meta content="" name="author"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('backend/assets/global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('backend/assets/global/plugins/simple-line-icons/simple-line-icons.min.css')}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('backend/assets/global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('backend/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}"
          rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
@stack('css-plugins')
<!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{{ asset('backend/assets/global/css/components-rounded.min.css')}}" rel="stylesheet"
          id="style_components" type="text/css"/>
    <link href="{{ asset('backend/assets/global/css/plugins.min.css')}}" rel="stylesheet" type="text/css"/>
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="{{ asset('backend/assets/layouts/layout/css/layout.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('backend/assets/layouts/layout/css/themes/darkblue.min.css')}}" rel="stylesheet"
          type="text/css" id="style_color"/>
    <link href="{{ asset('backend/assets/layouts/layout/css/custom.min.css')}}" rel="stylesheet" type="text/css"/>
    <!-- END THEME LAYOUT STYLES -->
    <style type="text/css" media="screen">
        .pagination > .active > a {
            padding: 3px 12px;
            max-height: 32px;
        }
    </style>
    <!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
<div class="page-wrapper">
    @include('backend.layouts.header')
    <div class='clearfix'></div>
    <div class="page-container">
    @include('backend.layouts.nav')
    <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                @yield('page-head')
                @yield('page-content')
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
    {{-- @include('backend.layouts.sidebar') --}}
</div>
@include('backend.layouts.footer')
</div>

<!--[if lt IE 9]>
<script src="{{ asset('backend/assets/global/plugins/respond.min.js')}}"></script>
<script src="{{ asset('backend/assets/global/plugins/excanvas.min.js')}}"></script>
<script src="{{ asset('backend/assets/global/plugins/ie8.fix.min.js')}}"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="{{ asset('backend/assets/global/plugins/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('backend/assets/global/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
@stack('script-plugins')
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="{{ asset('backend/assets/global/scripts/app.min.js')}}" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="{{ asset('backend/assets/layouts/layout/scripts/layout.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('backend/assets/layouts/layout/scripts/demo.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('backend/assets/layouts/global/scripts/quick-sidebar.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('backend/assets/layouts/global/scripts/quick-nav.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })

</script>
@stack('script-custom')
<!-- END THEME LAYOUT SCRIPTS -->
<style>
    .alertify {
        z-index: 99999;
    }
    .number-member .dashboard-stat .details {
        right: 0;
        padding-right: 0;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);
    }
    .number-member .dashboard-stat .details .number , .number-member .dashboard-stat .details .desc {
        text-align: center;
    }
    .number-member .dashboard-stat .details .number {
        font-size: 56px;
        font-weight: bold;
        line-height: 56px;
        padding-bottom: 10px;
        padding-top: 0;
    }
    .number-member .dashboard-stat .details .desc {

    }
    .number-member .dashboard-stat {
        position: relative;
    }
</style>
@stack('css-custom')
</body>