<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Đăng nhập vào trang quản trị chợ vỉa hè</title>
    <link rel="stylesheet" href="{{ asset('backend/login/main.css ') }}">
</head>

<body>
    <div class="main-bg"></div>
    <div class="login-form">
        <a href="#" class="admin-logo">
            <img src="{{asset('assets/img/logo/Logo-01.svg')}}" alt="">
        </a>
        <form action="{{route('admin.login')}}" method="post">
            {{ csrf_field() }}
            <h3>Đăng Nhập</h3>
            <div class="form-wrap">
                <input type="text" name="username" value="{{ old('username') }}" placeholder="Tên Tài Khoản" required> 
              {{ render_validate_msg('username',$errors) }}
            </div>
            <div class="form-wrap">
                <input type="password" name="pwd" placeholder="Mật khẩu" required> 
                  {{ render_validate_msg('pwd',$errors) }}
            </div>
            {{ render_validate_msg('errorlogin',$errors) }}
            <div class="form-wrap remember-box">
                <input type="checkbox" name="remember" value="1">
                <label> Ghi nhớ tài khoản</label>
            </div>
            <div class="form-wrap">
                <button type="submit">Đăng Nhập</button>
            </div>
        </form>
    </div>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all" rel="stylesheet" type="text/css">
</body>

</html>
