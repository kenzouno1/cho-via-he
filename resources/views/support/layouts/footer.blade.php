<!-- footer -->
<footer id="site-footer-support" class="site-footer-support site-footer text-center" role="footer support">
	<div class="container">
		<div class="footer-menu-suppor">
			<ul class="list-unstyled">
				<li>
					<a href="" title="">Trung tâm hố trợ</a>
				</li>
				<li>
					<a href="" title="">An toàn mua bán</a>
				</li>
				<li>
					<a href="" title="">Quy định cần biết</a>
				</li>
				<li>
					<a href="" title="">Liên hệ hố trợ</a>
				</li>
			</ul>
		</div>
		<!-- end footer menu suppor -->
		<hr>
		<div class="copyright">
			@ 2017 - Choviahe . All rights reserved  -  Designed by <a href="#" title="">RedSand JSC</a>
		</div>
		<!-- end copyright -->
	</div>
</footer>
<!-- end footer -->
