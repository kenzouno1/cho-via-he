<header id="site-header-support" class="site-header-support">
	<div class="container d-flex justify-content-between align-items-end">
		<div class="site-header-support-logo">
			<a href="{{ route('home')}}" title="Logo">
				<img src="{{asset('assets/img/logo/Logo-01.svg')}}" alt="Logo">
			</a>
		</div>
		<!-- end site header support logo -->
		<div class="site-header-support-menu">
			<div class="icon-toggle">
                <span></span>
                <span></span>
                <span></span>
            </div>
            <nav class="support-main-navigation">
                <ul class="main-menu list-unstyled">
                    <li><a href="{{route('support-center')}}" title="Trung tâm hỗ trợ">Trung tâm hỗ trợ </a></li>
                    <li><a href="{{route('an-toan-mua-ban')}}" title="AN TOÀN MUA BÁN"> AN TOÀN MUA BÁN </a></li>
                    <li><a href="{{route('support')}}" title="QUY ĐỊNH CẦN BIẾT">QUY ĐỊNH CẦN BIẾT </a></li>
                    <li><a href="{{route('contact')}}" title="lIÊN HỆ">lIÊN HỆ</a></li>
                </ul>
            </nav>
            <!-- end main navigation -->
		</div>
		<!-- end site header support menu -->
	</div>
</header>
<!-- end  site header support -->