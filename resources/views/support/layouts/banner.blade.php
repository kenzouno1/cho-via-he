<div class="banner-support" style="background-image: url('{{asset('assets/img/default-image/banner-support.png')}}');">
	<div class="container">
		<div class="row d-flex justify-content-center">
			<div class="col-md-10 ">
				<div class="support-search">
					<h6>Chúng tôi có thể giúp gì cho bạn ?</h6>
					<form class="form-search-support" action="">
						<div class="input-group">
					      <input type="text" class="form-control" placeholder="Nhập từ khóa bạn cần tìm">
					      <span class="input-group-btn">
					        <button class="btn btn-primary" type="button"><i class="ion-ios-search-strong"></i></button>
					      </span>
					    </div>
					</form>
					<!-- end form search support -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end banner support -->