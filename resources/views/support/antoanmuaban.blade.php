@extends('master-support')
@section('content-page')
	<main class="main-support" id="main-support">
		@include('support.layouts.banner')
		<div class="container">
			<div class="row d-flex justify-content-center">
				<div class="col-md-10">
					<div class="breadcrumb-support">
						<ol class="breadcrumb pl-0 mb-0">
						  <li class="breadcrumb-item"><a href="#" title="trang chủ">Trang chủ</a></li>
						  <li class="breadcrumb-item active">An toàn mua bán</li>
						</ol>
					</div>
					<!-- end breadcrumb support -->
					<section class="support-content bg-white p-2 px-sm-3 py-sm-4">
						<div class="support-tab-circle">
							<ul class="nav row d-flex justify-content-center" role="tablist">
							  <li class="nav-item col-4 col-md-3 text-center">
							    <a class="active" data-toggle="tab" href="#kinh-nghiem-danh-cho-nguoi-mua">
							    	<span class="card-support-icon">
							    		<i class="ion-android-contacts"></i>
							    	</span>
							    	<h5 class="card-support-title">
										Kinh nghiệm dành cho người mua
									</h5>
							    </a>
							  </li>
							  <li class="nav-item col-4 col-md-3 text-center">
							    <a data-toggle="tab" href="#kinh-nghiem-danh-cho-nguoi-ban">
							    	<span class="card-support-icon">
							    		<i class="ion-android-contact"></i>
							    	</span>
							    	<h5 class="card-support-title">
										Kinh nghiệm dành cho người bán
									</h5>
							    </a>
							  </li>
							  <li class="nav-item col-4 col-md-3 text-center">
							    <a data-toggle="tab" href="#kinh-nghiem-danh-cho-nguoi-tim-viec">
							    	<span class="card-support-icon"><i class="ion-briefcase"></i></span>
							    	<h5 class="card-support-title">
										Kinh nghiệm dành cho người tìm việc
									</h5>
							    </a>
							  </li>
							</ul>
							<!-- Tab panes -->
							<div class="tab-content">
								<div class="tab-pane active" id="kinh-nghiem-danh-cho-nguoi-mua" role="tabpanel">
								  	<h6 class="text-uppercase">Kinh nghiệm dành cho người mua</h6>
								  	<p>
								  		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
								  	</p>
								  	<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
								  	</p>
								</div>
								<div class="tab-pane" id="kinh-nghiem-danh-cho-nguoi-ban" role="tabpanel">
									<h6 class="text-uppercase">Kinh nghiệm dành cho người bán</h6>
								  	<p>
								  		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
								  	</p>
								  	<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
								  	</p>
								</div>
								<div class="tab-pane" id="kinh-nghiem-danh-cho-nguoi-tim-viec" role="tabpanel">
									<h6 class="text-uppercase">Kinh nghiệm dành cho người tìm việc</h6>
								  	<p>
								  		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
								  	</p>
								  	<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
								  	</p>
								</div>
							</div>
						</div>
						<!-- end support-tab-circle -->
					</section>
					<!-- end support content -->
				</div>
			</div>
			
		</div>
		@include('support.layouts.before-footer')
	</main>
@endsection

@push('script-custom')
	@include('support.assets.js')
@endpush
