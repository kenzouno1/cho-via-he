@extends('master-support')
@section('content-page')
	<main class="main-support" id="main-support">
		@include('support.layouts.banner')
		<div class="container">
			<div class="row d-flex justify-content-center">
				<div class="col-md-10">
					<div class="breadcrumb-support">
						<ol class="breadcrumb pl-0 mb-0">
						  <li class="breadcrumb-item"><a href="#" title="trang chủ">Trang chủ</a></li>
						  <li class="breadcrumb-item active">Liên hệ</li>
						</ol>
					</div>
					<!-- end breadcrumb support -->
					<section class="support-content bg-white p-2 px-sm-3 py-sm-4">
						<div class="support-contact row">
							<div class="col-md-5 mb-4 mb-md-0">
								<h6 class="font-weight-bold">Thông tin Chợ Vỉa Hè</h6>
								<address>
									<strong>Điện thoại:</strong> <a href="" title="hotline">0238 2 999 9xx</a><br>
									<strong>Email:</strong> <a href="mail:hotro@choviahe.com" title="Email">hotro@choviahe.com</a><br>
									<strong>Địa chỉ:</strong> Văn phòng trực thuộc tại<br>
									Số 75A, Nguyễn Minh Khai, Phường Lê Mao, Tp. Vinh, Nghệ An.
								</address>
								<a class="btn-support btn btn-secondary" href="" title="Hố trợ trực tuyến">Hố trợ trực tuyến</a>
							</div>
							<div class="col-md-7">
								<h6 class="font-weight-bold">
									Để Chợ Vỉa Hè hiểu bạn bạn, hãy nhập các thông tin đầy đủ nhất có thể chúng tôi sẽ hỗ trợ bạn tốt nhất.
								</h6>
								<form id="form-contact" class="form-contact" data-parsley-validate="">
									<div class="form-group">
									    <label>Tên của bạn *</label>
									    <input type="text" class="form-control" data-parsley-required="" data-parsley-required-message="Nhập Tên của bạn">
									</div>
									<div class="row">
										<div class="form-group col-md-6">
										    <label>Email của bạn *</label>
										    <input type="email" class="form-control" data-parsley-required="" data-parsley-required-message="Nhập Email của bạn" data-parsley-error-message="Email nhập vào không hợp lệ">
										</div>
										<div class="form-group col-md-6">
										    <label>Số điện thoại của bạn *</label>
										    <input type="number" class="form-control" data-parsley-required="" data-parsley-required-message="Nhập số điện thoại của bạn" data-parsley-length="[10, 11]" data-parsley-length-message="Vui lòng kiểm tra lại số điện thoại" data-parsley-type="number">
										</div>
									</div>
									<div class="form-group">
									    <label>Vấn đề bạn muốn trao đổi</label>
									    <select class="form-control">
										  <option>Vấn đề 1</option>
										  <option>Vấn đề 2</option>
										  <option>Vấn đề 3</option>
										  <option>Vấn đề 4</option>
										</select>
									</div>
									<div class="form-group">
									    <label>Nội dung *</label>
									    <textarea class="form-control" id="exampleTextarea" rows="3" data-parsley-required="" data-parsley-required-message="Nhập nội dung " data-parsley-length="[6, 5000]" data-parsley-length-message="Nội dung của bạn nhập không được ít hơn 6 ký tự và không được nhiều hơn 5000 ký tự"></textarea>
									</div>
									<div class="form-group">
									    <label>File đính kèm</label>
									     <input type="file" class="form-control-file" id="exampleInputFile" aria-describedby="fileHelp">
									</div>
									<button type="submit" class="btn btn-primary px-4 py-2 text-uppercase">Gửi yêu cầu</button>
								</form>
							</div>
						</div>
					</section>
					<!-- end support content -->
				</div>
			</div>
			
		</div>
		@include('support.layouts.before-footer')
	</main>
@endsection

@push('script-custom')
	@include('support.assets.js')
@endpush
