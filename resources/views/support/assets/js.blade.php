<script src="{!! asset('assets/vendor/parsley/parsley.min.js') !!}"></script>
<script>

// menu mobile
$('.site-header-support-menu .icon-toggle').click(function(event) {
	$('.support-main-navigation').slideToggle('fast');
});
//collapse
$('.card-support-list-header ').click(function(event) {
	if($(this).hasClass('show')){
		$('.card-support-list-header ').removeClass('show');
	} else{
		$('.card-support-list-header ').removeClass('show');
		$(this).addClass('show');
	}
	
});
//validate form new post
$("#form-contact").parsley({
    errorClass: 'has-danger',
    successClass: 'has-success',
    classHandler: function (ParsleyField) {
        return ParsleyField.$element.parents('.form-group');
    },
    errorsContainer: function (ParsleyField) {
        return ParsleyField.$element.parents('.form-group');
    },
    errorsWrapper: '<span class="form-control-feedback">',
    errorTemplate: '<div></div>'
});
//end validate form
</script>