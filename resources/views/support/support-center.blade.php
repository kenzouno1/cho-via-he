@extends('master-support')
@section('content-page')
	<main class="main-support" id="main-support">
		@include('support.layouts.banner')
		<div class="container">
			<div class="breadcrumb-support">
				<ol class="breadcrumb pl-0 mb-0">
				  <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
				  <li class="breadcrumb-item active">Trung tâm hố trợ</li>
				</ol>
			</div>
			<!-- end breadcrumb support -->
			<section class="support-center bg-white">
				<div class="row d-flex justify-content-center">
					<div class="col-md-10">
						<h5 class="text-uppercase mb-4 text-center">Các vấn đề thường gặp</h5>
						<div class="row d-flex flex-row mb-4 mb-lg-5">
							<div class="card-support text-center">
								<a href="">
									<div class="card-support-icon">
										<i class="ion-clipboard"></i>
									</div>
									<h5 class="card-support-title">
										Xóa tin
									</h5>
									<p class="card-support-description hidden-md-down">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad 
									</p>
								</a>
							</div>
							<!-- end  card support -->
							<div class="card-support text-center">
								<a href="">
									<div class="card-support-icon">
										<i class="ion-social-usd-outline"></i>
									</div>
									<h5 class="card-support-title">
										Vì sao bạn phải nạp tiền?
									</h5>
									<p class="card-support-description hidden-md-down">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad 
									</p>
								</a>
							</div>
							<!-- end  card support -->
							<div class="card-support text-center">
								<a href="">
									<div class="card-support-icon">
										<i class="ion-shield"></i>
									</div>
									<h5 class="card-support-title">
										An toàn mua bán
									</h5>
									<p class="card-support-description hidden-md-down">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad 
									</p>
								</a>
							</div>
							<!-- end  card support -->
							<div class="card-support text-center">
								<a href="">
									<div class="card-support-icon">
										<i class="ion-compose"></i>
									</div>
									<h5 class="card-support-title">
										Mẹo đăng tin
									</h5>
									<p class="card-support-description hidden-md-down">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad 
									</p>
								</a>
							</div>
							<!-- end  card support -->
							<div class="card-support text-center">
								<a href="">
									<div class="card-support-icon">
										<i class="ion-android-mail"></i>
									</div>
									<h5 class="card-support-title">
										Để tin không bị từ chối?
									</h5>
									<p class="card-support-description hidden-md-down">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad 
									</p>
								</a>
							</div>
							<!-- end  card support -->
						</div>
						<a class="btn-view-all btn btn-secondary text-uppercase text-center font-weight-bold" href="{{route('support')}}" title="Xem tất cả các mục">Xem tất cả các mục</a>
					</div>
				</div>
			</section>
		</div>
		@include('support.layouts.before-footer')
	</main>
@endsection

@push('script-custom')
	@include('support.assets.js')
@endpush
