@extends('master-support')
@section('content-page')
	<main class="main-support" id="main-support">
		@include('support.layouts.banner')
		<div class="container">
			<div class="row d-flex justify-content-center">
				<div class="col-md-10">
					<div class="breadcrumb-support">
						<ol class="breadcrumb pl-0 mb-0">
						  <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
						  <li class="breadcrumb-item active">Trung tâm hố trợ</li>
						</ol>
					</div>
					<!-- end breadcrumb support -->
					<section class="support-content bg-white p-2 px-sm-3 py-sm-4">
						<div id="accordion" role="tablist" aria-multiselectable="true">
							<div class="card card-support-list">
							    <div class="card-support-list-header show" role="tab" id="headingOne">
							      <h5 class="mb-0">
							        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
							          	<i class="ion-dollar"></i>
							          	Bán hàng
							        </a>
							      </h5>
							    </div>

							    <div id="collapseOne" class="collapse in" role="tabpanel" aria-labelledby="headingOne">
							      	<div class="card-support-list-block">
							        	<h6>Chủ đề 1</h6>
							        	<ol>
							        		<li>
							        			<a href="{{route('support-detail')}}" title="">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore ?</a>
							        		</li>
							        		<li>
							        			<a href="{{route('support-detail')}}" title="">Ut enim ad minim veniam ?</a>
							        		</li>
							        	</ol>
							        	<h6>Chủ đề 2</h6>
							        	<ol>
							        		<li>
							        			<a href="" title="">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore ?</a>
							        		</li>
							        		<li>
							        			<a href="" title="">Ut enim ad minim veniam ?</a>
							        		</li>
							        	</ol>
							      	</div>
							    </div>
							</div>
							<!-- end card support list -->
							<div class="card card-support-list">
							    <div class="card-support-list-header" role="tab" id="headingTwo">
							      <h5 class="mb-0">
							        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
							          	<i class="ion-android-cart"></i>
							          	Mua hàng
							        </a>
							      </h5>
							    </div>
							    <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
							     	<div class="card-support-list-block">
							        	<h6>Chủ đề 1</h6>
							        	<ol>
							        		<li>
							        			<a href="" title="">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore ?</a>
							        		</li>
							        		<li>
							        			<a href="" title="">Ut enim ad minim veniam ?</a>
							        		</li>
							        	</ol>
							        	<h6>Chủ đề 2</h6>
							        	<ol>
							        		<li>
							        			<a href="" title="">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore ?</a>
							        		</li>
							        		<li>
							        			<a href="" title="">Ut enim ad minim veniam ?</a>
							        		</li>
							        	</ol>
							      	</div>
							    </div>
							</div>
							<!-- end card support list -->
							<div class="card card-support-list">
							    <div class="card-support-list-header" role="tab" id="headingThree">
							      <h5 class="mb-0">
							        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
							        	<i class="ion-shield"></i>
							          	An toàn mua bán
							        </a>
							      </h5>
							    </div>
							    <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
							    	<div class="card-support-list-block">
							        	<h6>Chủ đề 1</h6>
							        	<ol>
							        		<li>
							        			<a href="" title="">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore ?</a>
							        		</li>
							        		<li>
							        			<a href="" title="">Ut enim ad minim veniam ?</a>
							        		</li>
							        	</ol>
							        	<h6>Chủ đề 2</h6>
							        	<ol>
							        		<li>
							        			<a href="" title="">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore ?</a>
							        		</li>
							        		<li>
							        			<a href="" title="">Ut enim ad minim veniam ?</a>
							        		</li>
							        	</ol>
							      	</div>
							    </div>
							</div>
							<!-- end card support list -->
							<div class="card card-support-list">
							    <div class="card-support-list-header" role="tab" id="headingFour">
							      <h5 class="mb-0">
							        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
							        <i class="ion-person"></i>
							          Hỗ trợ tài khoản
							        </a>
							      </h5>
							    </div>
							    <div id="collapseFour" class="collapse" role="tabpanel" aria-labelledby="headingFour">
							    	<div class="card-support-list-block">
							        	<h6>Chủ đề 1</h6>
							        	<ol>
							        		<li>
							        			<a href="" title="">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore ?</a>
							        		</li>
							        		<li>
							        			<a href="" title="">Ut enim ad minim veniam ?</a>
							        		</li>
							        	</ol>
							        	<h6>Chủ đề 2</h6>
							        	<ol>
							        		<li>
							        			<a href="" title="">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore ?</a>
							        		</li>
							        		<li>
							        			<a href="" title="">Ut enim ad minim veniam ?</a>
							        		</li>
							        	</ol>
							      	</div>
							    </div>
							</div>
							<!-- end card support list -->
							<div class="card card-support-list">
							    <div class="card-support-list-header" role="tab" id="headingFive">
							      <h5 class="mb-0">
							        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
							        <i class="ion-clipboard"></i>
							         	Quy định cần biết
							        </a>
							      </h5>
							    </div>
							    <div id="collapseFive" class="collapse" role="tabpanel" aria-labelledby="headingFive">
							    	<div class="card-support-list-block">
							        	<h6>Chủ đề 1</h6>
							        	<ol>
							        		<li>
							        			<a href="" title="">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore ?</a>
							        		</li>
							        		<li>
							        			<a href="" title="">Ut enim ad minim veniam ?</a>
							        		</li>
							        	</ol>
							        	<h6>Chủ đề 2</h6>
							        	<ol>
							        		<li>
							        			<a href="" title="">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore ?</a>
							        		</li>
							        		<li>
							        			<a href="" title="">Ut enim ad minim veniam ?</a>
							        		</li>
							        	</ol>
							      	</div>
							    </div>
							</div>
							<!-- end card support list -->
						</div>
					</section>
				</div>
			</div>
			
		</div>
		@include('support.layouts.before-footer')
	</main>
@endsection

@push('script-custom')
	@include('support.assets.js')
@endpush
