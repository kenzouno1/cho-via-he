@extends('master-support')
@section('content-page')
	<main class="main-support" id="main-support">
		@include('support.layouts.banner')
		<div class="container">
			<div class="row d-flex justify-content-center">
				<div class="col-md-10">
					<div class="breadcrumb-support">
						<ol class="breadcrumb pl-0 mb-0">
							<li class="breadcrumb-item"><a href="#" title="trang chủ">Trang chủ</a></li>
							<li class="breadcrumb-item active">Quy định cần biết</li>
						</ol>
					</div>
					<!-- end breadcrumb support -->
					<section class="support-content bg-white p-2 px-sm-3 py-sm-4">
						<div class="support-detail">
							<div class="support-detail-header dropdown">
								<a class="text-uppercase d-flex align-items-center" href="https://example.com" id="dropdownMenuLink" data-toggle="dropdown">
									<i class="ion-android-menu"></i> CÁC QUY ĐỊNH CẦN BIẾT khi sự dụng hệ thống chợ vỉa hè
								</a>

								<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
									<ol>
										<li>
											<a href="" title="">Quy định 1</a>
											<ol>
												<li>
													<a href="" title="">Quy định 1.1</a>
												</li>
												<li>
													<a href="" title="">Quy định 1.2</a>
												</li>
												<li>
													<a href="" title="">Quy định 1.3</a>
												</li>
												<li>
													<a href="" title="">Quy định 1.4</a>
												</li>
												<li>
													<a href="" title="">Quy định 1.5</a>
												</li>
											</ol>
										</li>
										<li>
											<a href="" title="">Quy định 2</a>
										</li>
										<li>
											<a href="" title="">Quy định 3</a>
										</li>
										<li>
											<a href="" title="">Quy định 4</a>
										</li>
										<li>
											<a href="" title="">Quy định 5</a>
										</li>
									</ol>
								</div>
							</div>
							<!-- end support-detail-header -->
							<div class="support-detail-content">
								<h6>I - Quy định 1</h6>
								<p>
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
								</p>
								<p>
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
								</p>
								<p>
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
								</p>
								<p>
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
								</p>
								<p>
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
								</p>
							</div>
							<!-- end support detail contet -->
							<div class="btn-support-relate d-flex justify-content-center">
								<a class="btn btn-secondary" href="" title="Bài viết trước">
									<i class="ion-angle-double-left"></i> Bài viết trước
								</a>
								<a class="btn btn-secondary" href="" title="Bài viết kế tiếp">
									Bài viết kế tiếp <i class="ion-angle-double-right"></i>
								</a>
							</div>
							<!-- end btn relate -->
							<div class="support-detail-relate">
								<h6>
									Các câu hỏi liên quan khác
								</h6>
								<ul class="list-unstyled">
									<li>
										<a href="" title="">
											Quy chế tài khoản
										</a>
									</li>
									<li>
										<a href="" title="">
											Chính sách bảo mật thông tin cá nhân
										</a>
									</li>
									<li>
										<a href="" title="">
											Quy định sự dụng tài khoản để đăng bài viết
										</a>
									</li>
									<li>
										<a href="" title="">
											Quy định kiểm duyệt thông tin và thời giản kiểm duyệt thông tin khi đưa lên hệ thống
										</a>
									</li>
								</ul>
							</div>
							<!-- end  support detail relate -->
						</div>
						<!-- end support-detail -->
					</section>
					<!-- end support content -->
				</div>
			</div>

		</div>
		@include('support.layouts.before-footer')
	</main>
@endsection

@push('script-custom')
	@include('support.assets.js')
@endpush
