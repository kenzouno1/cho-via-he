<ul class="item-save-search list-unstyled">
    <a href="{{\App\Helper\UrlHelper::getSavedSearchUrl($search)}}" title="Tìm kiếm">
        <li class="item-save-search-title">
            @if($search['keywords']!='')
                {{$search['keywords']}}
            @else
                Không có từ khóa
            @endif
        </li>
        <li><i class="ion-ios-location"></i> {{$search['address']}} (+ {{$search['dis']}} Km)</li>
        <li><i class="{{$search['cate']['image']}}"></i> {{$search['cate']['name']}}</li>
    </a>
    <a class="btn-delete" href="{{route('saved_search.remove',['index'=>$key])}}" title="Xóa mục này">
        <i class="ion-trash-a"></i>
    </a>
</ul>