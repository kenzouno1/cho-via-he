<div class="not-news text-center">
    <span class="icon-lagre"><i class="ion-ios-star"></i></span>
    <strong class="mb-2">Bạn chưa có đánh dấu “quan tâm” nào cả</strong>
    <p>
        Bằng cách mỗi lần xem hoặc tìm kiếm, hãy đánh dấu vào dấu sao nều bạn cần lưu tin vào danh sách
        Quan tâm
    </p>
</div>