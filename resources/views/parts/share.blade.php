<div class="dropdown share-post ">
    <button class="d-flex align-items-center dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="ion-android-share-alt"></i> Chia sẻ bài đăng
    </button>
    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
        <a onclick="window.open(this.href, 'mywin','left=50,top=50,width=600,height=350,toolbar=0'); return false;" class="dropdown-item" href="https://www.facebook.com/sharer.php?u={{$ads->getUrl()}}"><i class="ion-social-facebook"></i>Facebook</a>
        <a onclick="window.open(this.href, 'mywin','left=50,top=50,width=600,height=350,toolbar=0'); return false;" class="dropdown-item" href="https://twitter.com/intent/tweet?url={{$ads->getUrl()}}&text={{$ads->title}}&hashtags=choviahe"><i class="ion-social-twitter"></i>Twitter</a>
        <a onclick="window.open(this.href, 'mywin','left=50,top=50,width=600,height=350,toolbar=0'); return false;" class="dropdown-item" href="https://plus.google.com/share?url={{$ads->getUrl()}}"><i class="ion-google-plus"></i>Google Plush</a>
    </div>
</div>
{{--end share-post--}}
<div class="report-post-mobile hidden-md-up">
    <a data-toggle="modal" data-target="#modal-report" href="#" title="Báo cáo tin"><i class="ion-flag"></i> Báo cáo tin</a>
</div>
{{--end report-post-mobile--}}