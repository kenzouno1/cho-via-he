<!DOCTYPE html>
<html class="no-js " lang="en">
<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{isset($title) ? $title :'Chợ vỉa Hè | Trăm người bán - Vạn người mua'}}</title>
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('layouts.favicon')
    <!-- font -->
    {{--<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700&amp;subset=vietnamese"--}}
          {{--rel="stylesheet">--}}
    {{--<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=vietnamese" rel="stylesheet">--}}
    <link rel="stylesheet" href="{!! asset('assets/fonts/ionicons/css/ionicons.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/fonts/font-awesome/css/font-awesome.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/css/bootstrap.min.css') !!}">
    @stack('css-plugins')
    <link rel="stylesheet" href="{!! asset('assets/vendor/CustomScrollbar/jquery.mCustomScrollbar.min.css') !!}">
    <!-- Custom CSS  -->
    @stack('css-custom')
    <link rel="stylesheet" href="{!! asset('assets/css/main.min.css') !!}">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    {{--<!-- Modernizr js -->--}}
    {{--<script src="{!! asset('assets/js/modernizr.min.js') !!}"></script>--}}
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

    @stack('common')
    <script>
        var cvh_data = {
            home_url: '{{route('home')}}',
            favorites_url: '{{route('favorite.add')}}',
            saved_search_url: '{{route('saved_search')}}',
        }
        @auth
            var user_common = {!! Auth::user()->toCommon() !!}
        @endauth
    </script>
</head>

<body>
<div class="bg-overlay"></div>
<div id="wrapper">
    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a
            href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    @include('layouts.header')
    @yield('content-page')
    @include('layouts.footer')
</div>
<!--Javascript Library -->
<script src="{!! asset('assets/vendor/jquery2-2-4/jquery.min.js') !!}"></script>
<!-- getbootstrap -->
<script src="{!! asset('assets/js/tether.min.js') !!}"></script>
<script src="{!! asset('assets/js/bootstrap.min.js') !!}"></script>
@stack('script-plugins')
@yield('js')
<script src="{!! asset('assets/vendor/CustomScrollbar/jquery.mCustomScrollbar.js') !!}"></script>
@stack('script-custom')
<!-- Main Script -->
<script src="{!! asset('assets/js/main.js') !!}"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })
</script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/59db184b4854b82732ff4672/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
</body>
</html>