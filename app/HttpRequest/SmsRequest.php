<?php

namespace App\HttpRequest;

use GuzzleHttp\Client;

class SmsRequest
{
    private $apiKey;
    private $brandName;
    private $url;
    private $hostName;

    function __construct()
    {
        $this->apiKey = env('SMS_API_KEY');
        $this->brandName = env('SMS_API_BRAND_NAME');
        $this->url = env('SMS_API_URL');
        $this->hostName = env('SMS_HOST_NAME');
    }

    public function sendSms($phone = null, $content = null, $method = 'POST')
    {
        if (is_null($phone) || is_null($content))
            return false;
        $client = new Client();
        $body = [
            'PhoneNumber' => [$phone],
            'Content' => $content,
            'BrandName' => $this->brandName,
        ];
        $res = $client->request($method, $this->url, [
            'headers' => [
                'AccessKey' => $this->apiKey,
                'Content-Type' => 'application/json',
                'Origin' => $this->hostName,
            ],
            'body' => json_encode($body)
        ]);
        return $res;
    }

    public static function sendActiveCode($phone, $code)
    {
        $sms = new SmsRequest();
        $content = 'Ma kich hoat tai choviahe cua ban la ' . $code;
        return $sms->sendSms($phone, $content);
    }

    public static function sendForgotPwd($phone, $newPwd)
    {
        $sms = new SmsRequest();
        $content = 'Ma xac nhan dat lai mat khau cua ban tai choviahe la ' . $newPwd;
        return $sms->sendSms($phone, $content);
    }
}
