<?php
/**
 * Created by PhpStorm.
 * User: Dat Nguyen
 * Date: 7/28/2017
 * Time: 1:36 PM
 */

namespace App\ModelTrait;


use App\Ads;
use App\AdsMeta;
use function dump;

trait AdsTrait
{

    public function author()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function boost()
    {
        return $this->hasMany('App\AdsBoost', 'ads_id');
    }

    public function metas()
    {
        return $this->hasMany('App\AdsMeta', 'ads_id');
    }

    public function category()
    {
        return $this->belongsTo('App\Category', 'cate_id');
    }

    public function threads()
    {
        return $this->hasMany('App\Chat\Thread', 'ads_id');
    }

    protected function updateStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function locked()
    {
        return $this->updateStatus('locked');
    }

    public function active()
    {
        return $this->updateStatus('active');
    }

    public function deactive()
    {
        return $this->updateStatus('deactive');
    }

    public function hidden()
    {
        return $this->updateStatus('hidden');
    }

    public function update_meta($key, $value)
    {
        $meta = $this->metas->filter(function ($meta) use ($key) {
            return $meta->meta_key == $key;
        })->first();
        if ($meta) {
            $meta->meta_value = $value;
            $meta->save();
        } else {
            $this->insert_meta($key, $value);
        }
        return $meta;
    }

    public function get_meta($key, $default = false)
    {
        $meta = $this->metas->filter(function ($meta) use ($key) {
            return $meta->meta_key == $key;
        })->first();
        return $meta ? $meta->meta_value : $default;
    }

    public function insert_meta($key, $value)
    {
        $meta = new AdsMeta();
        $meta->meta_key = $key;
        $meta->meta_value = $value;
        $meta->ads_id = $this->id;
        $meta->save();
    }

    public function scopenearest($query, $from_latitude, $from_longitude, $distance = 10)
    {
        $distance += 5;
        $raw = \DB::raw('(6371 * acos( cos( radians(' . $from_latitude . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $from_longitude . ') ) + sin( radians(' . $from_latitude . ') ) * sin( radians( lat ) ) ) ) ');
        $rawas = \DB::raw('ROUND((6371 * acos( cos( radians(' . $from_latitude . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $from_longitude . ') ) + sin( radians(' . $from_latitude . ') ) * sin( radians( lat ) ) ) )) as dis');
        return $query->selectRaw('*')->addSelect($rawas)->orderBy($raw, 'asc')->where($raw, '<=', intval($distance));
    }

    protected function getRandomByFeatures($query, $feature)
    {
        return $query->whereHas('boost', function ($subQuery) use ($feature) {
            $subQuery->where('features', 'like', "%$feature%");
            $subQuery->expired();
        });
    }

    public function scopeTopSearch($query, $limit = 6)
    {
        return $this->getRandomByFeatures($query, 'top-search', $limit);
    }

    public function scopeShowHome($query, $limit = 6)
    {
        return $this->getRandomByFeatures($query, 'show-home', $limit);
    }


    public function scopeFullTextSearch($query, $search)
    {
        $whereRaw = \DB::raw("MATCH(title) AGAINST('?' IN BOOLEAN MODE) ");
        $selectRaw = \DB::raw("MATCH(title) AGAINST('?' IN BOOLEAN MODE) ");
        return $query->whereRaw($whereRaw, $search)->selectRaw($selectRaw, array($search));
//        return $query->where('title','like','%'.$search.'%');
    }

    public function scopeOrderByFullTextSearch($query, $search)
    {
        $raw = \DB::raw("MATCH(title) AGAINST(?) desc");
        return $query->orderByRaw($raw, array($search));
    }

    public function getRelated($lstCate, $limit = 3)
    {
        $lstCateId = $lstCate->pluck('id');
        return Ads::nearest($this->lat, $this->lng, 3000)
            ->with(['author', 'category', 'boost'])
            ->whereIn('cate_id', $lstCateId)
            ->where('id', '!=', $this->id)
            ->orderByDesc('cate_id')
            ->limit($limit)->get();
    }

    public function scopeIsActive($query)
    {
        return $this->queryIsStatus($query, 'active');
    }

    public function scopeIsDeactive($query)
    {
        return $this->queryIsStatus($query, 'deactive');
    }

    public function scopeIsHidden($query)
    {
        return $this->queryIsStatus($query, 'hidden');
    }

    public function scopeWithoutHidden($query)
    {
        return $this->queryWithoutStatus($query, 'hidden');
    }

    public function scopeWithoutActive($query)
    {
        return $this->queryWithoutStatus($query, 'active');
    }

    public function queryWithoutStatus($query, $status)
    {
        return $query->where('status', 'not like', $status);
    }

    public function queryIsStatus($query, $status)
    {
        return $query->whereStatus($status);
    }

}