<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Capability
 *
 * @property int $id
 * @property int $role_id
 * @property string $capability
 * @method static \Illuminate\Database\Query\Builder|\App\Capability whereCapability($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Capability whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Capability whereRoleId($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AbstractModel orderByField($field, $values)
 */
class Capability extends AbstractModel
{
  	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'role_id', 'capability'
	];
	public $timestamps = false;

	protected $hidden = [
	'deleted_at'
	];

	protected $table = 'role_capability';

}
