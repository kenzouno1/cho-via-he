<?php

namespace App;

use App\Chat\Message;
use App\Helper\AdsHelper;
use App\Helper\UtilHelper;
use Carbon\Carbon;
use Cmgmyr\Messenger\Traits\Messagable;
use Cog\Contracts\Ban\Bannable as BannableContract;
use Cog\Laravel\Ban\Traits\Bannable;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\SoftDeletes;
use function asset;
use function is_null;
use function json_encode;


/**
 * App\User
 *
 * @property int $id
 * @property string|null $phone
 * @property string|null $name
 * @property string|null $email
 * @property string $password
 * @property string|null $birthday
 * @property string $money
 * @property string|null $address
 * @property string|null $provider
 * @property string $provider_id
 * @property string|null $deleted_at
 * @property string|null $avatar
 * @property string $status
 * @property string|null $banned_at
 * @property string|null $remember_token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Cog\Ban\Models\Ban[] $bans
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\UserMeta[] $metas
 * @property-read \App\Roles $role
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Sms[] $sms
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\User onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereBannedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereProvider($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereProviderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\User withoutTrashed()
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Ads[] $ads
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Chat\Message[] $messages
 * @property-read \Illuminate\Database\Eloquent\Collection|\Cmgmyr\Messenger\Models\Participant[] $participants
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Chat\Thread[] $threads
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\PaymentLogs[] $payment
 * @property-read mixed $money_format
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AbstractModel orderByField($field, $values)
 */
class User extends AbstractModel implements AuthenticatableContract, BannableContract
{
    use SoftDeletes;
    use Bannable;
    use Authenticatable;
    use Messagable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'phone', 'name', 'email', 'password', 'birthday', 'money', 'address', 'status', 'avatar', 'provider', 'old_money'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'provider_id',
    ];

    protected $table = 'users';

    public function role()
    {
        return $this->belongsto('App\Roles', 'id');
    }

    public function sms()
    {
        return $this->hasMany('App\Sms', 'user_id');
    }

    public function metas()
    {
        return $this->hasMany('App\UserMeta', 'user_id');
    }

    public function ads()
    {
        return $this->hasMany('App\Ads', 'user_id');
    }

    public function payment()
    {
        return $this->hasMany('App\PaymentLogs', 'user_id');
    }

    public function block($time)
    {
        $this->bans()->create([
            'expired_at' => $time,
        ]);
    }

    public function label_class()
    {
        if ($this->isBanned()) {
            return 'danger';
        }
        switch ($this->status) {
            case 'wait' :
                return 'warning';
            default :
                return 'success';
        }
    }

    protected function validateCode($code, $type = 'active')
    {
        return Sms::whereCode($code)
            ->whereType($type)
            ->whereUserId($this->id)
            ->whereStatus(1)
            ->where('expire_at', '>', Carbon::now()->toDateTimeString())
            ->first();
    }

    public function active($code)
    {
        return $this->validateCode($code);
    }

    public function forgot($code)
    {
        return $this->validateCode($code, 'forgot');
    }

    public function isFacebookAccount()
    {
        return $this->provider == 'facebook';
    }

    public function isGoogleAccount()
    {
        return $this->provider == 'google';
    }


    public function isSocialAccount()
    {
        if ($this->isFacebookAccount() || $this->isGoogleAccount()) {
            return true;
        }
        return false;
    }

    public function hasAddress()
    {
        return (!is_null($this->address) && $this->address != '');
    }

    public function hasEmail()
    {
        return (!is_null($this->email) && $this->email != '');
    }

    public function getAvatar()
    {
        if (is_null($this->avatar) || !$this->avatar) {
            return asset('avatar/avatar-default.jpg');
        }
        return $this->avatar;
    }

    public function getAvatarAttribute($value)
    {
        if (is_null($value)) {
            return asset('avatar/avatar-default.jpg');
        }
        if (starts_with($value, 'avatar/')) {
            return asset($value);
        }
        return $value;
    }

    public function update_meta($key, $value)
    {
        $meta = UserMeta::whereMetaKey($key)->whereUserId($this->id)->limit(1)->first();
        if ($meta) {
            $meta->meta_value = $value;
            $meta->save();
        } else {
            $this->insert_meta($key, $value);
        }
        return $meta;
    }

    public function get_meta($key, $default = false)
    {
        $meta = $this->metas->filter(function ($meta) use ($key) {
            return $meta->meta_key == $key;
        })->first();
        return $meta ? $meta->meta_value : $default;
    }

    public function insert_meta($key, $value)
    {
        $meta = new UserMeta();
        $meta->meta_key = $key;
        $meta->meta_value = $value;
        $meta->user_id = $this->id;
        $meta->save();
    }

    public function updateFavorites($id)
    {
        $favorites = $this->get_meta('_favorites', json_encode([]));
        $favorites = json_decode($favorites, true);
        if (!in_array($id, $favorites)) {
            $favorites = AdsHelper::push($favorites, $id);
        } else {
            $index = array_search($id, $favorites);
            unset($favorites[$index]);
        }
        $favoritesJson = json_encode($favorites);
        $this->update_meta('_favorites', $favoritesJson);
        return $favorites;
    }

    public function updateSavedSearch($params)
    {
        $savedSearch = $this->getSavedSearch();
        if (!UtilHelper::arrayDiffList($savedSearch, $params)) {
            $savedSearch = AdsHelper::push($savedSearch, $params, 12);
            $savedSearchJson = json_encode($savedSearch);
            $this->update_meta('_saved-search', $savedSearchJson);
        }
        return $savedSearch;
    }

    public function updateMoney($amount)
    {
        $this->old_money = $this->money;
        $this->money += $amount;
    }

    public function removeSavedSeach($index)
    {
        $savedSearch = $this->getSavedSearch();
        if (isset($savedSearch[$index])) {
            unset($savedSearch[$index]);
            $savedSearchJson = json_encode($savedSearch);
            $this->update_meta('_saved-search', $savedSearchJson);
        }
        return $savedSearch;
    }

    public function removeAllSavedSeach()
    {
        $this->update_meta('_saved-search', json_encode([]));
    }

    public function getMoneyFormatAttribute()
    {
        return UtilHelper::money($this->money);
    }

    public function getSavedSearch($max = 6)
    {
        $arr = json_decode($this->getSavedSearchJson(), true);
        return array_slice($arr, 0, $max);
    }

    public function getSavedSearchJson()
    {
        return $this->get_meta('_saved-search', json_encode([]));
    }

    public function checkPriceAds($price)
    {
        return $this->money >= $price;
    }

    public function toCommon()
    {
        return json_encode([
            'id' => $this->id,
            'money' => $this->money,
        ]);
    }

    public function isNotActive()
    {
        return $this->status == 'wait';
    }

    public function isActive()
    {
        return $this->status == 'active';
    }

    public function isMod()
    {
        $lstMod = config('conf.mod');
        if (in_array($this->phone, $lstMod)) {
            return true;
        }
        return false;
    }
}