<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\UserMeta
 *
 * @property int $id
 * @property string $key
 * @property string $value
 * @property int $user_id
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Query\Builder|\App\UserMeta whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\UserMeta whereUserId($value)
 * @mixin \Eloquent
 * @property string $meta_key
 * @property string $meta_value
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserMeta whereMetaKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserMeta whereMetaValue($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AbstractModel orderByField($field, $values)
 */
class UserMeta extends AbstractModel
{
    protected $table = 'user_meta';
    public $timestamps =false;
    protected $fillable = [
        'key','value',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
