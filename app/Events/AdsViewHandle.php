<?php

namespace App\Events;

use App\Ads;
use Illuminate\Queue\SerializesModels;

class AdsViewHandle
{
    use  SerializesModels;

    public $ads;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Ads $ads)
    {
        $this->ads = $ads;
    }
}
