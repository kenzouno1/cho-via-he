<?php
/**
 * Created by PhpStorm.
 * User: Dat Nguyen
 * Date: 8/10/2017
 * Time: 3:35 PM
 */

namespace App\Chat;


use App\Helper\AuthHelper;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;
/**
 * App\Chat\Message
 *
 * @property int $id
 * @property int $thread_id
 * @property int $user_id
 * @property string $body
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Cmgmyr\Messenger\Models\Participant[] $participants
 * @property-read \App\Chat\Thread $thread
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Chat\Message whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Chat\Message whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Chat\Message whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Chat\Message whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Chat\Message whereThreadId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Chat\Message whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Chat\Message whereUserId($value)
 * @mixin \Eloquent
 */
class Message extends \Cmgmyr\Messenger\Models\Message
{
    public function getMessageAlight()
    {
        if (AuthHelper::current_user()->id == $this->user_id) {
            return 'right';
        }
        return 'left';
    }
}