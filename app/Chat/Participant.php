<?php
/**
 * Created by PhpStorm.
 * User: Dat Nguyen
 * Date: 9/27/2017
 * Time: 3:44 PM
 */

namespace App\Chat;


/**
 * App\Chat\Participant
 *
 * @property int $id
 * @property int $thread_id
 * @property int $user_id
 * @property \Carbon\Carbon|null $last_read
 * @property \Carbon\Carbon|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Chat\Thread $thread
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Chat\Participant whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Chat\Participant whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Chat\Participant whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Chat\Participant whereLastRead($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Chat\Participant whereThreadId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Chat\Participant whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Chat\Participant whereUserId($value)
 * @mixin \Eloquent
 */
class Participant extends \Cmgmyr\Messenger\Models\Participant
{
//    protected $table = 'chat_participants';
//    public function __construct(array $attributes = [])
//    {
//        parent::__construct($attributes);
//        $this->table = 'chat_participants';
//    }
}