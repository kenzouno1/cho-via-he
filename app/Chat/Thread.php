<?php

namespace App\Chat;

use App\Helper\AuthHelper;
use App\User;
use Cmgmyr\Messenger\Models\Models;
use function is_null;

/**
 * App\Chat\Thread
 *
 * @property int $id
 * @property string $subject
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property int $ads_id
 * @property-read \App\Ads $ads
 * @property-read \Cmgmyr\Messenger\Models\Message $latest_message
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Chat\Message[] $messages
 * @property-read \Illuminate\Database\Eloquent\Collection|\Cmgmyr\Messenger\Models\Participant[] $participants
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|\Cmgmyr\Messenger\Models\Thread between($participants)
 * @method static \Illuminate\Database\Eloquent\Builder|\Cmgmyr\Messenger\Models\Thread forUser($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Chat\Thread forUserThread($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|\Cmgmyr\Messenger\Models\Thread forUserWithNewMessages($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Chat\Thread userAdsThread($adsId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Chat\Thread whereAdsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Chat\Thread whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Chat\Thread whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Chat\Thread whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Chat\Thread whereSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Chat\Thread whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Chat\Thread limitMessage($limit)
 */
class Thread extends \Cmgmyr\Messenger\Models\Thread
{
    protected $fillable = ['subject', 'ads_id'];

    public function users()
    {
        return $this->belongsToMany(User::class, 'participants', 'thread_id', 'user_id');
    }

    public function ads()
    {
        return $this->belongsTo('App\Ads', 'ads_id', 'id');
    }

    public function scopeUserAdsThread($query, $adsId)
    {
        return $query->where('ads_id', '=', $adsId)->groupBy('id');
    }

    public function scopeForUserThread($query, $userId)
    {
        $threadsTable =Models::table('threads');
        $participantsTable = Models::table('participants');
//        $participantsTable = 'participants';
//        $threadsTable = 'threads';
        return $query->join($participantsTable, $this->getQualifiedKeyName(), '=', $participantsTable . '.thread_id')
            ->where($participantsTable . '.user_id', $userId)
            ->where($participantsTable . '.deleted_at', null)
            ->with('users')
            ->groupBy($threadsTable . '.id')
            ->select($threadsTable . '.*');
    }

    public function scopeLimitMessage($query, $limit)
    {
        return $query->with(['ads', 'users', 'messages' => function ($query) use ($limit) {
            return $query->take($limit + 1);
        }]);
    }

    public function getAuthor(){
        return $this->ads->author;
    }

    public function userParticipant(){
        $userId = AuthHelper::current_user()->id;
        return $this->participants->filter(function($user) use ($userId){
            return $user->user_id !=$userId;
        })->first()->user;
    }
}
