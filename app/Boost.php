<?php

namespace App;

use App\Helper\BoostHelper;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

/**
 * App\Boost
 *
 * @property int $id
 * @property string $name
 * @property int $price
 * @property int $days
 * @property string $type
 * @property string $description
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\BoostOptions[] $options
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Boost onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Boost whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Boost whereDays($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Boost whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Boost whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Boost whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Boost whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Boost wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Boost whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Boost whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Boost withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Boost withoutTrashed()
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AbstractModel orderByField($field, $values)
 */
class Boost extends AbstractModel
{

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'boosts';
    protected $fillable = [
        'id', 'name', 'price', 'days', 'type', 'description'
    ];

    public function options()
    {
        return $this->hasMany('App\BoostOptions', 'boost_id');
    }

    public function get_option($key,$single=false)
    {
        $query = DB::table('boost_options')
            ->where('key','=',$key)
            ->where('boost_id','=',$this->id);
        if($single){
            return $query->first()->value;
        }
        return $query->get();
    }

    public function get_package_html_id()
    {
        return BoostHelper::getBoostClass($this->id);
    }

    public function get_type_label_name()
    {
        switch ($this->type) {
            case 'package':
                return 'Gói Combo';
            default:
                return 'Gói tính năng';
        }
    }

    public function get_features()
    {
        return $this->get_option('features');
    }

    public function scopePackages($query){
        return $query->where('type','=','package');
    }
}
