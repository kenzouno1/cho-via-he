<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Roles
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Admins[] $admins
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Capability[] $capabilites
 * @method static \Illuminate\Database\Query\Builder|\App\Roles whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Roles whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Roles whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Roles whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Roles whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AbstractModel orderByField($field, $values)
 */
class Roles extends AbstractModel
{
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public static $protect = [1];
    protected $fillable = [
        'name', 'id'
    ];

    protected $table = 'roles';

    public function admins()
    {
        return $this->belongstoMany('App\Admins', 'role_relationship', 'admin_id', 'role_id');
    }

    public function capabilites()
    {
        return $this->hasMany('App\Capability', 'role_id');
    }

    public function caps()
    {
        $lst = $this->capabilites;
        $caps = [];
        if (!empty($lst)) {
            foreach ($lst as $key => $cap) {
                $caps[] = $cap->capability;
            }
        }
        return $caps;
    }

    public static function isProtect($id = null)
    {
        return in_array($id, self::$protect);
    }

}
