<?php

namespace App;

use App\Helper\UtilHelper;


/**
 * App\PackagePayment
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PackagePayment isActive()
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property float $price
 * @property float $money
 * @property int $status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PackagePayment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PackagePayment whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PackagePayment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PackagePayment whereMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PackagePayment whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PackagePayment wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PackagePayment whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PackagePayment whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @property-read mixed $money_format
 * @property-read mixed $price_format
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\PaymentLogs[] $payment
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AbstractModel orderByField($field, $values)
 */
class PackagePayment extends AbstractModel
{
    protected $table = 'payment_package';
    protected $fillable = [
        'name', 'id', 'price', 'money', 'status',
    ];

    public function payment(){
        return $this->hasMany('App\PaymentLogs', 'package_id');
    }
    public function scopeIsActive($query)
    {
        return $query->where('status', '=', true);
    }

    public function label_class()
    {
        return $this->status ? 'success' : 'danger';
    }

    public function getPriceFormatAttribute()
    {
        return UtilHelper::money($this->price);
    }

    public function getMoneyFormatAttribute()
    {
        return UtilHelper::money($this->money);
    }

    public function showMobile()
    {
        return $this->price <= 500000;
    }
}
