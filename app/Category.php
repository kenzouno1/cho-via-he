<?php

namespace App;

use App\Helper\CacheHelper;
use App\Helper\CategoryHelper;
use function asset;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use function dump;
use Illuminate\Support\Facades\Request;
use function is_object;
use function rand;
use function secure_asset;
use function var_dump;

/**
 * App\Category
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string|null $image
 * @property int $parent
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Ads[] $ads
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Category[] $childrenCate
 * @property-read \App\Category $parentCate
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category findSimilarSlugs(\Illuminate\Database\Eloquent\Model $model, $attribute, $config, $slug)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereParent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereSlug($value)
 * @mixin \Eloquent
 * @property string|null $thumbnail
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AbstractModel orderByField($field, $values)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereThumbnail($value)
 */
class Category extends AbstractModel
{
    use Sluggable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'slug', 'image', 'parent', 'id'
    ];
    public $timestamps = false;
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    protected $table = 'categories';

    public function parentCate()
    {
        return $this->belongsTo('App\Category', 'parent');
    }

    public function childrenCate()
    {
        return $this->hasMany('App\Category', 'parent');
    }

    public function ads()
    {
        return $this->hasMany('App\Ads', 'cate_id');
    }

    public function getThumbnailAttribute($value){
        return Request::secure() ? secure_asset($value) : asset($value);
    }

    public function getTemplate()
    {
        if ($this->isChildOfType('car')) {
            return 'car';
        } elseif ($this->isChildOfType('job')) {
            return 'job';
        } elseif ($this->isChildOfType('bds')) {
            return 'bds';
        } else {
            return 'basic';
        }
    }

    public function isChildOfType($type)
    {
        if ($type == 'car') {
            $lst = CategoryHelper::lstChildCar(true);
        } elseif ($type == 'bds') {
            $lst = CategoryHelper::lstChildLand(true);
        } elseif ($type == 'job') {
            $lst = CategoryHelper::lstChildJob(true);
        }

        return $lst->filter(function ($cate) {
            return $this->id == $cate->id;
        })->isNotEmpty();
    }

    public function hasChild()
    {
        return $this->getChildrent()->isNotEmpty();
    }

    public function isRoot()
    {
        return $this->parent == 0;
    }

    public function hasParent()
    {
        return $this->parent != 0;
    }

    public function cate_class($echo = false)
    {
        $className = array(
            'cate-' . $this->id,
            'cate-' . $this->slug,
        );
        if ($this->hasChild()) {
            $className[] = 'has-child';
        }
        if ($this->isRoot()) {
            $className[] = 'cate-root';
        } elseif ($this->hasParent()) {
            $className[] = 'child-of-' . $this->getParent()->slug;
        }
        if ($echo) {
            echo implode(' ', $className);
        } else {
            return implode(' ', $className);
        }
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name',
            ],
        ];
    }

    public function loadConfig()
    {
        switch ($this->getTemplate()) {
            case 'car' :
                return $this->loadConfigCar();
            case 'job':
                return $this->loadConfigJob();
            case 'bds':
                return $this->loadConfigBds();
            default :
                return [];
        }
    }

    protected function loadConfigCar()
    {
        $lstDongXe = [];
        $configHangXe = config("car.model");
        if (is_array($configHangXe) && isset($configHangXe[$this->slug])) {
            $lstDongXe = $configHangXe[$this->slug];
        }
        return compact('lstDongXe');
    }

    protected function loadConfigJob()
    {
        $lstMucLuong = $lstCongViec = [];

        $configMucLuong = config('job.mucluong');
        $configCongViec = config('job.congviec');
        if ($configMucLuong) {
            $lstMucLuong = $configMucLuong;
        }
        if ($configMucLuong) {
            $lstCongViec = $configCongViec;
        }
        return compact('lstMucLuong', 'lstCongViec');
    }

    protected function loadConfigBds()
    {
        $lstSoPhong = [];
        $configSoPhong = config('bds.sophong');
        if ($configSoPhong) {
            $lstSoPhong = $configSoPhong;
        }
        return compact('lstSoPhong');
    }

    public function lstParentCategories($withThis = true)
    {
        $cachedKey = $withThis ? 'nested_parent_with_' . $this->id : 'nested_parent_' . $this->id;
        return CacheHelper::load($cachedKey, function () use ($withThis) {
            $lst = collect();
            if ($withThis) {
                $lst->push($this);
            }
            if ($this->getParent()) {
                $lst->merge($this->getParent()->lstParentCategories(true));
            }
            return $lst;
        });
    }

    public function lstChildCategories($withThis = true, $lst = null, $collapse = true)
    {
        $cachedKey = $withThis ? 'nested_child_with_' . $this->id : 'nested_child_' . $this->id;
        return CacheHelper::load($cachedKey, function () use ($withThis, $lst, $collapse) {
            if (is_null($lst)) {
                $lst = collect();
            }
            if ($this->hasChild()) {
                $allChild = $this->getChildrent();
                $lst->push($allChild);
                foreach ($allChild as $child) {
                    if ($child->hasChild()) {
                        $lst = $child->lstChildCategories(false, $lst, false);
                    }
                }
            }
            if ($collapse) {
                $lst = $lst->collapse();
                if ($withThis == true) {
                    $lst->push($this);
                }
            }
            return $lst;
        });
    }

    public function countNested($withThis = true)
    {
        $cachedKey = 'count_' . $this->id;
        $time = Carbon::now()->addDays(1);
        return CacheHelper::load($cachedKey, function () use ($withThis) {
            return $this->countAllAds($withThis);
        }, $time);
    }

   public function countAllAds($withThis=true){
       $lstCate = $this->lstChildCategories($withThis)->pluck('id');
       $total = Ads::whereIn('cate_id', $lstCate)->isActive()->count();
       return $total < 100 ? rand(100,999) : $total;
    }

    public function getChildrent()
    {
        $cachedKey = 'child_' . $this->id;
        return CacheHelper::load($cachedKey, function () {
            return $this->childrenCate;
        });
    }

    public function getParent()
    {
        $cachedKey = 'parent_' . $this->id;
        return CacheHelper::load($cachedKey, function () {
            if (!$this->isRoot()) {
                return $this->parentCate;
            }
            return null;
        });
    }
}