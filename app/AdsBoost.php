<?php

namespace App;

use App\Helper\BoostHelper;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;


/**
 * App\AdsBoost
 *
 * @property int $id
 * @property string $type
 * @property int $ads_id
 * @property int $boost_id
 * @property float $price
 * @property string $features
 * @property int $renew
 * @property \Carbon\Carbon $expired_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @property-read \App\Ads $ads
 * @property-read \App\Boost $boost
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdsBoost whereAdsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdsBoost whereBoostId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdsBoost whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdsBoost whereExpiredAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdsBoost whereFeatures($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdsBoost whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdsBoost wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdsBoost whereRenew($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdsBoost whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdsBoost whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read mixed $expired_days
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdsBoost expired($days = 0)
 * @property-read mixed $is_expired
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AbstractModel orderByField($field, $values)
 */
class AdsBoost extends AbstractModel
{
    protected $fillable = [
        'type', 'ads_id', 'boost_id', 'expired_at',
    ];
    protected $table = 'ads_boost';
    protected $dates = ['expired_at'];

    public function getFeaturesAttribute($value)
    {
        return json_decode($value);
    }

    public function setFeaturesAttribute($value)
    {
        $this->attributes['features'] = json_encode($value);
    }

    public function boost()
    {
        return $this->belongsTo('App\Boost');
    }

    public function ads()
    {
        return $this->belongsTo('App\Ads');
    }

    public function boost_class()
    {
        $classes = [];
        if ($this->type == 'package') {
            $classes[] = BoostHelper::getBoostClass($this->boost_id);
        }
        return $classes;
    }

    public function getIsExpiredAttribute()
    {
        return Carbon::now()->lt($this->expired_at);
    }

    public function scopeExpired($query, $days = 0)
    {
        $raw = DB::raw('DATEDIFF(expired_at,NOW()) as expired_days');
        $query->whereRaw('DATEDIFF(expired_at,NOW()) > 0')
            ->havingRaw("SUM(DATEDIFF(expired_at,NOW())) > $days")
            ->addSelect($raw);
    }

    public static function getLstBoostPrice($lstBoost)
    {
        return $lstBoost->sum('price');
    }
}