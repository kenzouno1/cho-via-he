<?php

namespace App;

use App\Helper\AdsHelper;
use App\Helper\AuthHelper;
use App\Helper\CacheHelper;
use App\Helper\TimeHelper;
use App\Helper\UtilHelper;
use App\ModelTrait\AdsTrait;
use App\Query\SearchBuilder;
use Cviebrock\EloquentSluggable\Sluggable;
use EloquentFilter\Filterable;
use File;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Laravel\Scout\Searchable;
use function in_array;
use function is_array;
use function is_null;
use function is_numeric;
use function json_decode;
use function json_encode;
use function public_path;
use function str_limit;


/**
 * App\Ads
 *
 * @property int $id
 * @property string $title
 * @property string|null $slug
 * @property string|null $des
 * @property float|null $price
 * @property float|null $price_min
 * @property string $ads_type
 * @property string|null $price_type
 * @property string|null $state
 * @property string $status
 * @property string $fullname
 * @property string|null $email
 * @property string $address
 * @property float $lat
 * @property float $lng
 * @property int $view_count
 * @property string|null $images
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int $cate_id
 * @property int $user_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @property-read \App\User $author
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\AdsBoost[] $boost
 * @property-read \App\Category $category
 * @property-read mixed $ago
 * @property-read mixed $category_name
 * @property-read mixed $package_features
 * @property-read mixed $package_i_d
 * @property-read mixed $package_name
 * @property-read mixed $package_renew
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\AdsMeta[] $metas
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Chat\Thread[] $threads
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads nearest($from_latitude, $from_longitude, $distance = 10)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads filter($input = array(), $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads filterMeta($args)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads findSimilarSlugs(\Illuminate\Database\Eloquent\Model $model, $attribute, $config, $slug)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads fullTextSearch($search)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads isActive()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads isDeactive()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads showHome($limit = 6)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads topSearch($limit = 6)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads whereAdsType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads whereCateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads whereDes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads whereFullname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads whereImages($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads whereLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads whereLng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads wherePriceMin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads wherePriceType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads whereViewCount($value)
 * @mixin \Eloquent
 * @property \Carbon\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Ads onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Ads withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Ads withoutTrashed()
 * @property-read mixed $cache_category
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads filterHiddenPost()
 * @property-read mixed $excerpt
 * @property-read mixed $price_format
 * @property-read mixed $proposal_price
 * @property-read mixed $template
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads isHidden()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AbstractModel orderByField($field, $values)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ads withoutHidden()
 */
class Ads extends AbstractModel
{
    use Sluggable;
    use Filterable;
    use Searchable;
    use AdsTrait;
    use SoftDeletes;
    public $asYouType = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'des', 'price', 'price_min', 'ads_type', 'price_type', 'state', 'status',
        'fullname', 'email', 'address', 'slug', 'view_count', 'images',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'provider_id',
    ];
    protected $dates = ['deleted_at'];

    protected $appends = [];

    protected $table = 'ads';

    public function ads_class()
    {
        $classes = [
            'post',
            'post-' . $this->id,
            'template-' . $this->template
        ];
        if ($this->hasFeatures('highlight')) {
            $classes[] = 'has-highlight';
        }
        if ($this->hasFeatures('has-mockup')) {
            $classes[] = 'has-urgent';
        }
        if ($this->hasFeatures('top-search')) {
            $classes[] = 'in-top-search';
        }
        if ($this->hasFeatures('show-home')) {
            $classes[] = 'in-show-home';
        }
        if ($this->hasFeatures('has-gallery')) {
            $classes[] = 'has-gallery';
        }
        if ($this->isFavorites()) {
            $classes[] = 'favorited';
        }
        if ($this->isViewed()) {
            $classes[] = 'viewed';
        }
        foreach ($this->boost as $boost) {
            $classes = array_merge($classes, $boost->boost_class());
        }
        return implode(' ', $classes);
    }

    public function isFavorites()
    {
        return in_array($this->id, AdsHelper::getFavorites());
    }

    public function isViewed()
    {
        return in_array($this->id, AdsHelper::getViewed());
    }

    public function isAuthor()
    {
        $currentUser = AuthHelper::current_user();
        if ($currentUser && ($currentUser->id == $this->user_id)) {
            return true;
        }
        return false;
    }

    public function hasFeatures($features)
    {
        $lstBoost = $this->boost->filter(function ($boost) use ($features) {
            return (is_array($boost->features) && in_array($features, $boost->features));
        });
        if ($lstBoost->isNotEmpty()) {
            foreach ($lstBoost as $boost) {
                if ($boost->isExpired) {
                    return true;
                }
            }
        }
        return false;
    }

    public function getThumbnail($limit = 1, $showDefault = true)
    {
        $urls = [];
        $thumbnail = collect($this->images);
        $default = array(
            'small' => asset('assets/img/default-image/no-image.jpg'),
            'medium' => asset('assets/img/default-image/no-image.jpg'),
        );
        if ($thumbnail->isEmpty()) {
            $urls[] = $default;
        } else {
            $totalImages = $thumbnail->count();
            if ($totalImages == 1) {
                $image = $thumbnail->first();
                $urls[] = $this->getImageSrc($image, array('small', 'medium', 'vertical'));
            } elseif ($totalImages == 2) {
                $thumbnail->take(2)->each(function ($image) use (&$urls) {
                    $urls[] = $this->getImageSrc($image, array('small', 'medium', 'vertical'));
                });
            } else {
                $thumbnail->take($limit)->each(function ($image) use (&$urls) {
                    $urls[] = $this->getImageSrc($image, array('small', 'medium', 'vertical'));
                });
            }
        }
        return $urls;
    }

    public function getFolderUpload()
    {
        $year = $this->created_at->year;
        $month = $this->created_at->month;
        $folder = 'uploads/' . $year . '/' . $month . '/post-' . $this->id;
        return $folder;
    }

    public function getUrl()
    {
        return route('ads.details', ['slug' => $this->slug]);
    }

    public function getMap()
    {
        return [
            'center' => $this->getCenter(),
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->excerpt,
        ];
    }

    public function getCenter()
    {
        return $this->lat . ',' . $this->lng;
    }

    public function getCategoryNameAttribute()
    {
        return $this->cacheCategory->name;
    }

    public function getPackageNameAttribute()
    {
        return $this->getPackage()->name;
    }

    public function getPackageIDAttribute()
    {
        return $this->getPackage()->id;
    }

    public function getPackageRenewAttribute()
    {
        return $boost = $this->boost->filter(function ($boost) {
            return $boost->type == 'package';
        })->first()->renew;
    }

    public function getPackageFeaturesAttribute()
    {
        return $this->boost->filter(function ($boost) {
            return $boost->type == 'package';
        })->first()->features;
    }

    public function getMaxSalary()
    {
        return $this->metas->filter(function ($metas) {
            return $metas->meta_key == 'muc-luong-max';
        })->first();
    }

    public function getMinSalary()
    {
        return $this->metas->filter(function ($metas) {
            return $metas->meta_key == 'muc-luong-min';
        })->first();
    }

    public function getPriceFormatAttribute()
    {
        if ($this->cacheCategory->getTemplate() == 'job') {
            $max = $this->getMaxSalary();
            $min = $this->getMinSalary();
            if (!is_null($max) && !is_null($min) && $min->meta_value != $max->meta_value) {
                $minPrice = UtilHelper::money($min->meta_value);
                $maxPrice = $max->meta_value == 9999999999 ? 'hơn 100 triệu' : UtilHelper::money($max->meta_value);
                return $minPrice . ' - ' . $maxPrice;
            } elseif (!is_null($max) && !is_null($min) && $min->meta_value == $max->meta_value) {
                return UtilHelper::money($min->meta_value);
            }
        }
        if ($this->price_type == 'treat') {
            if (is_null($this->price) || $this->price == 0) {
                return 'thương lượng';
            }
            return UtilHelper::moneyNumber($this->price) . ' (thương lượng)';
        }
        if ($this->price_type == 'trade') {
            return 'Trao đổi';
        }
        return UtilHelper::money($this->price);
    }

    public function getMinPriceAttribute()
    {
        if (!is_null($this->price_min) || $this->price_min > 0) {
            return '<div class="price-min"> Thấp nhất ' . UtilHelper::moneyNumber($this->price_min) . '</div>';
        }
    }

    public function modelFilter()
    {
        return $this->provideFilter(\App\ModelFilters\AdsFilter::class);
    }

    public function toSearchableArray()
    {
        return [
            'title' => $this->title,
            'id' => $this->id,
        ];
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title',
            ],
        ];
    }

    public function scopeFilterMeta($query, $args)
    {
        return SearchBuilder::filterMeta($query, $args);
    }

    public function getExcerptAttribute()
    {
        return Str::words($this->des, 30, '...');

    }

    public function getDescriptionAttribute()
    {

        return nl2br(strip_tags($this->des, '<strong>'));

    }

    public function getAgoAttribute()
    {
        return TimeHelper::ago($this->created_at);
    }

    public function getPackage()
    {
        $boost = $this->boost()->whereType('package')->first();
        return $boost->boost;
    }

    public function getBoostFeatures($lstBoost)
    {
        if (is_string($lstBoost)) {
            $lstBoost = collect($lstBoost);
        }
        $lstIds = $lstBoost->pluck('id');
        $features = $this->boost->filter(function ($boost) use ($lstIds) {
            return $lstIds->contains($boost->boost_id);
        })->first();
        if ($features) {
            return $features->boost;
        }
        return $lstBoost->first();
    }

    public function isStatus($status)
    {
        return $this->status == $status;
    }

    public function favoriteClass()
    {
        return $this->isFavorites() ? 'active' : '';
    }

    public function getImagesAttribute($value)
    {
        if ($value != '' && !is_null($value)) {
            return json_decode($value, true);
        } else {
            return [];
        }
    }

    public function getImageSrc($image, $sizes)
    {
        $baseName = pathinfo($image, PATHINFO_FILENAME);
        if (is_array($sizes)) {
            $lst = [];
            foreach ($sizes as $size) {
                $lst[$size] = asset($this->getFolderUpload() . '/' . $baseName . '-' . $size . '.jpg');
            }
            return $lst;
        } else {
            return asset($this->getFolderUpload() . '/' . $baseName . '-' . $sizes . '.jpg');
        }
    }

    public function getImageName($image)
    {
        $name = File::basename($image);
        $patterns = '/(-medium|-small)/';
        return preg_replace($patterns, '', $name);
    }

    public function getImagePath($image, $sizes)
    {
        $baseName = pathinfo($image, PATHINFO_FILENAME);
        if (is_array($sizes)) {
            $lst = [];
            foreach ($sizes as $size) {
                $lst[$size] = public_path($this->getFolderUpload() . '/' . $baseName . '-' . $size . '.jpg');
            }
            return $lst;
        } else {
            return public_path($this->getFolderUpload() . '/' . $baseName . '-' . $sizes . '.jpg');
        }
    }

    public function getAllImageSrc($sizes)
    {
        $lst = [];
        foreach ($this->images as $image) {
            $lst[] = $this->getImageSrc($image, $sizes);
        }
        return $lst;
    }

    public function deleteImage($image)
    {
        $files = $this->getImagePath($image, ['small', 'medium']);
        foreach ($files as $file) {
            if (File::exists($file)) {
                File::delete($file);
            }
        }
    }

    public function toCommon()
    {
        $price = $this->price;
        if ($this->price_type == 'trade') {
            $price = 10000000000;
        }
        return json_encode([
            'id' => $this->id,
            'slug' => $this->slug,
            'price' => $price,
            'min_price' => $this->price_min,
        ]);
    }

    public function getCacheCategoryAttribute()
    {
        $cachedKey = 'category_of_' . $this->id;
        return CacheHelper::load($cachedKey, function () {
            return $this->category;
        });
    }

    public function getProposalPriceAttribute()
    {
        if (is_numeric($this->price_min) && $this->price_min > 0) {
            return $this->price_min;
        }
        return $this->price;
    }

    public function getTemplateAttribute()
    {
        return CacheHelper::load('ads_template-' . $this->id, function () {
            return $this->category->getTemplate();
        });
    }

    public function canProposalPrice()
    {
        return $this->price_type != 'trade';
    }

    public function hasInterestLoans()
    {
        if ($this->template == 'car') {
            return true;
        } else if ($this->template == 'bds' && $this->ads_type != 'rent') {
            return true;
        }
        return false;
    }

    public function isPublic()
    {
        return $this->status != 'deactive';
    }

    public function canView()
    {
        if (Auth::guard('admin')->check() || $this->isAuthor()) {
            return true;
        }
        return $this->status != 'deactive';
    }
}