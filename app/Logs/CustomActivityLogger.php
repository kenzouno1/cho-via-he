<?php
/**
 * Created by PhpStorm.
 * User: Dat Nguyen
 * Date: 8/21/2017
 * Time: 10:24 AM
 */

namespace App\Logs;


use App\Helper\AuthHelper;
use Illuminate\Auth\AuthManager;
use Illuminate\Contracts\Config\Repository;
use Spatie\Activitylog\ActivityLogger;

class CustomActivityLogger extends ActivityLogger
{
    public function __construct(AuthManager $auth, Repository $config)
    {
        parent::__construct($auth, $config);
        $this->causedBy = AuthHelper::current_user();
    }
}