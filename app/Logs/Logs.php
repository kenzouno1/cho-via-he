<?php

namespace App\Logs;

use App\Helper\AuthHelper;

/**
 * App\Logs
 *
 * @mixin \Eloquent
 */
class Logs
{
    public static function put($logname, $model, $des, $ip = null, $properties = array())
    {
        $user = AuthHelper::current_user();
        $properties['ip'] = $ip;
        activity($logname)->performedOn($model)
            ->causedBy($user)
            ->withProperties($properties)
            ->log($des);
    }

    public static function litle($logname, $msg, $ip = null, $properties = array())
    {
        $properties['ip'] = $ip;
        activity($logname)->withProperties($properties)->log($msg);
    }
}

