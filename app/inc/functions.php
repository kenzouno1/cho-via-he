<?php
function render_admin_menu($route)
{
    $routeName = $route->getName();
    $routePrefix = $route->getPrefix();
    $menus = config('nav');
    $user = Auth::guard('admin')->user();
    $capabilities = $user->getAllCap();

    foreach ($menus as $key => $nav):
        $class = $linkClass = $url = '';
        if (is_array($nav)):
            if (isset($nav['items'])) {
                $class .= 'start';
                $linkClass .= 'nav-toggle';
                $url = '#';
            } else {
                $url = route($key);
            }
            $current = false;
            if ($routeName == $key || $routePrefix === config('app.admin_prefix') . '/' . $key) {
                $class .= ' active';
                $current = true;
            }
            ?>
            <li class="nav-item <?php echo $class ?>">
                <a href="<?php echo $url ?>" class="nav-link <?php echo $linkClass ?>">
                    <i class="<?php echo $nav['icon'] ?>"></i>
                    <span class="title"><?php echo $nav['title'] ?></span>
                    <?php echo $current ? '<span class="selected"></span>' : '' ?>
                    <span class="arrow <?php echo $current ? 'open' : '' ?>"></span>
                </a>
                <?php if (isset($nav['items'])): ?>
                    <ul class="sub-menu">
                        <?php foreach ($nav['items'] as $link => $item):
                            // if (count(array_intersect($item['capability'],$capabilities))>0) :
                            $subClass = '';
                            if ($routeName == $link) {
                                $subClass .= 'active';
                            }
                            ?>
                            <li class='nav-item <?php echo $subClass ?>'>
                                <a href="<?php echo route($link) ?>" class="nav-link <?php echo $linkClass ?>"><span
                                            class="title"><?php echo $item['title'] ?></span></a>
                            </li>
                            <?php // endif;
                            ?>
                        <?php endforeach ?>
                    </ul>
                <?php endif; ?>
            </li>
        <?php else: ?>
            <li class="heading">
                <h3><?php echo $nav ?></h3>
            </li>
            <?php
        endif;
    endforeach;
}

function validate_form_class($name, $errors)
{
    if ($errors->has($name)) {
        echo isBackend() ? 'has-error' : 'has-danger';
    }
}

function isBackend()
{
    $prefix = Request::route()->getPrefix();
    if (is_null($prefix))
        return false;
    return str_contains($prefix, config('app.admin_prefix'));
}

function render_validate_msg($name, $errors, $always = false)
{
    if ($errors->has($name)) {
        if (isBackend()) {
            echo '<span class="help-block error">' . $errors->first($name) . '</span> ';
        } else {
            echo '<div class="form-control-feedback">' . $errors->first($name) . '</div>';
        }
    } elseif ($always && !isBackend()) {
        echo '<div class="form-control-feedback"></div>';
    }
}

function render_success_msg($errors, $input = 'success')
{
    if ($errors->has($input)) {
        echo '<div class="alert alert-success">' . $errors->first($input) . '</div>';
    }
}

function render_error_msg($errors)
{
    if ($errors->has('error')) {
        echo '<div class="alert alert-danger">' . $errors->first('error') . '</div>';
    }
}


function get_config_key($array, $value)
{
    return array_search($value, $array);
}

function render_bs_label($class, $label)
{
    return '<span class="label label-sm label-' . $class . '">' . $label . '</span>';
}

function checked_helper($type = 'checked', $default, $value, $echo = true)
{
    $flag = false;
    if (is_array($default)) {
        if (in_array($value, $default)) {
            $flag = true;
        }
    } else {
        if ((string)$default === (string)$value) {
            $flag = true;
        }
    }
    if ($echo) {
        if ($flag) {
            echo $type;
        }
    } else {
        return $flag;
    }
}

function checked($default, $value, $echo = true)
{
    return checked_helper('checked', $default, $value, $echo);
}

function selected($default, $value, $echo = true)
{
    return checked_helper('selected', $default, $value, $echo);
}

function get_avatar($isAdmin = false, $user = null)
{
    return \App\Helper\AuthHelper::getAvatar($isAdmin, $user);
}