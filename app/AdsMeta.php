<?php

namespace App;

use EloquentFilter\Filterable;

/**
 * App\AdsMeta
 *
 * @property int $id
 * @property string $meta_key
 * @property string $meta_value
 * @property int $ads_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Ads $ads
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdsMeta whereAdsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdsMeta whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdsMeta whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdsMeta whereMetaKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdsMeta whereMetaValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdsMeta whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdsMeta filter($input = array(), $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdsMeta hasRevenue()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdsMeta paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdsMeta simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdsMeta whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdsMeta whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdsMeta whereLike($column, $value, $boolean = 'and')
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AbstractModel orderByField($field, $values)
 */
class AdsMeta extends AbstractModel
{
    use Filterable;
    protected $table = 'ads_meta';
    protected $fillable = [
        'meta_key', 'meta_value',
    ];

    public function ads()
    {
        return $this->belongsTo('App\Ads');
    }

    public function scopeHasRevenue($query)
    {
        return $query->where('id', '>', 500);
    }

//    public function scopeAdsSold($query)
//    {
//        return $query->where('meta_key', '=', 'anbaidang');
//    }
}