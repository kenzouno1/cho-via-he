<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class UserAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,...$params)
    {
        if (!Auth::check()) {
            return redirect(route('auth.login').'?callback='.$request->fullUrl());
        }
        $user = Auth::user();
        if ($user->status === 'wait') {
            return redirect(route('auth.active'));
        }
        if ($user->status === config('conf.user.status.banned')) {
//            return redirect(route('auth.active'));
        }
        return $next($request);
    }
}
