<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cookie;

class CheckForMaintenanceMode
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        $lst = [];
//        foreach(range(1,255) as $item){
//            $lst[] = '113.160.170.'.$item;
//        }
//        if (env('APP_ENV')== 'production' && !App::isDownForMaintenance() &&
//            !in_array($request->ip(),$lst))
//        {
//            return response('Trang web hiện đang bảo trì vui lòng trở lại sau <3', 503);
//        }
        if (env('APP_ENV')== 'production' &&  !Cookie::has('isDemo')){
            return \Response::make(view('maintain'),200);
        }
        return $next($request);
    }
}
