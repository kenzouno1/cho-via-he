<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,...$params){
        if (!Auth::guard('admin')->check()) {
            return redirect(route('admin.login'));
        }
       $user = Auth::guard('admin')->user();
        // if (count(array_intersect($user->getAllCap(), $params)) == count($params)) {
            return $next($request);
        // }else{
             // return redirect(route('admin.dashboard'));
        // }
    }
}
