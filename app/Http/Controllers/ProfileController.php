<?php

namespace App\Http\Controllers;

use App\Ads;
use App\Helper\AdsHelper;
use App\Request\SmsRequest;
use App\User;
use function compact;
use Illuminate\Http\Request;

class ProfileController extends Controller
{

    public function viewProfileSelling($id)
    {
        $member = User::find($id);
        if (!$member){
            abort(404);
        }
        $lstAds = $member->ads()->isActive()->paginate(6);
        $totalAdsActive = $lstAds->total();
        $totalAdsSold = $member->ads()->isHidden()->count();
        return view('pages.userprofile.profile', compact('member', 'lstAds', 'totalAdsActive', 'totalAdsSold'));
    }

    public function viewProfileSold($id)
    {
        $member = User::find($id);
        if (!$member){
            abort(404);
        }
        $lstAds = $member->ads()->isHidden()->paginate(6);
        $totalAdsSold = $lstAds->total();
        $totalAdsActive = $member->ads()->isActive()->count();
        $active = 'sold';
        return view('pages.userprofile.profile', compact('member', 'lstAds', 'totalAdsActive', 'totalAdsSold','active'));
    }
}
