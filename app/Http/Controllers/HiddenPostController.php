<?php

namespace App\Http\Controllers;

use App\Ads;
use App\AdsMeta;
use Illuminate\Http\Request;

class HiddenPostController extends Controller
{
    public function hiddenPost(Request $request)
    {
        $radioStacked = $request->filled('radio-stacked') ? $request->get('radio-stacked') : 'hidden';
        $ads = Ads::find($request->get('post-id'));
        if ($ads) {
            $ads->hidden();
            $ads->save();
            $this->addMetaHidden($ads, $radioStacked);
        }
    }

    public function addMetaHidden($ads, $radioStacked)
    {
        $meta = new AdsMeta();
        $meta->meta_key = 'anbaidang';
        $meta->meta_value = $radioStacked;
        $meta->ads_id = $ads->id;
        $meta->save();
    }
}
