<?php

namespace App\Http\Controllers\Payment;

use App\Helper\AuthHelper;
use App\Http\Controllers\Controller;
use App\PackagePayment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Yajra\DataTables\Facades\DataTables;
use function abort;
use function array_merge;

class PaymentPackageController extends Controller
{
    public function getViewList()
    {
        return view('backend.pages.payment.package.list');
    }

    public function getViewListAjax()
    {
        $user = AuthHelper::current_user();
        $query = PackagePayment::query();
        return DataTables::of($query)
            ->addColumn('action', function ($package) {
                $html = '<a href="' . route('admin.payment.package.edit', ['id' => $package->id]) . '" class="btn btn-xs green">Sửa</a>';
                $html .= '<a href="#" class="btn btn-xs red btn-delete" data-name="' . $package->name . '"  data-id="' . $package->id . '" >Xóa</a>';
                return $html;
            })
            ->editColumn('money', function ($package) {
                return $package->money . ' Đ';
            })
            ->editColumn('price', function ($package) {
                return $package->price . ' Đ';
            })
            ->editColumn('status', function ($package) {
                $txt = $package->status ? 'Đang kích hoạt' : 'Tạm khóa';
                return '<span class="label label-sm label-' . $package->label_class() . '">' . $txt . '</span>';
            })
            ->rawColumns(['action', 'status'])
            ->make(true);
    }

    public function getViewNew()
    {
        return view('backend.pages.payment.package.new');
    }

    public function newPackage(Request $request)
    {
        $data = $request->all();
        $validate = $this->validator($data);
        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate)->withInput();
        }
        $name = $request->name;
        $package = new PackagePayment();
        $package->name = $request->name;
        $package->price = $request->price;
        $package->money = $request->money;
        $package->status = $request->filled('status');
        $package->save();
        $msg = new MessageBag(['success' => 'Đã tạo thành công gói ' . $name]);
        return redirect()->back()->withInput()->withErrors($msg);
    }

    public function validator($data, $rules = [])
    {
        $rules = array_merge(array(
            'name'  => 'required',
            'price' => 'required|integer',
            'money' => 'required|integer',
        ), $rules);
        $messages = array(
            'name.required'  => 'Vui lòng nhập tên gói nạp',
            'price.required' => 'Vui lòng nhập số tiền',
            'price.integer'  => 'Số tiền nạp phải là số',
            'money.required' => 'Vui lòng nhập số tiền sẽ nhận được',
            'money.integer'  => 'Số tiền nhận được phải là số',
        );
        return Validator::make($data, $rules, $messages);
    }

    public function getViewEdit($id)
    {
        $package = PackagePayment::whereId($id)->firstOrFail();
        if ($package) {
            return view('backend.pages.payment.package.edit', compact('package'));
        }
        abort(404);
    }

    public function editPackage(Request $request, $id)
    {
        $data = $request->all();
        $validate = $this->validator($data);
        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate)->withInput();
        }
        $package = PackagePayment::whereId($id)->firstOrFail();
        if ($package) {
            $name = $request->name;
            $package->name = $request->name;
            $package->price = $request->price;
            $package->money = $request->money;
            $package->status = $request->filled('status');
            $package->save();
            $msg = new MessageBag(['success' => 'Đã cập nhật thành công gói ' . $name]);
            return redirect()->back()->withInput()->withErrors($msg);
        }
        abort(404);
    }

    public function deletePackage(Request $request)
    {
        PackagePayment::destroy($request->id);
    }
}
