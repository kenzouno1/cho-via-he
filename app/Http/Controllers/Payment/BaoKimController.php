<?php

namespace App\Http\Controllers\Payment;

use App\Helper\AuthHelper;
use App\Http\Controllers\Controller;
use App\PackagePayment;
use App\Payment\BaoKim\Card\BaoKimCard;
use App\Payment\BaoKim\Pro\BaoKimPro;
use App\PaymentLogs;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Validator;
use function abort;
use function compact;
use function in_array;
use function redirect;

class BaoKimController extends Controller
{
    public function getViewPaymentStep1()
    {
        $step = 1;
        $user = AuthHelper::current_user();
        $lstPackage = PackagePayment::isActive()->orderBy('money')->get();
        return view('pages.payment.payment', compact('step', 'user', 'lstPackage'));
    }

    public function getViewPaymentStep2($package)
    {
        $package = PackagePayment::isActive()->whereId($package)->firstOrFail();

        if ($package) {
            $baokimPro = new BaoKimPro();
            $lstBanks = $baokimPro->getProfile();
            $internetBankingImg = $baokimPro->getBankInternetBankingImages($lstBanks);
            $localCardImg = $baokimPro->getBankCardImages($lstBanks);
            $creditCardImg = $baokimPro->getCreditCardImages($lstBanks);
            $user = AuthHelper::current_user();
            $step = 2;
            return view('pages.payment.payment', compact('step', 'user', 'package', 'lstBanks', 'internetBankingImg',
                'localCardImg', 'creditCardImg'));
        }
        abort(404);
    }

    public function paymentStep2Process(Request $request, $package)
    {
        $package = PackagePayment::isActive()->whereId($package)->firstOrFail();
        if ($package && $request->filled('bank_payment_method_id')) {
            $payMethodId = $request->get('bank_payment_method_id');

            $user = AuthHelper::current_user();
            $data = $request->all();
            $data['total_amount'] = $package->price;
            if (in_array($payMethodId, ['Viettel', 'Mobifone', 'Vinaphone'])) {
                $checkoutUrl = $this->payByMobileCard($package->id, $payMethodId);
            } else {
                $checkoutUrl = $this->payByPro($data, $package, $user);
            }
            return redirect($checkoutUrl);
        }
        abort(404);
    }

    protected function payByPro($data, $package, $user)
    {
        $baokim = new BaoKimPro();
        $result = $baokim->pay_by_card($data, $package, $user);
        if (!empty($result['error'])) {
            echo '<p><strong style="color:red">' . $result['error'] . '</strong></p></div>';
            die;
        }
        $checkoutUrl = isset($result['redirect_url']) ? $result['redirect_url'] : $result['guide_url'];
        return $checkoutUrl;
    }

    protected function payByMobileCard($package, $mang)
    {
        return route('payment.mobile', compact('package', 'mang'));
    }

    public function viewPaymentMobileCard($package, $mang)
    {
        $step = 'cards';
        return view('pages.payment.payment', compact('package', 'mang', 'step'));
    }

    public function paymentMobileCardProcess(Request $request, $package, $mang)
    {
        $validate = $this->validateCard($request->all());
        if ($validate->fails()) {
            return redirect()->back()->withInput()->withErrors($validate);
        }
        $pin = $request->get('pin');
        $seri = $request->get('seri');
        $baoKimCard = BaoKimCard::pay($mang, $pin, $seri);
        $result = json_decode($baoKimCard['data'], true);
        $user = AuthHelper::current_user();
        $paymentLogs = new PaymentLogs();
        $paymentLogs->description = $pin . '|' . $seri;
        $paymentLogs->user_id = $user->id;
        $paymentLogs->package_id = $package;
        $paymentLogs->type = 'card';
        $paymentLogs->name = $mang;
        if ($baoKimCard['status'] == 200) {
            $user->logName = 'log.payment';
            $amount = intval($result['amount']);
            $paymentLogs->status = 'success';
            $paymentLogs->money = $amount;
            $paymentLogs->save();
            $user->updateMoney($amount);
            $user->save();
        } else {
            $paymentLogs->package_id = $package;
            $paymentLogs->status = 'fail';
            $paymentLogs->money = 0;
            $paymentLogs->save();
            $msg = new MessageBag(['msg' => $result['errorMessage']]);
            return redirect()->back()->withErrors($msg);
        }

    }

    public function cancel($package)
    {
        $package = PackagePayment::find($package);
        if (!$package)
            abort(404);
        $user = AuthHelper::current_user();
        $step = '4-payment-fail';
        return view('pages.payment.payment', compact('step', 'package', 'user'));
    }

    public function paymentSuccess()
    {
        $step = '4-payment-successfull';
        $user = AuthHelper::current_user();
        $paymentLogs = PaymentLogs::whereUserId($user->id)->firstOrFail();
        $package = $paymentLogs->package;
        return view('pages.payment.payment', compact('step','user','paymentLogs','package'));
    }

    public function paymentSuccessCallback(Request $request, $package)
    {
        $package = PackagePayment::find($package);
        if ( !$package){
            abort(404);
        }
        //cancel
        if (!$request->filled('transaction_id')) {
            return redirect()->route('payment.cancel', compact('package'));
        }
        $user = AuthHelper::current_user();
        $paymentLogs = new PaymentLogs();
        $paymentLogs->name = 'Thanh toán gói ' . $package;
        $paymentLogs->transaction_id = $request->get('transaction_id');
        $paymentLogs->status = $request->get('transaction_status');
        $paymentLogs->type = $request->get('payment_type');
        $paymentLogs->money = $package->money;
        $paymentLogs->user_id = $user->id;
        $paymentLogs->package_id = $package;
        $paymentLogs->save();
        $user->updateMoney($package->money);
        $user->save();
        return redirect()->route('payment.success');
    }

    protected function validateCard($data)
    {
        $rules = [
            'seri' => 'required',
            'pin'  => 'required',
        ];
        $message = [
            'pin.required'  => 'Vui lòng nhập mã thẻ',
            'seri.required' => 'Vui lòng số seri thẻ',
        ];
        return Validator::make($data, $rules, $message);
    }
}