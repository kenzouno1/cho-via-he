<?php

namespace App\Http\Controllers\Pages;

use App\Ads;
use App\Helper\AdsHelper;
use App\Helper\CategoryHelper;
use App\Http\Controllers\Controller;
use function dump;

class HomeController extends Controller
{
    public function getViewHome()
    {
        $title = 'Chợ Vỉa Hè | Trăm người bán - Vạn Người mua';
        $lstCateChildOfCar = CategoryHelper::lstChildCar(false);
        $lstCateChildOfJob = CategoryHelper::lstChildJob(false);
        $lstCateChildOfLand = CategoryHelper::lstChildLand(false);
        $lstCarIds = $lstCateChildOfCar->pluck('id')->toArray();
        $lstJobIds = $lstCateChildOfJob->pluck('id')->toArray();
        $lstLandIds = $lstCateChildOfLand->pluck('id')->toArray();
        $lstLands = $this->getAds($lstLandIds);
        $lstJobs = $this->getAds($lstJobIds);
        $lstCars = $this->getAds($lstCarIds);
        $lstNews = $this->getAds(null, false);
        $lstFavorites = AdsHelper::getAdsFavorites();
        $lstViewed = AdsHelper::getAdsViewed();
        $lstViewedCate = CategoryHelper::getAllCateByAds($lstViewed);
        $lstFavoritesCate = CategoryHelper::getAllCateByAds($lstFavorites);
        $lstNewestCate = CategoryHelper::getAllCateByAds($lstNews);
        $lstSavedSearch = AdsHelper::getSavedSearch(12);
        return view('pages.home.home', compact('ads', 'lstCars', 'lstLands', 'lstJobs', 'lstNews',
            'lstCateChildOfCar', 'lstCateChildOfJob', 'lstCateChildOfLand', 'lstFavorites', 'lstViewed', 'title',
            'lstViewedCate', 'lstFavoritesCate', 'lstNewestCate', 'lstSavedSearch'));
    }

    protected function getAds($cateIds = null, $random = true, $limit = 6, $showHome = true,$next=true)
    {
        $query = Ads::limit($limit)
            ->isActive()
            ->with('author', 'boost', 'category');
        if ($random) {
            $query->inRandomOrder();
        } else {
            $query->orderByDesc('id');
        }
        if ($showHome) {
            $query->showHome();
        }
        if (is_array($cateIds)) {
            $query->whereIn('cate_id', $cateIds);
        }

        $lst = $query->get();
        if ($lst->count() < $limit && $next) {
            $nextLimit = $limit - $lst->count();
           return $lst->merge($this->getAds($cateIds, $random, $nextLimit, false,false));
        }
        return $lst;
    }
}
