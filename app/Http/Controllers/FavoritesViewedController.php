<?php

namespace App\Http\Controllers;

use App\Category;
use App\Helper\AdsHelper;
use Illuminate\Http\Request;
use function abort;
use function compact;
use function redirect;
use function response;

class FavoritesViewedController extends Controller
{
    public function addToFavorite(Request $request)
    {
        if ($request->ajax() && $request->filled('id')) {
            $id = $request->get('id');
            AdsHelper::updateFavorites($id);
            $msg = ['msg' => 'OK', 'status' => '200'];
        } else {
            $msg = ['msg' => 'Id not found', 'status' => '404'];
        }
        return response()->json($msg);
    }

    public function getFavoritesHtml(Request $request)
    {
        if (!$request->ajax()) {
            abort(404);
        }
        $view = ($request->filled('style') && $request->get('style') == 'card') ? 'template-part.home-favorited' : 'template-part.user-favorited';
        $lstFavorites = AdsHelper::getAdsFavorites(6);
        $html = '';
        $html .= view($view, compact('lstFavorites'));
        return response()->json(['html' => $html, 'status' => '200']);
    }

    public function viewFavorited()
    {
        $lstFavorites = AdsHelper::getAdsFavorites(12);
        return view('pages.myprofile.manage-favorites', compact('lstFavorites'));
    }

    public function viewViewed()
    {
        $lstViewed = AdsHelper::getAdsViewed(12);
        return view('pages.myprofile.manage-watched', compact('lstViewed'));
    }

    public function addToSavedSearch(Request $request)
    {
        if (!$request->ajax()) {
            abort(404);
        }
        $keyword = $request->filled('title') ? $request->get('title') : '';
        $address = $request->filled('address') ? $request->get('address') : '';
        $location = $request->filled('location') ? $request->get('location') : '';
        $distance = $request->filled('distance') ? $request->get('distance') : '';
        $style = $request->filled('style') ? $request->get('style') : '';
        $cate = [
            'name' => 'Tất Cả',
            'slug' => '',
            'image' => 'ion-android-menu',
        ];
        if ($request->filled('cate')) {
            $cateSlug = $request->get('cate');
            $cate = Category::whereSlug($cateSlug)->first()->toArray();
        }
        $lstSavedSearch = AdsHelper::updateSavedSearch($cate, $keyword, $location, $distance, $address);
        $view = $style == 'home' ? 'home-saved-search' : 'user-saved-search';
        $html = '';
        $html .= view('template-part.' . $view, compact('lstSavedSearch'));
        return response()->json(['html' => $html, 'status' => '200']);
    }

    public function viewSavedSearches()
    {
        $lstSavedSearch = AdsHelper::getSavedSearch(12);
        return view('pages.myprofile.saved-searches', compact('lstSavedSearch'));
    }

    public function removeItemSavedSearch($index)
    {
        AdsHelper::removeSavedSeach($index);
        return redirect()->route('member.saved-searches');
    }

    public function removeAllSavedSearch()
    {
        AdsHelper::removeAllSavedSeach();
        return redirect()->route('member.saved-searches');
    }
}