<?php

namespace App\Http\Controllers;

use App\Ads;
use App\Helper\UrlHelper;
use Illuminate\Http\Request;

class FormSearchController extends Controller
{
    //search auto complete
    public function getTitle(Request $request)
    {
        $search = $request->get('term');
        $lstAds = Ads::search($search)->take(10)->get();
        $result = $lstAds->pluck('title')->toArray();
        return \Response::json($result);
    }

    public function submit(Request $request)
    {
        $cate = $request->filled('cate') ? $request->get('cate') : '';
        $args = [
            'location' => $request->get('location'),
            'distance' => $request->get('distance'),
            'address' => $request->get('address'),
        ];
        if ($request->filled('title')) {
            $args['tu-khoa'] = $request->get('title');
        }
        return redirect(UrlHelper::getUrl($args, route('search.list', ['cate' => $cate])));
    }
}
