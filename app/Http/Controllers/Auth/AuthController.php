<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Logs\Logs;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Socialite;

class AuthController extends Controller
{
    function __construct()
    {
        $this->middleware('guest');
    }

    public function redirectToProvider($provider, Request $request)
    {
        return Socialite::driver($provider)->redirect();
    }
    /**
     * Obtain the user information from provider.  Check if the user already exists in our
     * database by looking up their provider_id in the database.
     * If the user exists, log them in. Otherwise, create a new user then log them in. After that
     * redirect them to the authenticated users homepage.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        if ($provider == 'google') {
            $user = Socialite::driver('google')->stateless()->user();
        } else {
            $user = Socialite::driver($provider)->user();
        }
        $authUser = $this->findOrCreateUser($user, $provider);
        Auth::login($authUser, true);
        if ($authUser->phone == '' || is_null($authUser->phone)) {
            return redirect()->route('auth.submit-phone');
        }
        if ($authUser->status != 'active') {
            return redirect()->route('auth.active');
        }
        if ($authUser->isBanned()) {
            return 'đã khóa';
        }
        return redirect()->intended('/');
    }

    /**
     * If a user has registered before using social auth, return the user
     * else, create a new user object.
     * @param  $user Socialite user object
     * @param $provider Social auth provider
     * @return  User
     */
    public function findOrCreateUser($user, $provider)
    {
        $authUser = User::where('provider_id', '=', $user->id)->first();
        if ($authUser) {
            Logs::litle('user.login', 'Tài khoản ' . $user->email . ' đăng nhập thông qua ' . $provider);
            return $authUser;
        }
        $newUser = new User();
        $newUser->name = $user->name;
        $newUser->password = Hash::make(str_random(8));
        $newUser->email = $user->getEmail();
        $newUser->provider = $provider;
        $newUser->provider_id = $user->getId();
        $newUser->avatar = $user->getAvatar();
        $newUser->save();
		Logs::litle('user.login', 'Tài khoản ' . $user->email . ' đăng ký thông qua ' . $provider);
		return $newUser;
	}
}
