<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Logs\Logs;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;

class AdminController extends Controller
{
    use  ThrottlesLogins;

    public function doGet()
    {
        return view('backend.login');
    }

    protected function validator($data)
    {
        $rules = array(
            'pwd'      => 'required|min:6|max:40',
            'username' => 'required|min:4|max:100',
        );
        $messages = array(
            'username.required' => 'Vui lòng nhập Tài khoản',
            'username.min'      => 'Tên tài khoản không hợp lệ',
            'username.max'      => 'Tên tài khoản không hợp lệ',
            'pwd.required'      => 'Vui lòng nhập mật khẩu',
            'pwd.min'           => 'Mật khẩu phải dài hơn 6 kí tự',
            'pwd.max'           => 'Mật khẩu phải nhỏ hơn 40 kí tự',
        );
        return Validator::make($data, $rules, $messages);
    }

    public function doLogin(Request $request)
    {
        $data = $request->all();
        $username = $request->input('username');
        $password = $request->input('pwd');
        $remember = $request->input('remember') ? true : false;
        $validate = $this->validator($data);
        $ip = $request->ip();
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            Logs::litle('lock', 'Khóa tài khoản ' . $username . ' do đăng nhập sai quá nhiều lần', $ip);
            $errors = new MessageBag(['errorlogin' => 'Tài khoản của bạn bị tạm khóa do đăng nhập sai quá số lần quy định']);
            return redirect()->back()->withInput()->withErrors($errors);
        }
        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate)->withInput();
        }
        if (Auth::guard('admin')->attempt(['username' => $username, 'password' => $password], $remember)) {
            $this->clearLoginAttempts($request);
            Logs::litle('admin.login', $username . ' đăng nhập thành công', $request);
            return redirect()->intended(route('admin.dashboard'));
        } else {
            Logs::litle('admin.login', $username . ' đăng nhập thất bại', $request->ip());
            $this->incrementLoginAttempts($request);
            $errors = new MessageBag(['errorlogin' => 'Tài khoản hoặc mật khẩu không đúng']);
            return redirect()->back()->withInput()->withErrors($errors);
        }
    }

    public function logout()
    {
        if (Auth::guard('admin')->check()) {
            Auth::guard('admin')->logout();
            Session::flush();
        }
        return redirect()->route('admin.login');
    }

    protected function hasTooManyLoginAttempts(Request $request)
    {
        $lockoutTime = config('conf.login.admin.time');    //lockout after 10 seconds (setting is in minutes hence devision by 60 for setting the time in seconds)
        $maxLoginAttempts = config('conf.login.admin.limit');    //lockout after 5 attempts
        return $this->limiter()->tooManyAttempts(
            $this->throttleKey($request), $maxLoginAttempts, $lockoutTime
        );
    }

    public function username()
    {
        return 'username';
    }
}
