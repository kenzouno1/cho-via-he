<?php

namespace App\Http\Controllers\Auth;

use App\Helper\AuthHelper;
use App\Http\Controllers\Controller;
use App\Logs\Logs;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;

class LoginController extends Controller{

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use ThrottlesLogins;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function doGet(Request $request){
        $callbackUrl = $request->filled('callback') ? $request->callback : '';
        return view('auth.login',compact('callbackUrl'));
    }

    protected function validator( $data) {
        $rules = array(
            'phone' => 'phone:VN|required',
            'pwd' => 'required|min:6|max:40',
        );
        $messages = array(
            'phone.required' => 'Vui lòng nhập số điện thoại',
            'phone.phone' => 'Số điện thoại không đúng',
            'pwd.required'=>'Vui lòng nhập mật khẩu',
            'pwd.min'=>'Mật khẩu phải dài hơn 6 kí tự',
            'pwd.max'=>'Mật khẩu phải nhỏ hơn 40 kí tự',
        );
        return Validator::make($data, $rules,$messages);
    }

    public function doLogin(Request $request){
        $data = $request->all();
        $phone = $request->input('phone');
        $password = $request->input('pwd');
        $remember = $request->filled('remember')? true : false;
        $validate = $this->validator($data);
        $ip = $request->ip();
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            Logs::litle('lock','Khóa tài khoản '.$username.' do đăng nhập sai quá nhiều lần',$ip);
            $errors = new MessageBag(['errorlogin' => 'Tài khoản của bạn bị tạm khóa do đăng nhập sai quá số lần quy định']);
            return redirect()->back()->withInput()->withErrors($errors);
        }
        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate)->withInput();
        }
         if( Auth::attempt(['phone' => $phone, 'password' =>$password],$remember)) {
             Logs::litle('user.login',$phone.' Đăng nhập thành công',$ip);
             $this->clearLoginAttempts($request);
             $user = AuthHelper::current_user();
             if ($user->status != 'active'){
                 return redirect()->route('auth.active');
             }
             if ($user->isBanned()){
                 return 'đã khóa';
             }
             $callbackUrl = $request->filled('_callback') ? $request->get('_callback') : '/';
             return redirect()->intended($callbackUrl);
         } else {
             $this->incrementLoginAttempts($request);
            Logs::litle('user.login',$phone.' Đăng nhập thất bại',$ip);
             $errors = new MessageBag(['error' => 'Số điện thoại hoặc mật khẩu không đúng']);
             return redirect()->back()->withInput()->withErrors($errors);
         }
    }

    protected function hasTooManyLoginAttempts(Request $request)
    {
        $lockoutTime = config('conf.login.user.time');
        $maxLoginAttempts = config('conf.login.user.limit');    //lockout after 5 attempts
        return $this->limiter()->tooManyAttempts(
            $this->throttleKey($request), $maxLoginAttempts, $lockoutTime
        );
    }

     public function logout(){
         Auth::logout();
         Session::flush();
         return redirect()->route('auth.login');
     }
    public function username()
    {
        return 'username';
    }
}