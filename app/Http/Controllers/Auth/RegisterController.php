<?php

namespace App\Http\Controllers\Auth;

use App\Helper\UtilHelper;
use App\Http\Controllers\Controller;
use App\HttpRequest\SmsRequest;
use App\Logs\Logs;
use App\Sms;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;

class RegisterController extends Controller
{

    /*
            |--------------------------------------------------------------------------
            | Register Controller
            |--------------------------------------------------------------------------
            |
            | This controller handles the registration of new users as well as their
            | validation and creation. By default this controller uses a trait to
            | provide this functionality without requiring any additional code.
            |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $rules = array(
            'phone' => 'phone:VN|unique:users,phone|required',
            'pwd' => 'required|min:6|max:40',
        );
        $messages = array(
            'phone.required' => 'Vui lòng nhập số điện thoại',
            'phone.unique' => 'Số điện thoại đã được đăng ký',
            'phone.phone' => 'Số điện thoại không đúng',
            'phone.min' => 'Số điện thoại không hợp lệ',
            'phone.max' => 'Số điện thoại không hợp lệ',
            'pwd.required' => 'Vui lòng nhập mật khẩu',
            'pwd.min' => 'Mật khẩu phải dài hơn 6 kí tự',
            'pwd.max' => 'Mật khẩu phải nhỏ hơn 40 kí tự',
        );
        return Validator::make($data, $rules, $messages);
    }

    public function doGet()
    {
        return view('auth.register');
    }

    public function doRegister(Request $request)
    {
        $data = $request->all();
        $data['phone'] = UtilHelper::phoneBuilder($request->input('phone'));
        $request->replace($data);
        $validate = $this->validator($data);
        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate);
        }
        $user = new User();
        $user->phone = $data['phone'];
        $user->password = bcrypt($data['pwd']);
        $user->status = config('conf.user.status.default');
        $user->provider_id = 'form-' . time();
        $user->save();
        $password = $request->input('pwd');
        $sms = new Sms();
        $sms->user_id = $user->id;
        $sms->type = 'active';
        $sms->save();
        SmsRequest::sendActiveCode($data['phone'], $sms->code);
        $ip = $request->ip();
        if (Auth::attempt(['phone' => $data['phone'], 'password' => $password])) {
            Logs::litle('user.register', 'Tài khoản ' . $data['phone'] . ' đăng ký thành công', $ip);
            return redirect()->route('auth.active');
        } else {
            Logs::litle('user.register', 'Tài khoản ' . $data['phone'] . ' đăng ký thất bại', $ip);
            $errors = new MessageBag(['error' => 'Xảy ra lỗi trong quá trình đăng ký.Vui lòng thử lại']);
            return redirect()->back()->withInput()->withErrors($errors);
        }
    }

}