<?php

namespace App\Http\Controllers\Auth;

use App\Helper\AuthHelper;
use App\Helper\SmsHelper;
use App\Helper\StringHelper;
use App\Http\Controllers\Controller;
use App\Logs\Logs;
use App\Sms;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use function abort;

class ActiveController extends Controller
{
    public function getViewActive($phone = null, Request $request)
    {
        if (is_null($phone)) {
            $user = AuthHelper::current_user();
            if (!$user || $user->isActive()) {
                abort(404);
            }
        } else {
            if (!SmsHelper::hasCode($phone, 'active')) {
                abort(404);
            }
        }
        $callbackUrl = $request->filled('callback') ? $request->get('callback') : '';
        $action = $request->filled('quen-mat-khau') ? $request->get('quen-mat-khau') : '';
        return view('auth.active', compact('callbackUrl', 'phone','action'));
    }

    public function activeAccountProcess(Request $request, $phone = null)
    {
        $data = $request->all();
        if (StringHelper::detectBadWord($data['name'])) {
            $errors = new MessageBag(['name' => 'Tên của bạn chứa các từ bị cấm.']);
            return redirect()->back()->withErrors($errors);
        }
        $validate = $this->validator($data);
        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate);
        }

        $code = $request->get('code');
        if (is_null($phone)) {
            $member = AuthHelper::current_user();
            $phone = $member->phone;
        } else {
            $member = DB::table('users')
                ->where('phone', '=', $phone)
                ->first();
        }
        if (SmsHelper::validateCode($phone, $code, 'active')) {
            DB::table('users')
                ->where('phone', '=', $phone)
                ->update([
                    'status' => 'active',
                    'name'   => $request->input('name')
                ]);
            Logs::litle('user.active', $member->name . 'Kích hoạt thành công tài khoản có số điện thoại' . $member->phone);
            $callbackUrl = $request->filled('_callback') ? $request->get('_callback') : '/';
            if ($request->has('action') && $request->get('action')=='quen-mat-khau'){
                $sms = new Sms();
                $sms->type = 'forgot';
                $sms->user_id = $member->id;
                $sms->save();
                SmsRequest::sendForgotPwd($phone, $sms->code);
            }
            return redirect()->intended($callbackUrl);
        } else {
            $errors = new MessageBag(['code' => 'Mã kích hoạt không chính xác.']);
            Logs::litle('user.active', $member->phone . ' kích hoạt không thành công.Mã kích hoạt ' . $code);
            return redirect()->back()->withInput()->withErrors($errors);
        }
    }

    protected function validator(array $data)
    {
        $rules = array(
            'code' => 'required',
            'name' => 'required',
        );
        $messages = array(
            'code.required' => 'Vui lòng nhập mã kích hoạt',
            'name.required' => 'Vui lòng nhập họ tên của bạn',
        );
        return Validator::make($data, $rules, $messages);
    }
}