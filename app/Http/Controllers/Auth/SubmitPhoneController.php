<?php

namespace App\Http\Controllers\Auth;

use App\Helper\AuthHelper;
use App\Helper\UtilHelper;
use App\Http\Controllers\Controller;
use App\HttpRequest\SmsRequest;
use App\Sms;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SubmitPhoneController extends Controller
{
    public function submitPhone(Request $request)
    {
        $data = $request->all();
        $data['phone'] = UtilHelper::phoneBuilder($request->input('phone'));
        $request->replace($data);
        $validate = $this->validator($data);
        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate);
        }
        $user = AuthHelper::current_user();
        $user->phone = $data['phone'];
        $sms = new Sms();
        $sms->user_id = $user->id;
        $sms->type = 'active';
        $sms->save();
        $user->save();
        SmsRequest::sendActiveCode($data['phone'], $sms->code);
        return redirect()->route('auth.active');
    }

    public function getViewSubmitPhone()
    {
        return view('auth.submit-phone');
    }

    protected function validator($data)
    {
        $rules = array(
            'phone' => 'phone:VN|required|unique:users,phone',
        );
        $messages = array(
            'phone.required' => 'Vui lòng nhập số điện thoại',
            'phone.phone'    => 'Số điện thoại không đúng',
            'phone.unique'   => 'Số điện thoại đã sử dụng',
        );
        return Validator::make($data, $rules, $messages);
    }
}