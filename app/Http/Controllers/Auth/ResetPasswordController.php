<?php

namespace App\Http\Controllers\Auth;

use App\Helper\AuthHelper;
use App\Helper\UtilHelper;
use App\Http\Controllers\Controller;
use App\HttpRequest\SmsRequest;
use App\Sms;
use App\User;
use function dump;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;

class ResetPasswordController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest', ['except' => ['getViewChangePass', 'changePass']]);
    }

    public function getViewForgotPwd()
    {
        return view('auth.passwords.reset');
    }

    public function getViewSubmitForgot($phone)
    {
        return view('auth.passwords.reset-submit-code', compact('phone'));
    }

    public function submitForgot(Request $request, $phone)
    {
        $phone = UtilHelper::phoneBuilder($phone);;
        $user = User::wherePhone($phone)->first();
        if (!$user) {
            $errors = new MessageBag(['code' => 'Không tìm thấy thông tin của tài khoản này']);
            return redirect()->back()->withInput()->withErrors($errors);
        }
        $data = $request->all();
        $validate = $this->validator($data, $rules = [
            'code' => 'required',
        ]);
        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate);
        }
        $code = $request->input('code');
        if ($sms = $user->forgot($code)) {
            $sms->status = false;
            $sms->save();
            Auth::loginUsingId($user->id);
            return redirect()->route('auth.forgot.changepwd');
        } else {
            $errors = new MessageBag(['code' => 'Mã xác nhận không chính xác.Vui lòng kiểm tra lại tin nhắn']);
            return redirect()->back()->withInput()->withErrors($errors);
        }
    }

    public function forgotPassword(Request $request)
    {
        $data = $request->all();
        $data['phone'] = UtilHelper::phoneBuilder($request->input('phone'));
        $request->replace($data);
        $validate = $this->validator($data, $rules = [
            'phone' => 'phone:VN|required',
        ]);
        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate);
        }
        $phone = $request->input('phone');
        $user = User::wherePhone($phone)->first();
        if (!$user) {
            $errors = new MessageBag(['phone' => 'Không tìm thấy thông tin cho số điện thoại này']);
            return redirect()->back()->withInput()->withErrors($errors);
        }

        if ($user->isNotActive()) {
            $sms = new Sms();
            $sms->user_id = $user->id;
            $sms->type = 'active';
            $sms->save();
//            SmsRequest::sendActiveCode($data['phone'], $sms->code);
            return redirect()->route('auth.active',
                [
                    'phone'    => $phone,
                    'callback' => route('auth.submit.forgot.pwd', compact('phone')),
                    'action'   => 'quen-mat-khau'
                ]
            )->with(
                [
                    'errtext' => 'Số điện thoại này chưa được kích hoạt.<br/> Để tiếp tục sử dụng các tính năng tại chợ vỉa hè vui lòng kích hoạt tài khoản',
                ]);
        }
        $sms = new Sms();
        $sms->type = 'forgot';
        $sms->user_id = $user->id;
        $sms->save();
        SmsRequest::sendForgotPwd($phone, $sms->code);
        return redirect()->route('auth.submit.forgot.pwd', compact('phone'));
    }

    public function getViewChangePass()
    {
        return view('auth.passwords.forgot-change');
    }

    public function changePass(Request $request)
    {
        $user = AuthHelper::current_user();
        $data = $request->all();
        $validate = $this->validator($data, $rules = [
            'newpwd' => 'min:6|max:30|required',
        ]);
        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate);
        }
        $user->log = 'update.pwd';
        $user->password = bcrypt($request->input('newpwd'));
        $user->save();
        return redirect()->route('home');
    }

    protected function validator($data, $rules)
    {
        $messages = array(
            'phone.required'  => 'Vui lòng nhập số điện thoại',
            'code.required'   => 'Vui lòng nhập mã xác nhận',
            'phone.phone'     => 'Số điện thoại không đúng',
            'newpwd.required' => 'Vui lòng nhập mật khẩu',
        );
        return Validator::make($data, $rules, $messages);
    }
}
