<?php

namespace App\Http\Controllers\Member;

use App\Helper\AuthHelper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;

class MemberProfileController extends Controller
{
    public function changePass(Request $request)
    {
        $data = $request->all();
        $validate = $this->validator($data, array(
            'name' => '',
            'password_new' => '',
            'password_confirm' => '',
            'password_old' => '',
        ));

        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate)->withInput();
        }
        $member = AuthHelper::current_user();
//        var_dump($request->filled('password_old')); exit();
        if ($request->filled('password_old') || $request->filled('password_new') || $request->filled('password_confirm')) {
            if (\Hash::check($request->input('password_old'), $member->password)) {
                $validate = $this->validator($data, array(
                    'name' => '',
                ));
                if ($validate->fails()) {
                    return redirect()->back()->withErrors($validate)->withInput();
                }
                $member->password = bcrypt($request->input('password_new'));
                $text = 'Đổi mật khẩu thành công';
            } else {
                $err = new MessageBag(['password_old' => 'Mật khẩu cũ không đúng']);
                return redirect()->back()->withInput()->withErrors($err);
            }
        } else {
            $member->email = $request->input('email');
            $text = 'Đổi email thành công';
        }
        $member->save();
        $msg = new MessageBag(['success_account' => $text]);
        return redirect()->back()->withInput()->withErrors($msg);
    }

    public function changeProfile(Request $request)
    {
        $data = $request->all();
        $validate = $this->validator($data, array(
            'email' => '',
            'name' => '',
            'password_new' => '',
            'password_confirm' => '',
            'password_old' => '',
        ));
        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate)->withInput();
        }
        $member = AuthHelper::current_user();
        $member->name = $request->input('name');
        $member->gender = $request->input('gender');
        $member->birthday = $request->input('birthday');
        $member->address = $request->input('address');
        $member->save();
        $msg = new MessageBag(['success_profile' => 'Đổi thông tin thành công']);
        return redirect()->back()->withInput()->withErrors($msg);
    }

    public function changeAvatar(Request $request)
    {
        $member = AuthHelper::current_user();
        $imageFile = \Image::make($request->imageData);
        $imageFile->resize(config('conf.avatar.size.w'), config('conf.avatar.size.h'));
        $filename = 'avatar-' . $member->id . '-' . time() . '.jpg';
        $imageFile->save('avatar/' . $filename);
        $member->avatar ='avatar/' . $filename;
        $member->save();
    }

    protected function validator($data, $rules)
    {

        $default = [
            'name' => 'required|max:50|min:6',
            'email' => 'email',
            'password_new' => 'required|max:50|min:6',
            'password_confirm' => 'same:password_new',
            'password_old' => 'required',
        ];
        $rules = array_merge($default, $rules);
        $message = array(
            'name.required' => 'Vui lòng nhập tên nhân viên',
            'name.max' => 'Tên nhân viên không được lớn hơn 50 kí tự',
            'name.min' => 'Tên nhân viên không được ít hơn 6 kí tự',
            'password_new.required' => 'Vui lòng nhập mật khẩu',
            'password_new.max' => 'Mật khẩu không được lớn hơn 50 kí tự',
            'password_new.min' => 'Mật khẩu không được ít hơn 6 kí tự',
            'email.required' => 'Vui lòng nhập email',
            'email.email' => 'Vui lòng nhập đúng dịnh dạng email',
            'password_old.required' => 'Vui lòng nhập password cũ',
            'password_confirm.same' => 'Xác nhận mật khẩu sai',
        );
        return Validator::make($data, $rules, $message);
    }

    public function viewProfile()
    {
        return view('pages.myprofile.profile');
    }
}
