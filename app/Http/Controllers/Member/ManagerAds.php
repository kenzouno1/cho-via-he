<?php

namespace App\Http\Controllers\Member;

use App\Ads;
use App\Boost;
use App\Category;
use App\Helper\AuthHelper;
use App\Http\Controllers\Controller;

class ManagerAds extends Controller
{
    public function viewAllPost()
    {
        $user = AuthHelper::current_user();
        $lstAds = Ads::whereUserId($user->id)->with(['boost'])->withoutHidden()->orderByDesc('id')->paginate(6);
        $counts = $this->buildCountAds();
        return view('pages.myprofile.manage-post', array_merge(compact('lstAds'), $counts));
    }

    public function viewActivePost()
    {
        $user = AuthHelper::current_user();
        $lstAds = Ads::whereUserId($user->id)->isActive()->orderByDesc('id')->with(['boost'])->paginate(6);
        $counts = $this->buildCountAds();
        return view('pages.myprofile.manage-post', array_merge(compact('lstAds'), $counts));
    }

    public function viewWaitPost()
    {
        $user = AuthHelper::current_user();
        $lstAds = Ads::whereUserId($user->id)->isDeactive()->orderByDesc('id')->with(['boost', 'metas'])->paginate(6);
        $counts = $this->buildCountAds();
        return view('pages.myprofile.manage-post', array_merge(compact('lstAds'), $counts));
    }

    protected function buildCountAds()
    {
        $user = AuthHelper::current_user();
        $countAll = Ads::whereUserId($user->id)->withoutHidden()->count();
        $countActive = Ads::whereUserId($user->id)->isActive()->count();
        $countDeactive = Ads::whereUserId($user->id)->isDeactive()->count();
        return compact('countAll', 'countDeactive', 'countActive');
    }
}
