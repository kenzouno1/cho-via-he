<?php

namespace App\Http\Controllers\Ads;

use function abort;
use App\Ads;
use App\AdsBoost;
use App\AdsImages;
use App\AdsMeta;
use App\Boost;
use App\Category;
use App\Helper\AuthHelper;
use App\Helper\CategoryHelper;
use App\Helper\CookieHelper;
use App\Helper\FileHelper;
use App\Helper\PaymentHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;


class NewAdsController extends AdsController
{
    function newAds(Request $request, $slug)
    {
        $cate = Category::where('slug', '=', $slug)->first();
        $member = AuthHelper::current_user();
        $lstBoost = $this->getBoost($request);
        $price = AdsBoost::getLstBoostPrice($lstBoost);
        if (!$cate || $member->money < $price) {
            abort(404);
        }
        $data = $request->all();
        if ($request->filled('email')) {
            $validate = $this->validator($data);
        } else {
            $validate = $this->validator($data, ['email' => '']);
        }
        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate)->withInput();
        }
        $ads = new Ads();
        $ads->cate_id = $cate->id;
        $ads->title = $request->input('title');
        $ads->des = $request->input('description');
        $ads->price_type = $request->input('price-type');
        if ($request->input('price-type') == 'fixed' || $request->input('price-type') == 'treat') {
            $ads->price = $request->input('price');
            if ($request->filled('has-min')) {
                $ads->price_min = $request->input('price-min');
            }
        }
        $ads->ads_type = $request->input('ads-type');
        $ads->state = $request->input('product-state');
        $ads->address = $request->input('address');
        $ads->lat = $request->input('lat');
        $ads->lng = $request->input('lng');
        $ads->user_id = $member->id;
        $ads->images = $request->get('list_image');
        $ads->save();
        $this->newAdsMeta($request, $ads, $cate);
        if ($member->isMod()) {
            $this->insert_meta('mod-fullname',$request->input('mod-fullname'));
            $this->insert_meta('mod-fullname',$request->input('mod-phone'));
        }
        $this->newAdsBoost($request, $lstBoost, $ads);
        $member->money = $member->money - $price;
        $member->save();
        FileHelper::createFolder('uploads', 'post-' . $ads->id);
        if (!empty($request->get('list_image'))) {
            $this->moveAdsImage($ads, $request);
        }
        $url = $ads->getUrl();
        return redirect($url);
    }

    protected function moveAdsImage($ads, $request)
    {
        $date = Carbon::now();
        $year = $date->year;
        $month = $date->month;
        $nameFolderTemp = 'temp/' . $year . '/' . $month . '/' . $request->input("rand");
        $nameFolderUpload = 'uploads/' . $year . '/' . $month . '/' . 'post-' . $ads->id;
        $files = \File::allFiles('temp/' . $year . '/' . $month . '/' . $request->input("rand"));
        foreach ($files as $file) {
            \File::move($nameFolderTemp . '/' . $file->getrelativePathname(), $nameFolderUpload . '/' . $file->getrelativePathname());
        }
        \File::deleteDirectory($nameFolderTemp);
    }


    protected function newAdsMeta($request, $ads, $cate)
    {
        $template = $cate->getTemplate();
        /*  Meta template Car */
        if ($template != 'basic') {
            $acceptMetas = config($template . '.meta');
            foreach ($acceptMetas as $meta) {
                if ($request->filled($meta) && !is_null($request->get($meta))) {
                    $newMeta = new AdsMeta();
                    $value = $request->get($meta);
                    $newMeta->meta_key = $meta;
                    $newMeta->meta_value = $value;
                    $newMeta->ads_id = $ads->id;
                    $newMeta->save();
                }
            }
        }
    }

    protected function addMeta($metaKey, $request, $ads)
    {
        if ($request->filled($metaKey)) {
            $meta = new AdsMeta();
            $meta->meta_key = $metaKey;
            $meta->meta_value = $request->input($metaKey);
            $meta->ads_id = $ads->id;
            $meta->save();
        }
    }

    protected function newAdsBoost($request, $lstBoost, $ads)
    {
        $package = $lstBoost->filter(function ($boost) {
            return $boost->type == 'package';
        })->first();
        $lstBoostAds = $lstBoost->filter(function ($boost) {
            return $boost->type != 'package';
        });
        $expired = Carbon::now()->addDays($package->days);
        $adsPackage = new AdsBoost;
        $adsPackage->type = 'package';
        $adsPackage->ads_id = $ads->id;
        $adsPackage->price = $package->price;
        $adsPackage->features = is_array($package->get_features()) ? $package->get_features() : array($package->get_features());
        $adsPackage->boost_id = intval($package->id);
        $adsPackage->expired_at = $expired;
        $adsPackage->renew = $request->filled('auto-renew');
        $adsPackage->save();
        //boost ads
        foreach ($lstBoostAds as $boost) {
            $expired = Carbon::now()->addDays($boost->days);
            $adsBoost = new AdsBoost();
            $adsBoost->type = 'boost';
            $adsBoost->ads_id = $ads->id;
            $adsBoost->boost_id = $boost->id;
            $adsBoost->price = $boost->price;
            $adsBoost->features = array($boost->get_features());
            $adsBoost->expired_at = $expired;
            $adsBoost->save();
        }
    }

    protected function validator($data, $newRules = [])
    {
        $rules = [
            'title' => 'required|max:200|min:10',
            'package' => 'required'
        ];
        $rules = array_merge($rules, $newRules);
        $message = array(
            'title.required' => 'Vui lòng nhập tiêu đề',
            'title.max' => 'Tiêu đề dài quá 200 ký tự',
            'title.min' => 'Tiêu đề quá ngắn',
            'package.required' => 'vui lòng chọn gói đăng'
        );
        return Validator::make($data, $rules, $message);
    }

    public function getViewSelectCate()
    {
        $lstCate = CategoryHelper::allRootCategories();
        return view('pages.ads.newads.selectcategory', ['lstCates' => $lstCate]);
    }

    public function getViewNewAds($slug)
    {
        $cate = Category::where('slug', '=', $slug)->first();
        if ($cate) {
            $template = $cate->getTemplate();
            $config = $cate->loadConfig();
            return view('pages.ads.newads.newads', array_merge(compact('member', 'packages',  'template', 'cate', 'lstFeatures'), $config));
        }
        abort(404);
    }
}
