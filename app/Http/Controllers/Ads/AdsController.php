<?php

namespace App\Http\Controllers\Ads;

use App\Ads;
use App\AdsBoost;
use App\AdsMeta;
use App\Boost;
use App\Helper\AuthHelper;
use App\Helper\BoostHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdsController extends Controller
{
    function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $lstMockup = BoostHelper::getLstMockup();
            $lstHighLight = BoostHelper::getLstHighlight();
            $lstShowHome = BoostHelper::getLstShowHome();
            $lstTopSearch = BoostHelper::getLstTopSearch();
            $member = AuthHelper::current_user();
            $packages = BoostHelper::getLstPackage();
            $lstFeatures = config('package.boost');
            view()->share(compact('lstHighLight','lstMockup','lstShowHome','lstTopSearch','packages','lstFeatures','member'));
            return $next($request);
        },['except' => ['getViewSelectCate']]);
    }

    public function ajaxCheckPrice(Request $request)
    {
        $adsPackage = null;
        if ($request->filled('ads_id')) {
            $ads = Ads::find($request->get('ads_id'));
            if (!$ads) {
                return;
            }
            $adsPackage = $ads->boost()->whereType('package')->first();
        }
        $lstBoost = $this->getBoost($request, $adsPackage);
        $price = AdsBoost::getLstBoostPrice($lstBoost);
        $member = AuthHelper::current_user();
        if (!$member->checkPriceAds($price)) {
            $parameters = [
                'checkPrice' => 'false',
                'currentPrice' => $member->money,
                'orderPrice' => $price
            ];
            return json_encode($parameters);
        } else {
            $parameters = ['checkPrice' => 'true'];
            return json_encode($parameters);
        }
    }

    public function getPackage($request, $adsPackage = null)
    {
        $package = $request->input('package');
        if ($package > 4 || $package <= 0) {
            abort(404);
        }
        $lst = [];

        if (is_null($adsPackage)) {
            //new ads
            $lst[] = $package;
        } else {
            //edit ads
            if ($package > $adsPackage->boost_id) {
                $lst[] = $package;
            }
        }
        return $lst;
    }

    public function getBoost(Request $request, $adsPackage = null)
    {
        $lstBoost = $this->getPackage($request, $adsPackage);
        if ($request->filled('check_mockup') && $request->input('check_mockup') == 'on') {
            $lstBoost[] = $request->input('mockup');
        }
        if ($request->filled('check_highlight') && $request->input('check_highlight') == 'on') {
            $lstBoost[] = $request->input('highlight');
        }
        if ($request->filled('check_show_home') && $request->input('check_show_home') == 'on') {
            $lstBoost[] = $request->input('show_home');
        }
        if ($request->filled('check_top_search') && $request->input('check_top_search') == 'on') {
            $lstBoost[] = $request->input('top-search');
        }
        return Boost::find($lstBoost);
    }

}
