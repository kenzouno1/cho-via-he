<?php

namespace App\Http\Controllers\Ads;

use App\Ads;
use App\AdsBoost;
use App\AdsMeta;
use App\Boost;
use App\Category;
use App\Helper\AuthHelper;
use App\Helper\BoostHelper;
use App\Helper\PaymentHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use function abort;

class EditAdsController extends AdsController
{
    public function getViewEditAds($id)
    {
        $ads = Ads::whereId($id)->first();
        if (!$ads || !$ads->isAuthor()) {
            abort(404);
        }
        $cate = Category::whereId($ads->cate_id)->first();
        $member = AuthHelper::current_user();
        $lstImage = $ads->getAllImageSrc('small');
        $template = $cate->getTemplate();
        $config = $cate->loadConfig();
        return view('pages.ads.editads.editads', array_merge(compact('member', 'template',
            'cate', 'ads', 'lstImage', 'selectedMockup'), $config));
    }

    public function editAds(Request $request, $id)
    {
        $ads = Ads::find($id);
        $adsPackage = $ads->boost()->whereType('package')->first();
        $lstBoost = $this->getBoost($request, $adsPackage);
        $price = AdsBoost::getLstBoostPrice($lstBoost);
        $member = AuthHelper::current_user();
        if (!$ads || !$ads->isAuthor() || $member->money < $price) {
            abort(404);
        }
        if (!$member->checkPriceAds($price)) {
            $parameters = [
                'checkPrice' => 'Tài khoản không đủ tiền',
                'currentPrice' => $member->money,
                'orderPrice' => $price
            ];
            return redirect()->back()->with($parameters);
        }
        $data = $request->all();
        $validate = $this->validator($data);
        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate)->withInput();
        }
        $this->editAdsImage($ads, $request);
        $ads->title = $request->get('title');
        $ads->des = $request->input('description');
        $ads->price_type = $request->input('price-type');
        if ($request->input('price-type') == 'fixed' || $request->input('price-type') == 'treat') {
            $ads->price = $request->input('price');
            if ($request->filled('has-min')) {
                $ads->price_min = $request->input('price-min');
            }
        } else {
            $ads->price = null;
        }
        $ads->ads_type = $request->input('ads-type');
        $ads->state = $request->input('product-state');
        $ads->address = $request->input('address');
        $ads->lat = $request->input('lat');
        $ads->lng = $request->input('lng');
        $ads->images = $request->input('list_image');
        $ads->status = 'deactive';
        if ($member->isMod()) {
            $ads->update_meta('mod-fullname', $request->input('mod-fullname'));
            $ads->update_meta('mod-phone', $request->input('mod-phone'));
        }
        $ads->save();
        $this->editAdsMeta($ads, $request);
        $this->editAdsBoost($request, $lstBoost, $ads, $adsPackage);
        $member->money = $member->money - $price;
        $member->save();
        $url = $ads->getUrl();
        return redirect($url);

    }

    public function editAdsMeta($ads, $request)
    {
        $template = $ads->category->getTemplate();
        if ($template != 'basic') {
            $metas = $ads->metas()->get();
            $acceptMetas = config($template . '.meta');
            $args = $metas->pluck('meta_key')->toArray();
            $difArray = array_diff($acceptMetas, $args);
            foreach ($metas as $meta) {
                if ($request->filled($meta->meta_key) && $meta->meta_value != $request->get($meta->meta_key)) {
                    $meta->meta_value = $request->get($meta->meta_key);
                    $meta->save();
                }
            }
            if ($request->filled('muc-luong-max')) {
                $ads->insert_meta('muc-luong-max', $request->get('muc-luong-max'));
            }
            if ($request->filled('muc-luong-min')) {
                $ads->insert_meta('muc-luong-min', $request->get('muc-luong-min'));
            }
            foreach ($difArray as $meta) {
                if ($request->filled($meta) && !is_null($request->get($meta))) {
                    $newMeta = new AdsMeta();
                    $value = $request->get($meta);
                    $newMeta->meta_key = $meta;
                    $newMeta->meta_value = $value;
                    $newMeta->ads_id = $ads->id;
                    $newMeta->save();
                }
            }
        }
    }

    public function editAdsImage($ads, $request)
    {
        $jsonImages = $request->input('list_image');
        $newImages = json_decode($jsonImages, true);
        if (is_array($newImages)) {
            $lstRemove = array_diff($ads->images, $newImages);
            foreach ($lstRemove as $image) {
                $ads->deleteImage($image);
            }
            $date = Carbon::now();
            $year = $date->year;
            $month = $date->month;
            $pathTempImages = 'temp/' . $year . '/' . $month . '/' . $request->input("rand");
            if (\File::exists($pathTempImages)) {
                $nameFolderTemp = 'temp/' . $year . '/' . $month . '/' . $request->input("rand");
                $files = \File::allFiles('temp/' . $year . '/' . $month . '/' . $request->input("rand"));
                foreach ($files as $file) {
                    \File::move($nameFolderTemp . '/' . $file->getrelativePathname(), $ads->getFolderUpload() . '/' . $file->getrelativePathname());
                }
                \File::deleteDirectory($nameFolderTemp);
            }
        } else {
            foreach ($ads->images as $image) {
                $ads->deleteImage($image);
            }
        }
    }


    public function editAdsBoost($request, $lstBoost, $ads, $adsPackage)
    {
        if ($request->filled('package') && $request->get('package') > $adsPackage->boost_id) {
            $packageID = $request->get('package');
            $currentPackage = $lstBoost->filter(function ($boost) {
                return $boost->type == 'package';
            })->first();
            $expired = Carbon::now()->addDays($currentPackage->days);
            $adsPackage->price = $currentPackage->price;
            $adsPackage->features = is_array($currentPackage->get_features()) ? $currentPackage->get_features() : array($currentPackage->get_features());
            $adsPackage->boost_id = $packageID;
            $adsPackage->expired_at = $expired;
            $adsPackage->renew = $request->filled('auto-renew');
            $adsPackage->save();
        }

        $lstBoostAds = $lstBoost->filter(function ($boost) {
            return $boost->type != 'package';
        });
        foreach ($lstBoostAds as $boost) {
            $expired = Carbon::now()->addDays($boost->days);
            $adsBoost = new AdsBoost();
            $adsBoost->type = 'boost';
            $adsBoost->ads_id = $ads->id;
            $adsBoost->boost_id = $boost->id;
            $adsBoost->price = $boost->price;
            $adsBoost->features = array($boost->get_features());
            $adsBoost->expired_at = $expired;
            $adsBoost->save();
        }

    }

    protected function validator($data, $newRules = [])
    {
        $rules = [
            'title' => 'required|max:200|min:10',
            'package' => 'required'
        ];
        $rules = array_merge($rules, $newRules);
        $message = array(
            'title.required' => 'Vui lòng nhập tiêu đề',
            'title.max' => 'Tiêu đề dài quá 200 ký tự',
            'title.min' => 'Tiêu đề quá ngắn',
            'package.required' => 'vui lòng chọn gói đăng'
        );
        return \Validator::make($data, $rules, $message);
    }
}
