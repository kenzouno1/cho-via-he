<?php

namespace App\Http\Controllers\LandingPage;

use App\Ads;
use App\Http\Controllers\Controller;

class LandingController extends Controller
{
    public function getAds($lstCateIds)
    {

        $lstAds = Ads::whereIn('cate_id', $lstCateIds)
            ->with('author','boost')
            ->isActive()->limit(10)->get();
        return $lstAds;
    }
}
