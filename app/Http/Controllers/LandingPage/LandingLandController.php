<?php

namespace App\Http\Controllers\LandingPage;

use App\Category;
use Illuminate\Http\Request;
use App\Helper\UrlHelper;

class LandingLandController extends LandingController
{
    public function getLandingPageBds()
    {
        $cateId = config('conf.landing_cate.land');
        $cate = Category::whereId($cateId)->first();
        if (!$cate) {
            abort(404);
        }
        $configLoaiBds = config('bds.loaibds');
        $configTinhThanh = config('conf.tinh_thanh');
        $configMucGia = config('bds.mucgia.muaban');
        if ($configLoaiBds) {
            $lstLoaiBds = $configLoaiBds;
        }
        if ($configTinhThanh) {
            $lstTinhThanh = $configTinhThanh;
        }
        if ($configMucGia) {
            $lstMucGia = $configMucGia;
        }
        $lstCategories = $cate->lstChildCategories(true);
        $lstCateIds = $lstCategories->pluck('id');
        $lstAds = $this->getAds($lstCateIds);
        $cateDetails = [
            'name' => 'Bất Động Sản',
            'count' => $cate->countNested(false),
            'image' => asset(config('conf.cate_bg.land')),
        ];
        return view('pages.landing.bds.bds', compact('lstLoaiBds', 'lstTinhThanh', 'lstMucGia',
            'lstAds', 'cateDetails', 'lstCategories', 'cate'));
    }

    public function searchLand(Request $request)
    {
        $cate = $request->get('cate');
        $args = [
            'location' => $request->get('latlng'),
            'address' => $request->get('location'),
            'tu-khoa' => $request->get('title'),
            'post-type' => $request->get('ads-type'),
            'price-min' => $request->get('mucgia')
        ];
        return redirect()->to(UrlHelper::getUrl($args, route('search.list', ['cate' => $cate])));
    }

    public function getListPrice(Request $request)
    {
        $ads_type = $request->get('ads_type');
        $configPrice = [];
        if ($ads_type != "rent") {
            $configPrice = config('bds.mucgia.muaban');
        } else {
            $configPrice = config('bds.mucgia.chothue');
        }
        return $configPrice;
    }
}
