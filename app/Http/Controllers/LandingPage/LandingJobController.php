<?php

namespace App\Http\Controllers\LandingPage;

use App\Category;
use App\Helper\UrlHelper;
use Illuminate\Http\Request;


class LandingJobController extends LandingController
{
    public function getLandingPageJob()
    {
        $cateId = config('conf.landing_cate.job');
        $cate = Category::whereId($cateId)->first();
        if (!$cate) {
            abort(404);
        }
        $lstMucLuong = config('job.mucluong');
        $lstTinhThanh = config('conf.tinh_thanh');
        $lstCategories = $cate->lstChildCategories(true);
        $lstCateIds = $lstCategories->pluck('id');
        $lstAds = $this->getAds($lstCateIds);
        $cateDetails = [
            'name' => 'Bất Động Sản',
            'count' => $cate->countNested(false),
            'image' => asset(config('conf.cate_bg.job')),
        ];
        return view('pages.landing.job.job',
            compact('configMucLuong', 'lstNganhNghe', 'lstTinhThanh', 'lstMucLuong',
                'lstAds', 'lstCategories', 'cateDetails', 'cate'));
    }

    public function searchJob(Request $request)
    {
        $cate = $request->get('cate');
        $args = [
            'location' => $request->filled('latlng') ? $request->get('latlng') : config('conf.default_search.location'),
            'address' => $request->get('location'),
            'tu-khoa' => $request->get('title'),
        ];
        if ($request->filled('muc-luong')) {
            $arrMucluong = explode('-', $request->get('muc-luong'));
            $args['muc-luong-min'] = $arrMucluong[0];
            $args['muc-luong-max'] = $arrMucluong[1];
        }
        return redirect(UrlHelper::getUrl($args, route('search.list', ['cate' => $cate])));
    }
}
