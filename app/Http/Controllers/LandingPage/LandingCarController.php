<?php

namespace App\Http\Controllers\LandingPage;

use App\Category;
use Illuminate\Http\Request;
use App\Helper\UrlHelper;

class LandingCarController extends LandingController
{
    public function getLandingPageCar()
    {
        $cateId = config('conf.landing_cate.car');
        $cate = Category::whereId($cateId)->first();
        if (!$cate) {
            abort(404);
        }
        $configHangXe = config('car.model');
        $configTinhThanh = config('conf.tinh_thanh');
        $configGiaxe = config('car.price');
        if ($configHangXe && is_array($configHangXe)) {
            $lstHangXe = array_keys($configHangXe);
        }
        if ($configTinhThanh) {
            $lstTinhThanh = $configTinhThanh;
        }
        if ($configGiaxe) {
            $lstGiaxe = $configGiaxe;
        }
        $lstCategories = $cate->lstChildCategories(true);
        $lstCateIds = $lstCategories->pluck('id');
        $lstAds = $this->getAds($lstCateIds);
        $cateDetails = [
            'name'  => 'Ô Tô',
            'count' => $cate->countNested(false),
            'image' => asset(config('conf.cate_bg.car')),
        ];
        return view('pages.landing.car.car', compact('lstHangXe', 'lstTinhThanh', 'lstGiaxe',
            'lstAds', 'cateDetails', 'lstCategories','cate'));
    }

    public function getModel(Request $request)
    {
        if ($request->ajax()) {
            $slug = $request->get('slug');
            $configHangXe = config("car.model");
            if (array_key_exists($slug, $configHangXe)) {
                $lstDongXe = $configHangXe[$slug];
            } else {
                $lstDongXe = [];
            }
            $data = ['status' => 200, 'data' => $lstDongXe];
        } else {
            $data = ['status' => 401, 'data' => 'Bạn không có quyền truy cập trang này'];
        }
        return response()->json($data);
    }

    public function searchCar(Request $request)
    {
        $cate =  $request->get('cate');
        $args = [
            'location'  => $request->get('latlng'),
            'address'   => $request->get('location'),
            'dongxe'    => $request->get('dongxe'),
            'trangthai' => $request->get('product-state'),
            'namsx'     => $request->get('year'),
            'tu-khoa'   => $request->get('title'),
        ];
        if ($request->filled('price')) {
            $price = $request->get('price');
            if ($price != '>1000000000' && $price != 0) {
                $priceArray = explode('-', $price);
                $args['price-min'] = $priceArray[0];
                $args['price-max'] = $priceArray[1];
            } else {
                $priceMin = str_replace('>', '', $price);
                $args['price-min'] = $priceMin;
            }
        }
        return redirect(UrlHelper::getUrl($args, route('search.list', ['cate' => $cate])));
    }
}
