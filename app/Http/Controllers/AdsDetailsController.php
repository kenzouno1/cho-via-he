<?php

namespace App\Http\Controllers;

use App\Ads;
use App\Events\AdsViewHandle;
use App\Helper\AdsHelper;
use App\Helper\MapHelper;
use App\Helper\NavHelper;
use function dump;
use Illuminate\Http\Request;
use function abort;

class AdsDetailsController extends Controller
{
    public function getViewDetails($slug)
    {
        $ads = Ads::where('slug', '=', $slug)
            ->with(['category', 'metas'])->first();
        if (!$ads || !$ads->canView()) {
            abort(404);
        }
        $title = $ads->title . '- Chợ Vỉa Hè';
        $lstCategories = $ads->category->lstParentCategories();
        event(new AdsViewHandle($ads));
        $breadcumbs = NavHelper::breadCrumbGenerator($lstCategories, $ads);
        $thumbs = $ads->getThumbnail(10, false);
        $lstAdd = [MapHelper::getMap($ads)];
        $lstSavedSearch = AdsHelper::getSavedSearch();
        $template = $ads->category->getTemplate();
        if ($template == 'job') {
            $lstRelated = $ads->getRelated($lstCategories, 6);
        } else {
            $lstRelated = $ads->getRelated($lstCategories);
        }
        return view('pages.post.post-detail', compact('ads', 'thumbs', 'lstAdd', 'lstSavedSearch',
            'lstRelated', 'breadcumbs', 'title', 'template'));
    }

    public function getPhoneAds(Request $request)
    {
        if ($request->ajax()) {
            $slug = $request->get('slug');
            if ($ads = Ads::whereSlug($slug)->limit(1)->with(['author'])->first()) {
                if ($ads->author->isMod()) {
                    $response = ['status' => 200, 'msg' => $ads->get_meta('mod-phone')];
                } else {
                    $response = ['status' => 200, 'msg' => $ads->author->phone];
                }
            } else {
                $response = ['status' => 404, 'msg' => 'Không tìm bài đăng hợp lệ'];
            }
        } else {
            $response = ['status' => 401, 'msg' => 'Chỉ hiển thị thông tin cho request ajax'];
        }
        return response()->json($response);
    }
}
