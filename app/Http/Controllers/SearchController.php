<?php

namespace App\Http\Controllers;

use function abort;
use App\Ads;
use App\AdsMeta;
use App\Category;
use App\Helper\AdsHelper;
use App\Helper\CategoryHelper;
use App\Helper\MapHelper;
use App\Helper\NavHelper;
use function dump;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    public function getViewNewestAds(Request $request, $cate = null)
    {
        $template = 'basic';
        $listCateID = null;
        if ($cate && !is_null($cate)) {
            $currentCate = Category::whereSlug($cate)->first();
            if (!$currentCate){
                abort(404);
            }
            $title ='Tìm Kiếm - '.$currentCate->name.' - Chợ Vỉa Hè';
            $listCateID = $currentCate->lstChildCategories()->pluck('id');
            $template = $currentCate->getTemplate();
            $listCate = NavHelper::searchCateNav($currentCate);
        }else{
            $title ='Tìm Kiếm - Chợ Vỉa Hè';
            $listCate = NavHelper::searchCateNav();
        }
        $view = $this->getViewStyle($request);
        $location = $this->getLocation($request);
        $listAdsNew = $this->getAds($request, $listCateID, 'newest');
        //get all ads not pagenavi
        $listAll = $this->getAds($request, $listCateID, 'all');
        if ($template != 'basic') {
            $meta = $this->getMetasCurrentAds($listAll, $template);
        }
        $listTopSearch = $this->getAds($request, $listCateID);
        $listMap = MapHelper::getMaps($listAdsNew->merge($listTopSearch));
        $lstSavedSearch = AdsHelper::getSavedSearch();
        return view('pages.search.search', compact('listTopSearch', 'listAdsNew', 'view', 'listMap', 'listCate',
            'currentCate', 'location', 'template', 'meta','lstSavedSearch','title'));
    }

    protected function getAds($request, $cateId = null, $type = 'top')
    {
        $query = Ads::isActive()->with('author', 'boost', 'category')->filter($request->all())->filterMeta($request->all());
        if ($request->filled('location')) {
            $locations = explode('-', $request->get('location'));
            $distance = $request->filled('distance') ? intval($request->get('distance')) : 10;
            $query->nearest($locations[0], $locations[1], $distance);
        }
        if (!is_null($cateId)) {
            $query->whereIn('cate_id', $cateId);
        }
        switch ($type) {
            case 'top':
                $lst = $query->topSearch()->limit(6)->inRandomOrder()->get();
                break;
            case 'newest' :
                $lst = $query->orderBy('id', 'desc')->paginate(10);
                break;
            case 'all' :
                $lst = $query->select('ads.id')->get();
                break;
            default:
                $lst = [];
                break;
        }
        return $lst;
    }

    protected function getLocation($request)
    {
        $location = [
            'center' => config('conf.default_search.center'),
            'zoom'   => config('conf.default_search.zoom')
        ];
        if ($request->filled('location')) {
            $location['center'] = str_replace('-', ',', $request->get('location'));
            $location['zoom'] = 13;
        }
        return $location;
    }

    protected function getViewStyle($request)
    {
        $view = 'list';
        if ($request->filled('view') && $request->get('view') == 'grid') {
            $view = 'grid';
        }
        return $view;
    }

    protected function getMetasCurrentAds($lst, $template)
    {
        $lstId = $lst->pluck('id');
        $acceptMeta = config($template . '.meta');
          $lstMetas=  DB::table('ads_meta')->whereIn('ads_id', $lstId)
            ->whereIn('meta_key', $acceptMeta)
            ->get();
        $lst = $lstMetas->groupBy('meta_key');
        return $lst;
    }
}