<?php

namespace App\Http\Controllers\Report;

use App\Ads;
use App\Http\Controllers\Controller;
use App\Report;
use Illuminate\Http\Request;

class ReportAdsController extends Controller
{
    public function report(Request $request)
    {
        if ($request->ajax() &&
            $request->has('reason') &&
            $request->has('id') &&
            $this->checkType($request->get('reason'))
        ) {
            $id = $request->get('id');
            $reason = $request->get('reason');
            $report = new Report();
            $report->reason = $reason;
            $report->target_id = $id;
            $report->ip = $request->ip();
            $report->target_type = Report::getActualClassNameForMorph(Ads::class);
            if ($request->has('phone')) {
                $report->phone = $request->get('phone');
            }
            if ($request->has('email')) {
                $report->email = $request->get('email');
            }
            if ($request->has('message')) {
                $report->message = $request->get('message');
            }
            $report->type = 'ads';
            $report->save();
        }
    }

    protected function checkType($type)
    {
        $accept =['fake','duplicate','selled','unavailable','cheat'];
        return in_array($type,$accept);
    }
}
