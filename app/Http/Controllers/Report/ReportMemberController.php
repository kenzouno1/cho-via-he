<?php

namespace App\Http\Controllers\Report;

use App\Report;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReportMemberController extends Controller
{
    public function report(Request $request)
    {
        if ($request->ajax() &&
            $request->filled('reason') &&
            $request->filled('id') &&
            $this->checkType($request->get('reason'))
        ) {
            $id = $request->get('id');
            $reason = $request->get('reason');
            $report = new Report();
            $report->reason = $reason;
            $report->target_id = $id;
            $report->ip = $request->ip();
            $report->target_type = Report::getActualClassNameForMorph(Ads::class);
            if ($request->filled('phone')) {
                $report->phone = $request->get('phone');
            }
            if ($request->filled('email')) {
                $report->email = $request->get('email');
            }
            if ($request->filled('message')) {
                $report->message = $request->get('message');
            }
            $report->type = 'member';
            $report->save();
        }
    }

    protected function checkType($type)
    {
        $accept = ['thumb-error', 'info-error', 'scam', 'other'];
        return in_array($type, $accept);
    }
}
