<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            view()->share('notification', $this->getNotifiCation());
            return $next($request);
        });
    }

    protected function getNotifiCation(){
        $notify = [];
        if (Auth::check()){
            $user = Auth::user();
            $notify['messager']= $user->unreadMessagesCount();
        }
        return collect($notify);
    }
}
