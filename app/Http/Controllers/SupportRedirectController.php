<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SupportRedirectController extends Controller
{
    public function detail(){
        return redirect('');
    }
    public function index(){
        return redirect('');
    }
    public function list(){
        return redirect('');
    }
    public function contact(){
        return redirect('');
    }
}
