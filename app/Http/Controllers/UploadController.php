<?php

namespace App\Http\Controllers;

use App\Helper\AuthHelper;
use App\Helper\FileHelper;
use App\ImagesFilter\AdsImageFilter;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

//use Intervention\Image\Image;

class UploadController extends Controller
{
    public function upload(Request $request)
    {
        $rand_folder = $request->input('rand');
        $folder = FileHelper::createFolder('temp', $rand_folder);
//        $member = AuthHelper::current_user();
        $this->validate($request, [
            'images' => 'dimensions:min_width=150,min_height=150'
        ]);
        $file = $request->images;
        $imageName = str_slug(basename($file->getClientOriginalName(), '.' . $file->getClientOriginalExtension()), '-') . '-' . time() . '.jpg';
        Image::make($file->getRealPath())->filter(new AdsImageFilter($folder, $imageName));
        echo $imageName;
    }

}