<?php

namespace App\Http\Controllers\backend;

use App\Ads;
use App\Helper\CategoryHelper;
use App\PaymentLogs;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminStatisticsController extends Controller
{
    public function viewDashboard()
    {
        $lstCount = $this->getCount();
        $countAds = $lstCount['count_ads'];
        $countUser = $lstCount['count_user'];
        $countCar = $lstCount['count_car'];
        $countJob = $lstCount['count_job'];
        $countBds = $lstCount['count_bds'];
        $countMoney = $lstCount['count_money'];
        return view('backend.pages.dashboard.dashboard', compact('countAds', 'countUser', 'countCar', 'countJob', 'countBds', 'countMoney'));
    }

    public function getCount()
    {
        $lstads = Ads::whereStatus('active');
        $user = User::all();
        $lstCateChildOfCar = CategoryHelper::lstChildCar(false);
        $lstCateChildOfJob = CategoryHelper::lstChildJob(false);
        $lstCateChildOfLand = CategoryHelper::lstChildLand(false);
        $lstCarIds = $lstCateChildOfCar->pluck('id')->toArray();
        $lstJobIds = $lstCateChildOfJob->pluck('id')->toArray();
        $lstLandIds = $lstCateChildOfLand->pluck('id')->toArray();
        $countAds = $lstads->count();
        $countUser = $user->count();
        $countCar = Ads::isActive()->whereIn('cate_id', $lstCarIds)->count();
        $countJob = Ads::isActive()->whereIn('cate_id', $lstJobIds)->count();
        $countBds = Ads::isActive()->whereIn('cate_id', $lstLandIds)->count();
        $countMoney = number_format(PaymentLogs::isSuccess()->sum('money'), 0);
        return $ajax_data = array(
            'count_ads' => $countAds,
            'count_user' => $countUser,
            'count_car' => $countCar,
            'count_job' => $countJob,
            'count_bds' => $countBds,
            'count_money' => $countMoney
        );
    }
}
