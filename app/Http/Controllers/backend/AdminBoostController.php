<?php

namespace App\Http\Controllers\backend;

use App\Boost;
use App\BoostOptions;
use App\Helper\AuthHelper;
use App\Helper\UtilHelper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Yajra\DataTables\Facades\DataTables;

class AdminBoostController extends Controller
{
    protected function validator($data)
    {
        $rules = [
            'name' => 'required|max:50',
            'days' => 'required|max:50|integer|min:0',

        ];
        $message = [
            'name.required' => '
            Vui lòng nhập tên gói cước',
            'name.max'      => 'Tên gói không được lớn hơn 50 kí tự',
            'days.required' => 'Vui lòng nhập số ngày',
            'days.max'      => 'số ngày không được phép lớn hơn 50',
            'days.min'      => 'Số ngày không được nhỏ hơn 0',
        ];
        return Validator::make($data, $rules, $message);
    }

    function newPackage(Request $request)
    {
        $user = AuthHelper::current_user();
        $data = $request->all();
        $validate = $this->validator($data);
        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate)->withInput();
        }
        $name = $request->input('name');
        $boost = new Boost();
        $boost->name = $name;
        $boost->price = $request->input('price');
        $boost->days = $request->input('days');
        $boost->type = 'boost';
        $boost->description = '';
        $boost->save();
        $features = $request->input('features');
        $boostOptions = new BoostOptions();
        $boostOptions->key = 'features';
        $boostOptions->value = $features;
        $boostOptions->boost_id = $boost->id;
        $boostOptions->save();
        $msg = new MessageBag(['success' => 'Đã tạo thành công gói ' . $name]);
        return redirect()->back()->withInput()->withErrors($msg);
    }

    function viewNewPackage()
    {
        $lstFeatures = config('package.boost');
        return view('backend.pages.package.boost.new', compact('lstFeatures'));
    }

    function viewListPackage()
    {
        return view('backend.pages.package.list');
    }

    function listPackageAjax()
    {
        $query = Boost::where('id', '>', '0');
        return DataTables::of($query)
            ->addColumn('action', function ($package) {
                $html = '<a href="' . route('admin.package.edit', ['id' => $package->id]) . '" class="btn btn-xs green">Sửa</a>';
                $html .= '<a href="#" class="btn btn-xs red btn-delete" data-name="' . $package->name . '"  data-id="' . $package->id . '" >Xóa</a>';
                return $html;
            })
            ->editColumn('days', function ($package) {
                if ($package->days == 0) return 'Không giới hạn';
                return $package->days . ' ngày';
            })
            ->editColumn('price', function ($package) {
                return UtilHelper::money($package->price);
            })
            ->editColumn('type', function ($package) {
                return $package->get_type_label_name();
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function viewEditPackage($id)
    {
        $lstFeatures = config('package.boost');
        $types = config('package.type');
        $package = Boost::whereId($id)->first();
        return view('backend.pages.package.edit', compact('package', 'lstFeatures', 'types'));
    }

    public function editPackage(Request $request, $id)
    {
        $package = Boost::whereId($id)->first();
        $data = $request->all();
        $validate = $this->validator($data);
        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate)->withInput();
        }
        $package->name = $request->input('name');
        if ($request->filled('description')) {
            $package->description = $request->input('description');
        }
        $package->price = $request->filled('price') ? $request->input('price') : 0;
        $package->days = $request->input('days');
        $oldFeatureIds = $package->options->filter(function ($option) {
            return $option->key == 'features';
        })->keyBy('id')->keys();
        BoostOptions::whereIn('id', $oldFeatureIds)->delete();
        if ($request->filled('features')) {
            $features = $request->input('features');
            foreach ($features as $feature) {
                $boostOptions = new BoostOptions();
                $boostOptions->key = 'features';
                $boostOptions->value = $feature;
                $boostOptions->boost_id = $id;
                $boostOptions->save();
            }
        }
        $package->save();
        $msg = new MessageBag(['success' => 'Đã cập nhật gói ' . $package->name]);
        return redirect()->back()->withInput()->withErrors($msg);
    }

    function removePackage(Request $request)
    {
        $id = $request->input('id');
        $package = Boost::whereId($id)->first();
        if ($package && $package->type != 'package') {
            $package->delete();
        }
    }
}
