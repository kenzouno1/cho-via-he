<?php

namespace App\Http\Controllers\backend;


use App\Admins;
use App\Helper\AuthHelper;
use App\Helper\FileHelper;
use App\Http\Controllers\Controller;
use App\Roles;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Image;
use function bcrypt;


class AdminEmployeeController extends Controller
{


    public function __construct()
    {

    }

    public function viewListEmployee()
    {
        $roles = Roles::all();
        return view('backend.pages.user.list', ['roles' => $roles]);
    }

    public function listEmployeeAjax(Request $request)
    {
        $query = Admins::query();
        return DataTables::of($query)
            ->addColumn('checkbox', function ($employee) {
                return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="' . $employee->id . '" data-set="#list-cate"><span></span></label>';
            })
            ->addColumn('action', function ($employee) {
                $html = '<a href="' . route('admin.employee.edit', ['id' => $employee->id]) . '" class="btn btn-xs green">Sửa</a>';
                $html .= '<a href="#" class="btn btn-xs red btn-delete" data-name="' . $employee->name . '"  data-id="' . $employee->id . '" >Xóa</a>';
                return $html;
            })
            ->addColumn('roles', function ($employee) {
                $html = '';
                foreach ($employee->role->toArray() as $role) {
                    $html .= $role['name'] . ',';

                }
                return rtrim($html, ",");
            })
            ->editColumn('status', function ($employee) {
                return '<span class="label label-sm label-' . $employee->label_class() . '">' . config('conf.employee.status.' . $employee->status) . '</span>';
            })
            ->rawColumns(['checkbox', 'action', 'roles', 'status'])
            ->make(true);
    }

    public function viewNewEmployee()
    {
        $roles = Roles::all();
        return view('backend.pages.employee.new', ['roles' => $roles]);
    }

    public function newEmployee(Request $request)
    {
        $data = $request->all();
        $validate = $this->validator($data);
        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate)->withInput();
        }
        $name = $request->input('name');
        $username = $request->input('username');
        $password = $request->input('pwd');
        $email = $request->input('email');
        $user = AuthHelper::current_user();
        $employee = new Admins;
        $employee->name = $name;
        $employee->username = $username;
        $employee->email = $email;
        $employee->password = bcrypt($password);
        $employee->status = $request->input('status');
        if ($request->filled('phone')) {
            $employee->phone = $request->input('phone');
        }
        if ($request->filled('add')) {
            $employee->address = $request->input('add');
        }
        if ($request->filled('birth')) {
            $employee->birthday = $request->input('birth');
        }
        if ($request->hasFile('avatar')) {
            $file = $request->avatar;
            $imageName = FileHelper::getFileName($file);
            Image::make($file->getRealPath())->resize(config('conf.avatar.size.w'), config('conf.avatar.size.h'))->save('avatar/' . $imageName);
            $employee->avatar = 'avatar/' . $imageName;
        }
        $employee->save();
        //insert all role relation
        $employee->role()->attach($request->input('role'));
        $msg = new MessageBag(['success' => 'Đã tạo thành công nhân viên ' . $username]);
        return redirect()->back()->withInput()->withErrors($msg);
    }

    public function viewEditEmployee($id)
    {
        $roles = Roles::all();
        $employee = Admins::findOrFail($id);
        return view('backend.pages.employee.edit', compact('id', 'roles', 'employee'));
    }

    public function editProfile(Request $request, $id)
    {
        $data = $request->all();
        $validate = $this->validateProfile($data);
        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate)->withInput();
        }
        $user = AuthHelper::current_user();
        $employee = Admins::where('id', $id)->first();
        $employee->name = $request->input('name');
        $employee->email = $request->input('email');
        $employee->birthday = $request->filled('birth') ? $request->input('birth') : null;
        $employee->phone = $request->filled('phone') ? $request->input('phone') : null;
        $employee->address = $request->filled('add') ? $request->input('add') : null;
        $employee->save();
        $msg = new MessageBag(['success' => 'Đã tạo cập nhật thông tin thành công ']);
        return redirect()->back()->withInput()->withErrors($msg);
    }

    public function editAvatar(Request $request, $id)
    {
        $data = $request->all();
        $validate = $this->validateAvatar($data);
        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate)->withInput()->with(['tab' => 'avatar']);
        }
        if ($request->hasFile('avatar')) {
            $file = $request->avatar;
            $user = AuthHelper::current_user();
            $employee = Admins::where('id', $id)->first();
            $imageName = FileHelper::getFileName($file);
            Image::make($file->getRealPath())->resize(config('conf.avatar.size.w'), config('conf.avatar.size.h'))->save('avatar/' . $imageName);
//            $file->move(public_path('avatar'), $imageName);
            $employee->avatar = $imageName;
            $employee->save();
            $msg = new MessageBag(['success' => 'Đã cập nhật ảnh đại diện thành công']);
            return redirect()->back()->withInput()->withErrors($msg)->with(['tab' => 'avatar']);
        }
    }

    public function editPwd(Request $request, $id)
    {
        $data = $request->all();
        $validate = $this->validatePwd($data);
        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate)->withInput()->with(['tab' => 'pwd']);
        }
        if ($request->input('pwd') != $request->input('re-pwd')) {
            $msg = new MessageBag(['re-pwd' => 'Vui lòng nhập lại mật khẩu chính xác']);
            return redirect()->back()->withInput()->withErrors($msg)->with(['tab' => 'pwd']);
        }
        $user = AuthHelper::current_user();
        $employee = Admins::where('id', $id)->first();
        $employee->password = bcrypt($request->input('pwd'));
        $employee->save();
        $msg = new MessageBag(['success' => 'Đã cập nhật mật khẩu thành công']);
        return redirect()->back()->withInput()->withErrors($msg)->with(['tab' => 'pwd']);
    }

    public function editRoles(Request $request, $id)
    {
        $user = AuthHelper::current_user();
        $employee = Admins::where('id', $id)->first();
        $employee->role()->sync($request->input('role'));
        $employee->save();
        $msg = new MessageBag(['success' => 'Đã cập nhật mật khẩu thành công']);
        return redirect()->back()->withInput()->withErrors($msg)->with(['tab' => 'pwd']);
    }

    protected function validator($data)
    {
        $rules = [
            'name'     => 'required|max:50|min:6',
            'username' => 'unique:admins,username|required|max:50|min:6',
            'email'    => 'required|email',
            'pwd'      => 'required|max:50|min:6',
            'role'     => 'required',
            'avatar'   => 'image|mimes:jpeg,png,jpg,gif,svg|max:' . config('conf.avatar.max'),
        ];
        $message = array(
            'name.required'     => 'Vui lòng nhập tên nhân viên',
            'name.max'          => 'Tên nhân viên không được lớn hơn 50 kí tự',
            'name.min'          => 'Tên nhân viên không được ít hơn 6 kí tự',
            'username.required' => 'Vui lòng nhập tên tài khoản',
            'username.max'      => 'Tên tài khoản không được lớn hơn 50 kí tự',
            'username.min'      => 'Tên tài khoản không được ít hơn 6 kí tự',
            'username.unique'   => 'Tài khoản này đã tồn tại',
            'pwd.required'      => 'Vui lòng nhập mật khẩu',
            'pwd.max'           => 'Mật khẩu không được lớn hơn 50 kí tự',
            'pwd.min'           => 'Mật khẩu không được ít hơn 6 kí tự',
            'email.required'    => 'Vui lòng nhập tên nhóm quyền',
            'email.email'       => 'Vui lòng nhập đúng dịnh dạng email',
            'role.required'     => 'Vui lòng chọn ít nhất một nhóm quyền',
            'avatar.max'        => 'Avatar chỉ được upload tối đa 2mb',
            'avatar.mimes'      => 'Chỉ được phép upload ảnh',
        );
        return Validator::make($data, $rules, $message);
    }

    protected function validateProfile($data)
    {
        $rules = [
            'name'  => 'required|max:50|min:6',
            'email' => 'required|email',
        ];
        $message = [
            'name.required' => 'Vui lòng nhập tên nhân viên',
            'name.max'      => 'Tên nhân viên không được lớn hơn 50 kí tự',
            'name.min'      => 'Tên nhân viên không được ít hơn 6 kí tự',
        ];
        return Validator::make($data, $rules, $message);
    }

    protected function validatePwd($data)
    {
        $rules = [
            'pwd' => 'required|max:50|min:6',
        ];
        $message = [
            'pwd.required' => 'Vui lòng nhập mật khẩu',
            'pwd.max'      => 'Mật khẩu không được lớn hơn 50 kí tự',
            'pwd.min'      => 'Mật khẩu không được ít hơn 6 kí tự',
        ];
        return Validator::make($data, $rules, $message);
    }

    protected function validateAvatar($data)
    {
        $rules = [
            'avatar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:' . config('conf.avatar.max'),
        ];
        $message = [
            'avatar.max'   => 'Avatar chỉ được upload tối đa 2mb',
            'avatar.mimes' => 'Chỉ được phép upload ảnh',
        ];
        return Validator::make($data, $rules, $message);
    }

    protected function validateRoles($data)
    {
        $rules = [
            'role' => 'required',
        ];
        $message = [
            'role.required' => 'Vui lòng chọn ít nhất một nhóm quyền',
        ];
        return Validator::make($data, $rules, $message);
    }
}
