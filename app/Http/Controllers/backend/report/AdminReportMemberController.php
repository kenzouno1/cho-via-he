<?php

namespace App\Http\Controllers\backend\report;

use App\Report;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminReportMemberController extends Controller
{
    public function reportMember()
    {
        return view('backend.pages.report.list-member');
    }

    public function reportMemberAjax(Request $request)
    {
        $query = Report::where('type', '=', 'member')->get()->filter(function ($report) {
            return $report->user->status == 'active';
        });
        return \DataTables::of($query)
            ->addColumn('checkbox', function ($report) {
                return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="' . $report->id . '" data-set="#list-report"><span></span></label>';
            })
            ->editColumn('email', function ($report) {
                if ($report->email == '' || is_null($report->email)) {
                    return 'Chưa Khai Báo';
                }
                return $report->email;
            })
            ->editColumn('name', function ($report) {
                return $report->user->name;
            })
            ->addColumn('action', function ($report) {
                $html = '';
                $html .= '
                    <div class="btn-group">
                        <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Tác vụ
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu" role="menu">';
                $html .= '<li> <a href="' . route('account-seller', ['id' => $report->target_id]) . '" target="_blank" class="view-post" data-target="' . $report->target_id . '" >Xem thông tin</a> </li>';
                $html .= '<li><a href="#" class="block-member" data-target="' . $report->target_id . '" data-report="' . $report->id . '">Khóa</a></li>';
                $html .= '<li><a href="#" class="del-report" data-target="' . $report->target_id . '" data-report="' . $report->id . '">Xóa báo cáo</a></li>';
                $html .= ' </ul>
                    </div>';
                return $html;
            })
            ->rawColumns(['checkbox', 'action'])
            ->make(true);
    }

    public function blockMember(Request $request)
    {
        $member = User::find($request->get('member_id'));
        if ($member) {
            $member->status = 'deactive';
            $member->save();
        }
        if ($request->filled('report_id')) {
            $report = Report::find($request->get('report_id'));
            $report->delete();
        }
    }
}
