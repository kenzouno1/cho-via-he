<?php

namespace App\Http\Controllers\backend\report;

use App\Ads;
use App\Report;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminReportAdsController extends Controller
{
    public function reportAds()
    {
        return view('backend.pages.report.list-ads');
    }

    public function reportAdsAjax(Request $request)
    {
        $query = Report::where('type', '=', 'ads')->get()->filter(function ($report) {
            return $report->ads->status == 'active';
        });
        return \DataTables::of($query)
            ->addColumn('checkbox', function ($report) {
                return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="' . $report->id . '" data-set="#list-report"><span></span></label>';
            })
            ->editColumn('email', function ($report) {
                if ($report->email == '' || is_null($report->email)) {
                    return 'Chưa Khai Báo';
                }
                return $report->email;
            })
            ->editColumn('title', function ($report) {
                return $report->ads->title;
            })
            ->editColumn('reason', function ($report) {
                return config("conf.report.$report->reason");
            })
            ->addColumn('action', function ($report) {
                $html = '';
                $html .= '
                    <div class="btn-group">
                        <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Tác vụ
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu" role="menu">';
                $html .= '<li> <a href="' . route('ads.details', ['slug' => $report->ads->slug]) . '" target="_blank" class="view-post" data-target="' . $report->target_id . '" >Xem tin</a> </li>';
                $html .= '<li><a href="#" class="hidden-post" data-target="' . $report->target_id . '" data-report="' . $report->id . '">Ẩn tin</a></li>';
                $html .= '<li><a href="#" class="del-post" data-target="' . $report->target_id . '" data-report="' . $report->id . '">Xóa tin</a></li>';
                $html .= '<li><a href="#" class="del-report" data-target="' . $report->target_id . '" data-report="' . $report->id . '">Xóa báo cáo</a></li>';
                $html .= ' </ul>
                    </div>';
                return $html;
            })
            ->rawColumns(['checkbox', 'action'])
            ->make(true);
    }

    public function hiddenAds(Request $request)
    {
        $ads = Ads::find($request->get('ads_id'));
        if ($ads) {
            $ads->status = 'waiting';
            $ads->save();
        }
        if ($request->filled('report_id')) {
            $report = Report::find($request->get('report_id'));
            $report->delete();
        }
    }

    public function deleteAds(Request $request)
    {
        $ads = Ads::find($request->get('ads_id'));
        if ($ads) {
            $ads->delete();
        }
        if ($request->filled('report_id')) {
            $report = Report::find($request->get('report_id'));
            $report->delete();
        }
    }

    public function deleteReport(Request $request)
    {
        if ($request->filled('report_id')) {
            $report = Report::find($request->get('report_id'));
            $report->delete();
        }
    }

    public function removeSelected(Request $request)
    {
        Report::destroy($request->input('id'));
    }
}
