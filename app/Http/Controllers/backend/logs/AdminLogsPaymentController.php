<?php

namespace App\Http\Controllers\backend\logs;

use App\PaymentLogs;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminLogsPaymentController extends Controller
{
    public function logsPayment()
    {
        return view('backend.pages.logs.list-payment');
    }

    public function logsPaymentAjax(Request $request)
    {
        $query = PaymentLogs::whereId(1);
        return \DataTables::of($query)
            ->editColumn('money', function ($logs) {
                return number_format($logs->money, 0) . ' VNĐ';
            })
            ->editColumn('user_id', function ($logs) {
                return User::find($logs->user_id)->phone;
            })
            ->editColumn('created_at', function ($logs) {
                return Carbon::parse($logs->created_at)->format('H:i:s d/m/Y');
            })
//            ->rawColumns(['money'])
            ->make(true);
    }
}
