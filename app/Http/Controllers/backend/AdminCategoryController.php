<?php

namespace App\Http\Controllers\backend;

use App\Category;
use App\Helper\AuthHelper;
use App\Helper\FileHelper;
use App\Http\Controllers\Controller;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Intervention\Image\Facades\Image;

class AdminCategoryController extends Controller
{

    protected function validator($data)
    {
        $rules = [
            'name' => 'required|max:50',
        ];
        $message = [
            'name.required' => 'Vui lòng nhập tên danh mục',
            'name.max'      => 'Tên danh mục không được lớn hơn 50 kí tự',
        ];
        return Validator::make($data, $rules, $message);
    }

    public function newCate(Request $request)
    {
        $user = AuthHelper::current_user();

        if ($user->can('category.create')) {
            $data = $request->all();
            $validate = $this->validator($data);
            if ($validate->fails()) {
                return redirect()->back()->withErrors($validate)->withInput();
            }
            $name = $request->input('name');
            $parent = $request->get('parent');
            $category = new Category();
            $category->name = $name;
            $category->parent = $parent;
            if ($request->filled('icon')) {
                $category->image = $request->input('icon');
            }
            $category->thumbnail = $this->getThumbnailPath($request);
            $category->save();
            $msg = new MessageBag(['success' => 'Đã tạo thành công danh mục ' . $name]);
            return redirect()->back()->withInput()->withErrors($msg);
        }
    }

    public function getThumbnailPath($request)
    {
        if ($request->hasFile('thumb')) {
            $file = $request->thumb;
            $imageName = FileHelper::getFileName($file);
            $imgPath = 'assets/img/category/' . $imageName;
            Image::make($file->getRealPath())->save($imgPath);
            return $imgPath;
        }
        return null;
    }

    public function viewListCate()
    {
        $categories = Category::all();
        return view('backend.pages.category.list', ['categories' => $categories]);
    }

    public function viewNewCate()
    {
        $categories = Category::all();
        return view('backend.pages.category.new', ['categories' => $categories]);
    }

    public function listCateAjax(Request $request)
    {
        $user = AuthHelper::current_user();
        $query = Category::where('id', '>', '0');
        return DataTables::of($query)
            ->addColumn('checkbox', function ($category) {
                return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="' . $category->id . '" data-set="#list-cate"><span></span></label>';
            })
            ->addColumn('action', function ($category) use ($user) {
                $html = '';
                if ($user->can('category.edit')) {
                    $html .= '<a href="' . route('admin.category.edit', ['id' => $category->id]) . '" class="btn btn-xs green">Sửa</a>';
                }
                if ($user->can('category.delete')) {
                    $html .= '<a href="#" class="btn btn-xs red btn-delete" data-name="' . $category->name . '"  data-id="' . $category->id . '" >Xóa</a>';
                }
                return $html;
            })
            ->editColumn('image', function ($category) {
                return '<div class="text-center"><i class="' . $category->image . '"></i></div>';
            })
            ->editColumn('parent', function ($category) {
                if ($category->parent === '0' || $category->parent === '' || $category->parent === 0) {
                    return '';
                }
                $parent = Category::where('id', $category->parent)->first();
                if (!is_null($parent)) {
                    return $parent->name;
                }
            })
            ->rawColumns(['checkbox', 'action', 'image'])
            ->make(true);
    }

    public function delCate(Request $request)
    {
        $user = AuthHelper::current_user();
        if ($user->can('category.delete')) {
            $id = intval($request->get('id'));
            Category::destroy($id);
        }
    }

    public function viewEditCate($id)
    {
        $categories = Category::where('id', '!=', $id)->get();
        $category = Category::where('id', $id)->first();
        if (is_null($category)) {
            return redirect()->route('admin.category.list');
        }
        return view('backend.pages.category.edit', ['categories' => $categories, 'category' => $category, 'id' => $id]);
    }

    public function editCate(Request $request, $id)
    {
        $user = AuthHelper::current_user();
        if ($user->can('category.edit')) {
            $data = $request->all();
            $validate = $this->validator($data);
            if ($validate->fails()) {
                return redirect()->back()->withErrors($validate)->withInput();
            }
            $name = $request->get('name');
            $icon = $request->get('icon');
            $parent = $request->get('parent');

            $cate = Category::where('id', $id)->first();
            $cate->name = $name;
            $cate->image = $icon;
            $cate->parent = $parent;
            $cate->thumbnail = $this->getThumbnailPath($request);
            $cate->save();
            $msg = new MessageBag(['success' => 'Đã cập nhật danh mục ' . $name]);
            return redirect()->back()->withInput()->withErrors($msg);
        }
    }

    public function removeSelected(Request $request)
    {
        $user = AuthHelper::current_user();
//        if ($user->can('admin.category.delete')) {
        $ids = $request->get('id');
        $lstCate = Category::destroy('id', $ids);
//        }
    }
}

