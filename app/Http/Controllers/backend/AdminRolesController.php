<?php

namespace App\Http\Controllers\backend;

use App\Capability;
use App\Helper\AuthHelper;
use App\Http\Controllers\Controller;
use App\Roles;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;

class AdminRolesController extends Controller
{

    public function __construct()
    {

    }

    protected function validator($data)
    {
        $rules = [
            'name' => 'required|max:50,unique.name',
        ];
        $message = [
            'name.required' => 'Vui lòng nhập tên nhóm quyền',
            'name.max'      => 'Tên nhóm quyền không được lớn hơn 50 kí tự',
            'name.unique'   => 'Tên nhóm quyền đã tồn tại.Vui lòng chọn tên khác',
        ];
        return Validator::make($data, $rules, $message);
    }

    public function viewListRoles()
    {
        $roles = Roles::whereNotIn('id', Roles::$protect)->get();
        return view('backend.pages.permission.list', ['roles' => $roles]);
    }

    public function listRolesAjax(Request $request)
    {
        $user = AuthHelper::current_user();

        $query = Roles::whereNotIn('id', Roles::$protect);
        if ($request->filled('name')) {
            $query->where('name', 'like', '%' . $request->get('name') . '%');
        }
        if (!empty($query)) {
            return DataTables::of($query)
                ->addColumn('action', function ($roles) use ($user) {
                    $html = '';
                    if ($user->can('roles.edit')) {
                        $html .= '<a href="' . route('admin.roles.edit', ['id' => $roles->id]) . '" class="btn btn-xs green">Sửa</a>';
                    }
                    if ($user->can('roles.delete')) {
                        $html .= '<a href="#" class="btn btn-xs red btn-delete" data-name="' . $roles->name . '"  data-id="' . $roles->id . '" >Xóa</a>';
                    }
                    return $html;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function viewNewRole()
    {
        $capabilities = config('capability');
        return view('backend.pages.permission.new', ['capabilities' => $capabilities]);
    }

    public function newRole(Request $request)
    {
        $user = AuthHelper::current_user();
        $data = $request->all();
        $validate = $this->validator($data);
        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate)->withInput();
        }
        $name = $request->input('name');
        $roles = new Roles;
        $roles->name = $name;
        $roles->save();
        if ($request->filled('cap')) {
            $cap = $request->input('cap');
            foreach ($cap as $value) {
                $capability = new Capability;
                $capability->role_id = $roles->id;
                $capability->capability = $value;
                $capability->save();
            }
        }

        $msg = new MessageBag(['success' => 'Đã tạo thành công nhóm quyền ' . $name]);
        return redirect()->back()->withInput()->withErrors($msg);
    }
//	public function removeSelected(Request $request) {
//		$user = AuthHelper::current_user();
//		$ids = $request->get('id');
//		DB::table('roles')
//			->whereIn('id', $ids)
//			->delete();
//		$this->log->pushLog(config('conf.logs_number.roles.remove'), $user->email . ' đã xóa ' . count($ids) . ' nhóm quyền ', $request->ip());
//	}
    public function delRole(Request $request)
    {
        $user = AuthHelper::current_user();
        if (!Roles::isProtect($request->get('id'))) {
            $id = $request->get('id');
            Roles::destroy($id);
        }
    }

    public function viewEditRole($id)
    {
        $roles = Roles::where('id', $id)->first();
        $capabilities = config('capability');
        if (is_null($roles)) {
            return redirect()->route('admin.roles.list');
        }
        return view('backend.pages.permission.edit', ['roles' => $roles, 'id' => $id, 'capabilities' => $capabilities]);
    }

    public function editRole(Request $request, $id)
    {
        $user = AuthHelper::current_user();

        if (!Roles::isProtect($id)) {
            $data = $request->all();
            $validate = $this->validator($data);
            if ($validate->fails()) {
                return redirect()->back()->withErrors($validate)->withInput();
            }
            $name = $request->get('name');
            if (($name != Roles::find($id)->name) && (Roles::where('name', '=', $name)->first() !== null)) {
                $msg = new MessageBag(['name' => 'Nhóm quyền ' . $name . ' đã tồn tại vui lòng chọn tên khác']);
                return redirect()->back()->withInput()->withErrors($msg);
            }
            $caps = $request->input('cap');
            $roles = Roles::where('id', $id)->first();
            $roles->name = $name;
            $roles->save();
            $attribues = array('attributes' => array());
            Capability::where('role_id', $id)->delete();
            if (isset($caps) && is_array($caps)) {
                foreach ($caps as $value) {
                    $attribues['attributes'][] = $value;
                    Capability::updateOrCreate(
                        [
                            'role_id'    => $id,
                            'capability' => $value,
                        ]);
                }
            }

            $msg = new MessageBag(['success' => 'Đã tạo cập nhật danh mục ' . $name]);
            return redirect()->back()->withInput()->withErrors($msg);
        }
    }
}
