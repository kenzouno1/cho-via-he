<?php

namespace App\Http\Controllers\backend;

use App\Helper\AuthHelper;
use App\Helper\FileHelper;
use App\Helper\UtilHelper;
use App\Http\Controllers\Controller;
use App\User;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Intervention\Image\Facades\Image;

class AdminMemberController extends Controller
{
    public function __construct()
    {

    }

    public function viewListMember()
    {
        return view('backend.pages.member.list');
    }

    public function listMemberAjax(Request $request)
    {
        $user = AuthHelper::current_user();
        $query = User::where('id', '>', '0');
        if ($request->filled('name')) {
            $query->where('name', 'like', '%' . $request->get('name') . '%');
        }
        return DataTables::of($query)
            ->addColumn('checkbox', function ($member) {
                return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="' . $member->id . '" data-set="#list-cate"><span></span></label>';
            })
            ->addColumn('action', function ($member) use ($user) {
                $html = '';
                $html .= '
                    <div class="btn-group">
                        <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Tác vụ
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="' . route('admin.member.edit', $member->id) . '">Chỉnh Sửa</a>
                            </li>';
                if ($member->status != 'active') {
                    $html .= '<li>
                                <a class="active-member"  data-name="' . $member->name . '" data-id="' . $member->id . '" href="javascript:;">Kích Hoạt</a>
                            </li>';
                } elseif ($member->isBanned()) {
                    $html .= '<li>
                                <a class="unblock-member" data-name="' . $member->name . '" data-toggle="" data-target=""  data-id="' . $member->id . '" href="javascript:;">Mở khóa</a>
                            </li>';
                } else {
                    $html .= '<li>
                                <a class="block-member" data-name="' . $member->name . '" data-toggle="modal" data-target="#modal-blockmember" data-id="' . $member->id . '" href="javascript:;">Khóa Tài Khoản</a>
                            </li>';
                }
                $html .= '<li>
                                <a class="remove-member" data-name="' . $member->name . '" data-id="' . $member->id . '"  href="javascript:;">Xóa </a>
                            </li>
                        </ul>
                    </div>';
                return $html;
            })
            ->editColumn('name', function ($member) {
                if ($member->name == '' || is_null($member->name)) {
                    return 'Thành Viên Chưa Khai Báo';
                }
                return $member->name;
            })
            ->editColumn('email', function ($member) {
                if ($member->email == '' || is_null($member->email)) {
                    return 'Thành Viên Chưa Khai Báo';
                }
                return $member->email;
            })
            ->editColumn('status', function ($member) {
                if ($member->isBanned()) {
                    $status = 'Đã khóa';
                } else {
                    $status = config('conf.user.status.' . $member->status);
                }
                return '<span class="label label-sm label-' . $member->label_class() . '">' . $status . '</span>';
            })
            ->rawColumns(['checkbox', 'action', 'image', 'status'])
            ->make(true);
    }

    public function viewNewMember()
    {
        return view('backend.pages.member.new');
    }

    public function newMember(Request $request)
    {
        $data = $request->all();
        $data['phone'] = UtilHelper::phoneBuilder($request->input('phone'));
        $request->replace($data);

        $validate = $this->validator($data);
        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate)->withInput();
        }
        $name = $request->input('name');
        $password = $request->input('password');
        $email = $request->input('email');
        $user = AuthHelper::current_user();
        $member = new User;
        $member->name = $name;
        $member->email = $email;
        $member->provider = 'admin';
        $member->provider_id = uniqid('admin-');
        $member->password = bcrypt($password);

        $member->status = $request->input('status');
        if ($request->filled('phone')) {
            $member->phone = $request->input('phone');
        }
        if ($request->filled('address')) {
            $member->address = $request->input('add');
        }
//        if ($request->filled('birthday')) {
//            $member->birthday = $request->input('birth');
//        }
        if ($request->hasFile('avatar')) {
            $file = $request->avatar;
            $imageName = FileHelper::getFileName($file);
            Image::make($file->getRealPath())->resize(config('conf.avatar.size.w'), config('conf.avatar.size.h'))->save('avatar/' . $imageName);
            $member->avatar = 'avatar/' . $imageName;
        }
        $member->save();
        $msg = new MessageBag(['success' => 'Đã tạo thành công thành viên ' . $name]);
        return redirect()->back()->withInput()->withErrors($msg);
    }

    public function delMember(Request $request)
    {
        $user = AuthHelper::current_user();
        User::destroy($request->get('id'));
    }

    public function removeSelected(Request $request)
    {
        $user = AuthHelper::current_user();
        User::destroy($request->input('id'));
    }

    public function activeMember(Request $request)
    {
        $user = AuthHelper::current_user();
        $member = User::find($request->get('id'));
        $member->status = 'active';
        $member->save();
    }

    /**
     * @param Request $request
     */
    public function updateStatus(Request $request)
    {
        $admin = AuthHelper::current_user();
        $id = $request->get('id');
        $status = $request->get('status');
        $user = User::find($id);
        if ($user) {
            $user->status = $status;
            $user->save();
        }
    }

    public function blockMember(Request $request)
    {
        $user = AuthHelper::current_user();
        $member = User::find($request->input('id-block'));
        $member->bans([
            'comment'    => $request->input('comment'),
            'expired_at' => '+1 ' . $request->input('day') . 'day',
        ]);
    }

    public function unblockMember(Request $request)
    {
//        echo $request->input('id');
        $user = AuthHelper::current_user();
        $member = User::where('id', $request->input('id'))->first();
        $member->unban();
    }

    /**
     *
     */
    public function viewEditMember($id)
    {
        $member = User::whereId($id)->first();
        return view('backend.pages.member.edit', compact('member'));
    }

    public function editMember(Request $request, $id)
    {
        $data = $request->all();
        $validate = $this->validator($data, false);
        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate)->withInput();
        }
        $user = AuthHelper::current_user();
        $member = User::where('id', $id)->first();
        $member->name = $request->input('name');
        $email = $request->input('email');
        if ($email != $member->email) {
            if (User::whereEmail($email)->withTrashed()->first()) {
                $err = new MessageBag(['email' => 'Email này đã tồn tại']);
                return redirect()->back()->withInput()->withErrors($err);
            }
            $member->email = $email;
        }
        $member->birthday = $request->filled('birthday') ? $request->input('birthday') : null;
        $member->address = $request->filled('address') ? $request->input('address') : null;
        $member->status = $request->input('status');
        if ($request->hasFile('avatar')) {
//            $member->avatar =
            $file = $request->avatar;
            $imageName = FileHelper::getFileName($file);
            Image::make($file->getRealPath())->resize(config('conf.avatar.size.w'), config('conf.avatar.size.h'))->save('avatar/' . $imageName);
            $member->avatar = $imageName;
        }
        $member->save();
        $msg = new MessageBag(['success' => 'Đã tạo cập nhật thông tin thành công ']);
        return redirect()->back()->withInput()->withErrors($msg);
    }

    protected function validator($data, $new = true)
    {
        $rules = [
            'name'  => 'required|max:50|min:6',
            'email' => 'email',

            'avatar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:' . config('conf.avatar.max'),
        ];
        if ($new == true) {
            $rules['phone'] = 'phone:VN|unique:users,phone|required';
            $rules['email'] = 'email|unique:users,email';
            $rules['password'] = 'required|max:50|min:6';
        }
        $message = array(
            'name.required'     => 'Vui lòng nhập tên nhân viên',
            'name.max'          => 'Tên nhân viên không được lớn hơn 50 kí tự',
            'name.min'          => 'Tên nhân viên không được ít hơn 6 kí tự',
            'password.required' => 'Vui lòng nhập mật khẩu',
            'password.max'      => 'Mật khẩu không được lớn hơn 50 kí tự',
            'password.min'      => 'Mật khẩu không được ít hơn 6 kí tự',
            'email.required'    => 'Vui lòng nhập email',
            'email.email'       => 'Vui lòng nhập đúng dịnh dạng email',
            'email.unique'      => 'Email đã tồn tại',
            'avatar.max'        => 'Avatar chỉ được upload tối đa 2mb',
            'avatar.mimes'      => 'Chỉ được phép upload ảnh',
            'phone.unique'      => 'Số điện thoại đã được đăng ký',
            'phone.phone'       => 'Số điện thoại không đúng',
            'phone.required'    => 'Nhập số điện thoại',
        );
        return Validator::make($data, $rules, $message);
    }
}