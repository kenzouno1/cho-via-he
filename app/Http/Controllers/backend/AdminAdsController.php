<?php

namespace App\Http\Controllers\backend;

use App\Ads;
use App\Boost;
use App\Category;
use App\Helper\CategoryHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;

class AdminAdsController extends Controller
{
    public function viewlistAdsActive()
    {
        $categories = Category::all();
        $lstPackages = Boost::all();
        $packages = $lstPackages->filter(function ($package) {
            return $package->type == 'package';
        });
        return view('backend.pages.adwords.list-active', compact('categories', 'packages'));
    }

    public function viewlistAdsDeactive()
    {
        $categories = Category::all();
        $lstPackages = Boost::all();
        $packages = $lstPackages->filter(function ($package) {
            return $package->type == 'package';
        });
        return view('backend.pages.adwords.list-deactive', compact('categories', 'packages'));
    }

    public function listActiveAjax(Request $request)
    {
        return $this->filterListAds('active', $request);
    }

    public function listDeactiveAjax(Request $request)
    {
        return $this->filterListAds('deactive', $request);
    }

    public function filterListAds($status, $request)
    {
        $query = Ads::whereStatus($status)->filter($request->all());
        return DataTables::of($query)
            ->addColumn('checkbox', function ($ads) {
                return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="' . $ads->id . '" data-set="#list-post"><span></span></label>';
            })
            ->addColumn('action', function ($ads) {
                $html = '';
                $html .= '
                    <div class="btn-group">
                        <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Tác vụ
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu" role="menu">';
                if ($ads->status == 'active') {
                    $html .= '<li>
                                <a class="update-status" data-status="deactive" data-id="' . $ads->id . '">Hủy Duyệt</a>
                            </li>';
                }
                if ($ads->status == 'deactive') {
                    $html .= '<li>
                                <a class="update-status" data-status="active" data-id="' . $ads->id . '">Duyệt</a>
                            </li>';
                }
                $html .= '<li>
                                <a href="' . route('member.manage-post.edit', ['id' => $ads->id]) . '" class="edit-post" data-id="' . $ads->id . '" >Chỉnh sửa</a>
                            </li>';
                $html .= '<li>
                                <a class="remove-post" href="javascript:;" data-id="' . $ads->id . '">Xóa Bài</a>
                            </li>
                        </ul>';
                $html .= '<a href="'.$ads->getUrl().'" class="btn btn-xs red btn-delete" data-id="' . $ads->id . '">Xem</a></div></div>';
                return $html;
            })
            ->editColumn('cate_id', function ($ads) {
                $parent = Category::where('id', $ads->cate_id)->first();
                if (!is_null($parent)) {
                    return $parent->name;
                }
            })
            ->rawColumns(['checkbox', 'action'])
            ->make(true);
    }

    public function updateStatus(Request $request)
    {
        $ads = Ads::whereId($request->input('id'))->first();
        $status = $request->input('status');
        if ($ads) {
            echo 'true';
            switch ($status) {
                case 'active':
                    $ads = $ads->active();
                    break;
                case 'lock':
                    $ads = $ads->locked();
                    break;
                default:
                    $ads = $ads->deactive();
            }
        }
        $ads->save();
    }
}
