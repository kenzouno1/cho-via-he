<?php

namespace App\Http\Controllers\Message;

use App\Ads;
use App\Chat\Thread;
use App\Helper\AuthHelper;
use App\Helper\MessageHelper;
use App\Helper\UtilHelper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use function redirect;

class MemberMessageController extends Controller
{
    public function sendMsg(Request $request)
    {
        if ($request->ajax() && $request->filled('post') && $request->filled('msg')) {
            $adsId = $request->get('post');
            $msg = $request->get('msg');
            $ads = Ads::find($adsId);
            if ($ads) {
                $type = $request->get('type');
                if ($type =='dicker'){
                    $msg = 'Tôi muốn mua sản phẩm này với giá '.UtilHelper::money($msg);
                }
                MessageHelper::sendMsg($ads, $msg);
                $response = ['status' => 204];
            } else {
                $response = ['status' => 401, 'msg' => 'Bạn không có quyền truy cập chức năng này'];
            }
        } else {
            $response = ['status' => 401, 'msg' => 'Bạn không có quyền truy cập chức năng này'];
        }
        return response()->json($response);
    }

    public function getViewMessage()
    {
        $user = AuthHelper::current_user();
        $lstMessages = Thread::forUserThread($user->id)->limitMessage(3)->latest('updated_at')->paginate(10);
        $view = 'list';
        return view('pages.myprofile.message', compact('lstMessages', 'view'));
    }

    public function getViewMessageDetails($slug)
    {
        $view = 'details';
        $id = substr($slug, strrpos($slug, '-') + 1);
        $message = Thread::limitMessage(10)->find($id);
        if ($message) {
            $user = AuthHelper::current_user();
            $message->markAsRead($user->id);
            return view('pages.myprofile.message', compact('message', 'view', 'id','slug'));
        } else {
            abort(404);
        }
    }

    public function remove($slug)
    {
        $id = substr($slug, strrpos($slug, '-') + 1);
        $message = Thread::limitMessage(10)->find($id);
        if ($message) {
            $message->delete();
            return redirect()->route('member.message');
        } else {
            abort(404);
        }
    }
}
