<?php

namespace App;


/**
 * App\BoostOptions
 *
 * @property int $id
 * @property int $boost_id
 * @property string $key
 * @property string $value
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BoostOptions whereBoostId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BoostOptions whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BoostOptions whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BoostOptions whereValue($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AbstractModel orderByField($field, $values)
 */
class BoostOptions extends AbstractModel
{
    protected $table = 'boost_options';
    public $timestamps = false;
    public function options()
    {
        return $this->belongsTo('App\Boost');
    }
}
