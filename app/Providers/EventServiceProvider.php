<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\AdsViewHandle' => [
            'App\Listeners\AdsCounter',
            'App\Listeners\UserViewed',
        ],
        'App\Events\NewAds'=>[
            'App\Listeners\Ads\AddBoostToAds',
            'App\Listeners\Ads\UploadAdsImage',
            'App\Listeners\Ads\CheckoutAds',
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
        Event::listen('ads.view', 'App\Events\AdsViewHandle');
    }
}
