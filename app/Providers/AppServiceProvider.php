<?php

namespace App\Providers;


use App\Helper\AdsHelper;
use App\Helper\TimeHelper;
use App\Helper\UtilHelper;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Validator;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
         if ($this->app->environment() !== 'production') {
             $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
        \Blade::directive('money',function($expression){
            return "<?php echo UtilHelper::money($expression); ?>";
        });
        \Blade::directive('ago',function($expression){
            return "<?php TimeHelper::ago($expression) ?>";
        });
    }

}
