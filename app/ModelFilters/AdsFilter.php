<?php namespace App\ModelFilters;

use App\Ads;
use App\AdsBoost;
use App\Helper\CategoryHelper;
use function dump;
use EloquentFilter\ModelFilter;

class AdsFilter extends ModelFilter
{
    /**
     * Related Models that have ModelFilters as well as the method on the ModelFilter
     * As [relatedModel => [input_key1, input_key2]].
     *
     * @var array
     */

    public $relations = [
//        'metas' => ['yearOfCar', 'madeIn', 'carStyle', 'numberSeats', 'fuel', 'boxNumber', 'kmHasRun', 'typeCar'],
    ];

    public function cate($cateId)
    {
        $helper = new CategoryHelper();
        $listCateID = $helper->getLstChild($cateId)->pluck('id');
        return $this->whereIn('cate_id', $listCateID);
    }

    public function boost($boost)
    {
        $adsBoosts = AdsBoost::where('boost_id', '=', $boost)->get();
        $listAds = [];
        foreach ($adsBoosts as $adsboost) {
            $listAds[] = $adsboost->ads_id;
        }
        return $this->whereIn('id', $listAds);
    }

    public function priceType($priceType)
    {
        return $this->wherePriceType($priceType);
    }

    public function postType($postType)
    {
        return $this->whereAdsType($postType);
    }

    public function priceMin($min)
    {
        return $this->where('price', '>', $min);
    }

    public function priceMax($max)
    {
        return $this->where('price', '<', $max);
    }

    public function trangThai($trangThai)
    {
        return $this->where('state', '=', $trangThai);
    }

    public function tuKhoa($title)
    {
        $lstFulltext = Ads::search($title)->get();
        if ($lstFulltext->isNotEmpty()) {
            $lstIds = $lstFulltext->pluck('id')->toArray();
            return $this->whereIn('id', $lstIds)->orderByField('id', $lstIds);;
        }else{
            return $this->where('id','<','1');
        }
//        return $this->fullTextSearch($title)->orderByFullTextSearch($title);
    }
}
