<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

/**
 * App\Admins
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $address
 * @property string $birthday
 * @property string $avatar
 * @property int $status
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Roles[] $role
 * @method static \Illuminate\Database\Query\Builder|\App\Admins whereAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Admins whereAvatar($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Admins whereBirthday($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Admins whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Admins whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Admins whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Admins whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Admins wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Admins wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Admins whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Admins whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Admins whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Admins whereUsername($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AbstractModel orderByField($field, $values)
 */
class Admins extends AbstractModel implements AuthenticatableContract
{
    use Authenticatable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'phone',
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $table = 'admins';


    public function getAllCap()
    {
        $capabilites = array();
        foreach ($this->role as $key => $role) {
            foreach ($role->capabilites as $key => $cap) {
                $capabilites[] = $cap->capability;
            }
        }
        return $capabilites;
    }

    public function role()
    {
        return $this->belongstoMany('App\Roles', 'role_relationship', 'admin_id', 'role_id');
    }

    public function can($cap)
    {
        return in_array($cap, $this->getAllCap());
    }

    public function getRolesId()
    {
        $roles = array();
        if ($this->role) {
            foreach ($this->role as $key => $role) {
                $roles[] = $role->id;
            }
        }
        return $roles;
    }

    public function label_class()
    {
        switch ($this->status) {
            case 'block' :
                return 'danger';
            default :
                return 'success';
        }
    }
}