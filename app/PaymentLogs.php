<?php

namespace App;

/**
 * App\PaymentLogs
 *
 * @property-read \App\PackagePayment $package
 * @property-read \App\User $user
 * @mixin \Eloquent
 * @property int $id
 * @property float $money
 * @property string $name
 * @property string $type
 * @property string $status
 * @property string|null $transaction_id
 * @property string|null $description
 * @property int $user_id
 * @property int $package_id
 * @property string $created_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentLogs whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentLogs whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentLogs whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentLogs whereMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentLogs whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentLogs wherePackageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentLogs whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentLogs whereTransactionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentLogs whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentLogs whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentLogs isSuccess()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AbstractModel orderByField($field, $values)
 */
class PaymentLogs extends AbstractModel
{
    protected $table = 'payment_logs';
    public $timestamps = false;
    protected $fillable = [
        'id', 'money', 'type', 'status', 'transaction_id', 'user_id', 'package_id', 'created_at', 'name'
    ];

    public function package()
    {
        return $this->belongsTo('App\PackagePayment');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function scopeIsSuccess($query)
    {
        return $query->whereIn('status', ['success',4]);
    }

}
