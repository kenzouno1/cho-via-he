<?php
/**
 * Created by PhpStorm.
 * User: Dat Nguyen
 * Date: 7/29/2017
 * Time: 3:17 PM
 */

namespace App\Query;


class SearchBuilder
{
    public static function filterMeta($query, $args)
    {
        $inputs = array_filter($args);
        $carMeta = config('car.meta');
        $jobMeta = config('job.meta');
        $bdsMeta = config('bds.meta');
        $acceptMeta = collect([$carMeta, $jobMeta, $bdsMeta])->collapse();
        foreach ($inputs as $key => $value) {
            if ($acceptMeta->contains($key)) {
                $query->whereHas('metas', function ($query) use ($key, $value) {
                    $metaQuery = $query->where('meta_key', '=', $key);
                    if ($key == 'muc-luong-min') {
                        $metaQuery->where('meta_value', '>=', intval($value));
                    } elseif ($key == 'muc-luong-max') {
                        $metaQuery->where('meta_value', '<=', intval($value));
                    }else{
                        $metaQuery->where('meta_value', '=', $value);
                    }
                });
            }
        }
        return $query;
    }
}
