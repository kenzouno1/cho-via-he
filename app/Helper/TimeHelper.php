<?php
/**
 * Created by PhpStorm.
 * User: Dat Nguyen
 * Date: 7/29/2017
 * Time: 2:24 PM
 */

namespace App\Helper;


use Carbon\Carbon;

class TimeHelper
{
    public static function ago($time){
        Carbon::setLocale('vi');
        return $time->diffForHumans(Carbon::now());
    }
}