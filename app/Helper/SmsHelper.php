<?php
/**
 * Created by PhpStorm.
 * User: Dat Nguyen
 * Date: 10/4/2017
 * Time: 9:53 AM
 */

namespace App\Helper;


use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class SmsHelper
{
    public static function hasCode($phone,$type)
    {
        return DB::table('sms_code')
            ->leftJoin('users', 'sms_code.user_id', '=', 'users.id')
            ->where('sms_code.status', '=', true)
            ->where('sms_code.type', '=', $type)
            ->where('users.phone', '=', $phone)
            ->where('sms_code.expire_at','>',Carbon::now()->toDateTimeString())
            ->select('sms_code.*', 'users.phone')
            ->first();
    }

    public static function validateCode($phone, $code,$type)
    {
        return DB::table('sms_code')
            ->leftJoin('users', 'sms_code.user_id', '=', 'users.id')
            ->where('sms_code.status', '=', true)
            ->where('sms_code.type', '=', $type)
            ->where('sms_code.code', '=', $code)
            ->where('sms_code.expire_at','>',Carbon::now()->toDateTimeString())
            ->where('users.phone', '=', $phone)
            ->select('sms_code.*', 'users.phone')
            ->first();
    }
}