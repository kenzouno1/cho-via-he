<?php
/**
 * Created by PhpStorm.
 * User: Dat Nguyen
 * Date: 7/29/2017
 * Time: 2:41 PM
 */

namespace App\Helper;


use Carbon\Carbon;

class FileHelper
{
    public static function getFileName($file, $rename = true)
    {
        $name = basename($file->getClientOriginalName(), '.' . $file->getClientOriginalExtension());
        if ($rename) {
            return $name . '_' . time() . '.' . $file->getClientOriginalExtension();
        }
        return $name;
    }

    public static function createFolder($parrent, $folderName = null)
    {
        $date = Carbon::now();
        $year = $date->year;
        $month = $date->month;
        if (!\File::exists($parrent . '/' . $year)) {
            \File::makeDirectory($parrent . '/' . $year);
        }
        if (!\File::exists($parrent . '/' . $year . '/' . $month)) {
            \File::makeDirectory($parrent . '/' . $year . '/' . $month);
        }
        $temp = $parrent . '/' . $year . '/' . $month;
        if (!is_null($folderName)) {
            $temp .= '/' . $folderName;
            if (!\File::exists($temp)) {
                \File::makeDirectory($temp);
            }
        }
        return $temp;
    }
}