<?php
/**
 * Created by PhpStorm.
 * User: Dat Nguyen
 * Date: 7/24/2017
 * Time: 8:42 AM
 */

namespace App\Helper;


use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use function starts_with;

class UrlHelper
{
    public static function getUrl($args, $url = null, $reset = false)
    {
        if (is_null($url)) {
            $url = route(Route::currentRouteName(), Route::getCurrentRoute()->parameters());
        }
        $currentQuery = Input::query();
        $query = array_merge($currentQuery, $args);
        if (count($query) > 0 && !$reset) {
            $url .= '?' . http_build_query($query);
        }
        return $url;
    }

    public static function activeUrl($args)
    {
        $currentQuery = Input::query();
        return count(array_intersect($args, $currentQuery)) > 0 ? 'active' : '';
    }

    public static function classUserProfileActiveUrl($prefix, $absolute = false)
    {
        $currentRoute = Request::route()->getName();
        if ($absolute) {
            return $currentRoute == $prefix ? 'active' : '';
        } else{
            return starts_with($currentRoute, $prefix) ? 'active' : '';
        }
    }

    public static function getSavedSearchUrl($search)
    {
        $args = [
            'location' => $search['location'],
            'distance' => $search['dis'],
        ];
        if ($search['keywords'] != '') {
            $args['tu-khoa'] = $search['keywords'];
        }
        $url = route('search.list', ['cate' => $search['cate']['slug']]);
        return UrlHelper::getUrl($args, $url);
    }
}