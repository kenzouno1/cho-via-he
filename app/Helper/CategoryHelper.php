<?php
/**
 * Created by PhpStorm.
 * User: Dat Nguyen
 * Date: 7/28/2017
 * Time: 1:56 PM
 */

namespace App\Helper;


use App\Category;
use Carbon\Carbon;
use function config;
use Illuminate\Support\Facades\DB;

class CategoryHelper
{

    public static function lstChildJob($withThis = false)
    {
        $jobId = config('conf.landing_cate.job');
        $helper = new CategoryHelper();
        return $helper->getLstChild($jobId, $withThis);
    }

    public static function lstChildLand($withThis = false)
    {
        $jobId = config('conf.landing_cate.land');
        $helper = new CategoryHelper();
        return $helper->getLstChild($jobId, $withThis);
    }

    public static function lstChildCar($withThis = false)
    {
        $carId = config('conf.landing_cate.car');
        $helper = new CategoryHelper();
        return $helper->getLstChild($carId, $withThis);
    }

    public function getLstChild($id, $withThis=true)
    {
        $cachedKey = $withThis ? 'nested_child_with_' . $id : 'nested_child_' . $id;
        return CacheHelper::load($cachedKey, function () use ($id, $withThis) {
            return Category::find($id)->lstChildCategories($withThis);
        });
    }

    public static function getAllCateByAds($lstAds, $limit = 6)
    {
        if ($lstAds) {
            $lstCateIds = $lstAds->pluck('cate_id');
            return Category::whereIn('id', $lstCateIds)->limit($limit)->select('categories.slug', 'categories.name', 'categories.id')->get();
        }
        return false;
    }

    public static function allRootCategories()
    {
        $cachedKey = 'root_categories';
        return CacheHelper::load($cachedKey, function () {
            return Category::whereParent(0)->get();
        });
    }
    public static function allRootCategoriesNotEmpty()
    {
        $cachedKey = 'root_categories_not_empty';
        return CacheHelper::load($cachedKey, function () {
            return Category::whereParent(0)
                ->has('ads','>','0')
//                ->withCount('ads')
                ->get();
        });
    }
    public static function getCountAdsById($id)
    {
        $key = 'count_' . $id;
        return CacheHelper::load($key, function () use ($id) {
            return Category::find($id)->countAllAds();
        }, Carbon::now()->addDays(1));
    }

    public static function getChildById($id)
    {
        $cachedKey = 'child_' . $id;
        return CacheHelper::load($cachedKey, function () use ($id) {
            return Category::find($id)->childrenCate;
        });
    }
}