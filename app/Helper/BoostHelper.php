<?php
/**
 * Created by PhpStorm.
 * User: Dat Nguyen
 * Date: 8/9/2017
 * Time: 9:14 AM
 */

namespace App\Helper;


use App\Boost;
use Illuminate\Support\Facades\DB;

class BoostHelper
{
    public static function getBoostClass($id)
    {
        switch ($id) {
            case 1:
                return 'feature-packages-free';
            case 2:
                return 'feature-packages-plus';
            case 3:
                return 'feature-packages-featured';
            case 4:
                return 'feature-packages-premium';
            default :
                return '';
        }
    }

    public static function instance()
    {
        return new BoostHelper();
    }

    public function lstBoost($type)
    {
        $key = 'list_boost_of_' . $type;
        return CacheHelper::load($key, function () use ($type) {
            return DB::table('boosts')
                ->join('boost_options', 'boosts.id', '=', 'boost_options.boost_id')
                ->where('key', '=', 'features')
                ->where('value', '=', $type)
                ->where('type', '=', 'boost')
                ->get();
        });
    }

    public static function getLstPackage(){
        $key = 'list_package';
        return CacheHelper::load($key, function () {
            return Boost::packages()->get();
        });
    }

    public static function getLstMockup()
    {
        return BoostHelper::instance()->lstBoost('has-mockup');
    }

    public static function getLstShowHome()
    {
        return BoostHelper::instance()->lstBoost('show-home');
    }

    public static function getLstHighlight()
    {
        return BoostHelper::instance()->lstBoost('highlight');
    }

    public static function getLstTopSearch()
    {
        return BoostHelper::instance()->lstBoost('top-search');
    }
}