<?php
/**
 * Created by PhpStorm.
 * User: Dat Nguyen
 * Date: 8/30/2017
 * Time: 11:50 AM
 */

namespace App\Helper;


use function str_contains;

class StringHelper
{
   public static function removeBadWord($str){
       $blacklist = config('badword');
       $pattent = implode('|',$blacklist);
       return preg_replace("/(".$pattent.")/", '***', $str);
   }
   public static function detectBadWord($str){
       $blacklist = config('badword');
      return str_contains($str,$blacklist);
   }
}