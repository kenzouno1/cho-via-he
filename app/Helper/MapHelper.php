<?php
/**
 * Created by PhpStorm.
 * User: duc
 * Date: 7/26/2017
 * Time: 10:49 AM
 */

namespace App\Helper;


class MapHelper
{

    public static function getMaps($listMap)
    {
        $args = [];
        foreach ($listMap as $key => $ads) {
            $args[] = MapHelper::getMap($ads,($key+1));
        }
        return $args;
    }
    public static function getMap($ads,$key=1){
        $args = $ads->getMap();
        $args['number'] = $key;
        return $args;
    }
}