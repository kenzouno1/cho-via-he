<?php
/**
 * Created by PhpStorm.
 * User: Dat Nguyen
 * Date: 7/24/2017
 * Time: 1:05 PM
 */

namespace App\Helper;


use function array_diff_key;
use function is_array;

class UtilHelper
{
    public static function isReload()
    {
        return isset($_SERVER['HTTP_CACHE_CONTROL']) && $_SERVER['HTTP_CACHE_CONTROL'] === 'max-age=0';
    }

    public static function phoneBuilder($phone)
    {
        $phone = str_replace('+84', '0', $phone);
        $firstChar = substr($phone, 0, 1);
        if ($firstChar != '0') {
            $phone = '0' . $phone;
        }
        return $phone;
    }

    public static function money($price, $decimal_place = 0, $symbol_thousand = '.', $prefix = 'đ')
    {
        if ($price == 0) return 'Miễn Phí';
        return number_format($price, $decimal_place, '', $symbol_thousand) . ' ' . $prefix;
    }
    public static function moneyNumber($price, $decimal_place = 0, $symbol_thousand = '.', $prefix = 'đ')
    {
        return number_format($price, $decimal_place, '', $symbol_thousand) . ' ' . $prefix;
    }

    public static function array_udiff($arr1, $arr2)
    {
        $result = true;
        if (count(array_diff_key($arr1, $arr2)) == 0) {
            foreach ($arr1 as $key => $item) {
                if (is_array($item)) {
                    $result = UtilHelper::array_udiff($item, $arr2[$key]);
                } else {
                    if ($item != $arr2[$key])
                        $result = false;
                }
                if ($result == false) {
                    return $result;
                }
            }
        }
        return $result;
    }

    public static function arrayDiffList($lst,$arr){
        $result = true;
        if(count($lst)==0){
            return false;
        }
        foreach ($lst as $item){
            if (!UtilHelper::array_udiff($item, $arr)){
                $result = false;
                break;
            }
        }
        return $result;
    }
}