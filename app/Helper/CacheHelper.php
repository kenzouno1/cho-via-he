<?php
/**
 * Created by PhpStorm.
 * User: Dat Nguyen
 * Date: 9/12/2017
 * Time: 9:38 AM
 */

namespace App\Helper;


use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use function is_null;

class CacheHelper
{
    public static function load($key, $callback,$time=null)
    {
        if (Cache::has($key)) {
            return Cache::get($key);
        }
        $data = $callback();
        if (is_null($time)){
            Cache::forever($key, $data);
        }else{
            Cache::add($key, $data, $time);
        }
        return $data;
    }
}