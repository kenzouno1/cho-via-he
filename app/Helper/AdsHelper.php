<?php
/**
 * Created by PhpStorm.
 * User: Dat Nguyen
 * Date: 8/1/2017
 * Time: 8:46 AM
 */

namespace App\Helper;

use App\Ads;
use Illuminate\Support\Facades\Cookie;
use function compact;

class AdsHelper
{
    public static function updateFavorites($id)
    {
        if ($user = AuthHelper::current_user()) {
            return $user->updateFavorites($id);
        }
        return AdsHelper::update($id, '_favorites');
    }

    public static function getFavorites()
    {
        if ($user = AuthHelper::current_user()) {
            $favorites = $user->get_meta('_favorites', json_encode([]));
        } else {
            $favorites = Cookie::has('_favorites') ? Cookie::get('_favorites') : json_encode([]);
        }
        return json_decode($favorites, true);
    }

    public static function getAdsFavorites($max = 6)
    {
        $lstIds = AdsHelper::getFavorites();
        return AdsHelper::get($lstIds, $max);
    }


    public static function getViewed()
    {
        return Cookie::has('_viewed') ? json_decode(Cookie::get('_viewed'), true) : [];
    }

    public static function getAdsViewed($max = 6)
    {
        $lstIds = AdsHelper::getViewed();
        return AdsHelper::get($lstIds, $max);
    }

    public static function updateViewed($id)
    {
        return AdsHelper::update($id, '_viewed', false);
    }

    public static function updateSavedSearch($cate, $keywords, $location, $dis, $address)
    {
        $params = compact('cate', 'keywords', 'location', 'dis', 'address');
        if ($user = AuthHelper::current_user()) {
            return $user->updateSavedSearch($params);
        } else {
            $old = AdsHelper::getSavedSearch();
            if (!UtilHelper::array_udiff($old, $params)) ;
            {
                return AdsHelper::update($params, '_savedSearch', false, 12);
            }
            return $old;
        }
    }

    public static function getSavedSearch($max=6)
    {
        if ($user =  AuthHelper::current_user()) {
            return $user->getSavedSearch($max);
        } else {
            $arr= Cookie::has('_savedSearch') ? json_decode(Cookie::get('_savedSearch'), true) : [];
            return array_slice($arr,0,$max);
        }
    }

    public static function removeSavedSeach($index)
    {
        if ($user = AuthHelper::current_user()) {
            $user->removeSavedSeach($index);
        } else {
            AdsHelper::remove($index, '_savedSearch');
        }
    }

    public static function removeAllSavedSeach()
    {
        if ($user = AuthHelper::current_user()) {
            $user->removeAllSavedSeach();
        } else {
            AdsHelper::removeAll('_savedSearch');
        }
    }

    public static function get($lstIds, $max = 6)
    {
        $lst = [];
        if (count($lstIds) > 0) {
            $lst = Ads::whereIn('id', $lstIds)
                ->with(['author', 'category', 'boost'])
                ->isActive()
                ->orderByField('id', $lstIds)
                ->limit($max)->get();
        }
        return $lst;
    }

    public static function push($lst, $value, $max = 12)
    {
        if (count($lst) == $max) {
            array_shift($lst);
        }
        $lst[] = $value;
        return $lst;
    }

    public static function remove($index, $key)
    {
        if (Cookie::has($key)) {
            $lst = json_decode(Cookie::get($key), true);
            if (isset($lst[$index])) {
                unset($lst[$index]);
            }
            Cookie::queue($key, json_encode($lst), 60 * 24 * 30);
            return $lst;
        }
    }

    /**
     * @param $value
     * @param string $key
     * @param bool $toggle | option remove if $value exist
     * @param integer $max | max list size
     * @return array|mixed
     */
    public static function update($value, $key = '_favorites', $toggle = true, $max = 12)
    {
        if (Cookie::has($key)) {
            $lst = json_decode(Cookie::get($key), true);
        } else {
            $lst = [];
        }
        if (!in_array($value, $lst)) {
            $lst = AdsHelper::push($lst, $value, $max);
        } elseif ($toggle) {
            $index = array_search($value, $lst);
            unset($lst[$index]);
        }
        Cookie::queue($key, json_encode($lst), 60 * 24 * 30);
        return $lst;
    }

    public static function removeAll($key)
    {
        Cookie::unqueue($key);
    }

}