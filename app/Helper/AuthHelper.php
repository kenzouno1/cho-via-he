<?php
/**
 * Created by PhpStorm.
 * User: Dat Nguyen
 * Date: 7/29/2017
 * Time: 2:29 PM
 */

namespace App\Helper;


use App\Admins;
use App\User;
use Illuminate\Support\Facades\Auth;

class AuthHelper
{
    public static function currentPhone()
    {
        if ($user =self::current_user())
            return $user->phone;
        return '';
    }
    public static function currentEmail()
    {
        if ($user =self::current_user())
            return $user->email;
        return '';
    }

    public static function can($cap)
    {
        $user = AuthHelper::current_user();
        return $user->can($cap);
    }

    public static function current_user()
    {
        if (!Auth::guard('admin')->check() && !Auth::check()) {
            return false;
        }
        if (isBackend()) {
            return Auth::guard('admin')->user();
        }
        return Auth::user();
    }


    public static function getAvatar($admin = false, $user = null)
    {
        if ($admin) {
            if (is_null($user)) {
                $user = Auth::guard('admin')->user();
            } else {
                $user = Admins::where('id', $user)->first();
            }
        } else {
            if (is_null($user)) {
                $user = Auth::user();
            } else {
                $user = User::where('id', $user)->first();
            }
        }
        if ($user) {
            if ($user->avatar != '') {
                return $user->avatar;
            }
        }
        return asset('avatar/avatar-default.jpg');
    }


    public static function getLoginUrl()
    {
        $url = route('auth.login');
        if (\Request::has('callback')) {
            $url .= '?callback=' . \Request::get('callback');
        }
        return $url;
    }
}