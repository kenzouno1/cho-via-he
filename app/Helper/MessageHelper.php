<?php
/**
 * Created by PhpStorm.
 * User: Dat Nguyen
 * Date: 8/7/2017
 * Time: 5:02 PM
 */

namespace App\Helper;

use App\Chat\Thread;
use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;

class MessageHelper
{
    public static function sendMsg($ads,$msg)
    {
        $msg = StringHelper::removeBadWord($msg);
        $currentUser = AuthHelper::current_user();
        $thread = Thread::userAdsThread($ads->id)->first();
        if (!$thread) {
            //create new Message
            $thread = new Thread();
            $thread->ads_id = $ads->id;
            $thread->subject = 'Tin nhắn mới trong tin ' . $ads->id;
            $thread->save();
        }
        //create new msg
        $message = new Message();
        $message->thread_id = $thread->id;
        $message->user_id = $currentUser->id;
        $message->body = $msg;
        $message->save();
        //send msg to
        $participant = new Participant();
        $participant->thread_id = $thread->id;
        $participant->user_id = $currentUser->id;
        $participant->last_read = new Carbon();
        $thread->addParticipant($ads->user_id);
        $participant->save();
    }
}