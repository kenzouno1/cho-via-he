<?php
/**
 * Created by PhpStorm.
 * User: duc
 * Date: 7/26/2017
 * Time: 9:48 AM
 */

namespace App\Helper;


use function collect;
use function dump;
use function is_null;

class NavHelper
{
    public static function searchCateNav($cate = null)
    {
        $args = [
            'root' => array(
                'title' => 'Tất Cả',
                'active' => false,
                'url' => route('search.list'),
            ),
        ];
        if (is_null($cate) || !$cate) {
            $root = CategoryHelper::allRootCategories();
            $args['root']['active'] = true;
            foreach ($root as $cate) {
                $args[] = array(
                    'title' => $cate->name,
                    'active' => false,
                    'url' => route('search.list', ['cate' => $cate->slug]),
                );
            }
        } else {
            $parent = $cate->getParent();
            $childs = $cate->getChildrent();
            $currentArgs = array(
                'title' => $cate->name,
                'active' => true,
                'url' => route('search.list', ['cate' => $cate->slug]),
            );
            if ($cate->hasChild()) {
                $currentArgs['child'] = array();
                foreach ($childs as $child) {
                    if ($child->hasChild() || $child->ads->count() > 0) {
                        $currentArgs['child'][] = array(
                            'title' => $child->name,
                            'active' => false,
                            'url' => route('search.list', ['cate' => $child->slug]),
                        );
                    }
                }
            }
            if ($cate->hasParent()) {
                $args[] = array(
                    'title' => $parent->name,
                    'active' => false,
                    'url' => route('search.list', ['cate' => $parent->slug]),
                    'child' => [$currentArgs],
                );
            } else {
                $args[] = $currentArgs;
            }
        }
        return $args;
    }

    public static function renderListCate()
    {
        $lstRoot = CategoryHelper::allRootCategories();
        $nav = new NavHelper();
        return $nav->renderLiCate($lstRoot);
    }

    protected function renderLiCate($lst, $showIcon = true)
    {
        $html = '';
        foreach ($lst as $cate) {
            $classes = 'has-child-dropdown';
            if ($cate->image != '' && $showIcon) {
                $classes .= ' has-icon';
            }
            $html .= ' <li class="' . $classes . '" data-value="' . $cate->slug . '">';
            if ($showIcon) {
                $html .= '<i class="' . $cate->image . '" ></i >';
            }
            $html .= '<a href="' . route('search.list', ['cate' => $cate->slug]) . '" class="text">
			                   ' . $cate->name . '
			                </a>';
            if ($cate->hasChild()) {
                $html .= '<span class="btn-toggle">
			                  <i class="ion-ios-arrow-down"></i>
			                </span>';
                $html .= '<ul>';
                $html .= $this->renderLiCate($cate->getChildrent(), false);
                $html .= '</ul>';
            }
            $html .= '</li>';
        }
        return $html;
    }

    public static function breadCrumbGenerator($lstCate, $ads)
    {
        $lstCate = $lstCate->sortBy('id')->take(-2);
        $html = "<ul class='breadcrumbs'>";
        $html .= "<li><a href='" . route('home') . "' title='Trang Chủ'>Trang Chủ</a></li>";
        foreach ($lstCate as $item) {
            $html .= "<li><a href='" . UrlHelper::getUrl([], route('search.list', ['cate' => $item->slug])) . "' title='" . $item->name . "'>" . $item->name . "</a></li>";
        }
        $html .= '<li class="active">' . $ads->title . '</li>';
        $html .= "</ul>";
        return $html;
    }

    public static function loadCategoryNav($id)
    {
        return CacheHelper::load('nav_' . $id, function () use ($id) {
            $lst = collect();
            $lstChild = CategoryHelper::getChildById($id);
            if ($lstChild->isNotEmpty()) {
                foreach ($lstChild as $cate) {
                    $lst->push([
                        'slug' => $cate->slug,
                        'count' => $cate->countNested(),
                        'thumbnail' => $cate->thumbnail,
                        'name' => $cate->name,
                    ]);
                }
            }
            return $lst;
        });
    }
}