<?php
/**
 * Created by PhpStorm.
 * User: Dat Nguyen
 * Date: 8/18/2017
 * Time: 1:48 PM
 */

namespace App\Payment\BaoKim\Pro;


use Barryvdh\Debugbar\Twig\Extension\Dump;
use function compact;
use function config;
use function view;

class BaoKimPro
{
    protected $apiUser;
    protected $apiPwd;
    protected $privateKey;
    protected $authType = 'merchant';
    protected $email;
    protected $url;
    protected $rest;
    protected $urlSuccess;
    protected $urlFailed;
    protected $urlCancel;
    protected $baseUrl;
    protected $payUrl;

    function __construct()
    {
        $this->apiUser = config('baokim.pro.user');
        $this->apiPwd = config('baokim.pro.pwd');
        $this->privateKey = config('baokim.pro.private_key');
        $this->email = config('baokim.pro.email');
        $this->url = config('baokim.pro.url');
        $this->payUrl = config('baokim.pro.url.pay_by_card');
        $this->baseUrl = route('home');
    }

    public function getProfile()
    {
        $param = array(
            'business' => $this->email,
        );
        $rest = new CallRestful();
        $call_API = $rest->call_API("GET", $param, $this->url['seller']);
        if (is_array($call_API)) {
            if (isset($call_API['error'])) {
                echo "<strong style='color:red'>call_API" . json_encode($call_API['error']) . "- code:" . $call_API['status'] . "</strong> - " . "System error. Please contact to administrator";
                die;
            }
        }

        $seller_info = json_decode($call_API, true);
        if (!empty($seller_info['error'])) {
            echo "<strong style='color:red'>seller_info" . json_encode($seller_info['error']) . "</strong> - " . "System error. Please contact to administrator";
            die;
        }

        $banks = $seller_info['bank_payment_methods'];

        return $banks;
    }

    public function getBankInternetBankingImages($banks)
    {
        return $this->generateBankImage($banks, config('baokim.pro.method.internet_banking'));
    }

    public function getBankCardImages($banks)
    {
        return $this->generateBankImage($banks, config('baokim.pro.method.local_card'));
    }

    public function getCreditCardImages($banks)
    {
        return $this->generateBankImage($banks, config('baokim.pro.method.credit_card'));
    }

    public function generateBankImage($banks, $payment_method_type)
    {
        $html = '';
        $count = 0;
        $banksLength = count($banks);
        foreach ($banks as $key => $bank) {
            if ($bank['payment_method_type'] == $payment_method_type) {
                $html .= view()->make('pages.payment.parts.bank', compact('bank'));
                $count++;
            }
            if ($count > 9 && $key == $banksLength - 1) {
                $html .= view()->make('pages.payment.parts.bank-last');
            }
        }
        return $html;
    }

    public function pay_by_card($data, $package, $user)
    {

        $order_id = time();
        $total_amount = str_replace('.', '', $data['total_amount']);

        $params['business'] = strval($this->email);
        $params['bank_payment_method_id'] = intval($data['bank_payment_method_id']);
        $params['transaction_mode_id'] = '1'; // 2- trực tiếp
        $params['escrow_timeout'] = 3;

        $params['order_id'] = $order_id;
        $params['total_amount'] = $total_amount;
        $params['shipping_fee'] = '0';
        $params['tax_fee'] = '0';
        $params['currency_code'] = 'VND';

        $params['url_success'] = route('payment.success.callback',compact('package'));
        $params['url_cancel'] =  route('payment.cancel',compact('package'));
        $params['url_detail'] = '';

        $params['order_description'] = $user->phone . ' thanh toán gói ' . $package->name . ' với mã đơn hàng ' . $order_id;
        $params['payer_name'] = $user->name;
        $params['payer_email'] = $user->email ? $user->email : 'tranhaiyen.skv@gmail.com';
        $params['payer_phone_no'] = $user->phone;
        $params['payer_address'] = $user->address ? $user->address : '';

        $rest = new CallRestful();
        $result = json_decode($rest->call_API("POST", $params,$this->payUrl), true);
        return $result;
    }

}