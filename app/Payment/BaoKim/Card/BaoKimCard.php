<?php
/**
 * Created by PhpStorm.
 * User: Dat Nguyen
 * Date: 7/13/2017
 * Time: 3:22 PM
 */

namespace App\Payment\BaoKim\Card;


class BaoKimCard
{
    protected $merchant_id;
    protected $api_username;
    protected $api_password;
    protected $secure_code;

    protected $arrayPost;


    public static function pay($mang, $pin, $seri)
    {
        $merchant_id = config('baokim.id');
        $api_username = config('baokim.mobile.user');
        $api_password = config('baokim.mobile.pwd');
        $secure_code = config('baokim.secret');
        $url = 'https://www.baokim.vn/the-cao/restFul/send';
        $coreApiUsr = 'merchant_19002';
        $coreApipwd = '19002mQ2L8ifR11axUuCN9PMqJrlAHFS04o';
        $arrayPost = [
            'card_id'        => $mang,
            'pin_field'      => $pin,
            'seri_field'     => $seri,
            'algo_mode'      => 'hmac',
            'merchant_id'    => $merchant_id,
            'api_username'   => $api_username,
            'api_password'   => $api_password,
            'transaction_id' => 'cvh_' . time(),
        ];
        ksort($arrayPost);
        $data_sign = hash_hmac('SHA1', implode('', $arrayPost), $secure_code);
        $arrayPost['data_sign'] = $data_sign;
        //curl post to bao kim api
        $curl = curl_init($url);
        curl_setopt_array($curl, array(
            CURLOPT_POST           => true,
            CURLOPT_HEADER         => false,
            CURLINFO_HEADER_OUT    => true,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTPAUTH       => CURLAUTH_DIGEST | CURLAUTH_BASIC,
            CURLOPT_USERPWD        => $coreApiUsr . ':' . $coreApipwd,
            CURLOPT_POSTFIELDS     => http_build_query($arrayPost)
        ));
        $result = curl_exec($curl);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        return ['status' => $status, 'data' => $result];
    }
}