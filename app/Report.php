<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Report
 *
 * @property int $id
 * @property string $type
 * @property string $reason
 * @property string $ip
 * @property string|null $phone
 * @property string|null $email
 * @property string|null $message
 * @property int $target_id
 * @property string $target_type
 * @property string $status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereTargetId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereTargetType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Ads $ads
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AbstractModel orderByField($field, $values)
 */
class Report extends AbstractModel
{
    protected $table = 'report';
    protected $fillable = [
        'id', 'status', 'reason', 'ip', 'status', 'target'
    ];

    public function ads()
    {
        return $this->belongsTo('App\Ads', 'target_id');
    }
    public function user()
    {
        return $this->belongsTo('App\User', 'target_id');
    }
}
