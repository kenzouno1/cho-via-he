<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * App\Sms
 *
 * @property int $id
 * @property string $code
 * @property string $expire_at
 * @property string $type
 * @property int $user_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sms whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sms whereExpireAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sms whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sms whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sms whereUserId($value)
 * @mixin \Eloquent
 * @property int $status
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sms whereStatus($value)
 */
class Sms extends Model
{
    protected $table = 'sms_code';
    protected $fillable = ['code', 'expire_at', 'type', 'status'];
    protected $dates = ['expire_at'];
    public $timestamps = false;

    protected static function boot()
    {
        self::creating(function ($model) {
            $model->expire_at = Carbon::parse('+2 hours');
            $model->code = random_int(1000, 9999);
            DB::table('sms_code')
                ->where('user_id', '=', $model->user_id)
                ->where('type', '=', $model->type)
                ->where('status', '=', $model->type)
                ->update(['status' => false]);
        });
    }
}
