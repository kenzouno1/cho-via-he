<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\AdsImages
 *
 * @property int $id
 * @property string $url
 * @property int $ads_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdsImages whereAdsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdsImages whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdsImages whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdsImages whereIndex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdsImages whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdsImages whereUrl($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AbstractModel orderByField($field, $values)
 */
class AdsImages extends AbstractModel
{
    protected $fillable = [
        'url', 'asd_id', 'index'
    ];
    protected $table = 'ads_images';

    public function getSrc($size)
    {
        $ext = pathinfo($this->url, PATHINFO_EXTENSION);
        $name = pathinfo($this->url, PATHINFO_FILENAME );
        if (in_array($size, array('small', 'medium'))) {
            return $name . '-' . $size . '.' . $ext;
        }
        return false;
    }
}
