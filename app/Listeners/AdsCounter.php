<?php

namespace App\Listeners;

use App\Events\AdsViewHandle;
use Illuminate\Session\Store;

class AdsCounter
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    protected $session;

    public function __construct(Store $session)
    {
        $this->session = $session;
    }
    /**
     * Handle the event.
     *
     * @param  AdsViewHandle $event
     * @return void
     */
    public function handle(AdsViewHandle $event)
    {
        $ads = $event->ads;
        if (!$this->isPostViewed($ads)) {
            $ads->increment('view_count');
            $this->storePost($ads);
        }
    }

    private function isPostViewed($ads)
    {
        $viewed = $this->session->get('viewed_posts', []);
        return array_key_exists($ads->id, $viewed);
    }

    private function storePost($ads)
    {
        $key = 'viewed_posts.' . $ads->id;
        $this->session->put($key, time());
    }
}
