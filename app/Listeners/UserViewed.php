<?php

namespace App\Listeners;

use App\Events\AdsViewHandle;
use App\Helper\AdsHelper;
use Illuminate\Session\Store;

class UserViewed
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    protected $session;

    public function __construct(Store $session)
    {
        $this->session = $session;
    }

    /**
     * Handle the event.
     *
     * @param  AdsViewHandle $event
     * @return void
     */
    public function handle(AdsViewHandle $event)
    {
        $ads = $event->ads;
        AdsHelper::updateViewed($ads->id);
    }
}