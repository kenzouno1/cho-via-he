<?php

namespace App;

use App\Logs\CustomActivityLogger;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\AbstractModel
 *
 * @mixin \Eloquent
 */
abstract class AbstractModel extends Model
{
    //auto log
    use LogsActivity;
    public $log = '';
    public $logName = '';
    protected static $logAttributes = [];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        self::$logAttributes = $this->fillable;
    }


    protected static function bootLogsActivity()
    {
        static::eventsToBeRecorded()->each(function ($eventName) {
            return static::$eventName(function (Model $model) use ($eventName) {
                if (!$model->shouldLogEvent($eventName)) {
                    return;
                }

                $description = $model->getDescriptionForEvent($eventName);
                $logName = $model->getLogNameToUse($eventName);

                if ($description == '') {
                    return;
                }

                app(CustomActivityLogger::class)
                    ->useLog($logName)
                    ->performedOn($model)
                    ->withProperties($model->attributeValuesToBeLogged($eventName))
                    ->log($description);
            });
        });
    }

    protected static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            if ($model->usesTimestamps()) {
                $date = date('Y-m-d H:i:s');
                $model->created_at = $date;
                $model->updated_at = $date;
            }
        });
        self::updating(function ($model) {
            if ($model->usesTimestamps()) {
                $date = date('Y-m-d H:i:s');
                $model->updated_at = $date;
            }
        });
    }

    public function getDescriptionForEvent(string $eventName): string
    {
        return $this->log != '' ? $this->log : $eventName;
    }

    public function getLogNameToUse(string $eventName = ''): string
    {
        return $this->logName != '' ? $this->logName : config('activitylog.default_log_name');
    }

    public function scopeOrderByField($query,$field,$values){
        $placeholders = implode(',',array_fill(0, count($values), '?'));
        $query->orderByRaw("field({$field},{$placeholders})", array_reverse($values));
    }
}