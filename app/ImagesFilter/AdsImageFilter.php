<?php
/**
 * Created by PhpStorm.
 * User: Dat Nguyen
 * Date: 7/8/2017
 * Time: 10:33 AM
 */

namespace App\ImagesFilter;

use Intervention\Image\Facades\Image;
use Intervention\Image\Filters\FilterInterface;
use function array_rand;
use function public_path;

class AdsImageFilter implements FilterInterface
{
//    private $folder = "public/uploads/";
    private $folderName;
    private $imageName;

    function __construct($folderName, $imageName)
    {
        $imageName = str_replace('.jpg', '', $imageName);
        $this->folderName = $folderName;
        $this->imageName = $imageName;
    }

    public function applyFilter(\Intervention\Image\Image $image)
    {
        $waterMarkUrl =  public_path('assets/img/default-image/watermark.png');
        $watermark = Image::make($waterMarkUrl);
        $path = $this->folderName . '/' . $this->imageName;
        $image->resize(600, null, function ($constraint) {
            $constraint->aspectRatio();
        })->insert($watermark, $this->getWaterMarkPosition(), 10, 10)
            ->save($path . '-medium' . '.jpg');
        $image->fit(263, 175, function ($constraint) {
            $constraint->upsize();
        })->save($path . '-small' . '.jpg');
        $image->fit(130, 175)
            ->save($path . '-vertical' . '.jpg');
        return $image;
    }

    public function getWaterMarkPosition()
    {
        $positon = [
            'top-left',
            'top-right',
            'botom-left',
            'bottom-right',
        ];
        return $positon[array_rand($positon)];
    }
}
