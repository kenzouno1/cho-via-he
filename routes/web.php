<?php
Route::name('support-center')->get('support-center', function () {
    return view('support.support-center');
});
Route::name('support')->get('support', function () {
    return view('support.support-list');
});
Route::name('support-detail')->get('support-detail', function () {
    return view('support.support-detail');
});
Route::name('an-toan-mua-ban')->get('an-toan-mua-ban', function () {
    return view('support.antoanmuaban');
});
Route::name('contact')->get('contact', function () {
    return view('support.support-contact');
});

//frontend

use App\Ads;
use App\Category;
use App\Helper\CacheHelper;
use App\PaymentLogs;
use App\User;

Route::name('debug')->get('/debug/{a?}', function () {
    dump(\App\Helper\BoostHelper::getLstHighlight());
});

Route::name('demo.login')->post('/demo/login', function (\Illuminate\Http\Request $request) {
    if ($request->user == 'choviahe' && $request->pwd == 'redsand@123') {
        Cookie::queue('isDemo', true, 60 * 24 * 30);
        return redirect()->route('home');
    } else {
        return view('maintain');
    }
});

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::group(['middleware' => 'user.demo'], function () {

    Route::group(['prefix' => 'thong-tin-nguoi-ban'], function () {
        Route::name('account-seller')->get('/user-{id}/', 'ProfileController@viewProfileSelling');
        Route::name('account-seller.lst.sold')->get('/user-{id}/da-ban', 'ProfileController@viewProfileSold');
        Route::name('account-seller.report')->post('/bao-cao-thanh-vien/', 'Report\ReportMemberController@report');
    });
    Route::group(['prefix' => 'o-to'], function () {
        Route::name('landing-page.car')->get('/', 'LandingPage\LandingCarController@getLandingPageCar');
        Route::name('landing-page.car.model')->post('/dong-xe', 'LandingPage\LandingCarController@getModel');
        Route::name('landing.car.search')->post('/tim-kiem', 'LandingPage\LandingCarController@searchCar');
    });
    Route::group(['prefix' => 'bat-dong-san'], function () {
        Route::name('landing-page.bds')->get('/', 'LandingPage\LandingLandController@getLandingPageBds');
        Route::name('landing-page.bds.timkiem')->post('/tim-kiem', 'LandingPage\LandingLandController@searchLand');
        Route::name('landing-page.bds.lstPrice')->post('/list-price', 'LandingPage\LandingLandController@getListPrice');
    });
    Route::group(['prefix' => 'viec-lam'], function () {
        Route::name('landing-page.job')->get('/', 'LandingPage\LandingJobController@getLandingPageJob');
        Route::name('landing-page.job.timkiem')->post('/tim-kiem', 'LandingPage\LandingJobController@searchJob');
    });
    Route::name('home')->get('/', 'Pages\HomeController@getViewHome');
// user and account
    Route::name('auth.social')->get('/auth/{provider}', 'Auth\AuthController@redirectToProvider');
    Route::get('/auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');
    Route::name('auth.login')->get('/dang-nhap', 'Auth\LoginController@doGet');
    Route::name('auth.login')->post('/dang-nhap', 'Auth\LoginController@doLogin');
    Route::name('auth.register')->get('/dang-ky', 'Auth\RegisterController@doGet');
    Route::name('auth.register')->post('/dang-ky', 'Auth\RegisterController@doRegister');
    Route::name('auth.logout')->get('/thoat', 'Auth\LoginController@logout');
    Route::name('auth.active')->get('/kich-hoat/{phone?}', 'Auth\ActiveController@getViewActive');
    Route::name('auth.active')->post('/kich-hoat/{phone?}', 'Auth\ActiveController@activeAccountProcess');
    Route::name('auth.submit-phone')->post('/nhap-so-dien-thoai', 'Auth\SubmitPhoneController@submitPhone');
    Route::name('auth.submit-phone')->get('/nhap-so-dien-thoai', 'Auth\SubmitPhoneController@getViewSubmitPhone');
    Route::name('auth.forgotpwd')->get('/quen-mat-khau', 'Auth\ResetPasswordController@getViewForgotPwd');
    Route::name('auth.forgotpwd')->post('/quen-mat-khau', 'Auth\ResetPasswordController@forgotPassword');
    Route::name('auth.submit.forgot.pwd')->get('/xac-nhan/{phone}', 'Auth\ResetPasswordController@getViewSubmitForgot');
    Route::name('auth.submit.forgot.pwd')->post('/xac-nhan/{phone}', 'Auth\ResetPasswordController@submitForgot');

    Route::name('saved_search')->post('/luu-tim-kiem/', 'FavoritesViewedController@addToSavedSearch');
    Route::name('saved_search.remove')->get('/da-tim-kiem/xoa/{index}', 'FavoritesViewedController@removeItemSavedSearch');
    Route::name('saved_search.remove.all')->get('/da-tim-kiem/xoa-tat-ca', 'FavoritesViewedController@removeAllSavedSearch');
    Route::name('favorite.add')->post('/quan-tam', 'FavoritesViewedController@addToFavorite');
    Route::name('favorite.get.html')->get('/quan-tam', 'FavoritesViewedController@getFavoritesHtml');
    Route::name('upload.upload')->post('upload', 'UploadController@upload');
    Route::name('search.list')->get('/tim-kiem/{cate?}', 'SearchController@getViewNewestAds');
    Route::name('search.autocomplete')->get('/autocomplete', 'FormSearchController@getTitle');
    Route::name('search.submit')->post('/submit', 'FormSearchController@submit');
    Route::name('ads.phone')->post('/xem-dien-thoai', 'AdsDetailsController@getPhoneAds');
    Route::name('report.ads')->post('/bao-cao/', 'Report\ReportAdsController@report');

    Route::group(['middleware' => 'user.auth'], function () {
        Route::group(['prefix' => 'thanh-toan'], function () {
            Route::name('payment.step1')->get('chon-goi-nap', 'Payment\BaoKimController@getViewPaymentStep1');
            Route::name('payment.step2')->get('/goi-{package}/chon-cach-nap/', 'Payment\BaoKimController@getViewPaymentStep2');
            Route::name('payment.step2.process')->post('/goi-{package}/chon-cach-nap/', 'Payment\BaoKimController@paymentStep2Process');
            Route::name('payment.mobile')->get('/goi-{package}/the-{mang}', 'Payment\BaoKimController@viewPaymentMobileCard');
            Route::name('payment.mobile')->post('/goi-{package}/the-{mang}', 'Payment\BaoKimController@paymentMobileCardProcess');
            Route::name('payment.success.callback')->get('/nap-the/goi-{package}/thanh-cong/process', 'Payment\BaoKimController@paymentSuccessCallback');
            Route::name('payment.success')->get('/nap-the/thanh-cong', 'Payment\BaoKimController@paymentSuccess');
            Route::name('payment.cancel')->get('/nap-the/goi-{package}/that-bai', 'Payment\BaoKimController@cancel');
        });
        Route::name('auth.forgot.changepwd')->get('/doi-mat-khau', 'Auth\ResetPasswordController@getViewChangePass');
        Route::name('auth.forgot.changepwd')->post('/doi-mat-khau', 'Auth\ResetPasswordController@changePass');
        Route::group(['prefix' => 'dang-tin'], function () {
            Route::name('ads.new.select_cate')->get('/chon-danh-muc', 'Ads\NewAdsController@getViewSelectCate');
            Route::name('ads.new')->get('/danh-muc/{slug}', 'Ads\NewAdsController@getViewNewAds');
            Route::name('ads.create')->post('/danh-muc/{slug}', 'Ads\NewAdsController@newAds');
            Route::name('ads.ajaxCheckPrice')->post('/kiem-tra-tai-khoan', 'Ads\AdsController@ajaxCheckPrice');
        });
        Route::group(['prefix' => 'tai-khoan'], function () {
            Route::name('member.profile')->get('thong-tin-ca-nhan', 'Member\MemberProfileController@viewProfile');
            Route::name('member.editprofile')->post('thong-tin-ca-nhan', 'Member\MemberProfileController@changeProfile');
            Route::name('member.change-avatar')->post('sua-anh-dai-dien', 'Member\MemberProfileController@changeAvatar');
            Route::name('member.change-pass-profile')->post('doi-mat-khau', 'Member\MemberProfileController@changePass');
            Route::name('member.saved-searches')->get('da-tim-kiem', 'FavoritesViewedController@viewSavedSearches');
            Route::name('member.favorited')->get('/tin-quan-tam', 'FavoritesViewedController@viewFavorited');
            Route::name('member.viewed')->get('/tin-da-xem', 'FavoritesViewedController@viewViewed');
            Route::name('member.hidden-post')->post('an-tin', 'HiddenPostController@hiddenPost');
            Route::group(['prefix' => '/quan-li-bai-dang'], function () {
                Route::name('member.manage-post')->get('/', 'Member\ManagerAds@viewAllPost');
                Route::name('member.manage-post.edit')->get('/chinh-sua/{id}', 'Ads\EditAdsController@getViewEditAds');
                Route::name('member.manage-post.edit')->post('/chinh-sua/{id}', 'Ads\EditAdsController@EditAds');
                Route::name('member.manage-post.active')->get('/hien-tai', 'Member\ManagerAds@viewActivePost');
                Route::name('member.manage-post.deactive')->get('/cho-xu-ly', 'Member\ManagerAds@viewWaitPost');
            });
            Route::group(['prefix' => 'tin-nhan'], function () {
                Route::name('member.message')->get('/', 'Message\MemberMessageController@getViewMessage');
                Route::name('member.message.details')->get('/chi-tiet/{slug}', 'Message\MemberMessageController@getViewMessageDetails');
                Route::name('member.message.send')->post('/gui', 'Message\MemberMessageController@sendMsg');
                Route::name('member.message.delete')->get('/xoa-tin/{slug}', 'Message\MemberMessageController@remove');
            });
        });
    });
    //cho xuong dưới cùng để tránh lỗi 404 các trang ko có prefix
    Route::name('ads.details')->get('/{slug}', 'AdsDetailsController@getViewDetails')->middleware('ads.view.filter');
});