<?php
/**
 * Created by PhpStorm.
 * User: Dat Nguyen
 * Date: 8/22/2017
 * Time: 3:38 PM
 */
Route::name('admin.logout')->get('/' . config('app.admin_prefix') . '/logout', 'Auth\AdminController@logout');
Route::group(['middleware' => 'admin.auth'], function () {
    Route::name('admin.login')->get('/' . config('app.admin_prefix') . '/login', 'Auth\AdminController@doGet');
    Route::name('admin.login')->post('/' . config('app.admin_prefix') . '/login', 'Auth\AdminController@doLogin');
});
Route::group(['prefix' => config('app.admin_prefix'), 'middleware' => 'admin'], function () {
    Route::group(['prefix' => 'dashboard'], function () {
        Route::name('admin.dashboard')->get('/', 'backend\AdminStatisticsController@viewDashboard');
        Route::name('admin.dashboard.count')->get('/count', 'backend\AdminStatisticsController@getCount');
    });
    Route::group(['prefix' => 'category'], function () {
        Route::name('admin.category.list')->get('/list', 'backend\AdminCategoryController@viewListCate')->middleware('admin:category.view');
        Route::name('admin.category.list')->post('/list', 'backend\AdminCategoryController@listCateAjax')->middleware('admin:category.view');
        Route::name('admin.category.new')->get('/new', 'backend\AdminCategoryController@viewNewCate')->middleware('admin:category.create');
        Route::name('admin.category.new')->post('/new', 'backend\AdminCategoryController@newCate')->middleware('admin:category.create');
        Route::name('admin.category.del')->post('/del', 'backend\AdminCategoryController@delCate')->middleware('admin:category.delete');
        Route::name('admin.category.edit')->get('/edit/{id}', 'backend\AdminCategoryController@viewEditCate')->middleware('admin:category.edit');
        Route::name('admin.category.edit')->post('/edit/{id}', 'backend\AdminCategoryController@editCate')->middleware('admin:category.edit');
        Route::name('admin.category.remove_selected')->get('/remove-selected', 'backend\AdminCategoryController@removeSelected')->middleware('admin:category.delete');
    });
    Route::group(['prefix' => 'employee'], function () {
        Route::name('admin.employee.list')->get('/list', 'backend\AdminEmployeeController@viewListEmployee')->middleware('admin:employee.view');
        Route::name('admin.employee.list')->post('/list', 'backend\AdminEmployeeController@listEmployeeAjax')->middleware('admin:employee.view');
        Route::name('admin.employee.new')->get('/new', 'backend\AdminEmployeeController@viewNewEmployee')->middleware('admin:employee.create');
        Route::name('admin.employee.new')->post('/new', 'backend\AdminEmployeeController@newEmployee')->middleware('admin:employee.create');
        Route::name('admin.employee.remove_selected')->get('/remove-selected', 'backend\AdminEmployeeController@removeSelected')->middleware('admin:employee.delete');
        Route::name('admin.employee.del')->post('/del', 'backend\AdminEmployeeController@delEmployee')->middleware('admin:employee.delete');
        Route::name('admin.employee.edit')->get('/edit/{id}', 'backend\AdminEmployeeController@viewEditEmployee')->middleware('admin:employee.edit');
        Route::name('admin.employee.edit.profile')->post('/edit/profile/{id}', 'backend\AdminEmployeeController@editProfile')->middleware('admin:employee.edit');
        Route::name('admin.employee.edit.password')->post('/edit/password/{id}', 'backend\AdminEmployeeController@editPwd')->middleware('admin:employee.edit');
        Route::name('admin.employee.edit.avatar')->post('/edit/avatar/{id}', 'backend\AdminEmployeeController@editAvatar')->middleware('admin:employee.edit');
        Route::name('admin.employee.edit.role')->post('/edit/role/{id}', 'backend\AdminEmployeeController@editRoles')->middleware('admin:employee.edit');
    });
    Route::group(['prefix' => 'roles'], function () {
        Route::name('admin.roles.list')->get('/list', 'backend\AdminRolesController@viewListRoles')->middleware('admin:roles.view');
        Route::name('admin.roles.list')->post('/list', 'backend\AdminRolesController@listRolesAjax')->middleware('admin:roles.view');
        Route::name('admin.roles.new')->get('/new', 'backend\AdminRolesController@viewNewRole')->middleware('admin:roles.create');
        Route::name('admin.roles.new')->post('/new', 'backend\AdminRolesController@newRole')->middleware('admin:roles.create');
        Route::name('admin.roles.del')->post('/del', 'backend\AdminRolesController@delRole')->middleware('admin:roles.delete');
        Route::name('admin.roles.edit')->get('/edit/{id}', 'backend\AdminRolesController@viewEditRole')->middleware('admin:roles.edit');
        Route::name('admin.roles.edit')->post('/edit/{id}', 'backend\AdminRolesController@editRole')->middleware('admin:roles.edit');
    });
    Route::group(['prefix' => 'member'], function () {
        Route::name('admin.member.list')->get('/list', 'backend\AdminMemberController@viewListMember')->middleware('admin:member.view');
        Route::name('admin.member.list')->post('/list', 'backend\AdminMemberController@listMemberAjax')->middleware('admin:member.view');
        Route::name('admin.member.block')->post('/block', 'backend\AdminMemberController@blockMember')->middleware('admin:member.edit');
        Route::name('admin.member.unblock')->post('/unblock', 'backend\AdminMemberController@unblockMember')->middleware('admin:member.edit');
        Route::name('admin.member.active')->post('/active', 'backend\AdminMemberController@activeMember')->middleware('admin:member.edit');
        Route::name('admin.member.new')->get('/new', 'backend\AdminMemberController@viewNewMember')->middleware('admin:member.create');
        Route::name('admin.member.new')->post('/new', 'backend\AdminMemberController@newMember')->middleware('admin:member.create');
        Route::name('admin.member.remove_selected')->get('/remove-selected', 'backend\AdminMemberController@removeSelected')->middleware('admin:member.delete');
        Route::name('admin.member.del')->post('/del', 'backend\AdminMemberController@delMember')->middleware('admin:member.delete');
        Route::name('admin.member.edit')->get('/edit/{id}', 'backend\AdminMemberController@viewEditMember')->middleware('admin:member.edit');
        Route::name('admin.member.edit')->post('/edit/{id}', 'backend\AdminMemberController@viewEditMember')->middleware('admin:member.edit');
        Route::name('admin.member.edit.profile')->post('/edit/profile/{id}', 'backend\AdminMemberController@editMember')->middleware('admin:member.edit');
    });
    Route::group(['prefix' => 'package'], function () {
        Route::name('admin.package.list')->get('/list', 'backend\AdminBoostController@viewListPackage');
        Route::name('admin.package.list')->post('/list', 'backend\AdminBoostController@listPackageAjax')->middleware('admin:member.view');
        Route::name('admin.package.new')->get('/new', 'backend\AdminBoostController@viewNewPackage')->middleware('admin:member.create');
        Route::name('admin.package.new')->post('/new', 'backend\AdminBoostController@newPackage')->middleware('admin:member.create');
        Route::name('admin.package.remove_selected')->get('/remove-selected', 'backend\AdminBoostController@removeSelected')->middleware('admin:member.delete');
        Route::name('admin.package.del')->post('/del', 'backend\AdminBoostController@removePackage')->middleware('admin:member.delete');
        Route::name('admin.package.edit')->get('/edit/{id}', 'backend\AdminBoostController@viewEditPackage')->middleware('admin:member.edit');
        Route::name('admin.package.edit')->post('/edit/{id}', 'backend\AdminBoostController@editPackage')->middleware('admin:member.edit');
    });
    Route::group(['prefix' => 'posts'], function () {
        Route::name('admin.ads.listActive')->get('/list/active', 'backend\AdminAdsController@viewlistAdsActive');
        Route::name('admin.ads.listActive')->post('/list/active', 'backend\AdminAdsController@listActiveAjax')->middleware('admin:ads.view');
        Route::name('admin.ads.listDeactive')->get('/list/deactive', 'backend\AdminAdsController@viewlistAdsDeactive');
        Route::name('admin.ads.listDeactive')->post('/list/deactive', 'backend\AdminAdsController@listDeactiveAjax')->middleware('admin:ads.view');
        Route::name('admin.ads.updateStatus')->post('/list', 'backend\AdminAdsController@updateStatus');
    });
    Route::group(['prefix' => 'payment'], function () {
        Route::name('admin.payment.package.list')->get('/goi-nap', 'Payment\PaymentPackageController@getViewList');
        Route::name('admin.payment.package.list')->post('/goi-nap', 'Payment\PaymentPackageController@getViewListAjax');
        Route::name('admin.payment.package.new')->get('/them-goi-nap', 'Payment\PaymentPackageController@getViewNew');
        Route::name('admin.payment.package.new')->post('/them-goi-nap', 'Payment\PaymentPackageController@newPackage');
        Route::name('admin.payment.package.edit')->get('/sua-goi-nap/{id}', 'Payment\PaymentPackageController@getViewEdit');
        Route::name('admin.payment.package.edit')->post('/sua-goi-nap/{id}', 'Payment\PaymentPackageController@editPackage');
        Route::name('admin.payment.package.del')->post('/xoa-goi-nap', 'Payment\PaymentPackageController@deletePackage');
    });
    Route::group(['prefix' => 'report'], function () {
        Route::name('admin.report.ads')->get('/bao-cao-tin', 'backend\report\AdminReportAdsController@reportAds');
        Route::name('admin.report.ads')->post('/bao-cao-tin', 'backend\report\AdminReportAdsController@reportAdsAjax');
        Route::name('admin.report.hiddenAds')->post('/an-tin', 'backend\report\AdminReportAdsController@hiddenAds');
        Route::name('admin.report.deleteAds')->post('/xoa-tin', 'backend\report\AdminReportAdsController@deleteAds');
        Route::name('admin.report.deleteReport')->post('/xoa-bao-cao', 'backend\report\AdminReportAdsController@deleteReport');
        Route::name('admin.report.removeSelected')->post('/remove-selected-report', 'backend\report\AdminReportAdsController@removeSelected');
        Route::name('admin.report.member')->get('/bao-cao-thanh-vien', 'backend\report\AdminReportMemberController@reportMember');
        Route::name('admin.report.member')->post('/bao-cao-thanh-vien', 'backend\report\AdminReportMemberController@reportMemberAjax');
        Route::name('admin.report.blockMember')->post('/xoa-thanh-vien', 'backend\report\AdminReportMemberController@blockMember');
    });
    Route::group(['prefix' => 'logs'], function () {
        Route::name('admin.logs.payment')->get('/lich-su-thanh-toan', 'backend\logs\AdminLogsPaymentController@logsPayment');
        Route::name('admin.logs.payment')->post('/lich-su-thanh-toan', 'backend\logs\AdminLogsPaymentController@logsPaymentAjax');
    });
});