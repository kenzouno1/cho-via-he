var lstMarker = [];
var map;
var latlgn = map_data.center.split(',');
var mapObject = {
    zoom: JSON.parse(map_data.zoom),
    center: new google.maps.LatLng(latlgn[0], latlgn[1]),
    streetViewControl: false,
    mapTypeControl: false,
    clickableIcons: false
};
map = new google.maps.Map(document.getElementById('gmap'), mapObject);
//if map loaded success load list marker
google.maps.event.addListenerOnce(map, 'idle', function () {
    addMarker();
});

function addMarker() {
    for (var i = 0; i < lstAdd.length; i++) {
        var obj = lstAdd[i];
        var latlgn = obj.center.split(',');
        var marker = new CustomMarker(
            new google.maps.LatLng(latlgn[0], latlgn[1]),
            map, obj
        );

        lstMarker.push(marker);
    }
}


function CustomMarker(latlng, map, args) {
    this.latlng = latlng;
    this.args = args;
    this.setMap(map);
}

CustomMarker.prototype = new google.maps.OverlayView();
CustomMarker.prototype.draw = function () {
    var self = this;
    var obj = this.args;
    var tag = this.tag;
    if (!tag) {
        tag = this.tag = document.createElement('a');
        tag.className = 'room-price-pin';
        tag.setAttribute('title', obj.title);
        tag.setAttribute('href', '#post-' + obj.id);
        tag.setAttribute('data-id', 'post-' + obj.id);
        tag.setAttribute('data-toggle', 'tooltip');
        tag.innerHTML =
            '<div class="wrap-marker">' +
            '<span class="marker-number">' + obj.number + '</span>' +
            '<div class="info">' +
            '<h5>' + obj.title + '</h5>' +
            '<div class="info-description">' + obj.description + '</div>' +
            '</div>' +
            '</div>';
        if (typeof(self.args.marker_id) !== 'undefined') {
            tag.dataset.marker_id = self.args.marker_id;
        }


        google.maps.event.addDomListener(tag, "click", function (event) {
            google.maps.event.trigger(self, "click");
            event.preventDefault();
            var target = $(this).data('id');
            if (!$(this).find('.info').hasClass('active')) {
                $('.wrap-marker .info.active').removeClass('active');
                $(this).find('.info').addClass('active');
            }
            $('.search-content .post').not('#' + target).css('opacity', '0.4');
            $('#' + target).css('opacity', 1);
            var scrollTop = $('#' + target).offset().top;
            $("html, body").stop().animate({scrollTop: scrollTop}, 800);
            return false;
        });
        var panes = this.getPanes();
        panes.overlayImage.appendChild(tag);
    }

    var point = this.getProjection().fromLatLngToDivPixel(this.latlng);

    if (point) {
        tag.style.left = point.x + 'px';
        tag.style.top = point.y + 'px';
    }
};

CustomMarker.prototype.remove = function () {
    if (this.tag) {
        this.tag.parentNode.removeChild(this.tag);
        this.tag = null;
        this.setMap(null);
    }
};

CustomMarker.prototype.getPosition = function () {
    return this.latlng;
};

function AddCurrentMarker(map) {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            map.setCenter(pos);
        });
    }
}

$(document).click(function (event) {
    if (!$(event.target).is('.room-price-pin') && !$(event.target).is('.room-price-pin *')) {
        $('.search-content .post').css('opacity', 1);
    }
});