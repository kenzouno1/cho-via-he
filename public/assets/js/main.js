(function ($) {
    'use strict';
    $(function ($) {
        /* ==========================================================================
           Menu header
           ========================================================================== */
        $('.header-topbar .icon-toggle').click(function (event) {
            $('body').toggleClass('stop-scrolling');
        });
        $('#modal-header-topbar-right .close').click(function (event) {
            $('body').removeClass('stop-scrolling');
        });
        // Scroll Menu main
        $(".main-navigation").mCustomScrollbar({
            axis: "x",
            theme: "minimal",
            advanced: {autoExpandHorizontalScroll: true}
        });
        //menu horixontal scroll
        $(".child-horizontal").mCustomScrollbar({
            theme: "minimal"
        });
        /* ==========================================================================
           Header
        ========================================================================== */
        // group file open
        $('.group-profile').click(function (event) {
            $(this).toggleClass('group-profile-open');
            $('.group-profile-sub').slideToggle(400);
            event.stopPropagation();
        });
        // hide on click body
        $(document).click(function (event) {
            if ($(window).width() > 980) {
                if (!$(event.target).is('.group-profile-sub, .group-profile-sub *')) {
                    $(".group-profile-sub").hide();
                }
                if (!$(event.target).is('.wrap-dropdown, .wrap-dropdown *')) {
                    $('.list-dropdown').slideUp();
                    $('.wrap-dropdown .btn-dropdown,.wrap-dropdown .btn-select').removeClass('change-color');
                }
                if (!$(event.target).is('.header-notification, .header-notification *')) {
                    console.log('cuck');
                    $('.header-notification-sub').slideUp();
                }
            }
        });

        // Notification open
        $('.header-notification > a').click(function (event) {
            event.preventDefault();
            $('.header-notification-sub').slideToggle();
            event.stopPropagation();
        });
        // Select Dropdown
        $('.has-child-dropdown > .btn-toggle').click(function (e) {
            $(this).closest('.has-child-dropdown').toggleClass('active');
        });

        /* ==========================================================================
           Form Search
           ========================================================================== */
        $('.btn-dropdown').click(function (e) {
            e.preventDefault();
            $(this).toggleClass('change-color');
            $(this).next('ul').slideToggle();
        });
        $('.wrap-dropdown-km .btn-select').click(function (e) {
            e.preventDefault();
            $(this).toggleClass('change-color');
        });

        $('.wrap-dropdown li .text').click(function (event) {
            $('.btn-dropdown, .btn-select, .has-child-dropdown .btn-toggle').removeClass('change-color');
        });
        //
        $('.has-child-dropdown .btn-toggle').click(function (e) {
            e.preventDefault();
            $(this).toggleClass('change-color');
        });

        $('.wrap-dropdown-categories li .text').click(function (e) {
            var value = $(this).closest('li').data('value');
            var icon = $(this).closest('.has-icon').children('i').clone();
            $('.btn-dropdown').html(icon);
            $(this).closest('.list-dropdown').slideUp('fast').next('input').val(value);
        });
        $('.wrapper-form-search .wrap-dropdown li .text').click(function (event) {
            event.preventDefault();
        });
        // $('.wrap-dropdown-categories .list-dropdown, .wrap-dropdown-km .list-dropdown').mCustomScrollbar({
        //     theme: "minimal"
        // });

        /* ==========================================================================
            Custom select
            ========================================================================== */
        $('.custom-select-option .btn-select').click(function (e) {
            e.preventDefault();
            $(this).next('ul').slideToggle();
        });
        $('.custom-select-option .option-select li').click(function (event) {
            var value = $(this).data('value');
            $(this).closest('ul').slideUp('fast').next('input').val(value);
            $(this).closest('.custom-select-option').find('a').text($(this).text());
        });

        /* ==========================================================================
           Add Favorite
           ========================================================================== */
        $('body').on('click', '.add-favorite', function (event) {
            event.preventDefault();
            $(this).toggleClass('active');
            var id = $(this).data('id');
            $.ajax({
                url: cvh_data.favorites_url,
                type: 'POST',
                data: {id: id},
            }).done(function (response) {
                if (response.status == 200) {
                    $(window).trigger('favorites.update');
                }
            })
        });
        //add
        $('.add-checkbox').click(function (event) {
            $(this).toggleClass('active');
            $(this).closest('.add').find('input[type="checkbox"]').prop('checked', $(this).hasClass('active'));
        });

    });

})(jQuery); // end JQuery namespace

function reloadListImage() {
    var list_image = [];
    var lenght = $('.upload-img-post li.success').length;
    for (var i = 0; i < lenght; i++) {
        list_image.push($('li.success').eq(i).data('src'));
    }
    var myJsonString = JSON.stringify(list_image);
    $('input[name="list_image"]').val(myJsonString);
}

if (typeof initMap == "undefined") {
    function initMap() {
    }
}