<?php
return [
    'meta' => array('dt', 'huong', 'sotang', 'phongngu', 'phongvs', 'nguoiban'),
    'title' => array(
        'dt' => 'Diện Tích',
        'huong' => 'Hướng nhà',
        'sotang' => 'Số tầng',
        'phongngu' => 'Số phòng ngủ',
        'phongvs' => 'Số phòng vệ sinh',
        'nguoiban' => 'Người bán là',
    ),
    'sophong' => array('1', '2', '3', '4', '5', '6', '8', '10', '15', '20'),
    'loaibds' => array(
        'can-ho-chung-cu' => 'Căn hộ / Chung cư',
        'nha-o' => 'Nhà ở',
        'dat' => 'Đất',
        'van-phong-mat-bang-kinh-doanh' => 'Văn phòng / Mặt bằng kinh',
        'phong-tro' => 'Phòng trọ'
    ),
    'mucgia' => array(
        'muaban' => array(
            '100000000' => 'lớn hơn 100 triệu',
            '300000000' => 'lớn hơn 300 triệu',
            '500000000' => 'lớn hơn 500 triệu',
            '1000000000' => 'lớn hơn 1 tỷ',
            '1500000000' => 'lớn hơn 1,5 tỷ',
            '2000000000' => 'lớn hơn 2 tỷ'
        ),
        'chothue' => array(
            '3000000' => 'lớn hơn 3 triệu',
            '5000000' => 'lớn hơn 5 triệu',
            '10000000' => 'lớn hơn 10 triệu',
            '15000000' => 'lớn hơn 15 triệu',
            '20000000' => 'lớn hơn 20 triệu',
            '50000000' => 'lớn hơn 50 triệu',
        )
    ),
    'tag' => array(
        'Căn hộ / Chung cư' => 'can-ho-chung-cu',
        'Nhà ở' => 'nha-o',
        'Đất' => 'dat',
        'Văn phòng / Mặt bằng kinh doanh' => 'van-phong-mat-bang-kinh-doanh',
        'Phòng trọ' => 'phong-tro'
    ),
    'quang-cao' => array(
        'name' => 'Bất động sản',
        'image' => 'assets/img/upload/cate3.png',
        'total-rand-post' => array('412', '505', '593')
    )
];