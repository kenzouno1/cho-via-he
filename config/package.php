<?php
/**
 * Created by PhpStorm.
 * User: Dat Nguyen
 * Date: 6/23/2017
 * Time: 9:10 AM
 */

return [
    'boost' => array(
        'top-search' => array(
                'label' => 'Tốp đầu trang tìm kiếm',
                'description' => 'Giữ tin bài đăng của bạn ở đầu danh mục. Nó sẽ xuất hiện trên cùng các kết quả tìm kiếm trong một thời gian, giúp quảng cáo của bạn được mọi người chú ý nhiều hơn.',
                'image' => 'assets/img/default-image/thumb-top-search.png',
            ),
        'has-mockup' => array(
            'label' => 'Hiển thị mockup',
            'description' => 'Bạn cần hiển thị thông tin về sản phẩm muốn bán ở điều kiện cần bán gâp? chúng tôi sẽ hiện thị thêm phía trên hình ảnh của bạn.',
            'image' => 'assets/img/default-image/thumb-mockup-ban.jpg',
        ),
        'highlight' => array(
            'label' => 'Nổi bật',
            'description' => 'Làm cho bài đăng của bạn nổi bật lên với nền tảng đầy màu sắc. Nó sẽ giúp tin của bạn được thu hút nhiều người truy cập hoặc tìm kiếm.',
            'image' => 'assets/img/default-image/thumb-highlight.png',
        ),
        'show-home' => array(
            'label' => 'Hiện ở trang chủ',
            'description' => 'Bạn muốn là trung tâm của sự chú ý? Đặt tin của bạn trong các danh mục trên trang chủ của Chợ Vỉa Hè. Nơi mọi người có thể nhìn thấy nhiều nhất.',
            'image' => 'assets/img/default-image/thumb-show-home.png',
        ),
        'has-gallery' => array(
          'label' => 'Hiển thị nhiều ảnh',
          'description' => 'Quảng cáo của bạn hiển thị thêm nhiều ảnh hơn để thu hút khách hàng',
          'image'=> 'assets/img/default-image/thumb-show-home.png'
        ),
    ),
    'package' => array(
        'numberImage' => array(
            '1' => 1,
            '2' => 3,
            '3' => 5,
            '4' => 10
        )
    )
];