<?php
return [
    'user' => array(
        'status' => array(
            'default' => 'wait',
            'wait' => 'Chờ Kích Hoạt',
            'active' => 'Đã Kích Hoạt',
        ),
    ),
    'employee' => array(
        'status' => array(
            'default' => 'active',
            'block' => 'Đã Khóa',
            'active' => 'Đã Kích Hoạt',
        ),
    ),
    'avatar' => array(
        'max' => 2048,
        'size' => array(
            'w' => 150,
            'h' => 150
        ),
    ),
    'login' => array(
        'admin' => array(
            'limit' => env('ADMIN_LOGIN_LIMIT', 5),
            'time' => env('ADMIN_LOGIN_LIMIT_TIME', 5),
        ),
        'user' => array(
            'limit' => env('USER_LOGIN_LIMIT', 5),
            'time' => env('USER_LOGIN_LIMIT_TIME', 5),
        ),
    ),
    'ads' => array(
        'status' => array(
            'default' => 'deactive',
            'deactive' => 'Chưa Duyệt',
            'active' => 'Đã Duyệt',
        ),
    ),
    'km' => array(
        0, 5, 10, 20, 50, 100, 150, 200, 250, 500
    ),
    'landing_cate' => array(
        'car' => 32,
        'land' => 18,
        'job' => 19,
    ),
    'report' => [
        'cheat' => 'Tin lừa đảo',
        'duplicate' => 'Trùng lặp',
        'selled' => 'Hàng đã bán',
        'unavailable' => 'Không liên lạc được',
        'fake' => 'Tin không đúng sự thật'
    ],
    'featured_cate' => [
        '17' => [
            'img' => 'cate1.png',
            'slug' => 'o-to-va-xe-may',
        ],
        '19' => [
            'img' => 'cate1.png',
            'slug' => 'viec-lam',
        ],
        '17' => [
            'img' => 'cate2.png',
            'slug' => 'o-to-va-xe-may',
        ],
        '17' => [
            'img' => 'cate1.png',
            'slug' => 'o-to-va-xe-may',
        ],
        '17' => [
            'img' => 'cate1.png',
            'slug' => 'o-to-va-xe-may',
        ],
    ],
    'main_menu' => [
        '' => [
            'img' => '',
            'slug' => '',
        ],
    ],
    'cate_bg' => [
        'car' => 'assets/img/upload/cate1.png',
        'land' => 'assets/img/upload/cate2.png',
        'job' => 'assets/img/upload/cate3.png',
    ],
    'tinh_thanh' => [
        "ChIJjSriTooZCjERuojAUiywHNw" => [
            "label" => "An Giang",
            "latlng" => "10.5215836-105.12589550000007"],
        "ChIJQ44uKJxsdTERUu3qQZlT48w" => [
            "label" => "Bà Rịa - Vũng Tàu",
            "latlng" => "10.5417397-107.24299759999997"],
        "ChIJeSj5jmq2dDERcxSNHInanzM" => [
            "label" => "Bình Dương",
            "latlng" => "11.3254024-106.47701699999993"],
        "ChIJHyJ92y9oczEREUL4cg4bwJQ" => [
            "label" => "Bình Phước",
            "latlng" => "11.7511894-106.72346390000007"],
        "ChIJoVmP1vLDdjERjsI0345CNyc" => [
            "label" => "Bình Thuận",
            "latlng" => "11.0903703-108.0720781"],
        "ChIJUXqvJl0KNTERcI9VxsAgWeU" => [
            "label" => "Bắc Ninh",
            "latlng" => "21.121444-106.11105010000006"],
        "ChIJHxIM1bNlCjERQG40rV7JoNw" => [
            "label" => "Đồng Tháp",
            "latlng" => "10.4937989-105.68817880000006"],
        "ChIJJepKuFLZdDERVRZuS_HmSuA" => [
            "label" => "Đồng Nai",
            "latlng" => "11.0686305-107.16759760000002"],
        "ChIJJ6lE5uy1czERrYej9DDhMHU" => [
            "label" => "Đắk Nông",
            "latlng" => "12.2646476-107.60980599999994"],
        "ChIJV3Ew99r3cTERFTOI2ZR2-Y4" => [
            "label" => "Đắk Lắk",
            "latlng" => "12.7100116-108.23775190000003"],
        "ChIJ8_vR4A4fLTER9A7fYoWMcUg" => [
            "label" => "Điện Biên",
            "latlng" => "21.8042309-103.10765249999997"],
        "ChIJGStwEvU5MzERteIKRp__AAU" => [
            "label" => "Yên Bái",
            "latlng" => "21.6837923-104.4551361"],
        "ChIJuy3qgk3lNDERXxAb4T-O0MA" => [
            "label" => "Vĩnh Phúc",
            "latlng" => "21.3608805-105.54743729999996"],
        "ChIJE45EIbybCjERpXVcaT5qxlI" => [
            "label" => "Vĩnh Long",
            "latlng" => "10.0861281-106.01699710000003"],
        "ChIJMXJO1JFCCzERzWzchde8giQ" => [
            "label" => "Tây Ninh",
            "latlng" => "11.3494766-106.06401789999995"],
        "ChIJ4R5JpfutNDERf3iEknEBVH8" => [
            "label" => "Tuyên Quang",
            "latlng" => "22.1726708-105.31311849999997"],
        "ChIJgSaoteuvCjERdWM3wQen4zw" => [
            "label" => "Tiền Giang",
            "latlng" => "10.4493324-106.34205040000006"],
        "ChIJj-0MGk4XoDERE4o57BhuS7c" => [
            "label" => "Trà Vinh",
            "latlng" => "9.812740999999999-106.29929119999997"],
        "ChIJ95BkcoecQTERtYTL98oPHKs" => [
            "label" => "Thừa Thiên Huế",
            "latlng" => "16.467397-107.59053259999996"],
        "ChIJmZJ2447GNjERT_urFvXeeAE" => [
            "label" => "Thanh Hóa",
            "latlng" => "20.1291279-105.31311849999997"],
        "ChIJd3qgchjENDERW0_vaRA3qVc" => [
            "label" => "Thái Nguyên",
            "latlng" => "21.5613771-105.87600399999997"],
        "ChIJEyolkscZQjERh2RDRKDjFPw" => [
            "label" => "TP Đà Nẵng",
            "latlng" => "16.0544068-108.20216670000002"],
        "ChIJM3A59dPkNTERx1sdOPgF5Go" => [
            "label" => "Thái Bình",
            "latlng" => "20.5386936-106.39347769999995"],
        "ChIJ0T2NLikpdTERKxE8d61aX_E" => [
            "label" => "TP Hồ Chí Minh",
            "latlng" => "10.8230989-106.6296638"],
        "ChIJ0xo_nvN6SjER6Ad6-F_I_6U" => [
            "label" => "TP Hải Phòng",
            "latlng" => "20.8449115-106.68808409999997"],
        "ChIJoRyG2ZurNTERqRfKcnt_iOc" => [
            "label" => "TP Hà Nội",
            "latlng" => "21.0277644-105.83415979999995"],
        "ChIJt-3jbZ9ioDERWbYg-9sJf1I" => [
            "label" => "TP Cần Thơ",
            "latlng" => "10.0451618-105.74685350000004"],
        "ChIJKfEdqqvkQDERKE3VIOD6ceQ" => [
            "label" => "Quảng Trị",
            "latlng" => "16.7943472-106.96340899999996"],
        "ChIJ-zyQ3XmDNDERbmTIyOCN7DA" => [
            "label" => "Phú Thọ",
            "latlng" => "21.268443-105.20455730000003"],
        "ChIJbyVFtz5UtTYRWm6Chdo0Pfo" => [
            "label" => "Lạng Sơn",
            "latlng" => "21.8563705-106.62913040000001"],
        "ChIJBw5Mv4w7zTYRaJBG-7m2oiE" => [
            "label" => "Lào Cai",
            "latlng" => "22.3380865-104.1487055"],
        "ChIJ39GqWCPnazERVIrYzKoRm2E" => [
            "label" => "Kon Tum",
            "latlng" => "14.661167-107.83884999999998"],
        "ChIJs2mi7ZHyoDER5dn-Lulyo3k" => [
            "label" => "Hậu Giang",
            "latlng" => "9.757897999999999-105.6412527"],
        "ChIJXSYdMDWvNTERgtTWzxbqqac" => [
            "label" => "Hưng Yên",
            "latlng" => "20.8525711-106.01699710000003"],
        "ChIJb9jgnBgUNDERF_4UZiqEJag" => [
            "label" => "Hoà Bình",
            "latlng" => "20.6861265-105.31311849999997"],
        "ChIJ52dYYbZcyjYRWFR2N_RyN2U" => [
            "label" => "Cao Bằng",
            "latlng" => "22.635689-106.25221429999999"],
        "ChIJn6e8DFyoCjERQ3UxZ-VUPKo" => [
            "label" => "Bến Tre",
            "latlng" => "10.1081553-106.44058719999998"],
        "ChIJ7c9Gs6kQNTERyFDrUFtMQjw" => [
            "label" => "Bắc Giang",
            "latlng" => "21.3014947-106.62913040000001"],
        "ChIJDb3qxxupmDERT8bH4BD4NO8" => [
            "label" => "Bạc Liêu",
            "latlng" => "9.251555500000002-105.51364719999992"],
        "ChIJvxtxbu8tbzERN_3lOgRMv0U" => [
            "label" => "Bình Định",
            "latlng" => "14.1665324-108.90268300000002"],
        "ChIJTeuFSkjgyjYR0G5JK0iQA78" => [
            "label" => "Bắc Kạn",
            "latlng" => "22.3032923-105.87600399999997"],
        "ChIJez-heqBJoTERs-aysVPn-a8" => [
            "label" => "Cà Mau",
            "latlng" => "8.962409899999999-105.12589550000007"],
        "ChIJo4EvAoIfbDERDbVDgfa1Uow" => [
            "label" => "Gia Lai",
            "latlng" => "13.8078943-108.109375"],
        "ChIJsXHT6sp5zDYRqIDLmrB4SIM" => [
            "label" => "Hà Giang",
            "latlng" => "22.7662056-104.93888530000004"],
        "ChIJ4WQZGBHFNTERUmgyOw25qXQ" => [
            "label" => "Hà Nam",
            "latlng" => "20.5835196-105.92299000000003"],
        "ChIJx8Pp7qEoODERaxB9uCZ7xxo" => [
            "label" => "Hà Tĩnh",
            "latlng" => "18.2943776-105.6745247"],
        "ChIJNVLgSPR6SjERU5AdBNFX6YI" => [
            "label" => "Hải Dương",
            "latlng" => "20.9385958-106.32068609999999"],
        "ChIJldPcqIwbcDERzggjMicIwmk" => [
            "label" => "Khánh Hòa",
            "latlng" => "12.2585098-109.05260759999999"],
        "ChIJzXUakhzaCTERevNfMyZB54c" => [
            "label" => "Kiên Giang",
            "latlng" => "9.8249587-105.12589550000007"],
        "ChIJUbTROSwyLTERLjxIkWCRKeI" => [
            "label" => "Lai Châu",
            "latlng" => "22.3686613-103.31190850000007"],
        "ChIJeUgqlTy2CjERI0HFz5nt6ts" => [
            "label" => "Long An",
            "latlng" => "10.695572-106.24312050000003"],
        "ChIJfdvzWSYTcTERbPVUX9_SWrs" => [
            "label" => "Lâm Đồng",
            "latlng" => "11.5752791-108.14286689999994"],
        "ChIJ3xtSUSEMNjERYt8l_r-i68g" => [
            "label" => "Nam Định",
            "latlng" => "20.2791804-106.20514839999998"],
        "ChIJrR1aC2TOOTERGaUZ13y_YVw" => [
            "label" => "Nghệ An",
            "latlng" => "19.2342489-104.92003649999992"],
        "ChIJwcgQb6twNjERLQwTPE5T5W0" => [
            "label" => "Ninh Bình",
            "latlng" => "20.2129969-105.92299000000003"],
        "ChIJrwn6F26VcDERRZ6Odp6S9fg" => [
            "label" => "Ninh Thuận",
            "latlng" => "11.6738767-108.86295719999998"],
        "ChIJ_65MY79wbzERgBoXHBy-bv4" => [
            "label" => "Phú Yên",
            "latlng" => "13.0881861-109.09287640000002"],
        "ChIJLW8pOPawODERj1kjYi6HH5A" => [
            "label" => "Quảng Bình",
            "latlng" => "17.6102715-106.34874739999998"],
        "ChIJezo14dQNQjEReXZ0ZrGrnOQ" => [
            "label" => "Quảng Nam",
            "latlng" => "15.5393538-108.01910199999998"],
        "ChIJcwkWqew-aDERse05Mm6NoU0" => [
            "label" => "Quảng Ngãi",
            "latlng" => "15.0759838-108.71257909999997"],
        "ChIJIY5foglSoDERTgqWSmqWc9Y" => [
            "label" => "Sóc Trăng",
            "latlng" => "9.6003688-105.95995390000007"],
        "ChIJ1R093JBZMjERezgPalpkLVg" => [
            "label" => "Sơn La",
            "latlng" => "21.1022284-103.72891670000001"],
        "ChIJL4V4mtcBSzERaoal8PR5B-0" => [
            "label" => "Quảng Ninh",
            "latlng" => "21.006382-107.29251440000007"]
    ],
    'default_search' => [
        'address' => 'Vinh, Nghệ An, Việt Nam',
        'location' => '18.6916546-105.673378',
        'center' => '18.6916546,105.673378',
        'zoom' => 10
    ],
    'price_search' => [
        'basic' => [
            '10000' => '10 nghìn',
            '50000' => '50 nghìn',
            '100000' => '100 nghìn',
            '500000' => '500 nghìn',
            '1000000' => '1 triệu',
            '2000000' => '2 triệu',
            '5000000' => '5 triệu',
            '10000000' => '10 triệu',
            '20000000' => '20 triệu',
            '50000000' => '50 triệu',
            '100000000' => '100 triệu',
            '200000000' => '200 triệu',
            '500000000' => '500 triệu',
            '1000000000' => '1 tỷ',
            '9999999999999' => 'trên 1 tỷ',
        ],
        'car' => [
            '50000000' => '50 triệu',
            '100000000' => '100 triệu',
            '200000000' => '200 triệu',
            '500000000' => '500 triệu',
            '1000000000' => '1 tỷ',
            '9999999999999' => 'trên 1 tỷ',
        ],
        'bds' => [
            '50000000' => '50 triệu',
            '100000000' => '100 triệu',
            '200000000' => '200 triệu',
            '500000000' => '500 triệu',
            '1000000000' => '1 tỷ',
            '9999999999999' => 'trên 1 tỷ',
        ],
        'job' => [
            '1000000' => '1 triệu',
            '2000000' => '2 triệu',
            '3000000' => '3 triệu',
            '4000000' => '4 triệu',
            '5000000' => '5 triệu',
            '7000000' => '7 triệu',
            '10000000' => '10 triệu',
            '15000000' => '15 triệu',
            '20000000' => '20 triệu',
            '50000000' => '50 triệu',
            '100000000' => '100 triệu',
            '999999' => '> 100 triệu'
        ]
    ],
    'mod' => ['0947626537', '0984782258', '01678718468']
];