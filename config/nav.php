<?php
return [
    'admin.dashboard' => array(
        'title' => 'Dashboard',
        'icon' => 'fa fa-dashboard',
    ),
    'admin' => 'Administrator',
    'adwords' => array(
        'title' => 'Bài Viết',
        'icon' => 'fa fa-file',
        'items' => array(
            'admin.ads.listActive' => array(
                'title' => 'Đã Duyệt',
            ),
            'admin.ads.listDeactive' => array(
                'title' => 'Chưa Duyệt',
            ),
        ),
    ),
    'report' => array(
        'title' => 'Báo Cáo',
        'icon' => 'fa fa-list',
        'items' => array(
            'admin.report.ads' => array(
                'title' => 'Báo cáo tin',
            ),
            'admin.report.member' => array(
                'title' => 'Báo cáo thành viên',
            ),
        ),
    ),
    'logs' => array(
        'title' => 'Lịch Sử',
        'icon' => 'fa fa-history',
        'items' => array(
            'admin.logs.payment' => array(
                'title' => 'Lịch Sử Thanh Toán',
            ),
        ),
    ),
    'category' => array(
        'title' => 'Danh Mục',
        'icon' => 'fa fa-list',
        'items' => array(
            'admin.category.list' => array(
                'title' => 'Quản lý danh mục',
                'capability' => ['category.view'],
            ),
            'admin.category.new' => array(
                'title' => 'Thêm danh mục',
                'capability' => ['category.create'],
            ),
        ),
    ),
    'package' => array(
        'title' => 'Gói Cước',
        'icon' => 'fa fa-list',
        'items' => array(
            'admin.package.list' => array(
                'title' => 'Quản lý gói cước',
                'capability' => ['package.view'],
            ),
            'admin.package.new' => array(
                'title' => 'Thêm gói cước',
                'capability' => ['package.create'],
            ),
        ),
    ),
    'employee' => array(
        'title' => 'Nhân Viên',
        'icon' => 'fa-users fa',
        'items' => array(
            'admin.employee.list' => array('title' => 'Quản lý Nhân Viên', 'capability' => ['employee.view']),
            'admin.employee.new' => array('title' => 'Thêm Nhân Viên', 'capability' => ['employee.create']),
        ),
    ),
    'anylize' => 'Thống kê',
    'roles' => array(
        'title' => 'Phân Quyền',
        'icon' => '',
        'items' => array(
            'admin.roles.list' => array('title' => 'Quản lý Nhóm Quyền', 'capability' => []),
            'admin.roles.new' => array('title' => 'Thêm Nhóm Quyền', 'capability' => []),
        )
    ),
    'member-head' => 'Thành viên',
    'member' => array(
        'title' => 'Thành viên',
        'icon' => 'fa-address-card',
        'items' => array(
            'admin.member.list' => array('title' => 'Quản lý Thành Viên', 'capability' => []),
            'admin.member.new' => array('title' => 'Thêm Thành Viên', 'capability' => []),
        )
    ),
    'payment' => array(
        'title' => 'Gói nạp',
        'icon' => 'fa fa-dollar',
        'items' => array(
            'admin.payment.package.list' => array('title' => 'Danh sách gói nạp', 'capability' => []),
            'admin.payment.package.new' => array('title' => 'Thêm gói nạp', 'capability' => []),
//            'admin.member.new' => array('title' => 'Thêm Thành Viên', 'capability' => []),
        )
    ),
];