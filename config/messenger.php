<?php

return [

    'user_model' => App\User::class,

    'message_model' => App\Chat\Message::class,

    'participant_model' => App\Chat\Participant::class,

    'thread_model' => App\Chat\Thread::class,

    /**
     * Define custom database table names - without prefixes.
     */
    'messages_table' => 'messager_messages',

    'participants_table' => 'messager_participants',

    'threads_table' => 'messager_threads',
];
