<?php
return [
    'meta' => array('noisx', 'dongxe', 'namsx', 'kmdachay', 'hopso', 'nhienlieu', 'socho', 'kieudang'),
    'title' => array(
        'noisx' => 'Sản xuất tại',
        'dongxe' => 'Dòng Xe',
        'namsx' => 'Năm Sản Suất',
        'kmdachay' => 'Km đã chạy',
        'hopso' => 'Hộp Số',
        'nhienlieu' => 'Loại Nhiên Liệu',
        'socho' => 'Số Chỗ Ngồi',
        'kieudang' => 'Kiểu Dáng',
    ),
    'general' => array(
        'noisx' => array(
            'vietnam' => 'Việt Nam',
            'india' => 'Ấn Độ',
            'korea' => 'Hàn Quốc',
            'thailand' => 'Thái Lan',
            'japan' => 'Nhật Bản',
            'china' => 'Trung Quốc',
            'usa' => 'Mỹ',
            'germany' => 'Đức',
            'taiwan' => 'Đài Loan',
            'Khác' => 'khác'
        ),
        'nhienlieu' => array(
            'gasoline' => 'Xăng',
            'oil' => 'Dầu',
            'hybrid' => 'Động cơ Hybrid'
        ),
        'hopso' => array(
            'tất cả' => 'all',
            'số tự động' => 'automatic',
            'số sàn' => 'manual'),
    ),
    '34' => array(
        'kieudang' => array(
            'sedan' => 'Sedan',
            'suv-cross-over' => 'Suv / Cross over',
            'hatchback' => 'Hatchback',
            'pick-up' => 'Pick-up (bán tải)',
            'minivan' => 'Minivan (MPV)',
            'van' => 'van',
            'coupe' => 'Coupe (2 cửa)',
            'Convertibles' => 'Mui trần',
            'Khác' => 'Kiểu dáng khác'
        ),
        'socho' => array(2, 4, 5, 6, 7, 8, 10, 12, 14, 16, 24)
    ),
    'model' => array(
        'mazda' => array('1000', '1200', '121', '1300', '1500', '1600', '1800', '323', '600', '626', '800', '808', '929', 'B1500', 'B1600', 'B1800', 'B2000', 'B2200', 'B2500', 'B2600', 'B4000', 'BT-50', 'Capella', 'CX-3', 'CX-5', 'CX-7', 'CX-9', 'E1300', 'E1400', 'E1600', 'E1800', 'E2000', 'E2200', 'E2500', 'E3000', 'E4100', 'Eunos', 'F1000', '2', '3', '6', 'Millenia', 'MPV', 'MX-5', 'MX6', 'Premacy', 'R100', 'RX4', 'RX5', 'RX7', 'RX-8', 'Savanna', 'Traveller', 'Tribute', 'Khác'),
        'mercedes' => array('180', '190', '200', '200D', '220', '220D', '230', '240', '250', '260', '280', '300', '320', '350', '380', '400', '420', '450', '500', '560', '600', 'A140', 'A150', 'A160', 'A170', 'A180', 'A190', 'A200', 'A250', 'A45', 'B180', 'B200', 'B250', 'C180', 'C200', 'C220', 'C230', 'C240', 'C250', 'C280', 'C300', 'C320', 'C32', 'C350', 'C36', 'C43', 'C55', 'C63', 'CL500', 'CL600', 'CL', 'CLA', 'CLC', 'CLK200', 'CLK200K', 'CLK230', 'CLK240', 'CLK280', 'CLK320', 'CLK350', 'CLK430', 'CLK500', 'CLK55', 'CLK63', 'CLS', 'E200', 'E200K', 'E220', 'E230', 'E240', 'E250', 'E250d', 'E270', 'E280', 'E300', 'E320', 'E350', 'E36', 'E400', 'E430', 'E500', 'E55', 'E63', 'G', 'GL', 'GLA', 'GLC', 'GLE', 'GLS', 'MB', 'ML', 'R', 'S280', 'S300', 'S320', 'S350', 'S400', 'S420', 'S430', 'S500', 'S55', 'S600', 'S63', 'S65', 'SL280', 'SL320', 'SL350', 'SL500', 'SL600', 'SL', 'SLC', 'SLK200', 'SLK230', 'SLK320', 'SLK', 'SLS', 'V', 'Valente', 'Viano', 'Vito', 'Khác'),
        'hyundai' => array('Accent', 'Coupe FX', 'Coupe SFX', 'Coupe SX', 'Elantra', 'Elantra Lavita', 'Excel', 'Genesis', 'Getz', 'Grandeur', 'i20', 'i30', 'i40', 'i45', 'iLoad', 'iMAX', 'IX35', 'Lantra', 'S', 'Santa Fe', 'Sonata', 'Terracan', 'Tiburon', 'Trajet', 'Tucson', 'Veloster', 'Khác'),
        'nisan' => array('120Y', '180', '200', '280', '300', '350Z', '370Z', '720', 'Almera', 'Altima', 'Bluebird', 'C20', 'Cabstar', 'Cedric', 'Cube', 'Dualis', 'E20', 'Elgrand', 'EXA', 'Gazelle', 'GT-R', 'Homer', 'Infiniti', 'Juke', 'Largo', 'Leaf', 'Maxima', 'Micra', 'Murano', 'Navara', 'Nomad', 'NX', 'NX-R', 'Pathfinder', 'Patrol', 'Pintara', 'Prairie', 'Pulsar', 'Scargo', 'Silvia', 'Serena', 'Skyline', 'Stagea', 'Stanza', 'Sunny', 'Terrano', 'Terrano II', 'The Ute', 'Tiida', 'Urvan', 'V30', 'Vanette', 'X-trail', 'Khác'),
        'toyota' => array('2000', '4 Runner', '700', '86', 'Aristo', 'Aurion', 'Avalon', 'Avensis', 'Blizzard', 'Bundera', 'Caldina', 'Camry', 'Carina', 'Celica', 'Corolla', 'Corona', 'Cressida', 'Crown', 'Dyna', 'Echo', 'Estima', 'Harrier', 'FJ Cruiser', 'Fortuner', 'Hiace', 'Hilux', 'Kluger', 'LandCruiser', 'Lexcen', 'Lite Ace', 'MR2', 'Paseo', 'Prius', 'Prius-C', 'Prius V', 'RAV4', 'Rukus', 'Sera', 'Soarer', 'Spacia', 'Sprinter', 'Starlet', 'Stout', 'Supra', 'T-18', 'Tarago', 'Tercel', 'Tiara', 'Townace', 'Toyoace', 'TRD', 'Vienta', 'Yaris', 'Khác'),
        'bmw' => array('16', '18', '1', '20', '25', '28', '2', '3.0', '3.3L', '3', '4', '5', '6', '7', '8', 'i3', 'i8', 'L7', 'M2', 'M3', 'M4', 'M5', 'M6', 'M', 'X1', 'X3', 'X4', 'X5', 'X6', 'Z3', 'Z4', 'Khác'),
        'kia' => array('Carens', 'Carnival', 'Cerato', 'Ceres', 'Credos', 'Grand Carnival', 'K2700', 'K2900', 'Magentis', 'Mentor', 'Optima', 'Picanto', 'Pregio', 'Procee`d', 'Rio', 'Rondo', 'Shuma', 'Sorento', 'Soul', 'Spectra', 'Sportage', 'Khác'),
        'lexus' => array('CT 200h. Hybrid', 'ES300', 'ES300h', 'ES350', 'GS200t', 'GS250', 'GS300', 'GS300h', 'GS350', 'GS430', 'GS450h', 'GS460', 'GS-F', 'IS200', 'IS200t', 'IS250', 'IS250C', 'IS300', 'IS300h', 'IS350', 'IS F', 'LFA', 'LS400', 'LS430', 'LS460', 'LS600h', 'LS600hL', 'LX470', 'LX570', 'NX200t', 'NX300h', 'RC200t', 'RC350', 'RC F', 'RX200t', 'RX270', 'RX330', 'RX350', 'RX400h', 'RX450h', 'SC430', 'Khác'),
        'mitsubishi' => array('3000', '380', 'ASX', 'Challenger', 'Colt', 'Cordia', 'D50', 'Delica', 'Express', 'FTO', 'Galant', 'Grandis', 'i-MiEV', 'L200', 'L300', 'Lancer', 'Legnum', 'Magna', 'Mirage', 'Nimbus', 'Outlander', 'Pajero', 'RVR', 'Pajero Sport', 'Sigma', 'Starion', 'Starwagon', 'Triton', 'Verada', 'Khác'),
        'porsche' => array('356', '718', '911', '912', '924', '928', '930', '944', '968', 'Boxster', 'Cayenne', 'Cayman', 'Macan', 'Panamera', 'Khác'),
        'honda' => array('1300', '600', 'Accord', 'ACTY', 'Beat', 'City', 'Civic', 'Concerto', 'CR-V', 'CRX', 'CR-Z', 'HR-V', 'HRV (4x4)', 'Insight', 'Integra', 'Jazz', 'Legend', 'Life', 'MDX', 'N360', 'NSX', 'Odyssey (6 Seat)', 'Odyssey (7 Seat)', 'Odyssey', 'Prelude', 'S2000', 'S600', 'S800', 'Z', 'Khác'),
        'mini' => array('Cooper', 'One', 'Ray', 'Khác'),
        'chevrolet' => array('Bel Air', 'C20', 'C30', 'Camaro', 'Corvette', 'Impala', 'Khác'),
        'rangerover' => array('Evoque', 'Highline', 'Launch Pack', 'Range Rover', 'Khác'),
        'smart' => array('Cabrio', 'City Coupe', 'Forfour', 'Fortwo', 'Roadster', 'Khác')
    ),
    'price' => array(
        '100000000-300000000' => '100 triệu - 300 triệu',
        '300000000-500000000' => '300 triệu - 500 triệu',
        '500000000-1000000000' => '500 triệu - 1 tỷ',
        '>1000000000' => 'trên 1 tỷ'
    ),
];