<?php

return [
	'user.create' => 'Tạo Nhân Viên',
	'user.view' => 'Xem DS Nhân Viên',
	'user.delete' => 'Xóa Nhân Viên',
	'user.edit' => 'Sửa Nhân Viên',

	'category.create' => 'Tạo Danh Mục',
	'category.view' => 'Xem DS Danh Mục',
	'category.delete' => 'Xóa Danh Mục',
	'category.edit' => 'Sửa Danh Mục',

	'roles.create' => 'Tạo Nhóm Quyền',
	'roles.view' => 'Xem DS Nhóm Quyền',
	'roles.delete' => 'Xóa Nhóm Quyền',
	'roles.edit' => 'Sửa Nhóm Quyền',

	// 'user.create' => 'Tạo Nhân Viên',
	// 'user.view' => 'Xem DS Nhân Viên',
	// 'user.delete' => 'Xóa Nhân Viên',
	// 'user.edit' => 'Sửa Nhân Viên',

	// 'user.create' => 'Tạo Nhân Viên',
	// 'user.view' => 'Xem DS Nhân Viên',
	// 'user.delete' => 'Xóa Nhân Viên',
	// 'user.edit' => 'Sửa Nhân Viên',
];