<?php
return [
    'meta' => array('muc-luong-min', 'muc-luong-max', 'hinhthuc', 'congty', 'diachi', 'hannop'),
    'title' => array(
        'mucluong' => 'Mức lương',
        'hinhthuc' => 'Hình thức',
        'congty' => 'Công ty',
        'diachi' => 'Địa chỉ',
        'hannop' => 'Hạn nộp',
        'muc-luong-min' => 'mức lương tối thiểu',
        'muc-luong-max' => 'mức lương tối đa',
    ),
    'mucluong' => array(
        '1000000-3000000' => '1 triệu - 3 triệu',
        '3000000-7000000' => '3 triệu - 7 triệu',
        '7000000-15000000' => '8 triệu - 20 triệu',
        '21000000-50000000' => '21 triệu - 50 triệu',
        '0-9999999999' => 'lớn hơn 100 triệu'
    ),
    'tag' => array(
        'nguời tìm việc' => 'nguoi-tim-viec',
        'Việc tìm người' => 'viec-tim-nguoi'
    ),
    'quang-cao' => array(
        'name' => 'Việc làm',
        'image' => 'assets/img/upload/cate2.png',
        'total-rand-post' => array('513', '605', '686')
    )
];