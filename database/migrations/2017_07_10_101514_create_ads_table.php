<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug')->nullable();
            $table->longText('des')->nullable();
            $table->decimal('price',11,0)->default(0)->nullable();
            $table->decimal('price_min',20,0)->nullable();
            $table->string('ads_type');
            $table->string('price_type')->nullable();
            $table->string('state')->nullable();
            $table->string('status')->default(config('conf.ads.status.default'));
            $table->string('address');
            $table->float('lat',10,6);
            $table->float('lng',10,6);
            $table->integer('view_count')->default(0);
            $table->text('images')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->integer('cate_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->foreign('cate_id')->references('id')->on('categories')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
        DB::statement('CREATE FULLTEXT INDEX `idx_cvh_ads_title`  ON `'.config('database.connections.mysql.prefix').'ads` (title) ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads');
    }
}
