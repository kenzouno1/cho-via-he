<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        Schema::create('users', function ( Blueprint $table) {
            $table->increments('id');
            $table->string('phone',14)->unique()->nullable();
            $table->string('name')->nullable();
            $table->string('email',100)->nullable();
            $table->string('password',60);
            $table->date('birthday')->nullable();
            $table->string('job')->nullable();
            $table->string('gender')->nullable();
            $table->string('money',20)->default('0');
            $table->string('old_money',20)->default('0');
            $table->string('address',150)->nullable();
            $table->string('provider',150)->nullable()->default('form');
            $table->string('provider_id',150)->unique();
            $table->softDeletes();
            $table->string('avatar')->nullable();
            $table->string('status')->default(config('conf.user.status.default'));
            $table->timestamp('banned_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
