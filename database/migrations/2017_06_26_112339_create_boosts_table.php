<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Boosts.
 *
 * @author  The scaffold-interface created at 2017-06-26 11:23:39am
 * @link  https://github.com/amranidev/scaffold-interface
 */
class CreateBoostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('boosts',function (Blueprint $table){

        $table->increments('id');
        
        $table->String('name');
        
        $table->decimal('price',20,0);
        
        $table->integer('days');
        
        $table->String('type');
        
        $table->String('description')->nullable();

        /**
         * Foreignkeys section
         */


        $table->timestamps();


        $table->softDeletes();
        // type your addition here
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('boosts');
    }
}
