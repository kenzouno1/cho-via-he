<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdsBoost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads_boost', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->integer('ads_id')->unsigned();
            $table->foreign('ads_id')->references('id')->on('ads')->onDelete('cascade');
            $table->integer('boost_id')->unsigned();
            $table->foreign('boost_id')->references('id')->on('boosts')->onDelete('cascade');
            $table->decimal('price',20,0);
            $table->string('features');
            $table->boolean('renew')->default(false);
            $table->timestamp('expired_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads_boost');
    }
}
