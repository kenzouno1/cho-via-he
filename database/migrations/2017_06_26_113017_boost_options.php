<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Boost_relations.
 *
 * @author  The scaffold-interface created at 2017-06-26 11:30:17am
 * @link  https://github.com/amranidev/scaffold-interface
 */
class BoostOptions extends Migration
{
    /**
     * Run the migrations.k
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('boost_options',function (Blueprint $table){
            $table->increments('id');
            $table->integer('boost_id')->unsigned();
            $table->String('key');
            $table->String('value');
            $table->foreign('boost_id')->references('id')->on('boosts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('boost_options');
    }
}
