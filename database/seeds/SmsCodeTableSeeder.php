<?php

use Illuminate\Database\Seeder;

class SmsCodeTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('sms_code')->delete();
        
        \DB::table('sms_code')->insert(array (
            0 => 
            array (
                'id' => 1,
                'code' => '1234',
                'expire_at' => '2017-07-22 08:43:09',
                'type' => 'active',
                'user_id' => 1,
            ),
        ));
        
        
    }
}