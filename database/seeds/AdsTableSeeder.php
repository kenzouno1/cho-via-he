<?php

use Illuminate\Database\Seeder;

class AdsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('ads')->delete();
        
        \DB::table('ads')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'TRƯỞNG NHÓM KINH DOANH',
                'slug' => 'truong-nhom-kinh-doanh',
                'des' => 'Mô tả công việc:
+ Lập kế hoạch kinh doanh cho phòng nhóm ngay từ đầu tháng
+ Lập chương trình thi đua trong tháng cho các phòng nhóm .
+ Hỗ trợ, huấn luyện các ứng viên mới, bám sát chương trình thi đua của Công ty, giám sát mục tiêu thực hiện của các phòng nhóm để có kế hoạch hỗ trợ kịp thời.
+ Chuyển giao kế hoạch cho cấp dưới.
+ Thực hiện các nhiệm vụ khác theo yêu cầu của công ty.
+ ƯU TIÊN: Những người đã có kinh nghiệm làm Quản Lý trong ngành Bảo Hiểm, Tài Chính, Kế Toán, hoặc trong lĩnh vực Kinh Doanh.

Quyền lợi được hưởng:
+ Thu nhập: >= 10 triệu/ tháng+ Các khoản thưởng theo hiệu quả làm việc + thưởng tháng + thưởng quý + Chương trình thi đua hàng tháng cùng nhiều quà tặng hấp dẫn từ công ty…
+ Được tham gia các khóa đào tạo chuẩn mực về những kiến thức chung trong lĩnh vực tài chính, bảo hiểm và kĩ năng quản lý, nhà lãnh đạo tương lại, kiến thức sản phẩm, ...
+ Được làm việc trong môi trường năng động và chuyên nghiệp, tập thể đoàn kết.
+ Cơ hội thăng tiến rõ ràng, gắn bó lâu dài cùng công ty.
+ Được hưởng các chế độ BH sức khỏe, du lịch trong nước và nước ngoài theo quy định của Công ty.
+ Được nghỉ ngày thứ 7, chủ nhật và các ngày lễ theo quy định của nhà nước.

Thời gian thử việc : 2 tháng.',
                'price' => '0',
                'price_min' => NULL,
                'ads_type' => 'buy',
                'price_type' => NULL,
                'state' => NULL,
                'status' => 'active',
                'address' => 'Tòa nhà Trung Đô - Đại Lộ Lê Nin - Phường Hưng Dũng - TP Vinh - Nghệ An',
                'lat' => 18.679152999999999,
                'lng' => 105.70670800000001,
                'view_count' => 2,
                'images' => '["\\r\\n\\r\\nedt1429159673-1507624896.jpg","\\r\\n\\r\\n170306-a1-1507624896.jpg"]',
                'created_at' => '2017-10-10 15:42:35',
                'updated_at' => '2017-10-11 09:55:00',
                'deleted_at' => NULL,
                'cate_id' => 108,
                'user_id' => 20,
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'TRỢ LÝ TUYỂN DỤNG',
                'slug' => 'tro-ly-tuyen-dung',
                'des' => 'Mô tả công việc:
- Nắm bắt nhanh yêu cầu tuyển dụng của Công ty để lập kế hoạch tuyển dụng
- Hỗ trợ đăng tin tuyển dụng, tiếp nhận hồ sơ ứng viên, sàng lọc ban đầu
- Sắp xếp lịch phỏng vấn, sơ vấn ứng viên và chuyển phỏng vấn các hồ sơ đạt yêu cầu
- Hoàn thành các thủ tục hành chính về tuyển dụng theo quy định của Công ty
- Thường xuyên tư tham gia các khoá đào tạo nâng cao nghiệp vụ của Công ty
- Sẵn sàng tham gia các hoạt động chung của Công ty.

Quyền lợi được hưởng:
- Mức lương hấp dẫn theo năng lực
- Thường xuyên được đào tạo, nghiệp vụ, kỹ năng chuyên môn.
-Môi trường làm việc chuyên nghiệp, sáng tạo.
- Được hưởng các chế độ BH, du lịch, thăng tiến hấp dẫn theo quy định của Công ty
- Được nghỉ ngày thứ 7, chủ nhật và các ngày lễ theo quy định của nhà nước.

Thời gian thử việc : 1 tháng.',
                'price' => '0',
                'price_min' => NULL,
                'ads_type' => 'buy',
                'price_type' => NULL,
                'state' => NULL,
                'status' => 'active',
                'address' => 'Tòa nhà Trung Đô - Đại Lộ Lê Nin - Phường Hưng Dũng - TP Vinh - Nghệ An',
                'lat' => 18.679152999999999,
                'lng' => 105.70670800000001,
                'view_count' => 2,
                'images' => '["\\r\\n\\r\\n170306-a1-1507625959.jpg","\\r\\n\\r\\nedt1429159673-1507625959.jpg"]',
                'created_at' => '2017-10-10 15:59:52',
                'updated_at' => '2017-10-11 08:12:27',
                'deleted_at' => NULL,
                'cate_id' => 113,
                'user_id' => 20,
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'CHUYÊN VIÊN TƯ VẤN TÀI CHÍNH',
                'slug' => 'chuyen-vien-tu-van-tai-chinh',
                'des' => 'Mô tả công việc:
+ Thực hiện tư vấn cho khách hàng các kênh đầu tư hiệu quả, an toàn trong hiện tại và tương lai,
+ Cung cấp những giải pháp tài chính tối ưu phù hợp với điều kiện và nhu cầu khách hàng,
+ Đàm phán, ký kết hợp đồng các gói bảo vệ tài chính với khách hàng,
+ Tìm kiếm và khai thác khách hàng tiềm năng,
+ Thực hiện các nhiệm vụ khác theo yêu cầu của công ty.

Quyền lợi được hưởng:
+ Thu nhập: >= 8 triệu/ tháng+ Các khoản thưởng theo hiệu quả làm việc + thưởng tháng + thưởng quý + Chương trình thi đua hàng tháng cùng nhiều quà tặng hấp dẫn từ công ty…
+ Được tham gia các khóa đào tạo chuẩn mực về những kiến thức chung trong lĩnh vực tài chính, bảo hiểm và kĩ năng quản lý, kiến thức sản phẩm, kỹ năng tư vấn...
+ Được làm việc trong môi trường năng động và chuyên nghiệp, tập thể đoàn kết.
+ Cơ hội thăng tiến rõ ràng, gắn bó lâu dài cùng công ty.
+ Được hưởng các chế độ BH sức khỏe, du lịch trong nước,và nước ngoài theo quy định của Công ty.
+ Thời gian linh hoạt.
+ Được nghỉ ngày thứ 7, chủ nhật và các ngày lễ theo quy định của nhà nước.

Yêu cầu chung:
+ Nam/nữ tuổi từ : 25 – 45
+ Nhiệt huyết, có tinh thần trách nhiệm đối với công việc.
+ Có khả năng làm việc theo nhóm.
+ Gắn bó lâu dài với Công ty.

hồ sơ:
- 3 ảnh 3x4
- Hộ khẩu, chứng minh nhân dân(Có photo công chứng trong 6 tháng gần nhất)
- Các bằng cấp có liên quan.',
                'price' => '0',
                'price_min' => NULL,
                'ads_type' => 'buy',
                'price_type' => NULL,
                'state' => NULL,
                'status' => 'active',
                'address' => 'Tòa nhà Trung Đô - Đại Lộ Lê Nin - Phường Hưng Dũng - TP Vinh - Nghệ An',
                'lat' => 18.679152999999999,
                'lng' => 105.70670800000001,
                'view_count' => 1,
                'images' => '["\\r\\n\\r\\n170306-a1-1507626187.jpg","\\r\\n\\r\\nedt1429159673-1507626188.jpg"]',
                'created_at' => '2017-10-10 16:03:42',
                'updated_at' => '2017-10-10 16:38:47',
                'deleted_at' => NULL,
                'cate_id' => 126,
                'user_id' => 20,
            ),
            3 => 
            array (
                'id' => 4,
                'title' => 'Tuyển nhân viên kinh doanh bán hàng Order',
                'slug' => 'tuyen-nhan-vien-kinh-doanh-ban-hang-order',
            'des' => 'Cần tuyển 02 nhân viên kinh doanh bán hàng Order(không phải chở hàng)
Sản phẩm hoá mỹ phẩm(xịt phòng, sáp thơm, kem đánh răng, dao cạo râu, bàn chải...) đã có mặt trên thị trường 10 năm
Tham khảo sản phẩm tại: imexco.com.vn
Khu vực làm việc: Thành phố Vinh
Mô tả công việc: 
- Cầm Catalog và hàng mẫu đến chào hàng tại các tiệm tạp hoá,mỹ phẩm CÓ SẴN CỐ ĐỊNH mời khách hàng mua hàng,đưa đơn hàng về nạp cho chi nhánh
- Khoảng 70% số cửa hàng đang bán sản phẩm của Imexco
- Đi làm bằng xe máy của mình
- Thời gian làm việc : Giờ hành chính nghỉ chủ nhật và các ngày lễ
Lương cứng 5.000.000
Không áp doanh số chỉ cần đi làm đủ 26 ngày công
Thưởng doanh số từ 2.000.000 đến 5.000.000
Thu nhập bình quân: 8.000.000 đến 11.000.000
Được đóng bảo hiểm xã hội, bảo hiểm y tế theo quy định nhà nước
Liên hệ: (Vui lòng đọc kỹ thông tin trước khi gọi) 
Anh Hải 0974. 423. 523
Hồ sơ bao gồm(Sơ yếu lý lịch, hộ khẩu, CMND, đơn xin việc, các văn bằng(nếu có) có dấu và công chứng)
Nạp hồ sơ và phỏng vấn tại: Số nhà 16 ngõ số 4 đường Bùi Huy Bích thành phố Vinh tỉnh Nghệ An(Cạnh bệnh viện y học cổ truyền)',
                'price' => '0',
                'price_min' => NULL,
                'ads_type' => 'buy',
                'price_type' => NULL,
                'state' => NULL,
                'status' => 'active',
                'address' => 'Đường Bùi Huy Bích, thành phố Vinh, tỉnh Nghệ An',
                'lat' => 18.692323000000002,
                'lng' => 105.69314900000001,
                'view_count' => 1,
                'images' => '["\\r\\n\\r\\n2-1-1507626473.jpg"]',
                'created_at' => '2017-10-10 16:09:06',
                'updated_at' => '2017-10-10 16:38:50',
                'deleted_at' => NULL,
                'cate_id' => 108,
                'user_id' => 20,
            ),
            4 => 
            array (
                'id' => 5,
                'title' => 'Tuyển nữ nhân viên bán hàng',
                'slug' => 'tuyen-nu-nhan-vien-ban-hang',
            'des' => 'Siêu thị mẹ và bé có nhu cầu tuyển thêm 2 nhân viên bán hàng bán hàng, yêu cầu, hiền lành, nhẹ nhàng và giao tiếp tốt,có kinh nghiệm bán hàng, thành thạo máy tính, học vấn từ trung cấp trở nên. Công việc, bán hàng tại shop và tư vấn trả lời cho khách online, thời gian làm 1 trong 2 ca,ca sáng từ 8h – 16h hoặc ca chiều từ 15h – 21h30( một ngày làm 1 ca, các ca luân phiên nhau có thể làm sang hoặc tốt trong 1 ngày). Tháng được nghỉ 1 ngày, hoặc nhờ nhau để nghỉ nhiều hơn. 
Lương cứng 2,5 triệu triệu + thưởng ( theo doanh số), các bạn đang làm có mức lương trên 3 triệu – 3 ,5 triệu, nếu bán tốt lương lên đến, 4 triệu, 5 triệu…
Địa điểm làm việc Hà Huy Tập – thành phố Vinh gần chợ Kênh Bắc. 
Yêu cầu hồ sơ photo nộp về mail hoadongtien456@gmail.com hoặc đến nộp trực tiếp. Sđt liên hệ 0978 289 820.',
                'price' => '0',
                'price_min' => NULL,
                'ads_type' => 'buy',
                'price_type' => NULL,
                'state' => NULL,
                'status' => 'active',
                'address' => 'Hà Huy Tập, Thành phố Vinh, Nghệ An, Việt Nam',
                'lat' => 18.693332000000002,
                'lng' => 105.68524600000001,
                'view_count' => 1,
                'images' => NULL,
                'created_at' => '2017-10-10 16:19:20',
                'updated_at' => '2017-10-10 16:38:53',
                'deleted_at' => NULL,
                'cate_id' => 107,
                'user_id' => 20,
            ),
            5 => 
            array (
                'id' => 6,
                'title' => 'Nhân Viên Kinh Doanh',
                'slug' => 'nhan-vien-kinh-doanh',
                'des' => 'Vị trí: Nhân viên kinh doanh
Số lượng: 03 người

Mô tả công việc
- Liên hệ với khách hàng trong công tác chào hàng, báo giá, tạo lập mối quan hệ với những khách hàng tiềm năng.
- Thực hiện các công đoạn của hoạt động bán hàng theo quy trình bán hàng đã được duyệt.
- Xây dựng kế hoạch bán hàng và tiếp thị tới khách hàng. Theo dõi, chăm sóc KH và báo cáo về tình hình thị trường với Trưởng bộ phận.
- Thu thập và báo cáo Trưởng phòng các thông tin phản hồi từ khách hàng, xử lý sau khi được phê duyệt.
- Phối hợp với Xưởng, nhắc nhở khách hàng đã mua xe về các chương trình hậu mãi, đóng thùng…

- Hỗ trợ phòng kế toán khi thực hiện hợp đồng, hỗ trợ việc quản lý tài sản, xe trưng bày.
- Lập báo cáo hàng tuần về các công việc đã/sẽ thực hiện. Báo cáo nghiệp vụ hàng tuần/tháng/quý với Trưởng bộ phận.
Quyền lợi được hưởng:
– Được làm việc trong môi trường chuyên nghiệp, thân thiện, ổn định, thu nhập xứng đáng với năng lực, được học hỏi nâng cao năng lực làm việc
– Lương cứng + phụ cấp ăn trưa + Thưởng doanh số bán hàng
– Được tham gia các khóa đào tạo về chuyên môn và kỹ năng bán hàng chuyên nghiệp từ cơ bản đến chuyên sâu
– Được hưởng các chế độ theo quy đinh của nhà nước cũng như của công ty

Yêu cầu:
– Giới tính: Nam – Độ tuổi: 20-35
– Trình độ: Cao đẳng, đại học các chuyên ngành Kinh tế/ QTKD/ Công nghệ ô tô
– Có kinh nghiệm và hiểu biết về Ô tô
– Kỹ năng giao tiếp, đàm phán tốt, có khả năng làm việc độc lập, làm việc theo nhóm
– Sử dụng máy tính thành thạo,
– Ưu tiên ứng viên có bằng lái ô tô từ B2 trở lên
– Mong muốn phát triển lâu dài cùng với Công ty.

Hồ sơ bao gồm:
– Đơn xin việc làm (ghi rõ quá trình công tác cũng như kinh nghiệm làm việc).
– Sơ yếu lý lịch
– Bản sao hộ khẩu và các chứng chỉ văn bằng liên quan
– Giấy chứng nhận sức khỏe
Ứng viên có thể gửi CV về địa chỉ ngoctrung@hinovietdang.vn',
                'price' => NULL,
                'price_min' => NULL,
                'ads_type' => 'buy',
                'price_type' => 'treat',
                'state' => NULL,
                'status' => 'active',
                'address' => 'xã Nghi Thuận, Nghi Lộc, Nghệ An, Việt Nam',
                'lat' => 18.810195,
                'lng' => 105.63538800000001,
                'view_count' => 1,
                'images' => '["\\r\\n\\r\\n01-hino-night-dai-ly-van-dao-1024x576-1507627523.jpg"]',
                'created_at' => '2017-10-10 16:25:37',
                'updated_at' => '2017-10-10 16:38:56',
                'deleted_at' => NULL,
                'cate_id' => 108,
                'user_id' => 20,
            ),
            6 => 
            array (
                'id' => 7,
                'title' => 'Dược Phúc Vinh Tuyển NV Giao Hàng',
                'slug' => 'duoc-phuc-vinh-tuyen-nv-giao-hang',
                'des' => 'Chi Nhánh CTY CP Dược Phúc Vinh Tuyển Lái Xe Kim Giao Hàng
Mô Tả Công Việc:
Giao hàng bằng Oto tại các huyện theo tuyến trên địa bàn nghệ an
giao hàng bằng xe máy trong tp và đưa hàng ra bến xe
Yêu cầu:
có bằng và lái được oto 
có xe máy
có khả năng bảo lãnh tài chính
nam tuổi từ 30-45,có sức khỏe tốt.chăm chỉ thật thà
Hạn Nộp Hồ Sơ
20-10-2017 (lư ý cty chỉ gọi những hồ sơ phù hợp)
Liên Hệ Nộp Hồ Sơ
chi nhánh cty CP dược phúc vinh tại nghệ an
20 đóc thiết- p. hưng bình -tp vinh
điện thoại liên hệ: 0902.224.823-0943.242.777 . 02388.696.656',
                'price' => NULL,
                'price_min' => NULL,
                'ads_type' => 'buy',
                'price_type' => 'treat',
                'state' => NULL,
                'status' => 'active',
                'address' => '20 Đốc Thiết, phường Hưng Bình, Thành phố Vinh, Nghệ An, Việt Nam',
                'lat' => 18.681704,
                'lng' => 105.676535,
                'view_count' => 1,
                'images' => '["\\r\\n\\r\\ntru-so-vinphaco-1507627919.jpg"]',
                'created_at' => '2017-10-10 16:33:07',
                'updated_at' => '2017-10-10 16:38:59',
                'deleted_at' => NULL,
                'cate_id' => 160,
                'user_id' => 20,
            ),
            7 => 
            array (
                'id' => 8,
                'title' => 'Tuyển Nhân Viên Thị Trường - Ngành Vật Liệu Xây Dựng',
                'slug' => 'tuyen-nhan-vien-thi-truong-nganh-vat-lieu-xay-dung',
                'des' => 'Công việc : Nhân viên thị trường , hỗ trợ cửa hàng, công việc khá vất vả, áp lực. 
Cơ hội tốt cho những người có hoàn cảnh khó khăn nhưng có ý chí vươn lên.
Những người muốn có cơ hội kinh doanh nhưng chưa tìm được giải pháp

Có đào tạo công việc
Yêu cầu : 
- Nam tốt nghiệp trung học phổ thông trở lên, có sức khỏe tốt
- Có mục tiêu làm việc lâu dài,
- Nhanh nhẹn chịu khó vui vẻ hòa đồng ,hăng hái nhiệt tình với công việc...
- Ưu tiên có năng khiếu văn nghệ , thể thao. , .

Cơ hội thăng tiến : Nhân viên có năng lực sẽ được bổ sung vào đội ngũ quản lý sau 1-2 năm .Trình bày rõ sau khi phỏng vấn xong.',
                'price' => '0',
                'price_min' => NULL,
                'ads_type' => 'buy',
                'price_type' => NULL,
                'state' => NULL,
                'status' => 'active',
                'address' => 'Đường Lê Ninh, Lệ Ninh, Thành phố Vinh, Nghệ An, Việt Nam',
                'lat' => 18.688141999999999,
                'lng' => 105.66392,
                'view_count' => 1,
                'images' => NULL,
                'created_at' => '2017-10-10 16:37:52',
                'updated_at' => '2017-10-10 16:39:02',
                'deleted_at' => NULL,
                'cate_id' => 107,
                'user_id' => 20,
            ),
            8 => 
            array (
                'id' => 9,
                'title' => 'Tuyển dụng Nhân viên kinh doanh website',
                'slug' => 'tuyen-dung-nhan-vien-kinh-doanh-website',
                'des' => 'Mô tả công việc:
- Gọi điện, giới thiệu dịch vụ, sản phẩm của Công ty đến đối tác.
- Xử lý các cuộc gọi của khách hàng liên quan đến, sản phẩm, dịch vụ Công ty.
- Đàm phán, thương thảo, ký kết hợp đồng với khách hàng đang có nhu cầu thiết kế website , SEO website, PR thương hiệu trên Internet.
- Tiếp cận các khách hàng tiềm năng để tư vấn, chốt hợp đồng thiết kế website, dịch vụ seo, phần mềm quản lý bán hàng
- Phát triển các ý tưởng và phương án kinh doanh
- Xây dựng cơ sở dữ liệu khách hàng, cập nhật và lưu trữ
- Phối hợp cùng trưởng nhóm kinh doanh và đội ngũ kỹ thuật
- Theo dõi các xu hướng của thị trường và thông tin về đối thủ cạnh tranh
Yêu cầu:
- Ưu tiên ứng viên có kinh nghiệm và hiểu biết về công nghệ thông tin, thiết kế website, dịch vụ seo website.
- Am hiểu về SEO hoặc đã từng sử quản trị website là một lợi thế.
- Trung thực, nhiệt tình, năng động, và có trách nhiệm với công việc.
- Có khả năng làm việc độc lập và làm việc theo nhóm.
- Giao tiếp tốt, nhanh nhẹn trong xử lý tình huống
- Có khả năng giao tiếp tốt, đặt lịch hẹn qua điện thoại.
- Có tính cẩn thận, trung thực, năng động, nhiệt tình, siêng năng, nhiệt huyết trong công việc luôn cố gắng học hỏi.
- Kỹ năng telesale tốt.
- Có sự cầu tiến và đam mê trong công việc, chịu được áp lực
Quyền lợi:
- Thu nhập hấp dẫn, lương cứng 3-6 triệu + % hoa hồng + thưởng - Phụ cấp xăng xe + Điện thoại
- Được đào tạo các kỹ năng về kinh doanh, tư vấn, làm việc nhóm và các kỹ năng mềm khác
- Có cơ hội làm việc trong môi trường năng động, hỗ trợ cao từ đồng nghiệp và quản lý, thân thiện, cơ hội thăng tiến cao

2. THỜI GIAN, ĐỊA ĐIỂM NHẬN HỒ SƠ:
2.1 Thời gian:đến hết ngày 30/10/2017
Buổi sáng : từ 07 giờ 30 đến 11 giờ 30
Buổi chiều: từ 13 giờ 30 đến 17 giờ 30 (trừ ngày CN)
- Các ứng viên quan tâm có thể nộp hồ sơ qua email hoặc trực tiếp đến tại Công ty.
2.2 Địa điểm nhận hồ sơ:
Phòng Nhân sự - Công ty TNHH TM & DV Tin học Nguyễn Linh
- ĐC : Số 02B, đường Phạm Ngũ Lão, P. Cửa Nam, Tp Vinh, Nghệ An.
- Điện thoại: 02383.968.777 – Hotline: 0968 97 3883 – 0941.90.2929

CV Online gửi về email: linhnm@bncgroup.com.vn

Tiêu đề ghi rõ Họ tên – Vị trí ứng tuyển
- CV nêu rõ quá trình học tập, công tác và kinh nghiệm làm việc.

Văn Phòng Đại Diện Chi Nhánh NghệAn :
Công ty TM & DV Tin Học Nguyễn Linh
ĐC : Số 02B, đường Phạm Ngũ Lão, P. Cửa Nam, Tp Vinh, Nghệ An.

www.webbnc.net - http://bncgroup.com.vn/',
                'price' => '0',
                'price_min' => NULL,
                'ads_type' => 'buy',
                'price_type' => NULL,
                'state' => NULL,
                'status' => 'active',
                'address' => 'Số 02B, đường Phạm Ngũ Lão, P. Cửa Nam, Tp Vinh, Nghệ An',
                'lat' => 18.667261,
                'lng' => 105.66713,
                'view_count' => 0,
                'images' => NULL,
                'created_at' => '2017-10-11 08:35:23',
                'updated_at' => '2017-10-11 09:44:51',
                'deleted_at' => NULL,
                'cate_id' => 108,
                'user_id' => 20,
            ),
            9 => 
            array (
                'id' => 10,
                'title' => 'Tuyển dụng Nhân viên kinh doanh website',
                'slug' => 'tuyen-dung-nhan-vien-kinh-doanh-website-1',
                'des' => 'Mô tả công việc:
- Gọi điện, giới thiệu dịch vụ, sản phẩm của Công ty đến đối tác.
- Xử lý các cuộc gọi của khách hàng liên quan đến, sản phẩm, dịch vụ Công ty.
- Đàm phán, thương thảo, ký kết hợp đồng với khách hàng đang có nhu cầu thiết kế website , SEO website, PR thương hiệu trên Internet.
- Tiếp cận các khách hàng tiềm năng để tư vấn, chốt hợp đồng thiết kế website, dịch vụ seo, phần mềm quản lý bán hàng
- Phát triển các ý tưởng và phương án kinh doanh
- Xây dựng cơ sở dữ liệu khách hàng, cập nhật và lưu trữ
- Phối hợp cùng trưởng nhóm kinh doanh và đội ngũ kỹ thuật
- Theo dõi các xu hướng của thị trường và thông tin về đối thủ cạnh tranh
Yêu cầu:
- Ưu tiên ứng viên có kinh nghiệm và hiểu biết về công nghệ thông tin, thiết kế website, dịch vụ seo website.
- Am hiểu về SEO hoặc đã từng sử quản trị website là một lợi thế.
- Trung thực, nhiệt tình, năng động, và có trách nhiệm với công việc.
- Có khả năng làm việc độc lập và làm việc theo nhóm.
- Giao tiếp tốt, nhanh nhẹn trong xử lý tình huống
- Có khả năng giao tiếp tốt, đặt lịch hẹn qua điện thoại.
- Có tính cẩn thận, trung thực, năng động, nhiệt tình, siêng năng, nhiệt huyết trong công việc luôn cố gắng học hỏi.
- Kỹ năng telesale tốt.
- Có sự cầu tiến và đam mê trong công việc, chịu được áp lực
Quyền lợi:
- Thu nhập hấp dẫn, lương cứng 3-6 triệu + % hoa hồng + thưởng - Phụ cấp xăng xe + Điện thoại
- Được đào tạo các kỹ năng về kinh doanh, tư vấn, làm việc nhóm và các kỹ năng mềm khác
- Có cơ hội làm việc trong môi trường năng động, hỗ trợ cao từ đồng nghiệp và quản lý, thân thiện, cơ hội thăng tiến cao

2. THỜI GIAN, ĐỊA ĐIỂM NHẬN HỒ SƠ:
2.1 Thời gian:đến hết ngày 30/10/2017
Buổi sáng : từ 07 giờ 30 đến 11 giờ 30
Buổi chiều: từ 13 giờ 30 đến 17 giờ 30 (trừ ngày CN)
- Các ứng viên quan tâm có thể nộp hồ sơ qua email hoặc trực tiếp đến tại Công ty.
2.2 Địa điểm nhận hồ sơ:
Phòng Nhân sự - Công ty TNHH TM & DV Tin học Nguyễn Linh
- ĐC : Số 02B, đường Phạm Ngũ Lão, P. Cửa Nam, Tp Vinh, Nghệ An.
- Điện thoại: 02383.968.777 – Hotline: 0968 97 3883 – 0941.90.2929

CV Online gửi về email: linhnm@bncgroup.com.vn

Tiêu đề ghi rõ Họ tên – Vị trí ứng tuyển
- CV nêu rõ quá trình học tập, công tác và kinh nghiệm làm việc.

Văn Phòng Đại Diện Chi Nhánh NghệAn :
Công ty TM & DV Tin Học Nguyễn Linh
ĐC : Số 02B, đường Phạm Ngũ Lão, P. Cửa Nam, Tp Vinh, Nghệ An.

www.webbnc.net - http://bncgroup.com.vn/',
                'price' => '0',
                'price_min' => NULL,
                'ads_type' => 'buy',
                'price_type' => NULL,
                'state' => NULL,
                'status' => 'active',
                'address' => 'Số 02B, đường Phạm Ngũ Lão, P. Cửa Nam, Tp Vinh, Nghệ An',
                'lat' => 18.667261,
                'lng' => 105.66713,
                'view_count' => 1,
                'images' => NULL,
                'created_at' => '2017-10-11 08:35:40',
                'updated_at' => '2017-10-11 09:44:48',
                'deleted_at' => NULL,
                'cate_id' => 108,
                'user_id' => 20,
            ),
            10 => 
            array (
                'id' => 11,
                'title' => 'CÔNG TY TNHH TM & QUẢNG CÁO THÀNH VINH',
                'slug' => 'cong-ty-tnhh-tm-quang-cao-thanh-vinh',
                'des' => 'Yêu cầu:
- Có khả năng sáng tạo, tư duy ý tưởng tốt
- Sử dụng các phần mềm thiết kế chuyên nghiệp một cách thành thạo (Autocad, Corel, AI, Photoshop, 3D...)
- Am hiểu lĩnh vực in ấn
- Có tinh thần trách nhiệm cao, có khả năng tự học, chăm chỉ cần cù chịu khó
- Có thể chịu được áp lực công việc, hoàn thành công việc đúng thời hạn
- Có kĩ năng giải quyết vấn đề tốt, có tính độc lập cao đồng thời có khả năng làm việc theo nhóm tốt
- Ưu tiên người có kinh nghiệm trong lĩnh vực Quảng Cáo.
*Quyền lợi:
- Được làm việc trong một môi trường năng động, chuyên nghiệp.
- Người lao động được hưởng các chế độ theo đúng Luật Lao động Việt Nam
*Mức lương: theo thỏa thuận tùy khả năng
web : http://quangcaothanhvinh.com',
                'price' => NULL,
                'price_min' => NULL,
                'ads_type' => 'buy',
                'price_type' => 'treat',
                'state' => NULL,
                'status' => 'active',
                'address' => '38b Hoàng Văn Thụ, Thành phố Vinh, Nghệ An, Việt Nam',
                'lat' => 18.691495,
                'lng' => 105.676619,
                'view_count' => 1,
                'images' => NULL,
                'created_at' => '2017-10-11 08:39:27',
                'updated_at' => '2017-10-11 09:44:45',
                'deleted_at' => NULL,
                'cate_id' => 152,
                'user_id' => 20,
            ),
            11 => 
            array (
                'id' => 12,
                'title' => 'THỢ QUẢNG CÁO, THỢ CƠ KHÍ',
                'slug' => 'tho-quang-cao-tho-co-khi',
                'des' => '- Thợ Quảng cáo, Thợ Cơ khí lương theo thỏa thuận tùy khả năng
- Ưu tiên thợ biết ốp Alu, biết làm Led, hàn Inox... nhanh nhẹn, chịu khó, trung thực, khiêm tốn....
- Không yêu cầu bằng cấp.
- Nộp hồ sơ về Email: quangcaothanhvinh@gmail.com
Website: http://quangcaothanhvinh.com
HỒ SƠ DỰ TUYỂN: 01 bộ hồ sơ, mỗi bộ gồm có: 01 bản Sơ yếu lý lịch, 01 bản sao giấy khai sinh, 01 bản công chứng bằng tốt nghiệp văn hoá, 01 sổ hộ khẩu, 01 bản chứng minh thư nhân dân (có công chứng), 02 ảnh cỡ 3x4.',
                'price' => NULL,
                'price_min' => NULL,
                'ads_type' => 'buy',
                'price_type' => 'treat',
                'state' => NULL,
                'status' => 'active',
                'address' => '38b Hoàng Văn Thụ, Thành phố Vinh, Nghệ An, Việt Nam',
                'lat' => 18.691495,
                'lng' => 105.676619,
                'view_count' => 1,
                'images' => NULL,
                'created_at' => '2017-10-11 08:41:58',
                'updated_at' => '2017-10-11 09:44:38',
                'deleted_at' => NULL,
                'cate_id' => 122,
                'user_id' => 20,
            ),
            12 => 
            array (
                'id' => 13,
                'title' => 'Nhân viên thiết kế',
                'slug' => 'nhan-vien-thiet-ke',
                'des' => 'Yêu cầu :
- Có kinh nghiệm thiết kế nội thất.
- Triển khai thiết kế sơ bộ.
- Khảo sát hiện trạng không gian nội thất.
- Tư vấn để xuất ý tưởng phù hợp cho khách hàng.
- Triển khải bản vẽ cho xưởng sản xuất gỗ.
- Bóc tách bản vẽ cho xưởng sản xuất.
- Siêng năng, thật thà, có trách nhiệm trong công việc.
- sử dụng thành thạo Autocad, biết vẽ 3D.',
                'price' => NULL,
                'price_min' => NULL,
                'ads_type' => 'buy',
                'price_type' => 'treat',
                'state' => NULL,
                'status' => 'active',
                'address' => 'đường Nguyễn minh châu Phường đông vĩnh thành phố vinh, nghệ an',
                'lat' => 18.682891999999999,
                'lng' => 105.65901700000001,
                'view_count' => 1,
                'images' => NULL,
                'created_at' => '2017-10-11 08:45:44',
                'updated_at' => '2017-10-11 09:44:34',
                'deleted_at' => NULL,
                'cate_id' => 153,
                'user_id' => 20,
            ),
            13 => 
            array (
                'id' => 14,
                'title' => 'Nhân Viên Thị Trường',
                'slug' => 'nhan-vien-thi-truong',
                'des' => 'Công việc :
- Tìm kiếm khách hàng mới.
- Chăm sóc khách hàng cũ.
- Khảo sát, đi thị trường tiếp xúc khách hàng.
- Am hiểu nội thất là 1 lợi thế.
- Chịu khó, siêng năng và nhanh nhẹn.',
                'price' => NULL,
                'price_min' => NULL,
                'ads_type' => 'buy',
                'price_type' => 'treat',
                'state' => NULL,
                'status' => 'active',
                'address' => 'Số 2 - Đường Nguyễn Minh Châu - Phường  Đông Vĩnh - Thành phố Vinh - Nghệ an  - Việt Nam',
                'lat' => 18.682891999999999,
                'lng' => 105.65901700000001,
                'view_count' => 1,
                'images' => NULL,
                'created_at' => '2017-10-11 08:54:14',
                'updated_at' => '2017-10-11 09:44:26',
                'deleted_at' => NULL,
                'cate_id' => 107,
                'user_id' => 20,
            ),
            14 => 
            array (
                'id' => 15,
                'title' => 'Kế toán trưởng',
                'slug' => 'ke-toan-truong',
                'des' => '- Số lượng: 01
- Giới tính: Nam/Nữ
- Độ tuổi: 30 – 45
- Trình độ chuyên môn: Tốt nghiệp Đại học chuyên ngành kế toán
- Ưu tiên người có chứng chỉ kế toán trưởng.
- Ngoại ngữ: Anh B1/TOEIC 450
- Sử dụng thành thạo các phần mềm kế toán: Misa, Totalsoft,…
- Kinh nghiệm nghề nghiệp: 3 – 5 năm ở vị trí tương đương',
                'price' => NULL,
                'price_min' => NULL,
                'ads_type' => 'buy',
                'price_type' => 'treat',
                'state' => NULL,
                'status' => 'active',
                'address' => 'Số 28, đường Nguyễn Sỹ Sách, TP.Vinh, Nghệ An.',
                'lat' => 18.686419999999998,
                'lng' => 105.678607,
                'view_count' => 1,
                'images' => '["\\r\\n\\r\\n15665765-1867099813510079-9128419848343779849-n-1507687616.jpg","\\r\\n\\r\\n15776992-1867933243426736-7342548320919677617-o-1507687620.jpg"]',
                'created_at' => '2017-10-11 09:07:02',
                'updated_at' => '2017-10-11 09:44:31',
                'deleted_at' => NULL,
                'cate_id' => 112,
                'user_id' => 20,
            ),
            15 => 
            array (
                'id' => 16,
                'title' => 'Nhân viên Quản trị mạng',
                'slug' => 'nhan-vien-quan-tri-mang',
                'des' => '- Số lượng: 01 người
- Giới tính: Nam
- Độ tuổi: 23 - 35
- Trình độ chuyên môn: Tốt nghiệp Đại học, Cao đẳng các ngành công nghệ thông tin hoặc có các chứng chỉ tương đương CCNA, CCNP.
- Ưu tiên người có kinh nghiệm trên 1 năm ở vị trí tương đương, ứng viên có các chứng chỉ về Security và Checkpoint và có khả năng cấu hình mạng, firewall.
II. QUYỀN LỢI:
- Được làm việc trong môi trường chuyên nghiệp, năng động và ổn định, chế độ đãi ngộ cao, thu nhập hấp dẫn.
- Được tham gia các khóa đào tạo do Công ty tổ chức.
- Làm việc theo giờ hành chính, chủ nhật và lễ tết nghỉ theo qui định.
- Được hưởng các quyền lợi theo Luật lao động, ...

III. YÊU CẦU HỒ SƠ DỰ TUYỂN:
- Sơ yếu lý lịch cá nhân tự thuật.
- Ảnh 3x4 số lượng 02 cái.
- Hồ sơ công chứng: Bằng, bảng điểm, các chứng chỉ liên quan, CMND
- Giấy chứng nhận sức khỏe, Giấy khai sinh (chấp nhận phô tô)
- Đơn xin đăng ký dự tuyển ghi rõ vị trí ứng tuyển và khả năng, kinh nghiệm làm việc của bản thân.

IV. ĐỊA CHỈ NHẬN HỒ SƠ:
- Liên hệ trong giờ hành chính qua các số máy:
- Tel: 0238 3799999 - Số máy lẻ: 515./Di động: 0947.957.789 (Chị Nhung)
- Nộp hồ sơ trực tiếp tại Lễ tân tầng 1 Bệnh viện Răng Hàm Mặt và Phẫu thuật tạo hình thẩm mỹ Thái Thượng Hoàng
- Đc: Số 28, đường Nguyễn Sỹ Sách, tp.Vinh, tỉnh Nghệ An.
- Hạn nộp hồ sơ: 15/10/2017.
- Ứng viên có thể nộp hồ sơ và CV trước qua mail: nhungnth@tthgroup.vn

Lưu ý: Công ty không trả lại hồ sơ cho các ứng viên không trúng tuyển.',
                'price' => NULL,
                'price_min' => NULL,
                'ads_type' => 'buy',
                'price_type' => 'treat',
                'state' => NULL,
                'status' => 'active',
                'address' => 'Số 28, đường Nguyễn Sỹ Sách, tp.Vinh, tỉnh Nghệ An',
                'lat' => 18.686419999999998,
                'lng' => 105.678607,
                'view_count' => 1,
                'images' => '["\\r\\n\\r\\n15665765-1867099813510079-9128419848343779849-n-1507687711.jpg","\\r\\n\\r\\n15776992-1867933243426736-7342548320919677617-o-1507687712.jpg"]',
                'created_at' => '2017-10-11 09:09:13',
                'updated_at' => '2017-10-11 09:44:23',
                'deleted_at' => NULL,
                'cate_id' => 139,
                'user_id' => 20,
            ),
            16 => 
            array (
                'id' => 17,
            'title' => 'Nhân viên Kỹ thuật (kỹ thuật chuyển giao và lắp đặt máy)',
                'slug' => 'nhan-vien-ky-thuat-ky-thuat-chuyen-giao-va-lap-dat-may',
                'des' => '- Số lượng: 02
- Giới tính: Nam/Nữ
- Độ tuổi: Từ 23 tuổi
- Trình độ chuyên môn: Tốt nghiệp Đại học, Cao đẳng chuyên ngành kỹ thuật
- Ưu tiên người có kinh nghiệm
II. QUYỀN LỢI:
- Được làm việc trong môi trường chuyên nghiệp, năng động và ổn định, chế độ đãi ngộ cao, thu nhập hấp dẫn.
- Được tham gia các khóa đào tạo do Công ty tổ chức.
- Làm việc theo giờ hành chính, chủ nhật và lễ tết nghỉ theo qui định.
- Được hưởng các quyền lợi theo Luật lao động, ...

III. YÊU CẦU HỒ SƠ DỰ TUYỂN:
- Sơ yếu lý lịch cá nhân tự thuật.
- Ảnh 3x4 số lượng 02 cái.
- Hồ sơ công chứng: Bằng, bảng điểm, các chứng chỉ liên quan, CMND
- Giấy chứng nhận sức khỏe, Giấy khai sinh (chấp nhận phô tô)
- Đơn xin đăng ký dự tuyển ghi rõ vị trí ứng tuyển và khả năng, kinh nghiệm làm việc của bản thân.

IV. ĐỊA CHỈ NHẬN HỒ SƠ:
- Liên hệ trong giờ hành chính qua các số máy:
- Tel: 0238 3799999 - Số máy lẻ: 515./Di động: 0947.957.789 (Chị Nhung)
- Nộp hồ sơ trực tiếp tại Lễ tân tầng 1 Bệnh viện Răng Hàm Mặt và Phẫu thuật tạo hình thẩm mỹ Thái Thượng Hoàng
- Đc: Số 28, đường Nguyễn Sỹ Sách, tp.Vinh, tỉnh Nghệ An.
- Hạn nộp hồ sơ: 15/10/2017.
- Ứng viên có thể nộp hồ sơ và CV trước qua mail: nhungnth@tthgroup.vn

Lưu ý: Công ty không trả lại hồ sơ cho các ứng viên không trúng tuyển.',
                'price' => NULL,
                'price_min' => NULL,
                'ads_type' => 'buy',
                'price_type' => 'treat',
                'state' => NULL,
                'status' => 'active',
                'address' => 'Số 28, đường Nguyễn Sỹ Sách, tp.Vinh, tỉnh Nghệ An',
                'lat' => 18.686419999999998,
                'lng' => 105.678607,
                'view_count' => 1,
                'images' => '["\\r\\n\\r\\n15665765-1867099813510079-9128419848343779849-n-1507687848.jpg","\\r\\n\\r\\n15776992-1867933243426736-7342548320919677617-o-1507687849.jpg"]',
                'created_at' => '2017-10-11 09:11:11',
                'updated_at' => '2017-10-11 09:44:17',
                'deleted_at' => NULL,
                'cate_id' => 141,
                'user_id' => 20,
            ),
            17 => 
            array (
                'id' => 18,
                'title' => 'Nhân viên kinh doanh',
                'slug' => 'nhan-vien-kinh-doanh-1',
                'des' => '- Số lượng: 02
- Giới tính: Nam/Nữ
- Độ tuổi: Từ 22 tuổi
- Ưu tiên người có kinh nghiệm trên 1 năm ở vị trí tương đương.

II. QUYỀN LỢI:
- Được làm việc trong môi trường chuyên nghiệp, năng động và ổn định, chế độ đãi ngộ cao, thu nhập hấp dẫn.
- Được tham gia các khóa đào tạo do Công ty tổ chức.
- Làm việc theo giờ hành chính, chủ nhật và lễ tết nghỉ theo qui định.
- Được hưởng các quyền lợi theo Luật lao động, ...

III. YÊU CẦU HỒ SƠ DỰ TUYỂN:
- Sơ yếu lý lịch cá nhân tự thuật.
- Ảnh 3x4 số lượng 02 cái.
- Hồ sơ công chứng: Bằng, bảng điểm, các chứng chỉ liên quan, CMND
- Giấy chứng nhận sức khỏe, Giấy khai sinh (chấp nhận phô tô)
- Đơn xin đăng ký dự tuyển ghi rõ vị trí ứng tuyển và khả năng, kinh nghiệm làm việc của bản thân.

IV. ĐỊA CHỈ NHẬN HỒ SƠ:
- Liên hệ trong giờ hành chính qua các số máy:
- Tel: 0238 3799999 - Số máy lẻ: 515./Di động: 0947.957.789 (Chị Nhung)
- Nộp hồ sơ trực tiếp tại Lễ tân tầng 1 Bệnh viện Răng Hàm Mặt và Phẫu thuật tạo hình thẩm mỹ Thái Thượng Hoàng
- Đc: Số 28, đường Nguyễn Sỹ Sách, tp.Vinh, tỉnh Nghệ An.
- Hạn nộp hồ sơ: 15/10/2017.
- Ứng viên có thể nộp hồ sơ và CV trước qua mail: nhungnth@tthgroup.vn

Lưu ý: Công ty không trả lại hồ sơ cho các ứng viên không trúng tuyển.',
                'price' => NULL,
                'price_min' => NULL,
                'ads_type' => 'buy',
                'price_type' => 'treat',
                'state' => NULL,
                'status' => 'active',
                'address' => 'Số 28, đường Nguyễn Sỹ Sách, tp.Vinh, tỉnh Nghệ An',
                'lat' => 18.686419999999998,
                'lng' => 105.678607,
                'view_count' => 1,
                'images' => '["\\r\\n\\r\\n15665765-1867099813510079-9128419848343779849-n-1507687987.jpg","\\r\\n\\r\\n15776992-1867933243426736-7342548320919677617-o-1507687988.jpg"]',
                'created_at' => '2017-10-11 09:13:43',
                'updated_at' => '2017-10-11 09:44:20',
                'deleted_at' => NULL,
                'cate_id' => 108,
                'user_id' => 20,
            ),
            18 => 
            array (
                'id' => 19,
                'title' => 'Nhân viên Kinh doanh làm việc tại Thành phố Vinh',
                'slug' => 'nhan-vien-kinh-doanh-lam-viec-tai-thanh-pho-vinh',
                'des' => 'SỐ LƯỢNG: 04 người

VỊ TRÍ TUYỂN DỤNG: Nhân viên Kinh doanh làm việc tại Thành phố Vinh

MÔ TẢ CÔNG VIỆC:

- Nghiên cứu, đánh giá thị trường và lập kế hoạch kinh doanh.

- Tổng hợp, theo dõi thông tin thị trường, đối thủ cạnh tranh liên quan đến sản phẩm;

- Tiến hành thực hiện các chương trình marketing, bán hàng trực tiếp và quản lý, chăm sóc khách hàng.

[​IMG] YÊU CẦU:

- Nam/ nữ; tuổi từ 22 đến 28, các trường hợp có kinh nghiệm, tuổi đời không quá 30.

- Trình độ tốt nghiệp: Cao đẳng trở lên..

- Có kỹ năng tư vấn bán hàng, nắm bắt tâm lý và thuyết phục khách hàng, kỹ năng xử lý khiếu nại của khách hàng...

- Có tinh thần trách nhiệm cao, khả năng làm việc độc lập và làm việc nhóm.

- Trung thực, nhiệt tình và nhanh nhẹn; Tư duy tốt, có khả năng giao tiếp tốt.

QUYỀN LỢI:

- Ứng viên được chọn sẽ được làm việc trong môi trường chuyên nghiệp, phát triển, năng động có nhiều cơ hội thăng tiến.

- Chế độ lương, thưởng, chính sách đãi ngộ thu hút và hấp dẫn theo năng lực và kết quả công việc của từng cá nhân; được tham gia Bảo hiểm xã hội, Bảo hiểm y tế, Bảo hiểm thất nghiệp theo quy định của Luật BHXH.

NỘP HỒ SƠ:

- Ứng viên quan tâm vui lòng nộp Hồ sơ/ CV qua email: phanngocnacatv@gmail.com; Hoặc trực tiếp tại Văn phòng giao dịch VTVcab Nghệ An - Số 21 Nguyễn Thị Minh Khai - TP Vinh - Nghệ An.

- Thời hạn: Khuyến khích các ứng viên nộp hồ sơ sớm.

- Điện thoại liên hệ: 0979 699 536 (Ms. Ngọc); 0986 019 777 ( Ms. Thảo)',
                'price' => NULL,
                'price_min' => NULL,
                'ads_type' => 'buy',
                'price_type' => 'treat',
                'state' => NULL,
                'status' => 'active',
                'address' => 'Số 21 Nguyễn Thị Minh Khai - TP Vinh - Nghệ An.',
                'lat' => 18.676959,
                'lng' => 105.675723,
                'view_count' => 1,
                'images' => '["\\r\\n\\r\\ntruyenhinhcapnghean-1507688314.jpg"]',
                'created_at' => '2017-10-11 09:19:34',
                'updated_at' => '2017-10-11 09:44:10',
                'deleted_at' => NULL,
                'cate_id' => 108,
                'user_id' => 20,
            ),
            19 => 
            array (
                'id' => 20,
                'title' => 'Tuyển nhân viên kinh doanh',
                'slug' => 'tuyen-nhan-vien-kinh-doanh',
                'des' => 'Tuyển nhân viên kinh doanh: 01 người

1. Mô tả Công việc
a. Nhân viên kinh doanh: 
– Phát triển mạng lưới phân phối thực phẩm nhập khẩu trên khu vực Nghệ An – Hà Tĩnh
– Chủ động xác định và thiết lập mối quan hệ với khách hàng tiềm năng.
– Duy trì mối quan hệ với khách hàng mới và khách hàng hiện tại để cung cấp các chiến lược cụ thể trong quá trình quảng bá sản phẩm
– Giới thiệu những sản phẩm/dịch vụ tốt nhất, những phương án về giá cả, đặc trưng của dịch vụ và các chương trình khuyến mãi để đáp ứng yêu cầu của khách hàng
– Xây dựng và phát triển mối quan hệ tốt với khách hàng, thương lượng các điều khoản bán hàng với khách hàng
– Đạt các chỉ tiêu kinh doanh hàng ngày, hàng tuần và hàng tháng
– Đảm nhận các công việc hành chính khác như lập hợp đồng
– Quản lý tiền hàng và công nợ của khách hàng
– Làm các bảng báo cáo hàng ngày và hàng tháng.
– Thực hiện các công việc khác theo yêu cầu
2. Yêu Cầu Công Việc
a. Nhân viên kinh doanh: 
Tốt nghiệp Cao đẳng/ Đại học trở lên chuyên ngành Kinh tế
( Ưu tiên những người có kinh nghiệm trong lĩnh vực bán hàng thực phẩm, đồ uống…)
-Sử dụng thành thạo máy tính
-Kỹ năng giao tiếp, đàm phán và thuyết phục
– Có xe máy
– Giới tính: Không yêu cầu
– Hình thức: Nhân viên chính thức
– Hình thức làm việc: Giờ hành chính
3. Quyền lợi
– Mức lương Nhân viên KD: 7-10 triệu
– Phụ cấp tiền xăng, điện thoại, thưởng tháng, tăng lương định kỳ hàng năm, thăng tiến trong công việc.
– Được làm việc trong môi trường năng động, chuyên nghiệp;
– Được đóng BHXH, BHYT, BHTN .
THỜI GIAN THỬ VIỆC: 1 THÁNG hưởng 100% lương cơ bản
4. Thông tin liên hệ nộp hồ sơ:
– Người liên hệ: Ms Linh (0905.747.135) – Ứng viên Vui lòng liên hệ giờ hành chính, vui lòng không gửi SMS
– Địa chỉ: Số 17 - Ngõ 83 - Đường Nguyễn Đức Cảnh - Tp Vinh - Nghệ An
- Hồ sơ xin việc bao gồm (bản photo):
+ Sơ yếu lý lịch: Tóm tắt thông tin và quá trình làm việc 3 công việc gần nhất
+ Hộ khẩu
+ Chứng minh thư
– Ứng viên có thể gửi hồ sơ qua email: vietau.ltd@gmail.com
Nội dung chủ đề gửi email: Vị trí ứng tuyển_Tên ứng viên
VD: Nhân viên Kinh Doanh _ Nguyễn Văn A
(Các ứng viên vui lòng gửi kèm CV trong bộ hồ sơ xin việc)',
                'price' => '0',
                'price_min' => NULL,
                'ads_type' => 'buy',
                'price_type' => NULL,
                'state' => NULL,
                'status' => 'active',
                'address' => 'Ngõ 83 - Đường Nguyễn Đức Cảnh - Tp Vinh - Nghệ An',
                'lat' => 18.678298000000002,
                'lng' => 105.68092300000001,
                'view_count' => 1,
                'images' => NULL,
                'created_at' => '2017-10-11 09:24:13',
                'updated_at' => '2017-10-11 09:44:13',
                'deleted_at' => NULL,
                'cate_id' => 108,
                'user_id' => 20,
            ),
            20 => 
            array (
                'id' => 21,
                'title' => 'Tuyển dụng Giáo viên tiếng Nhật',
                'slug' => 'tuyen-dung-giao-vien-tieng-nhat',
                'des' => 'Công ty Cổ phần Hợp tác Quốc tế Jasa chuyên hoạt động trong lĩnh vực Tư vấn Du học, Đào tạo Nhật ngữ, Hợp tác quốc tế... Hiện tại Jasa cần tuyển dụng nhân sự một số vị trí sau:
1. Giáo viên tiếng Nhật: 1 người
1.1 Tiêu chuẩn dự tuyển
- Nam nữ từ 22 đến 40 tuổi.
- Tốt nghiệp chuyên ngành tiếng Nhật. Trình độ Nhật ngữ từ N3 trở lên.
- Có kinh nghiệm giảng dạy. Ưu tiên các ứng viên đã qua công tác giảng dạy.
- Nhiệt tình, chịu khó, tận tâm tận tuỵ với học sinh.

1.2 Quyền lợi được hưởng
- Lương cao (theo thỏa thuận) + thưởng theo quý + thưởng năm
- Môi trường làm việc năng động và chuyên nghiệp, cơ hội thăng tiến
- Được xét duyệt đi du lịch trong và ngoài nước
- Được hưởng đầy đủ chế độ lao động theo quy định của nhà nước CHXHCN Việt Nam

Công việc sẽ được trao đổi cụ thể trong buổi phỏng vấn. Ưu tiên phỏng vấn sớm các hồ sơ đã nộp.

Ứng viên viết một email xin việc gửi đến nhà tuyển dụng kèm theo file hồ sơ xin việc (CV) gửi vào địa chỉ email: tuyendung@jasa.edu.vn

Lưu ý: 
- Ghi rõ vị trí tuyển dụng
- Chỉ phỏng vấn hồ sơ đạt yêu cầu
------------------------------------
Hoặc nộp hồ sơ trực tiếp tại:

PHÒNG HÀNH CHÍNH - NHÂN SỰ - CÔNG TY CỔ PHẦN HỢP TÁC QUỐC TẾ JASA

Địa chỉ: Số 45 Km3, Đại lộ Lê Nin, thành phố Vinh, tỉnh Nghệ An

Điện thoại: 02383.852.999 (gặp chị Hiệp)

Hotline: 0965666123 
------------------------------------
Thời gian nhận hồ sơ từ nay đến ngày 15/10/2017

Không trả lại hồ sơ đã nộp với bất kỳ lý do gì.',
                'price' => NULL,
                'price_min' => NULL,
                'ads_type' => 'buy',
                'price_type' => 'treat',
                'state' => NULL,
                'status' => 'active',
                'address' => 'Số 45 Km3, Đại lộ Lê Nin, thành phố Vinh, tỉnh Nghệ An',
                'lat' => 18.697555999999999,
                'lng' => 105.68555000000001,
                'view_count' => 1,
                'images' => '["\\r\\n\\r\\n22122014-chiatay9-1507688868.jpg","\\r\\n\\r\\n13102014-kobejla2-1507688867.jpg","\\r\\n\\r\\n25042015-ct3-1507688869.jpg"]',
                'created_at' => '2017-10-11 09:28:07',
                'updated_at' => '2017-10-11 09:44:03',
                'deleted_at' => NULL,
                'cate_id' => 133,
                'user_id' => 20,
            ),
            21 => 
            array (
                'id' => 22,
                'title' => 'TUYỂN DỤNG NHÂN VIÊN HÀNH CHÍNH HỖ TRỢ KINH DOANH',
                'slug' => 'tuyen-dung-nhan-vien-hanh-chinh-ho-tro-kinh-doanh',
                'des' => 'a. Số lượng: 01 người.
b. Mô tả công việc:
+ Thực hiện các công tác hành chánh, công tác đối ngoại, soạn thảo văn bản; tổng hợp doanh số cho Phòng kinh doanh
+ Tham mưu, giúp việc cho Cán bộ quản lý và trực tiếp thực hiện các công việc liên quan đến pháp lý, hành chánh…
+ Quản lý, tổ chức thực hiện công tác văn thư hành chính; quản trị cơ sở vật chất; văn phòng phẩm; phòng cháy chữa cháy;
+ Lập báo cáo theo yêu cầu của Ban lãnh đạo Công ty và Trưởng phòng;
+ Các công việc khác sẽ trao đổi trực tiếp nếu được tuyển dụng.
c. Yêu cầu: 
+ Giới tính: Nam, Nữ;
+ Kinh nghiệm: đã có kinh nghiệm trong công tác hành chính nhân sự hoặc làm việc ở vị trí tương đương ít nhất 01 năm trở lên.
+ Thành thạo kỹ năng tin học văn phòng và tìm kiếm khai thác thông tin trên hệ thống internet.
+ Tư cách đạo đức tốt, năng động, sáng tạo, trung thực, nhiệt tình trong công việc; sức khỏe tốt.
+ Tốt nghiệp Đại học chính quy trở lên chuyên ngành: Quản trị nhân lực, Luật, Quan hệ lao động, Kinh tế lao động; quản trị kinh doanh, tài chính ngân hàng hoặc các chuyên ngành liên quan;
d. Quyền lợi:
+ Được làm việc trong môi trường năng động, chuyên nghiệp.
+ Cơ hội thăng tiến không hạn chế; nâng cao nghiệp vụ thường xuyên.
+ Được cung cấp các trang thiết bị hiện đại cần thiết để nâng cao hiệu quả làm việc.
+ Lương cứng: 5.500.000 đồng - 6.000.000 đồng + Thưởng doanh số theo quý, theo năm theo phòng kinh doanh.
+ Hưởng các chế độ đãi ngộ theo luật lao động như: BHXH, BHYT, BHTN và các chế độ phúc lợi mở rộng khác như: sinh nhật, thăm quan, du lịch ...
f. Hồ sơ:
+ CV tóm tắt quá trình học tập và làm việc;
+ Các bằng cấp có liên quan (Bản photo).
e. Hồ sơ gửi về:
+ Gửi qua email: khanhlq@goldencity.com
+ Hoặc nộp hồ sơ trực tiếp tại:
Tầng 3, khách sạn Mường Thanh Phương Đông, số 02 đường Trường Thi, phường Trường Thi, Tp. Vinh, tỉnh Nghệ An (gặp trực tiếp Ms Lê Na - Phòng Kinh doanh 2)
Mọi thắc mắc liện hệ: 0945.399.933
Hạn nộp hồ sơ: 30/10/2017.

Lưu ý: Ứng viên chỉ cần nộp hồ sơ photo, hoặc gửi CV ứng tuyển vào địa chỉ mail: khanhlq@goldencity.com',
                'price' => '0',
                'price_min' => NULL,
                'ads_type' => 'buy',
                'price_type' => NULL,
                'state' => NULL,
                'status' => 'active',
                'address' => 'số 02 đường Trường Thi, phường Trường Thi, Tp. Vinh, tỉnh Nghệ An',
                'lat' => 18.67014,
                'lng' => 105.691846,
                'view_count' => 1,
                'images' => '["\\r\\n\\r\\n21688441-1490544057694813-5081215168527600411-o-1507689156.jpg","\\r\\n\\r\\n21768836-1490543977694821-8576412484738807541-o-1507689158.jpg","\\r\\n\\r\\n15590968-1199543790128176-4530494590008672030-o-1507689159.jpg","\\r\\n\\r\\n15591062-1199543916794830-5261102470503197774-o-1507689159.jpg"]',
                'created_at' => '2017-10-11 09:33:32',
                'updated_at' => '2017-10-11 09:44:07',
                'deleted_at' => NULL,
                'cate_id' => 108,
                'user_id' => 20,
            ),
            22 => 
            array (
                'id' => 23,
                'title' => 'NHÂN VIÊN KINH DOANH',
                'slug' => 'nhan-vien-kinh-doanh-2',
                'des' => 'a. Số lượng cần tuyển: 05 Người
+ Giới tính: nam, nữ
b. Mô tả:
- Thực hiện kế hoạch bán hàng của Công ty;
- Khảo sát thị trường, xác định cung - cầu. Thu thập thông tin về khách hàng, đối thủ cạnh tranh;
- Khảo sát thực tế nhu cầu của khách hàng về lĩnh vực kinh doanh của Công ty;
- Các công việc khác do cấp trên phân công.
c. Yêu cầu:
- Tốt nghiệp Cao Đẳng, Đại học chính quy trở lên các ngành Kinh tế, Ngân hàng, Tài chính, Kế toán, Xây dựng;
- Ngoại hình ưa nhìn
Ưu tiên: Các ứng viên đã làm ở các Công ty kinh doanh bất động sản, hoặc sale ô tô, ngân hàng, bán hàng online.
d. Quyền lợi:
+ Được làm việc trong môi trường năng động, chuyên nghiệp.
+ Cơ hội thăng tiến không hạn chế; nâng cao nghiệp vụ thường xuyên.
+ Được cung cấp các trang thiết bị hiện đại cần thiết để nâng cao hiệu quả làm việc.
+ Lương cứng: 6.200.000 đồng - 8.500.000 đồng (tùy theo năng lực và kinh nghiệm làm việc) + Thưởng doanh số theo quý, theo năm theo phòng kinh doanh.
+ Hưởng các chế độ đãi ngộ theo luật lao động như: BHXH, BHYT, BHTN và các chế độ phúc lợi mở rộng khác như: sinh nhật, thăm quan, du lịch ...
f. Hồ sơ:
+ CV tóm tắt quá trình học tập và làm việc;
+ Các bằng cấp có liên quan (Bản photo).
e. Hồ sơ gửi về:
+ Gửi qua email: khanhlq@goldencity.com
+ Hoặc nộp hồ sơ trực tiếp tại:
Tầng 3, khách sạn Mường Thanh Phương Đông, số 02 đường Trường Thi, phường Trường Thi, Tp. Vinh, tỉnh Nghệ An (gặp trực tiếp Ms Lê Na - Phòng Kinh doanh 2)
Mọi thắc mắc liện hệ: 0945.399.933
Hạn nộp hồ sơ: 30/10/2017.

Lưu ý: Ứng viên chỉ cần nộp hồ sơ photo, hoặc gửi CV ứng tuyển vào địa chỉ mail: khanhlq@goldencity.com',
                'price' => '0',
                'price_min' => NULL,
                'ads_type' => 'buy',
                'price_type' => NULL,
                'state' => NULL,
                'status' => 'active',
                'address' => 'số 02 đường Trường Thi, phường Trường Thi, Tp. Vinh, tỉnh Nghệ An',
                'lat' => 18.67014,
                'lng' => 105.691846,
                'view_count' => 1,
                'images' => '["\\r\\n\\r\\n15591062-1199543916794830-5261102470503197774-o-1507689414.jpg","\\r\\n\\r\\n21688441-1490544057694813-5081215168527600411-o-1507689410.jpg","\\r\\n\\r\\n21768836-1490543977694821-8576412484738807541-o-1507689411.jpg","\\r\\n\\r\\n15590968-1199543790128176-4530494590008672030-o-1507689412.jpg"]',
                'created_at' => '2017-10-11 09:37:30',
                'updated_at' => '2017-10-11 09:44:00',
                'deleted_at' => NULL,
                'cate_id' => 108,
                'user_id' => 20,
            ),
            23 => 
            array (
                'id' => 24,
                'title' => 'Nhân viên kinh doanh website chovinh.com',
                'slug' => 'nhan-vien-kinh-doanh-website-chovinh-com',
                'des' => '1. Vị trí tuyển dụng: nhân viên kinh doanh website chovinh.com
2. Mô tả công việc.
- Tìm kiếm, tiếp cận các khách hàng tiềm năng để tư vấn thu hút khách hàng
- Theo dõi và chăm sóc khách hàng
- Thu hồi công nợ
- Hỗ trợ công việc phòng HC-NS của Công ty Golden City
- Chi tiết công việc trao đổi khi phỏng vấn.
3. Quyền lợi:
- Mức lương: 4 - 4,5 triệu/1 tháng.
- Thưởng theo doanh thu của website
Tất cả nhân viên sau thời gian thử việc được Công ty đóng BHXH và được hưởng các chế độ theo quy định của Luật Lao động, Luật BHXH.... Thưởng tết và các ngày lễ trọng đại của dân tộc.
4. Yêu cầu:
- Yêu cầu nhân viên đã có kinh nghiệm trong lĩnh vực liên quan
- Có kinh nghiệm tư vấn bán hàng, thuyết phục khách hàng
- Khai thác tốt những thông tin trên Internet và các mối quan hệ xã hội.
- Giao tiếp tốt, nhanh nhẹn trong xử lý tình huống
- Kỹ năng sales online tốt.
- Am hiểu về SEO hoặc đã từng sử quản trị website là một lợi thế.
- Có tính cẩn thận, trung thực, năng động, nhiệt tình, siêng năng, nhiệt huyết trong công việc luôn cố gắng học hỏi.
5. Thời gian, địa điểm nhận hồ sơ: 
- Thời gian: đến hết ngày 30/09/2017
Buổi sáng : từ 08h đến 12h00
Buổi chiều: từ 13h30 giờ 00 đến 17h30 giờ 30 (trừ chiều thứ 7 và ngày CN)
- Địa điểm nhận hồ sơ: Phòng HC-NS - Công ty CP Golden City, Tầng 3, KS Phương Đông, Số 02 Trường Thi, Tp Vinh, Nghệ An. (Anh Hoàng: 0916.079997).
Lưu ý: Ứng viên có thể nộp hồ sơ photo, hoặc gửi CV ứng tuyển vào địa chỉ mail: hoangdd@goldencity.com. Ứng viên không trúng tuyển, không trả lại hồ sơ.',
                'price' => '0',
                'price_min' => NULL,
                'ads_type' => 'buy',
                'price_type' => NULL,
                'state' => NULL,
                'status' => 'active',
                'address' => 'Số 02 Trường Thi, Tp Vinh, Nghệ An',
                'lat' => 18.67014,
                'lng' => 105.691846,
                'view_count' => 1,
                'images' => '["\\r\\n\\r\\n15591062-1199543916794830-5261102470503197774-o-1507689632.jpg","\\r\\n\\r\\n21688441-1490544057694813-5081215168527600411-o-1507689629.jpg","\\r\\n\\r\\n21768836-1490543977694821-8576412484738807541-o-1507689630.jpg","\\r\\n\\r\\n15590968-1199543790128176-4530494590008672030-o-1507689631.jpg"]',
                'created_at' => '2017-10-11 09:41:15',
                'updated_at' => '2017-10-11 09:43:33',
                'deleted_at' => NULL,
                'cate_id' => 165,
                'user_id' => 20,
            ),
            24 => 
            array (
                'id' => 25,
                'title' => 'NHÂN VIÊN KINH DOANH',
                'slug' => 'nhan-vien-kinh-doanh-3',
                'des' => '1. NHÂN VIÊN KINH DOANH:
1.1. Số lượng cần tuyển: 10 người.
1.2. Mô tả:
- Thực hiện kế hoạch bán hàng của Công ty;
- Khảo sát thị trường, xác định cung - cầu. Thu thập thông tin về khách hàng, đối thủ cạnh tranh;
- Khảo sát thực tế nhu cầu của khách hàng về lĩnh vực kinh doanh của Công ty;
- Các công việc khác do cấp trên phân công.
1.3. Yêu cầu:
- Tốt nghiệp Cao đẳng trở lên các ngành Kinh tế, Ngân hàng, Tài chính, Kế toán, 
- Ngoại hình ưu nhìn
- Ưu tiên: Các ứng viên đã có kinh nghiệm làm việc
1.4. Quyền lợi:
- Được làm việc trong môi trường năng động, chuyên nghiệp.
- Cơ hội thăng tiến không hạn chế; nâng cao nghiệp vụ thường xuyên.
- Lương cứng : 6.000.000đ đến 8.000.000đ và các chế độ phúc lợi mở rộng khác.
- Hưởng các chế độ đãi ngộ theo luật lao động như: BHXH, BHYT, BHTN …
- Các quyền lợi khác theo quy định của Công ty.
2. THỜI GIAN, ĐỊA ĐIỂM NHẬN HỒ SƠ:
- Thời gian: liên tục nhận hồ sơ tất cả các ngày trong tuần.
- Địa điểm nhận hồ sơ: Nhận hồ sơ qua địa chỉ mail: quangminh.qa@gmail.com
- Người liên hệ: Anh Trần Quang Minh. Trưởng Phòng Hành Chính Nhân Sự
- Số Điện Thoại liên hệ: 0971.286.387',
                'price' => '0',
                'price_min' => NULL,
                'ads_type' => 'buy',
                'price_type' => NULL,
                'state' => NULL,
                'status' => 'active',
                'address' => 'Thành phố Vinh, Nghệ An, Việt Nam',
                'lat' => 18.679584999999999,
                'lng' => 105.681333,
                'view_count' => 1,
                'images' => NULL,
                'created_at' => '2017-10-11 09:48:07',
                'updated_at' => '2017-10-11 10:35:34',
                'deleted_at' => NULL,
                'cate_id' => 108,
                'user_id' => 20,
            ),
            25 => 
            array (
                'id' => 26,
                'title' => 'TRƯỞNG PHÒNG BÁN HÀNG chuyên trách xe Huyndai Đồng Vàng',
                'slug' => 'truong-phong-ban-hang-chuyen-trach-xe-huyndai-dong-vang',
                'des' => 'Mô tả công việc:
- Quản lí nhóm bán hàng (3 - 5 TVBH)

- Triển khai các công việc bán hàng; chịu trách nhiệm chính về doanh số bán hàng.
- Lập kế hoạch kinh doanh, giao dịch đàm phán với khách hàng.
- Thiết lập mạng lưới kinh doanh, thu thập thông tin thị trường và phát triển kinh doanh

- Lập và duy trì các mối quan hệ khách hàng tiềm năng .

- Thu thập, tổng hợp thông tin về đối thủ và sản phẩm cạnh tranh
- Xây dựng kế hoạch kinh doanh định kỳ, lập các chương trình Marketing
Yêu cầu:
- Tốt nghiệp Đại học chính quy, chuyên ngành kinh tế, QTKD hoặc liên quan
- Ít nhất 2 năm kinh nghiệm trở lên ở vị trí tương đương hoặc có 3 năm kinh nghiệm là TVBH loại giỏi
- Có khả năng quản lý và xây dựng chiến lược kinh doanh cho Công ty
- Có khả năng làm việc độc lập, chủ động, chịu được áp lực công việc cao, năng động sáng tạo và có khả năng tổ chức quản lý tốt
- Ưu tiên những người giao tiếp tốt tiếng anh, có giấy phép lái xe.
- Sử dụng thành thạo vi tính, Power point

Mức lương tối thiểu: 8 triệu/tháng
QUYỀN LỢI ĐƯỢC HƯỞNG
- Được làm việc trong môi trường chuyên nghiệp, thân thiện, ổn định, thu nhập xứng đáng theo năng lực, được học hỏi nâng cao trình độ.
- Được xét tăng lương theo năng lực thực tế, cơ hội thăng tiến cao và phát triển sự nghiệp lâu dài
- Được hưởng các chế độ phúc lợi khác theo chính sách Công ty đối với người lao động và luật lao động Việt Nam hiện hành
HỒ SƠ BAO GỒM:
- Đơn xin việc làm (viết tay)
- Sơ yếu lý lịch. Giấy chứng nhận sức khỏe
- Bản sao hộ khẩu, CMND và các bằng cấp, chứng chỉ liên quan
ĐỊA ĐIỂM NỘP HỒ SƠ :
Tại phòng tổ chức hành chính tổng hợp công ty.
Địa chỉ: 72 Phan Bội Châu, TP Vinh, tỉnh Nghệ An
Điện thoại: 0238.8509889 - Fax 0238 3 585.395
Địachỉ Email:Phuonggmnghean@gmail.com - otonghean.com.vn
Hoặc Chị Trần Lan Phương - Phòng tổ chức HCTH : 0946.210.680',
                'price' => '0',
                'price_min' => NULL,
                'ads_type' => 'buy',
                'price_type' => NULL,
                'state' => NULL,
                'status' => 'active',
                'address' => '72 Phan Bội Châu, Tp. Vinh, Nghệ An, Việt Nam',
                'lat' => 18.686979000000001,
                'lng' => 105.671336,
                'view_count' => 1,
                'images' => NULL,
                'created_at' => '2017-10-11 09:51:20',
                'updated_at' => '2017-10-11 10:35:31',
                'deleted_at' => NULL,
                'cate_id' => 107,
                'user_id' => 20,
            ),
            26 => 
            array (
                'id' => 27,
                'title' => 'KỸ THUẬT VIÊN GÒ HÀN -SƠN',
                'slug' => 'ky-thuat-vien-go-han-son',
                'des' => 'Yêu cầu công việc:
- Nam: Tuổi từ 20 đến 30, có sức khỏe tốt, nhanh nhẹn, cần cù chịu khó và tư cách đạo đức tốt.
- Tốt nghiệp PTTH trở lên

- Có mong muốn được học và làm nghề gò sơn ô tô
- Năng động, nhiệt tình và làm việc có trách nhiệm, có tính xây dựng tập thể.
*Ưu tiên ứng viên có kinh nghiệm nghề gò sơn 1 năm trở lên và có bằng lái xe ô tô


Công ty nhận đào tạo cho nhứng người muốn theo học thợ gò và sơn ô tô, (Sau khóa đào tạo nếu cá nhân đạt tiêu chuẩn sẽ được tuyển dụng vào làm việc chính thức tại Công ty).',
                'price' => NULL,
                'price_min' => NULL,
                'ads_type' => 'buy',
                'price_type' => 'treat',
                'state' => NULL,
                'status' => 'active',
                'address' => '72 Phan Bội Châu, Tp. Vinh, Nghệ An, Việt Nam',
                'lat' => 18.686979000000001,
                'lng' => 105.671336,
                'view_count' => 1,
                'images' => NULL,
                'created_at' => '2017-10-11 09:53:51',
                'updated_at' => '2017-10-11 10:35:25',
                'deleted_at' => NULL,
                'cate_id' => 122,
                'user_id' => 20,
            ),
            27 => 
            array (
                'id' => 28,
                'title' => 'NHÂN VIÊN KINH DOANH',
                'slug' => 'nhan-vien-kinh-doanh-4',
                'des' => 'Mô tả công việc:
- Giới thiệu và tư vấn sản phẩm xe ôtô Huyndai Đồng Vàng

- Tìm kiếm, chăm sóc khách hàng tiềm năng để giới thiệu và bán xe ôtô
- Đạt danh số về bán hàng.
Yêu cầu công việc:
- Nam, Tuổi từ 22 trở lên đến 32, có ngoại hình ưa nhìn, nhanh nhẹn, đam mê công việc kinh doanh
- Có kỹ năng bán hàng, kỹ năng đàm phán và thiết lập mối quan hệ với khách hàng, kỹ năng giao tiếp, kỹ năng giải quyết vấn đề.
- Năng động, chịu khó, ham học hỏi, sẵn sàng đi công tác xa, mối quan hệ Xã hội rộng
- Sử dụng máy tính thành thạo (tin học văn phòng)
*Ưu tiên những người có kinh nghiệm trong lĩnh vực kinh doanh hoặc Tiếp thị và hiểu biết về ngành ô tô; có bằng lái xe
QUYỀN LỢI ĐƯỢC HƯỞNG
- Được làm việc trong môi trường chuyên nghiệp, thân thiện, ổn định, thu nhập xứng đáng theo năng lực, được học hỏi nâng cao trình độ.
- Được xét tăng lương theo năng lực thực tế, cơ hội thăng tiến cao và phát triển sự nghiệp lâu dài
- Được hưởng các chế độ phúc lợi khác theo chính sách Công ty đối với người lao động và luật lao động Việt Nam hiện hành
HỒ SƠ BAO GỒM:
- Đơn xin việc làm (viết tay)
- Sơ yếu lý lịch. Giấy chứng nhận sức khỏe
- Bản sao hộ khẩu, CMND và các bằng cấp, chứng chỉ liên quan
ĐỊA ĐIỂM NỘP HỒ SƠ :
Tại phòng tổ chức hành chính tổng hợp công ty.
Địa chỉ: 72 Phan Bội Châu, TP Vinh, tỉnh Nghệ An
Điện thoại: 0238.8509889 - Fax 0238 3 585.395
Địachỉ Email:Phuonggmnghean@gmail.com - otonghean.com.vn
Hoặc Chị Trần Lan Phương - Phòng tổ chức HCTH : 0946.210.680',
                'price' => NULL,
                'price_min' => NULL,
                'ads_type' => 'buy',
                'price_type' => 'treat',
                'state' => NULL,
                'status' => 'active',
                'address' => '72 Phan Bội Châu, Tp. Vinh, Nghệ An, Việt Nam',
                'lat' => 18.686979000000001,
                'lng' => 105.671336,
                'view_count' => 1,
                'images' => NULL,
                'created_at' => '2017-10-11 09:58:40',
                'updated_at' => '2017-10-11 10:35:22',
                'deleted_at' => NULL,
                'cate_id' => 108,
                'user_id' => 20,
            ),
            28 => 
            array (
                'id' => 29,
                'title' => 'Tư vấn bán hàng',
                'slug' => 'tu-van-ban-hang',
                'des' => 'Yêu cầu:
- Giới tính: Nam/Nữ
- Độ tuổi: 22-28
- Ngoại hình: ưa nhìn, Nam cao từ 1m68, Nữ từ 1m60
- Trình độ: Cao đẳng/Đại học các chuyên ngành du lịch, kinh doanh, tài chính, marketing, kỹ thuật ô tô,...
- Yêu thích lĩnh vực kinh doanh ô tô.
- Năng động, có tố chất kinh doanh, trung thực.
- Kỹ năng giao tiếp khá. Có khả năng giao tiếp tiếng Anh (trìnhđộ B).
- Sử dụng tốt phần mềm MS office.
CHẾ ĐỘ LÀM VIỆC, CHẾ ĐỘ ƯU ĐÃI :

· Mức lương nhận việc : Thỏa thuận.

· Công ty hỗ trợ 01 bữa cơm ca (ăn trưa).

· Tham gia BHXH, BHYT sau 2 tháng làm việc.

· Hỗ trợ nhà ở tập thể cho các CBCNV ngoại tỉnh, nhà xa.

· Được làm việc trong môi trường chuyên nghiệp, có cơ hội thăng tiến và phát triển nghề nghiệp.

· Các chế độ khác theo Quy định của Công ty.

HỒ SƠ XIN VIỆC GỒM:

· Đơn xin việc viết tay.

· Phiếu đăng ký dự tuyển theo mẫu Trường Hải Ô tô (đăng tải từ địa chỉ http://tuyendung.thaco.com.vn/tuyen-dung)

· Sơ yếu lý lịch ( Có xác nhận của địa phương, thời hạn không quá 3 tháng kể từ ngày nộp hồ sơ).

· Giấy khám sức khỏe (Có xác nhận của TTYT tuyến quận trở lên, thời hạn không quá 3 tháng).

· Giấy khai sinh ( Bản sao).

· CMND, Hộ khẩu ( Sao y công chứng).

· Các văn bằng chứng chỉ (Sao y công chứng).

· 02 ảnh ( 3x4).


Hồ sơ gửi về Phòng Nhân sự
Ms. Nguyễn Thị Thanh Huệ - 0941184248
Địa chỉ: Công ty TNHH MTV Trường Hải Nghệ An, Đường 46, xóm 3, Nghi Phú, Tp.Vinh, Nghệ An (Ngã tư sân bay)
Ứng viên có thể download Phiếu đăng ký tại websitehttp://tuyendung.thaco.com.vn/và gửi về Email:nguyenthithanhhue@thaco.com.vn
Điện thoại cơ quan: 0388.693.693 (liên hệ trong giờ hành chính)
Lưu ý: Có thể nhận hồ sơ phô tô và không trả lại hồ sơ sau phỏng vấn',
                'price' => NULL,
                'price_min' => NULL,
                'ads_type' => 'buy',
                'price_type' => 'treat',
                'state' => NULL,
                'status' => 'active',
                'address' => 'Đường 46, xóm 3, Nghi Phú, Tp.Vinh, Nghệ An',
                'lat' => 18.712268999999999,
                'lng' => 105.67920700000001,
                'view_count' => 1,
                'images' => NULL,
                'created_at' => '2017-10-11 10:07:48',
                'updated_at' => '2017-10-11 10:35:19',
                'deleted_at' => NULL,
                'cate_id' => 107,
                'user_id' => 20,
            ),
            29 => 
            array (
                'id' => 30,
                'title' => 'Kỹ thuật viên sơn, Kỹ thuật viên đồng',
                'slug' => 'ky-thuat-vien-son-ky-thuat-vien-dong',
                'des' => 'Yêu cầu
- Giới tính: Nam
- Độ tuổi: 20 - 30 tuổi
- Trình độ: Lao động phổ thông/ Trung cấp/ Cao đẳng Công nghệ ô tô
- Kinh nghiệm: Ưu tiên thợ có kinh nghiệm tại các xưởng/hãng ô tô.
- Có tuyển dụng sinh viên trung cấp, cao đẳng mới ra trường vào đào tạo vừa học vừa làm


CHẾ ĐỘ LÀM VIỆC, CHẾ ĐỘ ƯU ĐÃI :

· Mức lương nhận việc : Thỏa thuận.

· Công ty hỗ trợ 01 bữa cơm ca (ăn trưa).

· Tham gia BHXH, BHYT sau 2 tháng làm việc.

· Hỗ trợ nhà ở tập thể cho các CBCNV ngoại tỉnh, nhà xa.

· Được làm việc trong môi trường chuyên nghiệp, có cơ hội thăng tiến và phát triển nghề nghiệp.

· Các chế độ khác theo Quy định của Công ty.

HỒ SƠ XIN VIỆC GỒM:

· Đơn xin việc viết tay.

· Phiếu đăng ký dự tuyển theo mẫu Trường Hải Ô tô (đăng tải từ địa chỉ http://tuyendung.thaco.com.vn/tuyen-dung)

· Sơ yếu lý lịch ( Có xác nhận của địa phương, thời hạn không quá 3 tháng kể từ ngày nộp hồ sơ).

· Giấy khám sức khỏe (Có xác nhận của TTYT tuyến quận trở lên, thời hạn không quá 3 tháng).

· Giấy khai sinh ( Bản sao).

· CMND, Hộ khẩu ( Sao y công chứng).

· Các văn bằng chứng chỉ (Sao y công chứng).

· 02 ảnh ( 3x4).


Hồ sơ gửi về Phòng Nhân sự
Ms. Nguyễn Thị Thanh Huệ - 0941184248
Địa chỉ: Công ty TNHH MTV Trường Hải Nghệ An, Đường 46, xóm 3, Nghi Phú, Tp.Vinh, Nghệ An (Ngã tư sân bay)
Ứng viên có thể download Phiếu đăng ký tại websitehttp://tuyendung.thaco.com.vn/và gửi về Email:nguyenthithanhhue@thaco.com.vn
Điện thoại cơ quan: 0388.693.693 (liên hệ trong giờ hành chính)
Lưu ý: Có thể nhận hồ sơ phô tô và không trả lại hồ sơ sau phỏng vấn',
                'price' => NULL,
                'price_min' => NULL,
                'ads_type' => 'buy',
                'price_type' => 'treat',
                'state' => NULL,
                'status' => 'active',
                'address' => 'Đường 46, xóm 3, Nghi Phú, Tp.Vinh, Nghệ An',
                'lat' => 18.712268999999999,
                'lng' => 105.67920700000001,
                'view_count' => 1,
                'images' => NULL,
                'created_at' => '2017-10-11 10:09:22',
                'updated_at' => '2017-10-11 10:35:16',
                'deleted_at' => NULL,
                'cate_id' => 141,
                'user_id' => 20,
            ),
            30 => 
            array (
                'id' => 31,
                'title' => 'Kỹ sư dịch vụ',
                'slug' => 'ky-su-dich-vu',
                'des' => '1. Vị trí: Kỹ sư dịch vụ, Số lượng: 03 người
Yêu cầu:
Nam, dưới 33 tuổi.

- Tốt nghiệp Đại học trở lên các chuyên ngành kỹ thuật ô tô.
- Có kinh nghiệm thực tế sửa chữa xe ô tô du lịch.
- Khả năng phân tích, tổng hợp và tư duy hệ thống.
- Nhạy bén với sự thay đổi của môi trường kinh doanh DVPT.
- Khả năng chịu áp lực công việc.
- GPLX dấu B2 trở lên
- Khả năng giao tiếp, đọc hiểu tài liệu chuyên ngành bằng tiếng Anh.
- Sử dụng tốt phần mềm MS office
CHẾ ĐỘ LÀM VIỆC, CHẾ ĐỘ ƯU ĐÃI :

· Mức lương nhận việc : Thỏa thuận.

· Công ty hỗ trợ 01 bữa cơm ca (ăn trưa).

· Tham gia BHXH, BHYT sau 2 tháng làm việc.

· Hỗ trợ nhà ở tập thể cho các CBCNV ngoại tỉnh, nhà xa.

· Được làm việc trong môi trường chuyên nghiệp, có cơ hội thăng tiến và phát triển nghề nghiệp.

· Các chế độ khác theo Quy định của Công ty.

HỒ SƠ XIN VIỆC GỒM:

· Đơn xin việc viết tay.

· Phiếu đăng ký dự tuyển theo mẫu Trường Hải Ô tô (đăng tải từ địa chỉ http://tuyendung.thaco.com.vn/tuyen-dung)

· Sơ yếu lý lịch ( Có xác nhận của địa phương, thời hạn không quá 3 tháng kể từ ngày nộp hồ sơ).

· Giấy khám sức khỏe (Có xác nhận của TTYT tuyến quận trở lên, thời hạn không quá 3 tháng).

· Giấy khai sinh ( Bản sao).

· CMND, Hộ khẩu ( Sao y công chứng).

· Các văn bằng chứng chỉ (Sao y công chứng).

· 02 ảnh ( 3x4).


Hồ sơ gửi về Phòng Nhân sự
Ms. Nguyễn Thị Thanh Huệ - 0941184248
Địa chỉ: Công ty TNHH MTV Trường Hải Nghệ An, Đường 46, xóm 3, Nghi Phú, Tp.Vinh, Nghệ An (Ngã tư sân bay)
Ứng viên có thể download Phiếu đăng ký tại websitehttp://tuyendung.thaco.com.vn/và gửi về Email:nguyenthithanhhue@thaco.com.vn
Điện thoại cơ quan: 0388.693.693 (liên hệ trong giờ hành chính)
Lưu ý: Có thể nhận hồ sơ phô tô và không trả lại hồ sơ sau phỏng vấn',
                'price' => NULL,
                'price_min' => NULL,
                'ads_type' => 'buy',
                'price_type' => 'treat',
                'state' => NULL,
                'status' => 'active',
                'address' => 'Đường 46, xóm 3, Nghi Phú, Tp.Vinh, Nghệ An',
                'lat' => 18.712268999999999,
                'lng' => 105.67920700000001,
                'view_count' => 1,
                'images' => NULL,
                'created_at' => '2017-10-11 10:09:26',
                'updated_at' => '2017-10-11 10:35:12',
                'deleted_at' => NULL,
                'cate_id' => 141,
                'user_id' => 20,
            ),
            31 => 
            array (
                'id' => 32,
                'title' => 'NHÂN VIÊN KINH DOANH',
                'slug' => 'nhan-vien-kinh-doanh-5',
                'des' => '- Số lượng 10 người
Mô tả công việc:

· Tìm kiếm khách hàng tiềm năng để giới thiệu & tư vấn bán hàng.

· Chủ động liên hệ & thiết lập những mối quan hệ kinh doanh tốt với khách hàng.

· Lập kế hoạch bán hàng và triển khai theo tuần/tháng/quý, hoàn thành các chỉ tiêu được giao.

· Lập phương án, Kế hoạch khai thác& phát triển thị trường nhằm đạt doanh số cao nhất.

· Đàm phán, tư vấn, ký kết Hợp đồng với khách hàng.

Yêu cầu công việc:

· Nam – Nữ, tuổi từ 21, tốt nghiệp TC trở lên các khối Kinh tế, QTKD hoặc Kỹ thuật ô tô.

· Ưu tiên ứng viên có kinh nghiệm trong ngành kinh doanh ôtô.

· Có kỹ năng giao tiếp tốt, kỹ năng thuyết phục khách hàng.

· Có khả năng làm việc lâu dài tại công ty.

· Chịu áp lực cao trong công việc. Cẩn thận, chăm chỉ, nhiệt tình với công việc.

Quyền lợi được hưởng:

· Mức lương: Lương cơ bản (4.000.000) + Thưởng doanh số (10.000.000) 

· Phụ cấp: Ăn trưa + Điện thoại

· Môi trường làm việc cởi mở, năng động, chuyên nghiệp.

· Đồng nghiệp nhiệt tình, thân thiện.

· Được đào tạo thêm để nâng cao nghiệp vụ chuyên môn.

· Các chế độ BHXH, BHYT theo Luật lao động.

· Các chế độ đãi ngộ, phúc lợi hấp dẫn của công ty.

Hồ sơ yêu cầu:

· CV, Đơn xin việc (Viết tay).

· Sơ yêu lý lịch, ghi rõ kinh nghiệm & quá trình của bản thân.

· Bản sao Giấy khám sức khỏe.

· Bản sao các văn bằng, chứng chỉ (Photo công chứng).

Liên hệ nộp HS:

CÔNG TY CP QUỐC TẾ HMT VIỆT NAM - CHI NHÁNH NGHỆ AN

Địa chỉ: Tầng 7, Tháp A, Tòa nhà Dầu Khí, Số 7 đường Quang Trung, tp. Vinh, Nghệ An

Nộp trực tiếp hoặc gửi qua Email: doanmanhhung.hmtvietnam@gmail.com

Người liên hệ:Mr. Hưng (0971.811.189) – Website: hmt.com.vn',
                'price' => '0',
                'price_min' => NULL,
                'ads_type' => 'buy',
                'price_type' => NULL,
                'state' => NULL,
                'status' => 'active',
                'address' => 'Tòa nhà Dầu Khí, Số 7 đường Quang Trung, tp. Vinh, Nghệ An',
                'lat' => 18.667435000000001,
                'lng' => 105.67369600000001,
                'view_count' => 1,
                'images' => NULL,
                'created_at' => '2017-10-11 10:11:57',
                'updated_at' => '2017-10-11 10:35:10',
                'deleted_at' => NULL,
                'cate_id' => 108,
                'user_id' => 20,
            ),
            32 => 
            array (
                'id' => 33,
                'title' => 'Kỹ sư thiết kế cơ khí',
                'slug' => 'ky-su-thiet-ke-co-khi',
                'des' => '· Nội dung công việc: Kỹ sư thiết kế cơ khí

· Số lượng: 01 người (Không phân biệt nam nữ).

· Yêu cầu: Các ửng cử viên đã tốt nghiệp các trường đại học kỹ thuật, chuyên ngành thiết kế cơ khí.

· Điều kiện làm việc 

· Thu nhập: 
Thời gian thử việc: 3,000,000 VNĐ/Tháng 
(đây là mức thu nhập áp dụng cho nhân viên mới ra trường, nhân viên có kinh nghiệm và năng lực sẽ thỏa thuận mức thu nhập khi phỏng vấn)

(Chưa bao gồm: Trợ cấp tiền ăn, Tiền làm thêm-tăng ca, trợ cấp xăng xe đi làm, phụ cấp trách nhiệm, phụ cấp tiếng Nhật, chi phí bảo hiểm xã hội).

· ＋Lương khởi điểm: : Thời gian thử việc: 3,000,000 VNĐ/Tháng 

(Chưa bao gồm phụ cấp, bảo hiểm). Khi ký hợp đồng chính thức sẽ quyết định mức lương dựa trên năng lực

＋Phụ cấp đi làm chuyên cần 400,000 VNĐ (Nếu đi đủ công trong 01 tháng).

＋ Khuyến khích làm việc: được đánh giá hàng tháng

＋ Phụ cấp trách nhiệm (khi có chức vụ)

＋Trợ cấp cơm ca (Hiện tại cá nhận chịu 5.000 VNĐ; Công ty Chịu 10.000VNĐ/suất ăn).

＋ Phụ cấp tiếng Nhật (N1=2.000.000VND/tháng, N2=1.200.000 VND/tháng, N3=800.000 VND/tháng, N4=400.000 VND/tháng, N5=200.000 VND/tháng). 


＋ Tiền làm thêm, tăng ca: Tính theo luật lao động Việt Nam

＋ Trợ cấp đi làm (Được thanh toán tiền xăng xe thực tế sử dụng từ chỗ ở đến chỗ làm việc theo tiêu chuẩn xe Wave của Honda).

· Được đóng bảo hiểm xã hội, thất nghiệp, ytế và các chế độ khác(Theo quy định hiện hành tại từng thời điểm).

· Nơi làm việc: Xã Châu Cường, huyện Quỳ Hợp, tỉnh Nghệ An

Làm việc theo giờ hành chính 8 tiếng/ngày

Mùa đông: 07.00 – 17.00 (Nghỉ trưa: 11.30 – 13.30).

Mùa hè: 07.00 – 17.30 (Nghỉ trưa: 11.30 – 14.00).

Thông thường làm theo giờ giấc trên, tuy nhiên tùy tình hình công việc có thể làm theo ca:

Ca 1: 07:00～15:00

Ca 2: 14.00～22.00

· Công việc làm: thiết kế bản vẽ cơ khí Và các việc khác công ty giao. 

· Một tháng được nghỉ 06 ngày: 02 ngày thứ 7 và 04 ngày Chủ Nhật; và các ngày nghỉ lễ theo quy định hiện hành.

· Thưởng và tăng lương: 01 lần/năm dựa theo năng lực và cống hiến.

· Thời gian thử việc: 02 tháng (Tháng đầu: 85% lương cơ bản, tháng thứ 2: 100% lương cơ bản).

2. Hồ sơ dự tuyển: 

· Bản sơ yếu lý lịch (bản chính có xác nhận UBND xã).

· Bằng tốt ngiệp, bảng điểm.(Bản sao có công chứng).

· Giấy khai sinh (Bản sao).

· Đơn xin việc (bản chính có xác nhận).

· Giấy khám sức khỏe.

· Ảnh 2 tấm (4cm x 6cm).

· Chứng minh nhân dân (Bản sao công chứng)

· Và số điện thoại có thể liên lạc được. 

3. Kế hoạch thi và phỏng vấn 

· Thời hạn nộp hồ sơ đến hết ngày: 15 tháng 10 năm 2017.

· Thời gian thi tuyển: Sẽ thông báo sau

· Thời gian phỏng vấn: Sẽ thông báo sau

· Địa điểm: Sẽ thông báo sau

· Hồ sơ nộp trực tiếp về: 

Công ty Khai thác đá vôi Yabashi Việt Nam

Đ/C: Xã Châu Cường, huyện Quỳ Hợp, tỉnh Nghệ An

Hoặc:Trung tâm VinhCAD (Công ty tại 146 Nguyễn Sỹ Sách, TP Vinh; 

Đ/C: 146 Nguyễn Sỹ Sách, TP Vinh, tỉnh Nghệ An 

· Chi tiết liên hệ: 

Mr Trần Hữu Lâm, Phó phòng Hành chính – Kế toán – Quản trị

Ms Nguyễn Thị Hà, Nhân viên Hành chính

ĐT: 0383.981180/0383.981181

DĐ: Mr Lâm: 0913060648 or 01648939399; Ms Hà: 0983 888 519',
                    'price' => '0',
                    'price_min' => NULL,
                    'ads_type' => 'buy',
                    'price_type' => NULL,
                    'state' => NULL,
                    'status' => 'active',
                    'address' => '146 Nguyễn Sỹ Sách, Tp. Vinh, Nghệ An, Việt Nam',
                    'lat' => 18.686634000000002,
                    'lng' => 105.690174,
                    'view_count' => 1,
                    'images' => NULL,
                    'created_at' => '2017-10-11 10:20:21',
                    'updated_at' => '2017-10-11 10:35:06',
                    'deleted_at' => NULL,
                    'cate_id' => 122,
                    'user_id' => 20,
                ),
                33 => 
                array (
                    'id' => 34,
                    'title' => 'NHÂN VIÊN RỬA XE',
                    'slug' => 'nhan-vien-rua-xe',
                    'des' => 'Số lượng: 01

Công việc:
+ rửa xe trưng bày và rửa các xe khách vào xưởng sửa chữa.
+ kiêm nhiệm thêm 1 số công việc lau dọn trong khu vực làm việc của công ty

Yêu cầu:
+ Nam, khỏe mạnh nhanh nhẹn
+ Trung thực, thật thà
+ Không yêu cầu kinh nghiệm

3. THỢ MÁY GẦM
Số lượng: 01

Yêu cầu:
- Có kinh nghiệm trong lĩnh vực sửa chữa ô-tô từ 01 năm trở lên, ưu tiên ứng viên dưới 35 tuổi.
- Tháo lắp các bộ phận chi tiết xe thuần thục chính xác không cần sự hướng dẫn của thợ chính.
- Nhanh nhẹn chịu khó, trung thực, làm việc thận trọng có trách nhiệm trong công việc được giao.

Lưu ý: thợ gầm máy có thể liên hệ trực tiếp với Mr.Hoàn phụ trách bộ phận dịch vụ, đt 0912185445',
                    'price' => NULL,
                    'price_min' => NULL,
                    'ads_type' => 'buy',
                    'price_type' => 'treat',
                    'state' => NULL,
                    'status' => 'active',
                    'address' => 'Thành phố Vinh, Nghệ An, Việt Nam',
                    'lat' => 18.679584999999999,
                    'lng' => 105.681333,
                    'view_count' => 1,
                    'images' => NULL,
                    'created_at' => '2017-10-11 10:25:44',
                    'updated_at' => '2017-10-11 10:35:03',
                    'deleted_at' => NULL,
                    'cate_id' => 149,
                    'user_id' => 20,
                ),
                34 => 
                array (
                    'id' => 35,
                    'title' => 'THỢ SƠN Ô TÔ',
                    'slug' => 'tho-son-o-to',
                    'des' => 'Yêu cầu:
- Sơn sửa các loại xe ô tô theo yêu cầu
- Làm đồng thân vỏ xe tốt
- Pha màu bắn màu tốt.
- Ít nhất 1 năm kinh nghiệm, có tay nghề cao',
                    'price' => NULL,
                    'price_min' => NULL,
                    'ads_type' => 'buy',
                    'price_type' => 'treat',
                    'state' => NULL,
                    'status' => 'active',
                    'address' => 'Nghệ An, Việt Nam',
                    'lat' => 19.234248999999998,
                    'lng' => 104.920036,
                    'view_count' => 1,
                    'images' => NULL,
                    'created_at' => '2017-10-11 10:27:10',
                    'updated_at' => '2017-10-11 10:35:28',
                    'deleted_at' => NULL,
                    'cate_id' => 149,
                    'user_id' => 20,
                ),
                35 => 
                array (
                    'id' => 36,
                    'title' => 'NHÂN VIÊN KHO PHỤ TÙNG',
                    'slug' => 'nhan-vien-kho-phu-tung',
                    'des' => 'Yêu câu:
- Tốt nghiệp chuyên ngành ô tô (trung cấp trở lên)
- Sử dụng thành thạo máy tính và các phần mềm văn phòng
- Hiểu biết về ô tô
Quyền lợi:
+ Được đào tạo nâng cao về các kỹ năng nghề nghiệp và cơ hội thăng tiến cao.
+ Được tham gia BHXH, BHYT
+ Được nghỉ Lễ, Tết theo chế độ của luật lao động

CHẾ ĐỘ LÀM VIỆC, CHẾ ĐỘ ƯU ĐÃI :
- Mức lương: Thỏa thuận.
- Công ty hỗ trợ cơm trưa
- Thưởng Lễ Tết và các chế độ khác của Công ty.

HỒ SƠ XIN VIỆC GỒM:
- Lý lịch công việc
- Các văn bằng chứng chỉ (Sao y công chứng).
- Các loại giấy tờ khác có thể bổ sung sau phỏng vấn.
(Riêng ứng viên ứng tuyển vị trí nhân viên kinh doanh cần nộp đơn ứng tuyển theo mẫu sau http://bit.ly/donungtuyen-chevroletvinh ) 

Hạn nộp hồ sơ: Đến hết ngày 06/10/2017 (ưu tiên ứng viên nạp hồ sớm)
Thời gian phỏng vấn: 07/10/2017
Hồ sơ gửi về Phòng Nhân sự
Mr. Nguyễn Phi Nam
SHOWROOM CHEVROLET VINH - Công ty TNHH Hợp tác Kinh tế VIỆT MỸ
Địa chỉ: Km01 Ngã ba quán Bàu – đầu đường 72m (rẽ phải đường 72m, bên cạnh chung cư dầu khí), Thành Phố Vinh – Tỉnh Nghệ An.
Hoặc gửi về Email: nguyenphinam@chevroletvinh.com.vn
Điện thoại: 0902.151.368 (vui lòng không nhắn tin)
Lưu ý: không trả lại hồ sơ sau phỏng vấn',
                    'price' => NULL,
                    'price_min' => NULL,
                    'ads_type' => 'buy',
                    'price_type' => 'treat',
                    'state' => NULL,
                    'status' => 'active',
                    'address' => 'Thành phố Vinh, Nghệ An, Việt Nam',
                    'lat' => 18.679584999999999,
                    'lng' => 105.681333,
                    'view_count' => 1,
                    'images' => NULL,
                    'created_at' => '2017-10-11 10:29:24',
                    'updated_at' => '2017-10-11 10:35:00',
                    'deleted_at' => NULL,
                    'cate_id' => 124,
                    'user_id' => 20,
                ),
                36 => 
                array (
                    'id' => 37,
                    'title' => 'PHỤ BẾP KIÊM TẠP VỤ',
                    'slug' => 'phu-bep-kiem-tap-vu',
                    'des' => 'Yêu cầu: chăm chỉ , thật thà
Quyền lợi:
+ Được đào tạo nâng cao về các kỹ năng nghề nghiệp và cơ hội thăng tiến cao.
+ Được tham gia BHXH, BHYT
+ Được nghỉ Lễ, Tết theo chế độ của luật lao động

CHẾ ĐỘ LÀM VIỆC, CHẾ ĐỘ ƯU ĐÃI :
- Mức lương: Thỏa thuận.
- Công ty hỗ trợ cơm trưa
- Thưởng Lễ Tết và các chế độ khác của Công ty.

HỒ SƠ XIN VIỆC GỒM:
- Lý lịch công việc
- Các văn bằng chứng chỉ (Sao y công chứng).
- Các loại giấy tờ khác có thể bổ sung sau phỏng vấn.
(Riêng ứng viên ứng tuyển vị trí nhân viên kinh doanh cần nộp đơn ứng tuyển theo mẫu sau http://bit.ly/donungtuyen-chevroletvinh ) 

Hạn nộp hồ sơ: Đến hết ngày 06/10/2017 (ưu tiên ứng viên nạp hồ sớm)
Thời gian phỏng vấn: 07/10/2017
Hồ sơ gửi về Phòng Nhân sự
Mr. Nguyễn Phi Nam
SHOWROOM CHEVROLET VINH - Công ty TNHH Hợp tác Kinh tế VIỆT MỸ
Địa chỉ: Km01 Ngã ba quán Bàu – đầu đường 72m (rẽ phải đường 72m, bên cạnh chung cư dầu khí), Thành Phố Vinh – Tỉnh Nghệ An.
Hoặc gửi về Email: nguyenphinam@chevroletvinh.com.vn
Điện thoại: 0902.151.368 (vui lòng không nhắn tin)
Lưu ý: không trả lại hồ sơ sau phỏng vấn',
                    'price' => NULL,
                    'price_min' => NULL,
                    'ads_type' => 'buy',
                    'price_type' => 'treat',
                    'state' => NULL,
                    'status' => 'active',
                    'address' => 'Thành phố Vinh, Nghệ An, Việt Nam',
                    'lat' => 18.679584999999999,
                    'lng' => 105.681333,
                    'view_count' => 1,
                    'images' => NULL,
                    'created_at' => '2017-10-11 10:30:47',
                    'updated_at' => '2017-10-11 10:34:53',
                    'deleted_at' => NULL,
                    'cate_id' => 114,
                    'user_id' => 20,
                ),
                37 => 
                array (
                    'id' => 38,
                'title' => 'NHÂN VIÊN KINH DOANH (tư vấn bán hàng)',
                    'slug' => 'nhan-vien-kinh-doanh-tu-van-ban-hang',
                    'des' => 'Số lượng: 06

Công việc: Tìm kiếm, tư vấn và chào bán sản phẩm xe ô tô của hãng. Chăm sóc khách hàng trước và sau bán hàng.

Yêu cầu:
+ Giới tính: Nam, Nữ
+ Độ tuổi: 22-40
+ Ngoại hình: ưa nhìn
+ Kỹ năng: Năng động, có mối quan hệ rộng, điềm tĩnh, giao tiếp tốt, cẩn thận chu đáo, kiên trì.
Quyền lợi:
+ Được đào tạo nâng cao về các kỹ năng nghề nghiệp và cơ hội thăng tiến cao.
+ Được tham gia BHXH, BHYT
+ Được nghỉ Lễ, Tết theo chế độ của luật lao động

CHẾ ĐỘ LÀM VIỆC, CHẾ ĐỘ ƯU ĐÃI :
- Mức lương: Thỏa thuận.
- Công ty hỗ trợ cơm trưa
- Thưởng Lễ Tết và các chế độ khác của Công ty.

HỒ SƠ XIN VIỆC GỒM:
- Lý lịch công việc
- Các văn bằng chứng chỉ (Sao y công chứng).
- Các loại giấy tờ khác có thể bổ sung sau phỏng vấn.
(Riêng ứng viên ứng tuyển vị trí nhân viên kinh doanh cần nộp đơn ứng tuyển theo mẫu sau http://bit.ly/donungtuyen-chevroletvinh ) 

Hạn nộp hồ sơ: Đến hết ngày 06/10/2017 (ưu tiên ứng viên nạp hồ sớm)
Thời gian phỏng vấn: 07/10/2017
Hồ sơ gửi về Phòng Nhân sự
Mr. Nguyễn Phi Nam
SHOWROOM CHEVROLET VINH - Công ty TNHH Hợp tác Kinh tế VIỆT MỸ
Địa chỉ: Km01 Ngã ba quán Bàu – đầu đường 72m (rẽ phải đường 72m, bên cạnh chung cư dầu khí), Thành Phố Vinh – Tỉnh Nghệ An.
Hoặc gửi về Email: nguyenphinam@chevroletvinh.com.vn
Điện thoại: 0902.151.368 (vui lòng không nhắn tin)
Lưu ý: không trả lại hồ sơ sau phỏng vấn',
                    'price' => NULL,
                    'price_min' => NULL,
                    'ads_type' => 'buy',
                    'price_type' => 'treat',
                    'state' => NULL,
                    'status' => 'active',
                    'address' => 'Thành phố Vinh, Nghệ An, Việt Nam',
                    'lat' => 18.679584999999999,
                    'lng' => 105.681333,
                    'view_count' => 1,
                    'images' => NULL,
                    'created_at' => '2017-10-11 10:31:19',
                    'updated_at' => '2017-10-11 10:34:56',
                    'deleted_at' => NULL,
                    'cate_id' => 108,
                    'user_id' => 20,
                ),
                38 => 
                array (
                    'id' => 39,
                    'title' => 'Kỹ thuật viên điện tự động hóa',
                    'slug' => 'ky-thuat-vien-dien-tu-dong-hoa',
                    'des' => '1. Kỹ thuật viên điện tự động hóa 

· Nội dung công việc: Kỹ thuật viên điện tự động hóa

· Số lượng: 2 người (Không phân biệt nam nữ)

· Yêu cầu: Các ửng cử viên đã tốt nghiệp các trường kỹ thuật, chuyên ngành điện tự động hóa hoặc các ngành tương đương, (Có thể: thiết kế bảng mạch,viết ngôn ngữ chương trình,lắp ráp thiết bị và bố trí hệ thống điện tự động hóa) 

· Điều kiện làm việc 

· Thu nhập:Thỏa thuận khi phỏng vấn

(Chưa bao gồm: Trợ cấp tiền ăn, Tiền làm thêm-tăng ca, trợ cấp xăng xe đi làm, phụ cấp trách nhiệm, phụ cấp tiếng Nhật, chi phí bảo hiểm xã hội).

· ＋Lương khởi điểm : Thời gian thử việc: 3,000,000 VNĐ/Tháng (đây là mức thu nhập áp dụng cho nhân viên mới ra trường, nhân viên có kinh nghiệm và năng lực sẽ thỏa thuận mức thu nhập khi phỏng vấn)

(Chưa bao gồm phụ cấp, bảo hiểm). Khi ký hợp đồng chính thức sẽ quyết định mức lương dựa trên năng lực

＋Phụ cấp đi làm chuyên cần 400,000 VNĐ (Nếu đi đủ công trong 01 tháng).

＋ Khuyến khích làm việc: được đánh giá hàng tháng

＋ Phụ cấp trách nhiệm (khi có chức vụ)

＋Trợ cấp cơm ca (Hiện tại cá nhận chịu 5.000 VNĐ; Công ty Chịu 10.000VNĐ/suất ăn).

＋ Phụ cấp tiếng Nhật (N1=2.000.000VND/tháng, N2=1.200.000 VND/tháng, N3=800.000 VND/tháng, N4=400.000 VND/tháng, N5=200.000 VND/tháng). 


＋ Tiền làm thêm, tăng ca: Tính theo luật lao động Việt Nam

＋ Trợ cấp đi làm (Được thanh toán tiền xăng xe thực tế sử dụng từ chỗ ở đến chỗ làm việc theo tiêu chuẩn xe Wave của Honda).

· Được đóng bảo hiểm xã hội, thất nghiệp, ytế và các chế độ khác(Theo quy định hiện hành tại từng thời điểm).

· Nơi làm việc: Xã Châu Cường, huyện Quỳ Hợp, tỉnh Nghệ An

Làm việc theo giờ hành chính 8 tiếng/ngày

Mùa đông: 07.00 – 17.00 (Nghỉ trưa: 11.30 – 13.30).

Mùa hè: 07.00 – 17.30 (Nghỉ trưa: 11.30 – 14.00).

Thông thường làm theo giờ giấc trên, tuy nhiên tùy tình hình công việc có thể làm theo ca:

Ca 1: 07:00～15:00

Ca 2: 14.00～22.00

· Công việc làm: thiết kế bảng mạch,viết ngôn ngữ chương trình,lắp ráp thiết bị điện tự động hóa. Và các việc khác công ty giao. 

· Một tháng được nghỉ 06 ngày: 02 ngày thứ 7 và 04 ngày Chủ Nhật; và các ngày nghỉ lễ theo quy định hiện hành.

· Thưởng và tăng lương: 01 lần/năm dựa theo năng lực và cống hiến.

· Thời gian thử việc: 02 tháng (Tháng đầu: 85% lương cơ bản, tháng thứ 2: 100% lương cơ bản).

2. Hồ sơ dự tuyển: 

· Bản sơ yếu lý lịch (bản chính có xác nhận UBND xã).

· Bằng tốt ngiệp, bảng điểm.(Bản sao có công chứng).

· Giấy khai sinh (Bản sao).

· Đơn xin việc (bản chính có xác nhận).

· Giấy khám sức khỏe.

· Ảnh 2 tấm (4cm x 6cm).

· Chứng minh nhân dân (Bản sao công chứng)

· Và số điện thoại có thể liên lạc được. 

3. Kế hoạch thi và phỏng vấn 

· Thời hạn nộp hồ sơ đến hết ngày: 15 tháng 10 năm 2017.

· Thời gian thi tuyển: Sẽ thông báo sau

· Thời gian phỏng vấn: Sẽ thông báo sau

· Địa điểm: Sẽ thông báo sau

· Hồ sơ nộp trực tiếp về: 

Công ty Khai thác đá vôi Yabashi Việt Nam

Đ/C: Xã Châu Cường, huyện Quỳ Hợp, tỉnh Nghệ An

Hoặc:Trung tâm VinhCAD (Công ty tại 146 Nguyễn Sỹ Sách, TP Vinh; 

Đ/C: 146 Nguyễn Sỹ Sách, TP Vinh, tỉnh Nghệ An 

· Chi tiết liên hệ: 

Mr Trần Hữu Lâm, Phó phòng Hành chính – Kế toán – Quản trị

Ms Nguyễn Thị Hà, Nhân viên Hành chính

ĐT: 0383.981180/0383.981181

DĐ: Mr Lâm: 0913060648 or 01648939399; Ms Hà: 0983 888 519',
                        'price' => '0',
                        'price_min' => NULL,
                        'ads_type' => 'buy',
                        'price_type' => NULL,
                        'state' => NULL,
                        'status' => 'active',
                        'address' => '146 Nguyễn Sỹ Sách, Tp. Vinh, Nghệ An, Việt Nam',
                        'lat' => 18.686634000000002,
                        'lng' => 105.690174,
                        'view_count' => 1,
                        'images' => NULL,
                        'created_at' => '2017-10-11 10:34:26',
                        'updated_at' => '2017-10-11 10:34:50',
                        'deleted_at' => NULL,
                        'cate_id' => 130,
                        'user_id' => 20,
                    ),
                    39 => 
                    array (
                        'id' => 40,
                        'title' => 'Nam kỹ thuật Bếp',
                        'slug' => 'nam-ky-thuat-bep',
                        'des' => '02 Nam kỹ thuật Bếp
Yêu cầu:
- Độ tuổi từ 20 đến 30 tuổi,
- Chiều cao từ 1m55 trở lên
(Ưu tiên người có kỹ thuật chế biến món ăn, có kinh nghiệm, chịu được áp lực cao trong công việc)
Quyền lợi:
- Được tham gia đóng BHXH, BHYT, BHTN. được hưởng các chế độ theo quy định của nhà nước
- Được trang bị quần áo đồng phục, bảo hộ lao động, bao ăn trưa và ăn tối
- Được hưởng lương tháng thứ 13, tiền thưởng tết, thăm hỏi, hiếu hỉ
- Được làm việc trong môi trường an toàn, sạch sẽ

Mức lương cao, hấp dẫn ( khởi điểm từ 4 đến 6 triệu trở lên, mức lương kỹ thuật Bếp theo thỏa thuận - được nghỉ 2 đến 4 ngày trong tháng được hưởng nguyên lương). Được nâng lương theo hiệu quả công việc từ tháng thứ 2 trở đi',
                        'price' => '0',
                        'price_min' => NULL,
                        'ads_type' => 'buy',
                        'price_type' => NULL,
                        'state' => NULL,
                        'status' => 'active',
                        'address' => 'Khách sạn Thành Vinh, số 213 Đường Lê Lợi, TP Vinh, Nghệ An',
                        'lat' => 18.684215000000002,
                        'lng' => 105.67452400000001,
                        'view_count' => 1,
                        'images' => NULL,
                        'created_at' => '2017-10-11 10:49:56',
                        'updated_at' => '2017-10-11 13:08:58',
                        'deleted_at' => NULL,
                        'cate_id' => 114,
                        'user_id' => 20,
                    ),
                    40 => 
                    array (
                        'id' => 41,
                        'title' => 'Nữ nhân viên bàn',
                        'slug' => 'nu-nhan-vien-ban',
                        'des' => '03 Nữ nhân viên bàn
Yêu cầu:
- Độ tuổi từ 18 đến 26 tuổi,
- Chiều cao từ 1m55 trở lên
- Ngoại hình ưa nhìn (Ưu tiên người có nghiệp vụ du lịch, có ngoại hình đẹp, có khả năng giao tiếp tốt, có kinh nghiệm, có năng khiếu ca hát)

Quyền lợi:
- Được tham gia đóng BHXH, BHYT, BHTN. được hưởng các chế độ theo quy định của nhà nước
- Được trang bị quần áo đồng phục, bảo hộ lao động, bao ăn trưa và ăn tối
- Được hưởng lương tháng thứ 13, tiền thưởng tết, thăm hỏi, hiếu hỉ
- Được làm việc trong môi trường an toàn, sạch sẽ

Mức lương cao, hấp dẫn ( khởi điểm từ 4 đến 6 triệu trở lên, mức lương kỹ thuật Bếp theo thỏa thuận - được nghỉ 2 đến 4 ngày trong tháng được hưởng nguyên lương). Được nâng lương theo hiệu quả công việc từ tháng thứ 2 trở đi

Nộp hồ sơ tại phòng Lễ Tân - Khách sạn Thành Vinh, số 213 Đường Lê Lợi, TP Vinh, Nghệ An',
                        'price' => '0',
                        'price_min' => NULL,
                        'ads_type' => 'buy',
                        'price_type' => NULL,
                        'state' => NULL,
                        'status' => 'active',
                        'address' => 'Khách sạn Thành Vinh, số 213 Đường Lê Lợi, TP Vinh, Nghệ An',
                        'lat' => 18.684215000000002,
                        'lng' => 105.67452400000001,
                        'view_count' => 1,
                        'images' => NULL,
                        'created_at' => '2017-10-11 10:51:58',
                        'updated_at' => '2017-10-11 13:08:55',
                        'deleted_at' => NULL,
                        'cate_id' => 113,
                        'user_id' => 20,
                    ),
                    41 => 
                    array (
                        'id' => 42,
                        'title' => 'Lễ tân, coi đồ theo ca',
                        'slug' => 'le-tan-coi-do-theo-ca',
                        'des' => 'Yêu cầu sức khoẻ tốt, tin cậy
Địa điểm làm việc

§ Trung tâm thương mại Lotus Center, Thị trấn Nam Đàn


Quyền lợi

§ Thu nhập: Lương thưởng tương xứng
Được hưởng đầy đủ BHXH, BHYT và BHTN theo luật lao động Việt Nam hiện hành.

§ Các chế độ đãi ngộ khác


Liên hệ gửi hồ sơ

§ Gửi hồ sơ bản mềm về địa chỉ: tuyendung@lotuscenter.vnhoặc nộp trực tiếp tại văn phòng giao dịch Bất động sản tại Lotus Center, Vân Diên, Nam Đàn

§ Lưu ý:Cần ghi rõ ứng tuyển vào vị trí nào.

§ Chúng tôi sẽ lọc hồ sơ và mời các ứng viên phù hợp đến phỏng vấn',
                        'price' => NULL,
                        'price_min' => NULL,
                        'ads_type' => 'buy',
                        'price_type' => 'treat',
                        'state' => NULL,
                        'status' => 'active',
                        'address' => 'Nam Đàn, Nghệ An, Việt Nam',
                        'lat' => 18.698345,
                        'lng' => 105.523991,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\nimage00974-1507695909.jpg","\\r\\n\\r\\nimage01162-1507695910.jpg"]',
                        'created_at' => '2017-10-11 11:26:29',
                        'updated_at' => '2017-10-11 13:08:43',
                        'deleted_at' => NULL,
                        'cate_id' => 111,
                        'user_id' => 20,
                    ),
                    42 => 
                    array (
                        'id' => 43,
                        'title' => 'Nhân viên thu ngân theo ca',
                        'slug' => 'nhan-vien-thu-ngan-theo-ca',
                        'des' => 'Yêu cầu biết sử dụng Vi tính, 12/12, tin cậy
Địa điểm làm việc

§ Trung tâm thương mại Lotus Center, Thị trấn Nam Đàn


Quyền lợi

§ Thu nhập: Lương thưởng tương xứng
Được hưởng đầy đủ BHXH, BHYT và BHTN theo luật lao động Việt Nam hiện hành.

§ Các chế độ đãi ngộ khác


Liên hệ gửi hồ sơ

§ Gửi hồ sơ bản mềm về địa chỉ: tuyendung@lotuscenter.vnhoặc nộp trực tiếp tại văn phòng giao dịch Bất động sản tại Lotus Center, Vân Diên, Nam Đàn

§ Lưu ý:Cần ghi rõ ứng tuyển vào vị trí nào.

§ Chúng tôi sẽ lọc hồ sơ và mời các ứng viên phù hợp đến phỏng vấn',
                        'price' => NULL,
                        'price_min' => NULL,
                        'ads_type' => 'buy',
                        'price_type' => 'treat',
                        'state' => NULL,
                        'status' => 'active',
                        'address' => 'Nam Đàn, Nghệ An, Việt Nam',
                        'lat' => 18.698345,
                        'lng' => 105.523991,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\nimage01162-1507695909.jpg"]',
                        'created_at' => '2017-10-11 11:26:37',
                        'updated_at' => '2017-10-11 13:08:51',
                        'deleted_at' => NULL,
                        'cate_id' => 112,
                        'user_id' => 20,
                    ),
                    43 => 
                    array (
                        'id' => 44,
                        'title' => 'Tuyển dụng bảo vệ',
                        'slug' => 'tuyen-dung-bao-ve',
                        'des' => 'Yêu cầu sức khoẻ tốt, tin cậy
Địa điểm làm việc

§ Trung tâm thương mại Lotus Center, Thị trấn Nam Đàn


Quyền lợi

§ Thu nhập: Lương thưởng tương xứng
Được hưởng đầy đủ BHXH, BHYT và BHTN theo luật lao động Việt Nam hiện hành.

§ Các chế độ đãi ngộ khác


Liên hệ gửi hồ sơ

§ Gửi hồ sơ bản mềm về địa chỉ: tuyendung@lotuscenter.vnhoặc nộp trực tiếp tại văn phòng giao dịch Bất động sản tại Lotus Center, Vân Diên, Nam Đàn

§ Lưu ý:Cần ghi rõ ứng tuyển vào vị trí nào.

§ Chúng tôi sẽ lọc hồ sơ và mời các ứng viên phù hợp đến phỏng vấn',
                        'price' => NULL,
                        'price_min' => NULL,
                        'ads_type' => 'buy',
                        'price_type' => 'treat',
                        'state' => NULL,
                        'status' => 'active',
                        'address' => 'Nam Đàn, Nghệ An, Việt Nam',
                        'lat' => 18.698345,
                        'lng' => 105.523991,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\nimage00974-1507695891.jpg","\\r\\n\\r\\nimage01162-1507695892.jpg"]',
                        'created_at' => '2017-10-11 11:26:43',
                        'updated_at' => '2017-10-11 13:08:39',
                        'deleted_at' => NULL,
                        'cate_id' => 118,
                        'user_id' => 20,
                    ),
                    44 => 
                    array (
                        'id' => 45,
                    'title' => 'Tuyển dụng Nhân viên IT (Nam )',
                        'slug' => 'tuyen-dung-nhan-vien-it-nam',
                        'des' => 'Yêu cầu kinh nghiệm về phần cứng, LAN, Internet
Địa điểm làm việc

§ Trung tâm thương mại Lotus Center, Thị trấn Nam Đàn


Quyền lợi

§ Thu nhập: Lương thưởng tương xứng
Được hưởng đầy đủ BHXH, BHYT và BHTN theo luật lao động Việt Nam hiện hành.

§ Các chế độ đãi ngộ khác


Liên hệ gửi hồ sơ

§ Gửi hồ sơ bản mềm về địa chỉ: tuyendung@lotuscenter.vnhoặc nộp trực tiếp tại văn phòng giao dịch Bất động sản tại Lotus Center, Vân Diên, Nam Đàn

§ Lưu ý:Cần ghi rõ ứng tuyển vào vị trí nào.

§ Chúng tôi sẽ lọc hồ sơ và mời các ứng viên phù hợp đến phỏng vấn',
                        'price' => NULL,
                        'price_min' => NULL,
                        'ads_type' => 'buy',
                        'price_type' => 'treat',
                        'state' => NULL,
                        'status' => 'active',
                        'address' => 'Nam Đàn, Nghệ An, Việt Nam',
                        'lat' => 18.698345,
                        'lng' => 105.523991,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\nimage01162-1507695909.jpg"]',
                        'created_at' => '2017-10-11 11:26:48',
                        'updated_at' => '2017-10-11 13:08:35',
                        'deleted_at' => NULL,
                        'cate_id' => 139,
                        'user_id' => 20,
                    ),
                    45 => 
                    array (
                        'id' => 46,
                        'title' => 'Tuyển dụng trưởng ngành hàng',
                        'slug' => 'tuyen-dung-truong-nganh-hang',
                        'des' => 'Mô tả công việc

1. Nắm rõ danh mục hàng hóa của ngành hàng phụ trách , hàng ngày giám sát tình trạng hàng tồn , hàng đặt để đảm bảo giảm thiểu tình trạng tồn cao cũng như đạt tỉ lệ độ phủ hàng như yêu cầu.
2. Ít nhất 1 lần/tuần phải đi ra thị trường và đối thủ để nghiên cứu, phân tích các vấn đề liên quan đến hàng hóa và giá cả ( loại hàng hóa, cơ cấu hàng hóa, hàng hóa mới, giá cả, chương trình khuyến mại, hoạt động hoạt náo, cách thức trưng bày, trang thiết bị, công cụ,…). Thường xuyên lấy ý kiến của khách hàng về hàng hóa , giá cả, dịch vụ và nhân viên của Ngành hàng quản lí. Lập báo cáo ngắn gọn 2 tuần/lần về các nhận xét, đánh giá và đề xuất nếu có.
3. Kiểm tra tối thiểu mỗi ca /lần tình trạng trưng bày hàng hóa, tình trạng vệ sinh trong siêu thị đối với Ngành hàng phụ trách để đảm bảo hình ảnh theo tiêu chuẩn và phát hiện các vấn đề không hợp lí từ góc nhìn của khách hàng. Thực hiện theo danh mục kiểm tra từng ngành hàng.


Yêu cầu công việc

§ Tốt nghiệp từ Trung cấp trở lên, Ưu tiên Chuyên ngành thương mại, QTKD, Marketing,

§ Ít nhất 1 năm kinh nghiệm làm việc trong lĩnh vực kinh doanh bán lẻ, hàng tiêu dùng

§ Ưu tiên kinh nghiệm Quản lý ngành hàng tại các siêu.

§ Giao tiếp tốt, biết lắng nghe, có khả năng dẫn dắt vấn đề

§ Sắp xếp, tổ chức công việc một cách khoa học, chặt chẽ

§ Khả năng dẫn dắt đội nhóm, phân tích, xử lý tình huống tốt

§ Dám dấn thân, đảm đương việc khó, thích nghi nhanh với sự thay đổi.

§ Khẩn trương, quyết liệt trong công việc, trách nhiệm, chủ động, không né tránh ỷ lại.

§ Thành thạo vi tính văn phòng
Địa điểm làm việc

§ Trung tâm thương mại Lotus Center, Thị trấn Nam Đàn


Quyền lợi

§ Thu nhập: Lương thưởng tương xứng
Được hưởng đầy đủ BHXH, BHYT và BHTN theo luật lao động Việt Nam hiện hành.

§ Các chế độ đãi ngộ khác


Liên hệ gửi hồ sơ

§ Gửi hồ sơ bản mềm về địa chỉ: tuyendung@lotuscenter.vnhoặc nộp trực tiếp tại văn phòng giao dịch Bất động sản tại Lotus Center, Vân Diên, Nam Đàn

§ Lưu ý:Cần ghi rõ ứng tuyển vào vị trí nào.

§ Chúng tôi sẽ lọc hồ sơ và mời các ứng viên phù hợp đến phỏng vấn',
                        'price' => '0',
                        'price_min' => NULL,
                        'ads_type' => 'buy',
                        'price_type' => NULL,
                        'state' => NULL,
                        'status' => 'active',
                        'address' => 'Nam Đàn, Nghệ An, Việt Nam',
                        'lat' => 18.698345,
                        'lng' => 105.523991,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\nimage01162-1507695909.jpg"]',
                        'created_at' => '2017-10-11 11:26:53',
                        'updated_at' => '2017-10-11 13:08:31',
                        'deleted_at' => NULL,
                        'cate_id' => 145,
                        'user_id' => 20,
                    ),
                    46 => 
                    array (
                        'id' => 47,
                        'title' => 'Tuyển dụng quản lý siêu thị',
                        'slug' => 'tuyen-dung-quan-ly-sieu-thi',
                        'des' => 'Mô tả công việc

- Quản lý doanh số, chi phí, lợi nhuận, đảm bảo vận hành hiệu quả đáp ứng chỉ tiêu KPI theo kế hoạch.

- Tổ chức, quản lý, chịu trách nhiệm về kết quả kinh doanh và mọi hoạt động của siêu thị.

- Hoạch định và thực hiện các kế hoạch phát triển kinh doanh tại Siêu Thị nhằm thúc đầy mạnh mẽ hoạt động bán hàng, doanh thu, mở rộng thị trường.

- Xây dựng và phân bổ các chỉ tiêu công việc cho từng phòng ban, cá nhân tại siêu thị để đạt mục tiêu được giao.

- Phối hợp các bộ phận chức năng thực hiện các chính sách giá, chương trình khuyến mãi, trưng bày sản phẩm,… 

- Quản lý siêu thị, hàng tồn kho, xây dựng phương án phòng chống thất thoát hàng hóa 

- Hoạch định nguồn nhân lực 


Yêu cầu công việc

- Trình độ: Tốt nghiệp Trung Cấp/ Cao Đẳng/ Đại Học trở lên.

- Giới tính: Nữ, tuổi từ 35 tuổi trở lên

- Kinh nghiệm chuyên môn: Ít nhất 3 năm kinh nghiệm trong lĩnh vực kinh doanh bán sỉ/lẻ, hàng tiêu dùng, điện máy hoặc hàng gia dụng

- Ưu tiên Ứng viên đã làm việc tại các siêu thị.

- Sức khỏe tốt, chịu được áp lực công việc cao.

- Sơ yếu lý lịch rõ ràng.

- Giao tiếp tốt, hoạt bát, hòa đồng, trung thực.

- Khả năng làm việc độc lập và theo nhóm.

- Làm việc tỉ mỉ, cẩn thận
Địa điểm làm việc

§ Trung tâm thương mại Lotus Center, Thị trấn Nam Đàn


Quyền lợi

§ Thu nhập: Lương thưởng tương xứng
Được hưởng đầy đủ BHXH, BHYT và BHTN theo luật lao động Việt Nam hiện hành.

§ Các chế độ đãi ngộ khác


Liên hệ gửi hồ sơ

§ Gửi hồ sơ bản mềm về địa chỉ: tuyendung@lotuscenter.vnhoặc nộp trực tiếp tại văn phòng giao dịch Bất động sản tại Lotus Center, Vân Diên, Nam Đàn

§ Lưu ý:Cần ghi rõ ứng tuyển vào vị trí nào.

§ Chúng tôi sẽ lọc hồ sơ và mời các ứng viên phù hợp đến phỏng vấn',
                        'price' => NULL,
                        'price_min' => NULL,
                        'ads_type' => 'buy',
                        'price_type' => 'treat',
                        'state' => NULL,
                        'status' => 'active',
                        'address' => 'Nam Đàn, Nghệ An, Việt Nam',
                        'lat' => 18.698345,
                        'lng' => 105.523991,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\nimage00974-1507695889.jpg","\\r\\n\\r\\nimage01162-1507695909.jpg"]',
                        'created_at' => '2017-10-11 11:27:00',
                        'updated_at' => '2017-10-11 13:08:27',
                        'deleted_at' => NULL,
                        'cate_id' => 108,
                        'user_id' => 20,
                    ),
                    47 => 
                    array (
                        'id' => 48,
                        'title' => 'Tuyển dụng nhân viên bán hàng',
                        'slug' => 'tuyen-dung-nhan-vien-ban-hang',
                        'des' => 'Thời trang Khánh Thơm tuyển gấp 2 nhân viên bán hàng.
Yêu cầu: Có kinh nghiệm bán hàng
Ngoại hình ưu nhìn
Mức lương: theo thỏa thuận
Liên hệ: Shop thời trang Khánh Thơm
Địa chỉ: Số 186, Đường Đặng Thái Thân, phường Quang Trung
Điện thoại: 0942 366 889 - 0966 626 809',
                        'price' => NULL,
                        'price_min' => NULL,
                        'ads_type' => 'buy',
                        'price_type' => 'treat',
                        'state' => NULL,
                        'status' => 'active',
                        'address' => 'Số 186, Đường Đặng Thái Thân, phường Quang Trung',
                        'lat' => 18.666839,
                        'lng' => 105.672685,
                        'view_count' => 1,
                        'images' => NULL,
                        'created_at' => '2017-10-11 11:29:57',
                        'updated_at' => '2017-10-11 13:08:47',
                        'deleted_at' => NULL,
                        'cate_id' => 107,
                        'user_id' => 20,
                    ),
                    48 => 
                    array (
                        'id' => 49,
                        'title' => 'Tuyển dụng nhân viên phục vụ',
                        'slug' => 'tuyen-dung-nhan-vien-phuc-vu',
                        'des' => 'Ưu tiên ứng viên ngoại hình khá, nữ chiều cao trên 1m58, nam cao trên 1m60, hoạt bát, chăm chỉ, giao tiếp tốt. Ưu tiên Nhân viên có kinh nghiệm làm Nhà hàng.
• Chăm chỉ, thật thà, nhanh nhẹn, chịu khó.
• Tuổi từ 18 trở lên
+ Chế độ:
• Lương thỏa thuận + thưởng + hoa hồng doanh thu - có thể thương lượng thêm tùy năng lực
• Bao ăn trưa, tối

+ Địa điểm làm việc:
Công ty TNHH Thương mại và Dịch vụ Phú Long Hưng - NHÀ HÀNG ĐẠI NGÀN - Số 36, Đ. Nguyễn Sỹ Sách, TP.Vinh ( đối diện Chợ Kênh Bắc)

+ Liên hệ trực tiếp:
Ms. Vinh - Số điện thoại: 094 678 1836 (Vui lòng gọi điện)

+ Thời hạn: trước 20/10/2017
Địa điểm : Nhà hàng Đại Ngàn - Số 36 - Đường Nguyễn Sỹ Sách - TP Vinh - Nghệ An',
                        'price' => '0',
                        'price_min' => NULL,
                        'ads_type' => 'buy',
                        'price_type' => NULL,
                        'state' => NULL,
                        'status' => 'active',
                        'address' => 'Số 36 - Đường Nguyễn Sỹ Sách - TP Vinh - Nghệ An',
                        'lat' => 18.686456,
                        'lng' => 105.680131,
                        'view_count' => 1,
                        'images' => NULL,
                        'created_at' => '2017-10-11 11:35:23',
                        'updated_at' => '2017-10-11 13:08:18',
                        'deleted_at' => NULL,
                        'cate_id' => 107,
                        'user_id' => 20,
                    ),
                    49 => 
                    array (
                        'id' => 50,
                        'title' => 'Tuyển dụng Thu ngân',
                        'slug' => 'tuyen-dung-thu-ngan',
                        'des' => 'Ưu tiên ứng viên ngoại hình khá, nữ chiều cao trên 1m58, hoạt bát, chăm chỉ, giao tiếp tốt. Ưu tiên Nhân viên có kinh nghiệm làm Thu ngân Nhà hàng.
• Chăm chỉ, thật thà, nhanh nhẹn, chịu khó.
• Tuổi từ 18 trở lên
+ Chế độ:
• Lương thỏa thuận + thưởng + hoa hồng doanh thu - có thể thương lượng thêm tùy năng lực
• Bao ăn trưa, tối

+ Địa điểm làm việc:
Công ty TNHH Thương mại và Dịch vụ Phú Long Hưng - NHÀ HÀNG ĐẠI NGÀN - Số 36, Đ. Nguyễn Sỹ Sách, TP.Vinh ( đối diện Chợ Kênh Bắc)

+ Liên hệ trực tiếp:
Ms. Vinh - Số điện thoại: 094 678 1836 (Vui lòng gọi điện)

+ Thời hạn: trước 20/10/2017
Địa điểm : Nhà hàng Đại Ngàn - Số 36 - Đường Nguyễn Sỹ Sách - TP Vinh - Nghệ An',
                        'price' => '0',
                        'price_min' => NULL,
                        'ads_type' => 'buy',
                        'price_type' => NULL,
                        'state' => NULL,
                        'status' => 'active',
                        'address' => 'Số 36 - Đường Nguyễn Sỹ Sách - TP Vinh - Nghệ An',
                        'lat' => 18.686456,
                        'lng' => 105.680131,
                        'view_count' => 1,
                        'images' => NULL,
                        'created_at' => '2017-10-11 11:35:25',
                        'updated_at' => '2017-10-11 13:08:14',
                        'deleted_at' => NULL,
                        'cate_id' => 112,
                        'user_id' => 20,
                    ),
                    50 => 
                    array (
                        'id' => 51,
                        'title' => 'Tuyển dụng Đầu bếp',
                        'slug' => 'tuyen-dung-dau-bep',
                        'des' => 'Ưu tiên ứng viên có kinh nghiệm đứng bếp Nhà hàng từ 5 năm trở lên, khỏe mạnh, nhanh nhẹn, chịu khó, có khả năng sắp xếp tổ chức công việc
• Chăm chỉ, thật thà, nhanh nhẹn, chịu khó.
• Tuổi từ 18 trở lên
+ Chế độ:
• Lương thỏa thuận + thưởng + hoa hồng doanh thu - có thể thương lượng thêm tùy năng lực
• Bao ăn trưa, tối

+ Địa điểm làm việc:
Công ty TNHH Thương mại và Dịch vụ Phú Long Hưng - NHÀ HÀNG ĐẠI NGÀN - Số 36, Đ. Nguyễn Sỹ Sách, TP.Vinh ( đối diện Chợ Kênh Bắc)

+ Liên hệ trực tiếp:
Ms. Vinh - Số điện thoại: 094 678 1836 (Vui lòng gọi điện)

+ Thời hạn: trước 20/10/2017
Địa điểm : Nhà hàng Đại Ngàn - Số 36 - Đường Nguyễn Sỹ Sách - TP Vinh - Nghệ An',
                        'price' => '0',
                        'price_min' => NULL,
                        'ads_type' => 'buy',
                        'price_type' => NULL,
                        'state' => NULL,
                        'status' => 'active',
                        'address' => 'Số 36 - Đường Nguyễn Sỹ Sách - TP Vinh - Nghệ An',
                        'lat' => 18.686456,
                        'lng' => 105.680131,
                        'view_count' => 1,
                        'images' => NULL,
                        'created_at' => '2017-10-11 11:35:28',
                        'updated_at' => '2017-10-11 13:08:10',
                        'deleted_at' => NULL,
                        'cate_id' => 114,
                        'user_id' => 20,
                    ),
                    51 => 
                    array (
                        'id' => 52,
                        'title' => 'Tuyển nhân viên lập trình viên PHP',
                        'slug' => 'tuyen-nhan-vien-lap-trinh-vien-php',
                        'des' => 'Lập Trình Viên:
Số lượng: 02 người
Mô tả công việc
- Phát triển các hệ thống, sản phẩm của công ty
- Công việc chi tiết sẽ trao đổi thêm khi phỏng vấn
Yêu cầu công việc
- Có tối thiểu 1 năm kinh nghiệm trên PHP,
- Sử dụng tốt các ngôn ngữ kỹ thuật: HTML, CSS3, Javascript, PHP, MySQL, XML,
-- Thành thạo html, javascript & jquery, css.
-Có khả năng cut html từ file thiết kế.
- Có khả năng làm việc độc lập hoặc theo nhóm.
Ưu tiên: Các ứng cứ viên có kinh nghiệm làm việc trên các framework PHP.
Mức lương: 7 000 000 - 9 000 000
Thời gian, địa điểm nhận hồ sơ: 
- Trụ sở VP Cty: Tầng 3, KS Mường Thanh Phương Đông, Tp Vinh, Nghệ An
- Thời gian nhận hồ sơ: từ 8h đến 17h30 hàng ngày (trừ chiều thứ 7 và ngày chủ nhật)
- Hạn nộp hồ sơ: đến hết ngày 30/09/2017.
- Điện thoại: 0918 673 086

Lưu ý: Ứng viên chỉ cần nộp hồ sơ photo, hoặc gửi CV ứng tuyển vào địa chỉ mail tuannv@goldencity.com . Các ứng viên đã thi tuyển vào vị trí Pháp chế trước đây không ứng tuyển vào đợt tuyển dụng này.',
                        'price' => '0',
                        'price_min' => NULL,
                        'ads_type' => 'buy',
                        'price_type' => NULL,
                        'state' => NULL,
                        'status' => 'active',
                        'address' => 'Khách sạn Mường Thanh Phương Đông, Tp Vinh, Nghệ An',
                        'lat' => 18.665330000000001,
                        'lng' => 105.690124,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\nkhu-do-thi-golden-city-da-nang-1507696667.jpg"]',
                        'created_at' => '2017-10-11 11:38:35',
                        'updated_at' => '2017-10-11 13:08:23',
                        'deleted_at' => NULL,
                        'cate_id' => 165,
                        'user_id' => 20,
                    ),
                    52 => 
                    array (
                        'id' => 53,
                        'title' => 'NHÂN VIÊN KIỂM SOÁT NỘI BỘ',
                        'slug' => 'nhan-vien-kiem-soat-noi-bo',
                        'des' => '1. Mô tả công việc:
- Kiểm tra tính hợp lý, hợp lệ của chứng từ
- Soát xét báo cáo tài chính định kỳ của Công ty và các đơn vị thành viên
- Báo cáo về toàn bộ các rủi ro và hệ thống quản trị rủi ro của doanh nghiệp
- Đảm bảo quy trình vận hành của công ty đúng quy định và tránh được rủi ro
- Đọc, nghiên cứu, tập hợp các văn bản pháp lý liên quan đến vấn đề tài chính, kế toán, thuế để đảm bảo các hoạt động của Công ty tuân thủ các văn bản hiện hành.
- Các công việc khác của Phòng và công việc khác được giao.
- Bóc tách khối lượng dự toán
2. Yêu cầu chuyên môn:
- Tốt nghiệp đại học chuyên ngành kế toán, kiểm toán, tài chính như Đại học Kinh Tế Quốc Dân, Học viện Tài Chính, Đại học Thương Mại…
- Có tối thiều 1 năm kinh nghiệm thực tế về kế toán, kiểm toán, kiểm soát nội bộ.
- Am hiểu các chính sách thuế và chế độ, chuẩn mực kế toán hiện hành.
- Tỉ mỉ, cận thận và đặc biệt là rất trung thực
- Tốt nghiệp chuyên ngành kế toán xây dựng, ưu tiên ứng viên nam
3. Quyền lợi được hưởng:
- Lương: thỏa thuận theo năng lực của ứng viên
- Thưởng: các ngày lễ, tết, lương tháng thứ 13 và thưởng cuối năm theo kết quả kinh doanh của Công ty.
- Tham gia BHXH đầy đủ và các chế độ khác theo quy đinh của Luật .
- Chế độ làm việc đảm bảo và cơ hội thăng tiến
- Môi trường làm việc chuyên nghiệp và năng động.
- Phúc lợi khác: Nghỉ mát, ăn trưa tại bếp cơ quan...
4. Địa điểm làm việc:
- Làm việc tại Công ty mẹ - Công ty CP Golden City
Ứng viên muốn biết thông tin cụ thể, vui lòng tham khảo thêm tại trang web của Công ty: http://goldencity.com/

NỘP HỒ SƠ
- Hồ sơ: Ưu tiên nộp hồ sơ trực tiếp, một bộ hồ sơ đầy đủ, CV mô tả đầy đủ, chi tiết quá trình học tập, kinh nghiệm làm việc.
- Địa chỉ nộp hồ sơ: tầng 3, khách sạn Mường Thành Phương Đông, số 2 , đường Trường Thi, Vinh, Nghệ An.
- Thời gian nhận hồ sơ: hết ngày 31/08/2017, khuyến khích các ứng viên nộp hồ sơ sớm, Công ty sẽ tổ chức thi tuyển khi có đủ số lượng ứng viên phù hợp.
- Có thể nộp qua mail cho những ứng viên ở xa không thể nộp hồ sơ trực tiếp. Địa chỉ mail nhận hồ sơ: hoaht@goldencity.com
- Điện thoại liên hệ: 02383 526 333.',
                        'price' => NULL,
                        'price_min' => NULL,
                        'ads_type' => 'buy',
                        'price_type' => 'treat',
                        'state' => NULL,
                        'status' => 'active',
                        'address' => 'khách sạn Mường Thành Phương Đông, số 2 , đường Trường Thi, Vinh, Nghệ An',
                        'lat' => 18.665330000000001,
                        'lng' => 105.690124,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\nkhu-do-thi-golden-city-da-nang-1507696817.jpg"]',
                        'created_at' => '2017-10-11 11:41:06',
                        'updated_at' => '2017-10-11 13:08:06',
                        'deleted_at' => NULL,
                        'cate_id' => 112,
                        'user_id' => 20,
                    ),
                    53 => 
                    array (
                        'id' => 54,
                        'title' => 'Cần gấp 1 nam giao hàng rau củ quả',
                        'slug' => 'can-gap-1-nam-giao-hang-rau-cu-qua',
                        'des' => 'Cần tuyển 1 nam giao hàng rau củ quả
Làm ở Chợ Lau ( gần quảng trường TP.VInh)
Thời gian: Sáng 6h30p đến 10h
Chiều : 15h đến 18h
Lương tháng : 3, 5 triệu
Công việc chính: là đi giao hàng rau cho các cửa hàng đã đặt sẵn
Xăng xe của Dì chủ
Cần người : Thật thà, Chịu khó, Công việc tuy không mệt nhưng phải cần mẫn
Liên hệ: 0965 104 186 - Gặp Tâm đê đi làm
Địa chỉ nhận việc : Số 54 - Nguyễn Khánh toàn ( đi đến 70 Nguyễn Sỹ Sách rẽ vào 100m)
XIN VIỆC HOÀN TOÀN MIỄN PHÍ.',
                        'price' => '0',
                        'price_min' => NULL,
                        'ads_type' => 'buy',
                        'price_type' => NULL,
                        'state' => NULL,
                        'status' => 'active',
                        'address' => 'Số 54 - Nguyễn Khánh toàn thành phố vinh nghệ an',
                        'lat' => 18.685935000000001,
                        'lng' => 105.685373,
                        'view_count' => 1,
                        'images' => NULL,
                        'created_at' => '2017-10-11 11:44:15',
                        'updated_at' => '2017-10-11 13:08:02',
                        'deleted_at' => NULL,
                        'cate_id' => 128,
                        'user_id' => 20,
                    ),
                    54 => 
                    array (
                        'id' => 55,
                        'title' => 'Cần 1 nam giao hàng đá lạnh',
                        'slug' => 'can-1-nam-giao-hang-da-lanh',
                        'des' => 'Cần 1 nam giao đá lạnh nhận ăn ở lại.
Chỉ cần thật thà,chịu khó, có sức khỏe,chủ quán dễ tính.
Nhân viên khá đông,có thể hỗ trợ nhau, vui vẽ, nhiệt tình, có chỗ ăn ở, có người nấu ăn.
Lương tháng: 4tr (nhận ăn ở lại và bao ăn ở cho thêm tiền ăn sáng tính
Gần phường Sân Bóng Hưng Dũng
Liên hệ:0965 104 186 - Gặp Tâm
Địa chỉ nhận việc: Số 54 - đ. Nguyễn Khánh Toàn - Hưng Dũng.
Ưu tiên mấy bạn ở Quê muốn đi làm để có chỗ ăn ở lại.',
                            'price' => '0',
                            'price_min' => NULL,
                            'ads_type' => 'buy',
                            'price_type' => NULL,
                            'state' => NULL,
                            'status' => 'active',
                            'address' => 'Số 54 - Đường Nguyễn Khánh Toàn -  Phường Hưng Dũng - Thành phố vinh - Nghệ An',
                            'lat' => 18.685935000000001,
                            'lng' => 105.685373,
                            'view_count' => 1,
                            'images' => NULL,
                            'created_at' => '2017-10-11 11:46:45',
                            'updated_at' => '2017-10-11 13:07:57',
                            'deleted_at' => NULL,
                            'cate_id' => 128,
                            'user_id' => 20,
                        ),
                        55 => 
                        array (
                            'id' => 56,
                            'title' => 'cần 1 dì giúp việc nhà tại vinh.',
                            'slug' => 'can-1-di-giup-viec-nha-tai-vinh',
                            'des' => 'Nhà có 2 vợ chồng và 2 bé lớn rồi.
Cần nữ làm việc nhà dọn dẹp nhà cửa nấu ăn.
2 bé đi học cấp 2 cả rôi.
Nhà ở Hưng Phúc gần cầu Kinh Bắc
Lương tháng: 3,5 - 4 triệu
(Làm 1 năm tặng thêm 1 chỉ vàng).
Liên hệ sdt: 0965 104 186
Cần gấp lắm ạ.',
                            'price' => '0',
                            'price_min' => NULL,
                            'ads_type' => 'buy',
                            'price_type' => NULL,
                            'state' => NULL,
                            'status' => 'active',
                            'address' => 'Đường hưng phúc thành phố vinh nghệ an việt nam',
                            'lat' => 18.682877999999999,
                            'lng' => 105.69865799999999,
                            'view_count' => 1,
                            'images' => NULL,
                            'created_at' => '2017-10-11 11:49:39',
                            'updated_at' => '2017-10-11 13:07:52',
                            'deleted_at' => NULL,
                            'cate_id' => 128,
                            'user_id' => 20,
                        ),
                        56 => 
                        array (
                            'id' => 57,
                            'title' => 'Cần tìm 1 nữ phụ giúp việc nhà',
                            'slug' => 'can-tim-1-nu-phu-giup-viec-nha',
                            'des' => 'Cần tìm 1 nữ phụ giúp việc nhà nhận ăn ở lại trong nhà.
Nhà 2 vợ chồng và 2 con lớn đi học cả rồi, vợ chồng đi làm suốt, nhà 2 tầng,
muốn tìm người phụ giúp việc nhà.
Chủ dễ tính, xem như người nhà, chưa biết việc có thể hướng dẫn thêm.
- Yêu cầu: thật thà, chịu khó, không yêu cầu độ tuổi, làm được việc là được.
- Lương tháng: 3,5triệu - 4 triệu( làm tốt thưởng thêm, bao ăn ở và các chi phí sinh hoạt).
- Liên Hệ: 0965 104 186 - Gặp Cháu Tâm
Địa chỉ: gần Số 54 - Đường Nguyễn Khánh Toàn - Hưng Phúc - Tp.Vinh..',
                            'price' => '0',
                            'price_min' => NULL,
                            'ads_type' => 'buy',
                            'price_type' => NULL,
                            'state' => NULL,
                            'status' => 'active',
                            'address' => 'Số 54 - Đường Nguyễn Khánh Toàn - Hưng Phúc - Tp.Vinh',
                            'lat' => 18.685935000000001,
                            'lng' => 105.685373,
                            'view_count' => 1,
                            'images' => NULL,
                            'created_at' => '2017-10-11 13:14:33',
                            'updated_at' => '2017-10-11 14:48:53',
                            'deleted_at' => NULL,
                            'cate_id' => 128,
                            'user_id' => 20,
                        ),
                        57 => 
                        array (
                            'id' => 58,
                            'title' => 'Kế Toán Tổng Hợp',
                            'slug' => 'ke-toan-tong-hop',
                            'des' => 'Chào các bạn, Hiện công ty TNHH Vật Tư Y Tế Miền Trung đang có nhu cầu tuyển 1 Kế Toán Tổng Hợp
* Mô tả công việc:
- Kiểm tra, cân đối số liệu kế toán
- Lập báo cáo tài chính, báo cáo kế toán theo yêu cầu của cấp trên.
- Lập hóa đơn, nhập, xuất kho
- Các công việc liên quan khác do lãnh đạo và kế toán trưởng giao.
* Yêu cầu:
- Ngoại hình ưa nhìn, Cẩn thận, chăm chỉ, hiền lành, có khả năng làm việc độc lập
*Mức lương: Theo thỏa thuận
-------------------------------------
Ưu tiên:
- Những bạn đã học qua khóa đào tạo "Tình huống các sai phạm trong khai thuế và kế toán tại doanh nghiệp" của Trung Tâm.
- Những bạn biết tiếng anh',
                            'price' => NULL,
                            'price_min' => NULL,
                            'ads_type' => 'buy',
                            'price_type' => 'treat',
                            'state' => NULL,
                            'status' => 'active',
                            'address' => 'Thành phố Vinh, Nghệ An, Việt Nam',
                            'lat' => 18.679584999999999,
                            'lng' => 105.681333,
                            'view_count' => 1,
                            'images' => NULL,
                            'created_at' => '2017-10-11 13:21:09',
                            'updated_at' => '2017-10-11 14:48:58',
                            'deleted_at' => NULL,
                            'cate_id' => 112,
                            'user_id' => 20,
                        ),
                        58 => 
                        array (
                            'id' => 59,
                            'title' => 'Tuyển lập trình viên',
                            'slug' => 'tuyen-lap-trinh-vien',
                            'des' => 'Chúng tôi đang tìm kiếm ứng viên: 
- Đam mê lập trình
- Chủ động trong công việc
- Muốn thử thách bản thân
- Muốn làm những sản phẩm sáng tạo, đột phá 

Vị trí KHÔNG phù hợp với những ứng viên: 
- Thụ động, giao việc gì làm việc nấy
- Quen làm việc 8 tiếng/ ngày
- Chỉ biết lập trình html/css, dùng jquery plugin, modify template wordpress

Đôi nét về Dev Team tại Vinh:
- Văn phòng nhỏ nhưng ấm cúng
- Môi trường trẻ trung, năng động
- Leader nhiệt tình, dễ tính

Join vào công ty nghĩa là bạn sẽ: 
- Được dìu dắt, chỉ bảo nhiệt tình đến khi làm được việc thì thôi
- Offer lương cực tốt 15.000.000 - 25.000.000 vnd 
- Hưởng lương tháng thứ 13 
- Chia sẻ lợi nhuận/ cổ phần theo đóng góp của bạn 
- Hưởng chế độ BHYT/ BHXH theo quy định của nhà nước
- Du lịch nước ngoài/ trong nước hàng năm 
- ... (Bạn yêu cầu gì nữa thì viết vào)

Quy trình tuyển dụng đơn giản: 
- Bạn gửi CV vào địa chỉ email: jobs@bespokify.com 
- Bạn sẽ được đặt lịch phỏng vấn trực tiếp tại văn phòng
- Sẽ có 1 bài test viết code nho nhỏ, bạn pass thì ngày hôm sau đi làm luôn

Q/A: nhiều bạn email hỏi những vấn đề này quá nên mình liệt kê ra đây luôn cho tiện.

Q: Bên mình có tuyển sinh viên năm cuối/ mới tốt nghiệp không? 
A: Vẫn tuyển vào vị trí internship. Sẽ có bài test code riêng cho những bạn này. Yêu cầu sơ bộ là phải học nhanh và có logic tốt. 

Q: Ngôn ngữ lập trình bên mình chủ yếu sử dụng là gì? 
A: Dự án bên mình làm trải rộng nhiều ngôn ngữ/ công nghệ khác nhau. Tư duy logic, khả năng giải quyết vấn đề là điều quan trọng nhất. 

Thông tin liên hệ: 
Bespokify Development Team @ Vinh 
Số 04, đường Phan Huy Chú, Vinh, Nghệ An
Người liên hệ: Trọng 0915 456 721',
                            'price' => '0',
                            'price_min' => NULL,
                            'ads_type' => 'buy',
                            'price_type' => NULL,
                            'state' => NULL,
                            'status' => 'active',
                            'address' => 'Số 04, đường Phan Huy Chú, Vinh, Nghệ An',
                            'lat' => 18.654841000000001,
                            'lng' => 105.696884,
                            'view_count' => 1,
                            'images' => NULL,
                            'created_at' => '2017-10-11 13:31:57',
                            'updated_at' => '2017-10-11 14:49:11',
                            'deleted_at' => NULL,
                            'cate_id' => 165,
                            'user_id' => 20,
                        ),
                        59 => 
                        array (
                            'id' => 60,
                            'title' => 'Tuyển dụng lập trình viên',
                            'slug' => 'tuyen-dung-lap-trinh-vien',
                            'des' => 'Số lượng: 02 người
Lương: 6 triệu trở lên (tùy vào khả năng của bạn)

Nội dung công việc:
Xây dựng website dựa trên các nền tảng: Symfony, Drupal, Magento, Shopify…(cái nào bạn chưa biết sẻ được hướng dẫn).
Làm theme, template cho các nền tảng nêu trên.
Và thực hiện các công việc do quản lý yêu cầu.
Yêu cầu:
Thành thạo HTML, CSS, Javascript.
Có khả năng lập trình trên ngôn ngữ PHP.
Nếu bạn đã từng làm việc với các nền tảng nêu trên là một lợi thế.
Quyền lợi:
Hưởng mức lương hấp dẫn.
Được đóng bảo hiểm và các phúc lợi xã hội khác.
Hưởng phần trăm dự án sao khi hoàn thành. Bonus thêm cho những bạn hoàn thành tốt công việc.
Thưởng tết và các ngày lễ.
Và rất nhiều các đãi ngộ khác…
Nếu thấy bản thân phù hợp nhanh tay gửi CV vào địa chỉ email: jobs@enguys.com.

Thông tin liên hệ:
Công ty CP An Phát Miền Trung
Số 04, đường Phan Huy Chú, Vinh, Nghệ An
Người liên hệ: Minh 0977 640 243',
                            'price' => '0',
                            'price_min' => NULL,
                            'ads_type' => 'buy',
                            'price_type' => NULL,
                            'state' => NULL,
                            'status' => 'active',
                            'address' => 'Số 04, đường Phan Huy Chú, Vinh, Nghệ An',
                            'lat' => 18.654841000000001,
                            'lng' => 105.696884,
                            'view_count' => 1,
                            'images' => NULL,
                            'created_at' => '2017-10-11 13:34:57',
                            'updated_at' => '2017-10-11 14:49:02',
                            'deleted_at' => NULL,
                            'cate_id' => 165,
                            'user_id' => 20,
                        ),
                        60 => 
                        array (
                            'id' => 61,
                            'title' => 'NHÂN VIÊN IT',
                            'slug' => 'nhan-vien-it',
                            'des' => 'GIỚI TÍNH: NAM
I- MÔ TẢ CÔNG VIỆC:
1. Quản trị hệ thống máy tính, hỗ trợ thành viên công ty hoạt động hiệu quả
2. Thiết lập hệ thống mạng nội bộ cho công ty.
3. Xử lý các sự cố về mạng.
4. Quản lý các thiết bị công nghệ thông tin và công nghệ của công ty.
5. Lên kế hoạch bảo trì, nâng cấp hệ thống, thiết bị định kỳ.
6. Đề xuất các phương án bảo trì, nâng cấp hệ thống, thay đổi dịch vụ theo yêu cầu phát triển của công ty.
7. Hỗ trợ Hành chính quản lý, bảo trì, thay thế tài sản thiết bị văn phòng.
8. Thực hiện một số công việc theo yêu cầu Trưởng phòng và Ban Giám đốc.
II- YÊU CẦU NĂNG LỰC VÀ KINH NGHIỆM LÀM VIỆC:
1. Tốt nghiệp từ Cao Đẳng trở lên thuộc chuyên ngành CNTT.
2. Có kinh nghiệm tối thiểu 01 năm về ngôn ngữ lập trình PHP, biết HTML, CSS, lập trình PHP, SEO, quản trị web….; về quản trị mạng, quản trị hệ tống server; về phần cứng máy tính.
3. Có trách nhiệm với công việc, làm việc nghiêm túc, nhanh nhẹn, nhiệt tình.
4 . Không có tiền án, tiền sự.
5 . Tiếng anh giao tiếp tốt.
III- QUYỀN LỢI ĐƯỢC HƯỞNG:
1- Mức lương thỏa thuận, tùy vào năng lực và kinh nghiệm làm việc
2- Được công ty đóng bảo hiểm xã hội, bảo hiểm y tế, bảo hiểm thất nghiệp
3- Được trang bị dụng cụ, thiết bị làm việc, đồng phục…
4- Được nghỉ phép năm; hưởng các khoản trợ cấp: thăm hỏi ốm đau, tang chế… và tiền làm thêm giờ theo quy định của Bộ luật lao động và công ty.
IV- HỒ SƠ DỰ TUYỂN:
1. Sơ yếu lý lịch (có xác nhận của chính quyền địa phương, kèm theo ảnh có đóng dấu giáp lai).
2. Chứng minh thư nhân dân + HKTT (01 bản công chứng và mang theo bản chính khi nộp hồ sơ).
3. Đơn xác nhận nhân sự (CA xã/phường xác nhận có đóng ảnh giáp lai)
4. Đơn xin viêc
5. Bằng cấp, chứng chỉ liên quan.
V- THỜI GIAN VÀ ĐỊA ĐIỂM LIÊN HỆ:
1. Thời gian tiếp nhận hồ sơ: từ ngày 10/10/2017 đến ngày 17 /10/2017(7h30- 16h30 hằng ngày)
2. Nơi nhận hồ sơ: Công Ty TNHH VN Nam Đàn Vạn An - Xóm 7, Nam Giang, Nam Đàn, Nghệ An.
3. Địa chỉ liên hệ: gửi hồ sơ đến địa chỉ: Tuyendung@vnnv.com.vn
4. SĐT: 02383 920999',
                            'price' => NULL,
                            'price_min' => NULL,
                            'ads_type' => 'buy',
                            'price_type' => 'treat',
                            'state' => NULL,
                            'status' => 'active',
                            'address' => 'Xóm 7, Nam Giang, Nam Đàn, Nghệ An',
                            'lat' => 18.734736999999999,
                            'lng' => 105.554765,
                            'view_count' => 1,
                            'images' => NULL,
                            'created_at' => '2017-10-11 13:38:41',
                            'updated_at' => '2017-10-11 14:49:07',
                            'deleted_at' => NULL,
                            'cate_id' => 139,
                            'user_id' => 20,
                        ),
                        61 => 
                        array (
                            'id' => 62,
                            'title' => 'Nhân viên Bán Hàng',
                            'slug' => 'nhan-vien-ban-hang',
                            'des' => '1. Số Lượng : 01 
2. Mô tả công việc. 
- Giới thiệu thông tin sản phẩm tới khách hàng. 
- Bán hàng đạt chỉ tiêu cty đề ra. 
- Xây dựng mối quan hệ với khách hàng. 
3. Quyền lơi. 
- Lương Cứng : 4.000.000
- Thưởng : 
+ Đạt 100% chỉ tiêu : 5.000.000
+ Đạt KPI : 2.000.000
=> Total : 11.000.000
- Được nghỉ chủ nhật, các ngày lễ tết theo quy định. 
- Được đóng bảo hiểm sau 6 tháng gắn bó với cty. 
4. Yêu Cầu. 
- Nhanh nhẹn, chăm chỉ, chịu khó, giao tiếp tốt. 
- Chịu được áp lực công việc. 
- Người có kinh nghiệm thị trường là 1 lợi thế.
- Thành thạo địa bàn TpVinh và Huyện lân cận.
- Có Smartphone để làm việc qua Viber or Zalo. 
Ứng viên quan tâm vui lòng liên hệ : 
Anh Thịnh : 0931.113.186
Hoặc gửi CV qua địa chỉ mail :
Buitrinhthinh@gmail.com.
Thanks all.',
                            'price' => '0',
                            'price_min' => NULL,
                            'ads_type' => 'buy',
                            'price_type' => NULL,
                            'state' => NULL,
                            'status' => 'active',
                            'address' => 'Số 43 Phạm Huy P.Quan Bàu',
                            'lat' => 18.689394,
                            'lng' => 105.667845,
                            'view_count' => 1,
                            'images' => NULL,
                            'created_at' => '2017-10-11 13:41:06',
                            'updated_at' => '2017-10-11 14:49:15',
                            'deleted_at' => NULL,
                            'cate_id' => 107,
                            'user_id' => 20,
                        ),
                        62 => 
                        array (
                            'id' => 63,
                            'title' => 'GIÁM ĐỐC MIỀN',
                            'slug' => 'giam-doc-mien',
                            'des' => '1. Mô tả công việc:
- Điều hành toàn bộ hoạt động kinh doanh của tối thiểu 1 nhãn hàng Sơn của 1 Công Ty (Thuộc Tập đoàn Sơn Đại Nam). Triển khai trong khu vực được giao quản lý từ 3->5 tỉnh thuộc cả 3 miền Bắc – Trung – Nam. 

- Tuyển dụng và quản lý hệ thống giám sát bán hàng, từ 20 GSBH trở lên.

- Mở được nhà phân phối

- Tuyển dụng và hỗ trợ NPP tiêu thụ sản phẩm

- Lập kế hoạch triển khai thị trường trong khu vực mình quản lý.

- Báo cáo công việc theo quy định của tập đoàn.

2. Yêu cầu

- Có độ tuổi từ 30 trở lên, có tối thiểu 02 năm kinh nghiệm

- Điều hành và quản lý trong Các doanh nghiệp với hệ thống nhân sự 30 người trở lên, ưu tiên những người có kinh nghiệm trong lĩnh vực: Xây dựng, phân phối các ngành hàng có liên quan đến vật liệu xây dựng, lĩnh vực sơn..

- Có khả năng chịu áp lực công việc cao

- Có khả năng đàm phán và đưa ra quyết định kịp thời chính xác

- Có khả năng đào tạo và hướng dẫn nhân sự thực thi nhiệm vụ

3 Quyền lợi:

- Được hưởng chế độ ưu đãi không hạn chế mức lương

- Thu nhập = lương cơ bản( 15tr – 100tr tùy năng lực) + hoa hồng doanh số + các khoản thưởng + phụ cấp công tác

- Các chế độ khác theo quy định của nhà nước.

4. Yêu cầu hồ sơ:
- Đơn xin việc viết tay.
- Sơ yếu lý lịch, hộ khẩu, chứng minh nhân dân (tất cả photo có công chứng, xác nhận của chính quyền địa phương và mới nhất trong vòng 6 tháng).
- Giấy khám sức khỏe (mới nhất trong vòng 6 tháng).
- 4 ảnh 3x4 (mới nhất trong vòng 1 tháng).
- Các bằng cấp liên quan.
5. Thông tin liên hệ:
Trang– Phòng Hành chính Nhân sự

SĐT: 0974.714.047
Hình thức nộp hồ sơ:
+ Trực tiếp: Xóm 10, Xã Nghi Liên, Tp.Vinh, Nghệ An.

+ Qua gmail: bantuyendung.vinh.laztu@gmail.com',
                            'price' => '0',
                            'price_min' => NULL,
                            'ads_type' => 'buy',
                            'price_type' => NULL,
                            'state' => NULL,
                            'status' => 'active',
                            'address' => 'Xóm 10, Xã Nghi Liên, Tp.Vinh, Nghệ An',
                            'lat' => 18.750437000000002,
                            'lng' => 105.65884800000001,
                            'view_count' => 1,
                            'images' => NULL,
                            'created_at' => '2017-10-11 13:47:56',
                            'updated_at' => '2017-10-11 14:49:18',
                            'deleted_at' => NULL,
                            'cate_id' => 108,
                            'user_id' => 20,
                        ),
                        63 => 
                        array (
                            'id' => 64,
                            'title' => 'Cần tìm Giúp việc gia đình',
                            'slug' => 'can-tim-giup-viec-gia-dinh',
                            'des' => 'có 2 bạn nữ muốn tìm việc làm thêm như giúp việc gia đinh vào khung giờ 9-13h hoặc từ 16h 30 trở đi. Ai có nhu cầu liên hệ hộ -mình với',
                            'price' => '0',
                            'price_min' => NULL,
                            'ads_type' => 'sell',
                            'price_type' => NULL,
                            'state' => NULL,
                            'status' => 'active',
                            'address' => 'Thành phố Vinh, Nghệ An, Việt Nam',
                            'lat' => 18.679584999999999,
                            'lng' => 105.681333,
                            'view_count' => 1,
                            'images' => NULL,
                            'created_at' => '2017-10-11 13:55:23',
                            'updated_at' => '2017-10-11 14:49:22',
                            'deleted_at' => NULL,
                            'cate_id' => 128,
                            'user_id' => 20,
                        ),
                        64 => 
                        array (
                            'id' => 65,
                            'title' => 'NHẬN LÀM KẾ TOÁN THUẾ CHO CÁC DN',
                            'slug' => 'nhan-lam-ke-toan-thue-cho-cac-dn',
                        'des' => 'Báo cáo THUẾ cho các DN : Là KẾ TOÁN TRƯỞNG GIỎI) với thâm niên trên 20 năm kinh nghiệm KẾ TOÁN TRƯỞNG trong các ngành nghề:

+ Sản xuất.

+ Thương mại.

+ Dịch vụ .

+ Khai thác

+ Xuất nhập khẩu ....

+ Đặc biệt là Ngành Xây Dựng.

Nhận làm KẾ TOÁN trọn gói hoặc Làm BÁO CÁO THUẾ ; Hạch Toán, vào sổ, làm Báo Cáo Tài Chính cho các DN , tư vấn, giải thích và đưa ra giải pháp cụ thể phù hợp cho từng DN . Hoàn thiện sổ sách, lập báo cáo định kỳ,vào sổ chi tiết , Lập Báo cáo tài chính và những công việc liên quan khác của Kế toán. Với cách làm việc rất khoa học, nhiều kinh nghiệm, nhiệt tình, cẩn thận và tâm huyết với công việc. Sẽ mang lại hiệu quả tốt nhất cho DN ..
Những Doanh nghiệp nào cùng hợp tác sẽ được hỗ trợ, tư vấn miễn phí về Công tác Tài chính, Thuế , Kế toán và Phương pháp tạo hồ sơ Chặt chẽ, hợp lý và logic để các Doanh nghiệp luôn tự tin trước các kỳ báo cáo thuế và trước các đoàn kiểm tra.',
                        'price' => '0',
                        'price_min' => NULL,
                        'ads_type' => 'buy',
                        'price_type' => NULL,
                        'state' => NULL,
                        'status' => 'active',
                        'address' => 'Hà Huy Tập, Tp. Vinh, Nghệ An, Việt Nam',
                        'lat' => 18.693332000000002,
                        'lng' => 105.68524600000001,
                        'view_count' => 1,
                        'images' => NULL,
                        'created_at' => '2017-10-11 14:01:27',
                        'updated_at' => '2017-10-11 14:49:26',
                        'deleted_at' => NULL,
                        'cate_id' => 112,
                        'user_id' => 20,
                    ),
                    65 => 
                    array (
                        'id' => 66,
                        'title' => 'Nhận dọn nhà theo giờ',
                        'slug' => 'nhan-don-nha-theo-gio',
                        'des' => 'em nhận dọn dẹp nhà cửa và giúp việc theo giờ nhanh nhẹn gọn gàng sạch sẽ thật thà .ai có nhu cầu cần dọn nhà Theo giờ',
                        'price' => NULL,
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'treat',
                        'state' => NULL,
                        'status' => 'active',
                        'address' => 'Thành phố Vinh, Nghệ An, Việt Nam',
                        'lat' => 18.679584999999999,
                        'lng' => 105.681333,
                        'view_count' => 1,
                        'images' => NULL,
                        'created_at' => '2017-10-11 14:05:59',
                        'updated_at' => '2017-10-11 14:49:30',
                        'deleted_at' => NULL,
                        'cate_id' => 128,
                        'user_id' => 20,
                    ),
                    66 => 
                    array (
                        'id' => 67,
                        'title' => 'Lái xe biết tiếng trung tìm việc',
                        'slug' => 'lai-xe-biet-tieng-trung-tim-viec',
                        'des' => 'Như tiêu đề, e cần tìm việc lái xe B2. Kinh nghiệm 7 năm. Bao gồm lái xe cho Sếp, taxi và xe tải chở hàng. E có thể giao tiếp tiếng trung thành thạo. Vậy quý cty hay cá nhân nào có nhu cầu tuyển dụng',
                        'price' => NULL,
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'treat',
                        'state' => NULL,
                        'status' => 'active',
                        'address' => 'Thành phố Vinh, Nghệ An, Việt Nam',
                        'lat' => 18.679584999999999,
                        'lng' => 105.681333,
                        'view_count' => 1,
                        'images' => NULL,
                        'created_at' => '2017-10-11 14:08:07',
                        'updated_at' => '2017-10-11 14:49:33',
                        'deleted_at' => NULL,
                        'cate_id' => 149,
                        'user_id' => 20,
                    ),
                    67 => 
                    array (
                        'id' => 68,
                        'title' => 'TÌm việc chạy bàn quán ăn, bán hàng quần áo',
                        'slug' => 'tim-viec-chay-ban-quan-an-ban-hang-quan-ao',
                        'des' => 'Em Ngô Thị Trang, sinh năm 1998.
Quê ở Trung Thành, Yên Thành - Nghệ An.
Muốn Xin việc ăn ở lại để đỡ chi phí
Cao 1m57, hiền lành, chịu khó, muốn đi làm để kiếm thêm thu nhập đỡ gia đình
Trước đã làm chạy bàn và bán hàng giày dép rồi
Việc muốn tìm: có thể là chạy bàn quán ăn, bán hàng quần áo....
Vậy Anh/Chị nào cần người làm thì liên hệ theo',
                        'price' => NULL,
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'treat',
                        'state' => NULL,
                        'status' => 'active',
                        'address' => 'Nghệ An, Việt Nam',
                        'lat' => 19.234248999999998,
                        'lng' => 104.920036,
                        'view_count' => 1,
                        'images' => NULL,
                        'created_at' => '2017-10-11 14:11:16',
                        'updated_at' => '2017-10-11 14:49:37',
                        'deleted_at' => NULL,
                        'cate_id' => 128,
                        'user_id' => 20,
                    ),
                    68 => 
                    array (
                        'id' => 69,
                        'title' => 'Nguyễn Thị Phương',
                        'slug' => 'nguyen-thi-phuong',
                        'des' => '- Mục tiêu ngắn hạn: Có công việc yêu thích, ổn định và củng cố nền tảng kiến thức chuyên môn công việc, cống hiến cho công ty.
- Mục tiêu dài hạn: được thăng lên cấp quản lý, có mức thu nhập cao
- Giao tiếp, thuyết phục, hỗ trợ khách hàng trong lựa chọn và mua sản phẩm
- Vệ sinh và trưng bày hàng hóa
- Nhập hàng và kiểm tra, kiểm kê hàng hóa
- Trưng bày hàng hóa và tư vấn bán hàng
- Thu ngân
- Tham mưu cho Ban lãnh đạo về
Thành tích đạt được:
- Kinh nghiệm bán hàng giúp duy trì lượng khách cũ và thu hút khách hàng mới
- Được thưởng nóng vì có sự đóng góp trong việc tham mưu kinh doanh cho Ban lãnh đạo',
                        'price' => '0',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => NULL,
                        'state' => NULL,
                        'status' => 'active',
                        'address' => 'Nghệ An, Việt Nam',
                        'lat' => 19.234248999999998,
                        'lng' => 104.920036,
                        'view_count' => 1,
                        'images' => NULL,
                        'created_at' => '2017-10-11 16:14:18',
                        'updated_at' => '2017-10-11 16:50:54',
                        'deleted_at' => NULL,
                        'cate_id' => 159,
                        'user_id' => 20,
                    ),
                    69 => 
                    array (
                        'id' => 70,
                        'title' => 'Tìm Việc Làm',
                        'slug' => 'tim-viec-lam',
                        'des' => 'Tôi nữ 55 tuổi ,tìm viêc làm phù hợp ,lâu dài. Có kinh nghiệm bán hàng',
                        'price' => '0',
                        'price_min' => NULL,
                        'ads_type' => 'buy',
                        'price_type' => NULL,
                        'state' => NULL,
                        'status' => 'active',
                        'address' => 'Nghệ An, Việt Nam',
                        'lat' => 19.234248999999998,
                        'lng' => 104.920036,
                        'view_count' => 1,
                        'images' => NULL,
                        'created_at' => '2017-10-11 16:19:06',
                        'updated_at' => '2017-10-11 16:50:57',
                        'deleted_at' => NULL,
                        'cate_id' => 107,
                        'user_id' => 20,
                    ),
                    70 => 
                    array (
                        'id' => 71,
                        'title' => 'EM SINH NĂM 1997 XIN VIỆC LÀM',
                        'slug' => 'em-sinh-nam-1997-xin-viec-lam',
                        'des' => '- Em tên là Trần thị kim Hiền , sinh năm 1997
- Quê Cát Văn , Thanh Chương
- Em từng làm ở Đà Nẵng 3 năm cty giặt là
- giờ e xin làm ở vinh lương 4 triệu, 
- em có kinh nghiệm chăm em, nhà hàng, giặt là được hết
em đang độc thân nên cần công viẹc làm',
                        'price' => NULL,
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'treat',
                        'state' => NULL,
                        'status' => 'active',
                        'address' => 'Thành phố Vinh, Nghệ An, Việt Nam',
                        'lat' => 18.679584999999999,
                        'lng' => 105.681333,
                        'view_count' => 1,
                        'images' => NULL,
                        'created_at' => '2017-10-11 16:21:37',
                        'updated_at' => '2017-10-11 16:51:00',
                        'deleted_at' => NULL,
                        'cate_id' => 107,
                        'user_id' => 20,
                    ),
                    71 => 
                    array (
                        'id' => 72,
                        'title' => 'E là Trang đang tìm việc làm phổ thông',
                        'slug' => 'e-la-trang-dang-tim-viec-lam-pho-thong',
                        'des' => 'E là Trang, sinh năm 1998, ở Thanh Phong, Thanh Chương, Nghệ An e đang rất cần việc. Ai có việc làm phổ thông nào giới thiệu giúp e vơi ạ',
                        'price' => NULL,
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'treat',
                        'state' => NULL,
                        'status' => 'active',
                        'address' => 'Thanh Phong, Nghệ An, Việt Nam',
                        'lat' => 18.822443,
                        'lng' => 105.354101,
                        'view_count' => 1,
                        'images' => NULL,
                        'created_at' => '2017-10-11 16:23:00',
                        'updated_at' => '2017-10-11 16:51:03',
                        'deleted_at' => NULL,
                        'cate_id' => 128,
                        'user_id' => 20,
                    ),
                    72 => 
                    array (
                        'id' => 73,
                        'title' => 'Nữ sinh năm 96 tìm việc tại vinh',
                        'slug' => 'nu-sinh-nam-96-tim-viec-tai-vinh',
                        'des' => 'Em sinh năm 1996. Có bằng cao đẳng kế toán. Muốn tìm việc ở vinh. Không đúng chuyên ngành cũng được. Ai có nhu cầu gọi em với ạ.',
                        'price' => NULL,
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'treat',
                        'state' => NULL,
                        'status' => 'active',
                        'address' => 'Thành phố Vinh, Nghệ An, Việt Nam',
                        'lat' => 18.679584999999999,
                        'lng' => 105.681333,
                        'view_count' => 1,
                        'images' => NULL,
                        'created_at' => '2017-10-11 16:24:27',
                        'updated_at' => '2017-10-11 16:51:06',
                        'deleted_at' => NULL,
                        'cate_id' => 112,
                        'user_id' => 20,
                    ),
                    73 => 
                    array (
                        'id' => 74,
                        'title' => 'Tìm Việc văn phòng',
                        'slug' => 'tim-viec-van-phong',
                        'des' => 'Tự Tin . thick công ciệc có tính chuyên môn cao . Kỹ năng : đánh văn bản bình thường , biết excel. Thông thạo intenet. phần mềm hướng dẫn qua có thể dùng . khả năg học hỏi nhanh . và một số cái khác có pt đi lại xe máy
yêu cầu lương trên 5tr',
                        'price' => '0',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => NULL,
                        'state' => NULL,
                        'status' => 'active',
                        'address' => 'Nghệ An, Việt Nam',
                        'lat' => 19.234248999999998,
                        'lng' => 104.920036,
                        'view_count' => 1,
                        'images' => NULL,
                        'created_at' => '2017-10-11 16:26:03',
                        'updated_at' => '2017-10-11 16:51:09',
                        'deleted_at' => NULL,
                        'cate_id' => 165,
                        'user_id' => 20,
                    ),
                    74 => 
                    array (
                        'id' => 75,
                        'title' => 'Tìm việc bán hàng',
                        'slug' => 'tim-viec-ban-hang',
                        'des' => 'Em Ngô Thị Trang, sinh năm 1998.
Quê ở Trung Thành, Yên Thành - Nghệ An.
Muốn Xin việc ăn ở lại để đỡ chi phí
Cao 1m57, hiền lành, chịu khó, muốn đi làm để kiếm thêm thu nhập đỡ gia đình
Trước đã làm chạy bàn và bán hàng giày dép rồi
Việc muốn tìm: có thể là chạy bàn quán ăn, bán hàng quần áo....',
                        'price' => NULL,
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'treat',
                        'state' => NULL,
                        'status' => 'active',
                        'address' => 'Trung Thành, Nghệ An, Việt Nam',
                        'lat' => 18.970503999999998,
                        'lng' => 105.415594,
                        'view_count' => 1,
                        'images' => NULL,
                        'created_at' => '2017-10-11 16:27:46',
                        'updated_at' => '2017-10-11 16:51:12',
                        'deleted_at' => NULL,
                        'cate_id' => 107,
                        'user_id' => 20,
                    ),
                    75 => 
                    array (
                        'id' => 76,
                        'title' => 'Nam Chuyên Ngành Quản Lý Xin Việc Làm',
                        'slug' => 'nam-chuyen-nganh-quan-ly-xin-viec-lam',
                        'des' => 'Đam mê trong lĩnh vực quản lý 
Em cần tìm việc làm tại vinh hoặc hà tĩnh
Hồ sơ đầy đủ tốt nghiệp 12/12
Từng có kinh nghiệm trong công việc nhà hàng khách sạn hàng tạp hoá
Từng làm quản lý hơn 1 năm
có tính chất : thật thà làm việc có ý thức có sức chịu áp lực cao
cần bao ăn ở
lương tháng trên 4tr',
                        'price' => '0',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => NULL,
                        'state' => NULL,
                        'status' => 'active',
                        'address' => 'Nghệ An, Việt Nam',
                        'lat' => 19.234248999999998,
                        'lng' => 104.920036,
                        'view_count' => 1,
                        'images' => NULL,
                        'created_at' => '2017-10-11 16:29:36',
                        'updated_at' => '2017-10-11 16:51:16',
                        'deleted_at' => NULL,
                        'cate_id' => 108,
                        'user_id' => 20,
                    ),
                    76 => 
                    array (
                        'id' => 77,
                        'title' => 'Nam 1994 xin việc làm',
                        'slug' => 'nam-1994-xin-viec-lam',
                        'des' => 'Em tên Huy. Sinh năm 1994. Cao 1m75. Nặng 58kg. 
Có kỹ năng 2 năm tự doanh: tư vấn bán hàng - chăm sóc khách hàng tốt. 
E vừa học xong cao đẳng điện kt3.
Em mong muốn tìm 1 công việc bán hàng. Quý công ty - doanh nghiệp nào đang cần người xin cho e 1 cơ hội phát triển.',
                        'price' => NULL,
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'treat',
                        'state' => NULL,
                        'status' => 'active',
                        'address' => 'Nghệ An, Việt Nam',
                        'lat' => 19.234248999999998,
                        'lng' => 104.920036,
                        'view_count' => 1,
                        'images' => NULL,
                        'created_at' => '2017-10-11 16:31:04',
                        'updated_at' => '2017-10-11 16:51:19',
                        'deleted_at' => NULL,
                        'cate_id' => 107,
                        'user_id' => 20,
                    ),
                    77 => 
                    array (
                        'id' => 78,
                        'title' => 'Nữ 1995 cần tìm việc',
                        'slug' => 'nu-1995-can-tim-viec',
                    'des' => 'Em cần tìm việc làm từ thứ 2 đến thứ 6 (thứ 7, chủ nhật hàng tuần e bận đi học thêm).
Em đã từng làm nhân viên văn phòng và bán hàng.
Em đã tốt nghiệp đh ngành luật kinh tế Đhv 
Em muốn đc gắn bó lâu dài ạ',
                        'price' => NULL,
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'treat',
                        'state' => NULL,
                        'status' => 'active',
                        'address' => 'Nghệ An, Việt Nam',
                        'lat' => 19.234248999999998,
                        'lng' => 104.920036,
                        'view_count' => 1,
                        'images' => NULL,
                        'created_at' => '2017-10-11 16:33:32',
                        'updated_at' => '2017-10-11 16:51:22',
                        'deleted_at' => NULL,
                        'cate_id' => 107,
                        'user_id' => 20,
                    ),
                    78 => 
                    array (
                        'id' => 79,
                        'title' => 'Nữ 92 tìm việc',
                        'slug' => 'nu-92-tim-viec',
                        'des' => 'Nguyễn thị hoa 
Sn 07-02-1992
Tốt nghiệp cd công nghệ thông tin
Đã có kinh nghiệm 2 năm văn phòng 
Cần tìm việc tương đương',
                        'price' => NULL,
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'treat',
                        'state' => NULL,
                        'status' => 'active',
                        'address' => 'Nghệ An, Việt Nam',
                        'lat' => 19.234248999999998,
                        'lng' => 104.920036,
                        'view_count' => 1,
                        'images' => NULL,
                        'created_at' => '2017-10-11 16:34:55',
                        'updated_at' => '2017-10-11 16:51:25',
                        'deleted_at' => NULL,
                        'cate_id' => 165,
                        'user_id' => 20,
                    ),
                    79 => 
                    array (
                        'id' => 80,
                        'title' => 'Tìm việc bán hàng',
                        'slug' => 'tim-viec-ban-hang-1',
                        'des' => 'nữ 96 cần tìm việc như bán hàng áo quần. quán cafe... không cần hồ sơ. làm trong giờ hành chính. gần trường kinh tế càng tốt ạ',
                        'price' => NULL,
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'treat',
                        'state' => NULL,
                        'status' => 'active',
                        'address' => 'Nghệ An, Việt Nam',
                        'lat' => 19.234248999999998,
                        'lng' => 104.920036,
                        'view_count' => 1,
                        'images' => NULL,
                        'created_at' => '2017-10-11 16:36:09',
                        'updated_at' => '2017-10-11 16:51:28',
                        'deleted_at' => NULL,
                        'cate_id' => 107,
                        'user_id' => 20,
                    ),
                    80 => 
                    array (
                        'id' => 81,
                        'title' => 'Nữ 1999 Muốn tìm việc ở lại luôn',
                        'slug' => 'nu-1999-muon-tim-viec-o-lai-luon',
                        'des' => 'hoc het lop 12
muon đi làm khoảng 3 năm
muốn nơi nào ăn ở lại',
                        'price' => NULL,
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'treat',
                        'state' => NULL,
                        'status' => 'active',
                        'address' => 'Nghệ An, Việt Nam',
                        'lat' => 19.234248999999998,
                        'lng' => 104.920036,
                        'view_count' => 1,
                        'images' => NULL,
                        'created_at' => '2017-10-11 16:37:28',
                        'updated_at' => '2017-10-11 16:51:32',
                        'deleted_at' => NULL,
                        'cate_id' => 107,
                        'user_id' => 20,
                    ),
                    81 => 
                    array (
                        'id' => 82,
                        'title' => 'Xe	Ford EcoSport	Titanium 1.5L AT	2017',
                        'slug' => 'xe-ford-ecosport-titanium-1-5l-at-2017',
                    'des' => 'Ford EcoSport 1.5 AT, Động cơ Eco mạnh mẽ. Số tự động 6 cấp. 7 Túi khí . Đèn sương mù trước/sau. Chốt khoá cửa an toàn cho trẻ em. Chìa khóa thông minh. Cảm Biến gạt mưa, đèn pha, hỗ trợ khởi hành ngang dốc,. Gương kính chỉnh điện. Điều hòa tự động. Dàn CD âm thanh chuẩn, 6 loa. Vô lăng gật gù trợ lực điều khiển điện tử. Hệ thống chống bó cứng phanh (ABS). Hệ thống phân phối lực phanh điện tử (EBD), hệ thống chống trơn trượt (ESP), trợ lực phanh khẩn cấp (BA)...
Vui lòng liên hệ trực tiếp với chúng tôi!',
                        'price' => '658000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'new',
                        'status' => 'active',
                        'address' => 'Đại lộ Lê Nin, Thành phố vinh, Tỉnh nghệ an',
                        'lat' => 18.707173000000001,
                        'lng' => 105.679686,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n2014-ford-ecosport-550x309-1507773132.jpg","\\r\\n\\r\\n14637966013489233658-1507773135.jpg","\\r\\n\\r\\n587c4ce94505c-1507773136.jpg","\\r\\n\\r\\necosport-14-1507773138.jpg","\\r\\n\\r\\ngia-xe-ford-ecosport-2017-1-1507773139.jpg","\\r\\n\\r\\nford-ecosport-black-edition-autodaily-8-1507773141.jpg"]',
                        'created_at' => '2017-10-12 08:52:42',
                        'updated_at' => '2017-10-12 13:41:51',
                        'deleted_at' => NULL,
                        'cate_id' => 170,
                        'user_id' => 20,
                    ),
                    82 => 
                    array (
                        'id' => 83,
                        'title' => 'Xe Daewoo Lanos SX 2004',
                        'slug' => 'xe-daewoo-lanos-sx-2004',
                        'des' => '- Nội thất: Xám
- Màu ngoại thất : Trắng
Bán Daewoo Lanos màu trắng, đời 2004. Xe đẹp, nội ngoại thất đẹp, đài AM/FM, radio, điều hòa mát, máy khỏe.
Vui lòng liên hệ với tôi.',
                        'price' => '68000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Nghệ An, Việt Nam',
                        'lat' => 19.234248999999998,
                        'lng' => 104.920036,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n2916551228-1507775483.jpg","\\r\\n\\r\\n20170711c21d7cfe-5db-df41-1507775481.jpg","\\r\\n\\r\\nc4160961d7f1e09a83cd2800x800-662b-1507775479.jpg","\\r\\n\\r\\n04c028ef13d880ea5718f960x720-0de3-1507775476.jpg","\\r\\n\\r\\n2017071184410d65-e1d-c608-1507775482.jpg"]',
                        'created_at' => '2017-10-12 09:32:01',
                        'updated_at' => '2017-10-12 13:41:58',
                        'deleted_at' => NULL,
                        'cate_id' => 171,
                        'user_id' => 20,
                    ),
                    83 => 
                    array (
                        'id' => 84,
                        'title' => 'Xe Honda Civic 1.5L Vtec Turbo 2017',
                        'slug' => 'xe-honda-civic-1-5l-vtec-turbo-2017',
                        'des' => 'Honda Civic 1.5L Vtec Turbo 2017 
Nhập khẩu nguyên chiếc từ Thái Lan. Công suất cực đại 170 Hp với thiết kế ngoại thất hoàn toàn mới trang bị cửa số trời, mâm 17 inch, đèn chạy ban ngày, đèn pha công nghệ Led. Động cơ 1.5 VTEC TURBO tăng tốc nhanh và mạnh mẽ tương đương động cơ 2.4L thường mà vẫn tiết kiệm nhiên liệu. Nội thất được trang bị màn hình cảm ứng 7 inch kết nối wifi, đàm thoại rảnh tay, vô lăng tích hợp nút điều chỉnh đa thông tin, thắng tay điện tử,....Hệ thống an toàn trang bị VSA, AHA, TCS, ABS, EBD, BA, ESS, HSA, 6 túi khí, camera lùi 3 góc quay, khóa cửa tự động...',
                        'price' => '89800000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'new',
                        'status' => 'active',
                        'address' => 'Đại lộ Lê nin – Phường Hưng Dũng – Thành Phố Vinh Nghệ An',
                        'lat' => 18.677146,
                        'lng' => 105.69452800000001,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\nhonda-oto-viet-nam-moi-lai-thu-civic-the-he-moi-tren-toan-quoc-1-1507776079.jpg","\\r\\n\\r\\n1-286235-1507776083.jpg","\\r\\n\\r\\n150444793697341-01-1507776077.jpg","\\r\\n\\r\\ndsc00114-1507776086.jpg","\\r\\n\\r\\n204783524-honda-civic-2017-1507776081.jpg","\\r\\n\\r\\n201707227457f34b-41c-364c-1507776064.jpg","\\r\\n\\r\\n201707225faf979d-5c5-fa38-1507776066.jpg","\\r\\n\\r\\n20170824230113-7200-wm-1507776078.jpg","\\r\\n\\r\\n2016-honda-civic-rear-three-quarter-boot-caught-undisguised-1507776085.jpg","\\r\\n\\r\\ncivic1-zing-1507776082.jpg"]',
                        'created_at' => '2017-10-12 09:42:15',
                        'updated_at' => '2017-10-12 13:42:04',
                        'deleted_at' => NULL,
                        'cate_id' => 168,
                        'user_id' => 20,
                    ),
                    84 => 
                    array (
                        'id' => 85,
                        'title' => 'Xe Mitsubishi Pajero Sport G 4x2 AT 2017',
                        'slug' => 'xe-mitsubishi-pajero-sport-g-4x2-at-2017',
                        'des' => 'Mitsubishi Pajero động cơ xăng V6 3.0L MIVEC, hộp số tự động INVECS 5 cấp Sport Mode kết hợp với lẫy chuyển số trên vô lăng. Đèn Xenon và có thể thay đổi độ cao chiếu sáng.. Kính chiếu hậu mạ crôm, chỉnh điện, gập điện. Tay nắm cửa ngoài mạ Crôm. Bậc lên xuống hông xe kiểu thể thao. Mâm bánh xe hợp kim 17-inch. Bảng điều khiển ốp gỗ. Nút chỉnh âm thanh trên vô lăng. Kính cửa điều khiển điện. Ghế lái chỉnh điện. Tay nắm cửa trong mạ crôm. Hệ thống âm thanh 6 loa. Hệ thống chống bó cứng phanh ABS. Khoá cửa từ xa. Dây đai an toàn với cơ cấu căng đai tự động cho các ghế trước. Hệ thống báo động chống trộm. Đèn sương mù trước viền bạc. Kính cửa màu sậm. Lưới tản nhiệt viền Crôm. Điều hòa nhiệt độ cho từng hàng ghế. Tay lái và cần số bọc da. Lẫy sang số thể thao trên vô lăng. Máy điều hoà hai giàn, tự động. Hệ thống âm thanh DVD với màn hình LCD. Túi khí an toàn đôi. Cảm biến lùi. Hệ thống phân phối lực phanh điện tử EBD',
                        'price' => '867000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'new',
                        'status' => 'active',
                        'address' => 'Đại Lộ Xô Viết Nghệ Tĩnh, TP Vinh Nghệ An',
                        'lat' => 18.715434999999999,
                        'lng' => 105.67395999999999,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n3-40-1507777746.jpg","\\r\\n\\r\\npajero-sport-1-1507777748.jpg","\\r\\n\\r\\n2012mitsubishi-pajero-sport-1507777750.jpg","\\r\\n\\r\\n1ipg-38-1507777751.jpg","\\r\\n\\r\\nall-new-pajero-sport-g-4x2-at-may-xang-1-cau-tu-dong-1507777752.jpg"]',
                        'created_at' => '2017-10-12 10:09:38',
                        'updated_at' => '2017-10-12 13:42:11',
                        'deleted_at' => NULL,
                        'cate_id' => 79,
                        'user_id' => 20,
                    ),
                    85 => 
                    array (
                        'id' => 86,
                        'title' => 'Xe Mazda 2 All new 2017',
                        'slug' => 'xe-mazda-2-all-new-2017',
                        'des' => '- Màu đỏ
Mazda 2 All New hoàn toàn mới với các tính năng vượt trội , Trang thiết bị trên xe với dàn CD nối với 6 loa. Điều hòa tự động. Không chỉ đơn thuần chỉ là một phương tiện đi lại .',
                        'price' => '53500000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Ngã tư sân bay, Đại lộ Lenin, Tp Vinh Nghệ An',
                        'lat' => 18.719273999999999,
                        'lng' => 105.67174199999999,
                        'view_count' => 2,
                        'images' => '["\\r\\n\\r\\nmazda2-sedan-620x350-1507778368.jpg","\\r\\n\\r\\n1492525900-149224106913923-00-1507778369.jpg","\\r\\n\\r\\n1492525900-149224107912512-02-1507778370.jpg","\\r\\n\\r\\n1492525901-149224110099228-03-1507778372.jpg","\\r\\n\\r\\nmazda2-sedan-2-e1429498084625-1507778373.jpg","\\r\\n\\r\\nmazda-2-sedan-001-1-1507778374.jpg","\\r\\n\\r\\nmazda2-1507778375.jpg"]',
                        'created_at' => '2017-10-12 10:20:08',
                        'updated_at' => '2017-10-13 09:10:14',
                        'deleted_at' => NULL,
                        'cate_id' => 70,
                        'user_id' => 20,
                    ),
                    86 => 
                    array (
                        'id' => 87,
                        'title' => 'Xe Mercedes Benz GLE Class GLE 400 4Matic 2017',
                        'slug' => 'xe-mercedes-benz-gle-class-gle-400-4matic-2017',
                    'des' => 'Bán Mercedes GLE 400 4matic với hộp số tự động 7 cấp 7G-TRONIC PLUS.Lưới tản nhiệt 2 nan với ngôi sao 3 cánh ở chính giữa.Mâm xe 18-inch 10 chấu.Bậc lên xuống bằng hợp kim nhôm.Chụp ống xả kép mạ chrome tích hợp với cản sau.Camera lùi hỗ trợ người lái quan sát phía sau khi lùi/đỗ xe.Cụm đèn trước LED toàn phần (Full-LED) thông minh.Đèn báo rẽ trên gương chiếu hậu, cụm đèn sau & đèn phanh thứ ba công nghệ LED.Gương chiếu hậu chống chói tự động;...',
                        'price' => '3519000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'new',
                        'status' => 'active',
                        'address' => 'Số 3, Mai Hắc Đế, TP. Vinh Nghệ An',
                        'lat' => 18.696771999999999,
                        'lng' => 105.67496800000001,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\nmercedes-gle-400-1507778881.jpg","\\r\\n\\r\\nmercedes-gle-400-4matic-1507778883.jpg","\\r\\n\\r\\nmercedes-phumyhung-mercedes-gle-400-4matic-coupe-2016-5-1507778884.jpg","\\r\\n\\r\\nmercedes-gle-400-4matic-exclusive-9-1507778886.jpg","\\r\\n\\r\\nmercedes-benz-gls-klasse-10-2-1507778887.jpg"]',
                        'created_at' => '2017-10-12 10:28:40',
                        'updated_at' => '2017-10-12 13:42:24',
                        'deleted_at' => NULL,
                        'cate_id' => 71,
                        'user_id' => 20,
                    ),
                    87 => 
                    array (
                        'id' => 88,
                        'title' => 'Xe Acura MDX SH-AWD	2007',
                        'slug' => 'xe-acura-mdx-sh-awd-2007',
                        'des' => 'Bán Acura MDX bản đầy đủ không thiếu thứ gì,xe tư nhân chính chủ, hình thức và chất lượng đẹp từ trong ra ngoài, phụ kiện đầy đủ,mọi thứ hoạt động hoàn hảo không có lỗi gì cả, cam kết không đâm đụng, ngập nước',
                        'price' => '860000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Thành phố Vinh, Nghệ An, Việt Nam',
                        'lat' => 18.679584999999999,
                        'lng' => 105.681333,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n20170830153455-2b25-wm-1507779527.jpg","\\r\\n\\r\\n112-0706-10z-2007-acura-mdx-interior-view-1507779522.jpg","\\r\\n\\r\\n20170830153454-0ca2-wm-1507779525.jpg","\\r\\n\\r\\n2017082640c97bb3-091-eab6-1507779524.jpg","\\r\\n\\r\\nxc60-rdx-fredericksburg-2-1507779528.jpg"]',
                        'created_at' => '2017-10-12 10:39:19',
                        'updated_at' => '2017-10-12 13:42:31',
                        'deleted_at' => NULL,
                        'cate_id' => 168,
                        'user_id' => 20,
                    ),
                    88 => 
                    array (
                        'id' => 89,
                        'title' => 'Xe Toyota Camry LE 2.4	2008',
                        'slug' => 'xe-toyota-camry-le-2-4-2008',
                        'des' => 'Bán camry le, xe tư nhân chính chủ, xe nhập khẩu mỹ của sổ trời, xe đi giữ gìn nên nội ngoại thất còn rất đẹp, máy móc nguyên bản không bị đâm đụng ngập nước, đảm bảo xem xe thích ngay, giá tốt cho anh em tiền tươi thóc thật có thiện chí mua sử dụng',
                        'price' => '680000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Thành phố Vinh, Nghệ An, Việt Nam',
                        'lat' => 18.679584999999999,
                        'lng' => 105.681333,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n201706206f325fbc-fcf-c369-1507780035.jpg","\\r\\n\\r\\n425-2008-1507780033.jpg","\\r\\n\\r\\nb41c981d25a943162b025800x600-c6c9-1507780036.jpg","\\r\\n\\r\\nroh1447761205-1507780037.jpg","\\r\\n\\r\\nimg-20150214-1-1507780039.jpg"]',
                        'created_at' => '2017-10-12 10:47:48',
                        'updated_at' => '2017-10-12 13:42:45',
                        'deleted_at' => NULL,
                        'cate_id' => 74,
                        'user_id' => 20,
                    ),
                    89 => 
                    array (
                        'id' => 90,
                        'title' => 'Xe Mercedes Benz Sprinter 311 CDI 2.2L 2005',
                        'slug' => 'xe-mercedes-benz-sprinter-311-cdi-2-2l-2005',
                        'des' => 'Xe chuyên du lịch k chạy tuyến, chạy hợp đồng, máy móc êm, nội thất đẹp, điều hoà mát, 4 lốp mới, nay k có nhu cầu sử dụng cần thanh lý',
                        'price' => '290000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Nghệ An, Việt Nam',
                        'lat' => 19.234248999999998,
                        'lng' => 104.920036,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n201706266201e19f-d75-1507780758.jpg","\\r\\n\\r\\n20170522bc9a0bc7-459-6fca-1507780754.jpg","\\r\\n\\r\\nd48f58cd843e6b21f8381640x360-783d-1507780759.jpg","\\r\\n\\r\\n04a9c9394cc65a3986bc9960x720-ddeb-1507780760.jpg","\\r\\n\\r\\n5427fbe22f26295f071c3960x720-8151-1507780761.jpg"]',
                        'created_at' => '2017-10-12 10:59:26',
                        'updated_at' => '2017-10-12 13:42:37',
                        'deleted_at' => NULL,
                        'cate_id' => 174,
                        'user_id' => 20,
                    ),
                    90 => 
                    array (
                        'id' => 91,
                        'title' => 'Xe Mazda 2 All new 2015',
                        'slug' => 'xe-mazda-2-all-new-2015',
                        'des' => 'Mazda 2 all new .Động cơ xăng 1.5 L. cấp,Số sàn 5 cấp.Chìa khóa điều khiển từ xa. Hệ thống âm thanh 4 loa tích hợp AM/FM/ CD/MP3/ AUX. 02 thắng đĩa ..
Cảm nhận từ sự khác biệt đối với dòng xe phân khúcB',
                        'price' => '535000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'new',
                        'status' => 'active',
                        'address' => 'Ngã tư sân bay, Đại lộ Lenin, Tp Vinh Nghệ An',
                        'lat' => 18.719273999999999,
                        'lng' => 105.67174199999999,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\nunnamed-1507781492.jpg","\\r\\n\\r\\n44f6586ffb1bfee71f6ae1024x768-4ad8-1507781486.jpg","\\r\\n\\r\\n842f696ae13093c7c8a761024x800-2c7b-1507781489.jpg","\\r\\n\\r\\n20141210-can-canh-mazda3-2015-vua-ra-mat-9-1507781491.jpg"]',
                        'created_at' => '2017-10-12 11:11:42',
                        'updated_at' => '2017-10-12 13:42:41',
                        'deleted_at' => NULL,
                        'cate_id' => 70,
                        'user_id' => 20,
                    ),
                    91 => 
                    array (
                        'id' => 92,
                        'title' => 'Xe Mercedes Benz E class E200 2017',
                        'slug' => 'xe-mercedes-benz-e-class-e200-2017',
                        'des' => 'Mercedes Benz E200 2017 màu trắng, Tay lái đa chức năng 4 chấu bọc da nappa.Đồng hồ thời gian analogue với ngôi sao Mercedes-Benz ở chính giữa bảng điều khiển.Hệ thống treo êm ái DIRECT CONTROL.Hệ thống lái Direct-Steer trợ lực điện với trợ lực & tỉ số truyền lái biến thiên theo tốc độ giúp đem lại cảm giác lái tối ưu, hỗ trợ đánh lái thoải mái & chính xác.Lẫy chuyển số bán tự động DIRECT SELECT phía sau tay lái.Chức năng ECO start/stop tự động ngắt động cơ khi xe tạm dừng; giúp giảm thiểu tiêu hao nhiêu liệu & khí xả.Hệ thống tự động bảo vệ PRE-SAFE® kết hợp tối đa các tính năng an toàn trong trường hợp khẩn cấp....',
                        'price' => '2990000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'new',
                        'status' => 'active',
                        'address' => 'Số 3, Mai Hắc Đế, TP. Vinh Nghệ An',
                        'lat' => 18.696771999999999,
                        'lng' => 105.67496800000001,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\ngia-mercedes-e200-1507782261.jpg","\\r\\n\\r\\nimg-1507782264.jpg","\\r\\n\\r\\nnoi-that-mercedes-s400-700x393-1507782265.jpg","\\r\\n\\r\\nautopress-mer-e-class-31-1507782267.jpg","\\r\\n\\r\\nmercedes-e200-7-1507782268.jpg","\\r\\n\\r\\nmercedes-e200-2017-mau-trang-b-1507782270.jpg"]',
                        'created_at' => '2017-10-12 11:25:01',
                        'updated_at' => '2017-10-12 13:42:56',
                        'deleted_at' => NULL,
                        'cate_id' => 71,
                        'user_id' => 20,
                    ),
                    92 => 
                    array (
                        'id' => 93,
                        'title' => 'Xe Ford Ranger XLT 2.2L 4x4 MT	2017',
                        'slug' => 'xe-ford-ranger-xlt-2-2l-4x4-mt-2017',
                    'des' => 'Ford Ranger XLT , xe nhập khẩu , Động cơ Turbo Diesel 2.2L TDCi, trục cam kép, có làm mát khí nạp. 6 số tay. Hai cầu chủ động / 4x4. Khoá cửa điều khiển từ xa. Đèn pha & gạt mưa tự động. Đèn sương mù. Trợ lực lái, ga tự động. Khoá cửa điện. 2 Túi khí phía trước. Hệ thống chống bó cứng phanh (ABS). Hệ thống âm thanh AM/FM, CD 1 đĩa, MP3, Ipod & USB. Màn hình hiển thị đa thông tin. Kết nối không dây & điều khiển bằng giọng nói. 6 loa....
Vui lòng liên hệ với chúng tôi để biết thêm thông tin!',
                        'price' => '79000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'new',
                        'status' => 'active',
                        'address' => 'Đại lộ Lê Nin, TP Vinh Nghệ An',
                        'lat' => 18.711506,
                        'lng' => 105.677661,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\nf9a9c46de84b2cr775189-fc3b-1507783666.jpg","\\r\\n\\r\\nvtxh-resized640x480-1507783669.jpg","\\r\\n\\r\\n54c8d89278ce45d73ccbc600x360-21bf-1507783670.jpg","\\r\\n\\r\\nxl-trang-1507783672.jpg","\\r\\n\\r\\nxe-ford-ranger-22l-4x4-mt-xl-6-1507783673.jpg"]',
                        'created_at' => '2017-10-12 11:48:15',
                        'updated_at' => '2017-10-12 13:43:02',
                        'deleted_at' => NULL,
                        'cate_id' => 175,
                        'user_id' => 20,
                    ),
                    93 => 
                    array (
                        'id' => 94,
                        'title' => 'Xe Honda Accord 2.4 AT 2017',
                        'slug' => 'xe-honda-accord-2-4-at-2017',
                        'des' => 'Honda Accord 2.4. Được trang bị hệ thống âm thanh Ghế chỉnh điện, AM/FM/MP3/CD, Hệ thống chống bó cứng phanh ABS, Phân bổ lực phanh EBD, Khe cắm USB, Cảm biến toàn thân, Tay nắm cửa - Viền kính mạ crom, Vôlăng tích hợp điều khiển, Kính chỉnh điện, Nội thất da, Ghế thể thao, Camera lùi,... Đèn gầm, Xi nhan gương, Gương chỉnh điện, Ghế chỉnh điện, Đèn pha cảm ứng, Túi khí hàng ghế trước, Hệ thống chống trộm.......
Vui lòng liên hệ trực tiếp để biết thêm chi tiết!',
                        'price' => '1198000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'new',
                        'status' => 'active',
                        'address' => 'Đại lộ Lê nin – P Hưng Dũng – Thành Phố Vinh Nghệ A',
                        'lat' => 18.682345999999999,
                        'lng' => 105.693444,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\nnormal-2017-honda-accord-2-4-at-20170327035239907-1507784174.jpg","\\r\\n\\r\\n20170115111140-7f12-1507784175.jpg","\\r\\n\\r\\nxehay-hondaaccord-2016-080917-1-1507784176.jpg","\\r\\n\\r\\nimages1152326-3-1507784178.jpg","\\r\\n\\r\\nnew-2017-honda-accord-sedan-touringautomatic-8498-15593908-3-1024-1507784179.jpg","\\r\\n\\r\\nhondaaccord2015-41-2-1507784181.jpg"]',
                        'created_at' => '2017-10-12 11:56:47',
                        'updated_at' => '2017-10-12 13:43:32',
                        'deleted_at' => NULL,
                        'cate_id' => 168,
                        'user_id' => 20,
                    ),
                    94 => 
                    array (
                        'id' => 95,
                        'title' => 'Xe Mercedes Benz E class E300 AMG 2017',
                        'slug' => 'xe-mercedes-benz-e-class-e300-amg-2017',
                        'des' => 'Mercedes Benz E class 300 AMG 2017, Trang bị hộp số tự động 9 cấp 9G-TRONIC. Mâm xe 19inch AMG 5 chấu kép, gói ngoại thất thể thao AMG. Nội thất ốp sợi kim loại, sở hữu 64 màu đèn viền nội thất. Cửa sổ trời siêu rộng Panoramic, 2 màn hình màu 12,3 inch. Hệ thống Airbalance với bộ lọc không khí cao cấp, Camera 360, cửa hít,...',
                        'price' => '2769000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'new',
                        'status' => 'active',
                        'address' => 'Số 3, Mai Hắc Đế, TP. Vinh Nghệ An',
                        'lat' => 18.696771999999999,
                        'lng' => 105.67496800000001,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\nmercedes-e300-2-1507788687.jpg","\\r\\n\\r\\nmercedes-benz-e200-2015-1-1507788682.jpg","\\r\\n\\r\\nmercedes-benz-e-class-81-1507788685.jpg","\\r\\n\\r\\nmercedes-benz-e-class-19-1507788689.jpg","\\r\\n\\r\\nmercedes-benz-e-class-unveiled-at-detroit-auto-show-live-photos-12-1507788691.jpg"]',
                        'created_at' => '2017-10-12 13:12:01',
                        'updated_at' => '2017-10-12 13:43:36',
                        'deleted_at' => NULL,
                        'cate_id' => 71,
                        'user_id' => 20,
                    ),
                    95 => 
                    array (
                        'id' => 96,
                        'title' => 'Xe Toyota Corona GL 2.0 Trước 1990',
                        'slug' => 'xe-toyota-corona-gl-2-0-truoc-1990',
                        'des' => 'Có nhu cầu lên đời nên bán xe cũ. Xe Toyota Corona đời 1988, xe công chức đi hàng ngày, máy êm, điều hòa mát lạnh, gương chỉnh điện, xe được chăm sóc thường xuyên, đài FM,Đầu đĩa, USB đầy đủ, nội thất còn nguyên bản, sườn không mục mọt, phanh, lốp mới thay, xe chạy rất ổn định, rât tiết kiệm xăng, dòng xe nhập khẩu nêm chạy rất đầm 100k/h vẫn êm ru,xe rất phu hợp cho người thu nhập thấp.',
                        'price' => '55000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Nghệ An, Việt Nam',
                        'lat' => 19.234248999999998,
                        'lng' => 104.920036,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n14b74a360fbad0a43d1ac360x480-530b-1507789302.jpg","\\r\\n\\r\\n44a53b155cb692f1052a3640x480-afd3-1507789304.jpg","\\r\\n\\r\\n2017061507b7e37d-0d2-40a8-1507789306.jpg","\\r\\n\\r\\na41b28b0e7cf63f413882640x360-e9c9-1507789307.jpg","\\r\\n\\r\\ntoyota-corona-3539515-5-orig-1507789309.jpg","\\r\\n\\r\\nc42e5bf457d090d9e4d96960x720-582f-1507789310.jpg"]',
                        'created_at' => '2017-10-12 13:22:13',
                        'updated_at' => '2017-10-12 13:44:06',
                        'deleted_at' => NULL,
                        'cate_id' => 74,
                        'user_id' => 20,
                    ),
                    96 => 
                    array (
                        'id' => 97,
                        'title' => 'Xe Kia Cerato 1.6 AT 2017',
                        'slug' => 'xe-kia-cerato-1-6-at-2017',
                        'des' => 'Xe 5 chỗ, động cơ xăng Gamma 1.6L, 4 xy lanh thẳng hàng, Dual-CVVT. 
Dài x Rộng x Cao: 4.560 x 1.780 x 1.445 (mm).
Số tự động (6 cấp) tích hợp lẫy chuyển số 
Điều hòa tự động 2 vùng có lọc khí Ion, có hộc gió cho hàng ghế sau. Hộc giữ lạnh.
Cảm biến hỗ trợ đỗ xe trước sau, Camera lùi (AT).
Nút khởi động & chìa khóa thông minh. Trang bị đèn Welcome (AT).
4 thắng đĩa, hệ thống chống bó cứng phanh ABS.
Tay lái trợ lực điện (MDPS) với 03 chế độ lái. Kết nối Bluetooth (AT).
Hệ thống cân bằng điện tử ESP, hệ thống ổn định thân xe VSM',
                        'price' => '6130000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'new',
                        'status' => 'active',
                        'address' => 'Ngã tư sân bay Vinh, Đại lộ Lê-Nin Nghệ An',
                        'lat' => 18.719273999999999,
                        'lng' => 105.67174199999999,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n11056-2017-forte-sedan-ex-e1452595124714-1507790420.jpg","\\r\\n\\r\\nkia-cerato16-1507790415.jpg","\\r\\n\\r\\nkia-cerato-2017-1507790418.jpg"]',
                        'created_at' => '2017-10-12 13:40:49',
                        'updated_at' => '2017-10-12 13:44:12',
                        'deleted_at' => NULL,
                        'cate_id' => 76,
                        'user_id' => 20,
                    ),
                    97 => 
                    array (
                        'id' => 98,
                        'title' => 'Xe Suzuki Vitara 1.6 AT	2017',
                        'slug' => 'xe-suzuki-vitara-1-6-at-2017',
                        'des' => 'Suzuki Vitara 1.6 AT phiên bản mới.
Được trang bị động cơ 1.6L, kích thước 4500 x 1810 x 1695mm, khoảng sang gầm xe 200mm, hộp số tự động 4 số. Hệ thống đẫn động 2 cầu với 4 chế độ vận hành N, 4H, 4H lock, 4L lock cho phép truyền lực đến 4 bánh xe toàn thời gian để đảm bảo khả năng bám đường tốt của xe trên mọi điều kiện địa hình. 
Liên hệ để biết thêm thông tin!',
                        'price' => '739000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'new',
                        'status' => 'active',
                        'address' => 'số 05B đường Nguyễn Trãi, P. Quán Bàu, TP Vinh Nghệ An',
                        'lat' => 18.70138,
                        'lng' => 105.67321699999999,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n46195210712305146845b11c6a8db75e-1507795563.jpg","\\r\\n\\r\\nhyundai-india-to-launch-ix25-suv-test-drives-1507795561.jpg","\\r\\n\\r\\n13-suzuki-vitara-tai-viet-nam-motorshow-2015-1507795565.jpg","\\r\\n\\r\\ndanh-gia-chi-tiet-oto-suzuki-vitara-2016-doi-moi-nhat-6-1504002833-1507795566.jpg","\\r\\n\\r\\n2015-suzuki-vitara-paris-2014-02-1507795568.jpg"]',
                        'created_at' => '2017-10-12 15:06:30',
                        'updated_at' => '2017-10-13 09:09:26',
                        'deleted_at' => NULL,
                        'cate_id' => 169,
                        'user_id' => 20,
                    ),
                    98 => 
                    array (
                        'id' => 99,
                        'title' => 'Xe Kia Cerato 1.6 AT 2017',
                        'slug' => 'xe-kia-cerato-1-6-at-2017-1',
                        'des' => 'Xe 5 chỗ, động cơ xăng Gamma 1.6L, 4 xy lanh thẳng hàng, Dual-CVVT. 
Dài x Rộng x Cao: 4.560 x 1.780 x 1.445 (mm).
Số tự động (6 cấp) tích hợp lẫy chuyển số 
Điều hòa tự động 2 vùng có lọc khí Ion, có hộc gió cho hàng ghế sau. Hộc giữ lạnh.
Cảm biến hỗ trợ đỗ xe trước sau, Camera lùi (AT).
Nút khởi động & chìa khóa thông minh. Trang bị đèn Welcome (AT).
4 thắng đĩa, hệ thống chống bó cứng phanh ABS.
Tay lái trợ lực điện (MDPS) với 03 chế độ lái. Kết nối Bluetooth (AT).
Hệ thống cân bằng điện tử ESP, hệ thống ổn định thân xe VSM',
                        'price' => '613000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'new',
                        'status' => 'active',
                        'address' => 'Ngã tư sân bay Vinh, Đại lộ Lê-Nin Nghệ An',
                        'lat' => 18.719273999999999,
                        'lng' => 105.67174199999999,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\nkia-cerato-2017-1507797337.jpg","\\r\\n\\r\\n11056-2017-forte-sedan-ex-e1452595124714-1507797340.jpg"]',
                        'created_at' => '2017-10-12 15:36:06',
                        'updated_at' => '2017-10-13 09:09:23',
                        'deleted_at' => NULL,
                        'cate_id' => 76,
                        'user_id' => 20,
                    ),
                    99 => 
                    array (
                        'id' => 100,
                        'title' => 'Xe Ford Everest Titanium 2.2L 4x2 AT 2017',
                        'slug' => 'xe-ford-everest-titanium-2-2l-4x2-at-2017',
                        'des' => 'Một chiếc SUV cỡ lớn đưa bạn đến bất cứ đâu, vượt trội về sức mạnh lẫn khả năng tiết kiệm nhiên liệu, với Everest mọi hành trình đều trở nên nhẹ nhàng và thú vị
Vui lòng liên hệ trực tiếp với Vinh Ford để biết thêm thông tin!',
                        'price' => '1329000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'new',
                        'status' => 'active',
                        'address' => 'Đại lộ Lê Nin, TP Vinh Nghệ An',
                        'lat' => 18.711506,
                        'lng' => 105.677661,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\nford-everest-32-4wd-limited-fordever2e18l-1507797686.jpg","\\r\\n\\r\\n1249163590458-1507797687.jpg","\\r\\n\\r\\n20170719075543-d726-wm-1507797689.jpg","\\r\\n\\r\\ndc7c1275595acd2a5518f3af0371568c-1507797690.jpg","\\r\\n\\r\\nford-everest-tita-22-2017-1507797691.jpg","\\r\\n\\r\\nkia-cerato-2017-1507797693.jpg","\\r\\n\\r\\n1249163493064-copy-1507797695.jpg","\\r\\n\\r\\n2015-ford-everest-10-1507797696.jpg","\\r\\n\\r\\n11056-2017-forte-sedan-ex-e1452595124714-1507797698.jpg"]',
                        'created_at' => '2017-10-12 15:42:12',
                        'updated_at' => '2017-10-13 09:09:20',
                        'deleted_at' => NULL,
                        'cate_id' => 170,
                        'user_id' => 20,
                    ),
                    100 => 
                    array (
                        'id' => 101,
                        'title' => 'Xe Suzuki Super Carry Truck	1.0 MT 2017',
                        'slug' => 'xe-suzuki-super-carry-truck-1-0-mt-2017',
                        'des' => 'Super Carry Truck - dòng xe thương mại 645kg, 02 chỗ ngồi, phun xăng điện tử tiết kiệm nhiên liệu và bảo vệ môi trường; thân xe và khung sườn chắc chắn, loại xe 5 số tới và 1 số lùi.
Hãy đến công ty chúng tôi để lái thử và trải nghiệm dòng xe bạn đang cần.',
                        'price' => '24900000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'new',
                        'status' => 'active',
                        'address' => 'số 05B đường Nguyễn Trãi, P. Quán Bàu, TP Vinh Nghệ An',
                        'lat' => 18.70138,
                        'lng' => 105.67321699999999,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n20170809c48d8e21-d32-56ee-1507797970.jpg","\\r\\n\\r\\n201708018ba3070d-ad6-84b0-1507797971.jpg","\\r\\n\\r\\n2017072063fd9e23-63b-75c0-1507797973.jpg","\\r\\n\\r\\n2017072089adce15-4d6-b492-1507797974.jpg","\\r\\n\\r\\nmaxresdefault-1507797976.jpg"]',
                        'created_at' => '2017-10-12 15:46:40',
                        'updated_at' => '2017-10-13 09:09:17',
                        'deleted_at' => NULL,
                        'cate_id' => 84,
                        'user_id' => 20,
                    ),
                    101 => 
                    array (
                        'id' => 102,
                        'title' => 'Xe Mazda BT 50 2.2L 4x4MT	2017',
                        'slug' => 'xe-mazda-bt-50-2-2l-4x4mt-2017',
                        'des' => 'Mazda BT 50 2.2 MT Facelift 4WD, số sàn, xe nhập khẩu nguyên chiếc. Động cơ diesel MD-CD 2.2L, Turbo I4. Số sàn 6 cấp. Đèn pha tự động, gương kính chỉnh điện. Ghế nỉ cao cấp. Điều hòa chỉnh cơ. Phanh ABS, 2 túi khí trước. Bậc lên xuống, cản trước/sau. Hệ thống âm thanh CD 4 loa, AUX,USB/Ipod. Hệ thống chống trộm....',
                        'price' => '620000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'new',
                        'status' => 'active',
                        'address' => 'Ngã tư sân bay, Đại lộ Lenin, Tp Vinh Nghệ An',
                        'lat' => 18.719273999999999,
                        'lng' => 105.67174199999999,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n8893463-uqjdkwposmnk44fu2norxz-1507798867.jpg","\\r\\n\\r\\nmazda-bt50-2016-tuvanmuaxe-3735-1507798869.jpg","\\r\\n\\r\\n2016-mazda-bt-50-10-1507798871.jpg","\\r\\n\\r\\nmazda-cx-5-2015-facelift-us-verze-39-800-600-1507798873.jpg","\\r\\n\\r\\n1280px-ford-ranger-xlt-25-tdci-doppelkabine-ii-facelift-frontansicht-10-september-2011-dusseldorf-1507798875.jpg","\\r\\n\\r\\nmazda-bt-50-2015-001-1886-1507798878.jpg"]',
                        'created_at' => '2017-10-12 16:01:46',
                        'updated_at' => '2017-10-13 09:09:13',
                        'deleted_at' => NULL,
                        'cate_id' => 70,
                        'user_id' => 20,
                    ),
                    102 => 
                    array (
                        'id' => 103,
                        'title' => 'Xe Mitsubishi Pajero Sport 3.0G 4x2 AT	2017',
                        'slug' => 'xe-mitsubishi-pajero-sport-3-0g-4x2-at-2017',
                        'des' => 'All New Pajero Sport 2017
Nhập khẩu nguyên chiếc từ Thái Lan
•	K/thước tổng thể (D x R x C): 4.785 x 1.815 x 1.805
•	Số chỗ ngồi: 7 chỗ
•	Dung tích xilanh: 2.998
•	Công suất cực đại (ps@rpm): 220/6.250
•	Hộp số: 8AT – Sport Mode
•	Hệ thống dẫn động: 2 cầu Super Select 4WD II
•	Hệ thống an toàn: 7 túi khí; ABS/EBD; cân bằng điện tử & kiểm soát lực kéo(ASTC); khởi hành ngang dốc (HSA); 
•	Trang thiết bị: đèn pha Led; cảm biến đèn pha, gạt mưa; cốp sau đóng mở điện; mâm đúc hợp kim 18”..
Vui lòng liên hệ trực tiếp, cảm ơn!
Gửi tin nhắn cho người bán',
                        'price' => '1199000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'new',
                        'status' => 'active',
                        'address' => 'Đại Lộ Xô Viết Nghệ Tĩnh, TP Vinh Nghệ An',
                        'lat' => 18.715434999999999,
                        'lng' => 105.67395999999999,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\ndbm-1-1507799171.jpg","\\r\\n\\r\\ndbm-1507799173.jpg","\\r\\n\\r\\npajero-sport-4-1507799175.jpg","\\r\\n\\r\\nmitsubishi-pajero-sport-th-0001-1507799176.jpg"]',
                        'created_at' => '2017-10-12 16:06:39',
                        'updated_at' => '2017-10-13 09:09:09',
                        'deleted_at' => NULL,
                        'cate_id' => 79,
                        'user_id' => 20,
                    ),
                    103 => 
                    array (
                        'id' => 104,
                        'title' => 'Xe Hyundai Porter 2008',
                        'slug' => 'xe-hyundai-porter-2008',
                        'des' => 'Bán xe Hyundai Porter II, 1 tấn, SX 2008, nhập khẩu nguyên chiếc, màu xanh, thùng Kín. Xe cứng đẹp, máy chất, nội thất đẹp, điều hòa mát sâu, dàn lốp còn mới đẹp,. Đăng kiểm và phí bảo trì đường bộ đến tháng 3/2018. ảnh thật của xe. Không nhận SMS, kg tiếp thợ, miễn trung gian. thanks',
                        'price' => '202000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Thái Hòa, Nghệ An, Việt Nam',
                        'lat' => 19.293112000000001,
                        'lng' => 105.46539,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\nhyundai-porter-a1244947948b2762902-2-orig-1507799726.jpg","\\r\\n\\r\\nis02457-c126df-1507799730.jpg","\\r\\n\\r\\nhporter2salon1-1507799721.jpg","\\r\\n\\r\\nhyundai-porter-a1244947948b2762902-2-orig-1-1507799724.jpg","\\r\\n\\r\\nhyundai-porter-a1244947948b2762902-7-orig-1507799727.jpg","\\r\\n\\r\\nis02430-7293ca-1507799729.jpg","\\r\\n\\r\\nh100-cabin-2-small-1507799732.jpg"]',
                        'created_at' => '2017-10-12 16:15:59',
                        'updated_at' => '2017-10-13 09:09:06',
                        'deleted_at' => NULL,
                        'cate_id' => 81,
                        'user_id' => 20,
                    ),
                    104 => 
                    array (
                        'id' => 105,
                        'title' => 'Xe Toyota Corona GL 2.0 Trước 1990',
                        'slug' => 'xe-toyota-corona-gl-2-0-truoc-1990-1',
                        'des' => 'Có nhu cầu lên đời nên bán xe cũ. Xe Toyota Corona đời 1988, xe công chức đi hàng ngày, máy êm, điều hòa mát lạnh, gương chỉnh điện, xe được chăm sóc thường xuyên, đài FM,Đầu đĩa, USB đầy đủ, nội thất còn nguyên bản, sườn không mục mọt, phanh, lốp mới thay, xe chạy rất ổn định, rât tiết kiệm xăng, dòng xe nhập khẩu nêm chạy rất đầm 100k/h vẫn êm ru,xe rất phu hợp cho người thu nhập thấp.',
                        'price' => '55000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Nghệ An, Việt Nam',
                        'lat' => 19.234248999999998,
                        'lng' => 104.920036,
                        'view_count' => 1,
                        'images' => '["\\n\\n20170712e5438b64-b49-a7f3-1507858858.jpg","\\n\\n2017062594dd501f-d5f-c2c8-1507858860.jpg","\\n\\n2017051631145a28-6da-1abc-1507858861.jpg","\\r\\n\\r\\n20170625e2d5aa5a-8fc-8d1b-1507859084.jpg","\\r\\n\\r\\n2017062526a9eb0b-069-5878-1507859084.jpg","\\r\\n\\r\\n2017062594dd501f-d5f-c2c8-1507859085.jpg","\\r\\n\\r\\n20170625100aaa7d-e6b-1507859086.jpg","\\r\\n\\r\\n20170625982d900a-c89-a890-1507859086.jpg"]',
                        'created_at' => '2017-10-13 08:41:23',
                        'updated_at' => '2017-10-13 09:09:02',
                        'deleted_at' => NULL,
                        'cate_id' => 74,
                        'user_id' => 20,
                    ),
                    105 => 
                    array (
                        'id' => 106,
                        'title' => 'Xe Mitsubishi Attrage 1.2CVT	2017',
                        'slug' => 'xe-mitsubishi-attrage-1-2cvt-2017',
                        'des' => 'Mitsubishi Attrage dùng động cơ 1.2L MIVEC, công suất 78 mã lực và mô-men xoắn cực đại 100 Nm, đi kèm với hộp số sàn 5 cấp hoặc vô cấp CVT. Dù thông số động cơ không mạnh mẽ lắm, nhưng lại góp phần giúp Attrage đạt mức tiêu thụ nhiên liệu rất thấp, chỉ vào khoảng 4,5 lít/100km. với cụm lưới tản nhiệt mạ chrome tạo cho ngoại hình xe trở nên cuốn hút hơn.Xe có chiều dài 4.245 mm, chiều rộng 1.670 mm, chiều cao 1515 mm và chiều dài cơ sở 2.550 mm....',
                        'price' => '481000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'new',
                        'status' => 'active',
                        'address' => 'Đại Lộ Xô Viết Nghệ Tĩnh, TP Vinh Nghệ An',
                        'lat' => 18.715434999999999,
                        'lng' => 105.67395999999999,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\nmitsuattrage48e13-dailymitsubishi-vn-b8d39e33-1e2a-4206-bf45-693b5d88f5ab-1507859363.jpg","\\r\\n\\r\\n20150427154116-f2a2-1507859364.jpg","\\r\\n\\r\\n1481358435032773712-1507859364.jpg","\\r\\n\\r\\nmitsubishi-attrage-cvt-2017-mau-do-21-600x600-1507859365.jpg"]',
                        'created_at' => '2017-10-13 08:49:51',
                        'updated_at' => '2017-10-13 09:08:59',
                        'deleted_at' => NULL,
                        'cate_id' => 79,
                        'user_id' => 20,
                    ),
                    106 => 
                    array (
                        'id' => 107,
                        'title' => 'Toyota Camry 2.4 màu đen sản xuất 2009 fom 2010',
                        'slug' => 'toyota-camry-2-4-mau-den-san-xuat-2009-fom-2010',
                        'des' => 'Em bán Camry giá chỉ bằng Kia K3 chỉ với 580 triệu đồng bạn đã sở hữu chiếc xe Camry huyền thoại đẳng cấp của Toyota 
🎀🎀🎀
Bán xe Toyota Camry 2.4 màu đen sản xuất 2009 fom 2010 xe còn rất đẹp được trang bị nhiều phụ kiện chính hãng: Ghế da, điều hoà tự động, gương kính chỉnh điện, ghế điện, khoá cửa điều khiển từ xa, túi khí an toàn, đèn sương mù, lazang đúc, hệ thống chống bó cứng phanh ABS, EBD... Vui lòng liên hệ trực tiếp để biết thêm thông tin chi tiết về xe!',
                        'price' => '580000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Thành phố Vinh, Nghệ An, Việt Nam',
                        'lat' => 18.679584999999999,
                        'lng' => 105.681333,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n21762184-1695394500473325-421888909773219557-n-1507859817.jpg","\\r\\n\\r\\n21687561-1695394563806652-7749747020110938493-n-1507859812.jpg","\\r\\n\\r\\n21687911-1695394653806643-8822963494148445735-n-1507859813.jpg","\\r\\n\\r\\n21751467-1695394687139973-7550500655235045469-n-1507859813.jpg","\\r\\n\\r\\n21761713-1695394803806628-2725914068937785924-n-1507859814.jpg","\\r\\n\\r\\n21761836-1695394593806649-8175341674459539675-n-1507859814.jpg","\\r\\n\\r\\n21764730-1695394473806661-522404548387752074-n-1507859815.jpg","\\r\\n\\r\\n21765164-1695394717139970-7549975646015466936-n-1507859816.jpg","\\r\\n\\r\\n21766376-1695394747139967-6424757854100611693-n-1507859816.jpg","\\r\\n\\r\\n21768095-1695394623806646-8698234727186490035-n-1507859817.jpg"]',
                        'created_at' => '2017-10-13 08:57:34',
                        'updated_at' => '2017-10-13 09:08:55',
                        'deleted_at' => NULL,
                        'cate_id' => 74,
                        'user_id' => 20,
                    ),
                    107 => 
                    array (
                        'id' => 108,
                        'title' => 'Xe Peugeot 408 2016',
                        'slug' => 'xe-peugeot-408-2016',
                        'des' => 'Peugeot 408 với thiết kế tinh tế và sang trọng, dòng xe 5 chỗ ngồi thuộc phân khúc C cỡ lớn cho không gian nội thất rộng rãi và tiện nghi. Đặc biệt với hệ thống an toàn đạt chuẩn Châu Âu được đánh giá là chiếc xe an toàn bậc nhất.
- Kiểu động cơ: 2.0 4 xi lanh
- Dài x Rộng x Cao (mm): 4688x1815x1525
- Mâm đúc hợp kim
- Nôi thất được bọc da cao cấp, ghế chỉnh điện.',
                        'price' => '740000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'new',
                        'status' => 'active',
                        'address' => 'Thành phố Vinh, Nghệ An, Việt Nam',
                        'lat' => 18.679584999999999,
                        'lng' => 105.681333,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n21558931-1541126255946305-5202567055014025173-n-1507860162.jpg","\\r\\n\\r\\n21686419-1541126492612948-897529358243386728-n-1507860163.jpg","\\r\\n\\r\\n21687763-1541126872612910-2953520881376245859-n-1507860164.jpg","\\r\\n\\r\\n21687947-1541126795946251-4423008171072025161-n-1507860164.jpg","\\r\\n\\r\\n21761834-1541126615946269-7132347351886887109-n-1507860165.jpg","\\r\\n\\r\\n21762088-1541126385946292-2121972570385986202-n-1507860165.jpg"]',
                        'created_at' => '2017-10-13 09:03:10',
                        'updated_at' => '2017-10-13 09:08:52',
                        'deleted_at' => NULL,
                        'cate_id' => 172,
                        'user_id' => 20,
                    ),
                    108 => 
                    array (
                        'id' => 109,
                        'title' => 'Xe Mazda CX 5 2.0AT 2017',
                        'slug' => 'xe-mazda-cx-5-2-0at-2017',
                        'des' => 'Dài x Rộng x Cao: 4.540mm x 1.840mm x 1.451 mm
- 05 chỗ CUV- AWD bốn bánh chuyển động toàn thời gian- Động cơ xăng 2.0 4-cylinder, - Hộp số tự động AT 6 cấp.
- Tích hợp AM/FM/CD/MP3/AUX/USB, Hệ thống âm thanh 9 loa BOSE
- Thắng đĩa trước, sau ABS, Cân bằng điện tử, Hệ thống chống trượt, Hệ thống cảnh báo áp suất lốp, 06 túi khí.
- Mâm đúc 19 inches, điều hoà tự động 2 hai , Cửa sổ trời.
- Điều khiển âm thanh tích hợp trên vô lăng, Kết nối Bluetooth 
- Camara lùi tích hợp trên màn hình DVD, chìa khóa thông minh start/ stop
Tháng 10 nhiều chương trình khuyến mãi và quà tặng ..
Xem ngay_ xem ngay.',
                        'price' => '799000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'new',
                        'status' => 'active',
                        'address' => 'Ngã tư sân bay thành phố vinh nghệ an',
                        'lat' => 18.719056999999999,
                        'lng' => 105.67201799999999,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n20150623155837-ede0-1507860469.jpg"]',
                        'created_at' => '2017-10-13 09:08:25',
                        'updated_at' => '2017-10-13 09:08:48',
                        'deleted_at' => NULL,
                        'cate_id' => 70,
                        'user_id' => 20,
                    ),
                    109 => 
                    array (
                        'id' => 110,
                        'title' => 'Xe Porsche Macan	2.0 2015',
                        'slug' => 'xe-porsche-macan-2-0-2015',
                        'des' => 'Cần bán porcher macan màu đen, sx năm 2015, xe chạy 15200km, xe chạy như mới. Ai có nhu cầu liên hệ mình.',
                        'price' => '3100000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Nghệ An, Việt Nam',
                        'lat' => 19.234248999999998,
                        'lng' => 104.920036,
                        'view_count' => 1,
                        'images' => '["\\n\\n01-2015-porsche-cayenne-turbo-s-detroit-1-1507861065.jpg","\\n\\nporsche-cayenne-2015-2359-5365-1507861065.jpg","\\n\\n1398585582porsche-macan-2015-19-1507861066.jpg"]',
                        'created_at' => '2017-10-13 09:18:24',
                        'updated_at' => '2017-10-13 10:46:35',
                        'deleted_at' => NULL,
                        'cate_id' => 80,
                        'user_id' => 20,
                    ),
                    110 => 
                    array (
                        'id' => 111,
                        'title' => 'Xe Thaco Ollin 2T4	2017',
                        'slug' => 'xe-thaco-ollin-2t4-2017',
                        'des' => 'Cần bán xe tải thaco 2.4 tấn xe mới mua trong năm 2017 ai có nhu cầu xin lien hệ. Cam ơn đa xem tin!',
                        'price' => '260000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Vinh, Nghệ An, Việt Nam',
                        'lat' => 18.679584999999999,
                        'lng' => 105.681333,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\nuxn1486971930-1507861332.jpg","\\r\\n\\r\\n14405652401279284514-1507861332.jpg","\\r\\n\\r\\n20160925215006-bf74-1507861332.jpg","\\r\\n\\r\\nkdo1497852402-1507861333.jpg","\\r\\n\\r\\nugg1488954723-1507861334.jpg","\\r\\n\\r\\n8604056-thaco-ollin-345-1507861334.jpg"]',
                        'created_at' => '2017-10-13 09:22:42',
                        'updated_at' => '2017-10-13 10:46:33',
                        'deleted_at' => NULL,
                        'cate_id' => 178,
                        'user_id' => 20,
                    ),
                    111 => 
                    array (
                        'id' => 112,
                        'title' => 'Xe Audi Q5 2.0 AT	2015',
                        'slug' => 'xe-audi-q5-2-0-at-2015',
                        'des' => 'Q5 2015 2.0 xe đăng kí năm 2016 , đi được 3 van 4 , xe chưa đâm đụng ngập nước',
                        'price' => '1800000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Nghi Kim, Thành phố Vinh, Nghệ An, Việt Nam',
                        'lat' => 18.724767,
                        'lng' => 105.652911,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n22279725-136732317070954-1151925492559801089-n-1507861583.jpg","\\r\\n\\r\\n22228215-136732330404286-3053574531664401882-n-1507861582.jpg","\\r\\n\\r\\n22221549-136732323737620-5119694391295834857-n-1507861580.jpg","\\r\\n\\r\\n22228159-136732327070953-5002127449030599451-n-1507861581.jpg","\\r\\n\\r\\n22228419-136732363737616-2676913386625118641-n-1507861582.jpg"]',
                        'created_at' => '2017-10-13 09:27:04',
                        'updated_at' => '2017-10-13 10:46:29',
                        'deleted_at' => NULL,
                        'cate_id' => 167,
                        'user_id' => 20,
                    ),
                    112 => 
                    array (
                        'id' => 113,
                        'title' => 'Xe Mazda BT 50 3.2L 4x4 AT	2013',
                        'slug' => 'xe-mazda-bt-50-3-2l-4x4-at-2013',
                        'des' => 'Xe chỉnh chủ ,nhập khẩu nguyên chiếc từ Thái lan 
Xe chạy rất cẩn thận còn nhu mới , nội thất da sang trọng , cảm biển lùi 06 túi khí 
Tóm lại muốn bản để lên đòi 
Xe đc bảo dưỡng định kỳ tại hạng mazda',
                        'price' => '635000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Nghệ An, Việt Nam',
                        'lat' => 19.234248999999998,
                        'lng' => 104.920036,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n20170512110747-5785-wm-1507862365.jpg","\\r\\n\\r\\nmazda-bt50-1-1507862365.jpg","\\r\\n\\r\\n399ad2b1-1507862366.jpg","\\r\\n\\r\\n20170729c67fe4d1-db5-1446-1507862367.jpg","\\r\\n\\r\\nd6179fde-1507862367.jpg"]',
                        'created_at' => '2017-10-13 09:39:51',
                        'updated_at' => '2017-10-13 10:46:26',
                        'deleted_at' => NULL,
                        'cate_id' => 70,
                        'user_id' => 20,
                    ),
                    113 => 
                    array (
                        'id' => 114,
                        'title' => 'Xe Daewoo Nubira	II 1.6 2004',
                        'slug' => 'xe-daewoo-nubira-ii-1-6-2004',
                        'des' => 'Bán Daewoo Nubira màu đen, đời 2004. Xe đẹp, nội ngoại thất còn mới, máy êm.
Vui lòng liên hệ với tôi.',
                        'price' => '108000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Nghệ An, Việt Nam',
                        'lat' => 19.234248999999998,
                        'lng' => 104.920036,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n19577421-125617474704480-555991618725788509-o-1507862660.jpg","\\r\\n\\r\\n19657363-125617478037813-7091833557277354971-n-1507862660.jpg","\\r\\n\\r\\n19693579-125617471371147-447470721509531930-o-1507862661.jpg","\\r\\n\\r\\n19621206-125617488037812-1237849489460177405-o-1507862661.jpg","\\r\\n\\r\\n19667454-125617484704479-4998673045720930218-o-1507862662.jpg"]',
                        'created_at' => '2017-10-13 09:44:46',
                        'updated_at' => '2017-10-13 10:46:24',
                        'deleted_at' => NULL,
                        'cate_id' => 172,
                        'user_id' => 20,
                    ),
                    114 => 
                    array (
                        'id' => 115,
                        'title' => 'Xe Mitsubishi Lancer Gala GLX 1.6AT 2005',
                        'slug' => 'xe-mitsubishi-lancer-gala-glx-1-6at-2005',
                        'des' => 'Bán xe Mitsubishi Gala GLX 1.6 AT đời 2005. Màu đen, số tự động. Sản xuất 2005, 1 chủ từ đầu. Xe đăng kiếm nộp phí đường bộ tới T12/2017. 4 lốp mới thay, xe trang bị 3 màn hình trước và sau ghế. Camera lùi. Xe đi bền bỉ, tiết kiệm nhiên liệu. Đẹp xuất sắc, đảm bảo ko hề có lỗi Đẹp từ A tới Z..
Liên hệ trực tiếp, cảm ơn!',
                        'price' => '235000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Thành phố Vinh, Nghệ An, Việt Nam',
                        'lat' => 18.679584999999999,
                        'lng' => 105.681333,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\nb413cabd3e11c7039d4e8960x540-2b54-1507862901.jpg","\\r\\n\\r\\na4b97aeba6626915aeb51960x540-9036-1507862902.jpg","\\r\\n\\r\\n2014-lancer-01-a5-1507862899.jpg","\\r\\n\\r\\n2495da8511cd59028821b960x540-9a4a-1507862900.jpg","\\r\\n\\r\\nc49e58f2beec23128d04e960x540-4d5e-1507862902.jpg","\\r\\n\\r\\nqxi1449142943-1507862903.jpg","\\r\\n\\r\\nlancer200310-1507862904.jpg"]',
                        'created_at' => '2017-10-13 09:48:52',
                        'updated_at' => '2017-10-13 10:46:17',
                        'deleted_at' => NULL,
                        'cate_id' => 79,
                        'user_id' => 20,
                    ),
                    115 => 
                    array (
                        'id' => 116,
                        'title' => 'Xe Mitsubishi Lancer Gala GLX 1.6AT 2005',
                        'slug' => 'xe-mitsubishi-lancer-gala-glx-1-6at-2005-1',
                        'des' => 'Bán xe Mitsubishi Gala GLX 1.6 AT đời 2005. Màu đen, số tự động. Sản xuất 2005, 1 chủ từ đầu. Xe đăng kiếm nộp phí đường bộ tới T12/2017. 4 lốp mới thay, xe trang bị 3 màn hình trước và sau ghế. Camera lùi. Xe đi bền bỉ, tiết kiệm nhiên liệu. Đẹp xuất sắc, đảm bảo ko hề có lỗi Đẹp từ A tới Z..
Liên hệ trực tiếp, cảm ơn!',
                        'price' => '235000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Thành phố Vinh, Nghệ An, Việt Nam',
                        'lat' => 18.679584999999999,
                        'lng' => 105.681333,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n12243589-515165935323262-4377208609248781843-n-1-1507864038.jpg","\\r\\n\\r\\n12250105-515165981989924-235170509120978198-n-1507864039.jpg","\\r\\n\\r\\n12250161-515166078656581-6687677697300978996-n-1507864040.jpg","\\r\\n\\r\\n12274393-515165921989930-3379448516457590617-n-1-1507864040.jpg","\\r\\n\\r\\n12274393-515165921989930-3379448516457590617-n-1507864041.jpg","\\r\\n\\r\\n12274559-515165948656594-4689258054265393954-n-1507864041.jpg","\\r\\n\\r\\n12274587-515166051989917-1456879786676148331-n-1507864042.jpg","\\r\\n\\r\\n12278928-515165995323256-3747262411434710451-n-1507864043.jpg","\\r\\n\\r\\n12279247-515166035323252-7517729333010997872-n-1507864043.jpg"]',
                        'created_at' => '2017-10-13 10:07:55',
                        'updated_at' => '2017-10-13 10:46:14',
                        'deleted_at' => NULL,
                        'cate_id' => 79,
                        'user_id' => 20,
                    ),
                    116 => 
                    array (
                        'id' => 117,
                        'title' => 'Xe Ssangyong Korando	2005',
                        'slug' => 'xe-ssangyong-korando-2005',
                        'des' => 'Xe đi kỹ, nội thất đẹp, lạnh sâu, xe không ngập nước, không va quẹt. Mua về đi ngay! A e ai có nhu cầu liên hệ nhé.',
                        'price' => '220000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Đường nguyễn sỹ sách thành phố vinh nghệ an',
                        'lat' => 18.686509999999998,
                        'lng' => 105.68732,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n18891510-1380212728714116-8355111937390065744-o-1507864252.jpg","\\r\\n\\r\\n18766673-1380212612047461-1653464578643878540-o-1507864253.jpg","\\r\\n\\r\\n18814826-1380212672047455-5622070297381215480-o-1507864254.jpg","\\r\\n\\r\\n18815452-1380212712047451-9109355502303382640-o-1507864255.jpg","\\r\\n\\r\\n18836712-1380212668714122-832891665746106263-o-1507864255.jpg","\\r\\n\\r\\n18766470-1380212622047460-4162345869152789806-o-1507864256.jpg","\\r\\n\\r\\n18766701-1380212662047456-3706931093758558062-o-1507864257.jpg","\\r\\n\\r\\n18738897-1380212618714127-540526348739161991-o-1507864258.jpg","\\r\\n\\r\\n18814635-1380212615380794-5358215117341050440-o-1507864258.jpg"]',
                        'created_at' => '2017-10-13 10:11:33',
                        'updated_at' => '2017-10-13 10:46:11',
                        'deleted_at' => NULL,
                        'cate_id' => 172,
                        'user_id' => 20,
                    ),
                    117 => 
                    array (
                        'id' => 118,
                        'title' => 'Xe Daewoo Matiz 0.8 MT 1999',
                        'slug' => 'xe-daewoo-matiz-0-8-mt-1999',
                        'des' => 'Bán Daewoo Matiz màu vàng, đời 1999. Xe đẹp, điều hòa mát, đài AM/FM, radio..
Vui lòng liên hệ với tôi.',
                        'price' => '52000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Nghệ An, Việt Nam',
                        'lat' => 19.234248999999998,
                        'lng' => 104.920036,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n19264392-10209906485720041-7909443766525134987-o-1507864635.jpg","\\r\\n\\r\\n19437520-10209906485680040-9223155916880635466-n-1507864635.jpg","\\r\\n\\r\\n19510152-10209906485880045-6356004211592636160-n-1507864636.jpg","\\r\\n\\r\\n19510633-10209906485640039-4904159515178757775-n-1507864636.jpg","\\r\\n\\r\\n19511539-10209906485840044-3902623679911022316-n-1507864637.jpg","\\r\\n\\r\\n19553992-10209906485560037-7634048719570146266-n-1507864637.jpg","\\r\\n\\r\\n19601381-10209906485600038-1059074156825084660-n-1507864638.jpg","\\r\\n\\r\\n19575115-10209906485800043-5385212232635708660-o-1507864638.jpg"]',
                        'created_at' => '2017-10-13 10:17:54',
                        'updated_at' => '2017-10-13 10:46:20',
                        'deleted_at' => NULL,
                        'cate_id' => 171,
                        'user_id' => 20,
                    ),
                    118 => 
                    array (
                        'id' => 119,
                        'title' => 'Xe Toyota Fortuner	2.5G 2009',
                        'slug' => 'xe-toyota-fortuner-2-5g-2009',
                        'des' => 'Tôi cần bán xe 7 chỗ Fortuner màu xám, sản xuất 2009. Xe nhà sử dụng nên cực giữ gìn. Xe trang bị đầy đủ hệ thống điều hòa, thắng an toàn, hệ thống âm thanh giải trí, gương chỉnh điện, bệ bước...
Ai có nhu cầu vui lòng liên hệ miễn qua trung gian.
Cảm ơn đã xem tin.',
                        'price' => '6100000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Quỳnh châu quỳnh lưu nghệ an Nghệ An',
                        'lat' => 19.215582999999999,
                        'lng' => 105.559162,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n3467da8b19699381017fd1000x746-5fdb-1507865157.jpg","\\r\\n\\r\\nban-xe-toyota-fortuner-doi-2015-mau-bac-889-1469166687-5791b45fe278b-1507865158.jpg","\\r\\n\\r\\nfortuner-25g-2009-1507865158.jpg","\\r\\n\\r\\nfortuner-2009-1507865159.jpg"]',
                        'created_at' => '2017-10-13 10:26:22',
                        'updated_at' => '2017-10-13 10:46:05',
                        'deleted_at' => NULL,
                        'cate_id' => 74,
                        'user_id' => 20,
                    ),
                    119 => 
                    array (
                        'id' => 120,
                        'title' => 'Xe Kia Morning Van 1.0 AT 2009',
                        'slug' => 'xe-kia-morning-van-1-0-at-2009',
                        'des' => 'Cần bán xe CB công an nữ đi ít, giữ gìn , không đâm đụng, biển số đẹp, xe đi kinh tế và thuận tiện...
Liên hệ để biết thêm thông tin.',
                        'price' => '195000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Thành phố Vinh, Nghệ An, Việt Nam',
                        'lat' => 18.679584999999999,
                        'lng' => 105.681333,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n39151-1507865562.jpg","\\r\\n\\r\\n46181-1507865563.jpg","\\r\\n\\r\\n62622-1507865564.jpg","\\r\\n\\r\\n89599-1507865564.jpg","\\r\\n\\r\\ndscn9449-1507865565.jpg"]',
                        'created_at' => '2017-10-13 10:33:11',
                        'updated_at' => '2017-10-13 10:46:02',
                        'deleted_at' => NULL,
                        'cate_id' => 76,
                        'user_id' => 20,
                    ),
                    120 => 
                    array (
                        'id' => 121,
                        'title' => 'Xe Daewoo Lacetti	SE 2009',
                        'slug' => 'xe-daewoo-lacetti-se-2009',
                        'des' => 'Cẩn bán gấp xe Lacetti nhập khẩu sản xuất năm 2009 màu đen số sàn. Cam kết xe đẹp, chất. Đăng kí một chủ từ mới. Ai có nhu cầu xin liên hệ',
                        'price' => '308000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Nghệ An, Việt Nam',
                        'lat' => 19.234248999999998,
                        'lng' => 104.920036,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n15327296-1182796005137079-5870908699763275440-n-1507865821.jpg","\\r\\n\\r\\n15401108-1182796065137073-3701572319505990775-n-1507865820.jpg","\\r\\n\\r\\n15253397-1182796055137074-6128322475827033441-n-1507865821.jpg","\\r\\n\\r\\n15337499-1182796018470411-3045720357584496605-n-1507865822.jpg","\\r\\n\\r\\n15390971-1182796028470410-3650945524225080227-n-1507865822.jpg"]',
                        'created_at' => '2017-10-13 10:37:22',
                        'updated_at' => '2017-10-13 10:46:08',
                        'deleted_at' => NULL,
                        'cate_id' => 171,
                        'user_id' => 20,
                    ),
                    121 => 
                    array (
                        'id' => 122,
                        'title' => 'Xe Kia Cerato 1.6 MT 2008',
                        'slug' => 'xe-kia-cerato-1-6-mt-2008',
                        'des' => 'Bán Kia Cerato màu trắng, đời 2008. Xe nhập khẩu, số sàn, tay lái trợ lực, điều hòa, đài AM/FM, radio, mâm đúc..
Vui lòng liên hệ với toi.',
                        'price' => '198000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Nghệ An, Việt Nam',
                        'lat' => 19.234248999999998,
                        'lng' => 104.920036,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n687721023173753-1507866098.jpg","\\r\\n\\r\\ncho-thue-kia-cerato-tu-lai-2-1507866098.jpg","\\r\\n\\r\\nkia-cerato-mau-trang-5-14-1507866099.jpg","\\r\\n\\r\\n2017-kia-cerato-australia-03-s-premium-02-1-0-1507866099.jpg","\\r\\n\\r\\n20160523143332-9ce0-wm-1507866100.jpg","\\r\\n\\r\\n20160523143410-7854-wm-1507866100.jpg"]',
                        'created_at' => '2017-10-13 10:42:01',
                        'updated_at' => '2017-10-13 10:45:59',
                        'deleted_at' => NULL,
                        'cate_id' => 76,
                        'user_id' => 20,
                    ),
                    122 => 
                    array (
                        'id' => 123,
                        'title' => 'test dữ liệu nhập',
                        'slug' => 'test-du-lieu-nhap',
                        'des' => 'ạn có biết chúng tôi MIỄN PHÍ khi chỉnh sửa bài đăng của bạn trong suốt thời gian bài đăng còn tồn tại trên hệ thống.',
                        'price' => '2345',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'deactive',
                        'address' => 'Vĩnh Yên, Vĩnh Phúc, Việt Nam',
                        'lat' => 21.297325999999998,
                        'lng' => 105.606066,
                        'view_count' => 1,
                        'images' => NULL,
                        'created_at' => '2017-10-13 10:43:25',
                        'updated_at' => '2017-10-13 10:45:50',
                        'deleted_at' => '2017-10-13 10:45:50',
                        'cate_id' => 76,
                        'user_id' => 20,
                    ),
                    123 => 
                    array (
                        'id' => 124,
                        'title' => 'Xe Hyundai Libero 2005',
                        'slug' => 'xe-hyundai-libero-2005',
                        'des' => 'Cần bán xe tải huyndai libero đời 2005. Xe gia đình đang sử dụng, chất lượng miễn lo, điều hòa mát lanh, điểu khiển kính, khóa và các hệ thống báo nhiên liệu,nước đều tự động.máy con zin.thùng bọc inoc.xe chuyên chở hang nhẹ nên rất ok.
Nay gia đình đang cần bán. Ai có nhu cầu alo. Chỉ bán cho người có nhu cầu sử dụng, miênc cò lái. Xe hiện đang ở Vinh. Ai mua có thể lái thử thoải mái.',
                        'price' => '235000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Thành phố Vinh, Nghệ An, Việt Nam',
                        'lat' => 18.679584999999999,
                        'lng' => 105.681333,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n201709125b385457-8d4-e7a6-1507866688.jpg","\\r\\n\\r\\n201709126fc62995-b41-037e-1507866688.jpg","\\r\\n\\r\\n201709124b6e6cc0-8b9-77b5-1507866686.jpg","\\r\\n\\r\\n20170912229d905b-b4b-7238-1507866689.jpg","\\r\\n\\r\\n20170912692ac5ed-320-7e43-1507866689.jpg"]',
                        'created_at' => '2017-10-13 10:51:56',
                        'updated_at' => '2017-10-13 11:13:16',
                        'deleted_at' => NULL,
                        'cate_id' => 81,
                        'user_id' => 20,
                    ),
                    124 => 
                    array (
                        'id' => 125,
                        'title' => 'Xe Ford Everest 2.5L 4x2 MT	2010',
                        'slug' => 'xe-ford-everest-2-5l-4x2-mt-2010',
                        'des' => 'bán fo evert đời cuối 2010 màu bạc xe chạy 11vạn rất đẹp nguyên bản kg kinh doanh hay dịch vụ chạy khách máy dầu số sàn lốp 5 quả 90% xe đã lắp rất nhiều đồ chơi vậy ae ai thiện chí alo nhé.',
                        'price' => '505000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Nghệ An, Việt Nam',
                        'lat' => 19.234248999999998,
                        'lng' => 104.920036,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\nkhahoang-img-4941-6236-5352-1507866971.jpg","\\r\\n\\r\\ncars-15900-ford-everest-2005-15900-7-1507866969.jpg","\\r\\n\\r\\nxbe1465266079-1507866967.jpg","\\r\\n\\r\\n292550858-1-644x461-ford-everest-2009-matic-jakarta-utara-1507866968.jpg","\\r\\n\\r\\nluot-gio-nap-capo-1507866969.jpg","\\r\\n\\r\\nos-1499394250131-1507866970.jpg","\\r\\n\\r\\nos-1499394252165-1507866970.jpg"]',
                        'created_at' => '2017-10-13 10:56:38',
                        'updated_at' => '2017-10-13 11:13:19',
                        'deleted_at' => NULL,
                        'cate_id' => 170,
                        'user_id' => 20,
                    ),
                    125 => 
                    array (
                        'id' => 126,
                        'title' => 'Xe Mitsubishi Jolie	MB 2002',
                        'slug' => 'xe-mitsubishi-jolie-mb-2002',
                        'des' => 'Bán xe Jolie màu xanh, đời 2002. Xe trang bị: màn hình, điều hòa, đài AM/Fm, máy êm.
Vui lòng liên hệ với tôi.',
                        'price' => '135000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Nghệ An, Việt Nam',
                        'lat' => 19.234248999999998,
                        'lng' => 104.920036,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n3524b95dbc80f4img-1507867247.jpg","\\r\\n\\r\\nmitsubishi-jolie-03-1507867247.jpg","\\r\\n\\r\\nmua-ban-o-to-cu-ha-noi-jolie-1507867248.jpg"]',
                        'created_at' => '2017-10-13 11:01:26',
                        'updated_at' => '2017-10-13 11:13:22',
                        'deleted_at' => NULL,
                        'cate_id' => 79,
                        'user_id' => 20,
                    ),
                    126 => 
                    array (
                        'id' => 127,
                        'title' => 'Xe Ford Transit Standard MID 2016',
                        'slug' => 'xe-ford-transit-standard-mid-2016',
                        'des' => 'Bán Ford Transit màu trắng, đời 2016. Xe trang bị: ghế da, đài AM/FM, radio, dàn lạnh..
Vui lòng liên hệ với tôi',
                        'price' => '677000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Nghệ An, Việt Nam',
                        'lat' => 19.234248999999998,
                        'lng' => 104.920036,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n20638201-341420429649382-8659155360778803699-n-1507867678.jpg","\\r\\n\\r\\n20637950-341420439649381-7134085025419122602-n-1507867678.jpg","\\r\\n\\r\\n20727832-341420432982715-7378004706031545460-n-1507867679.jpg","\\r\\n\\r\\n21369299-1438700372912126-5714412412359157211-n-1507867679.jpg","\\r\\n\\r\\n21432694-1438700369578793-7724534819340976556-n-1507867680.jpg"]',
                        'created_at' => '2017-10-13 11:08:27',
                        'updated_at' => '2017-10-13 11:13:25',
                        'deleted_at' => NULL,
                        'cate_id' => 170,
                        'user_id' => 20,
                    ),
                    127 => 
                    array (
                        'id' => 128,
                        'title' => 'Xe Renault Duster 2017',
                        'slug' => 'xe-renault-duster-2017',
                        'des' => 'Duster la mâu xe SUV kết hợp hài hoa giưa khả năng vận hanh trên mọi dịa hình. Công nghệ An toàn tôui ưu, thiết kế rộng rãi, tiện nghi va cá tính. Trang bị an toàn trên xe như: ABS EDA EPS HSA Cruise control, 4 túi khí, cảm biến lùi, Chế độ Eco và nhiêu tính năng khác hấp dân nưa. Hay liên hệ ngay để biêt thêm thông tin chi tiết: Renualt Vinh',
                        'price' => '739000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'new',
                        'status' => 'active',
                        'address' => 'Yên Thành, Nghệ An, Việt Nam',
                        'lat' => 19.051129,
                        'lng' => 105.453672,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\nrenault-duster-performance-1-1-1507867928.jpg","\\r\\n\\r\\n2017-renault-duster-interior-1507867925.jpg","\\r\\n\\r\\nduster-oroch-8-1507867926.jpg","\\r\\n\\r\\nrenault-duster-2017-1-1507867926.jpg","\\r\\n\\r\\nrenault-duster-2017-2-1507867927.jpg","\\r\\n\\r\\nrenault-duster-2017-6-1507867927.jpg"]',
                        'created_at' => '2017-10-13 11:12:44',
                        'updated_at' => '2017-10-13 11:13:13',
                        'deleted_at' => NULL,
                        'cate_id' => 172,
                        'user_id' => 20,
                    ),
                    128 => 
                    array (
                        'id' => 129,
                        'title' => 'Xe Ford Everest 2.5L 4x2 MT	2015',
                        'slug' => 'xe-ford-everest-2-5l-4x2-mt-2015',
                        'des' => 'Rao hộ ông anh bán xe lên đời. 
Ford Everest đời 2015 màu phấn hồng. 
số sàn 1 cầu, máy dầu. 
Chạy hơn 3 vạn xíu. 
Nguyên xi tới từng con ốc',
                        'price' => '700000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Đô Lương, Nghệ An, Việt Nam',
                        'lat' => 18.899097999999999,
                        'lng' => 105.336536,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\nban-xe-ford-everest-cu-doi-2015-so-san-mau-phan-hong-1496067698-592c2e726d6cd-1507868955.jpg","\\r\\n\\r\\n20150403094647-2a61-1507868954.jpg","\\r\\n\\r\\nford-everest-2-5l-4-2-mt-manh-me-tren-tung-chang-duong-2-c85bp6mrvs-1507868953.jpg","\\r\\n\\r\\nnoi-that-everest-limited-at-2-1507868954.jpg","\\r\\n\\r\\nford-everest-limited-42-at-fordvinhphuc-7-1507868955.jpg"]',
                        'created_at' => '2017-10-13 11:29:45',
                        'updated_at' => '2017-10-13 11:56:24',
                        'deleted_at' => NULL,
                        'cate_id' => 170,
                        'user_id' => 20,
                    ),
                    129 => 
                    array (
                        'id' => 130,
                        'title' => 'Xe Honda Civic 1.8 AT 2008',
                        'slug' => 'xe-honda-civic-1-8-at-2008',
                    'des' => 'Cần bán xe Honda Civic 1.8 AT, xe còn nguyên bản,đẹp, xe đi ít chỉ đi làm từ nhà đến cơ quan. Xe tên Công ty xuất hóa đơn. Ai cần liên hệ trực tiếp xem xe tại nhà riêng ngoài giờ hành chính (trong giờ hành chính xem xe tại cơ quan)',
                        'price' => '358000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'K14, Đường Lê Lợi, Thành phố Vinh Nghệ An',
                        'lat' => 18.681626999999999,
                        'lng' => 105.674712,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n41cbec56117c630e2a3d0be6cbb6ce94-1507870026.jpg","\\r\\n\\r\\n75173491-162982-5-1-civic-1507870027.jpg","\\r\\n\\r\\nimages-1507870027.jpg","\\r\\n\\r\\nnormal-2007-honda-civic-1-8-at-18052015095539230-jpg-1507870028.jpg","\\r\\n\\r\\nnormal-2008-honda-civic-1-8-at-30102015021636230-1507870029.jpg","\\r\\n\\r\\n15906-1-1507870029.jpg","\\r\\n\\r\\n15906-1507870030.jpg"]',
                        'created_at' => '2017-10-13 11:47:36',
                        'updated_at' => '2017-10-13 11:56:22',
                        'deleted_at' => NULL,
                        'cate_id' => 168,
                        'user_id' => 20,
                    ),
                    130 => 
                    array (
                        'id' => 131,
                        'title' => 'Xe Toyota Vios Limo 2007',
                        'slug' => 'xe-toyota-vios-limo-2007',
                        'des' => 'Gia đình gv bán xe để đổi số tự động! Xe rất đẹp. Máy êm ru côn số nhẹ như xe mới. Vì xe ko đi dịch vụ hay taxi nên rất ngon ai mua đến xem xe trực tiếp để biết',
                        'price' => '200000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Hợp Thành, Yên Thành, Nghệ An, Việt Nam',
                        'lat' => 19.006146000000001,
                        'lng' => 105.506409,
                        'view_count' => 2,
                        'images' => '["\\r\\n\\r\\n17799100-1709397579086797-4802792146260139773-n-1507870309.jpg","\\r\\n\\r\\n17759893-1709397855753436-7361279572854256296-n-1507870307.jpg","\\r\\n\\r\\n17795862-1709397832420105-7819946431919194433-n-1507870308.jpg","\\r\\n\\r\\n17795896-1709397829086772-83504814298388204-n-1507870308.jpg","\\r\\n\\r\\n17796543-1709397582420130-8925904958112105370-n-1507870309.jpg"]',
                        'created_at' => '2017-10-13 11:52:22',
                        'updated_at' => '2017-10-13 13:16:07',
                        'deleted_at' => NULL,
                        'cate_id' => 74,
                        'user_id' => 20,
                    ),
                    131 => 
                    array (
                        'id' => 132,
                        'title' => 'Xe Peugeot 208 2015',
                        'slug' => 'xe-peugeot-208-2015',
                        'des' => '-VẬN HÀNH: Nhờ thiết kế tinh tế, trực quan và công nghệ hiện đại, Peugeot 208 mang lại những trải nghiệm lái tuyệt vời. 
- VỊ TRÍ LÁI TRỰC QUAN: Vị trí của người lái trong xe Peugeot 208 được thiết kế theo phong cách hiện đại, mang đến cảm giác lái tuyệt vời và tự nhiên. Vô lăng được làm nhỏ lại giúp người lái kiểm soát và điều khiển xe tốt hơn. Màn hình hiển thị nhô lên giúp tầm nhìn tốt hơn và dễ dàng hơn.
- ĐỘNG CƠ MẠNH MẼ
Peugeot 208 sử dụng động cơ máy xăng 1.6L, động cơ đã có 8 năm liền đạt giải “Động cơ của năm" phân khúc động cơ xăng 1,4L-1,8L. Đây là mẫu xe hatchback đến từ châu Âu.',
                        'price' => '850000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'new',
                        'status' => 'active',
                        'address' => 'Thành phố Vinh, Nghệ An, Việt Nam',
                        'lat' => 18.679584999999999,
                        'lng' => 105.681333,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\nimg-2542-ff9ec-1507875612.jpg","\\r\\n\\r\\n741639bc3b086764d5ebd960x640-20f9-1507875612.jpg","\\r\\n\\r\\n54ae9a8aa2e07a5f9e56c960x640-066d-1507875609.jpg","\\r\\n\\r\\n94746bc7a227de95763d5666x384-fc0d-1507875611.jpg","\\r\\n\\r\\npeugeot-208-2015-ho-chi-minh-1371481463376381-1507875613.jpg"]',
                        'created_at' => '2017-10-13 13:20:46',
                        'updated_at' => '2017-10-13 15:01:16',
                        'deleted_at' => NULL,
                        'cate_id' => 172,
                        'user_id' => 20,
                    ),
                    132 => 
                    array (
                        'id' => 133,
                        'title' => 'Xe Toyota Zace GL	2005',
                        'slug' => 'xe-toyota-zace-gl-2005',
                        'des' => 'Bán Toyota Zace GL màu xanh, đời 2005. Xe đang hoạt động bình thường, màn hình DVD, điều hòa, mâm đúc, bệ bước..
Vui lòng liên hệ với tôi.',
                        'price' => '250000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Nghệ An, Việt Nam',
                        'lat' => 19.234248999999998,
                        'lng' => 104.920036,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n3-0745-1507875918.jpg","\\r\\n\\r\\n4dqxtoi-can-ban-xe-8-cho-toyota-zace-gl-42005-chay-dung-58000km-1507875916.jpg","\\r\\n\\r\\nmedia-dd-10-normal-2005-toyota-zace-gl-01042015120708-1507875917.jpg","\\r\\n\\r\\ntoyota-zace-216-1507875917.jpg","\\r\\n\\r\\n142325-2-1507875918.jpg","\\r\\n\\r\\n2095407-a5f7d48e8e13764f0f1c2b1d06d26ad9-1507875919.jpg"]',
                        'created_at' => '2017-10-13 13:25:42',
                        'updated_at' => '2017-10-13 15:01:19',
                        'deleted_at' => NULL,
                        'cate_id' => 74,
                        'user_id' => 20,
                    ),
                    133 => 
                    array (
                        'id' => 134,
                        'title' => 'Xe Isuzu NQR 2016',
                        'slug' => 'xe-isuzu-nqr-2016',
                        'des' => 'Xe Isuzu NQR 5,5tấn, xe còn khá mới , chính chủ chưa va chạm ngập nước , bảo quản xe tốt',
                        'price' => '660000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Khối Châu Hưng, Phường Vinh Tân, Thành phố vinh, Nghệ An',
                        'lat' => 18.661822000000001,
                        'lng' => 105.669263,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n20160618140645-0cd7-1507876132.jpg","\\r\\n\\r\\n7451cadae9467cfc4abd9800x600-93db-1507876132.jpg","\\r\\n\\r\\nxe-tai-moi-isuzu-1507876133.jpg"]',
                        'created_at' => '2017-10-13 13:29:39',
                        'updated_at' => '2017-10-13 15:01:22',
                        'deleted_at' => NULL,
                        'cate_id' => 82,
                        'user_id' => 20,
                    ),
                    134 => 
                    array (
                        'id' => 135,
                        'title' => 'Xe Mazda 323 1.6 MT 1997',
                        'slug' => 'xe-mazda-323-1-6-mt-1997',
                        'des' => 'Bán Mazda 323 màu trắng, đời 1997. Xe đẹp, có điều hòa, đài AM/FM, radio, mâm đúc..
Vui lòng liên hệ với tôi.',
                        'price' => '38000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Nghệ An, Việt Nam',
                        'lat' => 19.234248999999998,
                        'lng' => 104.920036,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n201511021148580501wm-0ead-1507876396.jpg","\\r\\n\\r\\n14ceb87a7212bb0e22da2640x479-106d-1507876394.jpg","\\r\\n\\r\\n14b89a33ded97be6b3f66640x480-5f1f-1507876394.jpg","\\r\\n\\r\\n440c297a342cb418ad5ec640x480-2590-1507876395.jpg","\\r\\n\\r\\n20160131200137f66awm-2451-1507876395.jpg"]',
                        'created_at' => '2017-10-13 13:33:38',
                        'updated_at' => '2017-10-13 15:01:25',
                        'deleted_at' => NULL,
                        'cate_id' => 70,
                        'user_id' => 20,
                    ),
                    135 => 
                    array (
                        'id' => 136,
                        'title' => 'Xe Toyota Innova 2.0MT 2007',
                        'slug' => 'xe-toyota-innova-2-0mt-2007',
                        'des' => 'Gia đình cần bán xe inova đời 2007 ,xe chính chủ không kinh doanh dịch vụ,nói chung là xe đẹp ai cần liên hệ xin cảm ơn',
                        'price' => '355000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Nghệ An, Việt Nam',
                        'lat' => 19.234248999999998,
                        'lng' => 104.920036,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n218-toyota-innova-cu-2007-1507876613.jpg","\\r\\n\\r\\n2089-innova-09-a1-462x300-1507876614.jpg","\\r\\n\\r\\nmaxresdefault-1507876614.jpg","\\r\\n\\r\\ninnova-1507876615.jpg","\\r\\n\\r\\nd280c560-a-1507876615.jpg"]',
                        'created_at' => '2017-10-13 13:37:23',
                        'updated_at' => '2017-10-13 15:01:29',
                        'deleted_at' => NULL,
                        'cate_id' => 74,
                        'user_id' => 20,
                    ),
                    136 => 
                    array (
                        'id' => 137,
                        'title' => 'Xe Toyota Corolla altis 1.8G MT 2001',
                        'slug' => 'xe-toyota-corolla-altis-1-8g-mt-2001',
                        'des' => 'Bán Toyota Corolla Altis số sàn, đời 2001. Xe Màu mận, 4 thắng đĩa, Điều hòa mát lạnh, tay lái trợ lực, mâm đúc..
Vui lòng liên hệ với tôi.',
                        'price' => '255000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Nghệ An, Việt Nam',
                        'lat' => 19.234248999999998,
                        'lng' => 104.920036,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n20170419-083913-1507876858.jpg","\\r\\n\\r\\ntoyota-altis-mau-nau-7-4-1507876857.jpg","\\r\\n\\r\\ntoyota-altis-2017-1507876858.jpg","\\r\\n\\r\\nnoi-that-corolla-altis-18mt-1507876859.jpg","\\r\\n\\r\\nnoi-that-toyota-corolla-altis-2016-20-va-1-8-gia-1507876859.jpg"]',
                        'created_at' => '2017-10-13 13:41:26',
                        'updated_at' => '2017-10-13 15:01:32',
                        'deleted_at' => NULL,
                        'cate_id' => 74,
                        'user_id' => 20,
                    ),
                    137 => 
                    array (
                        'id' => 138,
                        'title' => 'Xe Hyundai i10 Grand 1.0 AT	2016',
                        'slug' => 'xe-hyundai-i10-grand-1-0-at-2016',
                        'des' => 'cần thay đổi xe, đã đặt mua xe mới cần bán xe đã qua sử dụng, chưa ngập nước, xe mới do nữ dùng, công tác gần nên xe còn mới đảm bảo 100%',
                        'price' => '380000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Nghệ An, Việt Nam',
                        'lat' => 19.234248999999998,
                        'lng' => 104.920036,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n20150813165559-824d-wm-1507877095.jpg","\\r\\n\\r\\ngrand-i10-1-0-a-t-2015-2016-1-1507877095.jpg","\\r\\n\\r\\nthumbnail-1507877096.jpg","\\r\\n\\r\\nhyundai-i10-14-1507877096.jpg"]',
                        'created_at' => '2017-10-13 13:45:13',
                        'updated_at' => '2017-10-13 15:01:35',
                        'deleted_at' => NULL,
                        'cate_id' => 72,
                        'user_id' => 20,
                    ),
                    138 => 
                    array (
                        'id' => 139,
                        'title' => 'Xe Hyundai i10 Grand 1.0 AT 2016',
                        'slug' => 'xe-hyundai-i10-grand-1-0-at-2016-1',
                        'des' => 'cần thay đổi xe, đã đặt mua xe mới cần bán xe đã qua sử dụng, chưa ngập nước, xe mới do nữ dùng, công tác gần nên xe còn mới đảm bảo 100%',
                        'price' => '380000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Thành phố vinh Nghệ an',
                        'lat' => 18.679584999999999,
                        'lng' => 105.681333,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n20150813165559-824d-wm-1507879648.jpg","\\r\\n\\r\\nhyundai-i10-14-1507879649.jpg","\\r\\n\\r\\nthumbnail-1507879650.jpg","\\r\\n\\r\\ngrand-i10-1-0-a-t-2015-2016-1-1507879650.jpg"]',
                        'created_at' => '2017-10-13 14:27:56',
                        'updated_at' => '2017-10-13 15:01:38',
                        'deleted_at' => NULL,
                        'cate_id' => 72,
                        'user_id' => 20,
                    ),
                    139 => 
                    array (
                        'id' => 140,
                        'title' => 'Xe Nissan Sunny XV 2015',
                        'slug' => 'xe-nissan-sunny-xv-2015',
                        'des' => 'Cần bán xe Nissan Sunny màu trắng, số tự động, sx năm 2015. Tôi có nhu cầu đổi xe, ai có nhu cầu liên hệ với tôi 
xe qua sử dụng một chủ đang còn mới, màu trắng số km đã đi là 22000km.',
                        'price' => '450000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Quang Tiến - Thị Xã Thái Hoà - Nghệ An Nghệ An',
                        'lat' => 19.327604000000001,
                        'lng' => 105.40095100000001,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\nban-xe-nissan-sunny-xv-784-1462889985-5731ee01afdb1-1507880171.jpg","\\r\\n\\r\\n1442644438677829319-1507880170.jpg","\\r\\n\\r\\nnissan-sunny-impul-body-kit-1507880170.jpg","\\r\\n\\r\\n20150613153838-f95e-1507880171.jpg","\\r\\n\\r\\nmedia-4f-58-normal-2015-nissan-sunny-xv-15-09032015141830-1507880172.jpg"]',
                        'created_at' => '2017-10-13 14:36:45',
                        'updated_at' => '2017-10-13 15:01:41',
                        'deleted_at' => NULL,
                        'cate_id' => 73,
                        'user_id' => 20,
                    ),
                    140 => 
                    array (
                        'id' => 141,
                        'title' => 'Xe Nissan Navara XE 2.5AT 2013',
                        'slug' => 'xe-nissan-navara-xe-2-5at-2013',
                        'des' => 'Em bán xe Navara đời 2013. Xe chính chủ đứng bán. Tình trạng xe còn rất mới. Mới đăng kiểm và thay dàn lốp mới. Tình trạng xe còn rất hoàn hảo. Xem xe trực tiếp trong giờ hành chính. Miễn tiếp trung gian. Bao kiểm tra các kiểu',
                        'price' => '445000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Thành phố Vinh, Nghệ An, Việt Nam',
                        'lat' => 18.679584999999999,
                        'lng' => 105.681333,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n20150518094833-95ac-1507880417.jpg","\\r\\n\\r\\n33809-a-1507880416.jpg","\\r\\n\\r\\n20150518094902-1e68-1507880417.jpg","\\r\\n\\r\\n59d081a9bc0e6-1507880418.jpg"]',
                        'created_at' => '2017-10-13 14:40:51',
                        'updated_at' => '2017-10-13 15:01:44',
                        'deleted_at' => NULL,
                        'cate_id' => 73,
                        'user_id' => 20,
                    ),
                    141 => 
                    array (
                        'id' => 142,
                        'title' => 'Xe Lexus IS 250C 2012',
                        'slug' => 'xe-lexus-is-250c-2012',
                        'des' => 'Xe chỉ để đi lại loanh quanh nên còn rất mới. Lexus IS 250C màu trắng sở hữu phong cách thiết kế ngoại thất mới mẻ với những nét kết hợp độc đáo giữa hai dòng xe mui trần và coupe. Kiểu dáng thiết kế mang tính khí động học kéo dài toàn bộ thân xe, đi kèm với tổ hợp tấm chắn gầm xe giúp giảm thiểu hệ số cản xuống mức tốt nhất cho xe là 0.29 Cd, đồng thời hạn chế tối đa sự hỗn loạn gió trong khoang lái. Nhờ vậy, khi mở mui, người lái và hành khách vẫn cảm thấy dễ chịu.',
                        'price' => '1560000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Quỳ Hợp, Nghệ An, Việt Nam',
                        'lat' => 19.352892000000001,
                        'lng' => 105.17268199999999,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n754f288fc65d91img-1507880730.jpg","\\r\\n\\r\\n3sieu-xe152-1507880729.jpg","\\r\\n\\r\\nlexus-is250-fsport-01-1507880729.jpg"]',
                        'created_at' => '2017-10-13 14:45:58',
                        'updated_at' => '2017-10-13 15:01:48',
                        'deleted_at' => NULL,
                        'cate_id' => 77,
                        'user_id' => 20,
                    ),
                    142 => 
                    array (
                        'id' => 143,
                        'title' => 'Xe Daihatsu Charade 2007',
                        'slug' => 'xe-daihatsu-charade-2007',
                        'des' => 'Cần bán xe ô tô Daihatsu Charade đời 2007, nhập khẩu từ Nhật, số tự động, màu ghi vàng, có camera lùi, màn hình DVD siêu nét, loa âm thanh tốt, điều hòa mát lạnh.
Liên hệ để biết thêm thông tin.',
                        'price' => '235000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Số 56,đường Ngô Sĩ Liên thành phố vinh Nghệ An',
                        'lat' => 18.667435999999999,
                        'lng' => 105.69153900000001,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n20170621b778174f-e30-c9b5-1507881055.jpg","\\r\\n\\r\\n20170621cafc5fb2-ceb-c209-1507881055.jpg","\\r\\n\\r\\n20170621d75352df-545-9e55-1507881056.jpg","\\r\\n\\r\\n20170528192506-d829-1507881056.jpg"]',
                        'created_at' => '2017-10-13 14:51:36',
                        'updated_at' => '2017-10-13 15:01:54',
                        'deleted_at' => NULL,
                        'cate_id' => 87,
                        'user_id' => 20,
                    ),
                    143 => 
                    array (
                        'id' => 144,
                        'title' => 'Xe Hyundai County	2009',
                        'slug' => 'xe-hyundai-county-2009',
                        'des' => 'Cần bán xe ô tô Daihatsu Charade đời 2007, nhập khẩu từ Nhật, số tự động, màu ghi vàng, có camera lùi, màn hình DVD siêu nét, loa âm thanh tốt, điều hòa mát lạnh.
Liên hệ để biết thêm thông tin.',
                        'price' => '480000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Hợp Thành, Yên Thành, Nghệ An, Việt Nam',
                        'lat' => 19.006146000000001,
                        'lng' => 105.506409,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n2009-diesel-ban-xe-c-hyundai-county-i-2009-chinh-ch-gia-515tr-5510036506794607661-1507881376.jpg","\\r\\n\\r\\nnoi-that-xe-county-1507881376.jpg","\\r\\n\\r\\nhuyndai-county29-1507881377.jpg","\\r\\n\\r\\nhyundai-county-deluxe-2009-15591261658-1507881377.jpg"]',
                        'created_at' => '2017-10-13 14:56:47',
                        'updated_at' => '2017-10-13 15:01:51',
                        'deleted_at' => NULL,
                        'cate_id' => 98,
                        'user_id' => 20,
                    ),
                    144 => 
                    array (
                        'id' => 145,
                        'title' => 'Xe Hyundai County 2011',
                        'slug' => 'xe-hyundai-county-2011',
                        'des' => 'Gia dinh chung tôi cần bán xe county hyundai sx 2011 29 chỗ ngồi xe chuyên chạy hợp đồng du lịch. Xe giữ gìn tốt,xe còn như mới. Nay không có nhu cầu sử dụng ai có nhu cầu xin liên hệ để biết thêm chi tiết.',
                        'price' => '478000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Nghệ An, Việt Nam',
                        'lat' => 19.234248999999998,
                        'lng' => 104.920036,
                        'view_count' => 1,
                        'images' => NULL,
                        'created_at' => '2017-10-13 15:00:40',
                        'updated_at' => '2017-10-13 15:01:57',
                        'deleted_at' => NULL,
                        'cate_id' => 98,
                        'user_id' => 20,
                    ),
                    145 => 
                    array (
                        'id' => 146,
                        'title' => 'Exciter cuối 2011 xe zin',
                        'slug' => 'exciter-cuoi-2011-xe-zin',
                        'des' => 'bạn nào có nhu cầu mua để đi thì ok miễn tiếp cò vạc. xe chính chủ mua cuối năm 2011 đời đầu 5 số côn tay. xe còn zin nguyên bản chỉ thay nsd 10li của thái, cổ bô 32 và bóng pha oram bóng demi led. ai có nhu cầu mua thì lh hiếu',
                        'price' => '34000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Nghệ An, Việt Nam',
                        'lat' => 19.234248999999998,
                        'lng' => 104.920036,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\ncan-ban-exciter-cuoi-2011-chinh-chu-537-1406861186-53daff8210b94-1507882911.jpg","\\r\\n\\r\\n1517766-930a0911bc1a40604329384a450e93f7-1507882910.jpg","\\r\\n\\r\\n20150819105737588-1a0f-1507882910.jpg"]',
                        'created_at' => '2017-10-13 15:22:17',
                        'updated_at' => '2017-10-13 15:58:36',
                        'deleted_at' => NULL,
                        'cate_id' => 90,
                        'user_id' => 20,
                    ),
                    146 => 
                    array (
                        'id' => 147,
                        'title' => 'Bán xe máy',
                        'slug' => 'ban-xe-may',
                        'des' => 'xe chạy đang ngon.vì cần tiền nên bán .a e ai cần alo ngay',
                        'price' => '28000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Nghệ An, Việt Nam',
                        'lat' => 19.234248999999998,
                        'lng' => 104.920036,
                        'view_count' => 0,
                        'images' => '["\\r\\n\\r\\ncan-ban-exciter-cuoi-2011-chinh-chu-537-1406861186-53daff8210b94-1507883007.jpg"]',
                        'created_at' => '2017-10-13 15:24:00',
                        'updated_at' => '2017-10-13 15:58:39',
                        'deleted_at' => NULL,
                        'cate_id' => 90,
                        'user_id' => 20,
                    ),
                    147 => 
                    array (
                        'id' => 148,
                        'title' => 'Bán xe máy',
                        'slug' => 'ban-xe-may-1',
                        'des' => 'xe chạy đang ngon.vì cần tiền nên bán .a e ai cần alo ngay',
                        'price' => '28000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Nghệ An, Việt Nam',
                        'lat' => 19.234248999999998,
                        'lng' => 104.920036,
                        'view_count' => 0,
                        'images' => '["\\r\\n\\r\\ncan-ban-exciter-cuoi-2011-chinh-chu-537-1406861186-53daff8210b94-1507883007.jpg"]',
                        'created_at' => '2017-10-13 15:24:00',
                        'updated_at' => '2017-10-13 15:58:42',
                        'deleted_at' => NULL,
                        'cate_id' => 90,
                        'user_id' => 20,
                    ),
                    148 => 
                    array (
                        'id' => 149,
                        'title' => 'wave xám đời cuối 2015',
                        'slug' => 'wave-xam-doi-cuoi-2015',
                    'des' => 'Đổi xe bán em wave màu xám. Đời 2015. Chính chủ. Xe đã lên full đĩa zin của hãng và một số đồ chơi của thái nhé. Đèn đã lên LED L7 ( ngon và sáng dòng led ). Phuộc RCB mới cứng. Baga 10ly den tinh dien. Ai cần liên hệ sẽ có giá tốt hơn.',
                        'price' => '18000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Thành phố Vinh, Nghệ An, Việt Nam',
                        'lat' => 18.679584999999999,
                        'lng' => 105.681333,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\nb470bf983a0f3-256c-1507884039.jpg","\\r\\n\\r\\n20150901-7572c10d317c41114a5d2e5a104b997e-1441105466-1507884040.jpg","\\r\\n\\r\\n20150901-d879db094aad7f3a8433f12e8df55bbd-1441105634-1507884040.jpg"]',
                        'created_at' => '2017-10-13 15:41:09',
                        'updated_at' => '2017-10-13 15:58:45',
                        'deleted_at' => NULL,
                        'cate_id' => 179,
                        'user_id' => 20,
                    ),
                    149 => 
                    array (
                        'id' => 150,
                        'title' => 'Xe điện harley davidson',
                        'slug' => 'xe-dien-harley-davidson',
                        'des' => 'Xe này lúc mua 21.000.000 mới dùng đc một tuần không đi nên bán cho ai cần mua xe nghe được nhạc',
                        'price' => '20000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Đông Vĩnh, Thành phố Vinh, Nghệ An, Việt Nam',
                        'lat' => 18.686074999999999,
                        'lng' => 105.65908899999999,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n18inch-fat-tire-moped-vespa-harley-electric-scooterjpg-640x640-1507884191.jpg","\\r\\n\\r\\nshd-56pl5066708-adults-2-wheels-big-wheel-citycoco-electric-scooter-motorcycle-harley-motor-bike-1507884192.jpg","\\r\\n\\r\\nxedapdiensuzikad1xebaonamdokd-1507884192.jpg"]',
                        'created_at' => '2017-10-13 15:43:34',
                        'updated_at' => '2017-10-13 15:58:49',
                        'deleted_at' => NULL,
                        'cate_id' => 90,
                        'user_id' => 20,
                    ),
                    150 => 
                    array (
                        'id' => 151,
                        'title' => 'Cần mua xe waer apha màu trắng',
                        'slug' => 'can-mua-xe-waer-apha-mau-trang',
                        'des' => 'Em hoc sinh cần mua xe Ware mau trắng 6 den 8 triệu ai bán alo em tai thị xã thái hoà.',
                        'price' => '15000000',
                        'price_min' => NULL,
                        'ads_type' => 'buy',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Thị xã Thái Hòa, Nghệ An, Việt Nam',
                        'lat' => 19.293112000000001,
                        'lng' => 105.46539,
                        'view_count' => 1,
                        'images' => NULL,
                        'created_at' => '2017-10-13 15:45:27',
                        'updated_at' => '2017-10-13 15:58:51',
                        'deleted_at' => NULL,
                        'cate_id' => 179,
                        'user_id' => 20,
                    ),
                    151 => 
                    array (
                        'id' => 152,
                        'title' => 'Dream SYM Sanda Boss',
                        'slug' => 'dream-sym-sanda-boss',
                        'des' => 'Xe đẹp máy êm ai, chắc chắn đổ xăng kiếm tiền ae. Cần lh xem xe',
                        'price' => '4700000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Quán bàu thành phố vinh tỉnh nghệ an',
                        'lat' => 18.690650999999999,
                        'lng' => 105.669843,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\nad3833a515eda-bc6a-1507884561.jpg","\\r\\n\\r\\ncd5eac0e78f94-8014-1507884561.jpg","\\r\\n\\r\\n74d078fe97f84-9b12-1507884562.jpg","\\r\\n\\r\\n20115643-7f47d9473b5f4d1fb99f3e5d6f56b82f-1507884562.jpg"]',
                        'created_at' => '2017-10-13 15:50:07',
                        'updated_at' => '2017-10-13 15:58:55',
                        'deleted_at' => NULL,
                        'cate_id' => 181,
                        'user_id' => 20,
                    ),
                    152 => 
                    array (
                        'id' => 153,
                        'title' => 'Bãn xe Win cafe',
                        'slug' => 'ban-xe-win-cafe',
                        'des' => 'xe còn ngon. Bán hoặc gl vs min khờ. Gthl bs 37z9 9393 dàh cko ae sn 93.',
                        'price' => '7000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Xóm 5, tt. Yên Thành, Yên Thành, Nghệ An, Việt Nam',
                        'lat' => 18.997785,
                        'lng' => 105.463741,
                        'view_count' => 1,
                        'images' => NULL,
                        'created_at' => '2017-10-13 15:53:29',
                        'updated_at' => '2017-10-13 15:59:01',
                        'deleted_at' => NULL,
                        'cate_id' => 179,
                        'user_id' => 20,
                    ),
                    153 => 
                    array (
                        'id' => 154,
                        'title' => 'bán Xe atila',
                        'slug' => 'ban-xe-atila',
                        'des' => 'Cần Bán nhanh xe attila biển 37 ai cần liên hệ',
                        'price' => '4500000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'active',
                        'address' => 'Đường phạm hồng thái, phường vinh tân, thành phố vinh, nghệ an',
                        'lat' => 18.660941000000001,
                        'lng' => 105.669133,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n5b6f6207d1ee3-9f4e-1507884998.jpg","\\r\\n\\r\\n074-3b35-1507884998.jpg","\\r\\n\\r\\nsuzuki-sapphire-1507884999.jpg"]',
                        'created_at' => '2017-10-13 15:58:23',
                        'updated_at' => '2017-10-13 15:58:57',
                        'deleted_at' => NULL,
                        'cate_id' => 183,
                        'user_id' => 20,
                    ),
                    154 => 
                    array (
                        'id' => 155,
                        'title' => 'Bán Yamaha Exciter 2010',
                        'slug' => 'ban-yamaha-exciter-2010',
                        'des' => 'Xe long lanh,máy zin bốc chưa bổ chẻ,nay bán hoặc giao lưu với dòng xe khác như ex 2016-2017,sh....có phụ thêm! Xem xe ở hưng nguyên-vinh-sdt',
                        'price' => '3100000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'deactive',
                        'address' => 'Hưng Xá, Thành phố Vinh, Nghệ An, Việt Nam',
                        'lat' => 18.613204,
                        'lng' => 105.608998,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\nhgr1437317824-1507885250.jpg"]',
                        'created_at' => '2017-10-13 16:01:18',
                        'updated_at' => '2017-10-13 16:01:19',
                        'deleted_at' => NULL,
                        'cate_id' => 90,
                        'user_id' => 20,
                    ),
                    155 => 
                    array (
                        'id' => 156,
                        'title' => 'Jupiter Xám - Vàng',
                        'slug' => 'jupiter-xam-vang',
                        'des' => 'tất cả phụ tùng đều là nguyên bản, thậm chí ống xả mình bị vỡ nhưng hàn lại y nguyên luôn, ai quan tâm sms hoặc call cho mình',
                        'price' => '10000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'deactive',
                        'address' => 'Khu đô thị vườn xanh Đô lương nghệ an',
                        'lat' => 18.892097,
                        'lng' => 105.308762,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n20150810044125176-a4d6-1507885414.jpg","\\r\\n\\r\\n20150810044125613-5a9d-1507885414.jpg","\\r\\n\\r\\n20150810044125754-6afb-1507885415.jpg"]',
                        'created_at' => '2017-10-13 16:04:19',
                        'updated_at' => '2017-10-13 16:04:20',
                        'deleted_at' => NULL,
                        'cate_id' => 92,
                        'user_id' => 20,
                    ),
                    156 => 
                    array (
                        'id' => 157,
                        'title' => 'Xe nvx mới mua được 6 tháng',
                        'slug' => 'xe-nvx-moi-mua-duoc-6-thang',
                        'des' => 'Như tiêu đề. Xe còn mới chưa đụng chạm gì. Chạy hơn 9000km. Chính chủ sang tên nhanh. Mua bán tại nhà cho ae yên tâm nhé.',
                        'price' => '35000000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'deactive',
                        'address' => '104 QL1A, Quỳnh Hồng, Nghệ An, Việt Nam',
                        'lat' => 19.148249,
                        'lng' => 105.63204899999999,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\nyamaha-nvx-125-vietnam-ori-1507886034.jpg","\\r\\n\\r\\nnvx1-zing-1507886036.jpg"]',
                        'created_at' => '2017-10-13 16:14:32',
                        'updated_at' => '2017-10-13 16:14:34',
                        'deleted_at' => NULL,
                        'cate_id' => 93,
                        'user_id' => 20,
                    ),
                    157 => 
                    array (
                        'id' => 158,
                        'title' => 'Xe SH 150i hoặc giao lưu trao đổi xe khác',
                        'slug' => 'xe-sh-150i-hoac-giao-luu-trao-doi-xe-khac',
                        'des' => 'Bán hoăc giao lưu trao đổi xe SH 150i nhập khẩu đời 2008,màu đen,xe đẹp,nguyên bản từ A-Z . Là dòng fi kim phun điện tử nên rất tiết kiệm xăng và chạy rất đầm xe và bốc.nói chung xe rất đẹp ko chê vào đâu được,ai xem là thích liền. Ngoài ra nhà mình còn có nhiều màu khác nữa.',
                        'price' => '0',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'trade',
                        'state' => 'old',
                        'status' => 'deactive',
                        'address' => 'xã Quỳnh Hồng, Nghệ An, Việt Nam',
                        'lat' => 19.150113999999999,
                        'lng' => 105.64129800000001,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\nimages-1507887117.jpg","\\r\\n\\r\\n1368838709-sh150i-7-1507887116.jpg","\\r\\n\\r\\n563c34f682ce9-1446786294-1507887118.jpg"]',
                        'created_at' => '2017-10-13 16:32:57',
                        'updated_at' => '2017-10-13 16:33:00',
                        'deleted_at' => NULL,
                        'cate_id' => 89,
                        'user_id' => 20,
                    ),
                    158 => 
                    array (
                        'id' => 159,
                        'title' => 'Moto lifan euro 150',
                        'slug' => 'moto-lifan-euro-150',
                        'des' => 'xe giấy tờ đầy đủ biên 37 sang tên đổi chủ nhanh gọn máy móc ngon lành',
                        'price' => '10500000',
                        'price_min' => NULL,
                        'ads_type' => 'sell',
                        'price_type' => 'fixed',
                        'state' => 'old',
                        'status' => 'deactive',
                        'address' => 'Thanh Lương, Nghệ An, Việt Nam',
                        'lat' => 18.709364000000001,
                        'lng' => 105.42730899999999,
                        'view_count' => 1,
                        'images' => '["\\r\\n\\r\\n20171002150207-47c7-1507887409.jpg","\\r\\n\\r\\nlifan-lf250-cobra-2008-1-1507887410.jpg"]',
                        'created_at' => '2017-10-13 16:37:23',
                        'updated_at' => '2017-10-13 16:37:25',
                        'deleted_at' => NULL,
                        'cate_id' => 97,
                        'user_id' => 20,
                    ),
                ));
        
        
    }
}