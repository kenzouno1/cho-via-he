<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'id' => 17,
                'name' => 'Ô tô và Xe máy',
                'slug' => 'o-to-va-xe-may',
                'image' => 'fa fa-car',
                'thumbnail' => NULL,
                'parent' => 0,
            ),
            1 => 
            array (
                'id' => 18,
                'name' => 'Bất động sản',
                'slug' => 'bat-dong-san',
                'image' => 'ion-podium',
                'thumbnail' => NULL,
                'parent' => 0,
            ),
            2 => 
            array (
                'id' => 19,
                'name' => 'Việc làm',
                'slug' => 'viec-lam',
                'image' => 'ion-briefcase',
                'thumbnail' => NULL,
                'parent' => 0,
            ),
            3 => 
            array (
                'id' => 20,
                'name' => 'Thiết bị gia đình',
                'slug' => 'thiet-bi-gia-dinh',
                'image' => 'ion-ios-home',
                'thumbnail' => NULL,
                'parent' => 0,
            ),
            4 => 
            array (
                'id' => 21,
                'name' => 'Dịch vụ cho thuê',
                'slug' => 'dich-vu-cho-thue',
                'image' => 'ion-dollar',
                'thumbnail' => NULL,
                'parent' => 0,
            ),
            5 => 
            array (
                'id' => 22,
                'name' => 'Điện tử máy tính',
                'slug' => 'dien-tu-may-tinh',
                'image' => 'ion-android-laptop',
                'thumbnail' => NULL,
                'parent' => 0,
            ),
            6 => 
            array (
                'id' => 23,
                'name' => 'Thời trang',
                'slug' => 'thoi-trang',
                'image' => 'ion-person',
                'thumbnail' => NULL,
                'parent' => 0,
            ),
            7 => 
            array (
                'id' => 24,
                'name' => 'Sức khỏe & Sắc đẹp',
                'slug' => 'suc-khoe-sac-dep',
                'image' => 'fa fa-microchip',
                'thumbnail' => NULL,
                'parent' => 0,
            ),
            8 => 
            array (
                'id' => 25,
                'name' => 'Bách hóa gia đình',
                'slug' => 'bach-hoa-gia-dinh',
                'image' => 'fa fa-industry',
                'thumbnail' => NULL,
                'parent' => 0,
            ),
            9 => 
            array (
                'id' => 26,
                'name' => 'Đồ dùng văn phòng',
                'slug' => 'do-dung-van-phong',
                'image' => 'fa fa-building',
                'thumbnail' => NULL,
                'parent' => 0,
            ),
            10 => 
            array (
                'id' => 28,
                'name' => 'Sách & Dịch vụ',
                'slug' => 'sach-dich-vu',
                'image' => 'fa fa-book',
                'thumbnail' => NULL,
                'parent' => 0,
            ),
            11 => 
            array (
                'id' => 29,
                'name' => 'Đồng hồ & Phụ kiện',
                'slug' => 'dong-ho-phu-kien',
                'image' => 'fa fa-clock-o',
                'thumbnail' => NULL,
                'parent' => 0,
            ),
            12 => 
            array (
                'id' => 30,
                'name' => 'Thời trang nữ',
                'slug' => 'thoi-trang-nu',
                'image' => 'ion-person',
                'thumbnail' => NULL,
                'parent' => 23,
            ),
            13 => 
            array (
                'id' => 31,
                'name' => 'Thời trang nam',
                'slug' => 'thoi-trang-nam',
                'image' => 'ion-person',
                'thumbnail' => NULL,
                'parent' => 23,
            ),
            14 => 
            array (
                'id' => 32,
                'name' => 'Ô tô',
                'slug' => 'o-to',
                'image' => 'fa fa-car',
                'thumbnail' => NULL,
                'parent' => 17,
            ),
            15 => 
            array (
                'id' => 33,
                'name' => 'Xe máy',
                'slug' => 'xe-may',
                'image' => 'fa fa-car',
                'thumbnail' => NULL,
                'parent' => 17,
            ),
            16 => 
            array (
                'id' => 34,
                'name' => 'Xe hơi',
                'slug' => 'xe-hoi',
                'image' => 'fa fa-car',
                'thumbnail' => NULL,
                'parent' => 32,
            ),
            17 => 
            array (
                'id' => 35,
                'name' => 'Xe tải',
                'slug' => 'xe-tai',
                'image' => 'fa fa-car',
                'thumbnail' => NULL,
                'parent' => 32,
            ),
            18 => 
            array (
                'id' => 36,
                'name' => 'Xe khách',
                'slug' => 'xe-khach',
                'image' => 'fa fa-car',
                'thumbnail' => NULL,
                'parent' => 32,
            ),
            19 => 
            array (
                'id' => 38,
                'name' => 'Xe số',
                'slug' => 'xe-so',
                'image' => 'fa fa-car',
                'thumbnail' => NULL,
                'parent' => 33,
            ),
            20 => 
            array (
                'id' => 39,
                'name' => 'Xe ga',
                'slug' => 'xe-ga',
                'image' => 'fa fa-car',
                'thumbnail' => NULL,
                'parent' => 33,
            ),
            21 => 
            array (
                'id' => 41,
                'name' => 'Phụ tùng xe',
                'slug' => 'phu-tung-xe',
                'image' => 'fa fa-car',
                'thumbnail' => NULL,
                'parent' => 17,
            ),
            22 => 
            array (
                'id' => 47,
                'name' => 'Đồ gia dụng',
                'slug' => 'do-gia-dung',
                'image' => 'ion-ios-home',
                'thumbnail' => NULL,
                'parent' => 20,
            ),
            23 => 
            array (
                'id' => 48,
                'name' => 'Bếp và phòng ăn',
                'slug' => 'bep-va-phong-an',
                'image' => 'ion-ios-home',
                'thumbnail' => NULL,
                'parent' => 20,
            ),
            24 => 
            array (
                'id' => 49,
                'name' => 'Đồ nội thất, trang trí nhà cửa',
                'slug' => 'do-noi-that-trang-tri-nha-cua',
                'image' => 'ion-ios-home',
                'thumbnail' => NULL,
                'parent' => 20,
            ),
            25 => 
            array (
                'id' => 50,
                'name' => 'Chăm sóc nhà cửa',
                'slug' => 'cham-soc-nha-cua',
                'image' => 'ion-ios-home',
                'thumbnail' => NULL,
                'parent' => 20,
            ),
            26 => 
            array (
                'id' => 51,
                'name' => 'DIY & Ngoài trời',
                'slug' => 'diy-ngoai-troi',
                'image' => 'ion-ios-home',
                'thumbnail' => NULL,
                'parent' => 20,
            ),
            27 => 
            array (
                'id' => 52,
                'name' => 'Điện thoại & Máy tính bảng',
                'slug' => 'dien-thoai-may-tinh-bang',
                'image' => 'ion-android-laptop',
                'thumbnail' => NULL,
                'parent' => 22,
            ),
            28 => 
            array (
                'id' => 53,
                'name' => 'Máy tính và láp top',
                'slug' => 'may-tinh-va-lap-top',
                'image' => 'ion-android-laptop',
                'thumbnail' => NULL,
                'parent' => 22,
            ),
            29 => 
            array (
                'id' => 54,
                'name' => 'Máy ảnh và máy quay phim',
                'slug' => 'may-anh-va-may-quay-phim',
                'image' => 'ion-android-laptop',
                'thumbnail' => NULL,
                'parent' => 22,
            ),
            30 => 
            array (
                'id' => 55,
                'name' => 'Tivi, video, thiết bị số',
                'slug' => 'tivi-video-thiet-bi-so',
                'image' => 'ion-android-laptop',
                'thumbnail' => NULL,
                'parent' => 22,
            ),
            31 => 
            array (
                'id' => 56,
                'name' => 'Đồng hồ số',
                'slug' => 'dong-ho-so',
                'image' => 'fa fa-clock-o',
                'thumbnail' => NULL,
                'parent' => 29,
            ),
            32 => 
            array (
                'id' => 57,
                'name' => 'Đồng hồ điện tử',
                'slug' => 'dong-ho-dien-tu',
                'image' => 'fa fa-clock-o',
                'thumbnail' => NULL,
                'parent' => 29,
            ),
            33 => 
            array (
                'id' => 58,
                'name' => 'Phụ kiện',
                'slug' => 'phu-kien',
                'image' => 'fa fa-clock-o',
                'thumbnail' => NULL,
                'parent' => 29,
            ),
            34 => 
            array (
                'id' => 59,
                'name' => 'Căn hộ / Chung cư',
                'slug' => 'can-ho-chung-cu',
                'image' => 'ion-podium',
                'thumbnail' => NULL,
                'parent' => 18,
            ),
            35 => 
            array (
                'id' => 60,
                'name' => 'Nhà ở',
                'slug' => 'nha-o',
                'image' => 'ion-podium',
                'thumbnail' => NULL,
                'parent' => 18,
            ),
            36 => 
            array (
                'id' => 61,
                'name' => 'Đất',
                'slug' => 'dat',
                'image' => 'ion-podium',
                'thumbnail' => NULL,
                'parent' => 18,
            ),
            37 => 
            array (
                'id' => 62,
                'name' => 'Văn phòng / Mặt bằng kinh doanh',
                'slug' => 'van-phong-mat-bang-kinh-doanh',
                'image' => 'ion-podium',
                'thumbnail' => NULL,
                'parent' => 18,
            ),
            38 => 
            array (
                'id' => 63,
                'name' => 'Phòng trọ',
                'slug' => 'phong-tro',
                'image' => 'ion-podium',
                'thumbnail' => NULL,
                'parent' => 18,
            ),
            39 => 
            array (
                'id' => 70,
                'name' => 'Mazda',
                'slug' => 'mazda',
                'image' => 'fa fa-car',
                'thumbnail' => 'assets/img/category/logo-dt1_1505958100.jpg',
                'parent' => 34,
            ),
            40 => 
            array (
                'id' => 71,
                'name' => 'Mercedes',
                'slug' => 'mercedes',
                'image' => 'fa fa-car',
                'thumbnail' => 'assets/img/category/logo-dt2_1505958144.jpg',
                'parent' => 34,
            ),
            41 => 
            array (
                'id' => 72,
                'name' => 'Hyundai',
                'slug' => 'hyundai',
                'image' => 'fa fa-car',
                'thumbnail' => 'assets/img/category/hyundai_1505958155.jpg',
                'parent' => 34,
            ),
            42 => 
            array (
                'id' => 73,
                'name' => 'Nissan',
                'slug' => 'nissan',
                'image' => 'fa fa-car',
                'thumbnail' => 'assets/img/category/logo-dt3_1505958167.jpg',
                'parent' => 34,
            ),
            43 => 
            array (
                'id' => 74,
                'name' => 'Toyota',
                'slug' => 'toyota',
                'image' => 'fa fa-car',
                'thumbnail' => 'assets/img/category/logo-dt4_1505958182.jpg',
                'parent' => 34,
            ),
            44 => 
            array (
                'id' => 75,
                'name' => 'BMW',
                'slug' => 'bmw',
                'image' => 'fa fa-car',
                'thumbnail' => 'assets/img/category/logo-dt5_1505958192.jpg',
                'parent' => 34,
            ),
            45 => 
            array (
                'id' => 76,
                'name' => 'KIA',
                'slug' => 'kia',
                'image' => 'fa fa-car',
                'thumbnail' => 'assets/img/category/kia_1507106409.png',
                'parent' => 34,
            ),
            46 => 
            array (
                'id' => 77,
                'name' => 'Lexus',
                'slug' => 'lexus',
                'image' => 'fa fa-car',
                'thumbnail' => 'assets/img/category/lexus_1505891457_1505958315.png',
                'parent' => 34,
            ),
            47 => 
            array (
                'id' => 78,
                'name' => 'Mini',
                'slug' => 'mini',
                'image' => 'fa fa-car',
                'thumbnail' => 'assets/img/category/mini_1505891060_1505958381.png',
                'parent' => 34,
            ),
            48 => 
            array (
                'id' => 79,
                'name' => 'Mitsubishi',
                'slug' => 'mitsubishi',
                'image' => 'fa fa-car',
                'thumbnail' => 'assets/img/category/mits_1505976402.png',
                'parent' => 34,
            ),
            49 => 
            array (
                'id' => 80,
                'name' => 'Porsche',
                'slug' => 'porsche',
                'image' => 'fa fa-car',
                'thumbnail' => 'assets/img/category/porsche_1505958256.jpg',
                'parent' => 34,
            ),
            50 => 
            array (
                'id' => 81,
                'name' => 'Hyundai',
                'slug' => 'hyundai-1',
                'image' => 'fa fa-car',
                'thumbnail' => 'assets/img/category/hyundai_1505958447.jpg',
                'parent' => 35,
            ),
            51 => 
            array (
                'id' => 82,
                'name' => 'Isuzu',
                'slug' => 'isuzu',
                'image' => 'fa fa-car',
                'thumbnail' => 'assets/img/category/isuzu_1505958462.jpg',
                'parent' => 35,
            ),
            52 => 
            array (
                'id' => 83,
                'name' => 'Toyota',
                'slug' => 'toyota-1',
                'image' => 'fa fa-car',
                'thumbnail' => 'assets/img/category/logo-dt4_1505958475.jpg',
                'parent' => 35,
            ),
            53 => 
            array (
                'id' => 84,
                'name' => 'Suzuki',
                'slug' => 'suzuki',
                'image' => 'fa fa-car',
                'thumbnail' => 'assets/img/category/suzuki_1505958485.jpg',
                'parent' => 35,
            ),
            54 => 
            array (
                'id' => 87,
                'name' => 'Daihatsu',
                'slug' => 'daihatsu',
                'image' => 'fa fa-car',
                'thumbnail' => 'assets/img/category/daihatsu_1505975886.png',
                'parent' => 36,
            ),
            55 => 
            array (
                'id' => 88,
                'name' => 'piaggio',
                'slug' => 'piaggio',
                'image' => 'fa fa-car',
                'thumbnail' => 'assets/img/category/piaggio_1505958506.jpg',
                'parent' => 39,
            ),
            56 => 
            array (
                'id' => 89,
                'name' => 'Honda',
                'slug' => 'honda',
                'image' => 'fa fa-car',
                'thumbnail' => 'assets/img/category/honda_1505958515.jpg',
                'parent' => 39,
            ),
            57 => 
            array (
                'id' => 90,
                'name' => 'Xe máy khác',
                'slug' => 'xe-may-khac',
                'image' => 'fa fa-car',
                'thumbnail' => 'assets/img/category/logo-moto_1505958524.jpg',
                'parent' => 33,
            ),
            58 => 
            array (
                'id' => 91,
                'name' => 'Xe Ô tô khác',
                'slug' => 'xe-o-to-khac',
                'image' => 'fa fa-car',
                'thumbnail' => 'assets/img/category/logo-oto_1505958580.jpg',
                'parent' => 32,
            ),
            59 => 
            array (
                'id' => 92,
                'name' => 'yamaha',
                'slug' => 'yamaha',
                'image' => 'fa fa-car',
                'thumbnail' => 'assets/img/category/yamaha_1505958596.jpg',
                'parent' => 38,
            ),
            60 => 
            array (
                'id' => 93,
                'name' => 'yamaha',
                'slug' => 'yamaha-1',
                'image' => 'fa fa-car',
                'thumbnail' => 'assets/img/category/yamaha_1505958607.jpg',
                'parent' => 39,
            ),
            61 => 
            array (
                'id' => 95,
                'name' => 'Phụ tùng xe máy',
                'slug' => 'phu-tung-xe-may',
                'image' => 'fa fa-car',
                'thumbnail' => 'assets/img/category/logo-moto_1505958628.jpg',
                'parent' => 41,
            ),
            62 => 
            array (
                'id' => 96,
                'name' => 'Phụ tùng xe Ô tô',
                'slug' => 'phu-tung-xe-o-to',
                'image' => 'fa fa-car',
                'thumbnail' => 'assets/img/category/logo-oto_1505958637.jpg',
                'parent' => 41,
            ),
            63 => 
            array (
                'id' => 97,
                'name' => 'Xe thể thao',
                'slug' => 'xe-the-thao',
                'image' => 'fa fa-car',
                'thumbnail' => 'assets/img/category/img-default-car_1505958792.jpg',
                'parent' => 33,
            ),
            64 => 
            array (
                'id' => 98,
                'name' => 'hyundai',
                'slug' => 'hyundai-2',
                'image' => 'fa fa-car',
                'thumbnail' => 'assets/img/category/hyundai_1505958686.jpg',
                'parent' => 36,
            ),
            65 => 
            array (
                'id' => 99,
                'name' => 'thaco',
                'slug' => 'thaco',
                'image' => 'fa fa-car',
                'thumbnail' => 'assets/img/category/thaco_1505958698.jpg',
                'parent' => 36,
            ),
            66 => 
            array (
                'id' => 100,
                'name' => 'Samco',
                'slug' => 'samco',
                'image' => 'fa fa-car',
                'thumbnail' => 'assets/img/category/samco_1505958803.jpg',
                'parent' => 36,
            ),
            67 => 
            array (
                'id' => 105,
                'name' => 'Bác sĩ',
                'slug' => 'bac-si',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            68 => 
            array (
                'id' => 107,
                'name' => 'Bán hàng',
                'slug' => 'ban-hang',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            69 => 
            array (
                'id' => 108,
                'name' => 'Quản trị kinh doanh',
                'slug' => 'quan-tri-kinh-doanh',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            70 => 
            array (
                'id' => 109,
                'name' => 'Xây dựng',
                'slug' => 'xay-dung',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            71 => 
            array (
                'id' => 110,
                'name' => 'Khách sạn - Nhà hàng',
                'slug' => 'khach-san-nha-hang',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            72 => 
            array (
                'id' => 111,
                'name' => 'Marketing-Pr',
                'slug' => 'marketing-pr',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            73 => 
            array (
                'id' => 112,
                'name' => 'Kế toàn - Kiểm toán',
                'slug' => 'ke-toan-kiem-toan',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            74 => 
            array (
                'id' => 113,
                'name' => 'Nhân sự',
                'slug' => 'nhan-su',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            75 => 
            array (
                'id' => 114,
                'name' => 'Thực phẩm - Đồ uống',
                'slug' => 'thuc-pham-do-uong',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            76 => 
            array (
                'id' => 115,
                'name' => 'Kinh doanh bất động sản',
                'slug' => 'kinh-doanh-bat-dong-san',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            77 => 
            array (
                'id' => 116,
                'name' => 'Báo chí - Truyền hình',
                'slug' => 'bao-chi-truyen-hinh',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            78 => 
            array (
                'id' => 117,
                'name' => 'Bảo hiểm',
                'slug' => 'bao-hiem',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            79 => 
            array (
                'id' => 118,
                'name' => 'Bảo vệ',
                'slug' => 'bao-ve',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            80 => 
            array (
                'id' => 119,
                'name' => 'Biên - Phiên dịch',
                'slug' => 'bien-phien-dich',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            81 => 
            array (
                'id' => 120,
                'name' => 'Bưu chính',
                'slug' => 'buu-chinh',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            82 => 
            array (
                'id' => 121,
                'name' => 'Chứng khoán - Vàng',
                'slug' => 'chung-khoan-vang',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            83 => 
            array (
                'id' => 122,
                'name' => 'Cơ khí - Chế tạo',
                'slug' => 'co-khi-che-tao',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            84 => 
            array (
                'id' => 123,
                'name' => 'Công nghệ cao',
                'slug' => 'cong-nghe-cao',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            85 => 
            array (
                'id' => 124,
                'name' => 'Công nghiệp',
                'slug' => 'cong-nghiep',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            86 => 
            array (
                'id' => 125,
                'name' => 'Dầu khí - Hóa chất',
                'slug' => 'dau-khi-hoa-chat',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            87 => 
            array (
                'id' => 126,
                'name' => 'Đầu tư',
                'slug' => 'dau-tu',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            88 => 
            array (
                'id' => 127,
                'name' => 'Dệt may - Gia dày',
                'slug' => 'det-may-gia-day',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            89 => 
            array (
                'id' => 128,
                'name' => 'Dịch vụ',
                'slug' => 'dich-vu',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            90 => 
            array (
                'id' => 129,
                'name' => 'Điện tử viễn thông',
                'slug' => 'dien-tu-vien-thong',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            91 => 
            array (
                'id' => 130,
                'name' => 'Điện - Điện tử',
                'slug' => 'dien-dien-tu',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            92 => 
            array (
                'id' => 131,
                'name' => 'Du lịch',
                'slug' => 'du-lich',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            93 => 
            array (
                'id' => 132,
                'name' => 'Game',
                'slug' => 'game',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            94 => 
            array (
                'id' => 133,
                'name' => 'Giáo dục - Đào tạo',
                'slug' => 'giao-duc-dao-tao',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            95 => 
            array (
                'id' => 134,
                'name' => 'Hàng gia dụng',
                'slug' => 'hang-gia-dung',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            96 => 
            array (
                'id' => 135,
                'name' => 'Hàng không',
                'slug' => 'hang-khong',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            97 => 
            array (
                'id' => 136,
                'name' => 'Hóa học - Sinh học',
                'slug' => 'hoa-hoc-sinh-hoc',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            98 => 
            array (
                'id' => 137,
                'name' => 'Hoạch định - Dự án',
                'slug' => 'hoach-dinh-du-an',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            99 => 
            array (
                'id' => 138,
                'name' => 'In ấn - Xuất bản',
                'slug' => 'in-an-xuat-ban',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            100 => 
            array (
                'id' => 139,
                'name' => 'IT phần cứng / Mạng',
                'slug' => 'it-phan-cung-mang',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            101 => 
            array (
                'id' => 140,
                'name' => 'Kiến trúc - KT nội thất',
                'slug' => 'kien-truc-kt-noi-that',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            102 => 
            array (
                'id' => 141,
                'name' => 'Kỹ thuật',
                'slug' => 'ky-thuat',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            103 => 
            array (
                'id' => 142,
                'name' => 'Kỹ thuật ứng dụng',
                'slug' => 'ky-thuat-ung-dung',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            104 => 
            array (
                'id' => 143,
                'name' => 'Mỹ phẩm - Trang sức',
                'slug' => 'my-pham-trang-suc',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            105 => 
            array (
                'id' => 144,
                'name' => 'Ngân hàng',
                'slug' => 'ngan-hang',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            106 => 
            array (
                'id' => 145,
                'name' => 'Ngành nghề khác',
                'slug' => 'nganh-nghe-khac',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            107 => 
            array (
                'id' => 146,
                'name' => 'Nghệ thuật - Điện ảnh',
                'slug' => 'nghe-thuat-dien-anh',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            108 => 
            array (
                'id' => 148,
                'name' => 'Nông - Lâm - Ngư nghiệp',
                'slug' => 'nong-lam-ngu-nghiep',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            109 => 
            array (
                'id' => 149,
                'name' => 'Ô tô xe máy',
                'slug' => 'o-to-xe-may',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            110 => 
            array (
                'id' => 150,
                'name' => 'Pháp lý',
                'slug' => 'phap-ly',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            111 => 
            array (
                'id' => 151,
                'name' => 'Quan hệ đối ngoại',
                'slug' => 'quan-he-doi-ngoai',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            112 => 
            array (
                'id' => 152,
                'name' => 'Thiết kế đồ họa web',
                'slug' => 'thiet-ke-do-hoa-web',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            113 => 
            array (
                'id' => 153,
                'name' => 'Thiết kế mỹ thuật',
                'slug' => 'thiet-ke-my-thuat',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            114 => 
            array (
                'id' => 154,
                'name' => 'Thời trang',
                'slug' => 'thoi-trang-1',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            115 => 
            array (
                'id' => 155,
                'name' => 'Thủ công mỹ nghệ',
                'slug' => 'thu-cong-my-nghe',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            116 => 
            array (
                'id' => 156,
                'name' => 'Thư ký -  trợ lý',
                'slug' => 'thu-ky-tro-ly',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            117 => 
            array (
                'id' => 157,
                'name' => 'Thương mại điện tử',
                'slug' => 'thuong-mai-dien-tu',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            118 => 
            array (
                'id' => 158,
                'name' => 'Tiếp thị - Quảng cáo',
                'slug' => 'tiep-thi-quang-cao',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            119 => 
            array (
                'id' => 159,
                'name' => 'Tổ chức sự kiện - Tặng quà',
                'slug' => 'to-chuc-su-kien-tang-qua',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            120 => 
            array (
                'id' => 160,
                'name' => 'Vận tải',
                'slug' => 'van-tai',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            121 => 
            array (
                'id' => 161,
                'name' => 'Vật tư - Thiết bị',
                'slug' => 'vat-tu-thiet-bi',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            122 => 
            array (
                'id' => 162,
                'name' => 'Xuất nhập khẩu',
                'slug' => 'xuat-nhap-khau',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            123 => 
            array (
                'id' => 163,
                'name' => 'Y tê - Dược',
                'slug' => 'y-te-duoc',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            124 => 
            array (
                'id' => 165,
                'name' => 'IT Phần mềm',
                'slug' => 'it-phan-mem',
                'image' => NULL,
                'thumbnail' => NULL,
                'parent' => 19,
            ),
            125 => 
            array (
                'id' => 167,
                'name' => 'Audi',
                'slug' => 'audi',
                'image' => 'fa fa-car',
                'thumbnail' => NULL,
                'parent' => 34,
            ),
            126 => 
            array (
                'id' => 168,
                'name' => 'honda',
                'slug' => 'honda-1',
                'image' => 'fa fa-car',
                'thumbnail' => NULL,
                'parent' => 34,
            ),
            127 => 
            array (
                'id' => 169,
                'name' => 'suzuki',
                'slug' => 'suzuki-1',
                'image' => 'fa fa-car',
                'thumbnail' => NULL,
                'parent' => 34,
            ),
            128 => 
            array (
                'id' => 170,
                'name' => 'Ford',
                'slug' => 'ford',
                'image' => 'fa fa-car',
                'thumbnail' => NULL,
                'parent' => 34,
            ),
            129 => 
            array (
                'id' => 171,
                'name' => 'Daewoo',
                'slug' => 'daewoo',
                'image' => 'fa fa-car',
                'thumbnail' => NULL,
                'parent' => 34,
            ),
            130 => 
            array (
                'id' => 172,
                'name' => 'Xe Hơi Khác',
                'slug' => 'xe-hoi-khac',
                'image' => 'fa fa-car',
                'thumbnail' => NULL,
                'parent' => 34,
            ),
            131 => 
            array (
                'id' => 174,
                'name' => 'Mercedes Benz',
                'slug' => 'mercedes-benz',
                'image' => 'fa fa-car',
                'thumbnail' => NULL,
                'parent' => 36,
            ),
            132 => 
            array (
                'id' => 175,
                'name' => 'Ford',
                'slug' => 'ford-1',
                'image' => 'fa fa-car',
                'thumbnail' => NULL,
                'parent' => 35,
            ),
            133 => 
            array (
                'id' => 176,
                'name' => 'Mitsubishi',
                'slug' => 'mitsubishi-1',
                'image' => 'fa fa-car',
                'thumbnail' => 'assets/img/category/mits_1505976402_1507794926.png',
                'parent' => 35,
            ),
            134 => 
            array (
                'id' => 177,
                'name' => 'KIA',
                'slug' => 'kia-1',
                'image' => 'fa fa-car',
                'thumbnail' => 'assets/img/category/kia_1507106409_1507794972.png',
                'parent' => 35,
            ),
            135 => 
            array (
                'id' => 178,
                'name' => 'Xe tải khác',
                'slug' => 'xe-tai-khac',
                'image' => 'fa fa-car',
                'thumbnail' => NULL,
                'parent' => 35,
            ),
            136 => 
            array (
                'id' => 179,
                'name' => 'Honda',
                'slug' => 'honda-2',
                'image' => 'fa fa-car',
                'thumbnail' => 'assets/img/category/honda_1505958515_1507883132.jpg',
                'parent' => 38,
            ),
            137 => 
            array (
                'id' => 180,
                'name' => 'Suzuki',
                'slug' => 'suzuki-2',
                'image' => 'fa fa-car',
                'thumbnail' => 'assets/img/category/suzuki_1505958485_1507883174.jpg',
                'parent' => 38,
            ),
            138 => 
            array (
                'id' => 181,
                'name' => 'SYM',
                'slug' => 'sym',
                'image' => 'fa fa-car',
                'thumbnail' => NULL,
                'parent' => 38,
            ),
            139 => 
            array (
                'id' => 182,
                'name' => 'Hãng khác',
                'slug' => 'hang-khac',
                'image' => 'fa fa-car',
                'thumbnail' => 'assets/img/category/img-default-car_1507884446.jpg',
                'parent' => 38,
            ),
            140 => 
            array (
                'id' => 183,
                'name' => 'Victory',
                'slug' => 'victory',
                'image' => 'fa fa-car',
                'thumbnail' => NULL,
                'parent' => 39,
            ),
        ));
        
        
    }
}