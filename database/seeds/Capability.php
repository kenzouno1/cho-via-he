<?php

use Illuminate\Database\Seeder;

class Capability extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('role_relationship')->insert([
            'role_id' => 1,
            'admin_id' =>1,
        ]);
         DB::table('role_capability')->insert([
            'role_id' => 1,
            'capability' => 'category.create',
        ]);
          DB::table('role_capability')->insert([
            'role_id' => 1,
            'capability' => 'category.edit',
        ]);
         DB::table('role_capability')->insert([
            'role_id' => 1,
            'capability' => 'category.delete',
        ]);
        DB::table('role_capability')->insert([
            'role_id' => 1,
            'capability' => 'category.view',
        ]);

        DB::table('role_capability')->insert([
            'role_id' => 1,
            'capability' => 'user.create',
        ]);
         DB::table('role_capability')->insert([
            'role_id' => 1,
            'capability' => 'user.edit',
        ]);
        DB::table('role_capability')->insert([
            'role_id' => 1,
            'capability' => 'user.delete',
        ]);
        DB::table('role_capability')->insert([
            'role_id' => 1,
            'capability' => 'user.view',
        ]);

        DB::table('role_capability')->insert([
            'role_id' => 1,
            'capability' => 'roles.view',
        ]);
        DB::table('role_capability')->insert([
            'role_id' => 1,
            'capability' => 'roles.delete',
        ]);
        DB::table('role_capability')->insert([
            'role_id' => 1,
            'capability' => 'roles.edit',
        ]);
        DB::table('role_capability')->insert([
            'role_id' => 1,
            'capability' => 'roles.create',
        ]);
    }
}
