<?php

use Illuminate\Database\Seeder;

class BoostsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('boosts')->delete();
        
        \DB::table('boosts')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Miễn Phí',
                'price' => '0',
                'days' => 0,
                'type' => 'package',
                'description' => 'Tin quảng cáo rao vặt chuẩn',
                'created_at' => NULL,
                'updated_at' => '2017-07-25 19:40:35',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Tiêu biểu',
                'price' => '5000',
                'days' => 30,
                'type' => 'package',
                'description' => 'Làm cho quảng cáo của bạn nổi bật so với đám đông',
                'created_at' => NULL,
                'updated_at' => '2017-07-18 05:58:35',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Nổi bật',
                'price' => '15000',
                'days' => 30,
                'type' => 'package',
                'description' => 'Cung cấp cho quảng cáo của bạn một giao diện khác và luôn cập nhật kết quả tìm kiếm.',
                'created_at' => NULL,
                'updated_at' => '2017-07-25 19:42:14',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Ưu Tiên',
                'price' => '30000',
                'days' => 30,
                'type' => 'package',
                'description' => 'Làm cho quảng cáo của bạn nổi bật so với đám đông',
                'created_at' => NULL,
                'updated_at' => '2017-07-25 19:43:13',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Cần bán gấp',
                'price' => '10000',
                'days' => 7,
                'type' => 'boost',
                'description' => '',
                'created_at' => '2017-07-15 05:26:07',
                'updated_at' => '2017-07-18 04:59:26',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'top',
                'price' => '10000',
                'days' => 10,
                'type' => 'boost',
                'description' => '',
                'created_at' => '2017-07-15 05:26:15',
                'updated_at' => '2017-07-15 05:26:15',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'nổi bật',
                'price' => '10000',
                'days' => 10,
                'type' => 'boost',
                'description' => '',
                'created_at' => '2017-07-15 05:26:25',
                'updated_at' => '2017-07-15 05:26:25',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'home',
                'price' => '10000',
                'days' => 10,
                'type' => 'boost',
                'description' => '',
                'created_at' => '2017-07-15 05:26:30',
                'updated_at' => '2017-07-15 05:26:30',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Nổi bật',
                'price' => '15000',
                'days' => 14,
                'type' => 'boost',
                'description' => '',
                'created_at' => '2017-07-20 00:38:54',
                'updated_at' => '2017-07-20 00:38:54',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Nổi bật',
                'price' => '2000',
                'days' => 21,
                'type' => 'boost',
                'description' => '',
                'created_at' => '2017-07-20 00:39:27',
                'updated_at' => '2017-07-20 00:39:27',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Hiện ở trang chủ',
                'price' => '20000',
                'days' => 14,
                'type' => 'boost',
                'description' => '',
                'created_at' => '2017-07-20 00:39:59',
                'updated_at' => '2017-07-20 00:39:59',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'Hiện ở trang chủ',
                'price' => '25000',
                'days' => 21,
                'type' => 'boost',
                'description' => '',
                'created_at' => '2017-07-20 00:40:10',
                'updated_at' => '2017-07-20 00:40:10',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'Tốp đầu trang tìm kiếm',
                'price' => '35000',
                'days' => 21,
                'type' => 'boost',
                'description' => '',
                'created_at' => '2017-07-20 00:40:24',
                'updated_at' => '2017-07-20 00:40:24',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'Tốp đầu trang tìm kiếm',
                'price' => '30000',
                'days' => 14,
                'type' => 'boost',
                'description' => '',
                'created_at' => '2017-07-20 00:40:43',
                'updated_at' => '2017-07-20 00:40:43',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'Tốp đầu trang tìm kiếm',
                'price' => '30000',
                'days' => 14,
                'type' => 'boost',
                'description' => '',
                'created_at' => '2017-07-20 00:41:19',
                'updated_at' => '2017-07-25 12:33:04',
                'deleted_at' => '2017-07-25 12:33:04',
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'Hiển thị mockup',
                'price' => '15000',
                'days' => 14,
                'type' => 'boost',
                'description' => '',
                'created_at' => '2017-07-20 00:41:48',
                'updated_at' => '2017-07-20 00:41:48',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'Hiển thị mockup',
                'price' => '21000',
                'days' => 21,
                'type' => 'boost',
                'description' => '',
                'created_at' => '2017-07-20 00:41:58',
                'updated_at' => '2017-07-20 00:41:58',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}