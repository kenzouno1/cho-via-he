<?php

use Illuminate\Database\Seeder;

class PaymentPackageTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('payment_package')->delete();
        
        \DB::table('payment_package')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => '100.000 đ',
                'price' => '100000',
                'money' => '100000',
                'status' => 1,
                'created_at' => '2017-08-18 08:51:49',
                'updated_at' => '2017-09-27 15:13:38',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => '200.000 đ',
                'price' => '200000',
                'money' => '200000',
                'status' => 1,
                'created_at' => '2017-08-18 08:51:56',
                'updated_at' => '2017-09-27 15:14:07',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => '20.000 đ',
                'price' => '20000',
                'money' => '20000',
                'status' => 1,
                'created_at' => '2017-08-18 08:52:22',
                'updated_at' => '2017-09-27 15:14:12',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => '50.000 đ',
                'price' => '50000',
                'money' => '50000',
                'status' => 1,
                'created_at' => '2017-08-18 08:52:28',
                'updated_at' => '2017-09-27 15:14:19',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => '500.000 đ',
                'price' => '500000',
                'money' => '500000',
                'status' => 1,
                'created_at' => '2017-08-18 08:52:41',
                'updated_at' => '2017-09-27 15:14:26',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => '1.000.000 đ',
                'price' => '1000000',
                'money' => '1000000',
                'status' => 1,
                'created_at' => '2017-08-18 08:52:51',
                'updated_at' => '2017-09-27 15:13:53',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => '1.500.000 đ',
                'price' => '1500000',
                'money' => '1500000',
                'status' => 1,
                'created_at' => '2017-08-18 08:53:00',
                'updated_at' => '2017-09-27 15:14:36',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => '2.000.000 đ',
                'price' => '2000000',
                'money' => '2000000',
                'status' => 1,
                'created_at' => '2017-08-18 08:53:07',
                'updated_at' => '2017-09-27 15:14:42',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}