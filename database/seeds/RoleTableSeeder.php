<?php
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $time = date('Y-m-d H:i:s');
        DB::table('roles')->insert([
            'name' => 'administrator',
            'created_at'=> $time,
            'updated_at'=> $time,
        ]);
        DB::table('roles')->insert([
            'name' => 'employee',
            'created_at'=> $time,
            'updated_at'=> $time,
        ]);
    }
}