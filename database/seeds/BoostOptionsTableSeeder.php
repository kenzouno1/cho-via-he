<?php

use Illuminate\Database\Seeder;

class BoostOptionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('boost_options')->delete();
        
        \DB::table('boost_options')->insert(array (
            0 => 
            array (
                'id' => 1,
                'boost_id' => 1,
                'key' => 'view',
                'value' => '',
            ),
            1 => 
            array (
                'id' => 2,
                'boost_id' => 2,
                'key' => 'view',
                'value' => 'x2',
            ),
            2 => 
            array (
                'id' => 3,
                'boost_id' => 3,
                'key' => 'view',
                'value' => 'x4',
            ),
            3 => 
            array (
                'id' => 4,
                'boost_id' => 4,
                'key' => 'view',
                'value' => 'x12',
            ),
            4 => 
            array (
                'id' => 5,
                'boost_id' => 1,
                'key' => 'star',
                'value' => '2',
            ),
            5 => 
            array (
                'id' => 6,
                'boost_id' => 2,
                'key' => 'star',
                'value' => '3',
            ),
            6 => 
            array (
                'id' => 7,
                'boost_id' => 3,
                'key' => 'star',
                'value' => '4',
            ),
            7 => 
            array (
                'id' => 8,
                'boost_id' => 4,
                'key' => 'star',
                'value' => '5',
            ),
            8 => 
            array (
                'id' => 10,
                'boost_id' => 6,
                'key' => 'features',
                'value' => 'top-search',
            ),
            9 => 
            array (
                'id' => 11,
                'boost_id' => 7,
                'key' => 'features',
                'value' => 'highlight',
            ),
            10 => 
            array (
                'id' => 12,
                'boost_id' => 8,
                'key' => 'features',
                'value' => 'show-home',
            ),
            11 => 
            array (
                'id' => 13,
                'boost_id' => 5,
                'key' => 'features',
                'value' => 'has-mockup',
            ),
            12 => 
            array (
                'id' => 21,
                'boost_id' => 2,
                'key' => 'features',
                'value' => 'highlight',
            ),
            13 => 
            array (
                'id' => 25,
                'boost_id' => 9,
                'key' => 'features',
                'value' => 'highlight',
            ),
            14 => 
            array (
                'id' => 26,
                'boost_id' => 10,
                'key' => 'features',
                'value' => 'highlight',
            ),
            15 => 
            array (
                'id' => 27,
                'boost_id' => 11,
                'key' => 'features',
                'value' => 'show-home',
            ),
            16 => 
            array (
                'id' => 28,
                'boost_id' => 12,
                'key' => 'features',
                'value' => 'show-home',
            ),
            17 => 
            array (
                'id' => 29,
                'boost_id' => 13,
                'key' => 'features',
                'value' => 'top-search',
            ),
            18 => 
            array (
                'id' => 30,
                'boost_id' => 14,
                'key' => 'features',
                'value' => 'top-search',
            ),
            19 => 
            array (
                'id' => 31,
                'boost_id' => 15,
                'key' => 'features',
                'value' => 'top-search',
            ),
            20 => 
            array (
                'id' => 32,
                'boost_id' => 16,
                'key' => 'features',
                'value' => 'has-mockup',
            ),
            21 => 
            array (
                'id' => 33,
                'boost_id' => 17,
                'key' => 'features',
                'value' => 'has-mockup',
            ),
            22 => 
            array (
                'id' => 49,
                'boost_id' => 3,
                'key' => 'features',
                'value' => 'top-search',
            ),
            23 => 
            array (
                'id' => 50,
                'boost_id' => 3,
                'key' => 'features',
                'value' => 'highlight',
            ),
            24 => 
            array (
                'id' => 51,
                'boost_id' => 4,
                'key' => 'features',
                'value' => 'top-search',
            ),
            25 => 
            array (
                'id' => 52,
                'boost_id' => 4,
                'key' => 'features',
                'value' => 'highlight',
            ),
            26 => 
            array (
                'id' => 53,
                'boost_id' => 4,
                'key' => 'features',
                'value' => 'has-gallery',
            ),
        ));
        
        
    }
}