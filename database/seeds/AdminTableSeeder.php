<?php

use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $time = date('Y-m-d H:i:s');
        Db::table('admins')->insert(
            array(
                'username'       => 'blacker',
                'name'           => 'Dat Nguyen',
                'email'          => 'datnt37@gmail.com',
                'password'       => bcrypt('123456'),
                'birthday'       => null,
                'phone'          => '01678718468',
                'address'        => null,
                'avatar'         => null,
                'status'         => 'active',
                'remember_token' => '',
                'created_at'     => $time,
                'updated_at'     => $time,
            )
        );
    }
}
