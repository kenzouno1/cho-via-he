<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleTableSeeder::class);
        $this->call(AdminTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(Capability::class);
        $this->call(BoostsTableSeeder::class);
        $this->call(BoostOptionsTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
//        $this->call(AdsImagesTableSeeder::class);
        $this->call(SmsCodeTableSeeder::class);

        $this->call(PaymentPackageTableSeeder::class);
        $this->call(AdsTableSeeder::class);
        $this->call(AdsBoostTableSeeder::class);
        $this->call(AdsMetaTableSeeder::class);
    }
}
